//
//  UserDefaults.swift
//  hivejournal
//
//  Created by Dev on 5/22/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation

extension UserDefaults {
    var userSession: Any? {
        get { return object(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }
  
    var userId: String? {
        get { return string(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }

    var userHistory: Data? {
        get { return data(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }

    var lastNote: Data? {
        get { return data(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }

    var userIntentsHistory: Data? {
        get { return data(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }

    var noteBody: String? {
        get { return string(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }

    var notesList: Data? {
        get { return data(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }
}
