//
//  IntentHandler.swift
//  CreateNoteIntents
//
//  Created by Dev on 7/16/20.
//

import Intents
@available(iOS 10.0, *)
class IntentHandler: INExtension, CreateNoteIntentHandling {
    override func handler(for intent: INIntent) -> Any {
        // This is the default implementation.  If you want different objects to handle different intents,
        // you can override this and return the handler you want for that particular intent.
        
        return self
    }
    @available(iOS 12.0, *)
    func resolveBody(for intent: CreateNoteIntent, with completion: @escaping (INStringResolutionResult) -> Void) {
        if let body = intent.body {
              completion(.success(with: body))
        } else {
              completion(.needsValue())
        }
    }
    
    @available(iOS 12.0, *)
    func handle(intent: CreateNoteIntent, completion: @escaping (CreateNoteIntentResponse) -> Void) {
        if let note = Note(from: intent) {
            let storage = Storage()
            if storage.store(note) {
                completion(CreateNoteIntentResponse.success(noteBody: note.body))
            } else {
                completion(CreateNoteIntentResponse(code: .failure, userActivity: nil))
            }
        } else {
            completion(CreateNoteIntentResponse(code: .failure, userActivity: nil))
        }
    }
    
}
