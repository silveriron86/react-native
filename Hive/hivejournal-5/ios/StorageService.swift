//
//  File.swift
//  hivejournal
//
//  Created by Dev on 5/22/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation

protocol StorageService {
    func readHistory() throws -> Data
    func deleteHistory() -> Bool
    func store(_ note: Note) -> Bool
    func store(_ note: NoteType) -> Bool
}

enum HistoryError: Error {
    case generic
    case write
    case read
    case fileEmpty
}

class VideoMode {
  var interlaced = false
  var frameRate = 0.0
  var name: String?
}

final class Storage: StorageService {
    private let suitName = "group.com.hivejournal.mobile2.Shared"
    
    func saveUserId(_ userId: String) throws {
        let defaults = UserDefaults.standard
        defaults.set(userId, forKey: "USER_ID")
      
        if let userDefaults = UserDefaults(suiteName: suitName) {
            userDefaults.userId = userId
        } else {
            throw HistoryError.write
        }
    }
  
    func readHistory() throws -> Data {
        if let userDefaults = UserDefaults(suiteName: suitName),
            let history = userDefaults.userHistory {
            return history
        }
        
        throw HistoryError.fileEmpty
    }

    func deleteHistory() -> Bool {
        if let userDefaults = UserDefaults(suiteName: suitName) {
            userDefaults.userHistory = nil
            return true
        }
        return false
    }

    func readTypes() throws -> Data {
        if let userDefaults = UserDefaults(suiteName: suitName),
            let history = userDefaults.notesList {
            return history
        }

        throw HistoryError.fileEmpty
    }

    func deleteTypes() -> Bool {
        if let userDefaults = UserDefaults(suiteName: suitName) {
            userDefaults.notesList = nil
            return true
        }
        return false
    }

    func deleteType(at idx: Int) -> Bool {
        do {
            let json = try readTypes()

            if let list = Convert.convertToNoteTypes(json) {
                var newList = list
                newList.remove(at: idx)
                if let newJson = Convert.convertToData(newList) {
                    try saveTypes(newJson)
                }
            }
        } catch {
            logger("Read Error")
            return false
        }
        return true
    }

    func store(_ note: Note) -> Bool {

        if #available(iOS 12.0, *) {
          SiriService.donateInteraction(note.intent) { error in
            if let error = error {
              logger(error)
            }
          }
        } else {
          // Fallback on earlier versions
        }
        
        do {
            let json = try readHistory()

            if let list = Convert.convertToNotes(json) {
                var newList = list
                newList.append(note)
                if let newJson = Convert.convertToData(newList) {
                    try saveHistory(newJson)
                }
            }
        } catch let error as HistoryError {
            if error == .fileEmpty {
                if let newJson = Convert.convertToData([note]) {
                    try? saveHistory(newJson)
                }
            } else {
                logger("\(error.localizedDescription)")
                return false
            }
        } catch {
            logger("Read Error")
            return false
        }
        return true
    }

    func store(_ note: NoteType) -> Bool {

        do {
            let json = try readTypes()

            if let list = Convert.convertToNoteTypes(json) {
                var newList = list
                newList.append(note)
                if let newJson = Convert.convertToData(newList) {
                    try saveTypes(newJson)
                }
            }
        } catch let error as HistoryError {
            if error == .fileEmpty {
                if let newJson = Convert.convertToData([note]) {
                    try? saveTypes(newJson)
                }
            } else {
                logger("\(error.localizedDescription)")
                return false
            }
        } catch {
            logger("Read Error")
            return false
        }
        return true
    }

    func getLastNote() -> Note? {
        guard let data = try? readHistory() else {
            return nil
        }

        let history = Convert.convertToNotes(data)
        return history?.sorted { $0.timestamp > $1.timestamp }.first
    }

    func saveHistory(_ data: Data) throws {
        if let userDefaults = UserDefaults(suiteName: suitName) {
            userDefaults.userHistory = data
        } else {
            throw HistoryError.write
        }
    }

    // MARK: - Private
    private func saveTypes(_ data: Data) throws {
        if let userDefaults = UserDefaults(suiteName: suitName) {
            userDefaults.notesList = data
        } else {
            throw HistoryError.write
        }
    }
}

struct Convert {
    static func convertToNote(_ data: Data) -> Note? {
        let decoder = JSONDecoder()
        return try? decoder.decode(Note.self, from: data)
    }

    static func convertToNotes(_ data: Data) -> [Note]? {
        let decoder = JSONDecoder()
        return try? decoder.decode([Note].self, from: data)
    }

    static func convertToData<T: Equatable&Codable>(_ note: T) -> Data? {
        let encoder = JSONEncoder()
        return try? encoder.encode(note)
    }

    static func convertToData<T: Equatable&Codable>(_ list: [T]) -> Data? {
        let encoder = JSONEncoder()
        return try? encoder.encode(list)
    }

    static func convertToNoteType(_ data: Data) -> NoteType? {
        let decoder = JSONDecoder()
        return try? decoder.decode(NoteType.self, from: data)
    }

    static func convertToNoteTypes(_ data: Data) -> [NoteType]? {
        let decoder = JSONDecoder()
        return try? decoder.decode([NoteType].self, from: data)
    }
}
