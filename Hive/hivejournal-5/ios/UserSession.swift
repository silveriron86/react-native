//
//  UserSession.swift
//  hivejournal
//
//  Created by Dev on 5/22/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation

enum UserSession: String {
    case History
    case SartSession
    case Home
}
