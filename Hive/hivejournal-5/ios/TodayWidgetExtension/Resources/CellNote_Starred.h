//
//  CellNote_Starred.h
//  TodayWidgetExtension
//
//  Created by Dev on 6/15/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellNote_Starred : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *contentSubView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIButton *btnCircle;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstLine;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondLine;
@property (weak, nonatomic) IBOutlet UIButton *btnStar;


-(void)initCell:(NSString*)firstLine Second:(NSString*)secondLine Completed:(Boolean)isCompleted;

@end

NS_ASSUME_NONNULL_END
