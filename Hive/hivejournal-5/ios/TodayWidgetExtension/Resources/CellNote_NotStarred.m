//
//  CellNote_NotStarred.m
//  TodayWidgetExtension
//
//  Created by Dev on 6/16/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "CellNote_NotStarred.h"

@implementation CellNote_NotStarred

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

  self.contentSubView.layer.masksToBounds = YES;
  self.contentSubView.layer.cornerRadius = 8;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initCell:(NSString *)firstLine Second:(NSString *)secondLine Completed:(Boolean)isCompleted{
  
  [self.lblFirstLine setText:firstLine];
  [self.lblSecondLine setText:secondLine];
  if(isCompleted == YES) {
    [self.btnCircle setBackgroundImage:[UIImage imageNamed:@"ic_circle_option_filled.png"] forState:UIControlStateNormal];
  }else {
    [self.btnCircle setBackgroundImage:[UIImage imageNamed:@"ic_circle_option.png"] forState:UIControlStateNormal];
  }
}

- (IBAction)btnCircleTapped:(UIButton *)sender {
  [[NSNotificationCenter defaultCenter]
  postNotificationName:@"btnCircleTapped"
  object:nil];
}

- (IBAction)btnStarTapped:(UIButton *)sender {
  [[NSNotificationCenter defaultCenter]
  postNotificationName:@"btnCircleTapped"
  object:nil];
}

@end
