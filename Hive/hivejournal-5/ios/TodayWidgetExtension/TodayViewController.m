//
//  TodayViewController.m
//  TodayWidgetExtension
//
//  Created by Dev on 6/4/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "Webservice/HttpConnnection/WebServiceClient.h"
#import "CocoaSecurity/CocoaSecurity.h"
#import "CellNote_Starred.h"
#import "CellNote_NotStarred.h"

@interface TodayViewController () <NCWidgetProviding>

@end

@implementation TodayViewController

NSArray *tableData;
NSArray *tableNotData;

- (void)viewDidLoad {
  [super viewDidLoad];
  
  _starredTableView.delegate = self;
  _starredTableView.dataSource = self;

  
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  [formatter setDateFormat:@"EEEE MMMM d"];
  NSDate *currentDate = [NSDate date];
  _dateLabel.text = [formatter stringFromDate:currentDate];
  _dateLabel.textColor = [self getUIColorObjectFromHexString:@"4FC7FF" alpha: 1.0];
  _titleLabel.textColor = _dateLabel.textColor;
  
  self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth;

  [self reloadTable];
//  [NSTimer scheduledTimerWithTimeInterval:60 repeats:YES block:^(NSTimer * _Nonnull timer) {
//    [self reloadTable];
//  }];
}

- (void)viewWillAppear:(BOOL)animated{
  [self.view layoutIfNeeded];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
  selector:@selector(btnCircleTapped:)
  name:@"btnCircleTapped"
  object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
  [super viewWillDisappear:animated];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)btnCircleTapped:(id)sender{
  NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.hivejournal.mobile2.Shared"];
  [defaults setValue:@"YES" forKey:@"FROM_TODAY"];
  NSString *fromToday = [defaults stringForKey:@"FROM_TODAY"];
  [[self extensionContext] openURL:[NSURL URLWithString:@"com.hivejournal.mobile2://"] completionHandler:nil];
}

- (unsigned int)intFromHexString:(NSString *)hexStr
{
  unsigned int hexInt = 0;

  // Create scanner
  NSScanner *scanner = [NSScanner scannerWithString:hexStr];

  // Tell scanner to skip the # character
  [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];

  // Scan hex value
  [scanner scanHexInt:&hexInt];

  return hexInt;
}

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
  // Convert hex string to an integer
  unsigned int hexint = [self intFromHexString:hexStr];

  // Create a color object, specifying alpha as well
  UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
    blue:((CGFloat) (hexint & 0xFF))/255
    alpha:alpha];

  return color;
}

- (void)reloadTable {
//    NSString *secret = @"gmu4V2Mgmu4V2MCzST5CzST5";
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.hivejournal.mobile2.Shared"];
    NSString *userId = [defaults stringForKey:@"USER_ID"];
    if (userId != nil) {
      NSString *todoJson = [defaults stringForKey:@"TODO_NOTES"];
      if (todoJson != nil) {
        NSMutableDictionary* dataObject = (NSMutableDictionary*)[WebServiceClient jsonFrom:todoJson];
        tableData = (NSArray*)[dataObject objectForKey:@"starredData"];
        tableNotData = (NSArray*)[dataObject objectForKey:@"notStarredData"];
        [self.starredTableView reloadData];
        
        if([tableData count] == 0 && [tableNotData count] == 0) {
          self.starredTableView.hidden = YES;
        } else {
          self.starredTableView.hidden = NO;
        }
      }
    } else {
      self.starredTableView.hidden = YES;
    }
    
  [self.extensionContext setWidgetLargestAvailableDisplayMode:NCWidgetDisplayModeExpanded];
}

- (void)widgetActiveDisplayModeDidChange:(NCWidgetDisplayMode)activeDisplayMode withMaximumSize:(CGSize)maxSize {
    if (activeDisplayMode == NCWidgetDisplayModeExpanded) {
      NSInteger starredCnt = tableData.count;
      NSInteger notStarredCnt = tableNotData.count;
      if(starredCnt == 0 && notStarredCnt == 0) {
        self.preferredContentSize = CGSizeMake(0, 250);
      } else {
        self.preferredContentSize = CGSizeMake(0, 80 * ((starredCnt > 3 ? 3 : starredCnt) + (notStarredCnt > 3 ? 3 : notStarredCnt)) + 127);
      }
    } else if (activeDisplayMode == NCWidgetDisplayModeCompact) {
        self.preferredContentSize = maxSize;
    }
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
  NSMutableDictionary *note = (indexPath.section == 0) ? [[tableData objectAtIndex:indexPath.row] objectForKey:@"note"] : [[tableNotData objectAtIndex:indexPath.row] objectForKey:@"note"];
  NSString *noteText = [note objectForKey:@"decrypted_note"];
  NSString *noteTagsText = @"";
  NSArray *noteTags = [note objectForKey:@"note_tags"];
  BOOL isCompleted = [[note objectForKey:@"completed"] boolValue];
  if(noteTags != (id)[NSNull null]) {
    if([noteTags count] > 0) {
      noteTagsText = [NSString stringWithFormat:@"#%@", [noteTags componentsJoinedByString:@" #"]];
    }
  }

  if(indexPath.section == 0) {
    CellNote_Starred* cell =  [tableView dequeueReusableCellWithIdentifier:@"CellNote_Starred"];

    if(cell != nil){
      [cell initCell:noteText Second:noteTagsText Completed:isCompleted];
      return cell;
    }
  } else {
    CellNote_NotStarred* cell =  [tableView dequeueReusableCellWithIdentifier:@"CellNote_NotStarred"];

    if(cell != nil){
      [cell initCell:noteText Second:noteTagsText Completed:isCompleted];
      return cell;
    }
  }
  return [[UITableViewCell alloc] init];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  return 2;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
  NSInteger ret = section == 0 ? [tableData count] : [tableNotData count];
  return (ret > 3) ? 3 : ret;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return 80;
}


@end


