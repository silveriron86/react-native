//
//  IntentModule.swift
//  hivejournal
//
//  Created by Dev on 5/23/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

@objc(IntentModule)
class IntentModule: NSObject {
  @objc
  func getNotes(_ callback: RCTResponseSenderBlock) {
    do {
      let storage = Storage()
      let data = try storage.readHistory()
//      let decoder = JSONDecoder()
//      let list = try decoder.decode([Note].self, from: data)
      callback([String(data: data, encoding: .utf8) ?? ""])
    } catch {
      callback([""])
    }
  }
  
  @objc
  func clearNotes() {
    let storage = Storage()
    storage.deleteHistory()
  }
  
  @objc
  func updateNotes(_ json: String) {
    do {
      let storage = Storage()
      try storage.saveHistory(json.data(using: .utf8)!)
    } catch {
    }
  }
  
  @objc
  func saveUserId(_ userId: String) {
    if let userDefaults = UserDefaults(suiteName: "group.com.hivejournal.mobile2.Shared") {
        userDefaults.set(userId, forKey: "USER_ID")
        userDefaults.synchronize()
    }
  }
  
  @objc
  func isFromToday(_ callback: RCTResponseSenderBlock) {
    let userDefaults = UserDefaults(suiteName: "group.com.hivejournal.mobile2.Shared")
    let fromToday = userDefaults?.object(forKey: "FROM_TODAY")
    callback([fromToday ?? ""])
  }
  
  @objc
  func removeFromToday() {
    if let userDefaults = UserDefaults(suiteName: "group.com.hivejournal.mobile2.Shared") {
        userDefaults.set("NO", forKey: "FROM_TODAY")
        userDefaults.synchronize()
    }
  }

  @objc
  func saveTodoNotes(_ json: String) {
    if let userDefaults = UserDefaults(suiteName: "group.com.hivejournal.mobile2.Shared") {
        userDefaults.set(json, forKey: "TODO_NOTES")
        userDefaults.synchronize()
    }
  }
}
