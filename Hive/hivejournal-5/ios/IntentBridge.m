//
//  FooBridge.m
//  hivejournal
//
//  Created by Dev on 5/23/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(IntentModule, NSObject)
RCT_EXTERN_METHOD(getNotes: (RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(clearNotes)
RCT_EXTERN_METHOD(updateNotes: (NSString *)json)
RCT_EXTERN_METHOD(saveUserId: (NSString *)json)
RCT_EXTERN_METHOD(saveTodoNotes: (NSString *)json)
RCT_EXTERN_METHOD(isFromToday: (RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(removeFromToday)
@end
