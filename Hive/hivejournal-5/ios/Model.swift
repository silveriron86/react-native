//
//  Model.swift
//  hivejournal
//
//  Created by Dev on 5/22/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation

struct Note: Codable {
    var timestamp: TimeInterval
    var body: String
}

extension Note: Equatable {
    static func == (lhs: Note, rhs: Note) -> Bool {
        return
            lhs.timestamp == rhs.timestamp &&
                lhs.body == rhs.body
    }
}

@available(iOS 12.0, *)
extension Note {
    var intent: CreateNoteIntent {
        let createNoteIntent = CreateNoteIntent()
        createNoteIntent.body = self.body
        return createNoteIntent
    }

    init?(from intent: CreateNoteIntent) {
        guard let body = intent.body else {
                return nil
        }

        self.init(timestamp: Date().timeIntervalSince1970,
                  body: body)
    }
}

struct NoteType: Codable {
    var body: String
}

extension NoteType: Equatable {
    static func == (lhs: NoteType, rhs: NoteType) -> Bool {
        return lhs.body == rhs.body
    }
}
