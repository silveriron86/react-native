/* eslint-disable react/no-string-refs */
/* eslint-disable no-shadow */
/* eslint-disable prettier/prettier */
/* eslint-disable no-undef */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  Alert,
  Image,
  Keyboard,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  AppState,
  StatusBar,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import axios from 'axios';
import storage from '@react-native-firebase/storage';
import ImageProgress from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import ModalWrapper from 'react-native-modal-wrapper';
import FlashMessage, {showMessage} from 'react-native-flash-message';
import ApiConstants from '../../constants/ApiConstants';
import LoadingOverlay from '../../widgets/LoadingOverlay';
import JournalCover from './JournalCover';
// import Spotify from 'rn-spotify-sdk';
import {MongoActions, SettingActions, CommonActions} from '../../actions';
import Utils from '../../utils';
// import Locker from '../../widgets/Locker';
import CheckBox from '../../widgets/CheckBox';
import ImageConstants from '../../constants/ImageConstants';
import styles from './_styles';
import TagsModal from './TagsModal';
import FooterTags from './FooterTags';
import ImageSelector from './ImageSelector';
import FooterTrigger from '../../widgets/FooterTrigger';
import FeatureButtons from '../Stream/Reader/HorizontalStream/FeatureButtons';
import ImageViewer from '../../widgets/ImageViewer';
import Locker from '../../widgets/Locker';
import SaveCancel from './SaveCancel';
import ColorPicker from '../../widgets/ColorPicker';
import Colors from '../../constants/Colors';
import JournalLocker from './JournalLocker';
import frontStyles from '../Stream/Reader/HorizontalStream/_styles';
import FontEdit from '../../screens/Settings/FontEdit';
import * as RootNavigation from '../../navigation/RootNavigation';

const ellipsesIcon = require('../../../assets/icons/dots3.png');
const closeIcon = require('../../../assets/icons/circle-plus.png');
const soundFileIcon = require('../../../assets/icons/sound-file_icon.png');

class NotesScreen extends React.Component {
  constructor(props) {
    super(props);

    let cloudIndexes = Utils.shuffle(ImageConstants.clouds.length - 1);
    this.defaultPermissions = {
      see: this.props.noteVisibility.standard[0],
      read: this.props.noteVisibility.standard[1],
    };
    this.state = {
      body: '',
      loading: false,
      selection: null,
      spotifyInitialized: false,
      keyboardShown: false,
      isCheckedLast: true,
      cloudIndexes: cloudIndexes,
      cloudOpacity: 0,
      isShowTagsModal: false,
      selectedTagIndex: 0,
      insertedTags: [],
      newTags: [],
      background: -1,
      fontColor: Colors.scheme.blue,
      isFirstVisit: true,
      footerMode: 1,
      isClickedEllipses: false,
      selectedPhoto: null,
      selectedPermissions: {...this.defaultPermissions},
      isImageViewVisible: false,
      isRecording: false,
      userId: '',
      audioNoteUploadList: [],
      navigating: false,
      uploading: false,
      progress: 0,
      isFontModalVisible: false,
      selectedFont: 'PremiumUltra5',
      selectedFontSize: 24,
    };
  }

  componentDidMount() {
    Utils.trackScreenView('User views Notes page');

    this.mount = true;
    AppState.addEventListener('change', this._handleAppStateChange);

    global.eventEmitter.addListener('SAVED_FLOW', (_data) => {
      showMessage({
        message: 'Saved it!',
        description:
          'Before you forget: What are you doing, thinking or feeling right now?',
        type: 'success',
        duration: 3000,
      });
      setTimeout(() => {
        this._setCloudsOpacity();
      }, 500);
    });

    global.eventEmitter.addListener('SAVED_RESTUP', (data) => {
      showMessage({
        message: "Anything to note while it's still fresh in your mind?",
        description:
          'Any dreams? Why did you get to bed at ' +
          data.goBedTime +
          '? Why were you up at ' +
          data.outBedTime +
          '? Why was your sleep ' +
          data.howSleep +
          '?',
        type: 'success',
        duration: 5000,
      });
    });

    global.eventEmitter.addListener('FAILED_QUEUE', (data) => {
      this.props.setFailedQueue({
        data: data,
      });
    });

    global.eventEmitter.addListener('NOTE_CREATED', () => {
      const {lastNote, tags} = this.props;
      if (lastNote && lastNote.note) {
        let index = this.state.selectedTagIndex;

        if (typeof lastNote.note.type !== 'undefined') {
          if (lastNote.note.type === 'rating') {
            lastType = 'flow';
          } else if (lastNote.note.type === '3Q') {
            lastType = '3questions';
          }
          let lastType = lastNote.note.type;
          tags.forEach((item, i) => {
            if (item.type === lastType) {
              index = i;
            }
          });
          if (lastType === 'flow') {
            this.setState({
              insertedTags: lastNote.note.note_tags
                ? lastNote.note.note_tags
                : [],
            });
          }
        }

        this.onSelectTag(index, true);
      }

      global.eventEmitter.addListener('CAME_FROM_TODO', (data) => {
        this.onSelectCover(data.index);
      });
    });

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );

    if (this.props.spotifyEnabled === true) {
      Utils.initSpotify(() => {
        this.setState({
          spotifyInitialized: true,
        });
      }).catch((error) => {
        console.log(error.message);
        // Alert.alert("Error", error.message);
        // showMessage({
        //   message: "Error",
        //   description: error.message,
        //   type: "error"
        // });
      });
    }

    // For failed queue
    this.syncFailedQueue();
    const syncMinutesInterval = 1;
    setInterval(() => {
      this.syncFailedQueue();
    }, 1000 * 60 * syncMinutesInterval);

    NetInfo.addEventListener((state) => {
      if (state.isConnected) {
        this.handleFirstConnectivityChange();
      }
    });
    // NetInfo.isConnected.addEventListener(
    //   'connectionChange',
    //   this.handleFirstConnectivityChange,
    // );

    // Clouds for Flow Journey
    setInterval(() => {
      this._setCloudsOpacity();
    }, 1000 * 60 * 10); // per 10 mins

    setTimeout(() => {
      this._setCloudsOpacity();
    }, 1000);

    global.eventEmitter.addListener('TOGGLE_FLOW_JOURNEY', () => {
      this._setCloudsOpacity();
    });

    AsyncStorage.getItem('USER_ID', (_err, user_id) => {
      if (user_id) {
        this.setState({
          userId: user_id,
        });
      }
    });
  }

  _setCloudsOpacity = () => {
    if (!this.mount) {
      return;
    }

    let newCloudsOpacity = Utils.getFlowCloudsOpacity(this.props.flowJourney);
    if (
      this.state.cloudOpacity !== newCloudsOpacity &&
      newCloudsOpacity === 1
    ) {
      Utils.trackEvent('Action', 'User sees clouds at 100%');
    }

    this.setState({
      cloudOpacity: newCloudsOpacity,
    });
  };

  handleFirstConnectivityChange = (isConnected) => {
    if (isConnected === true) {
      // if online, refresh notes
      global.eventEmitter.emit('NOTE_CREATED', {actual: false});
      this.syncFailedQueue();
    }
  };

  syncFailedQueue = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected === true) {
        let queue = JSON.parse(JSON.stringify(this.props.failedQueue));
        if (queue && queue.length > 0) {
          let note = queue[0].note;
          delete note.failed;

          if (note.main_tag !== 'todo' && this.props.toneEnabled === true) {
            let apiData = ApiConstants.getToneAnalyzerApi();
            const note_orig = Utils.decrypt(note.note_orig);
            if (note_orig) {
              axios({
                method: 'GET',
                url: `${apiData.url}/v3/tone?version=${
                  apiData.version
                }&text=${encodeURIComponent(note_orig)}`,
                headers: {
                  Authorization: 'Basic ' + Utils.btoa('apiKey:' + apiData.key),
                },
              })
                .then((response) => {
                  console.log(response.data);
                  this.setState({
                    loading: false,
                  });

                  if (response.status === 200) {
                    note.ibm_tone = {
                      document_tone: response.data.document_tone,
                    };
                  }
                  this._syncCreateNote(note, queue);
                })
                .catch((error) => {
                  console.log(error);
                });
            } else {
              // if note_orig is empty
              this._syncCreateNote(note, queue);
            }
          } else {
            this._syncCreateNote(note, queue);
          }
        }
      }
    });
  };

  _syncCreateNote = (note, queue) => {
    this.props.createNote({
      data: note,
      cb: (res) => {
        console.log(res);
        if (typeof res.error === 'undefined') {
          // success
          global.eventEmitter.emit('NOTE_CREATED', {});
          queue.shift();
          this.props.setFailedQueue({
            data: queue,
          });

          // If it succeeds, then move to the next one
          if (queue && queue.length > 0) {
            this.syncFailedQueue();
          }
        }
      },
    });
  };

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    this.mount = false;
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (!this.mount) {
      return;
    }

    if (nextAppState === 'active') {
      /*
        when the app is re-launched, before deciding whether to show the journal cover, 
        if the user had started composing a note and then switched to a different app 
        (meaning that there is unsaved content in a note, then do not show the journal cover, just show the note still on compose mode.)
      */
      // this.setState({
      //   isFirstVisit: true,
      // });

      // if (this.props.user.validatedPhone === false) {
      this.props.getUnencryptedNotes({
        cb: (res) => {
          if (res.length > 0) {
            console.log(`.:.Found ${res.length} unencrypted notes to save`);
            res.forEach((item, index) => {
              console.log(`.:.Reading unencrypted note index ${index}`);
              let origText = item.note.note_unencrypted;
              // console.debug(`.:.Unencrypted note object: ${JSON.stringify(item)}`)

              let text = origText;
              let tags = Utils.getTags(origText);
              if (tags) {
                tags.forEach((tag, i) => {
                  tags[i] = tag.substr(1);
                  text = text.replace(tag + ' ', '');
                });
                text = text.replace('#' + tags[tags.length - 1], '');
              }
              const {
                selectedFont,
                selectedFontSize,
                selectedPermissions,
                fontColor,
              } = this.state;

              // console.debug(`.:.Unencrypted note: About to set main tag for index ${index}`)

              // read "main_tag" as EITHER a "type" journal name or a "short code"
              let mainTag = 'journal';

              // // ERROR: Actions may not have an undefined "type" property
              // this.props.tags.forEach(item => {
              //   if (item.type === item.note.main_tag || item.short === item.note.main_tag) {
              //     mainTag = item.note.main_tag;
              //   }
              // });
              if (item.note && item.note.main_tag) {
                mainTag = item.note.main_tag;
              }

              // console.debug(`.:.Unencrypted note: About to map other tags for index ${index}`)

              if (tags) {
                tags.map((tag) => tag.toLowerCase());
              }
              // console.debug(`.:.Unencrypted note: About to assemble new note for index ${index}`)

              let note = {
                note_orig: Utils.encrypt(origText),
                note_text: Utils.encrypt(text),
                note_tags: tags,
                creation_timestamp: moment().tz(this.props.location).format(),
                entry_ip: '0.0.0.0',
                entry_app_type: 'mobile',
                entry_app_version: '1.0',
                word_count: Utils.getWords(origText),
                font: selectedFont,
                font_size: selectedFontSize,
                color: fontColor,
                permissions: selectedPermissions
                  ? selectedPermissions
                  : this.defaultPermissions,
                screen_name: this.props.screenName,
                main_tag: mainTag.toLowerCase(),
                hide: false,
                createdFromClientType: 'SMS',
                user_id: item.note.user_id,
              };
              console.log(`.:.About to create note with text: ${origText}`);

              this.props.createNote({
                data: note,
                cb: () => {
                  global.eventEmitter.emit('NOTE_CREATED', {});
                  console.log(`.:.Created note with text: ${origText}`);
                },
              });

              this.props.archiveNote({
                data: {
                  _id: item._id,
                },
                cb: (_res) => {
                  console.log(_res);
                  console.log(`.:.Archived note with text: ${origText}`);
                },
              });
            });
          }
        },
      });
      // }
    }
  };

  _keyboardDidShow = () => {
    this.setState({
      keyboardShown: true,
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      keyboardShown: false,
    });
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.tags !== nextProps.tags) {
      if (nextProps.tags[this.state.selectedTagIndex].used === false) {
        this.setState({
          selectedTagIndex: 0,
        });
      }
    }
  }

  _saveNote = (note) => {
    Utils.trackEvent('Action', 'User saves any note');

    AsyncStorage.getItem('USER_ID', (_err, user_id) => {
      let arg = JSON.parse(JSON.stringify(note));
      arg.user_id = user_id;
      this.setState(
        {
          loading: true,
        },
        () => {
          const {lastNote} = this.props;
          const {isCheckedLast} = this.state;

          // If a note is saved which has that checkbox checked,
          if (
            lastNote &&
            isCheckedLast &&
            lastNote.note &&
            (lastNote.note.type === 'rating' || lastNote.note.type === 'sleep')
          ) {
            console.log('Last id: ', lastNote.id);
            // set the previous note to be "archived" by calling the note archive API
            this.props.archiveNote({
              data: {
                _id: lastNote.id,
              },
              cb: (_res) => {
                // create a new entry that is the same as the one that was just archived,
                // except that the "note" will be set to the contents of this new note that was just saved.
                let copiedNote = JSON.parse(JSON.stringify(lastNote.note));
                copiedNote.note_orig = arg.note_orig;
                copiedNote.note_tags = arg.note_tags;
                copiedNote.word_count = arg.word_count;
                copiedNote.note_text = arg.note_text;
                copiedNote.ibm_tone = arg.ibm_tone;

                this.props.createNote({
                  data: copiedNote,
                  cb: (res) => {
                    console.log(note.creation_timestamp);
                    this.props.setLastNote({
                      value: null,
                      id: null,
                    });
                    this._callDone(res, user_id, copiedNote);
                  },
                });
              },
            });
          } else {
            // If unchecked, will process as normal
            this.props.createNote({
              data: arg,
              cb: (res) => {
                this.props.setLastNote({
                  value: arg,
                  id: res.insertedId,
                });

                this._callDone(res, user_id, note);
              },
            });
          }
        },
      );
    });
  };

  _callDone = (res, user_id, note) => {
    AsyncStorage.getItem('IBM_PERSONALITY', (_err, ibm_personality) => {
      console.log(ibm_personality);
      let user = {
        auth_provider: 'auth0',
        user_id_auth: user_id,
      };
      if (ibm_personality) {
        user.ibm_personality = JSON.parse(ibm_personality);
      }
      let data = {
        user: user,
      };
      console.log('*** Saved ***');
      console.log(data);

      const audioNoteUploadList = [...this.state.audioNoteUploadList];
      if (audioNoteUploadList.length > 0) {
        AsyncStorage.setItem(
          `AUDIO_FILES_${res.insertedId}`,
          JSON.stringify(audioNoteUploadList),
        );

        audioNoteUploadList.forEach((file) => {
          let index = file.indexOf('note-audio-recording_');
          const filename = file.substring(index);
          storage()
            .ref(`audio-transcribe-uploads/${filename}`)
            .putFile(file)
            .on(
              storage.TaskEvent.STATE_CHANGED,
              (snapshot) => {
                if (snapshot.state === storage.TaskState.SUCCESS) {
                  console.log(snapshot);
                }
                // this.setState(state);
              },
              () => {
                console.log('audio uploading failed');
              },
            );
        });
      }

      this.setState(
        {
          loading: false,
          isCheckedLast: true,
          body: '',
          selectedPermissions: this.defaultPermissions,
          isClickedEllipses: false,
          audioNoteUploadList: [],
        },
        () => {
          // Alert.alert('You have submitted the data Please check it');
          if (res.error) {
            Utils.saveFailedQueue('Note creation', note);
          } else {
            if (this.state.selectedPhoto) {
              this.props.updateNoteImage({
                note_id: res.insertedId,
                image_url: this.state.selectedPhoto,
                cb: (res) => {
                  console.log(res);
                  this.setState({
                    selectedPhoto: null,
                  });
                },
              });
            }
            showMessage({
              message: '.:. got it .:.',
              // description: 'You have submitted the data Please check it.',
              type: 'success',
            });
          }
          global.eventEmitter.emit('NOTE_CREATED', {});
        },
      );
    });
  };

  _getRecentlyPlayed = (_session, _note) => {
    /*
    this.setState({
      loading: true
    }, () => {
      axios({
        method: 'GET',
        url: `${ApiConstants.getSpotifyApi().url}/me/player/recently-played`,
        headers: {
          Authorization: "Bearer " + session.accessToken
        }
      }).then(response => {
        // last 5 songs played
        let played = response.data.items;
        if(played.length > 5) {
          played = played.slice(0, 5);
        }
        note['spotify_history'] = played;
        this._saveNote(note)
      })
      .catch(error => {
        // error
        console.log(error);
        this.setState({
          loading: false
        }, ()=> {
          // Alert.alert("Error", error.message);
          // showMessage({
          //   message: "Error",
          //   description: error.message,
          //   type: "error"
          // });
        })
      })
    })*/
  };

  onCheckSpotify = (_note) => {
    /*
    let session = Spotify.getSession();
    if(session === null) {
      // log into Spotify
      Spotify.login().then((loggedIn) => {
        if(loggedIn) {
          // logged in
          session = Spotify.getSession();
          this._getRecentlyPlayed(session, note);
        }else {
          // Cancelled
          this._saveNote(note)
        }
      }).catch((error) => {
        console.log(error.message);
        // Alert.alert("Error", error.message);
        // showMessage({
        //   message: "Error",
        //   description: error.message,
        //   type: "error"
        // });
      });
    }else {
      this._getRecentlyPlayed(session, note);
    }*/
  };

  hideKeyboard = () => {
    Keyboard.dismiss();
  };

  onSaveNote = () => {
    this.hideKeyboard();
    setTimeout(() => {
      const {audioNoteUploadList, newTags, selectedTagIndex, body} = this.state;
      if (audioNoteUploadList.length === 0 && !body) {
        Alert.alert(null, 'Nothing to save...');
        return;
      }

      if (newTags.length === 0) {
        return this._onSaveNote();
      }

      Alert.alert(
        'Do you want to keep the new tags as pre-defined tags for future notes?',
        '',
        [
          {
            text: 'No',
            onPress: () => {
              this._onSaveNote();
            },
            style: 'cancel',
          },
          {
            text: 'Yes',
            onPress: () => {
              const {tags} = this.props;
              let data = JSON.parse(JSON.stringify(tags));
              newTags.forEach((t) => {
                data[selectedTagIndex].tags.push(t);
              });
              this.props.setSettings({type: 'TAGS', value: data});
              this._onSaveNote();
            },
          },
        ],
        {cancelable: false},
      );
    }, 500);
  };

  _onSaveNote = () => {
    const {
      insertedTags,
      fontColor,
      selectedPermissions,
      audioNoteUploadList,
      selectedFont,
      selectedFontSize,
    } = this.state;

    let origText = this.state.body;
    if (audioNoteUploadList.length > 0) {
      origText += ' .:. transcribing .:.';
    }
    if (insertedTags && insertedTags.length > 0) {
      insertedTags.forEach((t) => {
        origText += ` #${t}`;
      });
    }

    let text = origText;
    let tags = Utils.getTags(origText);
    if (tags) {
      tags.forEach((tag, i) => {
        tags[i] = tag.substr(1);
        text = text.replace(tag + ' ', '');
      });
      text = text.replace('#' + tags[tags.length - 1], '');
    }

    Keyboard.dismiss();

    let note = {
      note_orig: Utils.encrypt(origText),
      note_text: Utils.encrypt(text),
      note_tags: tags,
      creation_timestamp: moment().tz(this.props.location).format(),
      entry_ip: '0.0.0.0',
      entry_app_type: 'mobile',
      entry_app_version: '1.0',
      word_count: Utils.getWords(origText),
      font: selectedFont,
      font_size: selectedFontSize,
      color: fontColor,
      permissions: selectedPermissions
        ? selectedPermissions
        : this.defaultPermissions,
      background_image: this.state.background,
      screen_name: this.props.screenName,
      main_tag: this.props.tags[this.state.selectedTagIndex].type,
    };

    if (audioNoteUploadList.length > 0) {
      note['speech-to-text'] = true;
      // note['audio-upload-filename'] =
      note.audioDoneProcessing = false;
      const audioList = audioNoteUploadList.map((audio) => {
        let index = audio.indexOf('note-audio-recording_');
        return audio.substring(index);
      });
      note.audioNoteUploadList = audioList;
      note.audioNoteProcessedList = [];
      console.log(note);
    }

    this.setState({
      newTags: [],
      insertedTags: [],
    });

    if (note.main_tag !== 'todo' && this.props.toneEnabled === true) {
      let apiData = ApiConstants.getToneAnalyzerApi();
      this.setState(
        {
          loading: true,
        },
        () => {
          axios({
            method: 'GET',
            url: `${apiData.url}/v3/tone?version=${
              apiData.version
            }&text=${encodeURIComponent(origText)}`,
            headers: {
              Authorization: 'Basic ' + Utils.btoa('apiKey:' + apiData.key),
            },
          })
            .then((response) => {
              console.log(response.data);
              this.setState({
                loading: false,
              });

              if (response.status === 200) {
                note.ibm_tone = {
                  document_tone: response.data.document_tone,
                };
                this._procSpotifySaveNote(note);
              } else {
                this.setState(
                  {
                    loading: false,
                    body: '',
                    audioNoteUploadList: [],
                  },
                  () => {
                    Utils.saveFailedQueue('Tone analyzer service', note);
                  },
                );
              }
            })
            .catch((_error) => {
              this.setState(
                {
                  loading: false,
                  body: '',
                  audioNoteUploadList: [],
                },
                () => {
                  Utils.saveFailedQueue('Tone analyzer service', note);
                },
              );
            });
        },
      );
    } else {
      //disabled tone setting
      this._procSpotifySaveNote(note);
    }
  };

  onChangeText = (text) => {
    const {generalPreferences} = this.props;
    let chr = text.substr(this.state.selection.start - 1, 1);
    let code = chr.charCodeAt(0);
    if (generalPreferences.convertTagEnabled === true && code === 32) {
      // white space
      text = text.replace('  ', ' #');
      setTimeout(() => {
        this.setState(
          {
            body: text,
          },
          () => {
            this.updateNewTags(null);
          },
        );
      }, 1);
    } else if (generalPreferences.autoSaveEnabled === true && code === 10) {
      // Enter
      this.updateNewTags(this.onSaveNote);
    } else {
      this.setState({
        body: text,
      });
    }
  };

  updateNewTags = (cb) => {
    const {tags} = this.props;
    const {body, newTags, selectedTagIndex, insertedTags} = this.state;
    let data = JSON.parse(JSON.stringify(newTags));
    let insertedTagsData = JSON.parse(JSON.stringify(insertedTags));
    let curTags = Utils.getTags(body);
    let updatedBody = body;

    if (curTags) {
      curTags.forEach((t) => {
        if (
          newTags.indexOf(t.substr(1)) === -1 &&
          tags[selectedTagIndex].tags.indexOf(t.substr(1)) === -1
        ) {
          data.push(t.substr(1));
          insertedTagsData.push(t.substr(1));
        }

        let from = t + (cb ? '' : ' ');
        updatedBody = updatedBody.replace(from, '');
      });
      this.setState(
        {
          body: updatedBody,
          newTags: data,
          insertedTags: insertedTagsData,
        },
        () => {
          if (cb) {
            cb();
          }
        },
      );
    } else {
      if (cb) {
        cb();
      }
    }
  };

  _procSpotifySaveNote = (note) => {
    /*if(this.props.spotifyEnabled === true) {
      this.onCheckSpotify(note);
    }else {*/
    this._saveNote(note);
    // }
  };

  onKeyPress = (e) => {
    let text = this.state.body;
    const key = e.nativeEvent.key;
    if (key === 'Enter') {
      //aaa
    } else if (key === ' ') {
      text += key;
      text = text.replace('  ', ' #');
      this.setState({
        body: text,
      });
    } else {
      this.setState({
        body: text + key,
      });
    }
  };

  onCancelInput = () => {
    // clear out any text they have typed and remove any attachments they have added
    Keyboard.dismiss();
    this.setState({
      body: '',
      audioNoteUploadList: [],

      // If I tap cancel, after clearing out the note content, take me take me back to the notebook cover view
      isFirstVisit: true,
    });
  };

  onShowTagsModal = () => {
    const {isShowTagsModal} = this.state;
    if (isShowTagsModal === false) {
      Keyboard.dismiss();
    }
    setTimeout(() => {
      this.setState({
        isShowTagsModal: !isShowTagsModal,
      });
    }, 100);
  };

  onSelectTag = (index, isInit) => {
    this.setState(
      {
        selectedTagIndex: index,
        isShowTagsModal: false,
      },
      () => {
        if (isInit !== true) {
          this._updateNoteOptions(index);
        }
      },
    );
    global.eventEmitter.emit('CHANGED_COVER', {
      type: this.props.tags[index].type,
      index: index,
    });
  };

  _updateNoteOptions = (index) => {
    const {streamFont, fontSize, tags} = this.props;
    const tag = tags[index];
    this.setState({
      background:
        typeof tag.background !== 'undefined' && tag.background.length > 0
          ? Utils.getRandomInt(0, tag.background.length - 1)
          : -1,
      selectedFont: typeof tag.font === 'undefined' ? streamFont : tag.font,
      selectedFontSize:
        typeof tag.fontSize === 'undefined' ? fontSize : tag.fontSize,
    });
  };

  onInsertTag = (tag) => {
    /*
    const {body, selection} = this.state;
    let lText = '';
    let rText = '';
    if (selection) {
      lText = body.substr(0, selection.start);
      rText = body.substr(selection.start);
    }
    this.setState({
      body: lText + '#' + tag + rText
    });
    */
    let insertedTags = JSON.parse(JSON.stringify(this.state.insertedTags));
    let found = insertedTags.indexOf(tag);
    if (found > -1) {
      insertedTags.splice(found, 1);
    } else {
      insertedTags.push(tag);
    }
    this.setState({
      insertedTags: insertedTags,
    });
  };

  onSelectBackground = (index) => {
    const {background} = this.state;
    if (background !== index) {
      Utils.trackEvent('Action', 'User selects a background image');
    }

    this.setState(
      {
        background: background === index ? -1 : index,
      },
      () => {
        if (this.state.background === -1) {
          this.setState({
            fontColor: Colors.scheme.blue,
          });
        }
      },
    );
  };

  onPickColor = (color) => {
    const {background} = this.state;
    if (background === -1 && color === 'black') {
      return;
    }
    this.setState({
      fontColor: color,
    });
  };

  _visitTodoList = () => {
    RootNavigation.navigate('ToDo');
  };

  onSelectCover = (index, isFromSelect) => {
    const {footerMode} = this.state;
    const tag = this.props.tags[index];
    const isTodoJournal = tag.type === 'todo';
    global.eventEmitter.emit('CHANGED_COVER', {
      type: index >= 0 ? tag.type : 'All',
      index: index,
    });

    console.log(isTodoJournal, isFromSelect);
    if (isTodoJournal /* && isFromSelect === true*/) {
      this.setState(
        {
          selectedTagIndex: index,
        },
        () => {
          this._visitTodoList();
        },
      );
    } else {
      this.journalCoverRef.hideJournalSelector();
      this.setState(
        {
          selectedTagIndex: index,
          isFirstVisit: false,
        },
        () => {
          this._updateNoteOptions(index);
          console.log(footerMode);
          if (footerMode === 1) {
            if (this.noteTextInput) {
              this.noteTextInput.focus();
            }
          }
        },
      );
    }

    if (footerMode === 2) {
      RootNavigation.navigate('HomeStack');
      global.eventEmitter.emit('TO_FIRST_OWN_NOTE', {index: index});
    }

    if (!isTodoJournal || isFromSelect !== true) {
      this._doLock();
    }
  };

  onChangeCover = (type, index) => {
    console.log(type, index);
    this.setState({
      selectedTagIndex: index,
    });

    global.eventEmitter.emit('CHANGED_COVER', {type: type, index: index});
  };

  onToggleJournalSelector = () => {
    this.journalCoverRef.toggleJournalSelector();
  };

  onShowJournalSelector = () => {
    this.setState({
      isFirstVisit: true,
    });
    // this.journalCoverRef.showJournalSelector();
    this.onToggleJournalSelector();
  };

  onSelectJournal = (index) => {
    const {tags} = this.props;
    const {footerMode} = this.state;
    this._updateNoteOptions(index);

    global.eventEmitter.emit('CHANGED_COVER', {
      type: tags[index].type,
      index: index,
    });
    if (footerMode === 1) {
      // compose mode
      // open that journal and take them to the compose screen for that journal
      this.onSelectCover(index, true);
      this.onToggleJournalSelector();
    } else {
      // reader mode
      // open the horizontal/swipe reader for that journal.
      this.onToggleJournalSelector();
      this.setState(
        {
          selectedTagIndex: index,
        },
        () => {
          this.onSelectMode(1);
        },
      );
    }
  };

  _doLock = () => {
    const {passcode} = this.props;
    if (passcode.journalLocked !== true) {
      if (global.lockerTimer) {
        clearTimeout(global.lockerTimer);
      }

      global.lockerTimer = setTimeout(() => {
        let data = JSON.parse(JSON.stringify(passcode));
        data.journalLocked = true;
        this.props.setSettings({type: 'PASSCODE', value: data});
      }, passcode.after * 60 * 1000);
    }
  };

  onSelectMode = (mode) => {
    this.setState(
      {
        footerMode: mode,
      },
      () => {
        if (mode === 1) {
          RootNavigation.navigate('HomeStack');
          global.eventEmitter.emit('TO_FIRST_OWN_NOTE', {
            index: this.state.selectedTagIndex,
          });
        }
      },
    );
  };

  toggleEllipses = () => {
    const {isClickedEllipses} = this.state;
    if (!isClickedEllipses) {
      Keyboard.dismiss();
      global.eventEmitter.emit('NEW_TAB');
    } else {
      this.noteTextInput.focus();
    }
    this.setState({
      isClickedEllipses: !isClickedEllipses,
    });
  };

  onSelectPhoto = (url) => {
    console.log('**** select photo *** ', url);
    this.setState({
      selectedPhoto: url,
    });
  };

  onSelectPermissions = (permissions) => {
    console.log(permissions);
    this.setState({
      selectedPermissions: permissions,
    });
  };

  onToggleImage = () => {
    this.setState({
      isImageViewVisible: !this.state.isImageViewVisible,
    });
  };

  onDeleteImage = () => {
    this.onSelectPhoto(null);
  };

  onStartRecording = () => {
    this.setState({
      isRecording: true,
    });
  };

  onStopRecording = (audioPath) => {
    let audioNoteUploadList = this.state.audioNoteUploadList;
    console.log(audioNoteUploadList);
    audioNoteUploadList.push(audioPath);
    this.setState({
      isRecording: false,
      audioNoteUploadList: audioNoteUploadList,
    });
  };

  onUploading = (uploading, progress) => {
    this.setState({uploading, progress});
  };

  onToggleFontModal = () => {
    this.setState({
      isFontModalVisible: !this.state.isFontModalVisible,
    });
  };

  onSetFontSize = (size) => {
    this.setState({
      selectedFontSize: size,
    });
  };
  onSetFont = (font) => {
    this.setState({
      selectedFont: font,
    });
  };

  render() {
    const {
      body,
      loading,
      keyboardShown,
      isCheckedLast,
      cloudIndexes,
      cloudOpacity,
      isShowTagsModal,
      selectedTagIndex,
      insertedTags,
      newTags,
      background,
      fontColor,
      isFirstVisit,
      footerMode,
      isClickedEllipses,
      selectedPhoto,
      selectedPermissions,
      isImageViewVisible,
      isRecording,
      userId,
      audioNoteUploadList,
      uploading,
      progress,
      isFontModalVisible,
      selectedFont,
      selectedFontSize,
    } = this.state;

    const {fontSize, lastNote, generalPreferences, tags} = this.props;

    const w = Dimensions.get('window').width;
    const h = Dimensions.get('window').height; //- (Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 60 : 0)
    let ht = h - 100 - 70;
    let isVisibleFooter = RootNavigation.getVisibleFooter();
    let verticalOffset = isVisibleFooter ? 68 : 20;
    if (Utils.getInsetBottom() > 0) {
      verticalOffset = isVisibleFooter ? 90 : 42;
    }

    return (
      <Locker {...this.props} currentJournal={tags[selectedTagIndex]}>
        <JournalLocker
          {...this.props}
          currentJournal={tags[selectedTagIndex]}
          isCovered={isFirstVisit}>
          <SafeAreaView
            style={[styles.container, {backgroundColor: Colors.scheme.black}]}>
            <StatusBar
              backgroundColor={Colors.scheme.black}
              barStyle="light-content"
            />
            <ImageBackground
              style={{flex: 1}}
              imageStyle={{
                height: '100%',
                opacity: 0.4,
                backgroundColor: Colors.scheme.gray,
              }}
              source={
                generalPreferences.showBackgroundImg && background >= 0
                  ? ImageConstants.notes[background]
                  : null
              }>
              {!isFirstVisit && (
                <KeyboardAvoidingView
                  style={styles.container}
                  behavior="padding"
                  // keyboardVerticalOffset={Platform.select({ ios: isVisibleFooter ? 68 : 20, android: isVisibleFooter ? 68 : 20 })}
                  keyboardVerticalOffset={Platform.select({
                    ios: verticalOffset,
                    android: verticalOffset,
                  })}
                  enabled>
                  <View
                    style={[
                      styles.cloudWrapper,
                      {height: ht, opacity: cloudOpacity},
                    ]}>
                    <Image
                      source={ImageConstants.clouds[cloudIndexes[0] + 1]}
                      style={[styles.lCloudImage, {maxHeight: ht / 3}]}
                    />
                    <Image
                      source={ImageConstants.clouds[cloudIndexes[1] + 1]}
                      style={{
                        width: '60%',
                        maxHeight: ht / 3,
                        resizeMode: 'contain',
                      }}
                    />
                    <Image
                      source={ImageConstants.clouds[cloudIndexes[2] + 1]}
                      style={[styles.rCloudImage, {maxHeight: ht / 3}]}
                    />
                  </View>
                  <LoadingOverlay loading={loading} />
                  <FlashMessage position="center" />
                  {lastNote &&
                    lastNote.note &&
                    (lastNote.note.type === 'rating' ||
                      lastNote.note.type === 'sleep') && (
                      <CheckBox
                        checked={isCheckedLast}
                        type={
                          lastNote.note.type === 'sleep' ? 'Rest Up' : 'Flow'
                        }
                        onToggle={() => {
                          this.setState({
                            isCheckedLast: !this.state.isCheckedLast,
                          });
                        }}
                      />
                    )}
                  <View style={{flexDirection: 'row', marginTop: 40}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        height: '100%',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={this.onToggleImage}
                        style={{
                          alignSelf: 'center',
                          marginHorizontal: 5,
                        }}>
                        {selectedPhoto && (
                          <ImageProgress
                            source={{uri: selectedPhoto}}
                            indicator={Progress.Circle}
                            indicatorProps={{
                              size: 30,
                              borderWidth: 2,
                              color: '#3079c1',
                              unfilledColor: 'rgba(200, 200, 200, 0.2)',
                            }}
                            style={{
                              width: 40,
                              height: 40,
                              resizeMode: 'contain',
                            }}
                          />
                        )}
                      </TouchableOpacity>
                    </View>
                    <FooterTags
                      fontSize={fontSize}
                      selectedIndex={selectedTagIndex}
                      insertedTags={insertedTags}
                      allTags={tags}
                      newTags={newTags}
                      note={body}
                      insertTag={this.onInsertTag}
                      isComposed={true}
                    />
                  </View>
                  <View style={[styles.mainTagView, {marginTop: 5}]}>
                    <TouchableOpacity
                      onPress={this.onShowTagsModal}
                      style={styles.mainTagButton}>
                      <Text
                        style={[
                          styles.mainTagText,
                          {fontSize: fontSize, color: fontColor},
                        ]}>
                        #{tags[selectedTagIndex].short}
                      </Text>
                    </TouchableOpacity>
                    <TagsModal
                      allTags={tags}
                      visible={isShowTagsModal}
                      onCloseModal={this.onShowTagsModal}
                      onSelectTag={this.onSelectTag}
                      fontSize={fontSize}
                    />
                  </View>
                  <View style={{flex: 1, paddingLeft: 20}}>
                    <TextInput
                      ref={(ref) => {
                        this.noteTextInput = ref;
                      }}
                      onChangeText={this.onChangeText}
                      // onKeyPress={this.onKeyPress}
                      onSelectionChange={({nativeEvent: {selection}}) => {
                        this.setState({selection});
                      }}
                      style={[
                        styles.textArea,
                        {fontFamily: selectedFont, fontSize: selectedFontSize},
                        {color: fontColor},
                        // {borderWidth: 1, borderColor: 'white'},
                      ]}
                      autoCapitalize={'none'}
                      enablesReturnKeyAutomatically={true}
                      multiline={true}
                      value={body}
                      placeholderTextColor="white"
                    />
                  </View>
                  {isClickedEllipses && (
                    <View style={frontStyles.mainNoteOptions}>
                      <View style={{flex: 1}}>
                        {/* <View style={{height: 50}} /> */}
                        <View style={{flex: 1}}>
                          <FeatureButtons
                            position={'top'}
                            item={null}
                            permissions={selectedPermissions}
                            onSelectImage={this.onSelectPhoto}
                            onSelectPermissions={this.onSelectPermissions}
                            onUpdate={this.onUploading}
                          />
                        </View>
                        <View style={frontStyles.fontColorSelector}>
                          <TouchableOpacity
                            style={{marginRight: 5}}
                            onPress={this.onToggleFontModal}>
                            <Text style={frontStyles.fontButtonText}>Font</Text>
                          </TouchableOpacity>
                          <View style={{width: 100, paddingRight: 10}}>
                            <ColorPicker
                              color={fontColor}
                              onPickColor={this.onPickColor}
                              canPickBlack={background !== -1}
                              vertical={true}
                            />
                          </View>
                        </View>
                        <ImageSelector
                          value={background}
                          vertical={true}
                          onSelect={this.onSelectBackground}
                        />
                        <View style={{flex: 1}}>
                          <FeatureButtons
                            position={'bottom'}
                            item={null}
                            {...this.props}
                            permissions={selectedPermissions}
                            onSelectImage={this.onSelectPhoto}
                            onSelectPermissions={this.onSelectPermissions}
                            onUpdate={this.onUploading}
                          />
                        </View>
                      </View>
                      <View
                        style={[frontStyles.rightTagsSelector, {marginTop: 5}]}>
                        <FooterTags
                          fontSize={fontSize}
                          selectedIndex={selectedTagIndex}
                          insertedTags={insertedTags}
                          allTags={tags}
                          newTags={newTags}
                          note={body}
                          insertTag={this.onInsertTag}
                          vertical={true}
                        />
                      </View>

                      <ModalWrapper
                        containerStyle={[styles.modalContainer, {marginTop: 0}]}
                        style={[styles.modal, {width: w, height: h}]}
                        onRequestClose={this.onToggleFontModal}
                        shouldAnimateOnRequestClose={true}
                        shouldCloseOnOverlayPress={true}
                        visible={isFontModalVisible}
                        position={'right'}>
                        <View style={{flex: 1}}>
                          <FontEdit
                            onClose={this.onToggleFontModal}
                            isModal={true}
                            tagFont={selectedFont}
                            tagFontSize={selectedFontSize}
                            setFontSize={this.onSetFontSize}
                            setFont={this.onSetFont}
                          />
                        </View>
                      </ModalWrapper>
                    </View>
                  )}
                  {selectedPhoto !== '' && (
                    <ImageViewer
                      url={selectedPhoto}
                      visible={isImageViewVisible}
                      onToggle={this.onToggleImage}
                      onDelete={this.onDeleteImage}
                    />
                  )}

                  <TouchableOpacity
                    onPress={this.toggleEllipses}
                    style={styles.toggleButton}>
                    {!isClickedEllipses ? (
                      <Image
                        source={ellipsesIcon}
                        resizeMethod={'resize'}
                        style={styles.ellipsesIcon}
                      />
                    ) : (
                      <Image
                        source={closeIcon}
                        resizeMethod={'resize'}
                        style={[
                          styles.ellipsesIcon,
                          {
                            width: 40,
                            height: 40,
                            transform: [{rotate: '45deg'}],
                          },
                        ]}
                      />
                    )}
                  </TouchableOpacity>
                  {audioNoteUploadList.length > 0 ? (
                    <View style={styles.audioSaved}>
                      <Image
                        source={soundFileIcon}
                        style={styles.soundFileIcon}
                      />
                      <Text style={[styles.audioSavedText, {marginBottom: 45}]}>
                        {audioNoteUploadList.length} Audio File
                        {audioNoteUploadList.length > 1 && 's'} Saved
                      </Text>
                    </View>
                  ) : !keyboardShown ? (
                    <View style={styles.footerPadding} />
                  ) : null}
                  <SaveCancel
                    isRecording={isRecording}
                    generalPreferences={generalPreferences}
                    onSave={this.onSaveNote}
                    onCancel={this.onCancelInput}
                    onBook={() => this.refs.footerTrigger.onModeButtonTap(1)}
                    userId={userId}
                    mainTag={tags[selectedTagIndex].short}
                    onStart={this.onStartRecording}
                    onStop={this.onStopRecording}
                  />
                </KeyboardAvoidingView>
              )}

              <LoadingOverlay
                containerStyle={{height: Utils.getClientHeight() - 40}}
                loading={uploading}
                message={`Uploading: ${progress.toFixed(0)}%`}
              />

              <JournalCover
                ref={(ref) => (this.journalCoverRef = ref)}
                isFirstVisit={isFirstVisit}
                tags={tags}
                from="notes"
                index={selectedTagIndex}
                onPress={this.onSelectCover}
                onChange={this.onChangeCover}
                onSelectJournal={this.onSelectJournal}
                setSettings={this.props.setSettings}
              />
              <LoadingOverlay loading={this.state.navigating} />

              <FooterTrigger
                ref="footerTrigger"
                defaultJournal={this.props.tags[selectedTagIndex].type}
                flowJourney={this.props.flowJourney}
                screen="notes"
                color={fontColor}
                mode={footerMode}
                onSelectMode={this.onSelectMode}
                onToggleJournalSelector={this.onToggleJournalSelector}
                onShowJournalSelector={this.onShowJournalSelector}
                onPickColor={this.onPickColor}
                canPickBlack={background !== -1}
                onVisitTodo={this._visitTodoList}
              />
            </ImageBackground>
          </SafeAreaView>
        </JournalLocker>
      </Locker>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    screenName: state.SettingReducer.screenName,
    lastNote: state.CommonReducer.lastNote,
    mongoNotes: state.MongoReducer.mongoNotes,
    toneEnabled: state.SettingReducer.toneEnabled,
    spotifyEnabled: state.SettingReducer.spotifyEnabled,
    personalityEnabled: state.SettingReducer.personalityEnabled,
    streamFont: state.SettingReducer.streamFont,
    fontSize: state.SettingReducer.fontSize,
    noteVisibility: state.SettingReducer.noteVisibility,
    location: state.SettingReducer.location,
    generalPreferences: state.SettingReducer.generalPreferences,
    flowJourney: state.SettingReducer.flowJourney,
    tags: state.SettingReducer.tags,
    user: state.SettingReducer.user,
    error: state.MongoReducer.error,
    failedQueue: state.MongoReducer.failedQueue,
    passcode: state.SettingReducer.passcode,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getFilteredNotes: (req) =>
      dispatch(
        MongoActions.getFilteredNotes(req.tag, req.page, req.cb, req.size),
      ),
    archiveNote: (req) => dispatch(MongoActions.archiveNote(req.data, req.cb)),
    updateNoteImage: (req) =>
      dispatch(
        MongoActions.updateNoteImage(req.note_id, req.image_url, req.cb),
      ),
    createNote: (req) => dispatch(MongoActions.createNote(req.data, req.cb)),
    setLastNote: (req) =>
      dispatch(CommonActions.setLastNote(req.value, req.id)),
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
    setFailedQueue: (req) => dispatch(MongoActions.setFailedQueue(req.data)),
    getUnencryptedNotes: (req) =>
      dispatch(MongoActions.getUnencryptedNotes(req.cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NotesScreen);
