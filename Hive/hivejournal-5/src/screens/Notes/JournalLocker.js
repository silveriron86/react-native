/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Appearance, Text} from 'react-native';
import {connect} from 'react-redux';
import PINCode from '@haskkor/react-native-pincode';
import {SettingActions} from '../../actions';

class JournalLocker extends React.Component {
  constructor(props) {
    super(props);
    this.lockerTimer = null;
    this.state = {
      appearance: Appearance.getColorScheme(),
    };
  }

  componentDidMount() {
    this.mount = true;
    setInterval(() => {
      const appearance = Appearance.getColorScheme();
      if (this.mount && this.state.appearance !== appearance) {
        this.setState({
          appearance,
        });
      }
    }, 1000 * 5); // 10 seconds
  }

  doLock = () => {
    const {passcode} = this.props;
    if (passcode.journalLocked !== true) {
      if (global.lockerTimer) {
        clearTimeout(global.lockerTimer);
      }

      global.lockerTimer = setTimeout(() => {
        this.toggleLock(true);
      }, passcode.after * 60 * 1000);
    }
  };

  componentWillUnmount() {
    this.mount = false;
  }

  toggleLock = v => {
    if (!this.mount) {
      return;
    }

    let data = JSON.parse(JSON.stringify(this.props.passcode));
    data.journalLocked = v;
    this.props.setSettings({type: 'PASSCODE', value: data});
  };

  render() {
    const {passcode, children, currentJournal, isCovered} = this.props;
    const {appearance} = this.state;
    const isDark = appearance === 'dark';
    const modeTextColor = isDark ? 'white' : 'black';
    const modeBgColor = isDark ? 'black' : 'white';

    if (
      passcode.journalLocked === true &&
      passcode.enabled === true &&
      passcode.number !== '' &&
      currentJournal.locked === true &&
      isCovered === false
    ) {
      return (
        <View style={{flex: 1, backgroundColor: modeBgColor}}>
          <PINCode
            titleEnter={`Unlock Hive Journal "${currentJournal.type}"`}
            storedPin={passcode.number}
            status={'enter'}
            touchIDDisabled={true}
            stylePinCodeColorTitle={modeTextColor}
            stylePinCodeColorSubtitle={modeTextColor}
            onClickButtonLockedPage={() => {}}
            stylePinCodeButtonCircle={{
              backgroundColor: isDark ? '#1A1A1C' : 'rgb(242, 245, 251',
            }}
            handleResultEnterPin={result => {
              if (result === passcode.number) {
                this.toggleLock(false);
              }
            }}
          />
        </View>
      );
    }

    return children;
  }
}

const mapStateToProps = state => {
  return {
    passcode: state.SettingReducer.passcode,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setSettings: req =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(JournalLocker);
