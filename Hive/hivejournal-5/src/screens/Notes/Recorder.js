/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import moment from 'moment-timezone';
import {StyleSheet, Text, View, TouchableOpacity, Platform} from 'react-native';
import Sound from 'react-native-sound';
import {AudioRecorder, AudioUtils} from 'react-native-audio';

class Recorder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTime: 0.0,
      recording: false,
      paused: false,
      stoppedRecording: true,
      finished: false,
      audioPath: '',
      hasPermission: undefined,
    };
  }

  prepareRecordingPath = () => {
    const {userId, mainTag} = this.props;
    const timestamp = moment.utc().format('YYYYMMDDTHHmmss');
    const fileName = `note-audio-recording_${userId.replace(
      '|',
      '-',
    )}_${mainTag}_${timestamp}.aac`;
    const audioPath = AudioUtils.DocumentDirectoryPath + '/' + fileName;
    // console.log(audioPath);

    this.setState({
      audioPath: audioPath,
    });

    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: 'High',
      AudioEncoding: 'aac',
      AudioEncodingBitRate: 32000,
    });
  };

  componentDidMount() {
    AudioRecorder.requestAuthorization().then(isAuthorised => {
      this.setState({hasPermission: isAuthorised});

      if (!isAuthorised) {
        return;
      }

      // this.prepareRecordingPath();

      AudioRecorder.onProgress = data => {
        // console.log(data);
        let timestamp = Math.floor(data.currentTime);
        if (timestamp === 270) {
          // If recording length reaches 4.5 minutes:
          this._stop();
          this._record();
        }
        this.setState({currentTime: timestamp});
      };

      AudioRecorder.onFinished = data => {
        // Android callback comes in the form of a promise instead.
        if (Platform.OS === 'ios') {
          this._finishRecording(
            data.status === 'OK',
            data.audioFileURL,
            data.audioFileSize,
          );
        }
      };
    });
  }

  _stop = async () => {
    if (!this.state.recording) {
      console.warn("Can't stop, not recording!");
      return;
    }

    this._playStopSound();
    this.setState({
      stoppedRecording: true,
      recording: false,
      paused: false,
      currentTime: 0.0,
    });
    try {
      const filePath = await AudioRecorder.stopRecording();
      this.props.onStop(this.state.audioPath);

      if (Platform.OS === 'android') {
        this._finishRecording(true, filePath);
      }
      return filePath;
    } catch (error) {
      console.error(error);
    }
  };

  _playStopSound = () => {
    let sound = new Sound('stop.mp3', Sound.MAIN_BUNDLE, error => {
      if (error) {
        console.log('failed to load the sound', error);
      }
    });

    setTimeout(() => {
      sound.setNumberOfLoops(3);
      sound.play();
    }, 100);
  };

  _playStartSound = () => {
    let sound = new Sound('start.mp3', Sound.MAIN_BUNDLE, error => {
      if (error) {
        console.log('failed to load the sound', error);
      }
    });

    setTimeout(() => {
      sound.setNumberOfLoops(1);
      sound.play();
    }, 100);
  };

  _record = async () => {
    if (this.state.recording) {
      console.warn('Already recording!');
      return;
    }

    if (!this.state.hasPermission) {
      console.warn("Can't record, no permission granted!");
      return;
    }

    if (this.state.stoppedRecording) {
      this.prepareRecordingPath();
    }

    this._playStartSound();
    this.setState({recording: true, paused: false});
    this.props.onStart();

    try {
      const filePath = await AudioRecorder.startRecording();
      console.log(filePath);
    } catch (error) {
      console.error(error);
    }
  };

  _finishRecording = (didSucceed, filePath, fileSize) => {
    this.setState({finished: didSucceed});
    console.log(
      `Finished recording of duration ${
        this.state.currentTime
      } seconds at path: ${filePath} and size of ${fileSize || 0} bytes`,
    );
  };

  strPadLeft(string, pad, length) {
    return (new Array(length + 1).join(pad) + string).slice(-length);
  }

  render() {
    const {currentTime, recording} = this.state;
    const minutes = Math.floor(currentTime / 60);
    const seconds = currentTime - minutes * 60;
    const timeText =
      this.strPadLeft(minutes, '0', 2) + ':' + this.strPadLeft(seconds, '0', 2);
    return (
      <View style={styles.container}>
        {recording && currentTime > 0.0 && (
          <Text style={styles.progressText}>{timeText}</Text>
        )}
        {this.state.recording ? (
          <TouchableOpacity style={styles.button} onPress={this._stop}>
            <View style={styles.buttonInternal} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity style={styles.button} onPress={this._record}>
            <View style={[styles.buttonInternal, {borderRadius: 13}]} />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 5,
    left: 10,
  },
  button: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonInternal: {
    backgroundColor: 'red',
    width: 26,
    height: 26,
  },
  progressText: {
    textAlign: 'center',
    color: 'white',
  },
});

export default Recorder;
