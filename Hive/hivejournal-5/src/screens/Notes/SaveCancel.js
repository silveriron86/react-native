/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, Keyboard, Text, View, TouchableOpacity} from 'react-native';
// import Recorder from './Recorder';
import styles from './_styles';

const bookIcon = require('../../../assets/icons/book-open-1.png');

class SaveCancel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      keyboardShown: false,
    };
  }

  componentDidMount() {
    // this.keyboardDidShowListener = Keyboard.addListener(
    //   'keyboardDidShow',
    //   this._keyboardDidShow,
    // );
    // this.keyboardDidHideListener = Keyboard.addListener(
    //   'keyboardDidHide',
    //   this._keyboardDidHide,
    // );
  }

  componentWillUnmount() {
    // this.keyboardDidShowListener.remove();
    // this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({
      keyboardShown: true,
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      keyboardShown: false,
    });
  };

  render() {
    const {
      generalPreferences,
      // isRecording,
      style,
      userId,
      mainTag,
      onStart,
      onStop,
    } = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View style={{flex: 1}} />
        <TouchableOpacity
          style={styles.bookButton}
          onPress={() => {
            // Keyboard.dismiss();
            this.props.onBook();
          }}>
          <Image source={bookIcon} style={styles.bookIcon} />
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-end',
            justifyContent: 'flex-end',
          }}>
          {generalPreferences.autoSaveEnabled === false && (
            <TouchableOpacity
              style={[styles.actionBtn, styles.saveBtn]}
              onPress={this.props.onSave}>
              <Text style={[styles.actionBtnText, styles.saveBtnText]}>
                Save
              </Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity
            style={styles.actionBtn}
            onPress={this.props.onCancel}>
            <Text style={styles.actionBtnText}>Cancel</Text>
          </TouchableOpacity>
        </View>

        {/* {userId && (
          <Recorder
            userId={userId}
            mainTag={mainTag}
            onStart={onStart}
            onStop={onStop}
          />
        )} */}
      </View>
    );
  }
}

export default SaveCancel;
