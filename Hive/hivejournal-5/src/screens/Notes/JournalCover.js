/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  Animated,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Swiper from 'react-native-swiper';
import Video from 'react-native-video';
import ModalWrapper from 'react-native-modal-wrapper';
import JournalSelector from './JournalSelector';
import Colors from '../../constants/Colors';
import LoadingOverlay from '../../widgets/LoadingOverlay';
import styles from './_styles';
import Utils from '../../utils';
import EditTagType from '../../screens/Settings/Tags/EditTagType';

const coverVideos = [
  require('../../../assets/videos/clean/video.mov'),
  require('../../../assets/videos/running/video.mov'),
];

const closeIcon = require('../../../assets/icons/glyph_close_32x32.png');
const WIDTH = Dimensions.get('window').width;

export default class JournalCover extends React.Component {
  constructor(props) {
    super(props);

    this.journalCovers = [
      require('../../../assets/images/journal_cover/23941547_m.jpg'),
      require('../../../assets/images/journal_cover/15614483_m.jpg'),
      require('../../../assets/images/journal_cover/21688753_m.jpg'),
      require('../../../assets/images/journal_cover/44502796_m.jpg'),
      require('../../../assets/images/journal_cover/128994581_l.jpg'),
      require('../../../assets/images/journal_cover/24180817_l.jpg'),
      require('../../../assets/images/journal_cover/29454053_l.jpg'),
      require('../../../assets/images/journal_cover/48305579_m.jpg'),
      require('../../../assets/images/journal_cover/84449009_l.jpg'),
      require('../../../assets/images/journal_cover/89093660_l.png'),
      require('../../../assets/images/journal_cover/96818955_l.jpg'),
      require('../../../assets/images/journal_cover/97569233_l.jpg'),
      require('../../../assets/images/journal_cover/97849688_l.jpg'),
      require('../../../assets/images/journal_cover/109508040_l.jpg'),
      require('../../../assets/images/journal_cover/31720046_l.jpg'),
      require('../../../assets/images/journal_cover/46788276_m.jpg'),
      require('../../../assets/images/journal_cover/48004915_m.jpg'),
      require('../../../assets/images/journal_cover/48791833_m.jpg'),
      require('../../../assets/images/journal_cover/87176355_m.jpg'),
    ];
    this.animatedRightMargin = new Animated.Value(0);

    this.state = {
      isOpenedSelector: false,
      loading: false,
      selectedJournal: -1,
      isJournalAddModal: false,
    };
  }

  onIndexChanged = (index) => {
    this.props.onChange(this.props.tags[index].type, index);
  };

  hideJournalSelector = () => {
    const {isOpenedSelector} = this.state;

    if (isOpenedSelector) {
      this.toggleJournalSelector();
    }
  };

  showJournalSelector = () => {
    const {isOpenedSelector} = this.state;
    if (isOpenedSelector) {
      return;
    }

    this.toggleJournalSelector();
  };

  toggleJournalSelector = () => {
    const {isOpenedSelector} = this.state;
    Animated.timing(this.animatedRightMargin, {
      toValue: isOpenedSelector ? 0 : (-1 * WIDTH) / 2,
      duration: 300,
      useNativeDriver: true,
    }).start();

    console.log(global.eventEmitter);

    this.setState(
      {
        isOpenedSelector: !isOpenedSelector,
      },
      () => {
        global.eventEmitter.emit('TOGGLE_JOURNAL_SELECTOR', {
          opened: !isOpenedSelector,
        });
      },
    );
  };

  onAddJournal = () => {
    this.setState({
      selectedJournal: -1,
      isJournalAddModal: true,
    });
  };

  onEditJournal = (index) => {
    this.setState({
      selectedJournal: index,
      isJournalAddModal: true,
    });
  };

  onCloseModal = () => {
    this.setState({
      isJournalAddModal: false,
    });
  };

  render() {
    const {width, height} = Dimensions.get('window');
    const {
      isFirstVisit,
      index,
      tags,
      from,
      onPress,
      onSelectJournal,
    } = this.props;
    const {
      isOpenedSelector,
      loading,
      isJournalAddModal,
      selectedJournal,
    } = this.state;
    const vWidth = width / 2;
    const vHeight = (vWidth * 480) / 854;
    // console.log(isFirstVisit, isJournalAddModal);
    let items = [];
    if (from !== 'todo') {
      tags.forEach((item, _index) => {
        let option =
          typeof item.option === 'undefined' || item.option === null
            ? -1
            : item.option;

        items.push(
          <TouchableWithoutFeedback
            onPress={() => onPress(_index)}
            style={{flex: 1, borderWidth: 10, borderColor: 'blue'}}
            key={`journal-cover-${_index}`}>
            <View style={{flex: 1}}>
              {option >= 0 && (
                <Video
                  source={coverVideos[option]}
                  style={{
                    width: vWidth,
                    height: vHeight,
                    position: 'absolute',
                    marginTop: 100,
                    left: vWidth * 0.4,
                  }}
                  muted={true}
                  repeat={true}
                  resizeMode={'cover'}
                  rate={1.0}
                  ignoreSilentSwitch={'obey'}
                />
              )}

              <Image
                source={
                  this.journalCovers[
                    typeof item.cover === 'undefined' ? 0 : item.cover
                  ]
                }
                style={{
                  width: width,
                  height: height,
                  position: 'absolute',
                  top: 0,
                  left: 0,
                }}
              />
            </View>
          </TouchableWithoutFeedback>,
        );
      });
    }

    const editModal = (
      <ModalWrapper
        containerStyle={[
          styles.modalContainer,
          {
            marginHorizontal: 0,
            marginTop: Utils.getInsetTop(),
          },
        ]}
        style={styles.modal}
        onRequestClose={this.onCloseModal}
        shouldAnimateOnRequestClose={true}
        shouldCloseOnOverlayPress={true}
        visible={isJournalAddModal}
        position={'right'}>
        <View
          style={[
            styles.modalInner,
            {
              height: Utils.getClientHeight(),
              backgroundColor: 'white',
              borderWidth: 1,
              borderColor: 'white',
            },
          ]}>
          <EditTagType
            tags={tags}
            selected={selectedJournal}
            onClose={this.onCloseModal}
            setSettings={this.props.setSettings}
          />
        </View>
        <TouchableOpacity
          style={{position: 'absolute', top: 10, right: 10}}
          onPress={this.onCloseModal}>
          <Image source={closeIcon} style={{tintColor: '#333'}} />
        </TouchableOpacity>
      </ModalWrapper>
    );

    if (!isFirstVisit) {
      return (
        <Animated.View
          style={[
            {
              position: 'absolute',
              left: WIDTH,
              width: '50%',
              height: '100%',
              backgroundColor: 'rgba(0, 0, 0, .8)',
              transform: [
                {
                  translateX: this.animatedRightMargin,
                },
              ],
              zIndex: 9999,
              marginTop: 20,
              paddingBottom: 40,
            },
            this.props.selectorStyle,
          ]}>
          <JournalSelector
            allTags={tags}
            onSelect={onSelectJournal}
            onAdd={this.onAddJournal}
            onEdit={this.onEditJournal}
            selected={index}
            isOpened={isOpenedSelector}
            from={from}
          />
          {editModal}
        </Animated.View>
      );
    }
    return (
      <View
        style={[
          {
            flex: 1,
            position: from === 'notes' ? 'relative' : 'absolute',
            right: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0, 0, 0, .8)',
            zIndex: 9999,
          },
          this.props.style,
        ]}>
        <LoadingOverlay loading={loading} />
        {from !== 'todo' && (
          <Swiper
            containerStyle={{flex: 1}}
            index={index < 0 ? 0 : index}
            loop={false}
            activeDotColor={Colors.scheme.blue}
            showsPagination={true}
            onIndexChanged={this.onIndexChanged}>
            {items}
          </Swiper>
        )}

        <Animated.View
          style={{
            position: 'absolute',
            left: WIDTH,
            width: '50%',
            height: '100%',
            backgroundColor: 'rgba(0, 0, 0, .8)',
            transform: [
              {
                translateX: this.animatedRightMargin,
              },
            ],
          }}>
          <JournalSelector
            allTags={tags}
            onSelect={onSelectJournal}
            onAdd={this.onAddJournal}
            onEdit={this.onEditJournal}
            selected={index}
            isOpened={isOpenedSelector}
            from={from}
          />
        </Animated.View>

        {editModal}
      </View>
    );
  }
}
