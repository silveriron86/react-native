/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  ScrollView,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import styles from './_styles';
import ImageConstants from '../../constants/ImageConstants';
const LockIcon = require('../../../assets/icons/lock_b.png');

export default class ImageSelector extends React.Component {
  handleSelect = index => {
    const {tag, isModal} = this.props;
    if (isModal === true) {
      let value =
        tag && typeof tag.background !== 'undefined' ? tag.background : [];
      const found = value.indexOf(index);
      if (found >= 0) {
        value.splice(found, 1);
      } else {
        value.push(index);
      }
      console.log(value);
      this.props.onSelect(value);
    } else {
      this.props.onSelect(index);
    }
  };

  render() {
    const {tag, value, isModal, vertical} = this.props;
    let values =
      tag && typeof tag.background !== 'undefined' ? tag.background : [];
    let rows = [];
    let cols = [];

    const blackCol =
      isModal !== true ? (
        <TouchableOpacity
          onPress={() => this.handleSelect(-1)}
          key={'image-selector-black'}>
          <View style={[styles.colBkgImg, {backgroundColor: '#111'}]} />
          {(values.indexOf(-1) >= 0 || value === -1) && (
            <View style={styles.selectedBorder} />
          )}
        </TouchableOpacity>
      ) : null;

    ImageConstants.thumbs.forEach((item, index) => {
      if (index > 0 && index % 5 === 0) {
        rows.push(
          <ScrollView horizontal={true} key={`image-selector-row-${index / 5}`}>
            {index === 5 && blackCol}
            {cols}
          </ScrollView>,
        );
        cols = [];
      }

      if (index >= 10) {
        // cols.push(
        //   <TouchableWithoutFeedback
        //     disabled={true}
        //     key={`image-selector-${index}`}>
        //     <View style={styles.colBkgWrapper}>
        //       <Image source={item} style={styles.colBkgImg} />
        //       <View style={[styles.colBkgOverlay]} />
        //       <Image source={LockIcon} style={styles.lockIcon} />
        //     </View>
        //   </TouchableWithoutFeedback>,
        // );
      } else {
        cols.push(
          <TouchableOpacity
            onPress={() => this.handleSelect(index)}
            key={`image-selector-${index}`}>
            <Image source={item} style={styles.colBkgImg} />
            {(values.indexOf(index) >= 0 || value === index) && (
              <View style={styles.selectedBorder} />
            )}
          </TouchableOpacity>,
        );
      }
    });

    rows.push(
      <ScrollView horizontal={true} key={'image-selector-row-last'}>
        {cols}
      </ScrollView>,
    );

    return (
      <View
        style={[
          styles.imagesSelector,
          isModal && {height: 'auto'},
          vertical === true && {height: 230 - 98},
        ]}>
        {isModal ? (
          <View style={{height: 'auto'}}>{rows}</View>
        ) : (
          <ScrollView>{rows}</ScrollView>
        )}
      </View>
    );
  }
}
