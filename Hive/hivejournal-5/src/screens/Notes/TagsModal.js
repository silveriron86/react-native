/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Image,
} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import styles from './_styles';
import Utils from '../../utils';

const closeIcon = require('../../../assets/icons/glyph_close_32x32.png');
const bottomPadding = Utils.getInsetBottom();
const topPadding = Utils.getInsetTop();

export default class TagsModal extends React.Component {
  render() {
    const {visible, onCloseModal, onSelectTag, allTags, fontSize} = this.props;
    let categories = {};
    let rows = [];
    let cols = [];

    allTags.forEach((tag, idx) => {
      if (typeof categories[tag.category] === 'undefined') {
        categories[tag.category] = [];
      }
      if (tag.used === true) {
        categories[tag.category].push({
          tag: tag,
          index: idx,
        });
      }
    });

    Object.keys(categories).forEach((cat, idx) => {
      cols = [];
      categories[cat].forEach((r, _idx) => {
        let tags = r.tag;
        let prefix = tags.type.substring(0, tags.short.length);
        cols.push(
          <TouchableOpacity
            key={`tags-modal-cat-${idx}-${_idx}`}
            style={{paddingHorizontal: 5}}
            onPress={() => onSelectTag(r.index)}>
            <Text
              style={[
                styles.tagCatText,
                {fontWeight: '900'},
                {fontSize: fontSize},
              ]}>
              {tags.short !== prefix && `#${tags.short}/`}#{prefix}
              <Text style={{color: 'white'}}>
                {tags.type.slice(tags.short.length)}
              </Text>
            </Text>
          </TouchableOpacity>,
        );
      });
      rows.push(
        <View key={`tags-modal-row-${idx}`} style={{marginTop: 10}}>
          <Text style={[styles.tagCatText, {fontSize: fontSize}]}>
            --- {cat} ---
          </Text>
          <View style={styles.tagsCol}>{cols}</View>
        </View>,
      );
    });

    return (
      <ModalWrapper
        containerStyle={styles.modalContainer}
        style={styles.modal}
        onRequestClose={onCloseModal}
        shouldAnimateOnRequestClose={true}
        shouldCloseOnOverlayPress={true}
        visible={visible}
        position={'top'}>
        <ScrollView
          style={[
            styles.modalInner,
            {
              height:
                Dimensions.get('window').height -
                bottomPadding -
                topPadding -
                (bottomPadding > 0 ? 62 : 82),
              borderWidth: 1,
              borderColor: 'white',
            },
          ]}>
          {rows}
        </ScrollView>
        <TouchableOpacity
          style={{position: 'absolute', top: 10, right: 10}}
          onPress={onCloseModal}>
          <Image source={closeIcon} />
        </TouchableOpacity>
      </ModalWrapper>
    );
  }
}
