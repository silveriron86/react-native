/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  View,
  Text,
  ScrollView,
} from 'react-native';
import ImageConstants from '../../constants/ImageConstants';
import Colors from '../../constants/Colors';
import Utils from '../../utils';

const closeIcon = require('../../../assets/icons/glyph_close_32x32.png');
const gearIcon = require('../../../assets/icons/gear.png');

export default class JournalSelector extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {selected, isOpened, allTags, onSelect, from} = this.props;
    let categories = {};
    let rows = [];
    let cols = [];

    allTags.forEach((tag, idx) => {
      if (typeof categories[tag.category] === 'undefined') {
        categories[tag.category] = [];
      }
      if (tag.used === true) {
        categories[tag.category].push({
          tag: tag,
          index: idx,
        });
      }
    });

    Object.keys(categories).forEach((cat, idx) => {
      cols = [];
      categories[cat].forEach((r, _idx) => {
        let tags = r.tag;
        cols.push(
          <View
            key={`tags-modal-cat-${_idx}`}
            style={{
              position: 'relative',
              borderBottomWidth: selected === r.index ? 0 : 1,
              borderBottomColor: '#FFFFFF20',
            }}>
            <TouchableOpacity
              key={`tags-modal-cat-${idx}-${_idx}`}
              style={[
                styles.tagButton,
                selected === r.index && {
                  borderWidth: 2,
                  borderColor: '#29abe2',
                },
              ]}
              onPress={() => onSelect(r.index)}>
              <Image
                source={
                  ImageConstants.journalCoverThumbs[
                    typeof allTags[r.index].cover === 'undefined'
                      ? 0
                      : allTags[r.index].cover
                  ]
                }
                style={styles.thumb}
              />
              <View style={styles.tagTypeView}>
                <Text style={styles.tagText}>
                  {tags.type === '3questions' ? 'three questions' : tags.type}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.gearButton}
              onPress={() => this.props.onEdit(r.index)}>
              <Image source={gearIcon} style={styles.gearIcon} />
            </TouchableOpacity>
          </View>,
        );
      });
      rows.push(
        <View key={`tags-row-${idx}`} style={{marginTop: 10}}>
          <Text
            style={[
              styles.tagText,
              {
                color: Colors.scheme.blue,
                textAlign: 'center',
                paddingBottom: 5,
              },
            ]}>
            {cat}
          </Text>
          <View>{cols}</View>
        </View>,
      );
    });

    return (
      <View style={styles.container}>
        <ScrollView
          style={[styles.container, {paddingBottom: 20}]}
          contentContainerStyle={{paddingBottom: 40}}>
          {rows}
          {from === 'stream' && (
            <View style={{height: Utils.getInsetBottom()}} />
          )}
        </ScrollView>
        {isOpened && (
          <TouchableOpacity
            style={[styles.addButton, from === 'stream' && {bottom: 40}]}
            onPress={this.props.onAdd}>
            <Image source={closeIcon} style={styles.closeIcon} />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tagText: {
    fontFamily: 'PremiumUltra26',
    fontSize: 25,
    lineHeight: 22,
    color: 'white',
    paddingTop: 6,
  },
  tagTypeView: {
    flex: 1,
    alignItems: 'flex-start',
    padding: 7,
  },
  tagButton: {
    flexDirection: 'row',
  },
  thumb: {
    width: 90,
    height: 70,
  },
  closeIcon: {
    transform: [{rotate: '45deg'}],
    width: 40,
    height: 40,
  },
  addButton: {
    position: 'absolute',
    bottom: 0,
    left: -20,
    backgroundColor: 'rgba(0, 0, 0, .3)',
    borderRadius: 20,
  },
  gearButton: {
    position: 'absolute',
    width: 30,
    height: 30,
    right: 1,
    bottom: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  gearIcon: {
    width: 25,
    height: 25,
    tintColor: '#FFFFFF90',
  },
});
