/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import styles from './_styles';
import {ScrollView} from 'react-native-gesture-handler';
import Utils from '../../utils';

export default class FooterTags extends React.Component {
  render() {
    const {
      note,
      allTags,
      selectedIndex,
      insertTag,
      insertedTags,
      newTags,
      fontSize,
      isComposed,
      isTodo,
      vertical,
      isFullHeight,
    } = this.props;
    let cols = [];
    let data = [];
    let journalTags = [];
    if (selectedIndex >= 0) {
      data = JSON.parse(JSON.stringify(allTags[selectedIndex]));
      journalTags = data.tags;
    } else {
      allTags.forEach(j => {
        if (typeof j.tags !== 'string' && j.tags.length > 0) {
          j.tags.forEach(t => {
            if (journalTags.indexOf(t) < 0) {
              journalTags.push(t);
            }
          });
        }
      });
    }
    // let preDefinedTags = data.length;
    if (newTags.length > 0) {
      journalTags = journalTags.concat(newTags);
    }

    journalTags.forEach((t, idx) => {
      if (t.trim() !== '') {
        cols.push(
          <TouchableOpacity
            key={`footer-tags-${idx}`}
            style={{paddingHorizontal: 6}}
            onPress={() => insertTag(t)}>
            <Text
              style={[
                styles.footerTagText,
                ((note && note.search(t) > -1) ||
                  (insertedTags && insertedTags.indexOf(t) > -1)) && {
                  color: 'white',
                },
                {fontSize: fontSize},
                isTodo === true && {fontFamily: 'PremiumUltra5'},
              ]}>
              #{t}
            </Text>
          </TouchableOpacity>,
        );
      }
    });

    if (isTodo === true) {
      return (
        <View style={{width: '100%'}}>
          <View style={styles.tagsCol}>{cols}</View>
        </View>
      );
    }

    if (vertical === true) {
      return (
        <ScrollView
          style={[
            styles.footerTags,
            isComposed && {backgroundColor: 'transparent'},
            {
              marginTop: 40,
              maxHeight: Utils.getClientHeight() - 140,
              borderRadius: 5,
            },
          ]}
          contentContainerStyle={{paddingVertical: 10}}>
          <View style={{flexDirection: 'column', height: '100%'}}>{cols}</View>
        </ScrollView>
      );
    }

    return (
      <ScrollView
        style={[
          styles.footerTags,
          isComposed && {backgroundColor: 'transparent'},
          isFullHeight && {maxHeight: '100%'},
        ]}>
        <View style={[styles.tagsCol]}>{cols}</View>
      </ScrollView>
    );
  }
}
