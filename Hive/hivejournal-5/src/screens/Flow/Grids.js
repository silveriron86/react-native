/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, TouchableOpacity, Text, Dimensions} from 'react-native';
import styles from './_styles';
import Utils from '../../utils';

const {width} = Dimensions.get('window');
const SCALES = [];
for (let i = 10; i >= 0; i -= 1) {
  SCALES.push(i);
}

export default class Grids extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      satisfactionPos: [],
      energyPos: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.tags[5] !== nextProps.tags[5]) {
      this._updateTags();
    }
  }

  _getXY = (arrPos, position) => {
    let i = 0;
    let x = 0;
    let y = 0;
    let ret = false;
    do {
      let from = 10 - position.max;
      let to = 10 - position.min;

      x = Utils.getRandomInt(from, to - 1);
      y = Utils.getRandomInt(0, 2);

      let isExist = arrPos.filter(pos => pos.x === x && pos.y === y);
      ret = isExist.length === 0;
      i++;
    } while (ret === false || i < 10);
    return {x, y};
  };

  _updateTags = () => {
    const {tags, satisfaction, energy, positions} = this.props.tags[5];
    let satisfactionPos = [];
    let energyPos = [];

    if (satisfaction.length > 0) {
      satisfaction.forEach((item, i) => {
        let index = tags.indexOf(item);
        satisfactionPos.push(this._getXY(satisfactionPos, positions[index]));
      });
    }

    if (energy.length > 0) {
      energy.forEach((item, i) => {
        let index = tags.indexOf(item);
        energyPos.push(this._getXY(energyPos, positions[index]));
      });
    }

    this.setState({
      satisfactionPos: satisfactionPos,
      energyPos: energyPos,
    });
  };

  render() {
    const {satisfaction, energy} = this.props.tags[5];
    const {visibleLeftTags, visibleRightTags, selected} = this.props;
    const {satisfactionPos, energyPos} = this.state;
    const scaleHeight = this.props.height / 10;
    const scaleWidth = (width / 2 - 60) / 3;

    let leftTags = [];
    let rightTags = [];
    if (satisfactionPos.length > 0) {
      satisfactionPos.forEach((item, i) => {
        leftTags.push(
          <TouchableOpacity
            key={`satisfaction-tag-${i}`}
            style={[
              styles.tagBtn,
              {
                top:
                  item.x * scaleHeight +
                  ((item.y % 3 === 1 ? 1 : 0) * scaleHeight) / 2,
                left: width / 2 + 30 + item.y * scaleWidth,
              },
            ]}
            onPress={() => this.props.onSelectTag(satisfaction[i])}>
            <Text
              style={[
                styles.tagText,
                selected.indexOf(satisfaction[i]) > -1 && {color: '#30cff2'},
              ]}>
              #{satisfaction[i]}
            </Text>
          </TouchableOpacity>,
        );
      });
    }
    if (energyPos.length > 0) {
      energyPos.forEach((item, i) => {
        rightTags.push(
          <TouchableOpacity
            key={`energy-tag-${i}`}
            style={[
              styles.tagBtn,
              {
                top:
                  item.x * scaleHeight +
                  ((item.y % 3 === 1 ? 1 : 0) * scaleHeight) / 2,
                left: 10 + item.y * scaleWidth,
              },
            ]}
            onPress={() => this.props.onSelectTag(energy[i])}>
            <Text
              style={[
                styles.tagText,
                selected.indexOf(energy[i]) > -1 && {color: '#f29c55'},
              ]}>
              #{energy[i]}
            </Text>
          </TouchableOpacity>,
        );
      });
    }

    return (
      <>
        {visibleLeftTags === true && (
          <>
            {/* <View
              style={[
                styles.gridBG,
                {flexDirection: 'row', marginLeft: 10, width: width / 2 - 60},
              ]}>
              {[...Array(3)].map((col, idx) => {
                return (
                  <View
                    key={`grid-col-${idx}`}
                    style={[styles.gridCol, idx === 2 && {borderRightWidth: 1}]}
                  />
                );
              })}
            </> */}
            <View style={styles.gridBG}>
              {SCALES.map((v, i) => {
                return (
                  <View
                    style={
                      v > 0 && {flex: 1, position: 'relative', marginLeft: 10}
                    }
                    key={`scale-row-left-${i}`}>
                    <View
                      style={[
                        styles.scale,
                        {
                          borderColor: '#888',
                          // width: width / 2 - 40,
                          width: 0,
                        },
                      ]}
                    />
                  </View>
                );
              })}
            </View>
            <View style={styles.gridBG}>{rightTags}</View>
          </>
        )}

        {visibleRightTags === true && (
          <>
            {/* <View
              style={[
                styles.gridBG,
                {
                  flexDirection: 'row',
                  width: width / 2 - 40,
                  left: width / 2 + 30,
                },
              ]}>
              {[...Array(3)].map((col, idx) => {
                return (
                  <View
                    key={`grid-col-${idx}`}
                    style={[styles.gridCol, idx === 2 && {borderRightWidth: 1}]}
                  />
                );
              })}
            </View> */}

            <View style={[styles.gridBG, {left: width / 2 + 30}]}>
              {SCALES.map((v, i) => {
                return (
                  <View
                    style={v > 0 && {flex: 1, position: 'relative'}}
                    key={`scale-row-right-${i}`}>
                    <View
                      style={[
                        styles.scale,
                        {
                          borderColor: '#888',
                          // width: width / 2 - 40,
                          width: 0,
                        },
                      ]}
                    />
                  </View>
                );
              })}
            </View>
            <View style={styles.gridBG}>{leftTags}</View>
          </>
        )}
      </>
    );
  }
}
