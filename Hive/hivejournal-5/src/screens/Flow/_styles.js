import {StyleSheet, Dimensions} from 'react-native';
import Colors from '../../constants/Colors';
import Utils from '../../utils';

const {width} = Dimensions.get('window');
const mainAreaHeight = Utils.getClientHeight() - 100 - 60 - 58;

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.scheme.black,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Colors.scheme.gray,
  },
  padArea: {
    height: 20,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
  },
  sliderWrapper: {
    width: '100%',
    position: 'relative',
    marginTop: 32,
  },
  scaleBar: {
    position: 'absolute',
    width: 0,
    alignSelf: 'center',
    height: '100%',
    backgroundColor: '#ccc',
  },
  saveBtn: {
    position: 'relative',
    // bottom: -40,
    // marginTop:
    backgroundColor: Colors.scheme.blue,
    height: 25,
    paddingHorizontal: 20,
    justifyContent: 'center',
    borderRadius: 10,
    alignSelf: 'center',
    opacity: 0.9,
  },
  btnText: {
    color: 'white',
    fontFamily: 'PremiumUltra5',
    fontSize: 15,
  },
  scaleWrapper: {
    position: 'absolute',
    alignSelf: 'center',
  },
  scale: {
    alignSelf: 'center',
    width: 20,
    height: 1,
    flex: 1,
    borderTopWidth: 1,
    borderColor: '#ccc',
  },
  scaleText: {
    color: '#999',
    fontSize: 16,
  },
  labels: {
    position: 'absolute',
    width: '100%',
    flexDirection: 'row',
    marginTop: -25,
  },
  label: {
    textAlign: 'center',
    color: '#8e8e8e',
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'PremiumUltra5',
  },
  hexIcon: {
    width: '100%',
    height: 80,
    resizeMode: 'contain',
  },
  pointerLine: {
    width: '100%',
    height: 1,
    position: 'absolute',
    flexDirection: 'row',
  },
  descriptionModal: {
    position: 'absolute',
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: width / 2 - 40,
    height: mainAreaHeight,
    backgroundColor: 'rgba(255, 255, 255, 0.96)',
    marginTop: -mainAreaHeight - 15,
    // marginTop: -15,
    borderRadius: 5,
  },
  descriptionText: {
    padding: 15,
    fontFamily: 'PremiumUltra5',
    color: Colors.scheme.blue,
    fontSize: 18,
  },
  gridBG: {
    position: 'absolute',
    height: mainAreaHeight + 20,
  },
  tagBtn: {
    position: 'absolute',
    width: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
    // paddingHorizontal: 5,
  },
  gridCol: {
    flex: 1,
    borderLeftWidth: 1,
    borderColor: '#efefef',
  },
  tagText: {
    fontFamily: 'PremiumUltra26',
    fontSize: 20,
    color: '#eee',
  },
  tagToggleText: {
    color: '#efefef',
    fontFamily: 'PremiumUltra5',
    fontSize: 18,
  },
  tagToggleTouchableOpacity: {
    position: 'absolute',
    marginTop: 5,
    padding: 10,
  },
  textInputLabel: {
    paddingBottom: 7,
    fontFamily: 'PremiumUltra5',
    color: '#ffffff',
    fontSize: 28,
  },
  textInput: {
    fontFamily: 'PremiumUltra26',
    fontSize: 18,
    color: '#efefef',
    marginTop: -15,
    paddingHorizontal: 5,
    borderWidth: 1,
    borderColor: '#999',
    width: width - 20,
    height: 55,
    margin: 5,
    borderRadius: 10,
  },
});
