/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  TextInput,
} from 'react-native';
import DraggableFlatList from '../../widgets/DraggableFlatList';
import SettingConstants from '../../constants/SettingConstants';
// import Spotify from 'rn-spotify-sdk'
import axios from 'axios';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import {SettingActions, MongoActions, CommonActions} from '../../actions';
import ApiConstants from '../../constants/ApiConstants';
import LoadingOverlay from '../../widgets/LoadingOverlay';
import Fade from '../../widgets/Fade';
import Utils from '../../utils';
import Locker from '../../widgets/Locker';
import Login from '../../widgets/Login';
import EmptyView from '../../widgets/EmptyView';
import FooterTrigger from '../../widgets/FooterTrigger';
import Grids from './Grids';
import styles from './_styles';
import Colors from '../../constants/Colors';
import * as RootNavigation from '../../navigation/RootNavigation';

const hex1 = require('../../../assets/icons/blue_hex.png');
const hex2 = require('../../../assets/icons/orange_hex.png');
const mainAreaHeight = Utils.getClientHeight() - 100 - 60 - 58;
const scaleHeight = mainAreaHeight / 20;

const SCALES = [];
for (let i = 10; i >= 0; i -= 0.5) {
  SCALES.push(i);
}

class FlowScreen extends React.Component {
  constructor(props) {
    super(props);

    let data1 = [...Array(21)].map((d, index) => ({
      key: `item1-${index}`,
      label: index,
    }));
    let data2 = [...Array(21)].map((d, index) => ({
      key: `item2-${index}`,
      label: index,
    }));

    this.state = {
      isOpenedEnergyText: false,
      isOpenedHopeText: false,
      hope: props.hope,
      energy: props.energy,
      loading: false,
      data1: Utils.moveInArray(data1, 0, SCALES.indexOf(props.hope)),
      data2: Utils.moveInArray(data2, 0, SCALES.indexOf(props.energy)),
      visibleLeftTags: false,
      visibleRightTags: false,
      selectedTags: [],
      noteText: '',
      placeholderText: '',
      isEdited: false,
    };
  }

  componentDidMount() {
    setInterval(() => {
      this._setFlowBorder();
    }, 1000 * 60 * 10); // per 10 mins

    setTimeout(() => {
      this._setFlowBorder();
    }, 1000);

    global.eventEmitter.addListener('TOGGLE_FLOW_JOURNEY', () => {
      this._setFlowBorder();
    });

    this._updateNoteText();
  }

  _updateNoteText = () => {
    const {isEdited} = this.state;
    if (isEdited === false) {
      this.setState({
        placeholderText:
          'ex: ' +
          this._getDescriptionText('hope') +
          ' ' +
          this._getDescriptionText('energy'),
      });
    }
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.accessToken !== nextProps.accessToken) {
      if (nextProps.accessToken) {
        this._setFlowBorder();
      }
    }
  }

  _setFlowBorder = () => {
    RootNavigation.setParams({
      border: Utils.getFlowBorder(this.props.flowJourney),
    });
  };

  toggleTags = (side) => {
    if (side === 'left') {
      this.setState({
        visibleLeftTags: !this.state.visibleLeftTags,
      });
    } else {
      this.setState({
        visibleRightTags: !this.state.visibleRightTags,
      });
    }
  };

  renderItem = ({item, index, move, moveEnd, isActive}, side) => {
    const {visibleLeftTags, visibleRightTags} = this.state;

    if (item.label === 0) {
      return (
        <TouchableOpacity
          style={{
            height: scaleHeight,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPressIn={move}
          onPressOut={moveEnd}>
          {side === 'left' ? (
            visibleLeftTags === false ? (
              <>
                <View style={styles.pointerLine}>
                  <View style={{flex: 1}} />
                  <View
                    style={{flex: 1, marginRight: 30, backgroundColor: '#ccc'}}
                  />
                </View>
                <View
                  style={{
                    position: 'relative',
                    width: '100%',
                  }}>
                  <Image
                    source={hex1}
                    style={[styles.hexIcon, {marginLeft: -30}]}
                    resizeMethod="resize"
                  />
                  <TouchableOpacity
                    style={[styles.tagToggleTouchableOpacity, {left: 30}]}
                    onPress={() => this.toggleTags('right')}>
                    <Text style={styles.tagToggleText}>#tags</Text>
                  </TouchableOpacity>
                </View>
              </>
            ) : null
          ) : visibleRightTags === false ? (
            <>
              <View style={styles.pointerLine}>
                <View
                  style={{flex: 1, marginLeft: 30, backgroundColor: '#ccc'}}
                />
                <View style={{flex: 1}} />
              </View>
              <View style={{position: 'relative', width: '100%'}}>
                <Image
                  source={hex2}
                  style={[styles.hexIcon, {marginLeft: 30}]}
                  resizeMethod="resize"
                />
                <TouchableOpacity
                  style={[styles.tagToggleTouchableOpacity, {right: 35}]}
                  onPress={() => this.toggleTags('left')}>
                  <Text style={styles.tagToggleText}>#tags</Text>
                </TouchableOpacity>
              </View>
            </>
          ) : null}
        </TouchableOpacity>
      );
    }

    return <View style={{height: scaleHeight}} />;
  };

  _getDescriptionText = (type) => {
    const {data1, data2} = this.state;
    const {flowDescriptionPack} = this.props;
    let val = 0;
    let data = type === 'hope' ? data1 : data2;
    for (let i = 0; i < data.length; i++) {
      if (data[i].label === 0) {
        val = SCALES[i];
      }
    }
    let ret = '';
    SettingConstants.FLOW_DESCRIPTION_PACKS[flowDescriptionPack][type].forEach(
      (item, i) => {
        if (val >= item.ratingMin && val <= item.ratingMax) {
          ret = item.descriptionText;
        }
      },
    );
    return ret;
  };

  saveLevels = () => {
    const {data1, data2, selectedTags, noteText, placeholderText} = this.state;
    const {toneEnabled, noteVisibility} = this.props;

    let hope = 0;
    let energy = 0;
    for (let i = 0; i < data1.length; i++) {
      if (data1[i].label === 0) {
        hope = SCALES[i];
      }
      if (data2[i].label === 0) {
        energy = SCALES[i];
      }
    }

    this.setState(
      {
        hope,
        energy,
        isOpenedEnergyText: false,
        isOpenedHopeText: false,
        isEdited: false,
      },
      () => {
        this.props.setSettings({type: 'HOPE', value: hope});
        this.props.setSettings({type: 'ENERGY', value: energy});

        let origText = noteText ? noteText : placeholderText;
        let text = origText;
        if (selectedTags.length > 0) {
          selectedTags.forEach((t) => {
            origText += ` #${t}`;
          });
        }
        let tags = Utils.getTags(origText);
        if (tags) {
          tags.forEach((tag, i) => {
            tags[i] = tag.substr(1);
            text = text.replace(tag + ' ', '');
          });
          text = text.replace('#' + tags[tags.length - 1], '');
        }

        let note = {
          type: 'rating',
          energy: energy,
          hope: hope,
          note_orig: Utils.encrypt(origText),
          note_text: Utils.encrypt(text),
          note_tags: selectedTags,
          creation_timestamp: moment().tz(this.props.location).format(),
          entry_ip: '0.0.0.0',
          entry_app_type: 'mobile',
          entry_app_version: '1.0',
          word_count: Utils.getWords(origText),
          font: this.props.streamFont,
          font_size: this.props.fontSize,
          permissions: {
            see: noteVisibility.rating[0],
            read: noteVisibility.rating[1],
          },
          screen_name: this.props.screenName,
          main_tag: 'flow',
          color: Colors.scheme.blue,
        };

        if (!noteText) {
          note.noteTextIsSuggestedOnly = true;
        }
        console.log(noteText);

        if (toneEnabled === true) {
          let apiData = ApiConstants.getToneAnalyzerApi();
          this.setState(
            {
              loading: true,
            },
            () => {
              axios({
                method: 'GET',
                url: `${apiData.url}/v3/tone?version=${
                  apiData.version
                }&text=${encodeURIComponent(origText)}`,
                headers: {
                  Authorization: 'Basic ' + Utils.btoa('apiKey:' + apiData.key),
                },
              })
                .then((response) => {
                  if (response.status === 200) {
                    note.ibm_tone = {
                      document_tone: response.data.document_tone,
                    };
                    this._procSpotifySaveNote(note);
                  } else {
                    this.setState(
                      {
                        loading: false,
                      },
                      () => {
                        Utils.saveFailedQueue('Tone analyzer service', note);
                      },
                    );
                  }
                })
                .catch((error) => {
                  this.setState(
                    {
                      loading: false,
                    },
                    () => {
                      Utils.saveFailedQueue('Tone analyzer service', note);
                    },
                  );
                });
            },
          );
        } else {
          //disabled tone setting
          this._procSpotifySaveNote(note);
        }
      },
    );
  };

  // -------- save note --------
  _saveNote = (note) => {
    Utils.trackEvent('Action', 'User saves a Flow note');

    AsyncStorage.getItem('USER_ID', (_err, user_id) => {
      let arg = JSON.parse(JSON.stringify(note));
      arg.user_id = user_id;
      this.setState(
        {
          loading: true,
        },
        () => {
          this.props.createNote({
            data: arg,
            cb: (res) => {
              this.props.setLastNote({
                value: arg,
                id: res.insertedId,
              });

              AsyncStorage.getItem(
                'IBM_PERSONALITY',
                (__err, ibm_personality) => {
                  console.log(ibm_personality);
                  let user = {
                    auth_provider: 'auth0',
                    user_id_auth: user_id,
                  };
                  if (ibm_personality) {
                    user.ibm_personality = JSON.parse(ibm_personality);
                  }
                  let data = {
                    user: user,
                  };

                  console.log('*** Saved ***');
                  console.log(data);
                  this.setState(
                    {
                      loading: false,
                    },
                    () => {
                      global.eventEmitter.emit('NOTE_CREATED', {});
                      this.props.navigation.navigate('FeedStack');

                      if (res.error) {
                        Utils.saveFailedQueue('Note creation', note);
                      } else {
                        // Will be used in flow journey setting
                        let saved = Utils.nowUTC(this.props);
                        AsyncStorage.setItem('LAST_FLOW_ENTRY_SAVED', saved);
                        let flowJourney = JSON.parse(
                          JSON.stringify(this.props.flowJourney),
                        );
                        flowJourney.timeSinceLastFlowEntry = saved;
                        this.props.setSettings({
                          type: 'FLOW_JOURNEY',
                          value: flowJourney,
                        });

                        setTimeout(() => {
                          global.eventEmitter.emit('TOGGLE_FLOW_JOURNEY', {});
                        }, 500);
                        global.eventEmitter.emit('SAVED_FLOW', {});
                      }
                    },
                  );
                },
              );
            },
          });
        },
      );
    });
  };

  _getRecentlyPlayed = (session, note) => {
    /*
    this.setState({
      loading: true
    }, () => {
      axios({
        method: 'GET',
        url: `${ApiConstants.getSpotifyApi().url}/me/player/recently-played`,
        headers: {
          Authorization: "Bearer " + session.accessToken
        }
      }).then(response => {
        // last 5 songs played
        let played = response.data.items;
        if(played.length > 5) {
          played = played.slice(0, 5);
        }
        note['spotify_history'] = played;
        this._saveNote(note)
      })
      .catch(error => {
        this.setState({
          loading: false
        },() => {
          alert(error.message);
        })
      })
    })*/
  };

  onCheckSpotify = (note) => {
    /*
    let session = Spotify.getSession();
    if(session === null) {
      // log into Spotify
      Spotify.login().then((loggedIn) => {
        if(loggedIn) {
          // logged in
          session = Spotify.getSession();
          this._getRecentlyPlayed(session, note);
        }else {
          // cancelled
          this._saveNote(note)
        }
      }).catch((error) => {
        this.setState({
          loading: false
        },() => {
          alert(error.message);
        })
      });
    }else {
      this._getRecentlyPlayed(session, note);
    }*/
  };

  _procSpotifySaveNote = (note) => {
    /*if(this.props.spotifyEnabled === true) {
      this.onCheckSpotify(note);
    }else {*/
    this._saveNote(note);
    // }
  };
  // ----------------------------

  onSelectTag = (tag) => {
    let selectedTags = JSON.parse(JSON.stringify(this.state.selectedTags));
    let found = selectedTags.indexOf(tag);
    if (found > -1) {
      selectedTags.splice(found, 1);
    } else {
      selectedTags.push(tag);
    }
    this.setState({
      selectedTags: selectedTags,
    });
  };

  render() {
    const {
      data1,
      data2,
      isOpenedEnergyText,
      isOpenedHopeText,
      loading,
      visibleLeftTags,
      visibleRightTags,
      selectedTags,
      noteText,
      placeholderText,
    } = this.state;
    const {flowLabels, accessToken, tags} = this.props;
    if (accessToken === 'none') {
      return <EmptyView />;
    }

    if (accessToken === null) {
      return <Login />;
    }

    let visibleFooter = RootNavigation.getVisibleFooter();
    let scaleRows = [];
    SCALES.forEach((v, i) => {
      let isInt = v - parseInt(v, 10) === 0;
      scaleRows.push(
        <View
          style={v > 0 && {flex: 1, position: 'relative'}}
          key={`scale-row-${i}`}>
          <View style={[styles.scale, !isInt && {width: 10}]} />
          <View style={styles.scaleWrapper}>
            <Text
              style={[
                styles.scaleText,
                {marginTop: -12},
                {marginLeft: v % 2 === 1 ? -25 : 40},
                i === 0 && {marginTop: -25, marginLeft: 0},
              ]}>
              {isInt ? v : ''}
            </Text>
          </View>
        </View>,
      );
    });

    return (
      <Locker {...this.props}>
        <SafeAreaView style={[styles.screen]}>
          <View style={[styles.container]}>
            <LoadingOverlay loading={loading} />
            <View style={styles.padArea} />

            <View
              style={{alignSelf: 'flex-start', marginLeft: 20, marginTop: -20}}>
              <Text style={styles.textInputLabel}>Flow Log...</Text>
            </View>

            <TextInput
              style={styles.textInput}
              multiline={true}
              value={noteText}
              placeholder={placeholderText}
              placeholderTextColor="#efefef"
              onChangeText={(text) =>
                this.setState({isEdited: true, noteText: text})
              }
            />

            <View style={{zIndex: 9, marginTop: -15}}>
              <TouchableOpacity
                style={styles.saveBtn}
                onPress={this.saveLevels}>
                <Text style={styles.btnText}>Save Current Levels</Text>
              </TouchableOpacity>
            </View>

            <View style={[styles.sliderWrapper, {height: mainAreaHeight}]}>
              <View style={[styles.labels, {marginTop: -25}]}>
                <View style={{flex: 1}}>
                  <Text style={[styles.label, {color: '#00c8ed'}]}>
                    {flowLabels.hope.toUpperCase()}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <Text style={[styles.label, {color: '#fc9254'}]}>
                    {flowLabels.energy.toUpperCase()}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  height: mainAreaHeight,
                  position: 'relative',
                  marginTop: 0,
                }}>
                {scaleRows}
              </View>
              <View style={styles.scaleBar} />

              <View
                style={{flexDirection: 'row', flex: 1, position: 'absolute'}}>
                <View
                  style={{
                    flex: 1,
                    height: mainAreaHeight + scaleHeight,
                    marginTop: -scaleHeight / 2,
                  }}>
                  <DraggableFlatList
                    data={data1}
                    renderItem={(v) => this.renderItem(v, 'left')}
                    keyExtractor={(item, index) => `draggable-item-${item.key}`}
                    scrollPercent={1}
                    onMoveEnd={({data}) => {
                      this.setState({data1: data}, () => {
                        this._updateNoteText();
                      });
                    }}
                  />

                  <Fade visible={isOpenedEnergyText}>
                    <View style={[styles.descriptionModal, {marginLeft: 10}]}>
                      <Text style={styles.descriptionText}>
                        {this._getDescriptionText('energy')}
                      </Text>
                    </View>
                  </Fade>
                </View>
                <View
                  style={{
                    flex: 1,
                    position: 'relative',
                    height: mainAreaHeight + scaleHeight,
                    marginTop: -scaleHeight / 2,
                  }}>
                  <DraggableFlatList
                    data={data2}
                    renderItem={(v) => this.renderItem(v, 'right')}
                    keyExtractor={(item, index) => `draggable-item-${item.key}`}
                    scrollPercent={1}
                    onMoveEnd={({data}) => {
                      this.setState({data2: data}, () => {
                        this._updateNoteText();
                      });
                    }}
                  />

                  <Fade visible={isOpenedHopeText}>
                    <View style={[styles.descriptionModal, {marginLeft: 30}]}>
                      <Text style={styles.descriptionText}>
                        {this._getDescriptionText('hope')}
                      </Text>
                    </View>
                  </Fade>
                </View>
              </View>

              <Grids
                tags={tags}
                selected={selectedTags}
                height={mainAreaHeight}
                visibleLeftTags={visibleLeftTags}
                visibleRightTags={visibleRightTags}
                onSelectTag={this.onSelectTag}
              />
            </View>

            {/* <View style={{zIndex: 9}}>
              <TouchableOpacity
                style={styles.saveBtn}
                onPress={this.saveLevels}>
                <Text style={styles.btnText}>Save Current Levels</Text>
              </TouchableOpacity>
            </View> */}

            <View
              style={{
                flex: 1,
                width: '100%',
                justifyContent: 'flex-end',
                zIndex: visibleFooter === true ? 10 : 8,
              }}>
              {/* {visibleFooter === false && <View style={{height: 48}} />} */}
              <FooterTrigger
                screen="flow"
                flowJourney={this.props.flowJourney}
              />
            </View>
          </View>
        </SafeAreaView>
      </Locker>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tags: state.SettingReducer.tags,
    screenName: state.SettingReducer.screenName,
    flowJourney: state.SettingReducer.flowJourney,
    toneEnabled: state.SettingReducer.toneEnabled,
    spotifyEnabled: state.SettingReducer.spotifyEnabled,
    hope: state.SettingReducer.hope,
    energy: state.SettingReducer.energy,
    flowLabels: state.SettingReducer.flowLabels,
    flowDescriptionPack: state.SettingReducer.flowDescriptionPack,
    streamFont: state.SettingReducer.streamFont,
    fontSize: state.SettingReducer.fontSize,
    noteVisibility: state.SettingReducer.noteVisibility,
    location: state.SettingReducer.location,
    accessToken: state.CommonReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLastNote: (req) =>
      dispatch(CommonActions.setLastNote(req.value, req.id)),
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
    createNote: (req) => dispatch(MongoActions.createNote(req.data, req.cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlowScreen);
