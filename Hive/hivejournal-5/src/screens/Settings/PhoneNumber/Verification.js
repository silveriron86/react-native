/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import FlashMessage, {showMessage} from 'react-native-flash-message';
import Utils from '../../../utils';
import {styles} from '../_styles';

export default class Verification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      failedAttempts: 0,
      isPressedVerify: false,
      code: '',
    };
  }

  onVerify = () => {
    const {isPressedVerify, failedAttempts} = this.state;
    if (isPressedVerify || failedAttempts >= 3) {
      return;
    }

    this.setState({
      isPressedVerify: true,
    });

    this.props.verify({
      cb: res => {
        if (res && this.props.accessToken === res.user.verifyTokenString) {
          let user = JSON.parse(JSON.stringify(this.props.user));
          user.verifyMe = false;
          user.validatedPhone = true;
          this.props.setSettings({type: 'USER', value: user});
          Utils.trackEvent('Action', 'settings:mobile SMS:accepted terms');
          showMessage({
            message: 'Phone Number Validated',
            type: 'success',
          });
          setTimeout(() => {
            this.props.modalClose();
          }, 1000);
        } else {
          this.setState({
            failedAttempts: failedAttempts + 1,
          });
          if (failedAttempts < 2) {
            this.setState({
              isPressedVerify: false,
            });
          }
          showMessage({
            message: 'Incorrect Code',
            type: 'error',
          });
        }
      },
    });
  };

  render() {
    const {code, isPressedVerify, failedAttempts} = this.state;
    const {triggerSendVerificationCode} = this.props;
    return (
      <>
        {failedAttempts < 3 && (
          <>
            <Text style={styles.modalSubTitle}>
              Please check your phone and enter the verification code
            </Text>
            <TextInputMask
              type={'custom'}
              options={{
                mask: '999-999',
              }}
              value={code}
              onChangeText={text => {
                this.setState({
                  code: text,
                });
              }}
              placeholder="XXX-XXX"
              style={styles.pnMask}
            />
            <TouchableOpacity
              style={{marginTop: 10}}
              onPress={triggerSendVerificationCode}>
              <Text style={styles.termsText}>Send New Code</Text>
            </TouchableOpacity>
          </>
        )}

        {code.length === 7 && (
          <TouchableOpacity
            onPress={this.onVerify}
            activeOpacity={isPressedVerify || failedAttempts >= 3 ? 1 : 0.5}
            style={[
              styles.analyzeNowBtn,
              (isPressedVerify || failedAttempts >= 3) && {
                backgroundColor: '#aaa',
              },
            ]}>
            <Text style={styles.analyzeNowText}>verify</Text>
          </TouchableOpacity>
        )}

        <FlashMessage position="center" />
      </>
    );
  }
}
