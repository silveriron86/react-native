/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import {styles} from '../_styles';
const closeIcon = require('../../../../assets/icons/glyph_close_32x32.png');

export default class TermsModal extends React.Component {
  render() {
    const {visible, onClose} = this.props;

    return (
      <ModalWrapper
        containerStyle={styles.modalContainer}
        style={styles.modal}
        onRequestClose={onClose}
        shouldAnimateOnRequestClose={true}
        shouldCloseOnOverlayPress={true}
        visible={visible}
        position={'center'}>
        <View style={{padding: 20, paddingTop: 10}}>
          <Text style={styles.modalSubTitle}>Terms and Conditions</Text>
          <Text style={{marginTop: 5}}>
            Detailed content coming soon from SJ, but some general topics:
          </Text>
          <View style={styles.termsTextRow}>
            <Text>-- </Text>
            <Text>
              how this will be used (only for joined journeys and other
              user-approved scenarios),
            </Text>
          </View>
          <View style={styles.termsTextRow}>
            <Text>-- </Text>
            <Text>
              STOP will stop for each scenario, and STOP ALL will disable all
              "system-initiated messages"
            </Text>
          </View>
          <View style={styles.termsTextRow}>
            <Text>-- </Text>
            <Text>
              but can still receive and process incoming SMS (user --> system),
            </Text>
          </View>
          <View style={styles.termsTextRow}>
            <Text>-- </Text>
            <Text>standard text message charges apply, privacy terms</Text>
          </View>
          <TouchableOpacity onPress={onClose} style={styles.modalCloseButton}>
            <Image source={closeIcon} style={styles.closeIcon} />
          </TouchableOpacity>
        </View>
      </ModalWrapper>
    );
  }
}
