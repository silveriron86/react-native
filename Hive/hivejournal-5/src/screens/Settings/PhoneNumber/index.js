/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import {TextInputMask} from 'react-native-masked-text';
import {CheckBox} from 'react-native-elements';
import FlashMessage, {showMessage} from 'react-native-flash-message';
import Utils from '../../../utils';
import {styles} from '../_styles';
import TermsModal from './TermsModal';
import Verification from './Verification';
import Colors from '../../../constants/Colors';

const closeIcon = require('../../../../assets/icons/glyph_close_32x32.png');

class TermsCheckBox extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        {this.props.children}
      </TouchableOpacity>
    );
  }
}

export default class PhoneNumber extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleDatePicker: false,
      digits: '',
      isCheckedTerms: false,
      isVisibleTermsModal: false,
      isPressedAccept: false,
    };
  }

  showDatePicker = () => {
    console.log('here');
    this.setState({
      visibleDatePicker: true,
    });
  };

  onCancelDatPicker = () => {
    this.setState({
      visibleDatePicker: false,
    });
    Utils.trackEvent(
      'Action',
      'settings:mobile SMS:canceled:declined to enter b-day',
    );
  };

  onDateChange = date => {
    this.setState({
      visibleDatePicker: false,
    });

    let dt = moment(date);
    let age = moment().diff(dt, 'years');
    let user = JSON.parse(JSON.stringify(this.props.user));
    user.birthdate = dt.format('YYYY-MM-DD');
    user.over13 = age >= 13;
    this.props.setSettings({type: 'USER', value: user});
    Utils.trackEvent('Action', 'settings:mobile SMS:saved b-day');
  };

  onOK = () => {
    Utils.trackEvent('Action', 'settings:mobile SMS:canceled:was under 13');
    this.props.onClose();
  };

  toggleCheckBox = () => {
    this.setState({
      isCheckedTerms: !this.state.isCheckedTerms,
    });
  };

  toggleTermsModal = () => {
    if (this.state.isVisibleTermsModal === false) {
      Utils.trackEvent('Action', 'settings:mobile SMS:viewed terms');
    }
    this.setState({
      isVisibleTermsModal: !this.state.isVisibleTermsModal,
    });
  };

  triggerSendVerificationCode = () => {
    console.log('*** triggerSendVerificationCode');
  };

  onAccept = () => {
    if (this.state.isPressedAccept) {
      return;
    }

    let user = JSON.parse(JSON.stringify(this.props.user));
    user.phone = this.state.digits;
    user.verifyMe = true;
    user.verifyTokenSent = false;
    user.validatedPhone = true;
    user.acceptedTermsSMS = true;
    console.log(user);
    this.props.setSettings({type: 'USER', value: user});
    this.triggerSendVerificationCode();
    Utils.trackEvent('Action', 'settings:mobile SMS:accepted terms');
    this.setState({
      isPressedAccept: true,
    });
    showMessage({
      message: 'Phone Number Updated',
      type: 'success',
    });
  };

  render() {
    const {visible, onClose, user} = this.props;
    const {
      visibleDatePicker,
      digits,
      isCheckedTerms,
      isVisibleTermsModal,
      isPressedAccept,
    } = this.state;

    let min, max, date;
    let age = 0;
    if (user.birthdate) {
      age = moment().diff(user.birthdate, 'years');
    } else {
      min = moment()
        .subtract(100, 'years')
        .toDate();
      max = moment()
        .subtract(12, 'years')
        .toDate();
      date = moment()
        .subtract(25, 'years')
        .set({month: 0, date: 1})
        .toDate();
    }

    return (
      <ModalWrapper
        containerStyle={styles.modalContainer}
        style={styles.modal}
        onRequestClose={onClose}
        shouldAnimateOnRequestClose={true}
        shouldCloseOnOverlayPress={true}
        visible={visible}
        position={'right'}>
        <View style={styles.pnModal}>
          <Text style={styles.pnModalTitle}>Hive SMS</Text>
          <TouchableOpacity onPress={onClose} style={styles.modalCloseButton}>
            <Image source={closeIcon} style={styles.closeIcon} />
          </TouchableOpacity>
          {!user.birthdate ? (
            <>
              <Text style={styles.modalSubTitle}>
                hive SMS is available to users 13 and up
              </Text>
              <TouchableOpacity
                onPress={this.showDatePicker}
                style={styles.analyzeNowBtn}>
                <Text style={styles.analyzeNowText}>I am 13 or older</Text>
              </TouchableOpacity>
              <DateTimePicker
                mode="date"
                titleIOS="My Birthdate:"
                isVisible={visibleDatePicker}
                date={date}
                minimumDate={min}
                maximumDate={max}
                onConfirm={this.onDateChange}
                onCancel={this.onCancelDatPicker}
              />
            </>
          ) : (
            <>
              {user.verifyMe === true &&
              user.validatedPhone === false &&
              user.acceptedTermsSMS === true ? (
                <Verification
                  triggerSendVerificationCode={this.triggerSendVerificationCode}
                  modalClose={onClose}
                  {...this.props}
                />
              ) : (
                <>
                  {age >= 13 ? (
                    <>
                      <TextInputMask
                        type={'custom'}
                        options={{
                          mask: '+99999999999',
                        }}
                        value={digits}
                        onChangeText={text => {
                          this.setState({
                            digits: text,
                          });
                        }}
                        placeholder="+18589997777"
                        style={styles.pnMask}
                      />
                      {digits.length === 12 && (
                        <>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'flex-start',
                              marginTop: 10,
                            }}>
                            <CheckBox
                              Component={TermsCheckBox}
                              checked={isCheckedTerms}
                              checkedColor={Colors.scheme.blue}
                              uncheckedColor={Colors.scheme.blue}
                              onPress={this.toggleCheckBox}
                            />
                            <View style={{marginLeft: 5, flex: 1}}>
                              <Text>
                                I am at least 13 years old, and have read,
                                accepted the{' '}
                              </Text>
                              <TouchableOpacity
                                style={{marginTop: 2}}
                                onPress={this.toggleTermsModal}>
                                <Text style={styles.termsText}>
                                  hive SMS terms and conditions
                                </Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                          {isCheckedTerms && (
                            <TouchableOpacity
                              onPress={this.onAccept}
                              activeOpacity={isPressedAccept ? 1 : 0.5}
                              style={[
                                styles.analyzeNowBtn,
                                isPressedAccept && {backgroundColor: '#aaa'},
                              ]}>
                              <Text style={styles.analyzeNowText}>
                                I Accept
                              </Text>
                            </TouchableOpacity>
                          )}
                          <FlashMessage position="center" />
                        </>
                      )}
                    </>
                  ) : (
                    <>
                      <Text style={styles.modalSubTitle}>
                        {
                          'hive SMS features are currently only available to users 13 and'
                        }
                      </Text>
                      <TouchableOpacity
                        onPress={this.onOK}
                        style={styles.analyzeNowBtn}>
                        <Text style={styles.analyzeNowText}>OK</Text>
                      </TouchableOpacity>
                    </>
                  )}
                </>
              )}
            </>
          )}
        </View>
        <TermsModal
          visible={isVisibleTermsModal}
          onClose={this.toggleTermsModal}
        />
      </ModalWrapper>
    );
  }
}
