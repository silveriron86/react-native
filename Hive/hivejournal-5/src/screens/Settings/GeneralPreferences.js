import React from 'react';
import {Text, View, Switch} from 'react-native';
import {styles} from './_styles';

export default class GeneralPreferences extends React.Component {
  _setValue = (field, value) => {
    const {generalPreferences} = this.props;
    let data = JSON.parse(JSON.stringify(generalPreferences));
    data[field] = value;
    this.props.setSettings({type: 'GENERAL_PREFERENCES', value: data});
  };

  render() {
    const {generalPreferences} = this.props;
    return (
      <>
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>General Preferences</Text>
        </View>
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>
              Convert two spaces to a tag (beta)
            </Text>
          </View>
          <Switch
            value={generalPreferences.convertTagEnabled}
            onValueChange={value => {
              this._setValue('convertTagEnabled', value);
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>
              Auto-save note when tapping the enter key
            </Text>
          </View>
          <Switch
            value={generalPreferences.autoSaveEnabled}
            onValueChange={value => {
              this._setValue('autoSaveEnabled', value);
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>
              Show background image when composing
            </Text>
          </View>
          <Switch
            value={generalPreferences.showBackgroundImg}
            onValueChange={value => {
              this._setValue('showBackgroundImg', value);
            }}
          />
        </View>
      </>
    );
  }
}
