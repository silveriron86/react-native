/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {Text, View, Switch} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {TextInput} from 'react-native-gesture-handler';
import {Chevron} from 'react-native-shapes';
import DatePicker from 'react-native-datepicker';
import RNPickerSelect from 'react-native-picker-select';
import moment from 'moment-timezone';
import {SettingActions} from '../../actions';
import SettingConstants from '../../constants/SettingConstants';
import DummyData from '../../constants/DummyData';
import CustomSelector from '../../widgets/CustomSelector';
import {styles, selectStyle, selectTextInputStyle} from './_styles';
import Utils from '../../utils';

let SELECT_CLOUDS_MULTIPLIER = [];
for (let i = 1; i < 15; i++) {
  SELECT_CLOUDS_MULTIPLIER.push({label: i.toString(), value: i});
}

const DATETIME_STYLE = {
  dateInput: {margin: 0, height: 30, marginTop: -10, paddingHorizontal: 5},
};

class ThreeQ extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      isAdmin: false,
    };
  }

  _checkIfAdmin = () => {
    AsyncStorage.getItem('USER_ID', (_err, user_id) => {
      this.setState({
        isAdmin: user_id !== null && DummyData.adminUsers.includes(user_id),
      });
    });
  };

  updateLastSaved = (saved) => {
    this.props.setSettings({
      type: 'THREE_THINGS_SAVED',
      value: Utils.toUTC(saved),
    });
    this.saveLastOverrides();
  };

  onToggle3QOverrides = (value) => {
    if (value === true) {
      let saved = moment().tz(this.props.location).subtract(1, 'days').set({
        hour: 22,
        minute: 0,
        second: 0,
      });

      this.props.setSettings({
        type: 'THREE_THINGS_SAVED',
        value: Utils.toUTC(saved),
      });

      this.saveLastOverrides();
    } else {
      this.change3QOverrides();
    }
  };

  saveLastOverrides = () => {
    if (this.props.threeQOverridesEnabled) {
      setTimeout(() => {
        const {
          lastSavedTime,
          q3SavePeriod,
          snoozeTime,
          cloudsStartTime,
          cloudsMultiplier,
        } = this.props;
        AsyncStorage.setItem(
          'OVERRIDES_UPDATED',
          JSON.stringify({
            lastSavedTime,
            q3SavePeriod,
            snoozeTime,
            cloudsStartTime,
            cloudsMultiplier,
          }),
        );
      }, 10);
    }
  };

  componentWillReceiveProps(nextProps) {
    this._checkIfAdmin();
  }

  componentDidMount() {
    this._checkIfAdmin();
    this.change3QOverrides();
  }

  change3QOverrides = () => {
    const {threeQOverridesEnabled} = this.props;
    if (threeQOverridesEnabled) {
      // overrides
      AsyncStorage.getItem('OVERRIDES_UPDATED', (_err, saved) => {
        if (saved) {
          const updatedValues = JSON.parse(saved);
          AsyncStorage.getItem('LAST_3THINGS_SAVED', (__err, _saved) => {
            this.props.setSettings({
              type: 'THREE_THINGS_SAVED',
              value:
                updatedValues.lastSavedTime === 'null'
                  ? _saved
                  : updatedValues.lastSavedTime,
            });
          });
          this.props.setSettings({
            type: 'Q3_SAVE_PERIOD',
            value: updatedValues.q3SavePeriod,
          });
          this.props.setSettings({
            type: 'SNOOZE_TIME',
            value: updatedValues.snoozeTime,
          });
          this.props.setSettings({
            type: 'CLOUDS_START_TIME',
            value: updatedValues.cloudsStartTime,
          });
          this.props.setSettings({
            type: 'CLOUDS_MULTIPLIER',
            value: updatedValues.cloudsMultiplier,
          });
        } else {
          this.initRealOverrides();
        }
      });
    } else {
      this.initRealOverrides();
    }
  };

  initRealOverrides = () => {
    // use real values
    AsyncStorage.getItem('LAST_3THINGS_SAVED', (_err, saved) => {
      this.props.setSettings({type: 'THREE_THINGS_SAVED', value: saved});
    });
    this.props.setSettings({
      type: 'Q3_SAVE_PERIOD',
      value: SettingConstants.Q3_SAVE_PERIOD,
    });
    this.props.setSettings({
      type: 'SNOOZE_TIME',
      value: SettingConstants.SNOOZE_TIME,
    });
    this.props.setSettings({
      type: 'CLOUDS_START_TIME',
      value: SettingConstants.CLOUDS_START_TIME,
    });
    this.props.setSettings({
      type: 'CLOUDS_MULTIPLIER',
      value: SettingConstants.CLOUDS_MULTIPLIER,
    });
  };

  render() {
    const {
      location,
      snoozeTime,
      cloudsStartTime,
      cloudsMultiplier,
      q3SavePeriod,
      threeQJourneyEnabled,
      guide,
      threeQOverridesEnabled,
      lastSavedTime,
    } = this.props;
    console.log(location);
    return (
      <View style={{width: '100%'}}>
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>Enabled</Text>
          </View>
          <Switch
            value={threeQJourneyEnabled}
            onValueChange={(value) => {
              this.props.setSettings({
                type: '3Q_JOURNEY_ENABLED',
                value: value,
              });
            }}
          />
        </View>

        <View style={[styles.row, {paddingVertical: 3}]}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>Guide</Text>
          </View>
          <CustomSelector
            value={guide}
            onValueChange={(value) => {
              this.props.setSettings({type: 'GUIDE', value: value});
            }}
          />
        </View>

        <View style={styles.sepLine} />
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>Admin 3Q</Text>
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>3Q Overrides</Text>
          </View>
          <Switch
            value={threeQOverridesEnabled}
            onValueChange={(value) => {
              this.props.setSettings({
                type: '3Q_OVERRIDES_ENABLED',
                value: value,
              });
              setTimeout(() => {
                this.onToggle3QOverrides(value);
              }, 1);
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Last Saved Date & Time
            </Text>
          </View>
          <DatePicker
            disabled={!threeQOverridesEnabled}
            style={{
              width: 134,
              height: 30,
              backgroundColor: threeQOverridesEnabled ? 'white' : '#efefef',
            }}
            mode="datetime"
            placeholder=" "
            format="YYYY-MM-DD HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={DATETIME_STYLE}
            date={
              lastSavedTime ? Utils.toTimezone(lastSavedTime, location) : null
            }
            onDateChange={this.updateLastSaved}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Time Period To Save 3Q
            </Text>
          </View>
          <DatePicker
            style={{
              width: 50,
              height: 30,
              backgroundColor: threeQOverridesEnabled ? 'white' : '#efefef',
            }}
            mode="time"
            placeholder=""
            format="HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={DATETIME_STYLE}
            date={q3SavePeriod.start}
            disabled={!threeQOverridesEnabled}
            onDateChange={(v) => {
              this.props.setSettings({
                type: 'Q3_SAVE_PERIOD',
                value: {
                  start: v,
                  end: q3SavePeriod.end,
                },
              });
              this.saveLastOverrides();
            }}
          />
          <Text style={{marginHorizontal: 3}}>{'~'}</Text>
          <DatePicker
            style={{
              width: 50,
              height: 30,
              backgroundColor: threeQOverridesEnabled ? 'white' : '#efefef',
            }}
            mode="time"
            placeholder=""
            format="HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={DATETIME_STYLE}
            date={q3SavePeriod.end}
            disabled={!threeQOverridesEnabled}
            onDateChange={(v) => {
              this.props.setSettings({
                type: 'Q3_SAVE_PERIOD',
                value: {
                  start: q3SavePeriod.start,
                  end: v,
                },
              });
              this.saveLastOverrides();
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Snooze Length Of Time(Minutes)
            </Text>
          </View>
          <TextInput
            keyboardType={'number-pad'}
            value={snoozeTime}
            style={[
              styles.snoozeText,
              !threeQOverridesEnabled && styles.readOnly,
            ]}
            editable={threeQOverridesEnabled}
            onChangeText={(v) => {
              this.props.setSettings({type: 'SNOOZE_TIME', value: v});
              this.saveLastOverrides();
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Time To Start Clouds(Hours)
            </Text>
          </View>
          <TextInput
            keyboardType={'number-pad'}
            value={cloudsStartTime}
            style={[
              styles.snoozeText,
              !threeQOverridesEnabled && styles.readOnly,
            ]}
            editable={threeQOverridesEnabled}
            onChangeText={(v) => {
              this.props.setSettings({type: 'CLOUDS_START_TIME', value: v});
              this.saveLastOverrides();
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Clouds Multiplier
            </Text>
          </View>
          <View style={{opacity: threeQOverridesEnabled ? 1 : 0.6}}>
            <RNPickerSelect
              disabled={!threeQOverridesEnabled}
              placeholder={{}}
              items={SELECT_CLOUDS_MULTIPLIER}
              onValueChange={(value) => {
                this.props.setSettings({
                  type: 'CLOUDS_MULTIPLIER',
                  value: value,
                });
                this.saveLastOverrides();
              }}
              style={selectStyle}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              value={cloudsMultiplier}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    location: state.SettingReducer.location,
    cloudsMultiplier: state.SettingReducer.cloudsMultiplier,
    cloudsStartTime: state.SettingReducer.cloudsStartTime,
    q3SavePeriod: state.SettingReducer.q3SavePeriod,
    overrideSystemTime: state.SettingReducer.overrideSystemTime,
    tempSystemDateTime: state.SettingReducer.tempSystemDateTime,
    lastSavedTime: state.SettingReducer.lastSavedTime,
    threeQOverridesEnabled: state.SettingReducer.threeQOverridesEnabled,
    threeQJourneyEnabled: state.SettingReducer.threeQJourneyEnabled,
    guide: state.SettingReducer.guide,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ThreeQ);
