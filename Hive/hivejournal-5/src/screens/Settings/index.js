/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  StatusBar,
  Text,
  View,
  Switch,
  TouchableOpacity,
  Image,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {
  SiriShortcutsEvent,
  suggestShortcuts,
  clearAllShortcuts,
  getShortcuts,
  donateShortcut,
} from 'react-native-siri-shortcut';
import AsyncStorage from '@react-native-community/async-storage';
import VersionNumber from 'react-native-version-number';
import axios from 'axios';
import {Chevron} from 'react-native-shapes';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/AntDesign';
import appleAuth, {
  AppleAuthRequestOperation,
} from '@invertase/react-native-apple-authentication';
import {SettingActions, CommonActions, MongoActions} from '../../actions';
import SettingConstants from '../../constants/SettingConstants';
import ScreenNameConstants from '../../constants/ScreenNameConstants';
import ApiConstants from '../../constants/ApiConstants';
import LoadingOverlay from '../../widgets/LoadingOverlay';
import Utils from '../../utils';
import DummyData from '../../constants/DummyData';
import GeneralPreferences from './GeneralPreferences';
import {styles, selectStyle, selectTextInputStyle} from './_styles';
import Locker from '../../widgets/Locker';
import Login from '../../widgets/Login';
import EmptyView from '../../widgets/EmptyView';
import FooterTrigger from '../../widgets/FooterTrigger';
import PhoneNumber from './PhoneNumber';
import Colors from '../../constants/Colors';
import ShortcutsConstants from '../../constants/Shortcuts';
import * as RootNavigation from '../../navigation/RootNavigation';

const avatarIcon = require('../../../assets/icons/avatar.png');
// const auth0 = new Auth0(ApiConstants.getAuthApi());
let SELECT_SCREEN_NAMES1 = [];
let SELECT_SCREEN_NAMES2 = [];
let SELECT_SCREEN_NAMES3 = [];
ScreenNameConstants.adjectives.forEach((item, i) => {
  SELECT_SCREEN_NAMES1.push({label: Utils.capitalize(item), value: item});
});
ScreenNameConstants.colors.forEach((item, i) => {
  SELECT_SCREEN_NAMES2.push({label: item, value: item});
});
ScreenNameConstants.animals.forEach((item, i) => {
  SELECT_SCREEN_NAMES3.push({label: item, value: item});
});

const snSelectStyle = {
  viewContainer: {
    paddingVertical: 5,
    maxWidth: 200,
    backgroundColor: 'white',
    paddingLeft: 4,
  },
  iconContainer: {
    top: 5,
    right: 0,
  },
};

class SettingsScreen extends React.Component {
  constructor(props) {
    super(props);

    let screenNames = props.screenName.split(' ');
    this.state = {
      loading: false,
      isAdmin: false,

      // Screen name
      adjective: screenNames[0],
      color: screenNames[1],
      animal: screenNames[2],
      visiblePnModal: false,
    };
  }

  updateScreenName = () => {
    const {adjective, color, animal} = this.state;
    this.props.setSettings({
      type: 'SCREEN_NAME',
      value: `${adjective} ${color} ${animal}`,
    });
  };

  onChange = (value, type) => {
    this.props.setSettings({
      type: `${type.toUpperCase()}_ENABLED`,
      value: value,
    });
  };

  handleAnalyze = () => {
    let notes = this.props.notes.slice(0);
    let items = [];
    let total_wordsCnt = 0;
    let used_notes_cnt = 0;
    notes.forEach((n, i) => {
      let cnt = n.split(' ').length;
      if (total_wordsCnt + cnt <= SettingConstants.MAX_WORDS) {
        used_notes_cnt++;
        total_wordsCnt += cnt;
        items.push({
          content: n,
        });
      }
    });

    this.setState(
      {
        loading: true,
      },
      () => {
        let apiData = ApiConstants.getPersonalityInsightsApi();

        axios({
          method: 'POST',
          url: `${apiData.url}/v3/profile?version=${apiData.version}&consumption_preferences=true&raw_scores=true`,
          headers: {
            Authorization: 'Basic ' + Utils.btoa('apiKey:' + apiData.key),
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
          data: {
            contentItems: items,
          },
        })
          .then((response) => {
            console.log(response);
            this.setState(
              {
                loading: false,
              },
              () => {
                if (response.status === 200) {
                  console.log(response.data.personality);
                  AsyncStorage.setItem(
                    'IBM_PERSONALITY',
                    JSON.stringify(response.data.personality),
                  );

                  let updated_notes = this.props.notes.slice(used_notes_cnt);
                  this.props.setSettings({
                    type: 'NOTES',
                    value: updated_notes,
                  });
                }
              },
            );
          })
          .catch((error) => {
            console.log(error.response.data.error);
            this.setState({
              loading: false,
            });
          });
      },
    );
  };

  _logoutApple = async () => {
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: AppleAuthRequestOperation.LOGOUT,
    });
    await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);
  };

  handleLogOut = () => {
    if (global.scheduleTimer) {
      clearInterval(global.scheduleTimer);
    }

    const {user} = this.props;
    console.log(user);
    if (typeof user.type !== 'undefined' && user.type === 'Apple') {
      this._logoutApple();
    }

    this.setState(
      {
        isAdmin: false,
      },
      () => {
        this.props.resetCommon();
        this.props.resetMongo();
        this.props.setSettings({type: 'RESET'});
        global.eventEmitter.emit('LOGOUT', {});
        RootNavigation.navigate('FeedStack');
        RootNavigation.setVisibleFooter(false);
      },
    );
    // auth0.webAuth.clearSession();
  };

  _checkIfAdmin = () => {
    AsyncStorage.getItem('USER_ID', (_err, user_id) => {
      this.setState({
        isAdmin: user_id !== null && DummyData.adminUsers.includes(user_id),
      });
    });
  };

  componentWillReceiveProps(nextProps) {
    if (!this.mounted) {
      return;
    }
    this._checkIfAdmin();

    if (this.props.accessToken !== nextProps.accessToken) {
      RootNavigation.setParams({accessToken: nextProps.accessToken});

      if (nextProps.accessToken) {
        this._updateScreenNames(nextProps);
      }
    }

    if (this.props.screenName !== nextProps.screenName) {
      this._updateScreenNames(nextProps);
    }

    if (this.props.passcode !== nextProps.passcode) {
      RootNavigation.setParams({
        locked: nextProps.passcode.locked && nextProps.passcode.enabled,
      });
    }
  }

  _updateScreenNames = (nextProps) => {
    let screenNames = nextProps.screenName.split(' ');
    this.setState({
      adjective: screenNames[0].toLowerCase(),
      color: screenNames[1],
      animal: screenNames[2],
    });
  };

  componentWillUnmount() {
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;

    const {navigation, route, accessToken, passcode} = this.props;
    navigation.setParams({
      accessToken,
      locked: passcode.locked && passcode.enabled,
    });

    this._checkIfAdmin();

    global.eventEmitter.addListener('CREATE_JOURNAL', () => {
      this.props.navigation.navigate('TagsWithoutAnim', {isCreating: true});
    });

    if ((route.params?.logout ?? false) === true) {
      this.handleLogOut();
    }

    global.eventEmitter.addListener('TO_PASSCODE', () => {
      this.props.navigation.navigate('Passcode');
    });

    // For Shortcuts
    try {
      clearAllShortcuts();
      getShortcuts();
    } catch (e) {
      console.log("You're not running iOS 12!");
    }

    SiriShortcutsEvent.addListener(
      'SiriShortcutListener',
      this.handleSiriShortcut,
    );

    suggestShortcuts(ShortcutsConstants);
    ShortcutsConstants.forEach((opt) => {
      donateShortcut(opt);
    });
  }

  // For Shortcuts
  handleSiriShortcut = (info) => {
    console.log('--- Shortcut ---');
    console.log(info);
  };

  togglePnModal = () => {
    const {visiblePnModal} = this.state;
    if (!visiblePnModal) {
      Utils.trackEvent('Action', 'settings:mobile SMS');
    }
    this.setState({
      visiblePnModal: !visiblePnModal,
    });
  };

  render() {
    const {
      userData,
      // spotifyEnabled,
      toneEnabled,
      navigation,
      // streamFont,
      accessToken,
      tonePack,
      locations,
      location,
      // fontSize,
      doorway,
    } = this.props;
    const {loading, isAdmin, visiblePnModal} = this.state;
    if (accessToken === 'none') {
      return <EmptyView />;
    }

    if (accessToken === null) {
      return <Login />;
    }

    let allLocs = [];
    if (locations && locations.length > 0) {
      locations.forEach((loc, i) => {
        allLocs.push({
          label: loc.time_zone.id,
          value: loc.time_zone.id,
        });
      });
    }

    return (
      <Locker {...this.props}>
        <StatusBar backgroundColor={Colors.scheme.black} />
        <SafeAreaView style={{flex: 1, backgroundColor: Colors.scheme.black}}>
          <View style={{flex: 1}}>
            <ScrollView style={{flex: 1, backgroundColor: 'white'}}>
              <LoadingOverlay loading={loading} />
              <View style={{flex: 1}}>
                <View style={styles.branchContainer}>
                  <View style={{paddingBottom: 10}}>
                    <Text numberOfLines={1} style={styles.labelText}>
                      Version: {VersionNumber.appVersion}.
                      {VersionNumber.buildVersion}
                    </Text>
                  </View>
                  {userData && (
                    <>
                      <TouchableOpacity
                        style={[styles.row, styles.profileRow]}
                        onPress={() => navigation.navigate('Profile')}>
                        <View style={{width: 75}}>
                          {userData.picture ? (
                            <Image
                              source={{uri: userData.picture}}
                              style={styles.avatarIcon}
                            />
                          ) : (
                            <Image
                              source={avatarIcon}
                              style={styles.avatarIcon}
                            />
                          )}
                        </View>
                        <View style={{flex: 1}}>
                          <Text style={{fontSize: 22}} numberOfLines={1}>
                            {userData.name}
                          </Text>
                          <Text
                            style={{marginTop: 2, color: '#666'}}
                            numberOfLines={1}>
                            {userData.email}
                          </Text>
                        </View>
                        <Icon name="right" size={18} color="#666" />
                      </TouchableOpacity>
                      <View style={styles.sepLine} />
                    </>
                  )}
                  {/* <View style={[styles.row, {paddingVertical: 5}]}>
                    <TouchableOpacity
                      style={styles.goPremiumBtn}
                      onPress={() => navigation.navigate('Premium')}>
                      <Text style={styles.goPremiumBtnText}>Go Premium</Text>
                    </TouchableOpacity>
                  </View> */}

                  {/* <View style={styles.sepLine} />
                  <TouchableOpacity
                    style={[styles.row, {height: 50}]}
                    onPress={() => navigation.navigate('Journeys')}>
                    <View style={styles.switchLabel}>
                      <Text
                        style={[styles.labelText, styles.sectionHeaderText]}>
                        Journeys
                      </Text>
                    </View>
                    <Icon name="right" size={18} color="#666" />
                  </TouchableOpacity> */}

                  {/* <View style={styles.sepLine} /> */}
                  <TouchableOpacity
                    style={[styles.row, {height: 50}]}
                    onPress={() => navigation.navigate('Tags')}>
                    <View style={styles.switchLabel}>
                      <Text
                        style={[styles.labelText, styles.sectionHeaderText]}>
                        Journals
                      </Text>
                    </View>
                    <Icon name="right" size={18} color="#666" />
                  </TouchableOpacity>

                  <View style={styles.sepLine} />
                  <TouchableOpacity
                    style={[styles.row, {height: 50}]}
                    onPress={() => navigation.navigate('Shortcut')}>
                    <View style={styles.switchLabel}>
                      <Text
                        style={[styles.labelText, styles.sectionHeaderText]}>
                        Siri Shortcuts (beta)
                      </Text>
                    </View>
                    <Icon name="right" size={18} color="#666" />
                  </TouchableOpacity>

                  {/* <View style={styles.sepLine} />
                  <TouchableOpacity
                    style={[styles.row, {height: 50}]}
                    onPress={() => navigation.navigate('Personality')}>
                    <View style={styles.switchLabel}>
                      <Text
                        style={[styles.labelText, styles.sectionHeaderText]}>
                        Personality Type
                      </Text>
                    </View>
                    <Icon name="right" size={18} color="#666" />
                  </TouchableOpacity> */}

                  <View style={styles.sepLine} />
                  {/* <TouchableOpacity
                    style={[styles.row, {height: 50}]}
                    onPress={this.togglePnModal}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Phone number</Text>
                    </View>
                    <Icon name="right" size={18} color="#666" />
                  </TouchableOpacity> */}
                  {/* <PhoneNumber
                    visible={visiblePnModal}
                    onClose={this.togglePnModal}
                    {...this.props}
                  /> */}
                  <View style={styles.row}>
                    <View>
                      <Text numberOfLines={1} style={styles.labelText}>
                        Screen Name
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                      }}>
                      {/* <Text>{screenName}</Text> */}
                      <RNPickerSelect
                        placeholder={{}}
                        items={SELECT_SCREEN_NAMES1}
                        onValueChange={(v) => {
                          this.setState({adjective: v}, () => {
                            this.updateScreenName();
                          });
                        }}
                        value={this.state.adjective}
                        style={snSelectStyle}
                        useNativeAndroidPickerStyle={false}
                        Icon={null}
                      />
                      <RNPickerSelect
                        placeholder={{}}
                        items={SELECT_SCREEN_NAMES2}
                        onValueChange={(v) => {
                          this.setState({color: v}, () => {
                            this.updateScreenName();
                          });
                        }}
                        value={this.state.color}
                        style={snSelectStyle}
                        useNativeAndroidPickerStyle={false}
                        Icon={null}
                      />
                      <RNPickerSelect
                        placeholder={{}}
                        items={SELECT_SCREEN_NAMES3}
                        onValueChange={(v) => {
                          this.setState({animal: v}, () => {
                            this.updateScreenName();
                          });
                        }}
                        value={this.state.animal}
                        style={snSelectStyle}
                        useNativeAndroidPickerStyle={false}
                        Icon={null}
                      />
                    </View>
                  </View>

                  {/* <View style={styles.row}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Doorway</Text>
                    </View>
                    <RNPickerSelect
                      placeholder={{}}
                      items={SettingConstants.SELECT_DOORWAYS}
                      onValueChange={value => {
                        Utils.trackEvent('Action', 'User selects a doorway');
                        this.props.setSettings({type: 'DOORWAY', value: value});
                      }}
                      value={doorway}
                      style={selectStyle}
                      useNativeAndroidPickerStyle={false}
                      textInputProps={selectTextInputStyle}
                      Icon={() => {
                        return <Chevron size={1.5} color="gray" />;
                      }}
                    />
                  </View> */}

                  <TouchableOpacity
                    style={[styles.row, {height: 50}]}
                    onPress={() => navigation.navigate('Passcode')}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Passcode</Text>
                    </View>
                    <Icon name="right" size={18} color="#666" />
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[styles.row, {height: 50}]}
                    onPress={() => navigation.navigate('Visibility')}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Note Type Visibility</Text>
                    </View>
                    <Icon name="right" size={18} color="#666" />
                  </TouchableOpacity>

                  {/* <View style={styles.row}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Spotify Integration</Text>
                    </View>
                    <Switch value={spotifyEnabled} onValueChange={v=>this.onChange(v, 'spotify')}></Switch>
                  </View> */}

                  <View style={styles.row}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Notes Tone Analyzer</Text>
                    </View>
                    <Switch
                      value={toneEnabled}
                      onValueChange={(v) => this.onChange(v, 'tone')}
                    />
                  </View>

                  {/* <View style={styles.personalityRow}>
                    <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
                      <View style={styles.switchLabel}>
                        <Text style={styles.labelText}>Personality Analyzer</Text>
                      </View>
                      <Switch value={personalityEnabled} onValueChange={v=>this.onChange(v, 'personality')}></Switch>
                    </View>

                    {
                      personalityEnabled &&
                      <View style={{alignItems: 'flex-end'}}>
                      {
                        (wordsCnt >= SettingConstants.MIN_WORDS) ?
                        <TouchableOpacity style={styles.analyzeNowBtn} onPress={this.handleAnalyze}>
                          <Text style={styles.analyzeNowText}>Analyze now</Text>
                        </TouchableOpacity>
                        :
                        <View style={{marginTop: 10}}>
                          <Text style={{fontStyle: 'italic', color: 'gray', fontSize: 13}}>Available after 100 words are saved</Text>
                        </View>
                      }
                      </View>
                    }
                  </View>  */}

                  <View style={styles.row}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Tone Packs</Text>
                    </View>
                    <RNPickerSelect
                      placeholder={{}}
                      items={SettingConstants.SELECT_TONE_PACKS}
                      onValueChange={(value) => {
                        Utils.trackEvent('Action', 'User selects a tone pack');
                        this.props.setSettings({
                          type: 'TONE_PACK',
                          value: value,
                        });
                      }}
                      value={tonePack}
                      style={selectStyle}
                      useNativeAndroidPickerStyle={false}
                      textInputProps={selectTextInputStyle}
                      Icon={() => {
                        return <Chevron size={1.5} color="gray" />;
                      }}
                    />
                  </View>

                  {allLocs.length > 0 && location !== '' && (
                    <View style={styles.row}>
                      <View style={styles.switchLabel}>
                        <Text style={styles.labelText}>Timezones</Text>
                      </View>
                      <RNPickerSelect
                        placeholder={{}}
                        items={allLocs}
                        onValueChange={(value) => {
                          this.props.setSettings({
                            type: 'LOCATION',
                            value: value,
                          });
                        }}
                        style={selectStyle}
                        value={location}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={selectTextInputStyle}
                        Icon={() => {
                          return <Chevron size={1.5} color="gray" />;
                        }}
                      />
                    </View>
                  )}

                  <TouchableOpacity
                    style={[styles.row, {height: 50}]}
                    onPress={() => {
                      AsyncStorage.setItem('FROM_SETTINGS', 'YES');
                      navigation.navigate('Font');
                    }}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Font</Text>
                    </View>
                    <Icon name="right" size={18} color="#666" />
                  </TouchableOpacity>

                  <View style={styles.sepLine} />
                  <GeneralPreferences {...this.props} />

                  {isAdmin && (
                    <>
                      <View style={styles.sepLine} />
                      <TouchableOpacity
                        style={[styles.row, {height: 50}]}
                        onPress={() => navigation.navigate('Admin')}>
                        <View style={styles.switchLabel}>
                          <Text
                            style={[
                              styles.labelText,
                              styles.sectionHeaderText,
                            ]}>
                            Admin
                          </Text>
                        </View>
                        <Icon name="right" size={18} color="#666" />
                      </TouchableOpacity>
                    </>
                  )}
                </View>
              </View>
            </ScrollView>
            <FooterTrigger
              screen="settings"
              flowJourney={this.props.flowJourney}
            />
          </View>
        </SafeAreaView>
      </Locker>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    timezone: state.SettingReducer.timezone,
    flowJourney: state.SettingReducer.flowJourney,
    snoozeTime: state.SettingReducer.snoozeTime,
    toneEnabled: state.SettingReducer.toneEnabled,
    spotifyEnabled: state.SettingReducer.spotifyEnabled,
    personalityEnabled: state.SettingReducer.personalityEnabled,
    streamFont: state.SettingReducer.streamFont,
    tonePack: state.SettingReducer.tonePack,
    locations: state.SettingReducer.locations,
    location: state.SettingReducer.location,
    fontSize: state.SettingReducer.fontSize,
    passcode: state.SettingReducer.passcode,
    depthAndSize: state.SettingReducer.depthAndSize,
    overrideSystemTime: state.SettingReducer.overrideSystemTime,
    tempSystemDateTime: state.SettingReducer.tempSystemDateTime,
    screenName: state.SettingReducer.screenName,
    doorway: state.SettingReducer.doorway,
    generalPreferences: state.SettingReducer.generalPreferences,
    accessToken: state.CommonReducer.accessToken,
    userData: state.CommonReducer.userData,
    user: state.SettingReducer.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
    verify: (req) => dispatch(SettingActions.verify(req.cb)),
    setAccessToken: (req) => dispatch(CommonActions.setAccessToken(req.value)),
    setUser: (req) => dispatch(CommonActions.setUser(req.value)),
    resetCommon: (req) => dispatch(CommonActions.reset()),
    resetMongo: (req) => dispatch(MongoActions.reset()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);
