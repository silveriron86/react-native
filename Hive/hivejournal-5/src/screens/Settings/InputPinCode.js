import React from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import PINCode from '@haskkor/react-native-pincode';
import {SettingActions} from '../../actions';
import * as RootNavigation from '../../navigation/RootNavigation';

class InputPinCode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // confirmed: props.navigation.getParam('new'),
      confirmed: props.route.params.new ?? false,
    };
  }

  componentDidMount() {
    this.mount = true;
  }

  componentWillUnmount() {
    this.mount = false;
  }

  render() {
    const {confirmed} = this.state;
    const {navigation, passcode} = this.props;

    return (
      <View style={{flex: 1}}>
        {confirmed === true ? (
          <PINCode
            titleChoose={'Enter new passcode'}
            titleConfirm={'Re-enter new passcode'}
            status={'choose'}
            timeLocked={0}
            stylePinCodeColorTitle={'black'}
            stylePinCodeColorSubtitle={'black'}
            stylePinCodeCircle={{opacity: 1}}
            onClickButtonLockedPage={() => {}}
            touchIDDisabled={true}
            storePin={(code) => {
              let data = JSON.parse(JSON.stringify(passcode));
              data.enabled = true;
              data.number = code;
              this.props.setSettings({type: 'PASSCODE', value: data});
              setTimeout(() => {
                navigation.goBack();
              }, 50);
            }}
          />
        ) : (
          <PINCode
            titleEnter={'Enter current passcode'}
            status={'enter'}
            storedPin={passcode.number}
            timeLocked={0}
            touchIDDisabled={true}
            stylePinCodeColorTitle={'black'}
            stylePinCodeColorSubtitle={'black'}
            stylePinCodeCircle={{opacity: 1}}
            onClickButtonLockedPage={() => {}}
            handleResultEnterPin={(result) => {
              if (result === passcode.number) {
                this.setState({
                  confirmed: true,
                });
              }
            }}
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    passcode: state.SettingReducer.passcode,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InputPinCode);
