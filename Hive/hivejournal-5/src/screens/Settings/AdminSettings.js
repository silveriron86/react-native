/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View, Switch, Alert, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import DatePicker from 'react-native-datepicker';
import {TextInput} from 'react-native-gesture-handler';
import {styles} from './_styles';
import {SettingActions} from '../../actions';
import SettingConstants from '../../constants/SettingConstants';

const DATETIME_STYLE = {
  dateInput: {margin: 0, height: 30, marginTop: -10, paddingHorizontal: 5},
};

class AdminSettings extends React.Component {
  constructor(props) {
    super(props);
  }

  updateDepthSize = (type, index, value) => {
    let depthAndSize = JSON.parse(JSON.stringify(this.props.depthAndSize));
    if (type === 'enabled') {
      depthAndSize[type] = value;
      if (value === false) {
        depthAndSize = SettingConstants.defaultDepthSize;
      }
    } else {
      depthAndSize[type][index] = value;
    }

    console.log(depthAndSize);
    this.props.setSettings({type: 'DEPTH_SIZE', value: depthAndSize});
  };

  render() {
    const {overrideSystemTime, tempSystemDateTime, depthAndSize} = this.props;
    return (
      <ScrollView
        contentContainerStyle={{
          width: '100%',
          flex: 1,
          backgroundColor: 'white',
        }}>
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>Admin General</Text>
        </View>
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>Override System Time</Text>
          </View>
          <Switch
            value={overrideSystemTime}
            onValueChange={(value) => {
              if (tempSystemDateTime) {
                this.props.setSettings({
                  type: 'OVERRIDE_SYSTEM_TIME',
                  value: value,
                });
              } else {
                Alert.alert(
                  'Error',
                  'Firstly, please enter a [Temp System Date/Time UTC] value',
                );
              }
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Temp System Date/Time UTC
            </Text>
          </View>
          <DatePicker
            style={{width: 134, height: 30}}
            mode="datetime"
            placeholder=" "
            format="YYYY-MM-DD HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={DATETIME_STYLE}
            date={tempSystemDateTime ? tempSystemDateTime.substr(0, 16) : null}
            onDateChange={(v) => {
              this.props.setSettings({
                type: 'TEMP_SYSTEM_DATETIME',
                value: v + ' +0000',
              });
            }}
          />
        </View>

        <View style={styles.sepLine} />
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>Admin Depth/Size</Text>
        </View>
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>Overrides</Text>
          </View>
          <Switch
            value={depthAndSize.enabled}
            onValueChange={(value) => this.updateDepthSize('enabled', 0, value)}
          />
        </View>
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Card spacing horizontal
            </Text>
          </View>
          <TextInput
            keyboardType={'number-pad'}
            value={depthAndSize.hSpace[0].toString()}
            style={[
              styles.snoozeText,
              {width: 50},
              !depthAndSize.enabled && styles.readOnly,
            ]}
            onChangeText={(v) => this.updateDepthSize('hSpace', 0, v)}
          />
          <Text style={{marginHorizontal: 3}}>{'~'}</Text>
          <TextInput
            keyboardType={'number-pad'}
            value={depthAndSize.hSpace[1].toString()}
            style={[
              styles.snoozeText,
              {width: 50},
              !depthAndSize.enabled && styles.readOnly,
            ]}
            onChangeText={(v) => this.updateDepthSize('hSpace', 1, v)}
          />
        </View>
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Card vertical position
            </Text>
          </View>
          <TextInput
            keyboardType={'number-pad'}
            value={depthAndSize.vPosition[0].toString()}
            style={[
              styles.snoozeText,
              {width: 50},
              !depthAndSize.enabled && styles.readOnly,
            ]}
            onChangeText={(v) => this.updateDepthSize('vPosition', 0, v)}
          />
          <Text style={{marginHorizontal: 3}}>{'~'}</Text>
          <TextInput
            keyboardType={'number-pad'}
            value={depthAndSize.vPosition[1].toString()}
            style={[
              styles.snoozeText,
              {width: 50},
              !depthAndSize.enabled && styles.readOnly,
            ]}
            onChangeText={(v) => this.updateDepthSize('vPosition', 1, v)}
          />
        </View>
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Card size at vertical
            </Text>
          </View>
          <TextInput
            keyboardType={'number-pad'}
            value={depthAndSize.vSize[0].toString()}
            style={[
              styles.snoozeText,
              {width: 50},
              !depthAndSize.enabled && styles.readOnly,
            ]}
            onChangeText={(v) => this.updateDepthSize('vSize', 0, v)}
          />
          <Text style={{marginHorizontal: 3}}>{'~'}</Text>
          <TextInput
            keyboardType={'number-pad'}
            value={depthAndSize.vSize[1].toString()}
            style={[
              styles.snoozeText,
              {width: 50},
              !depthAndSize.enabled && styles.readOnly,
            ]}
            onChangeText={(v) => this.updateDepthSize('vSize', 1, v)}
          />
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    timezone: state.SettingReducer.timezone,
    depthAndSize: state.SettingReducer.depthAndSize,
    overrideSystemTime: state.SettingReducer.overrideSystemTime,
    tempSystemDateTime: state.SettingReducer.tempSystemDateTime,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminSettings);
