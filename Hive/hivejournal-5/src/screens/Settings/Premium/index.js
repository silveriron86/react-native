/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  Image,
} from 'react-native';
import Swiper from 'react-native-swiper';
import Colors from '../../../constants/Colors';

const PremiumImage1 = require('../../../../assets/images/premium.jpg');
const closeIcon = require('../../../../assets/icons/glyph_close_32x32.png');

export default class Premium extends React.Component {
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  render() {
    return (
      <SafeAreaView style={styles.premiumContainer}>
        <View style={{flex: 1}}>
          <Swiper
            containerStyle={styles.premiumSlidesWrapper}
            dotColor="#597691"
            activeDotColor={Colors.scheme.blue}
            showsButtons={true}
            showsPagination={true}
            onIndexChanged={this.onIndexChanged}>
            <View style={{flex: 1}}>
              <Image source={PremiumImage1} style={styles.premiumImage} />
            </View>
            <View style={{flex: 1}}>
              <Image source={PremiumImage1} style={styles.premiumImage} />
            </View>
            <View style={{flex: 1}}>
              <Image source={PremiumImage1} style={styles.premiumImage} />
            </View>
          </Swiper>
          <TouchableOpacity
            style={{position: 'absolute', top: 10, right: 10}}
            onPress={this.goBack}>
            <Image source={closeIcon} style={{tintColor: '#597691'}} />
          </TouchableOpacity>
        </View>
        <View style={styles.premiumFooter}>
          <Text style={styles.premiumTitle}>Unlock Premium</Text>
          <View style={styles.premiumButtons}>
            <TouchableOpacity style={styles.premiumBtn}>
              <Text style={styles.premiumBtnText}>MONTHLY</Text>
              <Text style={[styles.premiumBtnText, {fontSize: 15}]}>
                $4.99/month
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.premiumBtn}>
              <Text style={styles.premiumBtnText}>YEARLY</Text>
              <Text style={[styles.premiumBtnText, {fontSize: 15}]}>
                $35.99/year
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{alignItems: 'center', justifyContent: 'center'}}
            onPress={this.goBack}>
            <Text style={[styles.premiumBtnText, {fontSize: 14}]}>
              maybe later
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  premiumContainer: {
    backgroundColor: '#090d14',
    flex: 1,
  },
  premiumSlidesWrapper: {
    flex: 1,
  },
  premiumFooter: {
    height: 180,
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  premiumImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  premiumTitle: {
    fontFamily: 'Arial Rounded MT Bold',
    fontSize: 32,
    color: Colors.scheme.blue,
    textAlign: 'center',
  },
  premiumButtons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
  },
  premiumBtn: {
    borderWidth: 1,
    borderColor: Colors.scheme.blue,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginHorizontal: 15,
  },
  premiumBtnText: {
    fontFamily: 'Arial Rounded MT Bold',
    fontSize: 16,
    color: Colors.scheme.blue,
    textAlign: 'center',
  },
});
