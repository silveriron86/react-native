/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  StatusBar,
  Text,
  View,
  Switch,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Chevron} from 'react-native-shapes';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/AntDesign';
import {SettingActions} from '../../actions';
import LoadingOverlay from '../../widgets/LoadingOverlay';
import {styles, selectStyle, selectTextInputStyle} from './_styles';
import Locker from '../../widgets/Locker';
import * as RootNavigation from '../../navigation/RootNavigation';

const SELECT_AFTER = [
  {label: 'Immediately', value: 0},
  {label: 'After 1 minute', value: 1},
  {label: 'After 3 minutes', value: 3},
  {label: 'After 5 minutes', value: 5},
  {label: 'After 10 minutes', value: 10},
  {label: 'After 30 minutes', value: 30},
  {label: 'After 1 hour', value: 60},
];

class Postcode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.passcode !== nextProps.passcode) {
      RootNavigation.setParams({
        locked: nextProps.passcode.locked && nextProps.passcode.enabled,
      });
    }
  }

  componentDidMount() {
    this.mount = true;
    RootNavigation.setParams({
      locked: this.props.passcode.locked && this.props.passcode.enabled,
    });
  }

  componentWillUnmount() {
    this.mount = false;
  }

  _setValue = (field, value) => {
    const {passcode} = this.props;
    let data = JSON.parse(JSON.stringify(passcode));
    data[field] = value;
    this.props.setSettings({type: 'PASSCODE', value: data});
  };

  initPasscode = () => {
    this.props.setSettings({
      type: 'PASSCODE',
      value: {
        enabled: false,
        number: '',
        after: 0,
        locked: false,
        // notesLock: false,
        flowLock: false,
      },
    });
  };

  render() {
    const {passcode, navigation} = this.props;
    const {loading} = this.state;

    return (
      <Locker {...this.props}>
        <ScrollView style={{flex: 1, backgroundColor: 'white'}}>
          <StatusBar backgroundColor="blue" barStyle="light-content" />
          <LoadingOverlay loading={loading} />
          <View style={{flex: 1}}>
            <View style={styles.branchContainer}>
              <View style={styles.row}>
                <View style={styles.switchLabel}>
                  <Text style={styles.labelText}>Passcode</Text>
                </View>
                <Switch
                  value={passcode.enabled}
                  onValueChange={(v) => {
                    if (v === true) {
                      navigation.navigate('InputPinCode', {new: true});
                    } else {
                      this.initPasscode();
                    }
                  }}
                />
              </View>

              {passcode.enabled === true && (
                <>
                  <TouchableOpacity
                    style={styles.row}
                    onPress={() =>
                      navigation.navigate('InputPinCode', {new: false})
                    }>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Change Passcode</Text>
                    </View>
                    <Icon name="right" size={18} color="#666" />
                  </TouchableOpacity>

                  <View style={styles.row}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Require After</Text>
                    </View>
                    <RNPickerSelect
                      placeholder={{}}
                      items={SELECT_AFTER}
                      onValueChange={(v) => {
                        this._setValue('after', v);
                      }}
                      value={passcode.after}
                      style={selectStyle}
                      useNativeAndroidPickerStyle={false}
                      textInputProps={selectTextInputStyle}
                      Icon={() => {
                        return <Chevron size={1.5} color="gray" />;
                      }}
                    />
                  </View>

                  <View style={styles.sectionHeader}>
                    <Text style={styles.sectionHeaderText}>
                      Additional Lock
                    </Text>
                  </View>
                  {/* <View style={styles.row}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Notes Screen</Text>
                    </View>
                    <Switch
                      value={passcode.notesLock}
                      onValueChange={v => {
                        this._setValue('notesLock', v);
                      }}
                    />
                  </View> */}
                  <View style={styles.row}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>Flow Screen</Text>
                    </View>
                    <Switch
                      value={passcode.flowLock}
                      onValueChange={(v) => {
                        this._setValue('flowLock', v);
                      }}
                    />
                  </View>
                </>
              )}
            </View>
          </View>
        </ScrollView>
      </Locker>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    passcode: state.SettingReducer.passcode,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Postcode);
