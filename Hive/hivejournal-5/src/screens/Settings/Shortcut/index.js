/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-alert */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {StyleSheet, View, SafeAreaView, Text} from 'react-native';
import {presentShortcut} from 'react-native-siri-shortcut';
import AddToSiriButton, {
  SiriButtonStyles,
  supportsSiriButton,
} from 'react-native-siri-shortcut/AddToSiriButton';
import ShortcutsConstants from '../../../constants/Shortcuts';

export default class Shortcut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shortcutInfo: null,
      shortcutActivityType: null,
      addToSiriStyle: SiriButtonStyles.blackOutline,
      shortcuts: [],
    };
  }

  render() {
    const {addToSiriStyle} = this.state;

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          {supportsSiriButton && (
            <>
              <Text>1. Hive App Ideas</Text>
              <AddToSiriButton
                buttonStyle={addToSiriStyle}
                onPress={() => {
                  presentShortcut(ShortcutsConstants[0], ({status}) => {
                    console.log(`I was ${status}`);
                  });
                }}
                shortcut={ShortcutsConstants[0]}
              />
              <Text style={{marginTop: 15}}>2. New Entry in Journal</Text>
              <AddToSiriButton
                buttonStyle={addToSiriStyle}
                onPress={() => {
                  presentShortcut(ShortcutsConstants[1], ({status}) => {
                    console.log(`I was ${status}`);
                  });
                }}
                shortcut={ShortcutsConstants[1]}
              />
              <Text style={{marginTop: 15}}>3. Record Audio in Journal</Text>
              <AddToSiriButton
                buttonStyle={addToSiriStyle}
                onPress={() => {
                  presentShortcut(ShortcutsConstants[2], ({status}) => {
                    console.log(`I was ${status}`);
                  });
                }}
                shortcut={ShortcutsConstants[2]}
              />
              <Text style={{marginTop: 15}}>4. Add Photo to Journal</Text>
              <AddToSiriButton
                buttonStyle={addToSiriStyle}
                onPress={() => {
                  presentShortcut(ShortcutsConstants[3], ({status}) => {
                    console.log(`I was ${status}`);
                  });
                }}
                shortcut={ShortcutsConstants[3]}
              />
            </>
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
