/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import {CheckBox} from 'react-native-elements';
import ModalWrapper from 'react-native-modal-wrapper';
import RNPickerSelect from 'react-native-picker-select';
import moment from 'moment';
import {Chevron} from 'react-native-shapes';
import {styles} from './_styles';
import {SettingActions} from '../../../actions';
import {ProfileSelectors} from '../../../constants/ProfileSelectors';
import Colors from '../../../constants/Colors';
import {styles as pStyles, selectStyle} from '../_styles';

const closeIcon = require('../../../../assets/icons/glyph_close_32x32.png');

const N = 5;
const INTERVAL = 'months';

class TagCheckBox extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        {this.props.children}
      </TouchableOpacity>
    );
  }
}

class Personality extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: -1,
      selectedData: null,
      isChecked1: false,
      isChecked2: false,
      isModalVisible: false,
    };
  }

  toggleCheckBox = (index) => {
    let data =
      ProfileSelectors.type[0].mbti.source[0].categories[0].profileTypes[index];
    let found = this._findSaved(data.mainTypeShortName);
    data.subType = found ? found.subType : '';

    this.setState({
      selectedIndex: index === this.state.selectedIndex ? -1 : index,
      selectedData: data,
    });
  };

  onChangeValue = (type, value) => {
    let selectedData = JSON.parse(JSON.stringify(this.state.selectedData));
    selectedData[type] = value;
    this.setState({selectedData});
  };

  onToggleModal = () => {
    this.setState({
      isChecked1: false,
      isChecked2: false,
      isModalVisible: !this.state.isModalVisible,
    });
  };

  onToggleChecked = (index) => {
    let state = {...this.state};
    state[`isChecked${index}`] = !state[`isChecked${index}`];
    this.setState(state);
  };

  onSave = () => {
    const {selectedData} = this.state;
    const profiles = {...this.props.profiles};

    let found = this._findSaved(selectedData.mainTypeShortName);
    if (found) {
      profiles.type[0].mbti.map((item) => {
        if (item.mainType === selectedData.mainTypeShortName) {
          item.subType = selectedData.subType;
          item.savedDate = moment().format('YYYY-MM-DD');
        }
        return item;
      });
    } else {
      profiles.type[0].mbti.push({
        source: '16Personalities',
        category: 'analysts',
        mainType: selectedData.mainTypeShortName,
        subType: selectedData.subType,
        savedDate: moment().format('YYYY-MM-DD'),
      });
    }

    this.props.setSettings({type: 'PROFILES', value: profiles});
    this.onToggleModal();
  };

  _findSaved = (mainTypeShortName) => {
    const {profiles} = this.props;
    let found = null;
    if (profiles.type[0].mbti.length > 0) {
      found = profiles.type[0].mbti.find((item) => {
        return item.mainType === mainTypeShortName;
      });

      if (typeof found === 'undefined') {
        found = null;
      }
    }
    return found;
  };

  render() {
    const {
      selectedIndex,
      selectedData,
      isModalVisible,
      isChecked1,
      isChecked2,
    } = this.state;
    const profileTypes =
      ProfileSelectors.type[0].mbti.source[0].categories[0].profileTypes;

    return (
      <SafeAreaView style={{flex: 1}}>
        <ScrollView style={{flex: 1}}>
          {profileTypes.map((type, index) => {
            let SELECT_SUBTYPES = [{label: 'None', value: ''}];
            const disabled = index !== selectedIndex;
            type.subTypes.forEach((item) => {
              SELECT_SUBTYPES.push({
                label: item.subTypeLabel,
                value: item.subTypeShortName,
              });
            });

            const isThis =
              selectedData &&
              type.mainTypeShortName === selectedData.mainTypeShortName;
            const saved = this._findSaved(type.mainTypeShortName);

            return (
              <View
                style={[
                  styles.typeRow,
                  saved && {backgroundColor: '#d0f3df'},
                  !disabled && {backgroundColor: `${Colors.scheme.blue}15`},
                ]}
                key={`personality-${type.mainTypeShortName}`}>
                {/* <View style={styles.savedStamp}>
                  <Text style={styles.savedStampText}>Saved</Text>
                </View> */}
                <View style={styles.checkBoxWrapper}>
                  <CheckBox
                    Component={TagCheckBox}
                    checked={index === selectedIndex}
                    checkedColor={Colors.scheme.blue}
                    uncheckedColor={Colors.scheme.blue}
                    onPress={() => this.toggleCheckBox(index)}
                  />
                </View>
                <Image source={type.mainTypeImage} style={styles.typeImg} />
                <View style={{flex: 1, paddingLeft: 10}}>
                  <View style={styles.nameCodeRow}>
                    <Text style={[styles.typeName, disabled && {opacity: 0.8}]}>
                      {type.mainTypeLabel}
                    </Text>
                  </View>
                  <View
                    style={[
                      styles.dropdownWrapper,
                      {borderColor: disabled ? '#ccc' : '#666'},
                    ]}>
                    <RNPickerSelect
                      disabled={disabled}
                      placeholder={{}}
                      items={SELECT_SUBTYPES}
                      onValueChange={(v) => this.onChangeValue('subType', v)}
                      value={
                        isThis
                          ? selectedData.subType
                          : saved
                          ? saved.subType
                          : ''
                      }
                      style={selectStyle}
                      useNativeAndroidPickerStyle={false}
                      textInputProps={disabled ? {color: 'gray'} : {}}
                      Icon={() => {
                        return (
                          <Chevron
                            size={1.5}
                            color={disabled ? '#ccc' : '#666'}
                          />
                        );
                      }}
                    />
                  </View>
                  {isThis && selectedData.subType !== '' && (
                    <TouchableOpacity
                      style={[pStyles.analyzeNowBtn, {width: 90}]}
                      onPress={this.onToggleModal}>
                      <Text style={pStyles.analyzeNowText}>Submit</Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            );
          })}
        </ScrollView>

        <ModalWrapper
          containerStyle={pStyles.modalContainer}
          style={[
            pStyles.modal,
            {paddingTop: 30, paddingBottom: 20, paddingHorizontal: 20},
          ]}
          onRequestClose={this.onToggleModal}
          shouldAnimateOnRequestClose={true}
          shouldCloseOnOverlayPress={true}
          visible={isModalVisible}
          position={'top'}>
          <View style={{flexDirection: 'row'}}>
            <CheckBox
              Component={TagCheckBox}
              checked={isChecked1}
              checkedColor={Colors.scheme.blue}
              uncheckedColor={Colors.scheme.blue}
              onPress={() => this.onToggleChecked(1)}
            />
            <Text style={styles.modalText}>
              I understand that this value is considered 'permanent' and can
              only be changed once every {N} {INTERVAL}.
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <CheckBox
              Component={TagCheckBox}
              checked={isChecked2}
              checkedColor={Colors.scheme.blue}
              uncheckedColor={Colors.scheme.blue}
              onPress={() => this.onToggleChecked(2)}
            />
            <Text style={styles.modalText}>
              I certify that this personality type was validated with a test
              that I took through a certified testing provider.
            </Text>
          </View>
          {isChecked1 && isChecked2 && (
            <View style={{alignItems: 'center', marginTop: 5}}>
              <TouchableOpacity
                style={[pStyles.analyzeNowBtn, {width: 90}]}
                onPress={this.onSave}>
                <Text style={pStyles.analyzeNowText}>Save</Text>
              </TouchableOpacity>
            </View>
          )}

          <TouchableOpacity
            style={pStyles.closeIconBtn}
            onPress={this.onToggleModal}>
            <Image source={closeIcon} />
          </TouchableOpacity>
        </ModalWrapper>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tags: state.SettingReducer.tags,
    profiles: state.SettingReducer.profiles,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Personality);
