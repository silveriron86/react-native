import {StyleSheet} from 'react-native';
import Colors from '../../../constants/Colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  typeRow: {
    flexDirection: 'row',
    // alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    padding: 10,
  },
  typeImg: {
    width: 100,
    height: 100,
  },
  nameCodeRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  typeName: {
    color: Colors.scheme.blue,
    fontSize: 20,
  },
  typeCode: {
    fontSize: 20,
    color: '#576071',
    marginLeft: 10,
  },
  typeDescription: {
    fontSize: 14,
  },
  dropdownWrapper: {
    borderWidth: 1,
    borderColor: '#ccc',
    maxWidth: 110,
    marginVertical: 4,
  },
  savedStamp: {
    position: 'absolute',
    right: 0,
    top: 0,
    backgroundColor: '#2BC46C',
    padding: 5,
    borderRadius: 3,
    margin: 2,
  },
  savedStampText: {
    color: 'white',
    fontSize: 12,
  },
  modalText: {
    flex: 1,
    paddingLeft: 10,
    fontSize: 14,
    color: '#333',
  },
});
