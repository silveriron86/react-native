/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {Text, View, ScrollView} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {Chevron} from 'react-native-shapes';
import {styles, selectStyle, selectTextInputStyle} from './_styles';
import SettingConstants from '../../constants/SettingConstants';
import {SettingActions} from '../../actions';
import Locker from '../../widgets/Locker';
import * as RootNavigation from '../../navigation/RootNavigation';

// const SELECT_NOTE_TYPES = [
//     { label: 'Standard', value: '' },
//     { label: '3Q', value: '3Q' },
//     { label: 'Rest Up', value: 'sleep' },
//     { label: 'Flow', value: 'rating' }
// ]

class Visibility extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      standardCanIndex: 0,
      threeQCanIndex: 0,
      restUpCanIndex: 0,
      flowCanIndex: 0,
    };
  }

  _setValue = (field, index, value) => {
    const {noteVisibility} = this.props;
    let data = JSON.parse(JSON.stringify(noteVisibility));
    data[field][index] = value;
    this.props.setSettings({type: 'NOTE_VISIBILITY', value: data});
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.passcode !== nextProps.passcode) {
      RootNavigation.setParams({
        locked: nextProps.passcode.locked && nextProps.passcode.enabled,
      });
    }
  }

  componentDidMount() {
    RootNavigation.setParams({
      locked: this.props.passcode.locked && this.props.passcode.enabled,
    });
  }

  render() {
    const {noteVisibility} = this.props;

    return (
      <Locker {...this.props}>
        <ScrollView style={{width: '100%', backgroundColor: 'white'}}>
          {/* <View style={styles.sectionHeader}>
            <Text style={styles.sectionHeaderText}>Note Type Visibility</Text>
          </View>         */}

          <View style={[styles.row, {borderBottomWidth: 0, paddingTop: 20}]}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {fontSize: 16}]}>Standard</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {marginLeft: 15}]}>
                Can See Stream Images:
              </Text>
            </View>
            <RNPickerSelect
              placeholder={{}}
              items={SettingConstants.SELECT_VISIBLE_USERS}
              onValueChange={(v) => {
                this._setValue('standard', 0, v);
              }}
              style={selectStyle}
              value={noteVisibility.standard[0]}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>
          <View style={styles.row}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {marginLeft: 15}]}>
                Can Read Notes:
              </Text>
            </View>
            <RNPickerSelect
              placeholder={{}}
              items={SettingConstants.SELECT_VISIBLE_USERS}
              onValueChange={(v) => {
                this._setValue('standard', 1, v);
              }}
              style={selectStyle}
              value={noteVisibility.standard[1]}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>

          <View style={[styles.row, {borderBottomWidth: 0, paddingTop: 20}]}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {fontSize: 16}]}>3Q</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {marginLeft: 15}]}>
                Can See Stream Images:
              </Text>
            </View>
            <RNPickerSelect
              placeholder={{}}
              items={SettingConstants.SELECT_VISIBLE_USERS}
              onValueChange={(v) => {
                this._setValue('3Q', 0, v);
              }}
              style={selectStyle}
              value={noteVisibility['3Q'][0]}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>
          <View style={styles.row}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {marginLeft: 15}]}>
                Can Read Notes:
              </Text>
            </View>
            <RNPickerSelect
              placeholder={{}}
              items={SettingConstants.SELECT_VISIBLE_USERS}
              onValueChange={(v) => {
                this._setValue('3Q', 1, v);
              }}
              style={selectStyle}
              value={noteVisibility['3Q'][1]}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>

          <View style={[styles.row, {borderBottomWidth: 0, paddingTop: 20}]}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {fontSize: 16}]}>Flow</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {marginLeft: 15}]}>
                Can See Stream Images:
              </Text>
            </View>
            <RNPickerSelect
              placeholder={{}}
              items={SettingConstants.SELECT_VISIBLE_USERS}
              onValueChange={(v) => {
                this._setValue('rating', 0, v);
              }}
              style={selectStyle}
              value={noteVisibility['rating'][0]}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>
          <View style={styles.row}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {marginLeft: 15}]}>
                Can Read Notes:
              </Text>
            </View>
            <RNPickerSelect
              placeholder={{}}
              items={SettingConstants.SELECT_VISIBLE_USERS}
              onValueChange={(v) => {
                this._setValue('rating', 1, v);
              }}
              style={selectStyle}
              value={noteVisibility['rating'][1]}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>

          <View style={[styles.row, {borderBottomWidth: 0, paddingTop: 20}]}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {fontSize: 16}]}>Rest Up</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {marginLeft: 15}]}>
                Can See Stream Images:
              </Text>
            </View>
            <RNPickerSelect
              placeholder={{}}
              items={SettingConstants.SELECT_VISIBLE_USERS}
              onValueChange={(v) => {
                this._setValue('sleep', 0, v);
              }}
              style={selectStyle}
              value={noteVisibility['sleep'][0]}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>
          <View style={styles.row}>
            <View style={styles.switchLabel}>
              <Text style={[styles.labelText, {marginLeft: 15}]}>
                Can Read Notes:
              </Text>
            </View>
            <RNPickerSelect
              placeholder={{}}
              items={SettingConstants.SELECT_VISIBLE_USERS}
              onValueChange={(v) => {
                this._setValue('sleep', 1, v);
              }}
              style={selectStyle}
              value={noteVisibility['sleep'][1]}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>
        </ScrollView>
      </Locker>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    noteVisibility: state.SettingReducer.noteVisibility,
    passcode: state.SettingReducer.passcode,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Visibility);
