/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Slider from 'react-native-slider';
import {SettingActions} from '../../actions';
import {styles} from './_styles';
import Utils from '../../utils';
import Colors from '../../constants/Colors';

const AVAILABLE_FONTS = [
  {
    name: 'PremiumUltra26',
    text: 'Clearly this font is the best.',
  },
  {
    name: 'PremiumUltra5',
    text: "Umm, right. And that's why I'm the default.",
  },
  {
    name: 'PremiumUltra3',
    text: 'Pick me! No please, pick me!!',
  },
  {
    name: 'PremiumUltra2',
    text: "Don't pick me. Seriously, it's ok.",
  },
  {
    name: 'PremiumUltra4',
    text: "Honestly, you don't know what you're talking about.",
  },
  {
    name: 'PremiumUltra1',
    text: 'Why am I always last ???',
  },
];

class FontEdit extends React.Component {
  onGo = () => {
    const {navigation, onClose} = this.props;
    if (onClose) {
      onClose();
      return;
    }

    AsyncStorage.getItem('FROM_SETTINGS', (_err, from) => {
      if (from === 'YES') {
        navigation.goBack();
      } else {
        navigation.navigate('TabScreen');
      }
    });
  };

  _renderBody = () => {
    const {
      streamFont,
      fontSize,
      setSettings,
      tagFont,
      tagFontSize,
      isModal,
    } = this.props;
    return (
      <View style={styles.fontEditScreen}>
        <View style={{height: 120, justifyContent: 'center'}}>
          <Text style={styles.pickAFontText}>Pick a font</Text>
        </View>
        <View style={styles.fontSelector}>
          {AVAILABLE_FONTS.map((f, i) => (
            <TouchableOpacity
              key={`font-${f.name}`}
              style={styles.fontSelectorBtn}
              onPress={() => {
                Utils.trackEvent('Action', 'User selects a font');
                if (isModal === true) {
                  this.props.setFont(f.name);
                } else {
                  setSettings({type: 'STREAM_FONT', value: f.name});
                }
              }}>
              <Text
                numberOfLines={1}
                style={[
                  {
                    fontFamily: f.name,
                    fontSize: isModal ? tagFontSize : fontSize,
                  },
                  {
                    color:
                      (isModal === true && f.name === tagFont) ||
                      (isModal !== true && f.name === streamFont)
                        ? '#29abe2'
                        : 'white',
                  },
                ]}>
                {f.text}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.sliderWrapper}>
          <Text style={styles.smallText}>Small</Text>
          <View style={{flex: 1, alignItems: 'stretch', padding: 5}}>
            <Slider
              value={isModal ? tagFontSize : fontSize}
              minimumValue={10}
              maximumValue={50}
              step={1}
              trackStyle={sliderStyle.track}
              thumbStyle={sliderStyle.thumb}
              minimumTrackTintColor="#0f1f27"
              maximumTrackTintColor="#0f1f27"
              onSlidingComplete={(value) => {
                if (this.props.isModal === true) {
                  this.props.setFontSize(value);
                } else {
                  setSettings({type: 'FONT_SIZE', value: value});
                }
              }}
            />
          </View>
          <Text style={styles.bigText}>Big</Text>
        </View>
        <View style={{height: 100}}>
          <TouchableOpacity style={styles.goBtn} onPress={this.onGo}>
            <Text style={[styles.pickAFontText, {fontSize: 25}]}>Go</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    const {isModal} = this.props;
    if (isModal === true) {
      return <View style={{flex: 1}}>{this._renderBody()}</View>;
    }
    return <SafeAreaView style={{flex: 1}}>{this._renderBody()}</SafeAreaView>;
  }
}

const mapStateToProps = (state) => {
  return {
    streamFont: state.SettingReducer.streamFont,
    fontSize: state.SettingReducer.fontSize,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FontEdit);

let sliderStyle = StyleSheet.create({
  track: {
    height: 4,
    borderRadius: 2,
  },
  thumb: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: Colors.scheme.blue,
  },
});
