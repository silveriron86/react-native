/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';
// import {StackActions, NavigationActions} from 'react-navigation';
import {StackActions} from '@react-navigation/native';
import {styles} from './_styles';
import Utils from '../../utils';
import Locker from '../../widgets/Locker';
import * as RootNavigation from '../../navigation/RootNavigation';

const avatarIcon = require('../../../assets/icons/avatar.png');

class Profile extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (this.props.passcode !== nextProps.passcode) {
      RootNavigation.setParams({
        locked: nextProps.passcode.locked && nextProps.passcode.enabled,
      });
    }
  }

  componentDidMount() {
    RootNavigation.setParams({
      locked: this.props.passcode.locked && this.props.passcode.enabled,
    });
  }

  handleLogOut(obj) {
    // const resetAction = StackActions.reset({
    //   index: 0,
    //   actions: [
    //     NavigationActions.navigate({
    //       routeName: 'Settings',
    //       params: {logout: true},
    //     }),
    //   ],
    // });
    const resetAction = StackActions.replace('Settings', {logout: true});
    obj.props.navigation.dispatch(resetAction);
  }

  render() {
    const {userData} = this.props;
    console.log(userData);
    if (!userData) {
      return null;
    }

    let type = '';
    if (
      typeof userData.type !== 'undefined' ||
      userData.name === 'Apple user'
    ) {
      type = 'Apple';
    } else {
      let tmp = userData.sub.split('|');
      type = Utils.capitalize(tmp[0].split('-')[0]);
    }

    return (
      <Locker {...this.props}>
        <ScrollView
          contentContainerStyle={{
            backgroundColor: '#f0eff4',
            alignItems: 'center',
          }}>
          <View style={{width: 120, marginVertical: 20}}>
            {userData.picture ? (
              <Image
                source={{uri: userData.picture}}
                style={styles.avatarLargeIcon}
              />
            ) : (
              <Image source={avatarIcon} style={styles.avatarLargeIcon} />
            )}
          </View>
          <Text style={{fontSize: 22}}>{userData.name}</Text>
          <Text
            style={{
              fontSize: 15,
              marginTop: 10,
              marginBottom: 5,
              paddingHorizontal: 20,
            }}>
            {userData.email}
          </Text>
          {userData.emailVerified && (
            <View
              style={{
                marginVertical: 5,
                backgroundColor: 'green',
                padding: 3,
                paddingHorizontal: 10,
                borderRadius: 15,
              }}>
              <Text style={{fontSize: 11, color: 'white'}}>Email Verified</Text>
            </View>
          )}

          <View style={[styles.row, {borderBottomWidth: 0}]}>
            <View style={styles.switchLabel}>
              <TouchableOpacity
                style={[styles.analyzeNowBtn, {marginTop: 0, height: 40}]}
                onPress={() => this.handleLogOut(this)}>
                <Text style={styles.logoutText}>Log Out</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{backgroundColor: 'white', width: '100%', marginTop: 10}}>
            <View style={styles.row}>
              <View style={styles.switchLabel}>
                <Text style={styles.profileLabelText}>
                  Authentication Provider
                </Text>
                <Text style={styles.profileValueText}>{type}</Text>
              </View>
            </View>
            {typeof userData.familyName !== 'undefined' &&
              userData.familyName !== '' && (
                <View style={styles.row}>
                  <View style={styles.switchLabel}>
                    <Text style={styles.profileLabelText}>Family Name</Text>
                    <Text style={styles.profileValueText}>
                      {userData.familyName}
                    </Text>
                  </View>
                </View>
              )}

            {typeof userData.givenName !== 'undefined' &&
              userData.givenName !== '' && (
                <View style={styles.row}>
                  <View style={styles.switchLabel}>
                    <Text style={styles.profileLabelText}>Given Name</Text>
                    <Text style={styles.profileValueText}>
                      {userData.givenName}
                    </Text>
                  </View>
                </View>
              )}

            <View style={[styles.row, {borderBottomWidth: 0}]}>
              <View style={styles.switchLabel}>
                <Text style={styles.profileLabelText}>Nickname</Text>
                <Text style={styles.profileValueText}>{userData.nickname}</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </Locker>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.CommonReducer.userData,
    passcode: state.SettingReducer.passcode,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
