/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  Text,
  View,
  Switch,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {TextInput} from 'react-native-gesture-handler';
import {Chevron} from 'react-native-shapes';
import DatePicker from 'react-native-datepicker';
import RNPickerSelect from 'react-native-picker-select';
import ModalWrapper from 'react-native-modal-wrapper';
import {SettingActions} from '../../actions';
import SettingConstants from '../../constants/SettingConstants';
import DummyData from '../../constants/DummyData';
import {styles, selectStyle, selectTextInputStyle} from './_styles';
import Utils from '../../utils';
import * as RootNavigation from '../../navigation/RootNavigation';

let SELECT_HOPE_LABELS = [];
let SELECT_ENERGY_LABELS = [];
let SELECT_DESCRIPTION_PACKS = [];
let SELECT_CLOUDS_MULTIPLIER = [];
for (let i = 1; i <= 10; i++) {
  SELECT_CLOUDS_MULTIPLIER.push({label: i.toString(), value: i});
}

const DATETIME_STYLE = {
  dateInput: {margin: 0, height: 30, marginTop: -10, paddingHorizontal: 5},
};

SettingConstants.HOPE_LABELS.forEach((label, i) => {
  SELECT_HOPE_LABELS.push({label: Utils.capitalize(label), value: label});
});
SettingConstants.ENERGY_LABELS.forEach((label, i) => {
  SELECT_ENERGY_LABELS.push({label: Utils.capitalize(label), value: label});
});
SettingConstants.FLOW_DESCRIPTION_PACKS.forEach((e, i) => {
  SELECT_DESCRIPTION_PACKS.push({label: `Pack ${i + 1}`, value: i});
});

class FlowJourney extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      isAdmin: false,
      isOpenedInfo: false,
    };
  }

  openInfo = () => {
    this.setState({
      isOpenedInfo: true,
    });
  };

  onCloseInfoModal = () => {
    this.setState({
      isOpenedInfo: false,
    });
  };

  _renderPackRowItem = (type, item, index) => {
    return (
      <View
        style={styles.modalSubRow}
        key={`description-pack-${type}-${index}`}>
        <View>
          <Text>
            {item.ratingMin} ~ {item.ratingMax}
          </Text>
        </View>
        <View>
          <Text>{item.descriptionText}</Text>
        </View>
      </View>
    );
  };

  _checkIfAdmin = () => {
    AsyncStorage.getItem('USER_ID', (_err, user_id) => {
      this.setState({
        isAdmin: user_id !== null && DummyData.adminUsers.includes(user_id),
      });
    });
  };

  componentWillReceiveProps(nextProps) {
    this._checkIfAdmin();
  }

  componentDidMount() {
    this._checkIfAdmin();
    this.changeOverrides();
  }

  changeOverrides = () => {
    /*
    const { flowJourney } = this.props
    if(flowJourney.override) {
      // overrides
      AsyncStorage.getItem('FLOW_OVERRIDES_UPDATED', (err, saved) => {
        if(saved) {
          let updatedValues = JSON.parse(saved)
          AsyncStorage.getItem('LAST_FLOW_ENTRY_SAVED', (err, lastSavedTime) => {
            if(lastSavedTime) {
              updatedValues.timeSinceLastFlowEntry = lastSavedTime
            }
            updatedValues.override = flowJourney.override
            this.props.setSettings({ type: 'FLOW_JOURNEY', value: updatedValues })
          })
        }else {
          this.initRealOverrides()
        }
      })
    }else {
      this.initRealOverrides()
    }*/
  };

  initRealOverrides = () => {
    // use real values
    AsyncStorage.getItem('LAST_FLOW_ENTRY_SAVED', (_err, lastSavedTime) => {
      let flowJourney = JSON.parse(JSON.stringify(this.props.flowJourney));
      flowJourney.timeSinceLastFlowEntry = lastSavedTime ? lastSavedTime : null;
      flowJourney.cloudsMultiplier = 1;
      flowJourney.cloudsStartAfterHours =
        SettingConstants.defaultFlowJourney.cloudsStartAfterHours;
      flowJourney.cloudsFullAfterHours =
        SettingConstants.defaultFlowJourney.cloudsFullAfterHours;
      this.props.setSettings({type: 'FLOW_JOURNEY', value: flowJourney});
    });
  };

  _setValue = (field, value) => {
    const {flowJourney} = this.props;
    this.saveLastOverrides();
    let data = JSON.parse(JSON.stringify(flowJourney));
    data[field] = value;

    if (field === 'enabled' && value === true) {
      if (flowJourney.timeSinceLastFlowEntry === null) {
        data.timeSinceLastFlowEntry = Utils.nowUTC(this.props);
      }
    }

    this.props.setSettings({type: 'FLOW_JOURNEY', value: data});

    setTimeout(() => {
      global.eventEmitter.emit('TOGGLE_FLOW_JOURNEY', {enabled: data.enabled});
    }, 500);
  };

  saveLastOverrides = () => {
    if (this.props.flowJourney.override) {
      setTimeout(() => {
        const {flowJourney} = this.props;
        AsyncStorage.setItem(
          'FLOW_OVERRIDES_UPDATED',
          JSON.stringify(flowJourney),
        );
      }, 10);
    }
  };

  onNewFlowLog = () => {
    RootNavigation.navigate('Flow');
  };

  render() {
    const {flowJourney, location, flowLabels, flowDescriptionPack} = this.props;
    const {isAdmin, isOpenedInfo} = this.state;

    let overridesEnabled = isAdmin && flowJourney.override;

    return (
      <ScrollView style={{flex: 1}}>
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>Enabled</Text>
          </View>
          <Switch
            value={flowJourney.enabled}
            onValueChange={(value) => {
              this._setValue('enabled', value);
            }}
          />
        </View>
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Labels
            </Text>
          </View>
          <View>
            <RNPickerSelect
              items={SELECT_HOPE_LABELS}
              onValueChange={(value) => {
                this.props.setSettings({
                  type: 'FLOW_LABELS',
                  value: {hope: value, energy: flowLabels.energy},
                });
              }}
              value={flowLabels.hope}
              style={selectStyle}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              placeholder={{}}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>
          <View style={{width: 20}} />
          <View>
            <RNPickerSelect
              items={SELECT_ENERGY_LABELS}
              onValueChange={(value) => {
                this.props.setSettings({
                  type: 'FLOW_LABELS',
                  value: {hope: flowLabels.hope, energy: value},
                });
              }}
              value={flowLabels.energy}
              style={selectStyle}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              placeholder={{}}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>
        </View>

        <View style={styles.row}>
          <View
            style={[
              styles.switchLabel,
              {flexDirection: 'row', alignItems: 'center'},
            ]}>
            <Text
              style={[styles.labelText, {minWidth: 0, marginRight: 15}]}
              numberOfLines={1}>
              Description Packs
            </Text>
            <TouchableOpacity onPress={this.openInfo}>
              <Image
                source={require('../../../assets/icons/info.png')}
                style={styles.infoIcon}
                resizeMethod="scale"
              />
            </TouchableOpacity>
          </View>
          <RNPickerSelect
            items={SELECT_DESCRIPTION_PACKS}
            onValueChange={(value) => {
              this.props.setSettings({
                type: 'FLOW_DESCRIPTION_PACK',
                value: value,
              });
            }}
            value={flowDescriptionPack}
            style={selectStyle}
            useNativeAndroidPickerStyle={false}
            textInputProps={selectTextInputStyle}
            placeholder={{}}
            Icon={() => {
              return <Chevron size={1.5} color="gray" />;
            }}
          />
        </View>
        <View style={[styles.row, {flexDirection: 'column'}]}>
          <Text style={styles.flowLogText}>
            {flowJourney.timeSinceLastFlowEntry
              ? `Your last Flow log was at ${Utils.toTimezone(
                  flowJourney.timeSinceLastFlowEntry,
                  location,
                )}`
              : "You don't have any Flow logs yet."}
          </Text>
          <View style={{width: '100%', marginTop: 10}}>
            <TouchableOpacity
              style={[styles.analyzeNowBtn, {marginTop: 0, height: 40}]}
              onPress={this.onNewFlowLog}>
              <Text style={styles.logoutText}>New Flow Log</Text>
            </TouchableOpacity>
          </View>
          <Text style={styles.flowLogText2}>(literally takes 2 seconds)</Text>
          <Text style={styles.flowLogText3}>
            Forecast:
            {Utils.getFlowPastHours(flowJourney.timeSinceLastFlowEntry) >
            flowJourney.cloudsStartAfterHours ? (
              <Text>
                {' '}
                Getting a little cloudy on the logging front. Clear it up with a
                new entry.
              </Text>
            ) : (
              <Text>
                {' '}
                Clouds will start moving in about{' '}
                {flowJourney.cloudsStartAfterHours} hours from now.
              </Text>
            )}
          </Text>
        </View>

        <ModalWrapper
          containerStyle={styles.modalContainer}
          style={styles.modal}
          onRequestClose={this.onCloseInfoModal}
          shouldAnimateOnRequestClose={true}
          shouldCloseOnOverlayPress={true}
          visible={isOpenedInfo}
          position={'top'}>
          <View style={styles.modalInner}>
            <Text style={styles.modalSubTitle}>
              {flowLabels.hope.toUpperCase()}
            </Text>
            {SettingConstants.FLOW_DESCRIPTION_PACKS[
              flowDescriptionPack
            ].hope.map((item, index) => {
              return this._renderPackRowItem('hope', item, index);
            })}
            <Text style={styles.modalSubTitle}>
              {flowLabels.energy.toUpperCase()}
            </Text>
            {SettingConstants.FLOW_DESCRIPTION_PACKS[
              flowDescriptionPack
            ].energy.map((item, index) => {
              return this._renderPackRowItem('energy', item, index);
            })}
          </View>
        </ModalWrapper>

        {/* <View style={styles.sepLine}></View>
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>Admin Flow</Text>
        </View>   */}

        {isAdmin && (
          <>
            <View style={styles.sepLine} />
            <View style={styles.sectionHeader}>
              <Text style={styles.sectionHeaderText}>Admin</Text>
            </View>
            <View style={styles.row}>
              <View style={styles.switchLabel}>
                <Text style={styles.labelText}>Overrides</Text>
              </View>
              <Switch
                value={flowJourney.override}
                onValueChange={(value) => {
                  this._setValue('override', value);
                  setTimeout(() => {
                    this.changeOverrides();
                  }, 10);
                }}
              />
            </View>
          </>
        )}

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Clouds Multiplier
            </Text>
          </View>
          <View>
            <RNPickerSelect
              disabled={!overridesEnabled}
              placeholder={{}}
              items={SELECT_CLOUDS_MULTIPLIER}
              onValueChange={(value) => {
                this._setValue('cloudsMultiplier', value);
              }}
              style={selectStyle}
              useNativeAndroidPickerStyle={false}
              textInputProps={selectTextInputStyle}
              value={flowJourney.cloudsMultiplier}
              Icon={() => {
                return <Chevron size={1.5} color="gray" />;
              }}
            />
          </View>
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Time since last flow entry
            </Text>
          </View>
          <DatePicker
            disabled={!overridesEnabled}
            style={{
              width: 134,
              height: 30,
              backgroundColor: overridesEnabled ? 'white' : '#efefef',
            }}
            mode="datetime"
            placeholder=" "
            format="YYYY-MM-DD HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={DATETIME_STYLE}
            date={
              flowJourney.timeSinceLastFlowEntry
                ? Utils.toTimezone(flowJourney.timeSinceLastFlowEntry, location)
                : null
            }
            onDateChange={(saved) => {
              this._setValue('timeSinceLastFlowEntry', Utils.toUTC(saved));
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Clouds start after N hours
            </Text>
          </View>
          <TextInput
            keyboardType={'number-pad'}
            value={flowJourney.cloudsStartAfterHours.toString()}
            style={[styles.snoozeText, !overridesEnabled && styles.readOnly]}
            editable={overridesEnabled}
            onChangeText={(v) => {
              this._setValue('cloudsStartAfterHours', v);
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Clouds full after N hours
            </Text>
          </View>
          <TextInput
            keyboardType={'number-pad'}
            value={flowJourney.cloudsFullAfterHours.toString()}
            style={[styles.snoozeText, !overridesEnabled && styles.readOnly]}
            editable={overridesEnabled}
            onChangeText={(v) => {
              this._setValue('cloudsFullAfterHours', v);
            }}
          />
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    flowLabels: state.SettingReducer.flowLabels,
    flowDescriptionPack: state.SettingReducer.flowDescriptionPack,
    location: state.SettingReducer.location,
    overrideSystemTime: state.SettingReducer.overrideSystemTime,
    tempSystemDateTime: state.SettingReducer.tempSystemDateTime,
    flowJourney: state.SettingReducer.flowJourney,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlowJourney);
