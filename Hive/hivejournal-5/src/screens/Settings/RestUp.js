/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {Text, View, Switch} from 'react-native';
import moment from 'moment';
import RNPickerSelect from 'react-native-picker-select';
import {Chevron} from 'react-native-shapes';
import DatePicker from 'react-native-datepicker';
import {styles, selectStyle, selectTextInputStyle} from './_styles';
import {SettingActions} from '../../actions';
import CustomSelector from '../../widgets/CustomSelector';
import Utils from '../../utils';

const DATETIME_STYLE = {
  dateInput: {margin: 0, height: 30, marginTop: -10, paddingHorizontal: 5},
};

let SELECT_CHECK_INS = [];
let i = 0;
for (i = 0; i < 24; i++) {
  let t = moment().hour(i);
  SELECT_CHECK_INS.push({label: t.format('ha'), value: i});
}

let SELECT_SLEEP_AMOUNTS = [];
for (i = 6; i <= 10; i++) {
  SELECT_SLEEP_AMOUNTS.push({label: `${i.toString()}h`, value: i});
}

class RestUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenedInfo: false,
    };
  }

  _setValue = (field, value) => {
    const {restUpJourney} = this.props;
    let data = JSON.parse(JSON.stringify(restUpJourney));
    data[field] = value;
    this.props.setSettings({type: 'REST_UP_JOURNEY', value: data});
  };

  render() {
    const {restUpJourney} = this.props;
    console.log(restUpJourney);

    return (
      <View style={{width: '100%'}}>
        {/* <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>Rest Up</Text>
        </View>         */}
        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Rest Up Journey
            </Text>
          </View>
          <View>
            <Switch
              value={restUpJourney.enabled}
              onValueChange={(v) => {
                this._setValue('enabled', v);
              }}
            />
          </View>
        </View>
        {/* <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines = {1}>Check in at</Text>
          </View>
          <DatePicker
            style={{width: 50, height: 30, backgroundColor: restUpJourney.enabled ? 'white' : '#efefef'}} mode="time" placeholder="" format="HH:mm" confirmBtnText="Confirm" cancelBtnText="Cancel" showIcon={false} customStyles={DATETIME_STYLE}
            date={restUpJourney.checkInAt}
            disabled={!restUpJourney.enabled}
            onValueChange={(v) => {
              this._setValue('checkInAt', v);
            }}/>
        </View>  */}

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>Check in at</Text>
          </View>
          <RNPickerSelect
            placeholder={{}}
            items={SELECT_CHECK_INS}
            onValueChange={(v) => {
              this._setValue('checkInAt', v);
            }}
            style={selectStyle}
            value={restUpJourney.checkInAt}
            useNativeAndroidPickerStyle={false}
            textInputProps={selectTextInputStyle}
            Icon={() => {
              return <Chevron size={1.5} color="gray" />;
            }}
          />
        </View>

        <View style={[styles.row, {paddingVertical: 3}]}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>Guide</Text>
          </View>
          <CustomSelector
            value={restUpJourney.guide}
            onValueChange={(v) => {
              this._setValue('guide', v);
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText}>Ideal amount of sleep</Text>
          </View>
          <RNPickerSelect
            placeholder={{}}
            items={SELECT_SLEEP_AMOUNTS}
            value={restUpJourney.sleepAmount}
            onValueChange={(v) => {
              this._setValue('sleepAmount', v);
            }}
            style={selectStyle}
            useNativeAndroidPickerStyle={false}
            textInputProps={selectTextInputStyle}
            Icon={() => {
              return <Chevron size={1.5} color="gray" />;
            }}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Ideal time to wake up
            </Text>
          </View>
          <DatePicker
            style={{width: 50, height: 30}}
            mode="time"
            placeholder=""
            format="HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={DATETIME_STYLE}
            date={restUpJourney.wakeTime}
            onDateChange={(v) => {
              console.log(v);
              this._setValue('wakeTime', v);
            }}
          />
        </View>

        <View style={[styles.row, {height: 50}]}>
          <View style={styles.switchLabel}>
            <Text style={styles.labelText} numberOfLines={1}>
              Ideal time to get to bed
            </Text>
          </View>
          <Text
            style={[styles.labelText, {textAlign: 'right'}]}
            numberOfLines={1}>
            {Utils.getIdealBedTime(
              restUpJourney.wakeTime,
              restUpJourney.sleepAmount,
            )}
          </Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    restUpJourney: state.SettingReducer.restUpJourney,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RestUp);
