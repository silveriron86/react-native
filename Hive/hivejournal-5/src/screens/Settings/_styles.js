import {StyleSheet} from 'react-native';
import Colors from '../../constants/Colors';

export const styles = StyleSheet.create({
  whiteBG: {
    backgroundColor: 'white',
  },
  branchContainer: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
    // marginTop: 50,
    // justifyContent: "center"
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    paddingVertical: 10,
    marginHorizontal: 20,
  },
  personalityRow: {
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    paddingVertical: 10,
    marginHorizontal: 20,
  },
  switchLabel: {
    flex: 1,
    minWidth: 100,
  },
  labelText: {
    textAlign: 'left',
    minWidth: 100,
  },
  analyzeNowBtn: {
    marginTop: 10,
    backgroundColor: Colors.scheme.blue,
    height: 30,
    paddingHorizontal: 20,
    justifyContent: 'center',
    borderRadius: 3,
    alignItems: 'center',
  },
  analyzeNowText: {
    color: 'white',
    textAlign: 'center',
  },
  logoutText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  sectionHeader: {
    marginLeft: 10,
    paddingTop: 25,
    paddingLeft: 10,
    marginBottom: 0,
  },
  sectionHeaderText: {
    fontSize: 20,
  },
  snoozeText: {
    borderWidth: 1,
    borderColor: 'gray',
    width: 80,
    height: 30,
    textAlign: 'right',
    paddingHorizontal: 5,
  },
  readOnly: {
    backgroundColor: '#efefef',
    color: '#333',
  },
  infoIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  modalContainer: {
    margin: 20,
  },
  modal: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: '#ffffff',
  },
  modalInner: {
    padding: 20,
  },
  modalSubTitle: {
    // fontFamily: 'PremiumUltra5',
    color: Colors.scheme.blue,
    fontSize: 20,
    marginTop: 10,
    marginBottom: 5,
  },
  modalSubRow: {
    marginBottom: 10,
    paddingBottom: 3,
  },
  sepLine: {
    height: 20,
    width: '100%',
    backgroundColor: '#f0eff4',
    marginTop: -1,
  },
  avatarIcon: {
    width: 60,
    height: 60,
    borderRadius: 30,
    resizeMode: 'contain',
  },
  avatarLargeIcon: {
    width: 150,
    height: 150,
    borderRadius: 75,
    resizeMode: 'contain',
  },
  profileRow: {
    borderBottomWidth: 0,
    marginBottom: 10,
    height: 65,
    justifyContent: 'space-between',
  },
  profileLabelText: {
    color: '#666',
  },
  profileValueText: {
    fontSize: 16,
    marginTop: 3,
  },
  journeysContainer: {
    backgroundColor: '#606060',
  },
  journeyImgRow: {
    marginHorizontal: 10,
    marginBottom: 10,
  },
  journeyImg: {
    width: '100%',
    height: 220,
    resizeMode: 'cover',
  },
  journeyRowFooter: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 35,
    backgroundColor: '#00000090',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  journeyRowTitle: {
    fontFamily: 'PremiumUltra5',
    color: '#a5a0a1',
    fontSize: 18,
  },
  flowLogText: {
    color: Colors.scheme.blue,
  },
  flowLogText2: {
    fontFamily: 'PremiumUltra5',
    fontSize: 14,
    marginTop: 3,
  },
  flowLogText3: {
    marginTop: 10,
    fontFamily: 'PremiumUltra5',
    fontSize: 18,
    color: Colors.scheme.blue,
  },
  mainTagText: {
    fontFamily: 'PremiumUltra26',
    color: Colors.scheme.blue,
    fontSize: 21,
  },
  tagText: {
    fontFamily: 'PremiumUltra26',
    fontSize: 20,
    marginRight: 10,
  },
  childTags: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  tagRowWrapper: {
    marginHorizontal: 20,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  tagsRow: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  checkBoxWrapper: {
    width: 40,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  tagCatText: {
    // textAlign: 'center',
    textTransform: 'capitalize',
    fontSize: 16,
  },
  categoriesView: {
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 12,
  },
  categoriesText: {
    fontSize: 18,
    color: Colors.scheme.blue,
  },
  btn: {
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    color: 'white',
  },
  btnImg: {
    width: 20,
    height: 20,
    resizeMode: 'stretch',
  },
  categoryView: {
    paddingVertical: 8,
    paddingHorizontal: 20,
    backgroundColor: '#efefef',
    height: 'auto',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  txtInput: {
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    fontSize: 14,
  },
  editTagRow: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
  },
  tableHeader: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#efefef',
    paddingHorizontal: 10,
  },
  tableHeaderText: {
    textAlign: 'center',
  },
  tableRow: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: '#eee',
    paddingHorizontal: 10,
    height: 40,
    alignItems: 'center',
  },
  flowTagDelBtn: {
    marginLeft: 5,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  editFlowModal: {
    padding: 20,
  },
  editFlowModalRow: {
    paddingHorizontal: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  editFlowModalTitle: {
    fontFamily: 'PremiumUltra26',
    color: Colors.scheme.blue,
    fontSize: 25,
    marginBottom: 5,
    textAlign: 'center',
  },
  newTagInput: {
    flex: 1,
    height: 30,
    marginLeft: 15,
    borderWidth: 1,
    borderColor: '#999',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  flowCategoryText: {
    fontFamily: 'PremiumUltra26',
    color: Colors.scheme.blue,
    fontSize: 22,
    marginTop: 7,
  },
  fontEditScreen: {
    backgroundColor: 'black',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  pickAFontText: {
    fontFamily: 'PremiumUltra5',
    color: '#29abe2',
    fontSize: 32,
  },
  sliderWrapper: {
    height: 130,
    width: '100%',
    alignItems: 'center',
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  smallText: {
    fontFamily: 'PremiumUltra5',
    color: Colors.scheme.blue,
    fontSize: 15,
  },
  bigText: {
    fontFamily: 'PremiumUltra5',
    color: Colors.scheme.blue,
    fontSize: 35,
  },
  goBtn: {
    borderColor: '#29abe2',
    borderWidth: 1,
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 0,
    width: 100,
    alignItems: 'center',
  },
  fontSelector: {
    flex: 1,
    width: '100%',
  },
  fontSelectorBtn: {
    paddingVertical: 3,
    width: '100%',
    paddingHorizontal: 10,
  },
  selectedBorder: {
    position: 'absolute',
    width: 95,
    height: 65,
    borderWidth: 4,
    borderColor: Colors.scheme.blue,
    margin: 0,
  },
  colImg: {
    width: 90,
    height: 60,
    resizeMode: 'cover',
    margin: 2,
  },
  coverSelector: {
    width: '100%',
    // height: 90,
    padding: 2,
    backgroundColor: 'rgba(0, 0, 0, .8)',
  },
  coverVideoImg: {
    width: 80,
    height: 45,
    marginHorizontal: 5,
  },
  coverOptions: {
    marginTop: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
  },
  selectedOption: {
    borderWidth: 3,
    borderColor: Colors.scheme.blue,
  },
  pnModal: {
    padding: 20,
  },
  pnModalTitle: {
    // fontFamily: 'PremiumUltra26',
    color: Colors.scheme.blue,
    fontSize: 22,
    marginBottom: 5,
    textAlign: 'center',
  },
  modalCloseButton: {
    position: 'absolute',
    right: 10,
    top: 10,
    backgroundColor: 'black',
    borderRadius: 15,
    width: 30,
    height: 30,
  },
  closeIcon: {
    width: 30,
    height: 30,
  },
  pnMask: {
    borderWidth: 1,
    borderColor: '#aaa',
    padding: 5,
    fontSize: 15,
  },
  termsText: {
    color: Colors.scheme.blue,
    textDecorationLine: 'underline',
  },
  linkText: {
    color: Colors.scheme.blue,
    marginLeft: 10,
  },
  termsTextRow: {
    flexDirection: 'row',
    width: '100%',
    borderWidth: 1,
    borderColor: 'white',
    marginTop: 3,
  },
  closeIconBtn: {
    position: 'absolute',
    right: -10,
    top: -10,
    backgroundColor: 'black',
    borderRadius: 16,
  },
  exampleText: {
    fontFamily: 'PremiumUltra5',
    color: 'gray',
  },
  goPremiumBtn: {
    width: '100%',
    backgroundColor: Colors.scheme.blue,
    borderRadius: 10,
  },
  goPremiumBtnText: {
    fontFamily: 'PremiumUltra5',
    color: 'white',
    fontSize: 25,
    textAlign: 'center',
  },
});

export const selectStyle = {
  viewContainer: {
    paddingVertical: 5,
    maxWidth: 200,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
  },
  inputAndroid: {
    // backgroundColor: 'white',
  },
  iconContainer: {
    top: 5,
    right: 0,
  },
};

export const selectTextInputStyle = {
  underlineColorAndroid: 'cyan',
  marginRight: 20,
};
