/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {connect} from 'react-redux';
import {styles} from './_styles';
import ImageConstants from '../../constants/ImageConstants';
import {SettingActions} from '../../actions';
import Utils from '../../utils';

const lockIcon = require('../../../assets/icons/lock_b.png');
const flowBgImg = require('../../../assets/images/flow_background_full.jpg');

class Journeys extends React.Component {
  _setValue = (field, value) => {
    const {restUpJourney} = this.props;
    let data = JSON.parse(JSON.stringify(restUpJourney));
    data[field] = value;
    this.props.setSettings({type: 'REST_UP_JOURNEY', value: data});
  };

  render() {
    const {
      restUpJourney,
      threeQJourneyEnabled,
      flowJourney,
      navigation,
    } = this.props;
    console.log(restUpJourney);
    return (
      <SafeAreaView style={{flex: 1}}>
        <ScrollView style={styles.journeysContainer}>
          <View style={[styles.journeyImgRow, {marginTop: 10}]}>
            <TouchableOpacity onPress={() => navigation.navigate('RestUp')}>
              <Image
                source={ImageConstants.periods.sunrise[1]}
                style={styles.journeyImg}
              />
            </TouchableOpacity>
            <View style={styles.journeyRowFooter}>
              <Text style={styles.journeyRowTitle}>REST UP</Text>
              <Switch
                value={restUpJourney.enabled}
                style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
                onValueChange={(v) => {
                  this._setValue('enabled', v);
                }}
              />
            </View>
          </View>

          <View style={styles.journeyImgRow}>
            <Image
              source={ImageConstants.periods.night[2]}
              style={styles.journeyImg}
            />
            <View style={styles.journeyRowFooter}>
              <Text style={styles.journeyRowTitle}>Morning mantra</Text>
              <Image
                source={lockIcon}
                style={{
                  width: 30,
                  height: 25,
                  tintColor: 'white',
                  resizeMode: 'stretch',
                }}
              />
            </View>
          </View>

          <View style={styles.journeyImgRow}>
            <TouchableOpacity
              onPress={() => navigation.navigate('FlowJourney')}>
              <Image source={flowBgImg} style={styles.journeyImg} />
            </TouchableOpacity>
            <View style={styles.journeyRowFooter}>
              <Text style={styles.journeyRowTitle}>Flow</Text>
              <Switch
                value={flowJourney.enabled}
                style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
                onValueChange={(v) => {
                  let data = JSON.parse(JSON.stringify(flowJourney));
                  data.enabled = v;
                  if (v === true) {
                    if (flowJourney.timeSinceLastFlowEntry === null) {
                      data.timeSinceLastFlowEntry = Utils.nowUTC(this.props);
                    }
                  }
                  console.log('*** Changed flowJourney enabled');
                  console.log(data);
                  this.props.setSettings({type: 'FLOW_JOURNEY', value: data});
                  setTimeout(() => {
                    global.eventEmitter.emit('TOGGLE_FLOW_JOURNEY', {
                      enabled: v,
                    });
                  }, 500);
                }}
              />
            </View>
          </View>

          <View style={styles.journeyImgRow}>
            <TouchableOpacity onPress={() => navigation.navigate('ThreeQ')}>
              <Image
                source={ImageConstants.periods.sunset[3]}
                style={styles.journeyImg}
              />
            </TouchableOpacity>
            <View style={styles.journeyRowFooter}>
              <Text style={styles.journeyRowTitle}>3 Questions</Text>
              <Switch
                value={threeQJourneyEnabled}
                style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
                onValueChange={(v) => {
                  this.props.setSettings({
                    type: '3Q_JOURNEY_ENABLED',
                    value: v,
                  });
                }}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    restUpJourney: state.SettingReducer.restUpJourney,
    threeQJourneyEnabled: state.SettingReducer.threeQJourneyEnabled,
    flowJourney: state.SettingReducer.flowJourney,
    overrideSystemTime: state.SettingReducer.overrideSystemTime,
    tempSystemDateTime: state.SettingReducer.tempSystemDateTime,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Journeys);
