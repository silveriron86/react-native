/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Alert,
  KeyboardAvoidingView,
  Switch,
} from 'react-native';
import {connect} from 'react-redux';
import {Chevron} from 'react-native-shapes';
import RNPickerSelect from 'react-native-picker-select';
import {styles, selectStyle, selectTextInputStyle} from '../_styles';
import {SettingActions} from '../../../actions';
import Tags from '../../../constants/Tags';
import SelectOthers from '../Tags/SelectOthers';
import * as RootNavigation from '../../../navigation/RootNavigation';
// const deleteIcon = require('../../../../assets/icons/delete.png');

class EditTagType extends React.Component {
  constructor(props) {
    super(props);

    let index = -1;
    let childTags = [];
    let data = {
      short: '',
      category: 'journaling',
      used: true,
      type: '',
      required: false,
      cover: -1,
      background: -1,
      font: 'PremiumUltra5',
      fontSize: 24,
      option: -1,
      locked: false,
      noteLocked: false,
    };

    if (props.route) {
      // index = props.navigation.getParam('index');
      index = props.route.params?.index ?? 0;
    }

    if (props.selected >= 0) {
      index = props.selected;
    }

    if (index >= 0) {
      data = props.tags[index];
      if (typeof data.tags !== 'string') {
        childTags = data.tags.join(' ');
        if (data.required === true) {
          childTags = childTags.replace(Tags[index].tags.join(' '), '');
        }
      }
    }

    let SELECT_CATEGORIES = [];
    let categories = [];
    props.tags.forEach((tag) => {
      if (categories.indexOf(tag.category) < 0) {
        categories.push(tag.category);
      }
    });
    categories.forEach((category) => {
      SELECT_CATEGORIES.push({
        label: category,
        value: category,
      });
    });

    this.categories = SELECT_CATEGORIES;
    this.state = {
      index: index,
      short: data.short,
      category: data.category,
      used: data.used,
      childTags: childTags,
      type: data.type,
      tag: '',
      cover: data.cover,
      required: data.required,
      background: data.background,
      option: data.option,
      locked: data.locked,
      noteLocked: data.noteLocked,
      font: 'PremiumUltra5',
      fontSize: 24,
    };
  }

  toggleCheckBox = (index) => {
    const {tags} = this.props;
    let data = JSON.parse(JSON.stringify(tags));
    data[index].used = !data[index].used;
    this.props.setSettings({type: 'TAGS', value: data});
  };

  handleSave = () => {
    const {tags, navigation, selected} = this.props;
    const {
      short,
      childTags,
      type,
      category,
      used,
      required,
      cover,
      background,
      option,
      font,
      fontSize,
      locked,
      noteLocked,
    } = this.state;
    if (!short || !type) {
      Alert.alert('Error', 'All fields should be required.');
      return;
    }

    let data = JSON.parse(JSON.stringify(tags));
    let index = this.state.index;
    const isModal = typeof navigation === 'undefined';
    if (isModal) {
      // modal
      if (selected >= 0) {
        // update
        index = selected;
      } else {
        // create
        data.push({
          tags: childTags.split(' '),
          short,
          category,
          used,
          type,
          required,
          cover,
          background,
          option,
          font,
          fontSize,
          locked,
          noteLocked,
        });
      }
    }

    if (index >= 0) {
      data[index].category = category;
      data[index].short = short;
      data[index].type = type;
      data[index].locked = locked;
      data[index].noteLocked = noteLocked;

      let childTagsText = childTags;
      if (data[index].required === true) {
        childTagsText = Tags[index].tags.join(' ') + ' ' + childTags;
      }
      childTagsText = childTagsText.trim().replace('   ', ' ');
      let childTagsArray = childTagsText.split(' ');
      data[index].tags = [...new Set(childTagsArray)];
    }

    console.log(index, data);
    this.props.setSettings({type: 'TAGS', value: data});
    if (isModal) {
      this.props.onClose();
    } else {
      navigation.goBack();
    }
  };

  onChangeValue = (type, value) => {
    let state = this.state;
    state[type] = value;
    this.setState(state);
  };

  onDeleteTag = (index) => {
    const {childTags} = this.state;
    let tags = JSON.parse(JSON.stringify(childTags));
    tags.splice(index, 1);
    this.setState({
      childTags: tags,
    });
  };

  onSubmitEditing = () => {
    const {tag, childTags} = this.state;
    if (tag) {
      let tags = JSON.parse(JSON.stringify(childTags));
      tags.push(tag);
      this.setState({
        childTags: tags,
        tag: '',
      });
    }
  };

  onSelectBackground = (v) => {
    const {index} = this.state;
    const journal = index >= 0 ? this.props.tags[index] : null;
    if (journal) {
      this.onSetValue('background', v /*journal.background === v ? -1 : v*/);
    } else {
      this.setState({
        background: v,
      });
    }
  };

  onSelectCover = (coverIndex) => {
    const {index} = this.state;
    if (index >= 0) {
      const {tags} = this.props;
      let data = JSON.parse(JSON.stringify(tags));
      data[index].cover = coverIndex;
      data[index].option = -1;
      this.props.setSettings({type: 'TAGS', value: data});
    } else {
      this.setState({
        cover: coverIndex,
      });
    }
  };

  onSetValue = (type, value) => {
    const {index} = this.state;
    if (index >= 0) {
      const {tags} = this.props;
      let data = JSON.parse(JSON.stringify(tags));
      data[index][type] = value;
      this.props.setSettings({type: 'TAGS', value: data});
    } else {
      let state = {...this.state};
      state[type] = value;
      this.setState(state);
    }
  };

  onToggleLocked = (v) => {
    this.setState({locked: v});
    this.onSetValue('locked', v);
  };

  onToggleNoteLocked = (v) => {
    this.setState({noteLocked: v});
    this.onSetValue('noteLocked', v);
  };

  goPasscode = () => {
    const {navigation} = this.props;
    const isModal = typeof navigation === 'undefined';
    if (isModal) {
      RootNavigation.navigate('Settings');
      global.eventEmitter.emit('TO_PASSCODE');
      setTimeout(() => {
        this.props.onClose();
      }, 300);
    } else {
      navigation.navigate('Passcode');
    }
  };

  render() {
    const {
      index,
      required,
      short,
      type,
      childTags,
      category,
      cover,
      background,
      font,
      fontSize,
      option,
      locked,
      noteLocked,
    } = this.state;
    const {streamFont, tags, passcode} = this.props;
    const journal = index >= 0 ? tags[index] : null;
    let newOne = null;
    if (!journal) {
      newOne = {
        cover: cover,
        background: background,
        option: option,
        font: font,
        fontSize: fontSize,
      };
    }

    return (
      <KeyboardAvoidingView
        style={{flex: 1, backgroundColor: 'white'}}
        behavior="padding"
        enabled>
        {typeof this.props.navigation === 'undefined' && (
          <View style={{marginTop: 30, marginBottom: 15}}>
            <Text
              style={{
                textAlign: 'center',
                color: '#333',
                fontSize: 19,
                fontWeight: 'bold',
              }}>
              {index === -1 ? 'New' : 'Edit'} Journal
            </Text>
          </View>
        )}
        <ScrollView style={{flex: 1}}>
          <View style={[styles.row, {marginTop: 2}]}>
            <View style={{width: 100}}>
              <Text style={styles.labelText}>Category</Text>
            </View>
            <View style={{borderWidth: 1, borderColor: '#666'}}>
              <RNPickerSelect
                placeholder={{}}
                items={this.categories}
                onValueChange={(value) => {
                  this.setState({category: value});
                }}
                value={category}
                style={selectStyle}
                useNativeAndroidPickerStyle={false}
                textInputProps={selectTextInputStyle}
                Icon={() => {
                  return <Chevron size={1.5} color="gray" />;
                }}
              />
            </View>
          </View>
          <View>
            <View style={{marginHorizontal: 20, marginTop: 10}}>
              <Text style={[styles.labelText, {fontFamily: 'PremiumUltra5'}]}>
                "Journal Short Name" (To use when creating a note in this
                journal via text message or the web)
              </Text>
            </View>
            <View style={[styles.row, {marginVertical: 0}]}>
              <View style={styles.labelText} />
              <View style={{width: 70}}>
                <TextInput
                  style={[
                    styles.snoozeText,
                    {width: 60, textAlign: 'left'},
                    required === true && {backgroundColor: '#efefef'},
                  ]}
                  value={short}
                  maxLength={10}
                  editable={required === false || index === -1}
                  autoCapitalize="none"
                  onChangeText={(v) => this.onChangeValue('short', v)}
                />
              </View>
              <Text style={styles.exampleText}>Example: "Dream"</Text>
            </View>
          </View>
          <View style={[styles.row, {alignItems: 'flex-start'}]}>
            <View style={{marginTop: 5}}>
              <Text style={styles.labelText}>Journal Name</Text>
            </View>
            <View style={{flex: 1}}>
              <TextInput
                style={[
                  styles.snoozeText,
                  {width: '100%', textAlign: 'left'},
                  required === true && {backgroundColor: '#efefef'},
                ]}
                value={type}
                editable={required === false || index === -1}
                autoCapitalize="none"
                onChangeText={(v) => this.onChangeValue('type', v)}
              />
              <Text style={styles.exampleText}>
                Example: "My dream journal"
              </Text>
            </View>
          </View>
          <View>
            <View style={{marginHorizontal: 20, marginTop: 10}}>
              <Text style={[styles.labelText, {fontFamily: 'PremiumUltra5'}]}>
                Show these tags to quick-tap from notes in this journal
              </Text>
            </View>
            <View style={[styles.row, {alignItems: 'flex-start'}]}>
              <View style={{marginTop: 5, width: 100}}>
                <Text style={styles.labelText}>Journal</Text>
              </View>
              <View style={{flex: 1}}>
                <View>
                  {required === true &&
                    Tags[index].tags.map((item, ii) => {
                      return <Text key={`edit-tag-type-${ii}`}>{item}, </Text>;
                    })}
                  <TextInput
                    style={[
                      styles.snoozeText,
                      {width: '100%', textAlign: 'left', height: 120},
                    ]}
                    value={childTags}
                    autoCapitalize={'none'}
                    multiline={true}
                    onChangeText={(v) => this.onChangeValue('childTags', v)}
                  />
                </View>
              </View>
            </View>
          </View>

          <SelectOthers
            isModal={true}
            tag={journal ? journal : newOne}
            systemFontSize={fontSize}
            systemFont={streamFont}
            onSelectBackground={this.onSelectBackground}
            onSelectCover={this.onSelectCover}
            onSetFontSize={(v) => this.onSetValue('fontSize', v)}
            onSetFont={(v) => this.onSetValue('font', v)}
            onChangeOption={(v) => this.onSetValue('option', v)}
          />

          <View style={[styles.row, {}]}>
            <View style={{width: 60}}>
              <Text style={styles.labelText}>Lock</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Switch
                disabled={!passcode.enabled}
                value={locked}
                onValueChange={this.onToggleLocked}
              />
              <TouchableOpacity onPress={this.goPasscode}>
                <Text style={styles.linkText}>
                  {passcode.enabled
                    ? 'App-level passcode settings'
                    : 'Enable App-level passcode'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[styles.row, {}]}>
            <View style={{width: 220}}>
              <Text style={styles.labelText}>Also lock Create Note Screen</Text>
            </View>
            <Switch
              disabled={!passcode.enabled}
              value={noteLocked}
              onValueChange={this.onToggleNoteLocked}
            />
          </View>

          <View style={[styles.row, {borderBottomWidth: 0}]}>
            <View style={styles.switchLabel}>
              <TouchableOpacity
                style={[styles.analyzeNowBtn, {marginTop: 0, height: 40}]}
                onPress={this.handleSave}>
                <Text style={styles.logoutText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tags: state.SettingReducer.tags,
    streamFont: state.SettingReducer.streamFont,
    passcode: state.SettingReducer.passcode,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditTagType);
