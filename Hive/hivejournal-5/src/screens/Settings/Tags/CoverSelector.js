/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {FlatList, Image, TouchableOpacity, View, Text} from 'react-native';
import {styles} from '../_styles';
import ImageConstants from '../../../constants/ImageConstants';

const cleanImg = require('../../../../assets/videos/clean/screenshot.png');
const runningImg = require('../../../../assets/videos/running/screenshot.png');

export default class CoverSelector extends React.Component {
  render() {
    const {value, option, onSelect, onChangeOption} = this.props;
    const ITEM_HEIGHT = 90;

    return (
      <>
        <View style={styles.coverSelector}>
          <FlatList
            data={ImageConstants.journalCoverThumbs}
            initialScrollIndex={value}
            getItemLayout={(data, index) => ({
              length: ITEM_HEIGHT,
              offset: ITEM_HEIGHT * index,
              index,
            })}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => onSelect(index)}
                key={`cover-selector-${index}`}>
                <Image source={item} style={styles.colImg} />
                {value === index && <View style={styles.selectedBorder} />}
              </TouchableOpacity>
            )}
            horizontal={true}
          />
        </View>

        <View style={styles.coverOptions}>
          <Text>Video: </Text>
          <TouchableOpacity onPress={() => onChangeOption(0)}>
            <Image
              source={cleanImg}
              style={[
                styles.coverVideoImg,
                option === 0 && styles.selectedOption,
              ]}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onChangeOption(1)}>
            <Image
              source={runningImg}
              style={[
                styles.coverVideoImg,
                option === 1 && styles.selectedOption,
              ]}
            />
          </TouchableOpacity>
        </View>
      </>
    );
  }
}
