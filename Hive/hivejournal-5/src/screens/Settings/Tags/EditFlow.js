/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Alert,
  Text,
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import RNPickerSelect from 'react-native-picker-select';
import ModalWrapper from 'react-native-modal-wrapper';
import {styles} from '../_styles';
import {SettingActions} from '../../../actions';
// import Tags from '../../../constants/Tags';
import * as RootNavigation from '../../../navigation/RootNavigation';

const createIcon = require('../../../../assets/icons/plus_orange.png');
const deleteIcon = require('../../../../assets/icons/delete.png');
const snSelectStyle = {
  viewContainer: {
    marginLeft: 15,
    width: '100%',
    height: 30,
    borderWidth: 1,
    borderColor: '#999',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {},
};
const SELECT_FLOW_TYPES = [
  {label: 'Satisfaction', value: 'satisfaction'},
  {label: 'Energy', value: 'energy'},
];

let SELECT_MIN_MAX = [];
for (let i = 0; i <= 10; i++) {
  SELECT_MIN_MAX.push({label: i.toString(), value: i});
}

class EditFlow extends React.Component {
  constructor(props) {
    super(props);
    let index = props.route.params?.index ?? 0;
    let data = props.tags[index];
    let positions = [];
    if (typeof data.positions === 'undefined' || data.positions.length === 0) {
      if (data.tags.length > 0) {
        for (let i = 0; i < data.tags.length; i++) {
          positions.push({
            min: 0,
            max: 10,
          });
        }
      }
    } else {
      positions = JSON.parse(JSON.stringify(data.positions));
    }

    this.state = {
      index: index,
      isOpenedModal: false,
      positions: positions,
      childTags: data.tags,
      newTag: '',
      minValue: 0,
      maxValue: 10,
      type: 'satisfaction',
    };
  }

  componentDidMount() {
    const {tags, navigation} = this.props;
    const {index, positions} = this.state;

    navigation.setOptions({
      headerRight: (props) => (
        <TouchableOpacity onPress={this.onAddTag} style={{marginRight: 10}}>
          <Image source={createIcon} style={{width: 30, height: 30}} />
        </TouchableOpacity>
      ),
    });

    let data = tags[index];
    if (typeof data.positions === 'undefined') {
      this._onUpdate('', positions, '');
    }
  }

  onAddTag = () => {
    this.setState({
      isOpenedModal: true,
    });
  };

  onChangeNewTag = (t) => {
    this.setState({
      newTag: t,
    });
  };

  onCloseModal = () => {
    this.setState({
      isOpenedModal: false,
      newTag: '',
      minValue: 0,
      maxValue: 10,
    });
  };

  onSave = () => {
    const {newTag, minValue, maxValue, type} = this.state;
    if (!newTag) {
      Alert.alert('Error', 'Please input a tag name');
      return;
    }

    let positions = JSON.parse(JSON.stringify(this.state.positions));
    positions.push({
      min: minValue,
      max: maxValue,
    });

    let childTags = JSON.parse(JSON.stringify(this.state.childTags));
    childTags.push(newTag);

    this.setState(
      {
        positions: positions,
        childTags: childTags,
      },
      () => {
        this._onUpdate(childTags, positions, type);
        this.onCloseModal();
      },
    );
  };

  _onUpdate = (childTags, positions, type) => {
    const {tags} = this.props;
    const {index} = this.state;
    let data = JSON.parse(JSON.stringify(tags));
    if (childTags !== '') {
      data[index].tags = childTags;
    }
    if (type !== '') {
      if (typeof data[index][type] === 'undefined') {
        data[index][type] = [];
      }
      data[index][type].push(childTags[childTags.length - 1]);
    }
    if (positions !== '') {
      data[index].positions = positions;
    }
    this.props.setSettings({type: 'TAGS', value: data});
  };

  onDeleteTag = (index) => {
    let positions = JSON.parse(JSON.stringify(this.state.positions));
    let childTags = JSON.parse(JSON.stringify(this.state.childTags));
    positions.splice(index, 1);
    childTags.splice(index, 1);

    this.setState(
      {
        childTags: childTags,
        positions: positions,
      },
      () => {
        this._onUpdate(childTags, positions, '');
      },
    );
  };

  onChangeMin = (v, idx, type) => {
    const {positions} = this.state;
    let tmp = positions.slice(0);
    tmp[idx][type] = v;
    this.setState(
      {
        positions: tmp,
      },
      () => {
        this._onUpdate('', positions, '');
      },
    );
  };

  render() {
    const {tags, route} = this.props;
    const {
      positions,
      isOpenedModal,
      newTag,
      minValue,
      maxValue,
      type,
    } = this.state;
    let index = route.params?.index ?? 0;
    let data = tags[index];
    console.log(data, positions);
    let satisfactions = [];
    let energies = [];
    if (data.tags.length > 0) {
      data.tags.forEach((item, i) => {
        if (data.energy.indexOf(item) > -1) {
          energies.push(item);
        } else {
          satisfactions.push(item);
        }
      });
    }
    console.log(energies, satisfactions);

    return (
      <View style={{flex: 1}}>
        <ModalWrapper
          containerStyle={styles.modalContainer}
          style={styles.modal}
          onRequestClose={this.onCloseModal}
          shouldAnimateOnRequestClose={true}
          shouldCloseOnOverlayPress={true}
          visible={isOpenedModal}
          position={'top'}>
          <View style={styles.editFlowModal}>
            <Text style={styles.editFlowModalTitle}>Add a new tag</Text>
            <View style={styles.editFlowModalRow}>
              <View style={{width: 40}}>
                <Text>Type: </Text>
              </View>
              <View style={{width: 100}}>
                <RNPickerSelect
                  placeholder={{}}
                  items={SELECT_FLOW_TYPES}
                  onValueChange={(v) => {
                    this.setState({type: v});
                  }}
                  value={type}
                  style={snSelectStyle}
                  useNativeAndroidPickerStyle={false}
                  Icon={null}
                />
              </View>
            </View>
            <View style={styles.editFlowModalRow}>
              <View style={{width: 40}}>
                <Text>Tag: </Text>
              </View>
              <TextInput
                style={styles.newTagInput}
                autoCapitalize={'none'}
                value={newTag}
                onChangeText={this.onChangeNewTag}
              />
            </View>
            <View style={styles.editFlowModalRow}>
              <View style={{width: 40}}>
                <Text>Min: </Text>
              </View>
              <View style={{width: 30}}>
                <RNPickerSelect
                  placeholder={{}}
                  items={SELECT_MIN_MAX}
                  onValueChange={(v) => {
                    this.setState({minValue: v});
                  }}
                  value={minValue}
                  style={snSelectStyle}
                  useNativeAndroidPickerStyle={false}
                  Icon={null}
                />
              </View>
            </View>
            <View style={styles.editFlowModalRow}>
              <View style={{width: 40}}>
                <Text>Max: </Text>
              </View>
              <View style={{width: 30}}>
                <RNPickerSelect
                  placeholder={{}}
                  items={SELECT_MIN_MAX}
                  onValueChange={(v) => {
                    this.setState({maxValue: v});
                  }}
                  value={maxValue}
                  style={snSelectStyle}
                  useNativeAndroidPickerStyle={false}
                  Icon={null}
                />
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <TouchableOpacity
                  style={[styles.analyzeNowBtn, {backgroundColor: '#aaa'}]}
                  onPress={this.onCloseModal}>
                  <Text style={styles.analyzeNowText}>Cancel</Text>
                </TouchableOpacity>
              </View>
              <View style={{width: 20}} />
              <View style={{flex: 1}}>
                <TouchableOpacity
                  style={styles.analyzeNowBtn}
                  onPress={this.onSave}>
                  <Text style={styles.analyzeNowText}>Save</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ModalWrapper>

        <View style={{flex: 1}}>
          {[...Array(2)].map((_t, ii) => {
            return (
              <View
                style={{flex: 1, borderTopWidth: 1, borderColor: '#999'}}
                key={`flow-type-${ii}`}>
                <View style={styles.tableHeader}>
                  <View style={{flex: 1}}>
                    <Text style={styles.flowCategoryText}>
                      {ii === 0 ? 'Satisfaction' : 'Energy'}
                    </Text>
                  </View>
                  <View style={{width: 60}}>
                    <Text style={styles.tableHeaderText}>Min</Text>
                  </View>
                  <View style={{width: 60}}>
                    <Text style={styles.tableHeaderText}>Max</Text>
                  </View>
                  <View style={{width: 50}}>
                    <Text style={styles.tableHeaderText}>Action</Text>
                  </View>
                </View>

                <ScrollView>
                  {data.tags.map((item, idx) => {
                    if (
                      (ii === 0 && data.satisfaction.indexOf(item) > -1) ||
                      (ii === 1 && data.energy.indexOf(item) > -1)
                    ) {
                      return (
                        <View style={styles.tableRow}>
                          <View style={{flex: 1}}>
                            <Text>{item}</Text>
                          </View>
                          <View style={{width: 60}}>
                            <View style={{width: 30}}>
                              <RNPickerSelect
                                placeholder={{}}
                                items={SELECT_MIN_MAX}
                                onValueChange={(v) =>
                                  this.onChangeMin(v, idx, 'min')
                                }
                                value={positions[idx].min}
                                style={snSelectStyle}
                                useNativeAndroidPickerStyle={false}
                                Icon={null}
                              />
                            </View>
                          </View>
                          <View style={{width: 60}}>
                            <View style={{width: 30}}>
                              <RNPickerSelect
                                placeholder={{}}
                                items={SELECT_MIN_MAX}
                                onValueChange={(v) =>
                                  this.onChangeMin(v, idx, 'max')
                                }
                                value={positions[idx].max}
                                style={snSelectStyle}
                                useNativeAndroidPickerStyle={false}
                                Icon={null}
                              />
                            </View>
                          </View>
                          <View style={{width: 50}}>
                            <TouchableOpacity
                              style={styles.flowTagDelBtn}
                              onPress={() => this.onDeleteTag(idx)}>
                              <Image
                                source={deleteIcon}
                                style={{width: 15, height: 15}}
                              />
                            </TouchableOpacity>
                          </View>
                        </View>
                      );
                    }
                  })}
                </ScrollView>
              </View>
            );
          })}
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tags: state.SettingReducer.tags,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditFlow);
