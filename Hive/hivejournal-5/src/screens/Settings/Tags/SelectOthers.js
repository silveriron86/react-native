/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Image,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import ImageConstants from '../../../constants/ImageConstants';
import {styles} from '../_styles';
import FontEdit from '../FontEdit';
import CoverSelector from './CoverSelector';
import ImageSelector from '../../Notes/ImageSelector';
import Colors from '../../../constants/Colors';

const closeIcon = require('../../../../assets/icons/glyph_close_32x32.png');

export default class SelectOthers extends React.Component {
  state = {
    isFontModalVisible: false,
    isBgImagesModalVisible: false,
    isCoverImagesModalVisible: false,
  };

  onToggleFontModal = () => {
    this.setState({
      isFontModalVisible: !this.state.isFontModalVisible,
    });
  };

  onToggleCoverImagesModal = () => {
    this.setState({
      isCoverImagesModalVisible: !this.state.isCoverImagesModalVisible,
    });
  };

  onToggleBgModal = () => {
    this.setState({
      isBgImagesModalVisible: !this.state.isBgImagesModalVisible,
    });
  };

  render() {
    const {width, height} = Dimensions.get('window');
    const {tag, systemFontSize, systemFont, isModal} = this.props;
    const {
      isFontModalVisible,
      isBgImagesModalVisible,
      isCoverImagesModalVisible,
    } = this.state;

    let cover = 0;
    let option = 0;
    let fontSize = systemFontSize;
    let font = systemFont;
    let bgs = [];

    if (tag) {
      cover = tag === null || typeof tag.cover === 'undefined' ? 0 : tag.cover;
      option =
        typeof tag.option === 'undefined' || tag.option === null
          ? -1
          : tag.option;

      fontSize =
        typeof tag.fontSize === 'undefined' ? systemFontSize : tag.fontSize;
      font = typeof tag.font === 'undefined' ? systemFont : tag.font;
      bgs = typeof tag.background === 'undefined' ? [] : tag.background;
    }

    return (
      <>
        {isModal ? (
          <>
            <View style={[styles.row, {alignItems: 'flex-start'}]}>
              <View>
                <Text style={styles.labelText}>Cover Image</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'flex-end',
                }}>
                {cover >= 0 && (
                  <Image
                    source={ImageConstants.journalCoverThumbs[cover]}
                    style={[styles.colImg, {marginLeft: 10}]}
                  />
                )}
                <TouchableOpacity onPress={this.onToggleCoverImagesModal}>
                  <Text style={styles.linkText}>Select...</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={[
                styles.row,
                {flexDirection: 'column', alignItems: 'flex-start'},
              ]}>
              <View>
                <Text style={styles.labelText}>
                  Default Randomized Backgrounds
                </Text>
              </View>
              <ScrollView horizontal={true} style={{flex: 1}}>
                {bgs.length > 0 &&
                  bgs.map((bg, iii) => (
                    <Image
                      key={`bg-${iii}`}
                      source={ImageConstants.thumbs[bg]}
                      style={[styles.colImg, {marginLeft: 10}]}
                    />
                  ))}
              </ScrollView>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <TouchableOpacity onPress={this.onToggleBgModal}>
                  <Text style={styles.linkText}>Select multiple...</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={[
                styles.row,
                {
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                  paddingTop: 0,
                },
              ]}>
              <View
                style={[
                  styles.row,
                  {borderBottomWidth: 0, marginHorizontal: 0},
                ]}>
                <View>
                  <Text style={styles.labelText}>Default Font</Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity onPress={this.onToggleFontModal}>
                    <Text style={styles.linkText}>Select...</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <Text
                style={{
                  marginLeft: 10,
                  fontFamily: font,
                  fontSize: fontSize,
                  color: Colors.scheme.blue,
                }}>
                {font} / {fontSize.toFixed(0)}
              </Text>
            </View>
          </>
        ) : (
          <View style={{marginLeft: 20}}>
            <View>
              <Text>- Cover Image:</Text>
              <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                <Image
                  source={ImageConstants.journalCoverThumbs[cover]}
                  style={[styles.colImg, {marginLeft: 10}]}
                />
                <TouchableOpacity onPress={this.onToggleCoverImagesModal}>
                  <Text style={styles.linkText}>Select...</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{marginTop: 15}}>
              <Text>- Default Randomized Backgrounds:</Text>
              <ScrollView horizontal={true} style={{flex: 1, paddingLeft: 5}}>
                {bgs.length > 0 &&
                  bgs.map((bg, iii) => (
                    <Image
                      source={ImageConstants.thumbs[bg]}
                      style={[styles.colImg, {marginLeft: 5}]}
                    />
                  ))}
              </ScrollView>
              <TouchableOpacity onPress={this.onToggleBgModal}>
                <Text style={styles.linkText}>Select multiple...</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginTop: 15}}>
              <View style={{flexDirection: 'row'}}>
                <Text>- Default Font:</Text>
                <TouchableOpacity onPress={this.onToggleFontModal}>
                  <Text style={styles.linkText}>Select...</Text>
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  marginLeft: 10,
                  fontFamily: font,
                  fontSize: fontSize,
                  color: Colors.scheme.blue,
                }}>
                {font} / {fontSize.toFixed(0)}
              </Text>
            </View>
          </View>
        )}

        <ModalWrapper
          containerStyle={[styles.modalContainer, {marginVertical: 0}]}
          style={[styles.modal, {width: width, height: height}]}
          onRequestClose={this.onToggleFontModal}
          shouldAnimateOnRequestClose={true}
          shouldCloseOnOverlayPress={true}
          visible={isFontModalVisible}
          position={'right'}>
          <View style={{flex: 1}}>
            <FontEdit
              onClose={this.onToggleFontModal}
              isModal={true}
              tagFont={font}
              tagFontSize={fontSize}
              setFontSize={this.props.onSetFontSize}
              setFont={this.props.onSetFont}
            />
          </View>
        </ModalWrapper>

        <ModalWrapper
          containerStyle={styles.modalContainer}
          style={[
            styles.modal,
            {backgroundColor: 'white', height: 'auto', padding: 10},
          ]}
          onRequestClose={this.onToggleBgModal}
          shouldAnimateOnRequestClose={true}
          shouldCloseOnOverlayPress={true}
          visible={isBgImagesModalVisible}
          position={'top'}>
          <ImageSelector
            isModal={true}
            tag={tag}
            onSelect={this.props.onSelectBackground}
          />
          <TouchableOpacity
            style={styles.closeIconBtn}
            onPress={this.onToggleBgModal}>
            <Image source={closeIcon} />
          </TouchableOpacity>
        </ModalWrapper>

        <ModalWrapper
          containerStyle={styles.modalContainer}
          style={[styles.modal, {padding: 10}]}
          onRequestClose={this.onToggleCoverImagesModal}
          shouldAnimateOnRequestClose={true}
          shouldCloseOnOverlayPress={true}
          visible={isCoverImagesModalVisible}
          position={'top'}>
          <CoverSelector
            value={cover}
            option={option}
            onChangeOption={this.props.onChangeOption}
            onSelect={this.props.onSelectCover}
          />
          <TouchableOpacity
            style={styles.closeIconBtn}
            onPress={this.onToggleCoverImagesModal}>
            <Image source={closeIcon} />
          </TouchableOpacity>
        </ModalWrapper>
      </>
    );
  }
}
