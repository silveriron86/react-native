/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Image,
  Text,
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Alert,
  SafeAreaView,
} from 'react-native';
import {connect} from 'react-redux';
import {CheckBox} from 'react-native-elements';
import ModalWrapper from 'react-native-modal-wrapper';
import {styles} from '../_styles';
import {SettingActions} from '../../../actions';
import SelectOthers from './SelectOthers';
import Colors from '../../../constants/Colors';

const deleteIcon = require('../../../../assets/icons/delete.png');
const createIcon = require('../../../../assets/icons/plus_orange.png');
const editIcon = require('../../../../assets/icons/pencil.png');

class TagCheckBox extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        {this.props.children}
      </TouchableOpacity>
    );
  }
}

class Tags extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisibleModal: false,
      prevTitle: '',
      title: '',
      containerHeight: null,
    };
  }

  toggleCheckBox = (index) => {
    const {tags} = this.props;
    let data = JSON.parse(JSON.stringify(tags));
    data[index].used = !data[index].used;
    this.props.setSettings({type: 'TAGS', value: data});
  };

  componentDidMount() {
    const {tags} = this.props;
    let isExistCustom = false;
    tags.forEach((tag) => {
      if (tag.custom === true) {
        isExistCustom = true;
      }
    });

    if (!isExistCustom) {
      let data = JSON.parse(JSON.stringify(tags));
      data.push({
        custom: true,
        category: 'New Custom Category 1',
        type: 'New Custom Tags 1',
        short: 'N',
        tags: [],
        required: false,
        used: false,
        cover: 0,
      });
      this.props.setSettings({type: 'TAGS', value: data});
    }
  }

  onContentSizeChange = (contentWidth, contentHeight) => {
    // Save the content height in state
    this.setState({containerHeight: contentHeight});
    if (this.props.route.params?.isCreating ?? false) {
      this.scrollView.scrollTo({y: contentHeight, animated: false});
      this.props.navigation.setParams({isCreating: false});
    }
  };

  onCloseModal = () => {
    this.setState({
      isVisibleModal: false,
    });
  };

  getNewCustomCatIndex = (str, isCategory, cat) => {
    const {tags} = this.props;
    let data = JSON.parse(JSON.stringify(tags));
    let index = 0;
    if (isCategory) {
      data.forEach((t) => {
        if (t.category.indexOf(str) >= 0) {
          let num = parseInt(t.category.replace(str, ''), 10);
          if (num > index) {
            index = num;
          }
        }
      });
    } else {
      data.forEach((t) => {
        if (t.category === cat && t.type.indexOf(str) >= 0) {
          let num = parseInt(t.type.replace(str, ''), 10);
          if (num > index) {
            index = num;
          }
        }
      });
    }
    return index + 1;
  };

  onAddCategory = () => {
    const {tags} = this.props;
    let data = JSON.parse(JSON.stringify(tags));
    let index = this.getNewCustomCatIndex('New Custom Category', true, null);
    data.push({
      custom: true,
      category: 'New Custom Category ' + index,
      type: 'New Custom Type 1',
      short: 'N',
      tags: [],
      required: false,
      used: false,
    });
    this.props.setSettings({type: 'TAGS', value: data});
    setTimeout(() => {
      this.scrollView.scrollTo({y: this.state.containerHeight});
    }, 500);
  };

  onEditCategory = (cat) => {
    this.setState({
      prevTitle: cat,
      title: cat,
      isVisibleModal: true,
    });
  };

  onSaveCategory = () => {
    const {tags} = this.props;
    const {prevTitle, title} = this.state;
    let data = JSON.parse(JSON.stringify(tags));
    data.forEach((c) => {
      if (c.category === prevTitle) {
        c.category = title;
      }
    });
    this.props.setSettings({type: 'TAGS', value: data});
    this.onCloseModal();
  };

  onAddTags = (cat) => {
    const {tags} = this.props;
    let data = JSON.parse(JSON.stringify(tags));
    let index = this.getNewCustomCatIndex('New Custom Type', false, cat);
    data.push({
      custom: true,
      category: cat,
      type: 'New Custom Type ' + index,
      short: 'N',
      tags: [],
      required: false,
      used: false,
      cover: 0,
    });
    this.props.setSettings({type: 'TAGS', value: data});
    setTimeout(() => {
      this.scrollView.scrollTo({y: this.state.containerHeight});
    }, 500);
  };

  onEditType = (index) => {
    let editTagPage =
      this.props.tags[index].type === 'flow' ? 'EditFlow' : 'EditTagType';

    this.props.navigation.navigate(editTagPage, {
      index: index,
    });
  };

  onDeleteTag = (index) => {
    Alert.alert(
      'Are you sure to delete this type?',
      '',
      [
        {
          text: 'No',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Yes',
          onPress: () => {
            const {tags} = this.props;
            let data = JSON.parse(JSON.stringify(tags));
            data.splice(index, 1);
            this.props.setSettings({type: 'TAGS', value: data});
          },
        },
      ],
      {cancelable: false},
    );
  };

  updateTitle = (v) => {
    this.setState({
      title: v,
    });
  };

  onSelectCover = (tagIndex, coverIndex) => {
    const {tags} = this.props;
    let data = JSON.parse(JSON.stringify(tags));
    data[tagIndex].cover = coverIndex;
    data[tagIndex].option = -1;
    this.props.setSettings({type: 'TAGS', value: data});
  };

  onSetValue = (tagIndex, type, value) => {
    const {tags} = this.props;
    let data = JSON.parse(JSON.stringify(tags));
    data[tagIndex][type] = value;
    this.props.setSettings({type: 'TAGS', value: data});
  };

  render() {
    const {tags, streamFont, fontSize} = this.props;
    const {isVisibleModal, title} = this.state;
    let rows = [[], []];
    let allCats = [{}, {}];

    tags.forEach((tag, idx) => {
      // let catType = tag.custom === true ? 1 : 0;
      let catType = 0;
      if (typeof allCats[catType][tag.category] === 'undefined') {
        allCats[catType][tag.category] = [];
      }
      allCats[catType][tag.category].push({
        tag: tag,
        index: idx,
      });
    });

    // for (let i = 0; i < 2; i++) {
    for (let i = 0; i < 1; i++) {
      let categories = allCats[i];
      Object.keys(categories).forEach((cat, idx) => {
        let allTags = [];
        categories[cat].forEach((r, rIdx) => {
          let cols = [];
          let t = r.tag;
          let index = r.index;

          if (typeof t.tags !== 'string' && t.tags.length > 0) {
            t.tags.forEach((tag, tIdx) => {
              cols.push(
                <Text key={`tag-${idx}-${rIdx}-${tIdx}`} style={styles.tagText}>
                  #{tag}
                </Text>,
              );
            });
          }

          allTags.push(
            <View key={`tags-${idx}-${rIdx}`} style={styles.tagRowWrapper}>
              <View
                style={[
                  styles.tagsRow,
                  categories[cat].length - 1 === rIdx && {borderBottomWidth: 0},
                ]}>
                <View
                  style={[
                    styles.checkBoxWrapper,
                    {
                      width: 80,
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'flex-start',
                    },
                  ]}>
                  {t.required === true ? (
                    <CheckBox
                      Component={TouchableWithoutFeedback}
                      checked={t.used}
                      checkedColor="#aaa"
                      uncheckedColor="#aaa"
                    />
                  ) : (
                    <CheckBox
                      Component={TagCheckBox}
                      checked={t.used}
                      checkedColor={Colors.scheme.blue}
                      uncheckedColor={Colors.scheme.blue}
                      onPress={() => this.toggleCheckBox(index)}
                    />
                  )}
                  <Text>Active</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity
                    onPress={() => this.onEditType(index)}
                    style={styles.btn}>
                    <Image source={editIcon} style={styles.btnImg} />
                  </TouchableOpacity>
                  {t.custom === true && (
                    <TouchableOpacity
                      onPress={() => this.onDeleteTag(index)}
                      style={styles.btn}>
                      <Image source={deleteIcon} style={styles.btnImg} />
                    </TouchableOpacity>
                  )}
                </View>
              </View>
              <View style={{marginLeft: 20}}>
                <View
                  style={{flex: 1, justifyContent: 'center', paddingTop: 3}}>
                  <Text numberOfLines={1} style={styles.mainTagText}>
                    #{t.short} - #{t.type}{' '}
                  </Text>
                  <View style={styles.childTags}>{cols}</View>
                </View>
              </View>
              <SelectOthers
                isModal={false}
                tag={t}
                systemFontSize={fontSize}
                systemFont={streamFont}
                onSelectBackground={(v) => {
                  this.onSetValue(
                    index,
                    'background',
                    // t.background === v ? -1 : v,
                    v,
                  );
                }}
                onSelectCover={(v) => this.onSelectCover(index, v)}
                onSetFontSize={(v) => this.onSetValue(index, 'fontSize', v)}
                onSetFont={(v) => this.onSetValue(index, 'font', v)}
                onChangeOption={(v) => this.onSetValue(index, 'option', v)}
              />
            </View>,
          );
        });

        rows[i].push(
          <View style={{marginTop: 0}} key={`cat-${idx}-${i}`}>
            <View style={styles.categoryView}>
              <View>
                <Text numberOfLines={1} style={styles.tagCatText}>
                  {cat}
                </Text>
              </View>
              {(categories[cat][0].length === 0 ||
                categories[cat][0].tag.custom === true) && (
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity
                    onPress={() => this.onEditCategory(cat)}
                    style={styles.btn}>
                    <Image source={editIcon} style={styles.btnImg} />
                  </TouchableOpacity>
                  {categories[cat].length === 0 && (
                    <TouchableOpacity
                      onPress={this.deleteCategory}
                      style={styles.btn}>
                      <Image source={deleteIcon} style={styles.btnImg} />
                    </TouchableOpacity>
                  )}
                  <TouchableOpacity
                    onPress={() => this.onAddTags(cat)}
                    style={styles.btn}>
                    <Image source={createIcon} style={styles.btnImg} />
                  </TouchableOpacity>
                </View>
              )}
            </View>
            <View>{allTags}</View>
          </View>,
        );
      });
    }

    return (
      <SafeAreaView>
        <ScrollView
          style={styles.whiteBG}
          ref={(ref) => (this.scrollView = ref)}
          onContentSizeChange={this.onContentSizeChange}>
          <View style={styles.categoriesView}>
            <Text style={styles.categoriesText}>Default Categories</Text>
          </View>
          {rows[0]}
          <View style={styles.categoriesView}>
            <Text style={styles.categoriesText}>Custom Categories</Text>
            <TouchableOpacity onPress={this.onAddCategory} style={styles.btn}>
              <Image source={createIcon} style={styles.btnImg} />
            </TouchableOpacity>
          </View>
          {rows[1]}

          <ModalWrapper
            containerStyle={styles.modalContainer}
            style={styles.modal}
            onRequestClose={this.onCloseModal}
            shouldAnimateOnRequestClose={true}
            shouldCloseOnOverlayPress={true}
            visible={isVisibleModal}
            position={'top'}>
            <View style={styles.modalInner}>
              <TextInput
                autoFocus={true}
                style={styles.txtInput}
                value={title}
                onChangeText={this.updateTitle}
              />
              <View style={{alignItems: 'center'}}>
                <TouchableOpacity
                  style={[styles.analyzeNowBtn, {width: 100}]}
                  onPress={this.onSaveCategory}>
                  <Text style={styles.analyzeNowText}>Save</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ModalWrapper>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tags: state.SettingReducer.tags,
    streamFont: state.SettingReducer.streamFont,
    fontSize: state.SettingReducer.fontSize,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tags);
