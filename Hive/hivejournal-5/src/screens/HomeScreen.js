import React from 'react';
import {connect} from 'react-redux';
import StreamScreen from './Stream';
import Login from '../widgets/Login';
import EmptyView from '../widgets/EmptyView';

class HomeScreen extends React.Component {
  render() {
    const {accessToken} = this.props;
    console.log(this.props.accessToken);
    if (accessToken === 'none') {
      return <EmptyView />;
    }

    return accessToken === null ? (
      <Login />
    ) : (
      <StreamScreen props={this.props} />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    accessToken: state.CommonReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
