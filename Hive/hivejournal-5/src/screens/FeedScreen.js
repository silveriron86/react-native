/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, NativeModules, AppState} from 'react-native';
import {connect} from 'react-redux';
import NotesScreen from './Notes';
import Login from '../widgets/Login';
import EmptyView from '../widgets/EmptyView';
import {SettingActions, CommonActions, MongoActions} from '../actions';
import FontEdit from './Settings/FontEdit';
import Utils from '../utils';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../constants/Colors';
import Rate from 'react-native-rate';
import {SafeAreaView} from 'react-native-safe-area-context';

class FeedScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showFont: 'none',
    };
  }

  componentWillMount() {
    AsyncStorage.getItem('ACCESS_TOKEN', (_err, token) => {
      console.log('*** InitialState accessToken', token);
      this.props.setAccessToken({
        value: token ? token : null,
      });
    });
    AsyncStorage.getItem('USER', (_err, data) => {
      if (data) {
        this.props.setUser({
          value: JSON.parse(data),
        });
      }
    });
  }

  componentDidMount() {
    this.mount = true;
    AppState.addEventListener('change', this._handleAppStateChange);

    global.eventEmitter.addListener('NOTE_CREATED', (data) => {
      if (data.actual === false) {
        return;
      }

      const {
        appLastNoteSavedDate,
        qtyTotalUniqueDaysAppWasOpened,
        ratings,
      } = this.props;
      const today = moment.utc();
      console.log(
        '******************************',
        appLastNoteSavedDate,
        qtyTotalUniqueDaysAppWasOpened,
        ratings,
      );
      // if appLastNoteSavedDate < today,
      if (
        appLastNoteSavedDate === null ||
        moment.utc(appLastNoteSavedDate).isBefore(today)
      ) {
        // set appLastNoteSavedDate = today
        this.props.setSettings({
          type: 'LAST_NOTE_SAVED_DATE',
          value: moment().format(),
        });

        // increment new user key of qtyTotalUniqueDaysAppWasOpened
        this.props.setSettings({
          type: 'QTY_TOTAL_UNIQUE_DAYS',
          value: qtyTotalUniqueDaysAppWasOpened + 1,
        });

        // If they have saved notes on at least 7 distinct days AND if they have not been asked to rate the app yet
        if (
          qtyTotalUniqueDaysAppWasOpened + 1 > 6 &&
          ratings.ios.lastRequestDate === null
        ) {
          console.log('rate!!!');
          // Then display a native iOS "Rate the App" modal.
          Rate.rate(
            {
              AppleAppID: '1470596836',
              GooglePackageName: 'com.hivejournal.mobile2',
              AmazonPackageName: 'com.hivejournal.mobile2',
              // OtherAndroidURL: 'http://www.randomappstore.com/app/47172391',
              // preferredAndroidMarket: AndroidMarket.Google,
              preferInApp: true,
              openAppStoreIfInAppFails: true,
              fallbackPlatformURL: 'https://hivejournal.com/',
            },
            (success) => {
              if (success) {
                // If the modal is displayed, save today's date, ( user new key of ratings.ios.lastRequestDate = today )
                let o = {...ratings};
                o.ios.lastRequestDate = today;
                this.props.setSettings({
                  type: 'RATINGS',
                  value: o,
                });
              }
            },
          );
        }
      }
    });
  }

  componentWillUnmount() {
    this.mount = false;
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (!this.mount) {
      return;
    }

    if (nextAppState === 'active') {
      this._syncIntentNotes();
    }
  };

  onClose = () => {
    this.setState(
      {
        showFont: false,
      },
      () => {
        this.props.setSettings({type: 'SELECTED_FONT', value: true});
      },
    );
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedFont !== 'none') {
      if (!this.mount) {
        return;
      }
      this.setState({
        showFont: !nextProps.selectedFont,
      });
    }

    if (this.props.accessToken !== nextProps.accessToken) {
      if (nextProps.accessToken) {
        console.log('*** logged in');
        this._syncIntentNotes();
      }
    }
  }

  _syncIntentNotes = () => {
    AsyncStorage.getItem('USER_ID', (_err, user_id) => {
      console.log('*** _syncIntentNotes');
      if (user_id) {
        const {IntentModule} = NativeModules;
        const {noteVisibility, screenName, location, tags} = this.props;
        console.log('USER ID = ', user_id);
        IntentModule.saveUserId(user_id);
        IntentModule.getNotes((res) => {
          console.log('** result **');
          if (res === '') {
            console.log('No Data');
            IntentModule.clearNotes();
          } else {
            let notes = JSON.parse(res);
            if (notes.length > 0) {
              console.log('******** All of notes ******', notes);
              let items = JSON.parse(JSON.stringify(notes));
              const selectedFont = 'PremiumUltra5';
              const selectedFontSize = 24;
              const fontColor = Colors.scheme.blue;
              const journal = tags[0];
              console.log(journal);
              const background_image =
                typeof journal.background !== 'undefined' &&
                journal.background.length > 0
                  ? Utils.getRandomInt(0, journal.background.length - 1)
                  : -1;

              notes.forEach((item) => {
                let origText = item.body;
                let text = origText;
                let noteTags = Utils.getTags(origText);
                if (noteTags) {
                  noteTags.forEach((tag, i) => {
                    noteTags[i] = tag.substr(1);
                    text = text.replace(tag + ' ', '');
                  });
                  text = text.replace('#' + noteTags[noteTags.length - 1], '');
                }

                let note = {
                  note_orig: Utils.encrypt(origText),
                  note_text: Utils.encrypt(text),
                  note_tags: noteTags,
                  creation_timestamp: moment(item.timestamp, 'X')
                    .tz(location)
                    .format(),
                  entry_ip: '0.0.0.0',
                  entry_app_type: 'mobile',
                  entry_app_version: '1.0',
                  word_count: Utils.getWords(origText),
                  font: selectedFont,
                  font_size: selectedFontSize,
                  color: fontColor,
                  permissions: {
                    see: noteVisibility.standard[0],
                    read: noteVisibility.standard[1],
                  },
                  screen_name: screenName,
                  main_tag: 'journal',
                  user_id: user_id,
                  background_image,
                };
                this.props.createNote({
                  data: note,
                  cb: () => {
                    console.log(item);
                    global.eventEmitter.emit('NOTE_CREATED', {});
                    let index = items.findIndex((n) => {
                      return n.timestamp === item.timestamp;
                    });
                    items.splice(index, 1);
                    if (items.length > 0) {
                      IntentModule.updateNotes(JSON.stringify(items));
                    } else {
                      IntentModule.clearNotes();
                    }
                  },
                });
              });
            }
          }
        });
      }
    });
  };

  render() {
    const {showFont} = this.state;
    const {accessToken} = this.props;
    console.log('**** access token', accessToken);
    if (accessToken === 'none') {
      return <EmptyView />;
    }

    let loggedIn = accessToken === null ? false : true;

    if (loggedIn) {
      if (showFont === 'none') {
        return <EmptyView />;
      } else if (showFont === true) {
        return <FontEdit onClose={this.onClose} />;
      }

      return (
        <View style={{flex: 1, backgroundColor: 'black'}}>
          <NotesScreen props={this.props} />
        </View>
      );
    }

    return <Login />;
  }
}

const mapStateToProps = (state) => {
  return {
    toneEnabled: state.SettingReducer.toneEnabled,
    spotifyEnabled: state.SettingReducer.spotifyEnabled,
    mongoUser: state.MongoReducer.mongoUser,
    error: state.MongoReducer.error,
    locations: state.SettingReducer.locations,
    location: state.SettingReducer.location,
    accessToken: state.CommonReducer.accessToken,
    passcode: state.SettingReducer.passcode,
    selectedFont: state.SettingReducer.selectedFont,
    screenName: state.SettingReducer.screenName,
    noteVisibility: state.SettingReducer.noteVisibility,
    tags: state.SettingReducer.tags,
    appLastNoteSavedDate: state.SettingReducer.appLastNoteSavedDate,
    qtyTotalUniqueDaysAppWasOpened:
      state.SettingReducer.qtyTotalUniqueDaysAppWasOpened,
    ratings: state.SettingReducer.ratings,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createNote: (req) => dispatch(MongoActions.createNote(req.data, req.cb)),
    saveLocations: (req) => dispatch(SettingActions.saveLocations(req.cb)),
    setAccessToken: (req) => dispatch(CommonActions.setAccessToken(req.value)),
    setUser: (req) => dispatch(CommonActions.setUser(req.value)),
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
    getToDoNotes: (req) =>
      dispatch(MongoActions.getToDoNotes(req.page, req.cb, req.size)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FeedScreen);
