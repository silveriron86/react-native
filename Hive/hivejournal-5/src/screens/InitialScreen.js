/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {View} from 'react-native';

export default class InitialScreen extends React.Component {
  UNSAFE_componentWillMount() {
    AsyncStorage.getItem('WT_DONE', (_err, done) => {
      AsyncStorage.removeItem('FROM_SETTINGS');
      this.props.navigation.navigate(done ? 'TabScreen' : 'WalkthroughScreen');
    });
  }

  render() {
    return <View style={{flex: 1, backgroundColor: 'black'}} />;
  }
}
