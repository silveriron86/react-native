/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  Dimensions,
  Animated,
  ImageBackground,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import styles from './_styles';
import ImageConstants from '../../constants/ImageConstants';
import Utils from '../../utils';

const MOON_SIZE = 110; // moon width
const note_bg = require('../../../assets/images/notebook_bg.png');
const closeIcon = require('../../../assets/icons/glyph_close_32x32.png');

export default class MoonCloud extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      full: false,
    };
  }

  UNSAFE_componentWillMount() {
    this.w = Dimensions.get('window').width - 20;
    this.h = (this.w * 305) / 404;
    this.scaleAnimatedValue = new Animated.Value(0.2);
  }

  componentDidMount() {
    const {note} = this.props;

    const w = Dimensions.get('window').width - 20;
    const h = (w * 305) / 404;

    // initial values for Rest Up Journey
    let lCloudImg = 1;
    let rCloudImg = 5;
    let idealSleepAmount = 8;
    let idealWakeTime = '06:00';

    let idealBedTime = Utils.getIdealBedTime(idealWakeTime, idealSleepAmount);
    let selectedNote = note.note;

    if (typeof selectedNote.ideal_sleep_amount !== 'undefined') {
      idealSleepAmount = selectedNote.ideal_sleep_amount;
      idealWakeTime = selectedNote.ideal_wake_time;
      idealBedTime = selectedNote.ideal_bed_time;
    }

    let unit = MOON_SIZE / idealSleepAmount;
    let oneDayAgo = moment(selectedNote.creation_timestamp).subtract(1, 'days');
    let moonPhase = Utils.getMoonPhase(
      parseInt(oneDayAgo.format('YYYY'), 10),
      parseInt(oneDayAgo.format('M'), 10),
      parseInt(oneDayAgo.format('D'), 10),
    );

    let lCouldRight =
      Utils.getDiffHours(idealBedTime, selectedNote.bed_time) * unit;
    let rCloudLeft =
      Utils.getDiffHours(idealWakeTime, selectedNote.wake_time) * unit;
    let lCloudTop = Utils.getRandomInt(0, MOON_SIZE / 2);
    let rCloudTop = Utils.getRandomInt(0, MOON_SIZE / 2);

    let howSleep = Utils.decrypt(selectedNote.note_orig).replace(
      'Last night I slept ',
      '',
    );

    if (howSleep === 'great') {
      // "great": cloud-1.png & cloud-2.png
      lCloudImg = 1;
      rCloudImg = 2;
    } else if (howSleep === 'good') {
      // "good": cloud-1 & (cloud-3 or cloud-4)
      lCloudImg = 1;
      rCloudImg = Utils.getRandomInt(3, 4);
    } else if (howSleep === 'ok') {
      // "ok": cloud-3 & cloud-4
      lCloudImg = 3;
      rCloudImg = 4;
    } else if (howSleep === 'bad') {
      // "bad": (cloud-3 or cloud-4) & (cloud-5 or cloud-6)
      lCloudImg = Utils.getRandomInt(3, 4);
      rCloudImg = Utils.getRandomInt(5, 6);
    } else if (howSleep === 'terrible') {
      // "terrible": cloud-5 & cloud-6
      lCloudImg = 5;
      rCloudImg = 6;
    }
    this.setState({
      moonPhase,
      lCloudImg,
      rCloudImg,
      lCloudTop,
      rCloudTop,
      lCouldRight,
      rCloudLeft,
    });
  }

  handlePress = () => {
    Animated.spring(this.scaleAnimatedValue, {
      toValue: 1,
      friction: 5,
    }).start();

    this.setState({
      full: true,
    });
  };

  onClose = () => {
    Animated.spring(this.scaleAnimatedValue, {
      toValue: 0.2,
    }).start();

    this.setState({
      full: false,
    });
  };

  render() {
    const {
      moonPhase,
      lCloudImg,
      rCloudImg,
      lCloudTop,
      rCloudTop,
      lCouldRight,
      rCloudLeft,
      full,
    } = this.state;
    const {note, fontSize, isOther} = this.props;
    const animatedStyle = {
      transform: [{scale: this.scaleAnimatedValue}],
    };
    const WIDTH = Dimensions.get('window').width;

    let selectedNote = null;
    if (note) {
      selectedNote = note.note;
    }

    let selectedFont = 'PremiumUltra5';
    if (selectedNote && selectedNote.font) {
      selectedFont = selectedNote.font;
    }

    return (
      <View style={{flex: 1}}>
        <View style={styles.moonCloudModal}>
          <View style={{zIndex: 9, flex: 1, height: MOON_SIZE}}>
            <Image
              source={ImageConstants.clouds[lCloudImg]}
              style={[
                {
                  top: lCloudTop,
                  right: lCouldRight,
                  width: MOON_SIZE,
                  height: MOON_SIZE / 2,
                  alignSelf: 'flex-end',
                },
                styles.cloudImg,
              ]}
            />
          </View>
          <View>
            <Image
              source={ImageConstants.moonPhases[moonPhase]}
              style={{
                zIndex: 1,
                width: MOON_SIZE,
                height: MOON_SIZE,
                resizeMode: 'contain',
              }}
            />
          </View>
          <View style={{zIndex: 9, flex: 1, height: MOON_SIZE}}>
            <Image
              source={ImageConstants.clouds[rCloudImg]}
              style={[
                {
                  top: rCloudTop,
                  left: rCloudLeft,
                  width: MOON_SIZE,
                  height: MOON_SIZE / 2,
                  alignSelf: 'flex-start',
                },
                styles.cloudImg,
              ]}
            />
          </View>
        </View>
        {full &&
          (isOther === false ||
            (isOther === true && this.props.isFull === true)) && (
            <View
              style={[
                {top: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? -45 : 0},
                styles.otherNoteModal,
                isOther === true && {width: WIDTH, left: (WIDTH / 2) * -1 + 55}
              ]}
            />
          )}
        <View
          style={{
            width: '100%',
            height: '100%',
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TouchableWithoutFeedback onPress={this.handlePress} disabled={full}>
            <Animated.View
              style={[{marginTop: 90, marginLeft: 0}, animatedStyle]}>
              <ImageBackground
                imageStyle={{width: this.w, height: this.h, marginLeft: -15}}
                style={[styles.noteBg, {height: this.h - 30}]}
                source={note_bg}>
                <View style={{width: this.w - 30, flex: 1}}>
                  <ScrollView style={[{marginTop: 35}, styles.textWrapper]}>
                    <View onStartShouldSetResponder={() => full}>
                      <Text
                        style={{
                          fontFamily: selectedFont,
                          fontSize: fontSize,
                          color: '#4A4A4A',
                        }}>
                        {selectedNote.note_orig !== ''
                          ? Utils.decrypt(selectedNote.note_orig)
                          : ''}
                      </Text>
                    </View>
                  </ScrollView>
                </View>
              </ImageBackground>
              {full === true && (
                <View
                  style={[
                    styles.otherNoteCloseIcon,
                    {width: '100%', alignSelf: 'center', marginLeft: 0},
                  ]}>
                  <TouchableOpacity onPress={this.onClose}>
                    <Image source={closeIcon} />
                  </TouchableOpacity>
                </View>
              )}
            </Animated.View>
          </TouchableWithoutFeedback>
        </View>

        {/* <Animated.View style={[{position: 'absolute', paddingHorizontal: 10, marginTop: -20}, (full === false) && {marginTop: 0, right: -70}, animatedStyle]}>
          <ImageBackground imageStyle={{width: this.w, height: this.h}} style={[styles.noteBg, {height: this.h-30}]} source={note_bg}>
            <View style={{width: this.w-30, flex: 1}}>
              <ScrollView style={[{marginTop: 35}, styles.textWrapper]}>
                <View onStartShouldSetResponder={() => full}>
                  <Text style={{fontFamily: selectedFont, fontSize: fontSize, color: '#4A4A4A'}}>{(selectedNote.note_orig !== '') ? Utils.decrypt(selectedNote.note_orig) : ''}</Text>
                </View>
              </ScrollView>
            </View>
          </ImageBackground>
          {
            full === true &&
            <View style={[styles.otherNoteCloseIcon, {width: '100%'}]}>
              <TouchableOpacity onPress={this.props.onClose}>
                <Image source={closeIcon}/>
              </TouchableOpacity>
            </View>
          }
        </Animated.View>           */}

        {/* <View style={[{top: (Utils.nativeBaseDoesHeaderCorrectlyAlready() ?  - 45 : 0)}, styles.otherNoteModal]}>
          <Animated.View style={[{position: 'absolute', paddingHorizontal: 10, marginTop: -20}, (full === false) && {marginTop: 0, right: -70}, animatedStyle]}>
            <ImageBackground imageStyle={{width: this.w, height: this.h}} style={[styles.noteBg, {height: this.h-30}]} source={note_bg}>
              <View style={{width: this.w-30, flex: 1}}>
                <ScrollView style={[{marginTop: 35}, styles.textWrapper]}>
                  <View onStartShouldSetResponder={() => full}>
                    <Text style={{fontFamily: selectedFont, fontSize: fontSize, color: '#4A4A4A'}}>{(selectedNote.note_orig !== '') ? Utils.decrypt(selectedNote.note_orig) : ''}</Text>
                  </View>
                </ScrollView>
              </View>
            </ImageBackground>
            {
              full === true &&
              <View style={[styles.otherNoteCloseIcon, {width: '100%'}]}>
                <TouchableOpacity onPress={this.props.onClose}>
                  <Image source={closeIcon}/>
                </TouchableOpacity>
              </View>
            }
          </Animated.View>
        </View> */}
      </View>
    );
  }
}
