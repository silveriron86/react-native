/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  FlatList,
  Text,
  TouchableWithoutFeedback,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import styles from '../_styles';
import Utils from '../../../utils';
import Colors from '../../../constants/Colors';
import GifLoader from '../../../widgets/GifLoader';

const noteArrowDown = require('../../../../assets/icons/note-arrow-down.png');
const noteArrowUp = require('../../../../assets/icons/note-arrow-up.png');

export default class VerticalStream extends React.Component {
  constructor(props) {
    super(props);

    this.verticalBackgrounds = [
      require('../../../../assets/images/vertical_bg/24503958_m-overlay_white-text_mine_blue-other_black.jpg'),
      require('../../../../assets/images/vertical_bg/47912739_m-overlay_white-text_mine_blue-other_black.jpg'),
      require('../../../../assets/images/vertical_bg/65798600_m-overlay_black-text_mine_blue-other_white.jpg'),
      require('../../../../assets/images/vertical_bg/65798600_m-overlay_white-text_mine_blue-other_white.jpg'),
      require('../../../../assets/images/vertical_bg/69583522_m-overlay_black-text_mine_blue-other_white.jpg'),
      require('../../../../assets/images/vertical_bg/69583522_m-overlay_white-text_mine_blue-other_white.jpg'),
      require('../../../../assets/images/vertical_bg/71708919_m-overlay_black-text_mine_blue-other_white.jpg'),
      require('../../../../assets/images/vertical_bg/71708919_m-overlay_white-text_mine_blue-other_black.jpg'),
    ];

    let index = Utils.getRandomInt(0, this.verticalBackgrounds.length - 1);
    this.state = {
      backgroundIndex: index,
    };
  }

  componentDidMount() {
    if (this.props.isOwn === true) {
      Utils.trackScreenView('User views their own stream, vertical');
    } else {
      Utils.trackScreenView('User views stream of another user, vertical');
    }
  }

  _renderTextItem = ({item, index}) => {
    const {streamFont, fontSize, isOwn} = this.props;
    const {backgroundIndex} = this.state;
    let selectedFont = streamFont;
    if (item.note.font) {
      selectedFont = item.note.font;
    }
    let selectedColor = Colors.scheme.blue;
    // if (item.note.color) {
    //   selectedColor = item.note.color;
    // }
    // console.log(item);
    if (isOwn === false) {
      // other notes
      selectedColor = 'white';
      if (backgroundIndex < 2 || backgroundIndex === 7) {
        selectedColor = 'black';
      }
    }

    return (
      <TouchableWithoutFeedback
        // onPress={() => this.selectNote(item, true)}
        style={styles.verticalStreamRow}>
        <>
          <View style={styles.verticalStreamHeader}>
            <View style={{flex: 1}}>
              <Text
                numberOfLines={1}
                style={[
                  styles.verticalStreamHeaderText,
                  {textAlign: 'left', color: selectedColor},
                ]}
              />
            </View>
            <Image style={{width: 25, height: 25}} source={noteArrowDown} />
            <View style={{flex: 1}}>
              <Text
                style={[
                  styles.verticalStreamHeaderText,
                  {textAlign: 'right', color: selectedColor},
                ]}>
                {item.note.note_tags &&
                  item.note.note_tags.map((entry, iii) => {
                    return <Text>#{entry} </Text>;
                  })}
              </Text>
            </View>
          </View>

          <View style={{padding: 10}}>
            <Text
              style={{
                color: selectedColor,
                fontFamily: selectedFont,
                fontSize: fontSize,
              }}>
              {Utils.decrypt(item.note.note_orig)}
            </Text>
            <Text
              style={[
                styles.verticalStreamHeaderText,
                {textAlign: 'right', color: selectedColor},
              ]}>
              --{item.note.screen_name ? item.note.screen_name : 'Anonymous'}
            </Text>
          </View>

          <View style={styles.verticalStreamHeader}>
            <View style={{flex: 1}}>
              <Text
                numberOfLines={1}
                style={[
                  styles.verticalStreamHeaderText,
                  {textAlign: 'left', color: selectedColor},
                ]}>
                {item.note.screen_name ? item.note.screen_name : 'Anonymous'}
              </Text>
            </View>
            <Image style={{width: 25, height: 25}} source={noteArrowUp} />
            <View style={{flex: 1}}>
              <Text
                numberOfLines={1}
                style={[
                  styles.verticalStreamHeaderText,
                  {textAlign: 'right', color: selectedColor},
                ]}>
                {moment(item.note.creation_timestamp).fromNow()}
              </Text>
            </View>
          </View>
        </>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    const WIDTH = Dimensions.get('window').width;
    const HEIGHT = Dimensions.get('window').height;
    const {slides, loading, index, onEndReached} = this.props;
    const {backgroundIndex} = this.state;

    return (
      <View style={{flexDirection: 'row'}}>
        <ImageBackground
          style={{width: '100%', height: '100%'}}
          imageStyle={{height: Dimensions.get('window').height}}
          source={this.verticalBackgrounds[backgroundIndex]}>
          <View
            style={{
              width: WIDTH,
              height: HEIGHT - 50,
            }}>
            {loading && (
              <View style={[styles.notesLoader, {height: '100%'}]}>
                <GifLoader />
              </View>
            )}
            {slides.length > 0 && (
              <FlatList
                ref={this.flatListRef}
                initialScrollIndex={index}
                data={slides}
                keyExtractor={(item, _index) => item._id}
                renderItem={this._renderTextItem}
                onEndReachedThreshold={0.05}
                onEndReached={onEndReached}
                onScrollToIndexFailed={error => {
                  console.log('******************', error, this.flatListRef);
                  if (
                    typeof this.flatListRef !== 'undefined' &&
                    this.flatListRef !== null
                  ) {
                    this.flatListRef.scrollToOffset({
                      offset: error.averageItemLength * error.index,
                      animated: true,
                    });
                  }
                  setTimeout(() => {
                    if (
                      slides.length !== 0 &&
                      typeof this.flatListRef !== 'undefined' &&
                      this.flatListRef !== null
                    ) {
                      this.flatListRef.scrollToIndex({
                        index: error.index,
                        animated: true,
                      });
                    }
                  }, 100);
                }}
              />
            )}
          </View>
          <TouchableOpacity
            style={[styles.readerSwitchBtn, {marginTop: 5}]}
            onPress={() => this.props.onShowStreams(2)}>
            <Text style={{color: 'white', fontSize: 35}}>...</Text>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  }
}
