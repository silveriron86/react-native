/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Animated,
  AppState,
  NativeModules,
} from 'react-native';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import DraggableFlatList from 'react-native-draggable-flatlist';
import styles from '../../_styles';
import Utils from '../../../../utils';
import Colors from '../../../../constants/Colors';
import ImageConstants from '../../../../constants/ImageConstants';
import ToDoFilter from './ToDoFilter';
import tdrStyles from './_styles';
import TodoComposeModal from './ComposeModal';
import LoadingOverlay from '../../../../widgets/LoadingOverlay';

const plusIcon = require('../../../../../assets/icons/circle-plus.png');
const starFilledIcon = require('../../../../../assets/icons/star-filled.png');
const starIcon = require('../../../../../assets/icons/star.png');
const ellipsesIcon = require('../../../../../assets/icons/dots3.png');
const closeIcon = require('../../../../../assets/icons/circle-plus.png');

export default class ToDoReader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      reload: false,
      isOpenedFilter: false,
      filterOptions: {
        isAll: true,
        isCompleted: false,
        selectedTags: [...props.tags[2].tags],
      },
      starredData: [],
      notStarredData: [],
      isComposeModal: false,
      loadingNotes: false,
      selectedItem: null,
      bgIndex: -1,
    };
    this.animatedRightMargin = new Animated.Value(170);
  }

  componentDidMount() {
    this.mounted = true;
    AppState.addEventListener('change', this._handleAppStateChange);
    global.eventEmitter.addListener('LOGOUT', (data) => {
      this.setState({
        starredData: [],
        notStarredData: [],
      });
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.accessToken !== prevProps.accessToken) {
      AsyncStorage.getItem('ACCESS_TOKEN', (_err, token) => {
        if (token) {
          AsyncStorage.getItem('TODO_FILTER', (__err, options) => {
            if (options) {
              this.setState(
                {
                  filterOptions: JSON.parse(options),
                },
                () => {
                  this._firstLoad();
                },
              );
            } else {
              this._firstLoad();
            }
          });
          this._setBackground();
        }
      });
    }
  }

  _firstLoad = () => {
    const {todoNotes} = this.props;
    if (todoNotes && todoNotes.length > 0) {
      this._reloadLists();
    } else {
      this._loadAll();
    }
  };

  componentWillUnmount() {
    this.mounted = false;
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (!this.mounted) {
      return;
    }

    if (nextAppState === 'active') {
      this._setBackground();
    }
  };

  _setBackground = () => {
    const tag = this.props.tags[2];
    let bgIndex = -1;
    if (typeof tag.background !== 'undefined' && tag.background.length > 0) {
      bgIndex =
        tag.background[Utils.getRandomInt(0, tag.background.length - 1)];

      if (tag.background.length > 1) {
        while (bgIndex === this.state.bgIndex) {
          bgIndex =
            tag.background[Utils.getRandomInt(0, tag.background.length - 1)];
        }
      }
    }
    this.setState({bgIndex});
  };

  _loadAll = () => {
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.getToDoNotes({
          page: 1,
          cb: (_res) => {
            this.setState({
              loading: false,
            });
            this._reloadLists();
          },
          size: 500,
        });
      },
    );
  };

  _setupLists = (props) => {
    let starredItems = [];
    let notStarredItems = [];
    let slides = [...props.todoNotes];
    if (slides.length === 0) {
      return [[], []];
    }

    const {tags} = this.props;
    if (typeof tags[2].sort !== 'undefined') {
      if (tags[2].sort.sortedStarred.length > 0) {
        tags[2].sort.sortedStarred.map((id, index) => {
          let found = slides.find((n) => n._id === id);
          if (found) {
            found.sort = index;
          }
        });
      }
      if (tags[2].sort.sortedUnStarred.length > 0) {
        tags[2].sort.sortedUnStarred.map((id, index) => {
          let found = slides.find((n) => n._id === id);
          if (found) {
            found.sort = index;
          }
        });
      }
    }

    slides.forEach((item) => {
      if (item.note.main_tag === 'todo' && item.archived !== true) {
        if (item.note.starred === true) {
          starredItems.push(item);
        } else {
          notStarredItems.push(item);
        }
      }
    });

    starredItems.sort((a, b) => a.sort - b.sort);
    notStarredItems.sort((a, b) => a.sort - b.sort);
    return [starredItems, notStarredItems];
  };

  _reloadLists = () => {
    const lists = this._setupLists(this.props);
    console.log('***************');
    console.log(lists);
    this.setState({
      starredData: [...lists[0]],
      notStarredData: [...lists[1]],
    });

    this._updateTodayWidget(lists);
  };

  _updateTodayWidget = (lists) => {
    if (lists[0].length > 0) {
      lists[0].map(
        (n) => (n.note.decrypted_note = Utils.decrypt(n.note.note_text)),
      );
      // if (lists[0].length > 5) {
      //   lists[0] = lists[0].slice(0, 5);
      // }
    }
    if (lists[1].length > 0) {
      lists[1].map(
        (n) => (n.note.decrypted_note = Utils.decrypt(n.note.note_text)),
      );
      // if (lists[1].length > 5) {
      //   lists[1] = lists[1].slice(0, 5);
      // }
    }

    console.log('************ TODO LIST ************');
    console.log({
      starredData: lists[0].filter((item) => this._filterItems(item)),
      notStarredData: lists[1].filter((item) => this._filterItems(item)),
    });
    const {IntentModule} = NativeModules;
    IntentModule.saveTodoNotes(
      JSON.stringify({
        starredData: lists[0].filter((item) => this._filterItems(item)),
        notStarredData: lists[1].filter((item) => this._filterItems(item)),
      }),
    );
  };

  _getParam = (note) => {
    return {
      background_image:
        typeof note.starred === 'undefined' ? -1 : note.background_image,
      color:
        typeof note.color === 'undefined' ? Colors.scheme.blue : note.color,
      starred: typeof note.starred === 'undefined' ? false : note.starred,
      completed: typeof note.completed === 'undefined' ? false : note.completed,
    };
  };

  onStar = (item, value) => {
    const index = this.props.todoNotes.findIndex(
      (slide) => slide._id === item._id,
    );
    this.props.todoNotes[index].note.starred = value;
    this.setState({
      reload: !this.state.reload,
    });
    let param = this._getParam(item.note);
    param.starred = value;
    this.props.updateNoteParam({
      note_id: item._id,
      param,
      cb: (res) => {
        this._reloadLists();
      },
    });
  };

  onComplete = (item) => {
    const slides = [...this.props.todoNotes];
    const index = slides.findIndex((slide) => slide._id === item._id);
    slides[index].note.completed =
      typeof slides[index].note.completed === 'undefined'
        ? true
        : !slides[index].note.completed;
    this.setState({
      reload: !this.state.reload,
    });

    let param = this._getParam(item.note);
    param.completed = slides[index].note.completed;
    this.props.updateNoteParam({
      note_id: item._id,
      param,
      cb: (res) => {
        this._reloadLists();
      },
    });
  };

  toggleComposeModal = () => {
    this.setState({
      isComposeModal: !this.state.isComposeModal,
    });
  };

  onToggleFilter = () => {
    const {isOpenedFilter} = this.state;

    Animated.timing(this.animatedRightMargin, {
      toValue: isOpenedFilter ? 170 : 0,
      duration: 300,
      useNativeDriver: true,
    }).start();

    this.setState({
      isOpenedFilter: !isOpenedFilter,
    });
  };

  onHandleFilter = (options) => {
    AsyncStorage.setItem('TODO_FILTER', JSON.stringify(options));
    this.setState(
      {
        filterOptions: options,
      },
      () => {
        this._updateTodayWidget(this._setupLists(this.props));
      },
    );
  };

  onEdit = (item) => {
    this.setState({selectedItem: item}, () => {
      this.toggleComposeModal();
    });
  };

  renderItem = ({item, drag}) => {
    return (
      <TouchableOpacity style={tdrStyles.todoTodayRow} onLongPress={drag}>
        <TouchableOpacity
          style={[tdrStyles.circleButton, {width: 42, height: 42}]}
          onPress={() => this.onComplete(item)}
          onLongPress={drag}>
          <View
            style={[
              tdrStyles.circleIcon,
              item.note.completed === true && {
                backgroundColor: 'white',
              },
            ]}
          />
        </TouchableOpacity>
        <View style={{flex: 1}}>
          <TouchableOpacity
            onPress={() => this.onEdit(item)}
            onLongPress={drag}>
            <Text style={tdrStyles.todoTodayText}>
              {Utils.decrypt(item.note.note_text)}
            </Text>
          </TouchableOpacity>
          <View style={tdrStyles.noteTags}>
            <Text style={tdrStyles.tagText}>
              {item.note.note_tags &&
                item.note.note_tags.map((entry, i) => {
                  return <Text key={`${item._id}-entry-${i}`}> #{entry}</Text>;
                })}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          style={tdrStyles.starFilledButton}
          onPress={() => this.onStar(item, false)}
          onLongPress={drag}>
          <Image source={starFilledIcon} style={tdrStyles.starFilledIcon} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  renderNotItem = ({item, drag}) => {
    return (
      <TouchableOpacity
        style={[tdrStyles.todoTodayRow, tdrStyles.todoLaterRow]}
        onLongPress={drag}>
        <TouchableOpacity
          style={[tdrStyles.circleButtonLater]}
          onPress={() => this.onComplete(item)}
          onLongPress={drag}>
          <View
            style={[
              tdrStyles.circleIconLater,
              item.note.completed === true && {
                backgroundColor: 'white',
              },
            ]}
          />
        </TouchableOpacity>
        <View style={{flex: 1}}>
          <TouchableOpacity
            onPress={() => this.onEdit(item)}
            onLongPress={drag}>
            <Text style={[tdrStyles.todoLaterText]}>
              {Utils.decrypt(item.note.note_text)}
            </Text>
          </TouchableOpacity>
          <View style={tdrStyles.noteTags}>
            <Text style={tdrStyles.tagText}>
              {item.note.note_tags &&
                item.note.note_tags.map((entry, i) => {
                  return <Text key={`${item._id}-entry-${i}`}> #{entry}</Text>;
                })}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          style={tdrStyles.starFilledButton}
          onPress={() => this.onStar(item, true)}
          onLongPress={drag}>
          <Image source={starIcon} style={tdrStyles.starIcon} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  _filterItems = (item) => {
    const {filterOptions} = this.state;
    let isIncluded = false;
    if (!filterOptions.isCompleted && item.note.completed) {
      return false;
    }

    if (filterOptions.isAll) {
      isIncluded = true;
    } else if (
      item.note.note_tags &&
      item.note.note_tags.length > 0 &&
      filterOptions.selectedTags.length > 0
    ) {
      item.note.note_tags.forEach((tag) => {
        if (filterOptions.selectedTags.indexOf(tag) >= 0) {
          isIncluded = true;
        }
      });
    }
    return isIncluded;
  };

  setStarredData = (data) => {
    this.setState(
      {
        starredData: data,
      },
      () => {
        this._updateSort();
      },
    );
  };

  setNotStarredData = (data) => {
    this.setState(
      {
        notStarredData: data,
      },
      () => {
        this._updateSort();
      },
    );
  };

  _updateSort = () => {
    const {starredData, notStarredData} = this.state;
    const sortedStarred = starredData.map((item) => item._id);
    const sortedUnStarred = notStarredData.map((item) => item._id);

    // this.props.updateTodoSort({
    //   data: {
    //     sortedStarred,
    //     sortedUnStarred,
    //   },
    //   cb: _res => {},
    // });

    const {tags} = this.props;
    let data = JSON.parse(JSON.stringify(tags));
    data[2].sort = {
      sortedStarred,
      sortedUnStarred,
    };
    this.props.setSettings({type: 'TAGS', value: data});
    setTimeout(() => {
      this._updateTodayWidget(this._setupLists(this.props));
    }, 1500);
  };

  onCreate = (args) => {
    this.setState({
      loadingNotes: true,
    });
    this.props.createNote({
      data: args,
      cb: (res) => {
        this._reloadNotesList(false);
      },
    });
  };

  onArchive = (args) => {
    this.setState({
      loadingNotes: true,
    });
    this.props.archiveNote({
      data: args,
      cb: (res) => {
        console.log(res);
        this._reloadNotesList(true);
      },
    });
  };

  _reloadNotesList = (isUpdate) => {
    this.props.getToDoNotes({
      page: 1,
      cb: (_res) => {
        if (isUpdate) {
          this.setState({
            isComposeModal: false,
          });
        }

        this.setState(
          {
            loadingNotes: false,
          },
          () => {
            this._reloadLists();
          },
        );
      },
      size: 500,
    });
  };

  onUpdate = (selectedNote, item) => {
    this.setState({
      loadingNotes: true,
    });
    this.props.createNote({
      data: selectedNote,
      cb: (r) => {
        this.props.archiveNote({
          data: {
            _id: item._id,
          },
          cb: (_res) => {
            this._reloadNotesList(true);
          },
        });
      },
    });
  };

  render() {
    const {
      starredData,
      notStarredData,
      isComposeModal,
      loadingNotes,
      selectedItem,
      loading,
      bgIndex,
    } = this.state;
    const {
      tags,
      location,
      streamFont,
      fontSize,
      noteVisibility,
      screenName,
      generalPreferences,
      setSettings,
    } = this.props;
    const WIDTH = Dimensions.get('window').width;
    console.log(starredData, notStarredData);

    return (
      <View style={{flex: 1}}>
        <ImageBackground
          style={{width: '100%', height: '100%'}}
          imageStyle={{height: Dimensions.get('window').height}}
          source={bgIndex >= 0 ? ImageConstants.notes[bgIndex] : null}>
          <View
            style={{
              width: WIDTH,
              height: Utils.getClientHeight() - 40,
            }}>
            <View style={tdrStyles.todayFocus}>
              <Text style={tdrStyles.todayFocusText}>Today's Focus</Text>
              <Text
                style={[
                  tdrStyles.todayFocusText,
                  tdrStyles.todayFocusDateText,
                ]}>
                {moment().format('dddd MMMM D')}
                {this.state.reload}
              </Text>
            </View>
            <View style={tdrStyles.completedList}>
              <DraggableFlatList
                data={starredData.filter((item) => this._filterItems(item))}
                renderItem={this.renderItem}
                keyExtractor={(item) =>
                  `draggable-item-${item._id}-${
                    typeof item.note.completed !== 'undefined'
                      ? item.note.completed
                      : false
                  }`
                }
                onDragEnd={({data}) => this.setStarredData(data)}
              />
            </View>
            <View style={tdrStyles.betweenPad} />
            <View style={{flex: 1, paddingBottom: 8}}>
              <DraggableFlatList
                data={notStarredData.filter((item) => this._filterItems(item))}
                renderItem={this.renderNotItem}
                keyExtractor={(item) =>
                  `draggable-not-item-${item._id}-${
                    typeof item.note.completed !== 'undefined'
                      ? item.note.completed
                      : false
                  }`
                }
                onDragEnd={({data}) => this.setNotStarredData(data)}
              />
            </View>
            <TouchableOpacity
              style={tdrStyles.plusButton}
              onPress={() => this.onEdit(null)}>
              <Image source={plusIcon} style={tdrStyles.plusIcon} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.onToggleFilter}
              style={styles.toggleButton}>
              <Image
                source={ellipsesIcon}
                resizeMethod={'resize'}
                style={styles.ellipsesIcon}
              />
            </TouchableOpacity>
            {this.state.isOpenedFilter && (
              <View style={tdrStyles.filterCloseView}>
                <TouchableOpacity
                  style={tdrStyles.closeButton}
                  onPress={this.onToggleFilter}>
                  <Image source={closeIcon} style={tdrStyles.closeIcon} />
                </TouchableOpacity>
              </View>
            )}
            <Animated.View
              style={[
                tdrStyles.filterWrapper,
                {transform: [{translateX: this.animatedRightMargin}]},
              ]}>
              <ToDoFilter tags={tags} onFilter={this.onHandleFilter} />
            </Animated.View>
          </View>
        </ImageBackground>
        <TodoComposeModal
          loading={loadingNotes}
          item={selectedItem}
          tags={tags}
          location={location}
          streamFont={streamFont}
          fontSize={fontSize}
          noteVisibility={noteVisibility}
          screenName={screenName}
          visible={isComposeModal}
          generalPreferences={generalPreferences}
          setSettings={setSettings}
          onCreate={this.onCreate}
          onUpdate={this.onUpdate}
          onArchive={this.onArchive}
          onClose={this.toggleComposeModal}
        />
        <LoadingOverlay loading={loading} />
      </View>
    );
  }
}
