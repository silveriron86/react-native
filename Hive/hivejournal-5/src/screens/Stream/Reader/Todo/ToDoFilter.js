/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../../../../constants/Colors';

export default class ToDoFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isCompleted: false,
      isAll: true,
      selectedTags: [...props.tags[2].tags],
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('TODO_FILTER', (_err, options) => {
      if (options) {
        this.setState(JSON.parse(options));
      }
    });
  }

  onToggleChecked = () => {
    this.setState(
      {
        isCompleted: !this.state.isCompleted,
      },
      () => {
        this.handleFilter();
      },
    );
  };

  onToggleAll = () => {
    const {isAll} = this.state;
    this.setState(
      {
        isAll: !isAll,
        selectedTags: isAll ? [] : [...this.props.tags[2].tags],
      },
      () => {
        this.handleFilter();
      },
    );
  };

  onSelectTag = (tag) => {
    let tags = [...this.state.selectedTags];
    let index = tags.indexOf(tag);
    console.log(index);
    if (index >= 0) {
      tags.splice(index, 1);
    } else {
      tags.push(tag);
    }

    this.setState(
      {
        selectedTags: tags,
        isAll: this.props.tags[2].tags.length === tags.length ? true : false,
      },
      () => {
        this.handleFilter();
      },
    );
  };

  handleFilter = () => {
    const {selectedTags, isCompleted, isAll} = this.state;
    this.props.onFilter({
      selectedTags,
      isCompleted,
      isAll,
    });
  };

  render() {
    const tags = this.props.tags[2].tags;
    const {isCompleted, isAll, selectedTags} = this.state;

    return (
      <ScrollView style={styles.container}>
        <View style={styles.tagRow}>
          <CheckBox
            title="Completed"
            textStyle={[
              styles.tagText,
              {color: Colors.scheme.blue, marginTop: 5},
            ]}
            containerStyle={{
              backgroundColor: 'transparent',
              borderWidth: 0,
              margin: 0,
              padding: 0,
            }}
            checked={isCompleted}
            checkedColor={Colors.scheme.blue}
            uncheckedColor={Colors.scheme.blue}
            onPress={this.onToggleChecked}
          />
        </View>
        <View style={styles.tagRow}>
          <CheckBox
            title="All Tags"
            textStyle={[
              styles.tagText,
              {color: Colors.scheme.blue, marginTop: 5},
            ]}
            containerStyle={{
              backgroundColor: 'transparent',
              borderWidth: 0,
              margin: 0,
              padding: 0,
            }}
            checked={isAll}
            checkedColor={Colors.scheme.blue}
            uncheckedColor={Colors.scheme.blue}
            onPress={this.onToggleAll}
          />
        </View>
        {tags.length > 0 &&
          [...new Set(tags)].map((tag) => {
            if (!tag) {
              return null;
            }
            return (
              <View style={styles.tagRow} key={`tag-${tag}`}>
                <TouchableOpacity
                  style={styles.tagButton}
                  onPress={() => this.onSelectTag(tag)}>
                  <Text
                    style={[
                      styles.tagText,
                      selectedTags.indexOf(tag) < 0 && {color: '#666'},
                    ]}>
                    #{tag}
                  </Text>
                </TouchableOpacity>
              </View>
            );
          })}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(13,13,13,0.9)',
    height: '100%',
    width: '100%',
    paddingHorizontal: 10,
    borderLeftWidth: 1,
    borderLeftColor: '#FFFFFF50',
  },
  tagRow: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#FFFFFF50',
    marginVertical: 3,
  },
  tagButton: {
    paddingVertical: 1,
    width: '100%',
  },
  tagText: {
    color: 'white',
    fontSize: 25,
    fontFamily: 'PremiumUltra26',
  },
});
