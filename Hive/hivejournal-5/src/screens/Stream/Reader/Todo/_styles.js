import {StyleSheet} from 'react-native';
import Colors from '../../../../constants/Colors';
import Utils from '../../../../utils';

export default StyleSheet.create({
  todayFocus: {
    backgroundColor: '#252525',
    paddingTop: 15,
    paddingBottom: 10,
    opacity: 0.9,
  },
  todayFocusText: {
    color: '#4fc7ff',
    fontSize: 45,
    fontFamily: 'PremiumUltra26',
    textAlign: 'center',
  },
  todayFocusDateText: {
    fontSize: 25,
  },
  switchBtn: {
    marginTop: 5,
    shadowOpacity: 0,
    backgroundColor: Colors.scheme.black,
  },
  plusButton: {
    position: 'absolute',
    bottom: 10,
    right: 10,
  },
  plusIcon: {
    width: 45,
    height: 45,
  },
  todoTodayRow: {
    backgroundColor: Colors.scheme.blue,
    borderRadius: 8,
    paddingVertical: 12,
    paddingRight: 24,
    marginTop: 8,
    marginHorizontal: 8,
    flexDirection: 'row',
    alignItems: 'center',
    opacity: 0.7,
  },
  todoTodayText: {
    color: 'white',
    fontFamily: 'PremiumUltra5',
    fontSize: 22,
    marginLeft: 50,
  },
  todoLaterText: {
    color: 'white',
    fontFamily: 'PremiumUltra5',
    fontSize: 18,
  },
  starFilledButton: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
  starFilledIcon: {
    width: 20,
    height: 20,
  },
  starIcon: {
    width: 15,
    height: 15,
  },
  circleButton: {
    position: 'absolute',
    top: 10,
    left: 10,
    width: 30,
    height: 30,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    zIndex: 999999,
  },
  circleButtonLater: {
    position: 'absolute',
    top: 10,
    left: 10,
    width: 42,
    height: 42,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  circleIcon: {
    width: 30,
    height: 30,
    borderWidth: 3,
    borderColor: 'white',
    borderRadius: 15,
  },
  circleIconLater: {
    width: 20,
    height: 20,
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 10,
  },
  todoLaterRow: {
    width: '80%',
    backgroundColor: '#000000',
    alignSelf: 'center',
    borderRadius: 5,
    paddingVertical: 8,
    paddingRight: 20,
    paddingLeft: 35,
    opacity: 0.45,
  },
  betweenPad: {
    backgroundColor: 'rgba(0, 0, 0, 0.16)',
    height: 20,
  },
  completedList: {
    backgroundColor: 'rgba(0, 0, 0, 0.16)',
    paddingBottom: 10,
    flex: 1,
  },
  noteTags: {
    flex: 1,
    alignItems: 'flex-end',
  },
  tagText: {
    textAlign: 'right',
    color: 'white',
    fontSize: 18,
    fontFamily: 'PremiumUltra26',
  },
  editTextInput: {
    backgroundColor: '#1F1F1F',
    height: 120,
    width: '100%',
    fontSize: 28,
    fontFamily: 'PremiumUltra5',
    paddingHorizontal: 15,
    paddingTop: 40,
    paddingBottom: 10,
    color: Colors.scheme.blue,
    borderRadius: 20,
  },
  modalContainer: {
    backgroundColor: '#000000',
    opacity: 0.9,
  },
  modal: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: 'transparent',
  },
  modalInner: {
    padding: 20,
    flexDirection: 'row',
  },
  modalWrapper: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  doneButton: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  doneButtonText: {
    fontSize: 20,
    fontFamily: 'PremiumUltra5',
    color: Colors.scheme.blue,
  },
  starIconButton: {
    position: 'absolute',
    right: 2,
    top: 2,
    padding: 10,
  },
  archiveButton: {
    height: 36,
    width: 36,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 5,
    marginTop: 2,
    marginLeft: 5,
  },
  filterWrapper: {
    position: 'absolute',
    zIndex: 9999,
    height: Utils.getClientHeight() - 80,
    width: 170,
    marginTop: 40,
    right: 0,
  },
  closeButton: {
    padding: 5,
  },
  closeIcon: {
    width: 45,
    height: 45,
    transform: [{rotate: '45deg'}],
  },
  filterCloseView: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
});
