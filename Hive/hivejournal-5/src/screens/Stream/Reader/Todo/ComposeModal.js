/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Keyboard,
  Alert,
} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import FooterTags from '../../../Notes/FooterTags';
import styles from './_styles';
import noteStyles from '../../../Notes/_styles';
import Utils from '../../../../utils';
import Colors from '../../../../constants/Colors';
import LoadingOverlay from '../../../../widgets/LoadingOverlay';

const starIcon = require('../../../../../assets/icons/star.png');
const starFilledIcon = require('../../../../../assets/icons/star-filled.png');
const archiveIcon = require('../../../../../assets/icons/archive.png');

export default class TodoComposeModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      body: '',
      insertedTags: [],
      newTags: [],
      isStarred: false,
      keyboardShown: true,
      selection: null,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.visible !== nextProps.visible &&
      nextProps.visible === true
    ) {
      console.log(nextProps.item);
      this.setState({
        isStarred:
          nextProps.item && typeof nextProps.item.note.starred !== 'undefined'
            ? nextProps.item.note.starred
            : false,
        body: nextProps.item
          ? Utils.decrypt(nextProps.item.note.note_text)
          : '',
        insertedTags:
          nextProps.item && nextProps.item.note.note_tags
            ? nextProps.item.note.note_tags
            : [],
        newTags: [],
      });
      setTimeout(() => {
        this.noteTextInput.focus();
      }, 150);
    }
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
  }

  _keyboardDidShow = () => {
    this.setState({
      keyboardShown: true,
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      keyboardShown: false,
    });
  };

  onInsertTag = (tag) => {
    console.log(tag);
    let insertedTags = JSON.parse(JSON.stringify(this.state.insertedTags));
    let found = insertedTags.indexOf(tag);
    if (found > -1) {
      insertedTags.splice(found, 1);
    } else {
      insertedTags.push(tag);
    }
    this.setState({
      insertedTags: insertedTags,
    });
  };

  onChangeText = (text) => {
    const {generalPreferences} = this.props;
    let chr = text.substr(this.state.selection.start - 1, 1);
    let code = chr.charCodeAt(0);
    if (generalPreferences.convertTagEnabled === true && code === 32) {
      // white space
      text = text.replace('  ', ' #');
      setTimeout(() => {
        this.setState(
          {
            body: text,
          },
          () => {
            this.updateNewTags();
          },
        );
      }, 1);
    } else {
      this.setState({
        body: text,
      });
    }
  };

  updateNewTags = () => {
    const {tags} = this.props;
    const {body, newTags, insertedTags} = this.state;
    let data = JSON.parse(JSON.stringify(newTags));
    let insertedTagsData = JSON.parse(JSON.stringify(insertedTags));
    let curTags = Utils.getTags(body);
    let updatedBody = body;

    if (curTags) {
      curTags.forEach((t) => {
        if (
          newTags.indexOf(t.substr(1)) === -1 &&
          tags[2].tags.indexOf(t.substr(1)) === -1
        ) {
          data.push(t.substr(1));
          insertedTagsData.push(t.substr(1));
        }

        let from = t + ' ';
        updatedBody = updatedBody.replace(from, '');

        this.setState({
          body: updatedBody,
          newTags: data,
          insertedTags: insertedTagsData,
        });
      });
    }
  };

  onStar = () => {
    this.setState({
      isStarred: !this.state.isStarred,
    });
  };

  onKeyPress = (e) => {
    if (!this.props.item) {
      const key = e.nativeEvent.key;
      if (key === 'Enter') {
        this.onCreate();
      }
    }
  };

  onDone = () => {
    // const {body} = this.state;
    Keyboard.dismiss();
    this.setState({
      body: '',
      insertedTags: [],
      newTags: [],
    });

    this.props.onClose();
  };

  onCreate = () => {
    const {body, insertedTags, isStarred} = this.state;
    const {
      location,
      noteVisibility,
      streamFont,
      screenName,
      fontSize,
    } = this.props;
    if (!body) {
      this.props.onClose();
    } else {
      const selectedTagIndex = 2;
      let origText = body;
      if (insertedTags && insertedTags.length > 0) {
        insertedTags.forEach((t) => {
          origText += ` #${t}`;
        });
      }
      let text = origText;
      let tags = Utils.getTags(origText);
      if (tags) {
        tags.forEach((tag, i) => {
          tags[i] = tag.substr(1);
          text = text.replace(tag + ' ', '');
        });
        text = text.replace('#' + tags[tags.length - 1], '');
      }
      // Keyboard.dismiss();

      AsyncStorage.getItem('USER_ID', (_err, user_id) => {
        let note = {
          note_orig: Utils.encrypt(origText),
          note_text: Utils.encrypt(text),
          note_tags: tags,
          creation_timestamp: moment().tz(location).format(),
          entry_ip: '0.0.0.0',
          entry_app_type: 'mobile',
          entry_app_version: '1.0',
          word_count: Utils.getWords(origText),
          font:
            typeof this.props.tags[selectedTagIndex].font === 'undefined'
              ? streamFont
              : this.props.tags[selectedTagIndex].font,
          font_size:
            typeof this.props.tags[selectedTagIndex].fontSize === 'undefined'
              ? fontSize
              : this.props.tags[selectedTagIndex].fontSize,
          color: Colors.scheme.blue,
          permissions: {
            see: noteVisibility.standard[0],
            read: noteVisibility.standard[1],
          },
          background_image: -1,
          screen_name: screenName,
          main_tag: 'todo',
          user_id: user_id,
          starred: isStarred,
        };
        this._saveJournalTags();
        this.props.onCreate(note);
        setTimeout(() => {
          this.setState({
            body: '',
          });
        }, 100);
      });
    }
  };

  _saveJournalTags = () => {
    const {newTags} = this.state;
    if (newTags.length > 0) {
      const {tags} = this.props;
      let data = JSON.parse(JSON.stringify(tags));
      newTags.forEach((t) => {
        data[2].tags.push(t);
      });
      this.props.setSettings({type: 'TAGS', value: data});
      this.setState({
        newTags: [],
      });
    }
  };

  onUpdate = () => {
    const {body, insertedTags, isStarred} = this.state;
    const {item} = this.props;

    let selectedNote = JSON.parse(JSON.stringify(item.note));
    let text = body;
    if (insertedTags && insertedTags.length > 0) {
      insertedTags.forEach((tag, i) => {
        text += ` #${tag}`;
      });
    }

    selectedNote.starred = isStarred;
    selectedNote.note_orig = Utils.encrypt(text);
    selectedNote.note_text = Utils.encrypt(body);
    selectedNote.note_tags = insertedTags;
    selectedNote.replacesNoteId = item._id;
    selectedNote.creation_timestamp = item.note.creation_timestamp;
    selectedNote.replacedNoteTimestamp = moment()
      .tz(this.props.location)
      .format();

    this._saveJournalTags();
    this.props.onUpdate(selectedNote, item);
  };

  onPressArchive = () => {
    Alert.alert(
      'Are you sure to archive this note?',
      '',
      [
        {
          text: 'Archive',
          onPress: this.handleArchive,
        },
        {
          text: 'Cancel',
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  };

  handleArchive = () => {
    Keyboard.dismiss();
    setTimeout(() => {
      this.props.onArchive(this.props.item);
    }, 50);
  };

  render() {
    const {visible, onClose, tags, streamFont, loading, item} = this.props;
    const {body, insertedTags, newTags, isStarred, keyboardShown} = this.state;
    const selectedTagIndex = 2;
    const fontSize = 20;
    const fontFamily =
      typeof tags[selectedTagIndex].font === 'undefined'
        ? streamFont
        : tags[selectedTagIndex].font;

    return (
      <ModalWrapper
        containerStyle={styles.modalContainer}
        style={styles.modal}
        onRequestClose={onClose}
        shouldAnimateOnRequestClose={true}
        shouldCloseOnOverlayPress={true}
        visible={visible}
        position={'top'}>
        <View style={styles.modalWrapper}>
          <View style={{marginVertical: 10}}>
            <FooterTags
              fontSize={fontSize}
              selectedIndex={selectedTagIndex}
              insertedTags={insertedTags}
              allTags={tags}
              newTags={newTags}
              note={body}
              insertTag={this.onInsertTag}
              isComposed={true}
              isTodo={true}
            />
          </View>
          <View style={{width: '100%', alignItems: 'flex-start'}}>
            <TextInput
              ref={(ref) => {
                this.noteTextInput = ref;
              }}
              style={[styles.editTextInput, {fontFamily: fontFamily}]}
              autoCapitalize={'none'}
              textAlignVertical="center"
              enablesReturnKeyAutomatically={true}
              multiline={true}
              value={body}
              onKeyPress={this.onKeyPress}
              onChangeText={this.onChangeText}
              onSelectionChange={({nativeEvent: {selection}}) => {
                this.setState({selection});
              }}
            />
            <TouchableOpacity
              style={styles.starIconButton}
              onPress={this.onStar}>
              {isStarred ? (
                <Image source={starFilledIcon} style={styles.starFilledIcon} />
              ) : (
                <Image source={starIcon} style={styles.starFilledIcon} />
              )}
            </TouchableOpacity>
          </View>
          {keyboardShown && (
            <>
              {item ? (
                <View
                  style={[
                    noteStyles.actionBtns,
                    {justifyContent: 'space-between'},
                  ]}>
                  <TouchableOpacity
                    style={styles.archiveButton}
                    onPress={this.onPressArchive}>
                    <Image
                      source={archiveIcon}
                      style={{width: 30, height: 30, tintColor: 'white'}}
                    />
                  </TouchableOpacity>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity
                      style={[noteStyles.actionBtn, noteStyles.saveBtn]}
                      onPress={this.onUpdate}>
                      <Text
                        style={[
                          noteStyles.actionBtnText,
                          noteStyles.saveBtnText,
                        ]}>
                        Save
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={noteStyles.actionBtn}
                      onPress={this.props.onClose}>
                      <Text style={noteStyles.actionBtnText}>Cancel</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              ) : (
                <TouchableOpacity
                  style={styles.doneButton}
                  onPress={this.onDone}>
                  <Text style={styles.doneButtonText}>
                    {body !== '' ? 'CANCEL' : 'DONE'}
                  </Text>
                </TouchableOpacity>
              )}
            </>
          )}
        </View>
        <LoadingOverlay loading={loading} containerStyle={{marginLeft: -10}} />
      </ModalWrapper>
    );
  }
}
