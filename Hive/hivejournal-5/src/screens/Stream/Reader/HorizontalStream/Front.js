/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  ScrollView,
  ImageBackground,
  Image,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
} from 'react-native';
import ImageProgress from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import ModalWrapper from 'react-native-modal-wrapper';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import Utils from '../../../../utils';
import styles from '../../_styles';
import ImageConstants from '../../../../constants/ImageConstants';
import Colors from '../../../../constants/Colors';
import FeatureButtons from './FeatureButtons';
import ImageViewer from '../../../../widgets/ImageViewer';
import ColorPicker from '../../../../widgets/ColorPicker';
import ImageSelector from '../../../Notes/ImageSelector';
import FooterTags from '../../../Notes/FooterTags';
import SaveCancel from '../../../Notes/SaveCancel';
import StreamSwitcher from '../StreamSwitcher';
import TagsModal from '../../../Notes/TagsModal';
import frontStyles from './_styles';
import AudioFilesList from './AudioFilesList';
import LoadingOverlay from '../../../../widgets/LoadingOverlay';
import FontEdit from '../../../Settings/FontEdit';
import * as RootNavigation from '../../../../navigation/RootNavigation';

const ellipsesIcon = require('../../../../../assets/icons/dots3.png');
const soundFileIcon = require('../../../../../assets/icons/sound-file_icon.png');
const plusIcon = require('../../../../../assets/icons/circle-plus.png');

export default class Front extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isClickedEllipses: false,
      isImageViewVisible: false,
      isPermModalVisible: false,
      isEditMode: false,
      inputNoteText: '',
      selectedColor:
        typeof props.item.note.color !== 'undefined'
          ? props.item.note.color
          : Colors.scheme.blue,
      selectedBackground: props.item.note.background_image,
      keyboardShown: false,
      isShowTagsModal: false,
      selectedMainTag: props.item.note.main_tag,
      updatedTagIndex: props.tags.findIndex((item) => {
        return item.type === props.item.note.main_tag;
      }),
      audioFiles: [],
      audioModalVisible: false,
      uploading: false,
      progress: 0,
      isFontModalVisible: false,
    };
  }

  _getAudioFiles = () => {
    AsyncStorage.getItem(`AUDIO_FILES_${this.props.item._id}`, (err, files) => {
      this.setState(
        {
          audioModalVisible: false,
          audioFiles: err ? [] : JSON.parse(files),
        },
        () => {
          // console.log(this.state.audioFiles);
        },
      );
    });
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.index !== nextProps.index) {
      this.setState({
        selectedColor:
          typeof nextProps.item.note.color !== 'undefined'
            ? nextProps.item.note.color
            : Colors.scheme.blue,
        selectedBackground: nextProps.item.note.background_image,
        isEditMode: false,
        selectedMainTag: nextProps.item.note.main_tag,
      });
      this._getAudioFiles();
    }
  }

  componentDidMount() {
    this._getAudioFiles();
    global.eventEmitter.addListener('COLOR_CHANGED', (data) => {
      this.setState({
        selectedColor:
          typeof data.color !== 'undefined' ? data.color : Colors.scheme.blue,
      });
    });

    global.eventEmitter.addListener('BACKGROUND_CHANGED', (data) => {
      this.setState({
        selectedBackground: data.background,
      });
    });

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );

    if (this.props.isTodo === true) {
      // this.toggleEllipses();
      // this.onToggleEdit();
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({
      keyboardShown: true,
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      keyboardShown: false,
    });
  };

  toggleEllipses = () => {
    const {isClickedEllipses} = this.state;
    this.setState({
      isEditMode: false,
      isClickedEllipses: !isClickedEllipses,
    });
  };

  onTogglePermissionModal = () => {
    this.setState({
      isPermModalVisible: !this.state.isPermModalVisible,
    });
  };

  onToggleImage = () => {
    this.setState({
      isImageViewVisible: !this.state.isImageViewVisible,
    });
  };

  onToggleEdit = () => {
    global.eventEmitter.emit('EDIT_MODE', {mode: !this.state.isEditMode});
    this.setState({
      inputNoteText: this.state.isEditMode
        ? ''
        : Utils.decrypt(this.props.item.note.note_text),
      isEditMode: !this.state.isEditMode,
    });
  };

  onDeleteImage = () => {
    this.props.updateNoteImage({
      note_id: this.props.item._id,
      image_url: '',
      cb: (res) => {
        console.log(res);
      },
    });
  };

  onToggleAudioModal = () => {
    this.setState({
      audioModalVisible: !this.state.audioModalVisible,
    });
  };

  onSaveNote = () => {
    const {
      inputNoteText,
      selectedColor,
      selectedBackground,
      selectedMainTag,
    } = this.state;
    const {item, insertedTags} = this.props;

    let selectedNote = JSON.parse(JSON.stringify(item.note));
    let text = inputNoteText;
    if (insertedTags && insertedTags.length > 0) {
      insertedTags.forEach((tag, i) => {
        text += ` #${tag}`;
      });
    }

    selectedNote.main_tag = selectedMainTag;
    selectedNote.note_orig = Utils.encrypt(text);
    selectedNote.note_text = Utils.encrypt(inputNoteText);
    selectedNote.note_tags = insertedTags;
    selectedNote.color = selectedColor;
    selectedNote.background_image = selectedBackground;
    selectedNote.replacesNoteId = item._id;
    selectedNote.creation_timestamp = item.note.creation_timestamp;
    selectedNote.replacedNoteTimestamp = moment()
      .tz(this.props.location)
      .format();

    this.props.setLoading(true);
    this.props.createNote({
      data: selectedNote,
      cb: (r) => {
        console.log(r);
        this.props.archiveNote({
          data: {
            _id: item._id,
          },
          cb: (_res) => {
            console.log(_res);
            global.eventEmitter.emit('NOTE_CREATED', {});
            this.props.setLoading(false);
            this.props.initNote();
          },
        });
      },
    });
  };

  onCancelInput = () => {
    Keyboard.dismiss();
    this.onToggleEdit();
  };

  _getMainTag = (tagName) => {
    let ret = 'j';
    this.props.tags.forEach((item) => {
      if (item.type === tagName) {
        ret = item.short;
      }
    });
    return ret;
  };

  onShowTagsModal = () => {
    const {isShowTagsModal, isEditMode} = this.state;
    if (!isEditMode) {
      return;
    }

    if (isShowTagsModal === false) {
      Keyboard.dismiss();
    }
    setTimeout(() => {
      this.setState({
        isShowTagsModal: !isShowTagsModal,
      });
    }, 100);
  };

  onSelectTag = (index) => {
    this.setState({
      updatedTagIndex: index,
      selectedMainTag: this.props.tags[index].type,
      isShowTagsModal: false,
    });
  };

  onUploading = (uploading, progress) => {
    this.setState({uploading, progress});
  };

  onTryEdit = () => {
    this.onToggleEdit();
    setTimeout(() => {
      this.noteTextInput.focus();
    }, 50);
  };

  onToggleFontModal = () => {
    this.setState({
      isFontModalVisible: !this.state.isFontModalVisible,
    });
  };

  onSetFontSize = () => {};
  onSetFont = () => {};

  render() {
    const {
      isClickedEllipses,
      isImageViewVisible,
      isEditMode,
      inputNoteText,
      selectedColor,
      selectedBackground,
      keyboardShown,
      isShowTagsModal,
      updatedTagIndex,
      audioFiles,
      audioModalVisible,
      uploading,
      progress,
      isFontModalVisible,
    } = this.state;
    const {
      streamFont,
      fontSize,
      item,
      generalPreferences,
      insertedTags,
      fontColor,
      background,
      newTags,
      tags,
      body,
      selectedTagIndex,
    } = this.props;
    const {width, height} = Dimensions.get('window');
    let selectedFont = streamFont;
    let selectedFontSize = this.props.fontSize;

    if (typeof item.note.font !== 'undefined') {
      selectedFont = item.note.font;
    }

    if (typeof item.note.font_size !== 'undefined') {
      selectedFontSize = item.note.font_size;
    }

    let isVisibleFooter = RootNavigation.getVisibleFooter();

    let verticalOffset = isVisibleFooter ? 68 : 20;
    if (Utils.getInsetBottom() > 0) {
      verticalOffset = isVisibleFooter ? 90 : 42;
    }

    let mainTagText = 'j';
    if (isEditMode) {
      console.log('updatedTagIndex = ', updatedTagIndex);
      if (updatedTagIndex >= 0) {
        mainTagText = tags[updatedTagIndex].short;
      }
    } else if (item.note.main_tag) {
      mainTagText = this._getMainTag(item.note.main_tag);
    }

    const w = Dimensions.get('window').width;
    const h = Dimensions.get('window').height;

    return (
      <KeyboardAvoidingView
        style={{
          width: width,
          height: Utils.getClientHeight() - 38,
        }}
        behavior="padding"
        keyboardVerticalOffset={Platform.select({
          ios: verticalOffset,
          android: verticalOffset,
        })}
        enabled>
        <View
          style={{
            height: '100%',
            backgroundColor: 'black',
          }}>
          <ImageBackground
            style={{
              width: '100%',
              height: '100%',
              paddingLeft: 20,
              paddingTop: 5,
            }}
            imageStyle={{
              width: width,
              height: height,
              resizeMode: 'cover',
              opacity: 0.4,
            }}
            source={
              selectedBackground > -1
                ? ImageConstants.notes[selectedBackground]
                : null
            }>
            <View style={{height: 45}} />
            <View
              style={{
                flex: 1,
                borderLeftWidth: 1,
                borderLeftColor: Colors.scheme.blue,
                marginBottom: !keyboardShown && isEditMode ? 45 : 0,
                opacity: isClickedEllipses ? 0.3 : 1,
              }}>
              <View
                style={[
                  styles.mainTagView,
                  isClickedEllipses === true && {opacity: 0.3},
                ]}>
                <TouchableOpacity
                  onPress={this.onShowTagsModal}
                  style={styles.mainTagButton}
                  disabled={!isEditMode}>
                  <Text
                    style={[
                      styles.mainTagText,
                      {fontSize: fontSize, color: selectedColor},
                    ]}>
                    #{mainTagText}
                  </Text>
                </TouchableOpacity>
                <TagsModal
                  allTags={tags}
                  visible={isShowTagsModal}
                  onCloseModal={this.onShowTagsModal}
                  onSelectTag={this.onSelectTag}
                  fontSize={fontSize}
                />
              </View>
              {isEditMode === true && (
                <View
                  style={{
                    flex: 1,
                    padding: 0,
                    borderColor: 'white',
                    paddingHorizontal: 10,
                  }}>
                  <TextInput
                    ref={(ref) => {
                      this.noteTextInput = ref;
                    }}
                    multiline={true}
                    value={inputNoteText}
                    style={{
                      fontFamily: selectedFont,
                      fontSize: selectedFontSize,
                      color: selectedColor,
                      height: '100%',
                    }}
                    onChangeText={(v) => {
                      this.setState({inputNoteText: v});
                    }}
                  />
                </View>
              )}
              {(isEditMode === false || isClickedEllipses === true) && (
                <ScrollView
                  style={[
                    {flex: 1, paddingHorizontal: 10, paddingTop: 5},
                    isClickedEllipses === true && {opacity: 0.3},
                  ]}>
                  <View>
                    <Text
                      style={{
                        fontFamily: selectedFont,
                        fontSize: selectedFontSize,
                        color: selectedColor,
                      }}
                      onPress={this.onTryEdit}>
                      {typeof item.note.note_text !== 'undefined' &&
                      item.note.note_text !== ''
                        ? Utils.decrypt(item.note.note_text)
                        : Utils.decrypt(item.note.note_orig)}
                    </Text>
                  </View>
                </ScrollView>
              )}
              <TouchableOpacity
                onPress={this.onToggleImage}
                style={{alignSelf: 'flex-end', margin: 10}}>
                {typeof item.imageURL !== 'undefined' && item.imageURL !== '' && (
                  <ImageProgress
                    source={{uri: item.imageURL}}
                    indicator={Progress.Circle}
                    indicatorProps={{
                      size: 30,
                      borderWidth: 2,
                      color: '#3079c1',
                      unfilledColor: 'rgba(200, 200, 200, 0.2)',
                    }}
                    style={{
                      width: 40,
                      height: 40,
                      resizeMode: 'contain',
                    }}
                  />
                )}
              </TouchableOpacity>
              {isEditMode === false && (
                <>
                  {audioFiles && audioFiles.length > 0 && (
                    <TouchableOpacity
                      style={frontStyles.audioFilesBtn}
                      onPress={this.onToggleAudioModal}>
                      <Image
                        source={soundFileIcon}
                        style={frontStyles.soundFileIcon}
                      />
                      <Text style={frontStyles.audioSavedText}>
                        [{audioFiles.length}] Audio Files
                      </Text>
                    </TouchableOpacity>
                  )}
                  {!isClickedEllipses && (
                    <View style={styles.childTags}>
                      {item.note.note_tags &&
                        item.note.note_tags.length > 0 &&
                        [...new Set(item.note.note_tags)].map((entry) => {
                          return (
                            <Text
                              key={`entry-${entry}`}
                              style={styles.footerTagText}>
                              #{entry}
                            </Text>
                          );
                        })}
                    </View>
                  )}
                  <View
                    style={[
                      frontStyles.plusButtonWrapper,
                      isClickedEllipses && {opacity: 0},
                    ]}>
                    {/*<Text
                      style={[
                        styles.otherNoteScreenNameText,
                        {textAlign: 'right', marginRight: 10},
                      ]}>
                      {item.note.screen_name
                        ? item.note.screen_name
                        : 'Anonymous'}
                    </Text>
                    */}
                    <TouchableOpacity
                      // style={tdrStyles.plusButton}
                      onPress={this.props.goComposeMode}>
                      <Image source={plusIcon} style={frontStyles.plusIcon} />
                    </TouchableOpacity>
                  </View>
                </>
              )}
            </View>

            {isEditMode && (
              <View style={{marginLeft: -20}}>
                <SaveCancel
                  generalPreferences={generalPreferences}
                  onSave={this.onSaveNote}
                  onCancel={this.onCancelInput}
                />
              </View>
            )}
          </ImageBackground>
        </View>
        {isClickedEllipses && !isEditMode && (
          <View style={frontStyles.mainNoteOptions}>
            <View style={{flex: 1}}>
              <View style={{height: 50}} />
              <View style={{height: '18%'}}>
                <FeatureButtons
                  position={'top'}
                  item={item}
                  onEdit={this.onToggleEdit}
                  isEditMode={isEditMode}
                  {...this.props}
                  onUpdate={this.onUploading}
                />
              </View>
              <View style={frontStyles.fontColorSelector}>
                <TouchableOpacity
                  style={{marginRight: 5}}
                  onPress={this.onToggleFontModal}>
                  <Text style={frontStyles.fontButtonText}>Font</Text>
                </TouchableOpacity>
                <View style={{width: 100, paddingRight: 10}}>
                  <ColorPicker
                    color={fontColor}
                    onPickColor={this.props.onPickColor}
                    canPickBlack={background !== -1}
                    vertical={true}
                  />
                </View>
              </View>
              <ImageSelector
                value={background}
                vertical={true}
                onSelect={this.props.onSelectBackground}
              />
              <View style={{flex: 1}}>
                <FeatureButtons
                  position={'bottom'}
                  item={item}
                  onEdit={this.onToggleEdit}
                  isEditMode={isEditMode}
                  {...this.props}
                  onUpdate={this.onUploading}
                />
              </View>
            </View>
            <View style={frontStyles.rightTagsSelector}>
              <FooterTags
                fontSize={fontSize}
                selectedIndex={selectedTagIndex}
                insertedTags={insertedTags}
                allTags={tags}
                newTags={newTags}
                note={body}
                insertTag={this.props.onInsertTag}
                vertical={true}
              />
              <View style={{padding: 10}}>
                <TouchableOpacity onPress={this.props.goComposeMode}>
                  <Image source={plusIcon} style={frontStyles.plusIcon} />
                </TouchableOpacity>
              </View>
            </View>

            <ModalWrapper
              containerStyle={[styles.modalContainer, {marginTop: 0}]}
              style={[styles.modal, {width: w, height: h}]}
              onRequestClose={this.onToggleFontModal}
              shouldAnimateOnRequestClose={true}
              shouldCloseOnOverlayPress={true}
              visible={isFontModalVisible}
              position={'right'}>
              <View style={{flex: 1}}>
                <FontEdit
                  onClose={this.onToggleFontModal}
                  isModal={true}
                  tagFont={
                    typeof tags[selectedTagIndex].font === 'undefined'
                      ? streamFont
                      : tags[selectedTagIndex].font
                  }
                  tagFontSize={fontSize}
                  setFontSize={this.onSetFontSize}
                  setFont={this.onSetFont}
                />
              </View>
            </ModalWrapper>
          </View>
        )}
        {!isClickedEllipses && (
          <>
            {this.props.alignType === 2 && (
              <TouchableOpacity
                style={styles.readerSwitchBtn}
                onPress={() => this.props.onShowStreams(1)}>
                <Text style={{color: 'white', fontSize: 35}}>...</Text>
              </TouchableOpacity>
            )}
          </>
        )}
        <LoadingOverlay
          loading={uploading}
          message={`Uploading: ${progress.toFixed(0)}%`}
        />
        {typeof item.imageURL !== 'undefined' && item.imageURL !== '' && (
          <ImageViewer
            url={item.imageURL}
            visible={isImageViewVisible}
            onToggle={this.onToggleImage}
            onDelete={this.onDeleteImage}
          />
        )}

        <StreamSwitcher
          alignType={this.props.alignType}
          large={isClickedEllipses}
          onCloseModal={
            isClickedEllipses ? this.toggleEllipses : this.props.initNote
          }
          onShowStreams={this.props.onShowStreams}
        />

        <TouchableOpacity
          onPress={this.toggleEllipses}
          style={styles.toggleButton}>
          <Image
            source={ellipsesIcon}
            resizeMethod={'resize'}
            style={styles.ellipsesIcon}
          />
        </TouchableOpacity>

        <AudioFilesList
          visible={audioModalVisible}
          files={audioFiles}
          onClose={this.onToggleAudioModal}
        />
      </KeyboardAvoidingView>
    );
  }
}
