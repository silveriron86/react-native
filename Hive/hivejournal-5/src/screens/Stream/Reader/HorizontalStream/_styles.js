import {StyleSheet, Dimensions} from 'react-native';
import Utils from '../../../../utils';
import Colors from '../../../../constants/Colors';

export default StyleSheet.create({
  container: {
    width: Dimensions.get('window').width - 120,
    height: '100%',
    left: 0,
    marginTop: 0,
    paddingRight: 10,
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  wrapper: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  selectWrapper: {
    borderWidth: 1,
    borderColor: '#666',
    width: 180,
    marginTop: 5,
    marginBottom: 10,
  },
  soundFileIcon: {
    tintColor: 'white',
    width: 22,
    height: 22,
  },
  audioSavedText: {
    fontFamily: 'PremiumUltra26',
    fontSize: 20,
    marginLeft: 5,
    color: 'white',
  },
  audioFilesBtn: {
    // position: 'absolute',
    // bottom: -20,
    // left: 30,
    flexDirection: 'row',
  },
  audioFileText: {
    fontFamily: 'PremiumUltra26',
    fontSize: 22,
    marginTop: 7,
    marginLeft: 5,
  },
  modalWrapper: {
    backgroundColor: 'white',
    width: '100%',
    height: 300,
    position: 'absolute',
    bottom: Utils.getInsetBottom() + 40,
    borderRadius: 5,
    padding: 20,
  },
  audioFileRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 3,
  },
  plusButtonWrapper: {
    paddingBottom: 10,
    paddingRight: 10,
    alignItems: 'flex-end',
  },
  plusIcon: {
    width: 45,
    height: 45,
    tintColor: 'rgba(255, 255, 255, 0.6)',
  },
  saveOnEnter: {
    // backgroundColor: 'rgba(0,0,0,0.3)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  saveOnEnterText: {
    color: 'white',
    fontFamily: 'PremiumUltra5',
    fontSize: 18,
    // marginLeft: -15,
    marginRight: 8,
  },
  featureIcon: {
    width: 34,
    resizeMode: 'contain',
  },
  mainNoteOptions: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  fontButtonText: {
    color: Colors.scheme.blue,
    fontFamily: 'PremiumUltra5',
    fontSize: 18,
  },
  fontColorSelector: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  rightTagsSelector: {
    width: 120,
    height: '100%',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
});
