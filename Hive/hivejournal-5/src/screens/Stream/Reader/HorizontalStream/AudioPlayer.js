/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  Text,
  Slider,
  TouchableOpacity,
  Platform,
  Alert,
} from 'react-native';

import Sound from 'react-native-sound';
const img_pause = require('../../../../../node_modules/react-native-sound-playerview/resources/ui_pause.png');
const img_play = require('../../../../../node_modules/react-native-sound-playerview/resources/ui_play.png');

export default class AudioPlayer extends React.Component {
  constructor() {
    super();
    this.state = {
      playState: 'paused', //playing, paused
      playSeconds: 0,
      duration: 0,
    };
    this.sliderEditing = false;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.filepath !== nextProps.filepath) {
      this.setState(
        {
          playState: 'paused', //playing, paused
          playSeconds: 0,
          duration: 0,
        },
        () => {
          if (this.sound) {
            this.sound.release();
          }
          this.sound = null;
          this._initSounder();
        },
      );
    }
  }

  componentDidMount() {
    this.play();
    this._initSounder();
  }

  _initSounder = () => {
    this.timeout = setInterval(() => {
      if (
        this.sound &&
        this.sound.isLoaded() &&
        this.state.playState === 'playing' &&
        !this.sliderEditing
      ) {
        this.sound.getCurrentTime((seconds, isPlaying) => {
          this.setState({playSeconds: seconds});
        });
      }
    }, 100);
  };

  componentWillUnmount() {
    if (this.sound) {
      this.sound.release();
      this.sound = null;
    }
    if (this.timeout) {
      clearInterval(this.timeout);
    }
  }

  onSliderEditStart = () => {
    this.sliderEditing = true;
  };
  onSliderEditEnd = () => {
    this.sliderEditing = false;
  };
  onSliderEditing = value => {
    if (this.sound) {
      this.sound.setCurrentTime(value);
      this.setState({playSeconds: value});
    }
  };

  play = async () => {
    if (this.sound) {
      this.sound.play(this.playComplete);
      this.setState({playState: 'playing'});
    } else {
      const filepath = this.props.filepath;
      console.log('[Play]', filepath);

      this.sound = new Sound(filepath, '', error => {
        if (error) {
          console.log('failed to load the sound', error);
          Alert.alert('Notice', 'audio file error. (Error code : 1)');
          this.setState({playState: 'paused'});
        } else {
          this.setState({
            playState: 'playing',
            duration: this.sound.getDuration(),
          });
          this.sound.play(this.playComplete);
        }
      });
    }
  };
  playComplete = success => {
    if (this.sound) {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
        Alert.alert('Notice', 'audio file error. (Error code : 2)');
      }
      this.setState({playState: 'paused', playSeconds: 0});
      this.sound.setCurrentTime(0);
    }
  };

  pause = () => {
    if (this.sound) {
      this.sound.pause();
    }

    this.setState({playState: 'paused'});
  };

  getAudioTimeString(seconds) {
    const h = parseInt(seconds / (60 * 60), 10);
    const m = parseInt((seconds % (60 * 60)) / 60, 10);
    const s = parseInt(seconds % 60, 10);

    return (
      (h < 10 ? '0' + h : h) +
      ':' +
      (m < 10 ? '0' + m : m) +
      ':' +
      (s < 10 ? '0' + s : s)
    );
  }

  render() {
    const currentTimeString = this.getAudioTimeString(this.state.playSeconds);
    const durationString = this.getAudioTimeString(this.state.duration);

    return (
      <View
        style={{
          height: 100,
          justifyContent: 'center',
          backgroundColor: 'black',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginVertical: 10,
          }}>
          {this.state.playState === 'playing' && (
            <TouchableOpacity
              onPress={this.pause}
              style={{marginHorizontal: 20}}>
              <Image source={img_pause} style={{width: 30, height: 30}} />
            </TouchableOpacity>
          )}
          {this.state.playState === 'paused' && (
            <TouchableOpacity
              onPress={this.play}
              style={{marginHorizontal: 20}}>
              <Image source={img_play} style={{width: 30, height: 30}} />
            </TouchableOpacity>
          )}
        </View>
        <View
          style={{
            marginHorizontal: 15,
            flexDirection: 'row',
          }}>
          <Text style={{color: 'white', alignSelf: 'center'}}>
            {currentTimeString}
          </Text>
          <Slider
            onTouchStart={this.onSliderEditStart}
            // onTouchMove={() => console.log('onTouchMove')}
            onTouchEnd={this.onSliderEditEnd}
            // onTouchEndCapture={() => console.log('onTouchEndCapture')}
            // onTouchCancel={() => console.log('onTouchCancel')}
            onValueChange={this.onSliderEditing}
            value={this.state.playSeconds}
            maximumValue={this.state.duration}
            maximumTrackTintColor="gray"
            minimumTrackTintColor="white"
            thumbTintColor="white"
            style={{
              flex: 1,
              alignSelf: 'center',
              marginHorizontal: Platform.select({ios: 5}),
            }}
          />
          <Text style={{color: 'white', alignSelf: 'center'}}>
            {durationString}
          </Text>
        </View>
      </View>
    );
  }
}
