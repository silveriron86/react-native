/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {ScrollView, Image, TouchableOpacity, Text} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import styles from './_styles';
import AudioPlayer from './AudioPlayer';
import Colors from '../../../../constants/Colors';

const closeIcon = require('../../../../../assets/icons/glyph_close_32x32.png');
const playIcon = require('../../../../../assets/icons/play.png');

export default class AudioFilesList extends React.Component {
  state = {
    filepath: '',
  };

  onPlay = (file) => {
    this.setState({
      filepath: file,
    });
  };

  render() {
    const {visible, files, onClose} = this.props;
    const {filepath} = this.state;

    return (
      <ModalWrapper
        style={styles.modalWrapper}
        onRequestClose={onClose}
        shouldAnimateOnRequestClose={true}
        shouldCloseOnOverlayPress={true}
        visible={visible}
        position={'bottom'}>
        <ScrollView style={{flex: 1}}>
          {files &&
            files.map((file, index) => {
              let tmp = file.split('_');
              return (
                <TouchableOpacity
                  style={styles.audioFileRow}
                  onPress={() => this.onPlay(file)}>
                  <Image
                    source={playIcon}
                    style={{
                      tintColor:
                        filepath === file ? Colors.scheme.blue : '#333',
                      width: 30,
                      height: 30,
                    }}
                  />
                  <Text
                    style={[
                      styles.audioFileText,
                      filepath === file && {color: Colors.scheme.blue},
                    ]}>
                    {tmp[tmp.length - 1]}
                  </Text>
                </TouchableOpacity>
              );
            })}
        </ScrollView>
        {filepath !== '' && <AudioPlayer filepath={filepath} />}
        <TouchableOpacity
          style={{position: 'absolute', top: 10, right: 10}}
          onPress={onClose}>
          <Image source={closeIcon} style={{tintColor: '#333'}} />
        </TouchableOpacity>
      </ModalWrapper>
    );
  }
}
