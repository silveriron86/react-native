/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  Alert,
  // TextInput,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Share from 'react-native-share';
import storage from '@react-native-firebase/storage';
import uuid from 'uuid/v4';
import {Chevron} from 'react-native-shapes';
import ModalWrapper from 'react-native-modal-wrapper';
import RNPickerSelect from 'react-native-picker-select';
import {CheckBox as CheckBoxElement} from 'react-native-elements';
import styles from '../../_styles';
import fStyles from './_styles';
import Utils from '../../../../utils';
import SettingConstants from '../../../../constants/SettingConstants';
import {selectStyle, selectTextInputStyle} from '../../../Settings/_styles';

const archiveIcon = require('../../../../../assets/icons/archive.png');
const lockIcon = require('../../../../../assets/icons/lock_new.png');
const addPhotoIcon = require('../../../../../assets/icons/camera-white-1.png');
const shareIcon = require('../../../../../assets/icons/share.png');

export default class FeatureButtons extends React.Component {
  constructor(props) {
    super(props);
    this.completed = false;
    this.state = {
      loading: false,
      canSee: 'any',
      canRead: 'any',
      imageUri: '',
      progress: 0,
      isImageViewVisible: false,
      uploading: false,
      isPermModalVisible: false,
    };
  }

  _setPermission = (item) => {
    if (item) {
      const {noteVisibility} = this.props;
      let type = 'standard';
      if (item.note.type) {
        type = item.note.type;
      }

      this.setState({
        canSee: item.note.permissions
          ? item.note.permissions.see
          : noteVisibility[type][0],
        canRead: item.note.permissions
          ? item.note.permissions.read
          : noteVisibility[type][1],
      });
    } else {
      const {permissions} = this.props;
      this.setState({
        canSee: permissions.see,
        canRead: permissions.read,
      });
    }
  };

  onPressTrash = () => {
    Alert.alert(
      'Are you sure to archive this note?',
      '',
      [
        {
          text: 'Archive',
          onPress: this.handleArchive,
        },
        {
          text: 'Cancel',
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  };

  handleArchive = () => {
    this.props.setLoading(true);

    this.props.archiveNote({
      data: this.props.item,
      cb: () => {
        global.eventEmitter.emit('NOTE_CREATED', {});
        this.props.setLoading(false);
      },
    });
  };

  handleLock = () => {
    const {item, onSelectPermissions} = this.props;
    if (!item) {
      this.setState({
        isPermModalVisible: false,
      });
      onSelectPermissions({
        see: this.state.canSee,
        read: this.state.canRead,
      });
      return;
    }

    this.setState(
      {
        isPermModalVisible: false,
      },
      () => {
        this.props.setLoading(true);
        this.props.updateNotePermissions({
          data: item,
          permissions: {
            see: this.state.canSee,
            read: this.state.canRead,
          },
          cb: () => {
            this.props.setLoading(false);
          },
        });
      },
    );
  };

  handleShare = () => {
    const {item} = this.props;
    Share.open({
      title: 'Share via',
      message: Utils.decrypt(item.note.note_orig),
      // url: `https://hivejournal.com/note/${item._id}`,
    });
  };

  handleAddPhoto = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        // alert('You cancelled image picker 😟');
      } else if (response.error) {
        console.log('And error occured: ', response.error);
      } else {
        this.completed = false;
        this.props.onUpdate(true, 0);
        this.setState({
          imageUri: response.uri,
        });
        const ext = this.state.imageUri.split('.').pop(); // Extract image extension
        const filename = `${uuid()}.${ext}`; // Generate unique name
        this.setState({uploading: true});

        const imageRef = storage().ref(`notes/images/${filename}`);
        imageRef.putFile(this.state.imageUri).on(
          storage.TaskEvent.STATE_CHANGED,
          (snapshot) => {
            let state = {};
            state = {
              ...this.state,
              progress: (snapshot.bytesTransferred / snapshot.totalBytes) * 100, // Calculate progress percentage
            };
            if (snapshot.state === storage.TaskState.SUCCESS) {
              this.completed = true;
              const {item} = this.props;
              if (item) {
                this.props.updateNoteImage({
                  note_id: item._id,
                  image_url: snapshot.downloadURL,
                  cb: () => {
                    state = {
                      ...state,
                      uploading: false,
                      imageUri: '',
                      progress: 0,
                    };
                    this.setState(state);
                    this.props.onUpdate(false, 0);
                  },
                });
              } else {
                this.setState({
                  uploading: false,
                  imageUri: '',
                  progress: 0,
                });

                imageRef
                  .getDownloadURL()
                  .then((url) => {
                    this.props.onSelectImage(url);
                    this.props.onUpdate(false, 0);
                  })
                  .catch((error) => {
                    this.props.onUpdate(false, 0);
                  });
              }
            } else {
              if (this.completed === false) {
                this.setState(state);
                this.props.onUpdate(state.uploading, state.progress);
              }
            }
          },
          () => {
            // unsubscribe();
            // eslint-disable-next-line no-alert
            alert('Sorry, Try again.');
          },
        );
      }
    });
  };

  handleDelete = () => {
    let {failedQueue, item} = this.props;
    failedQueue.forEach((element, index) => {
      if (element._id === item._id) {
        failedQueue.splice(index, 1);
        return this.props.setFailedQueue({
          data: failedQueue,
        });
      }
    });
  };

  onTogglePermissionModal = () => {
    this.setState({
      isPermModalVisible: !this.state.isPermModalVisible,
    });
  };

  onPressLock = () => {
    this._setPermission(this.props.item);
    this.onTogglePermissionModal();
  };

  onUpdateAutoSave = () => {
    const data = {...this.props.generalPreferences};
    data.autoSaveEnabled = !data.autoSaveEnabled;
    this.props.setSettings({type: 'GENERAL_PREFERENCES', value: data});
  };

  render() {
    const {canSee, canRead, isPermModalVisible} = this.state;
    const {item, generalPreferences, position} = this.props;

    return (
      <View style={fStyles.container}>
        <View
          style={[
            fStyles.wrapper,
            position === 'top' && {justifyContent: 'flex-end'},
          ]}>
          {position === 'top' && (
            <View style={{paddingBottom: 15}}>
              {item && (
                <>
                  <TouchableOpacity
                    style={styles.trashButton}
                    onPress={this.handleShare}>
                    <Text style={fStyles.saveOnEnterText}>SHARE</Text>
                    <Image
                      source={shareIcon}
                      style={[styles.trashIcon, fStyles.featureIcon]}
                    />
                  </TouchableOpacity>
                </>
              )}
              <TouchableOpacity
                style={[styles.trashButton, {marginTop: 15}]}
                onPress={this.handleAddPhoto}>
                <Text style={fStyles.saveOnEnterText}>ADD IMAGE</Text>
                <Image
                  source={addPhotoIcon}
                  style={[styles.trashIcon, fStyles.featureIcon]}
                />
              </TouchableOpacity>
            </View>
          )}
          {position === 'bottom' && (
            <View style={{paddingTop: 15}}>
              <View style={styles.trashButton}>
                <CheckBoxElement
                  title="SAVE ON ENTER"
                  iconRight
                  textStyle={[fStyles.saveOnEnterText, {marginRight: 10}]}
                  size={28}
                  checked={generalPreferences.autoSaveEnabled}
                  checkedColor="white"
                  uncheckedColor="white"
                  containerStyle={{
                    backgroundColor: 'transparent',
                    borderWidth: 0,
                    margin: 0,
                    padding: 0,
                    marginRight: 0,
                  }}
                  onPress={this.onUpdateAutoSave}
                />
              </View>
              <TouchableOpacity
                style={[styles.trashButton, {marginTop: 15}]}
                onPress={this.onPressLock}>
                <Text style={fStyles.saveOnEnterText}>PERMISSIONS</Text>
                <Image
                  source={lockIcon}
                  style={[styles.trashIcon, fStyles.featureIcon]}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
        {position === 'bottom' && item && (
          <View style={{position: 'absolute', bottom: 10, left: 10}}>
            <TouchableOpacity
              style={styles.trashButton}
              onPress={this.onPressTrash}>
              <Image
                source={archiveIcon}
                style={[styles.trashIcon, fStyles.featureIcon]}
              />
              <Text style={[fStyles.saveOnEnterText, {marginLeft: 5}]}>
                ARCHIVE
              </Text>
            </TouchableOpacity>
          </View>
        )}

        <ModalWrapper
          containerStyle={styles.modalContainer}
          style={[styles.modal, {backgroundColor: 'white', marginTop: 60}]}
          onRequestClose={this.onTogglePermissionModal}
          shouldAnimateOnRequestClose={true}
          shouldCloseOnOverlayPress={true}
          visible={isPermModalVisible}
          position={'top'}>
          <View style={{margin: 30}}>
            <Text style={styles.lockText}>
              Who can see icons and "tones" for this note in their stream?
            </Text>
            <View style={fStyles.selectWrapper}>
              <RNPickerSelect
                placeholder={{}}
                items={SettingConstants.SELECT_VISIBLE_USERS}
                onValueChange={(v) => {
                  this.setState({canSee: v});
                }}
                style={selectStyle}
                value={canSee}
                useNativeAndroidPickerStyle={false}
                textInputProps={selectTextInputStyle}
                Icon={() => {
                  return <Chevron size={1.5} color="gray" />;
                }}
              />
            </View>
            <Text style={styles.lockText}>
              Who can read the text of this note?
            </Text>
            <View style={fStyles.selectWrapper}>
              <RNPickerSelect
                placeholder={{}}
                items={SettingConstants.SELECT_VISIBLE_USERS}
                onValueChange={(v) => {
                  this.setState({canRead: v});
                }}
                style={selectStyle}
                value={canRead}
                useNativeAndroidPickerStyle={false}
                textInputProps={selectTextInputStyle}
                Icon={() => {
                  return <Chevron size={1.5} color="gray" />;
                }}
              />
            </View>
            <CommonButtons
              onSave={this.handleLock}
              onCancel={this.onTogglePermissionModal}
            />
          </View>
        </ModalWrapper>
      </View>
    );
  }
}

class CommonButtons extends React.Component {
  render() {
    return (
      <View style={[styles.noteActions, {marginLeft: 0, marginTop: 10}]}>
        <TouchableOpacity
          onPress={this.props.onSave}
          style={styles.archiveSaveBtn}>
          <Text style={styles.archiveSaveText}>Save</Text>
        </TouchableOpacity>
        <View style={{width: 10}} />
        <TouchableOpacity onPress={this.props.onCancel}>
          <Text style={[styles.cancelBtnText, {color: '#4F6A92'}]}>Cancel</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
