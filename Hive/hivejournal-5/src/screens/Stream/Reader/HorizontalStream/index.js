/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  ImageBackground,
  Image,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import FlipComponent from 'react-native-flip-component';
// import ImageProgress from 'react-native-image-progress';
// import * as Progress from 'react-native-progress';
import Swiper from 'react-native-swiper';
import Utils from '../../../../utils';
import styles from '../../_styles';
// import Tags from '../../../../constants/Tags';
// import {ProfileSelectors} from '../../../../constants/ProfileSelectors';
import Front from './Front';
import LoadingOverlay from '../../../../widgets/LoadingOverlay';
import GifLoader from '../../../../widgets/GifLoader';

// const corkBoardImg = require('../../../../../assets/images/corkboard.jpg');
// const landscapeBackImg = require('../../../../../assets/images/journey-journal-cover-blank-landscape-1.png');
// const portraitBackImg = require('../../../../../assets/images/journey-journal-cover-blank-portrait-1.png');

export default class HorizontalStream extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isClickedEllipses: false,
      isFlipped: false,
      index: props.slides.findIndex((item) => {
        return item._id === props.selected._id;
      }),
      dimensions: [],
      processing: false,
    };
  }

  componentDidMount() {
    this.mount = true;
    this.props.setIndex(this.state.index);

    if (this.props.isOwn === true) {
      Utils.trackScreenView('User views their own stream, horizontal');
    } else {
      Utils.trackScreenView('User views stream of another user, horizontal');
    }

    this.props.slides.forEach((item, _index) => {
      if (item.imageURL) {
        Image.getSize(item.imageURL, (width, height) => {
          this._setDimension(_index, width, height);
        });
      } else {
        this._setDimension(_index, 0, 0);
      }
    });
  }

  componentWillUnmount() {
    this.mount = false;
  }

  setItem = (index) => {
    this.refs.sliderX.scrollBy(index, false);
  };

  _setDimension = (index, width, height) => {
    let dimensions = [...this.state.dimensions];
    dimensions[index] = {
      width: width,
      height: height,
    };
    this.setState({dimensions});
  };

  onFlipView = () => {
    if (!this.mount) {
      return;
    }
    this.setState({
      isFlipped: !this.state.isFlipped,
    });
  };

  _renderItem = (item, index) => {
    const {isFlipped, dimensions} = this.state;
    const {slides, profiles} = this.props;
    const {width, height} = Dimensions.get('window');

    let isFirst =
      slides.findIndex((_item) => {
        return _item._id === item._id;
      }) === 0;

    let isLast =
      slides.findIndex((_item) => {
        return _item._id === item._id;
      }) ===
      slides.length - 1;

    let image = '';
    let imgWidth = 0;
    let imgHeight = 0;
    if (
      dimensions[index] &&
      dimensions[index].width > 0 &&
      dimensions[index].height > 0
    ) {
      image = typeof item.imageURL !== 'undefined' ? item.imageURL : '';
      // let rate = dimensions[index].width / dimensions[index].height;
      if (dimensions[index].width > dimensions[index].height) {
        imgWidth = width / 1.5;
        imgHeight = imgWidth / (877 / 539);
      } else {
        imgWidth = width / 2;
        imgHeight = imgWidth / (573 / 877);
      }
    }

    // let typeImg = null;
    // let typeName = null;
    // let typeColor = null;
    // const mbti = profiles.type[0].mbti;
    // const colors = ['#5f394d', '#56ac8a', '#51a9ab', '#dfc10c'];

    // if (mbti.length > 0) {
    //   const profileTypes =
    //     ProfileSelectors.type[0].mbti.source[0].categories[0].profileTypes;
    //   let found = profileTypes.findIndex(type => {
    //     return type.mainTypeShortName === mbti[mbti.length - 1].mainType;
    //   });
    //   typeColor = colors[parseInt(found / 4, 10)];
    //   typeImg = profileTypes[found].mainTypeImage;
    //   typeName = `${profileTypes[found].mainTypeLabel} (${
    //     mbti[mbti.length - 1].subType
    //   })`;
    // }

    return (
      <FlipComponent
        key={`horizontal-stream-${item._id}`}
        scale={1}
        isFlipped={isFlipped}
        frontView={
          <Front
            index={this.state.index}
            item={item}
            onFlipView={this.onFlipView}
            setLoading={(v) => this.setState({processing: v})}
            {...this.props}
          />
        }
        backView={
          <View
            style={{
              width: width,
              height: height - 50,
            }}>
            {/*<>
              <ImageBackground
                style={{
                  width: '100%',
                  height: '100%',
                  paddingLeft: 20,
                  paddingTop: 5,
                }}
                imageStyle={{
                  width: width,
                  height: height,
                  resizeMode: 'cover',
                }}
                source={corkBoardImg}>
                {isFirst && (
                  <View style={[styles.firstOneView, {left: 0}]}>
                    <Text style={styles.firstOneText}>.:. First one .:.</Text>
                  </View>
                )}
                {isLast && (
                  <View style={[styles.firstOneView, {right: 0}]}>
                    <Text style={styles.firstOneText}>.:. Last one .:.</Text>
                  </View>
                )}
                <Text
                  style={[
                    styles.otherNoteScreenNameText,
                    {
                      textAlign: 'center',
                      fontSize: 25,
                      marginTop: 40,
                      marginLeft: -20,
                      paddingHorizontal: 35,
                    },
                  ]}>
                  {item.note.screen_name ? item.note.screen_name : 'Anonymous'}
                </Text>
                {image !== '' && (
                  <View
                    style={[
                      styles.frameImg,
                      {
                        transform: [
                          {rotate: imgWidth < imgHeight ? '5deg' : '-5deg'},
                        ],
                        alignItems: 'center',
                        justifyContent: 'center',
                      },
                    ]}>
                    <ImageProgress
                      source={{uri: image}}
                      indicator={Progress.Pie}
                      indicatorProps={{
                        size: 30,
                        borderWidth: 2,
                        color: 'white',
                      }}
                      style={{width: imgWidth - 50, height: imgHeight - 50}}
                    />
                    <Image
                      source={
                        dimensions[index].width > dimensions[index].height
                          ? landscapeBackImg
                          : portraitBackImg
                      }
                      style={{
                        position: 'absolute',
                        width: imgWidth,
                        height: imgHeight,
                      }}
                    />
                  </View>
                )}
                {typeImg && (
                  <View style={{position: 'absolute', bottom: 10, right: 0}}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 15,
                        textAlign: 'center',
                        width: 150,
                        color: typeColor,
                      }}>
                      {typeName}
                    </Text>
                    <Image source={typeImg} style={{width: 150, height: 150}} />
                  </View>
                )}

                <TouchableOpacity
                  style={[styles.readerSwitchBtn, {marginLeft: -10}]}
                  onPress={this.onFlipView}>
                  <Text style={{color: 'white', fontSize: 35}}>...</Text>
                </TouchableOpacity>
              </ImageBackground>
            </>*/}
          </View>
        }
      />
    );
  };

  onIndexChanged = (index) => {
    if (!this.mount) {
      return;
    }
    this.setState(
      {
        index: index,
      },
      () => {
        this.props.setIndex(index);
        if (index + 1 === this.props.slides.length) {
          this.props.onEndReached();
        }
      },
    );
  };

  render() {
    const {index, processing} = this.state;
    const {slides, loading} = this.props;

    let items = [];
    slides.forEach((item, _index) => {
      items.push(this._renderItem(item, _index));
    });
    console.log('xxx = ', Utils.nativeBaseDoesHeaderCorrectlyAlready());
    return (
      <View
        style={[
          {
            // top: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? -45 : 0,
            top: 0,
          },
          styles.otherNoteModal,
          {
            backgroundColor: 'rgba(0, 0, 0, 1)',
            justifyContent: 'flex-start',
            overflow: 'hidden',
          },
        ]}>
        {loading && (
          <View style={[styles.notesLoader, {height: '100%'}]}>
            <GifLoader />
          </View>
        )}
        <LoadingOverlay loading={processing} />
        <Swiper
          // ref={component => (this.swiper = component)}
          ref="sliderX"
          containerStyle={{flex: 1}}
          index={index}
          loop={false}
          showsPagination={false}
          onIndexChanged={this.onIndexChanged}>
          {items}
        </Swiper>
      </View>
    );
  }
}
