/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import styles from '../_styles';

const closeIcon = require('../../../../assets/icons/glyph_close_32x32.png');

export default class StreamSwitcher extends React.Component {
  render() {
    const {onCloseModal, large} = this.props;

    return (
      <>
        <TouchableOpacity
          onPress={onCloseModal}
          style={styles.closeHorizontalButton}>
          <Image
            source={closeIcon}
            resizeMethod={'resize'}
            style={[
              styles.closeIcon,
              large === true && {width: 45, height: 45},
            ]}
          />
        </TouchableOpacity>

        {/*alignType === 2 && (
          <TouchableOpacity
            style={styles.readerSwitchBtn}
            onPress={() => onShowStreams(1)}>
            <Text style={{color: 'white', fontSize: 35}}>...</Text>
          </TouchableOpacity>
        )*/}
      </>
    );
  }
}
