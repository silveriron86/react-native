/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  Image,
  TextInput,
} from 'react-native';
import moment from 'moment-timezone';
// import Spotify from 'rn-spotify-sdk';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import ModalWrapper from 'react-native-modal-wrapper';
import SettingConstants from '../../constants/SettingConstants';
import ImageConstants from '../../constants/ImageConstants';
import DummyData from '../../constants/DummyData';
import ApiConstants from '../../constants/ApiConstants';
import LoadingOverlay from '../../widgets/LoadingOverlay';
import {MongoActions, SettingActions, CommonActions} from '../../actions';
import Utils from '../../utils';
import styles from './_styles';

class ThreeQJourney extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.initialState;
  }

  get initialState() {
    return {
      isNewToday: true,
      questionIndex: 0,
      qIndexes: [],
      introIndexes: [],
      continueBtnIndexes: [],
      snoozeBtnIndexes: [],
      spotifyInitialized: false,
      entries: [],
      answers: [],
      loading: false,
    };
  }

  componentDidMount() {
    this.mount = true;
    this._init();
  }
  _init = () => {
    if (!this.mount) {
      return;
    }
    const {spotifyEnabled} = this.props;

    this._setNewToday();

    setTimeout(() => {
      this._scheduleTask();
    }, 1000);

    global.scheduleTimer = setInterval(
      this._scheduleTask,
      SettingConstants.JIMMY_SCHEDULER_INTERVAL,
    );
    global.eventEmitter.addListener('LOGOUT', (data) => {
      if (!this.mount) {
        return;
      }
      this.setState(this.initialState);
    });

    if (spotifyEnabled === true) {
      Utils.initSpotify(() => {
        this.setState({
          spotifyInitialized: true,
        });
      });
    }
  };

  _setNewToday = () => {
    if (!this.mount) return;

    const {lastSavedTime, location} = this.props;
    if (lastSavedTime) {
      let lastDay = Utils.toTimezone(lastSavedTime, location);
      let today = Utils.nowTimezone(this.props);

      this.setState({
        isNewToday: !(lastDay.substr(0, 10) === today.substr(0, 10)),
      });
    }
  };

  _generateQuestionIndexes = () => {
    // generate random question indexes
    const {guide} = this.props;
    return [
      Utils.shuffle(DummyData.threeThings[guide].questions[0].length),
      Utils.shuffle(DummyData.threeThings[guide].questions[1].length),
    ];
  };

  _generateBtnIndexes = (idx) => {
    const {guide} = this.props;
    return Utils.shuffle(
      idx === 1
        ? DummyData.threeThings.continueBtnTexts.length
        : DummyData.threeThings.snoozeBtnTexts.length,
    );
  };

  _generateIntroIndexes = () => {
    const {guide} = this.props;
    return [
      Utils.shuffle(DummyData.threeThings[guide].introTexts[0].length),
      Utils.shuffle(DummyData.threeThings[guide].introTexts[1].length),
      Utils.shuffle(DummyData.threeThings[guide].introTexts[2].length),
    ];
  };

  _getIntroText = (idx) => {
    const {guide} = this.props;
    let ii = this.state.introIndexes[idx][0];
    return DummyData.threeThings[guide].introTexts[idx][ii];
  };

  _scheduleTask = () => {
    if (!this.mount) return;

    const {isNewToday} = this.state;
    const {
      q3SavePeriod,
      // threeQJourneyEnabled,
      isOpenedModal,
      isOpenedOthers,
    } = this.props;

    let now = Utils.nowMoment(this.props);
    let hhMM = now.format('HH:mm');

    if (/*threeQJourneyEnabled === true*/ false && isOpenedOthers === false) {
      if (isOpenedModal === false) {
        // console.log(q3SavePeriod.start, hhMM, q3SavePeriod.end)
        if (hhMM >= q3SavePeriod.start && hhMM <= q3SavePeriod.end) {
          if (isNewToday) {
            this.setState(
              {
                qIndexes: this._generateQuestionIndexes(),
                continueBtnIndexes: this._generateBtnIndexes(1),
                snoozeBtnIndexes: this._generateBtnIndexes(2),
                introIndexes: this._generateIntroIndexes(),
              },
              () => {
                this.setState(
                  {
                    note: null,
                    isNewToday: false,
                    introText: this._getIntroText(0),
                    questionIndex: 0,
                    question: this._getQuestionText(),
                    entries: [],
                  },
                  () => {
                    this._openQuestionModal();
                  },
                );
              },
            );
          }
        } else {
          this.setState({
            isNewToday: true,
            questionIndex: 0,
          });
          this.onCloseModal();
        }
      }
    } else {
      this.onCloseModal();
    }
  };

  componentWillUnmount() {
    this.mount = false;
  }

  onCloseModal = () => {
    this.props.onOpenModal(false, null);
  };

  _getQuestionText = () => {
    const {guide} = this.props;
    const {questionIndex, qIndexes} = this.state;
    let qSet = DummyData.threeThings[guide].questions[questionIndex % 2];
    let qIndex = qIndexes[questionIndex % 2][parseInt(questionIndex / 2, 10)];
    let question = qSet[qIndex];
    // let question = qSet[Math.floor(Math.random() * qSet.length)];
    return question;
  };

  onContinue = () => {
    this.setState(
      {
        introText: '',
        // questionIndex: this.state.questionIndex + 1
      },
      () => {
        this.setState({
          question: this._getQuestionText(),
        });
      },
    );
  };

  onSnooze = () => {
    this.props.onOpenModal(false, () => {
      setTimeout(() => {
        if (this.state.isNewToday === false) {
          if (!this.mount) return;

          this.setState(
            {
              introText: this._getIntroText(
                parseInt(this.state.questionIndex / 2, 10),
              ),
            },
            () => {
              this._openQuestionModal();
            },
          );
        }
      }, parseInt(this.props.snoozeTime, 10) * 1000 * 60);
    });
  };

  onKeyPressAnswer = (e) => {
    const {questionIndex} = this.state;
    const {guide, toneEnabled, noteVisibility} = this.props;
    if (e.nativeEvent.key === 'Enter') {
      // here answer will be sent to API
      console.log(
        '*** Saved (' + questionIndex + ') answer',
        this.state.question,
        this.state.answer,
      );
      let entries = this.state.entries.slice(0);
      let answers = this.state.answers.slice(0);
      if (questionIndex % 2 === 0) {
        entries.push({
          q_good: this.state.question,
          a_good: Utils.encrypt(this.state.answer),
          q_reason: '',
          a_reason: '',
        });
      } else {
        entries[entries.length - 1].q_reason = this.state.question;
        entries[entries.length - 1].a_reason = Utils.encrypt(this.state.answer);
      }
      answers.push(this.state.answer);

      let intro = '';
      if (questionIndex % 2 === 1) {
        intro =
          questionIndex === 5
            ? DummyData.threeThings[guide].congratsText
            : this._getIntroText(parseInt((questionIndex + 1) / 2, 10));
      }

      setTimeout(() => {
        // because onChangeAnswer is called after onKeyPress, delay is needed.
        this.props.onOpenModal(questionIndex === 5 ? false : true, null);

        this.setState(
          {
            questionIndex: questionIndex + 1,
            introText: intro,
            entries: entries,
            answers: answers,
            answer: '',
          },
          () => {
            this.setState({
              question: this._getQuestionText(),
            });
          },
        );
        console.log('questionIndex: ', questionIndex);
        if (questionIndex === 5) {
          // final
          console.log('final');
          let origText = answers.join(' ');
          console.log(origText);

          let note = {
            type: '3Q',
            '3Q_data': {
              guide: Utils.capitalize(guide),
              entries: entries,
            },
            note_orig: Utils.encrypt(origText),
            note_tags: Utils.getTags(origText),
            creation_timestamp: moment().tz(this.props.location).format(),
            entry_ip: '0.0.0.0',
            entry_app_type: 'mobile',
            entry_app_version: '1.0',
            word_count: Utils.getWords(origText),
            font: this.props.streamFont,
            font_size: this.props.fontSize,
            permissions: {
              see: noteVisibility['3Q'][0],
              read: noteVisibility['3Q'][1],
            },
            screen_name: this.props.screenName,
            main_tag: '3questions',
          };
          console.log(note);
          let saved = Utils.nowUTC(this.props);
          AsyncStorage.setItem('LAST_3THINGS_SAVED', saved);
          this.props.setSettings({type: 'THREE_THINGS_SAVED', value: saved});

          if (toneEnabled === true) {
            let apiData = ApiConstants.getToneAnalyzerApi();
            this.setState(
              {
                loading: true,
              },
              () => {
                axios({
                  method: 'GET',
                  url: `${apiData.url}/v3/tone?version=${
                    apiData.version
                  }&text=${encodeURIComponent(origText)}`,
                  headers: {
                    Authorization:
                      'Basic ' + Utils.btoa('apiKey:' + apiData.key),
                  },
                })
                  .then((response) => {
                    if (response.status === 200) {
                      note.ibm_tone = {
                        document_tone: response.data.document_tone,
                      };
                      this._procSpotifySaveNote(note);
                    } else {
                      this.setState(
                        {
                          loading: false,
                        },
                        () => {
                          Utils.saveFailedQueue('Tone analyzer service', note);
                        },
                      );
                    }
                  })
                  .catch((error) => {
                    this.setState(
                      {
                        loading: false,
                      },
                      () => {
                        Utils.saveFailedQueue('Tone analyzer service', note);
                      },
                    );
                  });
              },
            );
          } else {
            //disabled tone setting
            this._procSpotifySaveNote(note);
          }

          this.setState({
            entires: [],
          });
        }
      }, 30);
    }
  };

  onChangeAnswer = (text) => {
    this.setState({
      answer: text,
    });
  };

  _openQuestionModal = () => {
    return;
    if (this.props.threeQJourneyEnabled === true) {
      AsyncStorage.getItem('CURRENT_TAB', (err, tab) => {
        if (this.state.isNewToday === false) {
          if (tab === 'SettingsStack') {
            setTimeout(this._openQuestionModal, 5000);
          } else {
            if (this.props.isOpenedOthers === true) {
              setTimeout(() => {
                if (this.props.isOpenedOthers === false) {
                  this.props.onOpenModal(true, null);
                }
              }, 3000);
            } else {
              this.props.onOpenModal(true, null);
            }
          }
        }
      });
    }
  };

  onAskLater = () => {
    this.props.onOpenModal(false, () => {
      setTimeout(() => {
        this._openQuestionModal();
      }, parseInt(this.props.snoozeTime, 10) * 1000 * 60);
    });
  };

  _saveNote = (note) => {
    AsyncStorage.getItem('USER_ID', (err, user_id) => {
      let arg = JSON.parse(JSON.stringify(note));
      arg.user_id = user_id;
      console.log(arg);
      this.setState(
        {
          loading: true,
        },
        () => {
          this.props.createNote({
            data: arg,
            cb: (res) => {
              console.log(res);
              this.props.setLastNote({
                value: arg,
                id: res.insertedId,
              });

              AsyncStorage.getItem(
                'IBM_PERSONALITY',
                (err, ibm_personality) => {
                  console.log(ibm_personality);
                  let user = {
                    auth_provider: 'auth0',
                    user_id_auth: user_id,
                  };
                  if (ibm_personality) {
                    user.ibm_personality = JSON.parse(ibm_personality);
                  }
                  let data = {
                    user: user,
                  };
                  console.log('*** Saved ***');
                  console.log(data);
                  this.setState(
                    {
                      loading: false,
                      body: '',
                    },
                    () => {
                      this.props.onOpenModal(true, () => {
                        this.props.onRefreshNotes();

                        if (res.error) {
                          Utils.saveFailedQueue('Note creation', note);
                        }
                      });
                    },
                  );
                },
              );
            },
          });
        },
      );
    });
  };

  _getRecentlyPlayed = (session, note) => {
    /*
    this.setState({
      loading: true
    }, () => {
      axios({
        method: 'GET',
        url: `${ApiConstants.getSpotifyApi().url}/me/player/recently-played`,
        headers: {
          Authorization: "Bearer " + session.accessToken
        }
      }).then(response => {
        // last 5 songs played
        let played = response.data.items;
        if(played.length > 5) {
          played = played.slice(0, 5);
        }
        note['spotify_history'] = played;
        this._saveNote(note)
      })
      .catch(error => {
        this.setState({
          loading: false
        },() => {
          alert(error.message);
        })
      })
    })
    */
  };

  onCheckSpotify = (note) => {
    /*
    let session = Spotify.getSession();
    if(session === null) {
      // log into Spotify
      Spotify.login().then((loggedIn) => {
        if(loggedIn) {
          // logged in
          session = Spotify.getSession();
          this._getRecentlyPlayed(session, note);
        }else {
          // cancelled
          this._saveNote(note)
        }
      }).catch((error) => {
        this.setState({
          loading: false
        },() => {
          alert(error.message);
        })
      });
    }else {
      this._getRecentlyPlayed(session, note);
    }
    */
  };

  _procSpotifySaveNote = (note) => {
    /*
    if(this.props.spotifyEnabled === true) {
      this.onCheckSpotify(note);
    }else {
    */
    this._saveNote(note);
    // }
  };

  render() {
    const {
      questionIndex,
      introText,
      question,
      answer,
      continueBtnIndexes,
      snoozeBtnIndexes,
      loading,
    } = this.state;
    const {
      isOpenedModal,
      isOpenedOthers,
      streamFont,
      fontSize,
      guide,
    } = this.props;

    let w = Dimensions.get('window').width - 20;
    let h = (w * 305) / 404;
    let continueBtnText = '';
    let snoozeBtnText = '';

    if (isOpenedModal === true) {
      let sIdx = parseInt(questionIndex / 2, 10);
      let btnIdx = continueBtnIndexes[sIdx];
      continueBtnText = DummyData.threeThings.continueBtnTexts[btnIdx];
      btnIdx = snoozeBtnIndexes[sIdx];
      snoozeBtnText = DummyData.threeThings.snoozeBtnTexts[btnIdx];
    }

    return (
      <>
        <LoadingOverlay loading={loading} />
        <ModalWrapper
          containerStyle={[styles.modalContainer]}
          style={styles.modal}
          onRequestClose={this.onCloseModal}
          shouldAnimateOnRequestClose={true}
          // shouldCloseOnOverlayPress={(introText && questionIndex === 6) ? true : false}
          shouldCloseOnOverlayPress={true}
          visible={isOpenedModal === true && isOpenedOthers === false}
          position={'top'}>
          <View style={styles.modalInner}>
            <View style={styles.jimmyWrapper}>
              <Image
                source={
                  guide === 'jimmy'
                    ? ImageConstants.icons.jimmy
                    : ImageConstants.icons.robot
                }
                style={styles.jimmyIcon}
              />
            </View>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={styles.questionText}>
                {introText ? introText : question}
              </Text>
            </View>
            {questionIndex < 6 && (
              <View style={styles.answerWrapper}>
                <ImageBackground
                  imageStyle={{width: w, height: h}}
                  style={styles.noteBg}
                  source={ImageConstants.note}>
                  {introText ? (
                    <>
                      <View
                        style={{
                          marginTop: 35,
                          width: w - 30,
                          height: h - 85,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <TouchableOpacity onPress={this.onContinue}>
                          <Text style={[styles.laterBtnText, {fontSize: 24}]}>
                            {continueBtnText}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <TouchableOpacity
                        style={styles.laterBtn}
                        onPress={this.onSnooze}>
                        <Text style={[styles.laterBtnText]}>
                          {snoozeBtnText}
                        </Text>
                      </TouchableOpacity>
                    </>
                  ) : (
                    <>
                      <TextInput
                        placeholder={'Enter answer here...'}
                        placeholderTextColor={'#3079c195'}
                        value={answer}
                        onChangeText={this.onChangeAnswer}
                        onKeyPress={this.onKeyPressAnswer}
                        multiline={true}
                        style={[
                          {marginTop: 35, width: w - 30, height: h - 85},
                          styles.textWrapper,
                          styles.noteText,
                          {fontFamily: streamFont, fontSize: fontSize},
                        ]}
                      />
                      <TouchableOpacity
                        style={styles.laterBtn}
                        onPress={this.onAskLater}>
                        <Text
                          style={[
                            styles.laterBtnText,
                            {fontFamily: streamFont},
                          ]}>
                          Ask me later
                        </Text>
                      </TouchableOpacity>
                    </>
                  )}
                </ImageBackground>
              </View>
            )}
          </View>
        </ModalWrapper>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    screenName: state.SettingReducer.screenName,
    streamFont: state.SettingReducer.streamFont,
    toneEnabled: state.SettingReducer.toneEnabled,
    spotifyEnabled: state.SettingReducer.spotifyEnabled,
    fontSize: state.SettingReducer.fontSize,
    snoozeTime: state.SettingReducer.snoozeTime,
    q3SavePeriod: state.SettingReducer.q3SavePeriod,
    lastSavedTime: state.SettingReducer.lastSavedTime,
    overrideSystemTime: state.SettingReducer.overrideSystemTime,
    tempSystemDateTime: state.SettingReducer.tempSystemDateTime,
    threeQJourneyEnabled: state.SettingReducer.threeQJourneyEnabled,
    guide: state.SettingReducer.guide,
    noteVisibility: state.SettingReducer.noteVisibility,
    location: state.SettingReducer.location,
    error: state.MongoReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLastNote: (req) =>
      dispatch(CommonActions.setLastNote(req.value, req.id)),
    createNote: (req) => dispatch(MongoActions.createNote(req.data, req.cb)),
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ThreeQJourney);
