/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, FlatList, Image, TouchableOpacity} from 'react-native';
import ImageConstants from '../../constants/ImageConstants';
import SettingConstants from '../../constants/SettingConstants';
import Utils from '../../utils';
import styles from './_styles';
import GifLoader from '../../widgets/GifLoader';

const lockIcon = require('../../../assets/icons/lock.png');

export default class OtherNotes extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.initialState;
  }

  get initialState() {
    let newSizes = this._generateSizes();
    return {
      sizes: newSizes[0],
      hSpaces: newSizes[1],
      pageNum: 0,
      loadingOtherNotes: false,
    };
  }

  componentDidMount() {
    this.mount = true;
    this._loadNotes();

    global.eventEmitter.addListener('LOGOUT', (data) => {
      if (!this.mount) {
        return;
      }
      this.setState(this.initialState);
    });
  }

  componentWillUnmount() {
    this.mount = false;
  }

  _generateSizes = () => {
    const {depthAndSize} = this.props;
    let sizes = [];
    let hSpaces = [];

    for (let i = 0; i < 10; i++) {
      let size = Utils.getRandomInt(
        depthAndSize.vSize[0],
        depthAndSize.vSize[1],
      );
      let hSpace = Utils.getRandomInt(
        depthAndSize.hSpace[0],
        depthAndSize.hSpace[1],
      );
      sizes.push(size);
      hSpaces.push(hSpace);
    }
    return [sizes, hSpaces];
  };

  _loadNotes = () => {
    if (!this.mount) {
      return;
    }

    let {sizes, hSpaces, pageNum} = this.state;
    let newSizes = this._generateSizes();
    sizes = sizes.concat(newSizes[0]);
    hSpaces = hSpaces.concat(newSizes[1]);

    this.setState(
      {
        sizes,
        hSpaces,
        pageNum: pageNum + 1,
        loadingOtherNotes: true,
      },
      () => {
        this.props.getExceptNotes({
          page: pageNum + 1,
          cb: (res) => {
            if (this.mount) {
              this.setState({
                loadingOtherNotes: false,
              });
            }
          },
        });
      },
    );
  };

  onPullNotes = () => {
    this._loadNotes();
  };

  onRefreshNotes = () => {
    if (!this.mount) {
      return;
    }
    this.setState(
      {
        pageNum: 0,
      },
      () => {
        this._loadNotes();
      },
    );
  };

  selectNote = (item, isToggle) => {
    if (!this.mount) {
      return;
    }
    const {note, setNote, initNote} = this.props;
    if (isToggle === true) {
      if (note !== null && note._id === item._id) {
        setNote(null);
        return;
      }
    }

    initNote();

    setTimeout(() => {
      setNote(item);
    }, 1);
  };

  _renderItem = ({item, index}) => {
    const {sizes, hSpaces} = this.state;
    const {note, toneEnabled, tonePack} = this.props;
    let rows = [];
    let size = sizes[index];

    if (toneEnabled && item.note.ibm_tone && item.note.ibm_tone.document_tone) {
      let tones = item.note.ibm_tone.document_tone.tones;
      if (tones.length > 1) {
        if (size < 90) {
          size = 90;
        }
      }
    }

    let horizontalSpace = hSpaces[index];

    if (item.archived === true) {
      return null;
    }

    let hexIconStyle = [styles.hexIcon, {width: size, height: size}];
    if (item.note.type === '3Q') {
      let img = (
        <Image
          key={`gray-hex-${index}`}
          source={ImageConstants.hex.gray}
          style={hexIconStyle}
          resizeMethod="resize"
        />
      );

      if (
        toneEnabled &&
        item.note.ibm_tone &&
        item.note.ibm_tone.document_tone
      ) {
        let tones = item.note.ibm_tone.document_tone.tones;
        if (tones.length > 0) {
          if (tones.filter((e) => e.tone_id === 'joy').length > 0) {
            if (
              tones.filter(
                (e) =>
                  e.tone_id === 'anger' ||
                  e.tone_id === 'fear' ||
                  e.tone_id === 'sadness',
              ).length > 0
            ) {
              // if tone does not contain any of: (anger, fear, sadness))
              img = (
                <Image
                  key={`yellow-gem-${index}`}
                  source={ImageConstants.hex.gem}
                  style={hexIconStyle}
                  resizeMethod="resize"
                />
              );
            } else {
              img = (
                <Image
                  key={`yellow-hex-${index}`}
                  source={ImageConstants.hex.yellow}
                  style={hexIconStyle}
                  resizeMethod="resize"
                />
              );
            }
          } else {
            img = (
              <Image
                key={`blue-hex-${index}`}
                source={ImageConstants.hex.blue}
                style={hexIconStyle}
                resizeMethod="resize"
              />
            );
          }
        }
      }

      rows.push(img);
    } else {
      // normal note
      if (
        toneEnabled &&
        item.note.ibm_tone &&
        item.note.ibm_tone.document_tone
      ) {
        let tones = item.note.ibm_tone.document_tone.tones;
        if (tones.length > 0) {
          if (tones.filter((e) => e.tone_id === 'joy').length > 0) {
            // if contains joy
            rows.push(
              <Image
                key={`yellow-circle-${index}`}
                source={ImageConstants.circle.yellow}
                style={hexIconStyle}
                resizeMethod="resize"
              />,
            );

            if (tones.length > 1) {
              let stack = [];
              let updated = tones.slice(0);
              let joy = [];
              updated.forEach((tone, i) => {
                if (tone.tone_id === 'joy') {
                  joy.push(JSON.parse(JSON.stringify(tone)));
                  updated.splice(i, 1);
                }
              });
              updated = joy.concat(updated).slice(0);
              for (let ii = updated.length - 1; ii >= 0; ii--) {
                let initR =
                  (size - 20 * (updated.length - 1) - (size * 50) / 140) / 2;
                let tone = updated[ii];
                stack.push(
                  <Image
                    key={`tone-${index}-${ii}`}
                    source={SettingConstants.TONE_PACKS[tonePack][tone.tone_id]}
                    resizeMethod={'resize'}
                    style={[
                      styles.toneImg,
                      {width: (size * 50) / 140, height: (size * 100) / 140},
                      {
                        right: initR,
                        marginRight: (ii * 20 * size) / 140,
                        marginTop: (-ii * 10 * size) / 140,
                      },
                    ]}
                  />,
                );
              }
              rows.push(stack);
            }
          } else {
            // display bottle
            tones.sort(function (a, b) {
              return b.score - a.score;
            });

            let tone_id = tones[0].tone_id;
            rows.push(
              <Image
                key={`bottle-${tones[0].tone_id}-${index}`}
                source={ImageConstants.bottles[tone_id]}
                style={hexIconStyle}
                resizeMethod="resize"
              />,
            );
          }
        } else {
          // no tones
          rows.push(
            <Image
              key={`no-tones-${index}`}
              source={ImageConstants.circle.gray}
              style={hexIconStyle}
              resizeMethod="resize"
            />,
          );
        }
      } else {
        rows.push(
          <Image
            key={`no-tones-${index}`}
            source={ImageConstants.circle.gray}
            style={hexIconStyle}
            resizeMethod="resize"
          />,
        );
      }
    }

    return (
      <TouchableOpacity
        onPress={() => this.selectNote(item, true)}
        style={[
          styles.noteCol,
          {width: size, marginRight: horizontalSpace, padding: 5},
          note &&
            note.creation_timestamp === item.note.creation_timestamp &&
            styles.selectedNoteCol,
        ]}>
        {rows}
        {note && note._id === item._id && note.note.permissions.read !== 'any' && (
          <View style={styles.noteLockWrapper}>
            <View style={styles.noteLock}>
              <Image
                source={lockIcon}
                resizeMethod={'resize'}
                style={styles.noteLockIcon}
              />
            </View>
          </View>
        )}
      </TouchableOpacity>
    );
  };

  render() {
    const {mongoOtherNotes, note} = this.props;
    const {loadingOtherNotes} = this.state;

    return (
      <View style={{height: 120, position: 'relative'}}>
        {loadingOtherNotes && (
          <View style={styles.notesLoader}>
            <GifLoader />
          </View>
        )}
        <FlatList
          horizontal={true}
          data={mongoOtherNotes}
          extraData={this.props}
          keyExtractor={(item, index) => item._id}
          renderItem={this._renderItem}
          onEndReachedThreshold={0.05}
          onEndReached={this.onPullNotes}
        />
      </View>
    );
  }
}
