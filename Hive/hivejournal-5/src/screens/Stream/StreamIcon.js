import React from 'react';
import {Image} from 'react-native';
import SettingConstants from '../../constants/SettingConstants';
import ImageConstants from '../../constants/ImageConstants';
import styles from './_styles';

export default class StreamIcon extends React.Component {
  render() {
    const {toneEnabled, tonePack, item, index} = this.props;
    let rows = [];

    if (item.note.failed === true) {
      rows.push(
        <Image
          key={`failed-queue-${index}`}
          source={ImageConstants.oval}
          style={styles.hexIcon}
          resizeMethod="resize"
        />,
      );
    }

    if (item.note.type === '3Q') {
      let img = (
        <Image
          key={`gray-hex-${index}`}
          source={ImageConstants.hex.gray}
          style={styles.hexIcon}
          resizeMethod="resize"
        />
      );

      if (
        toneEnabled &&
        item.note.ibm_tone &&
        item.note.ibm_tone.document_tone
      ) {
        let tones = item.note.ibm_tone.document_tone.tones;
        if (tones.length > 0) {
          if (tones.filter(e => e.tone_id === 'joy').length > 0) {
            if (
              tones.filter(
                e =>
                  e.tone_id === 'anger' ||
                  e.tone_id === 'fear' ||
                  e.tone_id === 'sadness',
              ).length > 0
            ) {
              // if tone does not contain any of: (anger, fear, sadness))
              img = (
                <Image
                  key={`yellow-gem-${index}`}
                  source={ImageConstants.hex.gem}
                  style={styles.hexIcon}
                  resizeMethod="resize"
                />
              );
            } else {
              img = (
                <Image
                  key={`yellow-hex-${index}`}
                  source={ImageConstants.hex.yellow}
                  style={styles.hexIcon}
                  resizeMethod="resize"
                />
              );
            }
          } else {
            img = (
              <Image
                key={`blue-hex-${index}`}
                source={ImageConstants.hex.blue}
                style={styles.hexIcon}
                resizeMethod="resize"
              />
            );
          }
        }
      }

      rows.push(img);
    } else {
      // normal note
      if (
        toneEnabled &&
        item.note.ibm_tone &&
        item.note.ibm_tone.document_tone
      ) {
        let tones = item.note.ibm_tone.document_tone.tones;
        if (tones.length > 0) {
          if (tones.filter(e => e.tone_id === 'joy').length > 0) {
            // if contains joy
            rows.push(
              <Image
                key={`yellow-circle-${index}`}
                source={ImageConstants.circle.yellow}
                style={styles.hexIcon}
                resizeMethod="resize"
              />,
            );

            if (tones.length > 1) {
              let stack = [];
              let updated = tones.slice(0);
              let joy = [];
              updated.forEach((tone, i) => {
                if (tone.tone_id === 'joy') {
                  joy.push(JSON.parse(JSON.stringify(tone)));
                  updated.splice(i, 1);
                }
              });

              updated = joy.concat(updated).slice(0);
              for  (let ii = updated.length - 1; ii >= 0; ii--) {
                let initR = (140 - 20 * (updated.length -  1) - 50) / 2;
                let tone = updated[ii];
                stack.push(
                  <Image
                    key={`tone-${index}-${ii}`}
                    source={SettingConstants.TONE_PACKS[tonePack][tone.tone_id]}
                    resizeMethod={'resize'}
                    style={[
                      styles.toneImg,
                      {right: initR, marginRight: ii * 20, marginTop: -ii * 10},
                    ]}
                  />,
                );
              }
              rows.push(stack);
            }
          } else {
            // display bottle
            tones.sort(function(a, b) {
              return b.score - a.score;
            });

            let tone_id = tones[0].tone_id;
            rows.push(
              <Image
                key={`bottle-${tones[0].tone_id}-${index}`}
                source={ImageConstants.bottles[tone_id]}
                style={styles.hexIcon}
                resizeMethod="resize"
              />,
            );
          }
        } else {
          // no tones
          rows.push(
            <Image
              key={`no-tones-${index}`}
              source={ImageConstants.circle.gray}
              style={styles.hexIcon}
              resizeMethod="resize"
            />,
          );
        }
      } else {
        rows.push(
          <Image
            key={`no-tones-${index}`}
            source={ImageConstants.circle.gray}
            style={styles.hexIcon}
            resizeMethod="resize"
          />,
        );
      }
}

    return rows;
  }
}

