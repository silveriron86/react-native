/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  FlatList,
  View,
  ScrollView,
  ImageBackground,
  Text,
  Dimensions,
  Animated,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
} from 'react-native';
import MoonCloud from './MoonCloud';
import Utils from '../../utils';
import styles from './_styles';
import StreamIcon from './StreamIcon';
import HorizontalStream from './Reader/HorizontalStream';
import VerticalStream from './Reader/VerticalStream';
import StreamSwitcher from './Reader/StreamSwitcher';
import Colors from '../../constants/Colors';
import GifLoader from '../../widgets/GifLoader';

const note_bg = require('../../../assets/images/notebook_bg.png');
const closeIcon = require('../../../assets/icons/glyph_close_32x32.png');

export default class OtherNoteModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNum: 0,
      isVisibleNotes: false,
      loadingNotes: false,
      full: true,
      fullMoon: false,
      alignType: 2,
      isSelectedNote: true,
      selectedNoteIndex: 0,
    };
  }

  UNSAFE_componentWillMount() {
    this.w = Dimensions.get('window').width - 20;
    this.h = (this.w * 305) / 404;

    this.scaleAnimatedValue = new Animated.Value(1);
    this.streamIconScaled = new Animated.Value(0.5);
    this.singleScaled = new Animated.Value(0.7);
    this.moonCloudAnimatedValue = new Animated.Value(0.3);
  }

  componentDidMount() {
    this._loadNotes();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.note._id !== nextProps.note._id) {
      this.scaleAnimatedValue = new Animated.Value(1);
      this.moonCloudAnimatedValue = new Animated.Value(0.3);
      this.setState({
        fullMoon: false,
        full: true,
        isSelectedNote: true,
      });
    }
  }

  handlePressMoon = () => {
    Animated.spring(this.moonCloudAnimatedValue, {
      toValue: 0.3,
      duration: 1,
    }).start();

    Animated.spring(this.moonCloudAnimatedValue, {
      toValue: 1,
      friction: 4,
    }).start();

    setTimeout(() => {
      this.setState({
        fullMoon: true,
      });
    }, 100);
  };

  closeModal = () => {};

  _loadNotes = () => {
    const {pageNum} = this.state;
    this.setState(
      {
        pageNum: pageNum + 1,
        loadingNotes: true,
      },
      () => {
        this.props.getNotesOtherSingle({
          user_id: this.props.note.note.user_id,
          page: pageNum + 1,
          cb: (res) => {
            this.setState({
              loadingNotes: false,
            });
          },
        });
      },
    );
  };

  onPullNotes = () => {
    this._loadNotes();
  };

  selectNote = (item, isToggle) => {
    this.setState({
      isVisibleNotes: false,
      full: true,
    });
    this.props.setNote(item);
  };

  onSetIndex = (index) => {
    this.setState({
      selectedNoteIndex: index,
    });
  };

  _renderItem = ({item, index}) => {
    const {toneEnabled, tonePack, note} = this.props;
    const streamScaled = {
      transform: [{scale: this.singleScaled}],
    };

    let selectedNote = null;
    if (note) {
      selectedNote = note.note;
    }

    if (item.archived === true) {
      return null;
    }

    return (
      <TouchableOpacity
        onPress={() => this.selectNote(item, true)}
        style={[
          {width: 100, height: 90, marginRight: 10, paddingHorizontal: 5},
          selectedNote &&
            selectedNote.creation_timestamp === item.note.creation_timestamp &&
            styles.selectedNoteCol,
        ]}>
        <Animated.View style={streamScaled}>
          <StreamIcon
            item={item}
            index={index}
            toneEnabled={toneEnabled}
            tonePack={tonePack}
          />
        </Animated.View>
      </TouchableOpacity>
    );
  };

  onClose = () => {
    this.setState({
      full: false,
      isVisibleNotes: false,
    });
    this.props.onClose();
  };

  onPressArrow = () => {
    this.setState({
      isVisibleNotes: true,
    });
  };

  onSelectAlignType = (type) => {
    this.setState({
      alignType: type,
    });
  };

  onCloseModal = () => {
    this.setState({
      isVisibleNotes: false,
      full: false,
      fullMoon: false,
    });
    this.props.onClose();
  };

  onShowStreams = (type) => {
    this.setState({
      alignType: type,
      full: true,
    });
  };

  render() {
    const {
      note,
      fontSize,
      otherSingleUserNotes,
      streamFont,
      profiles,
    } = this.props;
    const {
      full,
      loadingNotes,
      isVisibleNotes,
      fullMoon,
      alignType,
      isSelectedNote,
      selectedNoteIndex,
    } = this.state;
    const animatedStyle = {
      transform: [
        {scale: this.scaleAnimatedValue},
        // {
        //   translateY: this.scaleAnimatedValue.interpolate({
        //     inputRange: [0.2, 1],
        //     outputRange: [this.h, 0]
        //   }),
        // }
      ],
    };

    const moonAnimatedStyle = {
      transform: [{scale: this.moonCloudAnimatedValue}],
    };

    let selectedNote = null;
    if (note) {
      selectedNote = note.note;
    }

    let selectedFont = 'PremiumUltra5';
    let selectedColor = Colors.scheme.blue;
    if (selectedNote) {
      if (selectedNote.font) {
        selectedFont = selectedNote.font;
      }
      if (selectedNote.color) {
        selectedColor = selectedNote.color;
      }
    }

    return (
      <View
        style={{
          height: Dimensions.get('window').height - 50,
          overflow: 'hidden',
        }}>
        {selectedNote.type === '3Q' ? (
          <View style={styles.q3note}>
            <ScrollView style={{height: '100%'}}>
              {selectedNote['3Q_data'].entries.map((entry, iii) => {
                return (
                  <View key={`3q-note-${iii}`}>
                    <View style={[styles.entryRow, {marginTop: 10}]}>
                      <View style={styles.entryCol}>
                        <Text
                          style={{
                            fontFamily: selectedFont,
                            fontSize: 16,
                            color: '#3079c1',
                            textAlign: 'right',
                          }}>
                          {'<:'}
                        </Text>
                      </View>
                      <View style={styles.entryCol} />
                    </View>
                    <View style={[styles.entryRow, {marginBottom: 10}]}>
                      <View style={styles.entryCol}>
                        <Text
                          style={{
                            fontFamily: selectedFont,
                            fontSize: 16,
                            color: '#3079c1',
                            textAlign: 'right',
                          }}>
                          {Utils.decrypt(entry.a_good)}
                        </Text>
                      </View>
                      <View style={styles.entryCol}>
                        <Text
                          style={{
                            fontFamily: selectedFont,
                            fontSize: 16,
                            color: '#979797',
                            textAlign: 'left',
                          }}>
                          {Utils.decrypt(entry.a_reason)}
                          {'\n'}
                          {':>'}
                        </Text>
                      </View>
                    </View>
                  </View>
                );
              })}
              {selectedNote['3Q_data'].entries.map((entry, iii) => {
                return (
                  <View key={`3q-note-${iii}`}>
                    <View style={[styles.entryRow, {marginTop: 10}]}>
                      <View style={styles.entryCol}>
                        <Text
                          style={{
                            fontFamily: selectedFont,
                            fontSize: 16,
                            color: '#3079c1',
                            textAlign: 'right',
                          }}>
                          {'<:'}
                        </Text>
                      </View>
                      <View style={styles.entryCol} />
                    </View>
                    <View style={[styles.entryRow, {marginBottom: 10}]}>
                      <View style={styles.entryCol}>
                        <Text
                          style={{
                            fontFamily: selectedFont,
                            fontSize: 16,
                            color: '#3079c1',
                            textAlign: 'right',
                          }}>
                          {Utils.decrypt(entry.a_good)}
                        </Text>
                      </View>
                      <View style={styles.entryCol}>
                        <Text
                          style={{
                            fontFamily: selectedFont,
                            fontSize: 16,
                            color: '#979797',
                            textAlign: 'left',
                          }}>
                          {Utils.decrypt(entry.a_reason)}
                          {'\n'}
                          {':>'}
                        </Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </ScrollView>

            <TouchableOpacity
              style={{position: 'absolute', right: 10, marginTop: 10}}
              onPress={this.onCloseModal}>
              <Image source={closeIcon} />
            </TouchableOpacity>
            <View style={[styles.divider, {height: '100%'}]} />
          </View>
        ) : selectedNote.type === 'sleep' ? (
          <TouchableWithoutFeedback onPress={this.handlePressMoon}>
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Animated.View style={[moonAnimatedStyle]}>
                <MoonCloud
                  note={note}
                  fontSize={fontSize}
                  isOther={true}
                  isFull={fullMoon}
                />
              </Animated.View>
            </View>
          </TouchableWithoutFeedback>
        ) : (
          <>
            {!isVisibleNotes && (
              <TouchableWithoutFeedback
                onPress={this.handlePress}
                disabled={full}>
                <View
                  style={[
                    {
                      top: Utils.nativeBaseDoesHeaderCorrectlyAlready()
                        ? -45
                        : 0,
                    },
                    styles.otherNoteModal,
                    full && {backgroundColor: 'transparent'},
                  ]}>
                  {full === true && (
                    <>
                      {alignType === 1 ? (
                        <VerticalStream
                          isOwn={false}
                          index={selectedNoteIndex}
                          slides={otherSingleUserNotes}
                          streamFont={streamFont}
                          fontSize={fontSize}
                          loading={loadingNotes}
                          onEndReached={this.onPullNotes}
                        />
                      ) : (
                        <HorizontalStream
                          isOwn={false}
                          selected={note}
                          setIndex={this.onSetIndex}
                          slides={otherSingleUserNotes}
                          streamFont={streamFont}
                          fontSize={fontSize}
                          loading={loadingNotes}
                          profiles={profiles}
                          onEndReached={this.onPullNotes}
                        />
                      )}
                    </>
                  )}

                  {isSelectedNote && (
                    <Animated.View
                      style={[
                        {
                          position: 'absolute',
                          paddingHorizontal: 10,
                          marginTop: -20,
                        },
                        animatedStyle,
                      ]}>
                      <ImageBackground
                        imageStyle={{width: this.w, height: this.h}}
                        style={[styles.noteBg, {height: this.h - 30}]}
                        source={note_bg}>
                        <View style={{width: this.w - 30, flex: 1}}>
                          <ScrollView
                            style={[{marginTop: 35}, styles.textWrapper]}>
                            <View onStartShouldSetResponder={() => full}>
                              <Text
                                style={{
                                  fontFamily: selectedFont,
                                  fontSize: fontSize,
                                  color: selectedColor,
                                }}>
                                {selectedNote.note_orig !== ''
                                  ? Utils.decrypt(selectedNote.note_orig)
                                  : ''}
                              </Text>
                            </View>
                          </ScrollView>
                        </View>
                      </ImageBackground>
                      {full === true && (
                        <View
                          style={[styles.otherNoteCloseIcon, {width: '100%'}]}>
                          <TouchableOpacity
                            onPress={() => {
                              this.setState({
                                isSelectedNote: false,
                              });
                            }}>
                            <Image source={closeIcon} />
                          </TouchableOpacity>
                        </View>
                      )}
                    </Animated.View>
                  )}

                  {!full && (
                    <>
                      <View
                        style={[
                          styles.otherNoteScreenName,
                          {
                            marginBottom: Utils.nativeBaseDoesHeaderCorrectlyAlready()
                              ? 30
                              : 0,
                          },
                        ]}>
                        <Text style={styles.otherNoteScreenNameText}>
                          {selectedNote.screen_name
                            ? selectedNote.screen_name
                            : 'Anonymous'}
                        </Text>
                      </View>
                    </>
                  )}

                  <StreamSwitcher
                    alignType={alignType}
                    onCloseModal={this.onCloseModal}
                    onShowStreams={this.onShowStreams}
                  />
                </View>
              </TouchableWithoutFeedback>
            )}
            {full && isVisibleNotes && (
              <>
                <View
                  style={[
                    {
                      top: Utils.nativeBaseDoesHeaderCorrectlyAlready()
                        ? -45
                        : 0,
                    },
                    styles.otherNoteModal,
                  ]}>
                  <View style={styles.otherSingleUserNotes}>
                    {loadingNotes && (
                      <View style={[styles.notesLoader, {height: '100%'}]}>
                        <GifLoader />
                      </View>
                    )}
                    {otherSingleUserNotes.length > 0 && (
                      <FlatList
                        horizontal={true}
                        data={otherSingleUserNotes}
                        keyExtractor={(item, index) => item._id}
                        renderItem={this._renderItem}
                        onEndReachedThreshold={0.05}
                        onEndReached={this.onPullNotes}
                      />
                    )}
                  </View>
                </View>

                <View
                  style={[
                    styles.otherNoteCloseIcon,
                    {
                      position: 'absolute',
                      top: Dimensions.get('window').height / 2 - 50,
                      alignItems: 'center',
                      width: '100%',
                      marginLeft: 0,
                    },
                  ]}>
                  <TouchableOpacity onPress={this.onClose}>
                    <Image source={closeIcon} />
                  </TouchableOpacity>
                </View>
              </>
            )}
          </>
        )}
      </View>
    );
  }
}
