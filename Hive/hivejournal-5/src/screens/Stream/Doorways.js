/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import Swiper from 'react-native-swiper';
import {Image, TouchableWithoutFeedback} from 'react-native';
import Colors from '../../constants/Colors';

const IMAGES = [
  require('../../../assets/images/home/1.png'),
  require('../../../assets/images/home/2.png'),
  require('../../../assets/images/home/3.png'),
  require('../../../assets/images/home/4.png'),
  require('../../../assets/images/home/5.png'),
  require('../../../assets/images/home/6.png'),
  require('../../../assets/images/home/7.png'),
];

export default class Doorways extends React.Component {
  render() {
    let items = [];
    IMAGES.forEach((img, index) => {
      items.push(
        <TouchableWithoutFeedback
          key={`doorway-${index}`}
          style={{flex: 1, overflow: 'hidden'}}
          onPress={this.props.onClose}>
          <Image
            source={img}
            style={{
              width: '100%',
              height: '100%',
              resizeMode: 'cover',
            }}
          />
        </TouchableWithoutFeedback>,
      );
    });

    return (
      <Swiper
        containerStyle={{flex: 1}}
        loop={true}
        activeDotColor={Colors.scheme.blue}
        showsPagination={true}
        onIndexChanged={this.onIndexChanged}>
        {items}
      </Swiper>
    );
  }
}