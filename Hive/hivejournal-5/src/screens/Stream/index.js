/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  TouchableWithoutFeedback,
  Image,
  FlatList,
  Dimensions,
} from 'react-native';
import Video from 'react-native-video';
import moment from 'moment-timezone';
import SunCalc from 'suncalc';
// import GestureRecognizer from 'react-native-swipe-gestures';
import SettingConstants from '../../constants/SettingConstants';
import ImageConstants from '../../constants/ImageConstants';
import LoadingOverlay from '../../widgets/LoadingOverlay';
import {MongoActions, SettingActions} from '../../actions';
import Utils from '../../utils';
import styles from './_styles';
// import ThreeQJourney from './ThreeQJourney';
// import RestUpJourney from './RestUpJourney';
import Locker from '../../widgets/Locker';
import OtherNotes from './OtherNotes';
import NoteModal from './NoteModal';
import OtherNoteModal from './OtherNoteModal';
import StreamIcon from './StreamIcon';
import Fade from '../../widgets/Fade';
import FooterTrigger from '../../widgets/FooterTrigger';
import FooterTags from '../Notes/FooterTags';
import ImageSelector from '../Notes/ImageSelector';
import JournalCover from '../Notes/JournalCover';
import Colors from '../../constants/Colors';
import GifLoader from '../../widgets/GifLoader';
import Doorways from './Doorways';
import * as RootNavigation from '../../navigation/RootNavigation';

const clouds_bg = require('../../../assets/images/clouds.png');

class StreamScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.initialState;
  }

  get initialState() {
    return {
      loading: false,
      note: null,
      otherNote: null,
      bgIndex: 0,
      isOpenedResetUp: false,
      isOpenedThreeQ: false,
      pageNum: 0,
      loadingNotes: false,
      isOpenedDoorway: true,
      isOpenedDoorwayDelay: true,
      footerMode: 2,
      selectedTagIndex: -1,
      isFirstVisit: false,
      editMode: false,
      background: -1,
      fontColor: Colors.scheme.blue,
      insertedTags: [],
      newTags: [],

      // For search
      searchTags: [],
      searchText: '',
    };
  }

  componentDidMount() {
    this.mount = true;
    this.sunriseIdx = Utils.getRandomInt(
      0,
      ImageConstants.periods.sunrise.length - 1,
    );
    this.dayIdx = Utils.getRandomInt(0, ImageConstants.periods.day.length - 1);
    this.sunsetIdx = Utils.getRandomInt(
      0,
      ImageConstants.periods.sunset.length - 1,
    );
    this.nightIdx = Utils.getRandomInt(
      0,
      ImageConstants.periods.night.length - 1,
    );

    this._loadNotes();

    global.eventEmitter.addListener('LOGOUT', (data) => {
      if (!this.mount) {
        return;
      }
      this.setState(this.initialState);
    });

    global.eventEmitter.addListener('NOTE_CREATED', (data) => {
      this.onRefreshNotes();
    });

    global.eventEmitter.addListener('NEW_TAB', (tab) => {
      if (tab === 'HomeStack') {
        this.setState({
          note: null,
          // isOpenedDoorway: true,
          // isOpenedDoorwayDelay: true,
        });
      }
    });

    global.eventEmitter.addListener('TO_FIRST_OWN_NOTE', (data) => {
      this.setState(
        {
          selectedTagIndex: data.index,
          isOpenedDoorwayDelay: false,
          isOpenedDoorway: false,
        },
        () => {
          this.refreshFilteredNotes();
        },
      );
    });

    global.eventEmitter.addListener('DESELECT_JOURNEY', () => {
      this.setState(
        {
          selectedTagIndex: -1,
          pageNum: 0,
        },
        () => {
          this.onToggleJournalSelector();
          this._loadNotes();
        },
      );
    });

    global.eventEmitter.addListener('EDIT_MODE', (data) => {
      const {note} = this.state;
      this.setState({
        editMode: data.mode,
        background: note ? note.note.background_image : -1,
      });
      if (data.mode === true) {
        this.updateNewTags();
      }
    });

    global.eventEmitter.addListener('CHANGED_COVER', (data) => {
      let state = {
        selectedTagIndex: data.index,
      };

      if (typeof data.searchText !== 'undefined') {
        state.searchTags = data.searchTags;
        state.searchText = data.searchText ? data.searchText.toLowerCase() : '';
      } else {
        if (this.state.selectedTagIndex !== data.index) {
          state.searchTags = [];
          state.searchText = '';
        }
      }

      this.setState(state, () => {
        if (data.type === 'todo') {
          // if (data.type === 'todo' && this.noteModalRef) {
          this.setState(
            {
              isOpenedDoorwayDelay: false,
              isOpenedDoorway: false,
            },
            () => {
              if (
                this.props.mongoNotes.length > 0 &&
                this.props.mongoNotes[0].note.main_tag === 'todo'
              ) {
                this.selectNote(this.props.mongoNotes[0]);
                this._showTodoList();
              } else {
                this.refreshFilteredNotes(() => {
                  this._showTodoList();
                });
              }
            },
          );
        }
        this.onRefreshNotes();
      });
    });
  }

  _showTodoList = () => {
    setTimeout(() => {
      if (typeof this.noteModalRef !== 'undefined') {
        this.noteModalRef.onShowStreams(1);
      }
    }, 10);
  };

  refreshFilteredNotes = (callback) => {
    this.setState(
      {
        pageNum: 0,
      },
      () => {
        this._loadNotes((res) => {
          const {mongoNotes} = this.props;
          // const filteredNotes = res.filter(n => {
          const filteredNotes = mongoNotes.filter((n) => {
            return n.archived !== true;
          });
          if (filteredNotes.length > 0) {
            this.selectNote(filteredNotes[0]);
            if (callback) {
              setTimeout(() => {
                callback();
              }, 100);
            }
          } else {
            // deselect and hide modal
            this.setState({
              note: null,
            });
          }
        });
      },
    );
  };

  onPressDoorway = () => {
    this.setState(
      {
        isOpenedDoorway: false,
      },
      () => {
        setTimeout(() => {
          this.setState({
            isOpenedDoorwayDelay: false,
          });
        }, 300);
      },
    );
  };

  _loadNotes = (callback) => {
    const {pageNum, selectedTagIndex} = this.state;
    this.setState(
      {
        pageNum: pageNum + 1,
        loadingNotes: true,
      },
      () => {
        let args = {
          page: pageNum + 1,
          cb: (res) => {
            this.setState(
              {
                loadingNotes: false,
              },
              () => {
                if (callback) {
                  callback(res);
                }
              },
            );
          },
          size: selectedTagIndex === 2 ? 500 : 10,
          exclude: 'todo',
        };

        if (selectedTagIndex !== -1) {
          args.tag = this.props.tags[selectedTagIndex].type;
          this.props.getFilteredNotes(args);
        } else {
          this.props.getNotes(args);
        }
      },
    );
  };

  onPullNotes = () => {
    console.log('On End Reached');
    this._loadNotes();
  };

  onRefreshNotes = () => {
    this.setState(
      {
        note: null,
        pageNum: 0,
      },
      () => {
        this._loadNotes();
      },
    );
  };

  componentWillUnmount() {
    this.mount = false;
  }

  _updateInsertedTags = (note) => {
    let origText = Utils.decrypt(note.note.note_orig);
    let tags = Utils.getTags(origText);
    if (tags) {
      tags.forEach((tag, i) => {
        tags[i] = tag.substr(1);
      });
    }
    this.setState({
      insertedTags: tags,
    });
  };

  selectNote = (item, isToggle) => {
    // const { noteVisibility } = this.props
    let oldStateNote = this.state.note;
    this.setState(
      {
        note: null,
      },
      () => {
        if (isToggle === true) {
          if (oldStateNote !== null && oldStateNote._id === item._id) {
            return;
          }
        }

        Utils.trackEvent('Action', 'User taps on one of their own notes');
        this.setState(
          {
            note: item,
            otherNote: null,
            fontColor: item.note.color,
          },
          () => {
            if (item.note.main_tag === 'todo') {
              if (typeof this.noteModalRef !== 'undefined') {
                this.noteModalRef.onShowStreams(1);
              }
            }
          },
        );
        this._updateInsertedTags(item);
      },
    );
  };

  _getCloudsOpacity = () => {
    const {
      location,
      cloudsMultiplier,
      cloudsStartTime,
      lastSavedTime,
    } = this.props;
    if (lastSavedTime) {
      let startTime = moment(lastSavedTime, 'YYYY-MM-DD HH:mm ZZ')
        .tz(location)
        .format('YYYY-MM-DD ' + SettingConstants.THREE_QUESTIONS_DUE_END_TIME); //10PM
      let duration = moment.duration(
        Utils.nowMoment(this.props).diff(moment(startTime)),
      );
      let hours = duration.asHours();

      if (hours > parseInt(cloudsStartTime, 10)) {
        let opacity =
          ((hours - parseInt(cloudsStartTime, 10)) / 100) * cloudsMultiplier;
        return opacity < 1 ? opacity : 1;
      }
    }

    return 0;
  };

  onOpenModal = (type, val, cb) => {
    let state = this.state;
    state[type] = val;
    this.setState(state, () => {
      if (cb) {
        cb();
      }
    });
  };

  _renderItem = ({item, index}) => {
    const {note} = this.state;
    const {toneEnabled, tonePack} = this.props;

    if (item.archived === true) {
      return null;
    }

    return (
      <TouchableOpacity
        onPress={() => this.selectNote(item, true)}
        style={[
          styles.noteCol,
          {marginRight: 10, padding: 5},
          note &&
            note.creation_timestamp === item.note.creation_timestamp &&
            styles.selectedNoteCol,
        ]}>
        <StreamIcon
          item={item}
          index={index}
          toneEnabled={toneEnabled}
          tonePack={tonePack}
        />
      </TouchableOpacity>
    );
  };

  setOtherNote = (v) => {
    if (v) {
      this.setState({
        note: null,
      });
    } else {
      Utils.trackEvent('Action', 'User taps on a note of another user');
    }

    this.setState({
      otherNote: v,
    });
  };

  setSingleOtherNote = (v) => {
    this.setState(
      {
        otherNote: null,
      },
      () => {
        setTimeout(() => {
          this.setState({
            otherNote: v,
          });
        }, 1);
      },
    );
  };

  initNoteStates = () => {
    this.setState({
      note: null,
      loading: false,
    });
  };

  _getBackground = () => {
    const {location, locations} = this.props;
    if (locations.length === 0) {
      return ImageConstants.backgrounds[0];
    }

    let curLoc = locations[0];
    locations.forEach((loc, i) => {
      if (loc.time_zone.id === location) {
        curLoc = loc;
      }
    });

    let sunTimes = SunCalc.getTimes(
      /*Date*/ moment().tz(location),
      /*Number*/ curLoc.latitude,
      /*Number*/ curLoc.longitude,
    );

    // 1) Sunrise: dawn until sunriseEnd
    if (
      moment().isAfter(moment(sunTimes.dawn)) &&
      moment().isBefore(moment(sunTimes.sunriseEnd))
    ) {
      // console.log('*** Sunrise')
      return ImageConstants.periods.sunrise[this.sunriseIdx];
    }

    // 2) Day: sunriseEnd until sunsetStart
    if (
      moment().isAfter(moment(sunTimes.sunriseEnd)) &&
      moment().isBefore(moment(sunTimes.sunsetStart))
    ) {
      // console.log('*** Day')
      return ImageConstants.periods.day[this.dayIdx];
    }

    // 3) Sunset: sunsetStart until night
    if (
      moment().isAfter(moment(sunTimes.sunsetStart)) &&
      moment().isBefore(moment(sunTimes.night))
    ) {
      // console.log('*** Sunset')
      return ImageConstants.periods.sunset[this.sunsetIdx];
    }

    // 4) Night: night until dawn
    let nextDawn = moment(sunTimes.dawn).add(1, 'days');
    let prevNight = moment(sunTimes.night).subtract(1, 'days');
    if (
      (moment().isAfter(moment(sunTimes.night)) &&
        moment().isBefore(nextDawn)) ||
      (moment().isAfter(prevNight) && moment().isBefore(moment(sunTimes.dawn)))
    ) {
      return ImageConstants.periods.night[this.nightIdx];
    }

    return ImageConstants.backgrounds[0];
  };

  onToggleJournalSelector = () => {
    this.journalCoverRef.toggleJournalSelector();
  };

  onShowJournalSelector = () => {
    this.setState({
      isFirstVisit: true,
    });
    // this.journalCoverRef.showJournalSelector();
    this.onToggleJournalSelector();
  };

  onSelectCover = (index) => {
    this.journalCoverRef.hideJournalSelector();
    this.setState({
      selectedTagIndex: index,
      isFirstVisit: false,
    });
  };

  onChangeCover = (type, index) => {
    this.setState({
      selectedTagIndex: index,
    });

    global.eventEmitter.emit('CHANGED_COVER', {type: type, index: index});
  };

  onSelectJournal = (index) => {
    global.eventEmitter.emit('CHANGED_COVER', {
      type: index >= 0 ? this.props.tags[index].type : 'All',
      index: index,
    });
    this.onToggleJournalSelector();
    setTimeout(() => {
      const {footerMode} = this.state;
      if (footerMode === 1) {
        // compose mode
        // open that journal and take them to the compose screen for that journal
        this.onSelectMode(2);
      } else {
        // reader mode
        this.setState(
          {
            note: null,
            selectedTagIndex: index,
          },
          () => {
            this.refreshFilteredNotes();
          },
        );
      }
    }, 100);
  };

  onSelectMode = (mode) => {
    this.setState(
      {
        footerMode: mode,
      },
      () => {
        this.onSelectCover(this.state.selectedTagIndex);
        RootNavigation.navigate('FeedStack');
      },
    );
  };

  goComposeMode = () => {
    this.onSelectMode(2);
  };

  _getTagIndex = () => {
    const {selectedTagIndex, note} = this.state;
    const {tags} = this.props;
    let tagIndex = selectedTagIndex;
    if (note && tagIndex === -1) {
      tagIndex = tags.findIndex((item) => {
        return item.type === note.note.main_tag;
      });
    }
    return tagIndex;
  };

  onInsertTag = (tag) => {
    let insertedTags = JSON.parse(JSON.stringify(this.state.insertedTags));
    let found = insertedTags.indexOf(tag);
    if (found > -1) {
      insertedTags.splice(found, 1);
    } else {
      insertedTags.push(tag);
    }
    this.setState({
      insertedTags: insertedTags,
    });
  };

  onSelectBackground = (index) => {
    const {background} = this.state;
    if (background !== index) {
      Utils.trackEvent('Action', 'User selects a background image');
    }

    let new_bg = background === index ? -1 : index;
    this.setState(
      {
        background: new_bg,
      },
      () => {
        if (this.state.background === -1) {
          this.setState({
            fontColor: Colors.scheme.blue,
          });
        }
      },
    );
    global.eventEmitter.emit('BACKGROUND_CHANGED', {background: new_bg});
  };

  onPickColor = (color) => {
    const {background} = this.state;
    if (background === -1 && color === 'black') {
      return;
    }
    this.setState({
      fontColor: color,
    });
    global.eventEmitter.emit('COLOR_CHANGED', {color: color});
  };

  updateNewTags = () => {
    const {tags} = this.props;
    const {note, newTags, insertedTags} = this.state;
    const body = Utils.decrypt(note.note.note_orig);
    const selectedTagIndex = this._getTagIndex();
    let data = JSON.parse(JSON.stringify(newTags));
    let insertedTagsData = JSON.parse(JSON.stringify(insertedTags));
    let curTags = Utils.getTags(body);
    let updatedBody = body;

    if (curTags) {
      curTags.forEach((t, i) => {
        if (
          newTags.indexOf(t.substr(1)) === -1 &&
          tags[selectedTagIndex].tags.indexOf(t.substr(1)) === -1
        ) {
          data.push(t.substr(1));
          insertedTagsData.push(t.substr(1));
        }

        let from = t + ' ';
        updatedBody = updatedBody.replace(from, '');
      });
      this.setState({
        // body: updatedBody,
        newTags: data,
        insertedTags: insertedTagsData,
      });
    }
  };

  render() {
    const {
      loading,
      note,
      otherNote,
      isOpenedThreeQ,
      isOpenedResetUp,
      loadingNotes,
      isOpenedDoorway,
      isOpenedDoorwayDelay,
      footerMode,
      selectedTagIndex,
      isFirstVisit,
      editMode,
      newTags,
      insertedTags,
      fontColor,
      background,
      searchTags,
      searchText,
    } = this.state;
    const {
      mongoNotes,
      location,
      doorway,
      failedQueue,
      tags,
      fontSize,
    } = this.props;
    let isVisibleFooter = RootNavigation.getVisibleFooter();

    let filteredNotes = mongoNotes;
    if (failedQueue && failedQueue.length > 0) {
      let reversed = [...failedQueue].reverse();
      filteredNotes = reversed.concat(filteredNotes);
    }

    if (searchText !== '' || searchTags.length > 0) {
      // search
      filteredNotes = filteredNotes.filter((n) => {
        let retSearchText = true;
        if (searchText !== '') {
          let text = Utils.decrypt(n.note.note_text);
          retSearchText = text.toLowerCase().indexOf(searchText) >= 0;
        }

        let retSearchTags = false;
        if (searchTags.length === 0) {
          retSearchTags = true;
        } else {
          if (n.note.note_tags && n.note.note_tags.length > 0) {
            searchTags.forEach((t) => {
              if (n.note.note_tags.indexOf(t) >= 0) {
                retSearchTags = true;
              }
            });
          }
        }
        return retSearchText && retSearchTags;
      });
    }

    let selectedNote = null;
    let selectedOtherNote = null;
    if (note) {
      selectedNote = note.note;
    }

    if (otherNote) {
      selectedOtherNote = otherNote.note;
    }

    let cloudsOpacity = this._getCloudsOpacity();
    let visibleFooter = RootNavigation.getVisibleFooter();

    let visibleVideo = true;
    if (mongoNotes && mongoNotes.length > 0) {
      let diffDays =
        moment().diff(moment(mongoNotes[0].note.creation_timestamp)) /
        1000 /
        3600 /
        24;
      if (diffDays < 5) {
        visibleVideo = false;
      }
    }

    return (
      <Locker {...this.props}>
        <SafeAreaView style={styles.container}>
          <View style={{flex: 1}}>
            <ImageBackground
              style={{flex: 1}}
              imageStyle={{height: Utils.getClientHeight()}}
              source={this._getBackground()}>
              {visibleVideo && (
                <Video
                  source={require('../../../assets/videos/103814411_1080p.mov')}
                  style={styles.backgroundVideo}
                  muted={true}
                  repeat={true}
                  resizeMode={'cover'}
                  rate={1.0}
                  ignoreSilentSwitch={'obey'}
                />
              )}
              <Image
                source={clouds_bg}
                style={[styles.cloudsImg, {opacity: cloudsOpacity}]}
              />
              {location !== '' && (
                <>
                  {/* <ThreeQJourney
                    {...this.props}
                    isOpenedModal={isOpenedThreeQ}
                    isOpenedOthers={isOpenedResetUp}
                    onOpenModal={(v, cb) =>
                      this.onOpenModal('isOpenedThreeQ', v, cb)
                    }
                    onRefreshNotes={this.onRefreshNotes}
                  /> */}

                  {/* <RestUpJourney
                    {...this.props}
                    isOpenedModal={isOpenedResetUp}
                    isOpenedOthers={isOpenedThreeQ}
                    onOpenModal={(v, cb) =>
                      this.onOpenModal('isOpenedResetUp', v, cb)
                    }
                    onRefreshNotes={this.onRefreshNotes}
                  /> */}
                </>
              )}

              <LoadingOverlay loading={loading} />

              <View style={{flex: 1, zIndex: 9999}}>
                {!isOpenedThreeQ && !isOpenedResetUp && selectedNote && (
                  <NoteModal
                    ref={(ref) => (this.noteModalRef = ref)}
                    {...this.props}
                    setLoading={(v) => this.setState({loading: v})}
                    initNote={this.initNoteStates}
                    loading={loadingNotes}
                    onPullNotes={this.onPullNotes}
                    note={note}
                    insertedTags={insertedTags}
                    setNote={(v) => {
                      this.setState({note: v});
                      this._updateInsertedTags(v);
                    }}
                    fontColor={fontColor}
                    background={background}
                    body={Utils.decrypt(
                      typeof selectedNote.note_text !== 'undefined'
                        ? selectedNote.note_text
                        : selectedNote.note_orig,
                    )}
                    newTags={newTags}
                    selectedTagIndex={this._getTagIndex()}
                    onInsertTag={this.onInsertTag}
                    onPickColor={this.onPickColor}
                    onSelectBackground={this.onSelectBackground}
                    goComposeMode={this.goComposeMode}
                    searchArgs={{searchText, searchTags}}
                  />
                )}
                {!isOpenedThreeQ &&
                  !isOpenedResetUp &&
                  selectedOtherNote &&
                  selectedOtherNote.permissions.read === 'any' && (
                    <OtherNoteModal
                      {...this.props}
                      note={otherNote}
                      initNote={this.initNoteStates}
                      setNote={this.setOtherNote}
                      onClose={() => this.setOtherNote(null)}
                    />
                  )}
                {/* <GestureRecognizer
                  onSwipeLeft={this.onSwipeLeft}
                  onSwipeRight={this.onSwipeRight}
                  config={config}
                  style={{
                    flex: 1
                  }}>
                  </GestureRecognizer> */}
              </View>
              {!isOpenedThreeQ && !isOpenedResetUp && (
                <>
                  {!visibleVideo && (
                    <OtherNotes
                      {...this.props}
                      initNote={this.initNoteStates}
                      note={otherNote}
                      setNote={this.setOtherNote}
                    />
                  )}

                  <View style={{height: 140}}>
                    {loadingNotes && (
                      <View style={styles.notesLoader}>
                        <GifLoader />
                      </View>
                    )}
                    {filteredNotes.length > 0 && (
                      <FlatList
                        horizontal={true}
                        data={filteredNotes}
                        keyExtractor={(item, index) => item._id}
                        renderItem={this._renderItem}
                        onEndReachedThreshold={0.05}
                        onEndReached={this.onPullNotes}
                      />
                    )}
                  </View>
                  {visibleFooter === false && <View style={{height: 48}} />}
                </>
              )}

              <JournalCover
                ref={(ref) => (this.journalCoverRef = ref)}
                isFirstVisit={isFirstVisit}
                tags={tags}
                from="stream"
                index={selectedTagIndex}
                onPress={this.onSelectCover}
                onChange={this.onChangeCover}
                onSelectJournal={this.onSelectJournal}
                setSettings={this.props.setSettings}
              />

              <FooterTrigger
                screen="stream"
                currentNote={note}
                flowJourney={this.props.flowJourney}
                defaultJournal={
                  selectedTagIndex >= 0
                    ? this.props.tags[selectedTagIndex].type
                    : 'All'
                }
                searchText={searchText}
                searchTags={searchTags}
                isOpenedOwnNote={note ? true : false}
                mode={footerMode}
                editMode={editMode}
                color={fontColor}
                tags={this.props.tags}
                onSelectMode={this.onSelectMode}
                onToggleJournalSelector={this.onToggleJournalSelector}
                onShowJournalSelector={this.onShowJournalSelector}
                onPickColor={this.onPickColor}
              />
              {isVisibleFooter && note && editMode && (
                <View style={{zIndex: 999999}}>
                  <FooterTags
                    fontSize={fontSize}
                    selectedIndex={this._getTagIndex()}
                    insertedTags={insertedTags}
                    allTags={tags}
                    newTags={newTags}
                    note={Utils.decrypt(selectedNote.note_text)}
                    insertTag={this.onInsertTag}
                  />
                  <ImageSelector
                    value={background}
                    onSelect={this.onSelectBackground}
                  />
                </View>
              )}
            </ImageBackground>
          </View>
        </SafeAreaView>
        {/* {isOpenedDoorwayDelay && (
          <View
            style={{
              position: 'absolute',
              zIndex: 9999,
              width: '100%',
              height: '100%',
            }}>
            <Fade visible={isOpenedDoorway}>
              <View style={{width: '100%', height: '100%'}}>
                <Doorways onClose={this.onPressDoorway} />
              </View>
            </Fade>
          </View>
        )} */}
      </Locker>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tonePack: state.SettingReducer.tonePack,
    flowJourney: state.SettingReducer.flowJourney,
    toneEnabled: state.SettingReducer.toneEnabled,
    spotifyEnabled: state.SettingReducer.spotifyEnabled,
    location: state.SettingReducer.location,
    locations: state.SettingReducer.locations,
    fontSize: state.SettingReducer.fontSize,
    snoozeTime: state.SettingReducer.snoozeTime,
    cloudsMultiplier: state.SettingReducer.cloudsMultiplier,
    cloudsStartTime: state.SettingReducer.cloudsStartTime,
    q3SavePeriod: state.SettingReducer.q3SavePeriod,
    lastSavedTime: state.SettingReducer.lastSavedTime,
    overrideSystemTime: state.SettingReducer.overrideSystemTime,
    tempSystemDateTime: state.SettingReducer.tempSystemDateTime,
    threeQJourneyEnabled: state.SettingReducer.threeQJourneyEnabled,
    guide: state.SettingReducer.guide,
    noteVisibility: state.SettingReducer.noteVisibility,
    depthAndSize: state.SettingReducer.depthAndSize,
    doorway: state.SettingReducer.doorway,
    error: state.MongoReducer.error,
    todoNotes: state.MongoReducer.todoNotes,
    mongoNotes: state.MongoReducer.mongoNotes,
    mongoOtherNotes: state.MongoReducer.mongoOtherNotes,
    failedQueue: state.MongoReducer.failedQueue,
    otherSingleUserNotes: state.MongoReducer.otherSingleUserNotes,
    tags: state.SettingReducer.tags,
    profiles: state.SettingReducer.profiles,
    generalPreferences: state.SettingReducer.generalPreferences,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createNote: (req) => dispatch(MongoActions.createNote(req.data, req.cb)),
    archiveNote: (req) => dispatch(MongoActions.archiveNote(req.data, req.cb)),
    getNotes: (req) =>
      dispatch(MongoActions.getNotes(req.page, req.cb, req.size, req.exclude)),
    getFilteredNotes: (req) =>
      dispatch(
        MongoActions.getFilteredNotes(req.tag, req.page, req.cb, req.size),
      ),
    getExceptNotes: (req) =>
      dispatch(MongoActions.getExceptNotes(req.page, req.cb)),
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
    updateNotePermissions: (req) =>
      dispatch(
        MongoActions.updateNotePermissions(req.data, req.permissions, req.cb),
      ),
    setFailedQueue: (req) => dispatch(MongoActions.setFailedQueue(req.data)),
    getNotesOtherSingle: (req) =>
      dispatch(MongoActions.getNotesOtherSingle(req.user_id, req.page, req.cb)),
    updateNoteImage: (req) =>
      dispatch(
        MongoActions.updateNoteImage(req.note_id, req.image_url, req.cb),
      ),
    updateNoteVideoURL: (req) =>
      dispatch(
        MongoActions.updateNoteVideoURL(req.note_id, req.video_url, req.cb),
      ),
    getToDoNotes: (req) =>
      dispatch(MongoActions.getToDoNotes(req.page, req.cb, req.size)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StreamScreen);
