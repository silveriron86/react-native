/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  ImageBackground,
} from 'react-native';
import moment from 'moment-timezone';
// import Spotify from 'rn-spotify-sdk';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import ModalWrapper from 'react-native-modal-wrapper';
import RNPickerSelect from 'react-native-picker-select';
import SettingConstants from '../../constants/SettingConstants';
import ImageConstants from '../../constants/ImageConstants';
import DummyData from '../../constants/DummyData';
import ApiConstants from '../../constants/ApiConstants';
import LoadingOverlay from '../../widgets/LoadingOverlay';
import {MongoActions, SettingActions, CommonActions} from '../../actions';
import Utils from '../../utils';
import styles from './_styles';

let SELECT_HOW_SLEEP = [];
SettingConstants.HOW_SLEEP.forEach((v, _i) => {
  SELECT_HOW_SLEEP.push({label: v, value: v});
});

const SELECT_TIMES = Utils.getTimes();

class RestUpJourney extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.initialState;
  }

  get initialState() {
    return {
      isNewToday: true,
      spotifyInitialized: false,
      loading: false,
      howSleep: 'good',
      goBedTime: '22:00',
      outBedTime: '07:00',
    };
  }

  componentDidMount() {
    this.mount = true;
    this._init();
  }
  _init = () => {
    if (!this.mount) {
      return;
    }
    const {spotifyEnabled} = this.props;

    this._setNewToday();

    setTimeout(() => {
      this._scheduleTask();
    }, 1000);

    global.scheduleTimer = setInterval(
      this._scheduleTask,
      SettingConstants.JIMMY_SCHEDULER_INTERVAL,
    );
    global.eventEmitter.addListener('LOGOUT', (_data) => {
      if (!this.mount) {
        return;
      }
      this.setState(this.initialState);
    });

    if (spotifyEnabled === true) {
      Utils.initSpotify(() => {
        this.setState({
          spotifyInitialized: true,
        });
      });
    }
  };

  _setNewToday = () => {
    if (!this.mount) {
      return;
    }

    const {restUpJourney, location} = this.props;
    if (restUpJourney.lastSavedTime) {
      let lastDay = Utils.toTimezone(restUpJourney.lastSavedTime, location);
      let today = Utils.nowTimezone(this.props);

      this.setState({
        isNewToday: !(lastDay.substr(0, 10) === today.substr(0, 10)),
      });
    }
  };

  _getIntroText = () => {
    const {restUpJourney} = this.props;
    let index = Utils.getRandomInt(
      0,
      DummyData.restUp[restUpJourney.guide].introTexts.length - 1,
    );
    // console.log(index);
    return DummyData.restUp[restUpJourney.guide].introTexts[index];
  };

  _scheduleTask = () => {
    if (!this.mount) {
      return;
    }

    const {isNewToday} = this.state;
    const {restUpJourney, isOpenedModal, isOpenedOthers} = this.props;

    let now = Utils.nowMoment(this.props);
    let hour = now.format('H');
    // console.log(
    //   'Rest Up: ',
    //   hour,
    //   restUpJourney.checkInAt,
    //   isOpenedModal,
    //   isOpenedOthers,
    // );

    if (/*restUpJourney.enabled === true*/ false && isOpenedOthers === false) {
      if (isOpenedModal === false) {
        // console.log(hour, restUpJourney.checkInAt)
        if (hour >= restUpJourney.checkInAt) {
          if (isNewToday) {
            this.setState(
              {
                note: null,
                isNewToday: false,
                introText: this._getIntroText(),
              },
              () => {
                this._openQuestionModal();
              },
            );
          }
        } else {
          this.setState({
            isNewToday: true,
          });
          this.onCloseModal();
        }
      }
    } else {
      this.onCloseModal();
    }
  };

  componentWillUnmount() {
    this.mount = false;
  }

  onCloseModal = () => {
    this.props.onOpenModal(false, null);
  };

  onContinue = () => {
    this.setState({
      introText: '',
    });
  };

  onSnooze = () => {
    this.props.onOpenModal(false, () => {
      setTimeout(() => {
        if (this.state.isNewToday === false) {
          if (!this.mount) {
            return;
          }
          this._openQuestionModal();
        }
      }, parseInt(this.props.snoozeTime, 10) * 1000 * 60);
    });
  };

  onSave = () => {
    const {restUpJourney, toneEnabled, noteVisibility} = this.props;
    const {howSleep, goBedTime, outBedTime} = this.state;
    this.onCloseModal();

    let origText = 'Last night I slept ' + howSleep;

    let note = {
      type: 'sleep',
      wake_time: outBedTime,
      bed_time: goBedTime,
      note_orig: Utils.encrypt(origText),
      note_tags: Utils.getTags(origText),
      creation_timestamp: moment().tz(this.props.location).format(),
      entry_ip: '0.0.0.0',
      entry_app_type: 'mobile',
      entry_app_version: '1.0',
      ideal_sleep_amount: restUpJourney.sleepAmount,
      ideal_bed_time: Utils.getIdealBedTime(
        restUpJourney.wakeTime,
        restUpJourney.sleepAmount,
      ),
      ideal_wake_time: restUpJourney.wakeTime,
      word_count: Utils.getWords(origText),
      font: this.props.streamFont,
      font_size: this.props.fontSize,
      permissions: {
        see: noteVisibility.sleep[0],
        read: noteVisibility.sleep[1],
      },
      screen_name: this.props.screenName,
      main_tag: 'sleep',
    };
    let saved = Utils.nowUTC(this.props);

    let lastData = JSON.parse(JSON.stringify(restUpJourney));
    lastData.lastSavedTime = saved;
    this.props.setSettings({type: 'REST_UP_JOURNEY', value: lastData});

    if (toneEnabled === true) {
      let apiData = ApiConstants.getToneAnalyzerApi();
      this.setState(
        {
          loading: true,
        },
        () => {
          axios({
            method: 'GET',
            url: `${apiData.url}/v3/tone?version=${
              apiData.version
            }&text=${encodeURIComponent(origText)}`,
            headers: {
              Authorization: 'Basic ' + Utils.btoa('apiKey:' + apiData.key),
            },
          })
            .then((response) => {
              if (response.status === 200) {
                note.ibm_tone = {
                  document_tone: response.data.document_tone,
                };
                this._procSpotifySaveNote(note);
              } else {
                this.setState(
                  {
                    loading: false,
                  },
                  () => {
                    Utils.saveFailedQueue('Tone analyzer service', note);
                  },
                );
              }
            })
            .catch((_error) => {
              this.setState(
                {
                  loading: false,
                },
                () => {
                  Utils.saveFailedQueue('Tone analyzer service', note);
                },
              );
            });
        },
      );
    } else {
      //disabled tone setting
      this._procSpotifySaveNote(note);
    }
  };

  onChangeAnswer = (text) => {
    this.setState({
      answer: text,
    });
  };

  _openQuestionModal = () => {
    return;
    if (this.props.restUpJourney.enabled === true) {
      AsyncStorage.getItem('CURRENT_TAB', (_err, tab) => {
        if (this.state.isNewToday === false) {
          if (tab === 'SettingsStack') {
            setTimeout(this._openQuestionModal, 5000);
          } else {
            if (this.props.isOpenedOthers === true) {
              setTimeout(() => {
                if (this.props.isOpenedOthers === false) {
                  this.props.onOpenModal(true, null);
                }
              }, 3000);
            } else {
              this.props.onOpenModal(true, null);
            }
          }
        }
      });
    }
  };

  onAskLater = () => {
    this.props.onOpenModal(false, () => {
      setTimeout(() => {
        this._openQuestionModal();
      }, parseInt(this.props.snoozeTime, 10) * 1000 * 60);
    });
  };

  _saveNote = (note) => {
    Utils.trackEvent('Action', 'User saves a Rest Up note');

    const {howSleep, goBedTime, outBedTime} = this.state;
    AsyncStorage.getItem('USER_ID', (_err, user_id) => {
      let arg = JSON.parse(JSON.stringify(note));
      arg.user_id = user_id;
      this.setState(
        {
          loading: true,
        },
        () => {
          this.props.createNote({
            data: arg,
            cb: (res) => {
              console.log(res);
              this.props.setLastNote({
                value: arg,
                id: res.insertedId,
              });

              AsyncStorage.getItem(
                'IBM_PERSONALITY',
                (__err, ibm_personality) => {
                  console.log(ibm_personality);
                  let user = {
                    auth_provider: 'auth0',
                    user_id_auth: user_id,
                  };
                  if (ibm_personality) {
                    user.ibm_personality = JSON.parse(ibm_personality);
                  }
                  let data = {
                    user: user,
                  };
                  console.log('*** Saved ***');
                  console.log(data);
                  this.setState(
                    {
                      loading: false,
                    },
                    () => {
                      global.eventEmitter.emit('NOTE_CREATED', {
                        timestamp: arg.creation_timestamp,
                      });
                      this.props.onRefreshNotes();

                      if (res.error) {
                        Utils.saveFailedQueue('Note creation', note);
                      } else {
                        global.eventEmitter.emit('SAVED_RESTUP', {
                          goBedTime: moment(goBedTime, 'HH:mm').format(
                            'hh:mma',
                          ),
                          outBedTime: moment(outBedTime, 'HH:mm').format(
                            'hh:mma',
                          ),
                          howSleep,
                        });
                      }

                      this.props.props.navigation.navigate('FeedStack');
                    },
                  );
                },
              );
            },
          });
        },
      );
    });
  };

  _getRecentlyPlayed = (_session, _note) => {
    /*
    this.setState({
      loading: true
    },() => {
      axios({
        method: 'GET',
        url: `${ApiConstants.getSpotifyApi().url}/me/player/recently-played`,
        headers: {
          Authorization: "Bearer " + session.accessToken
        }
      }).then(response => {
        // last 5 songs played
        let played = response.data.items;
        if(played.length > 5) {
          played = played.slice(0, 5);
        }
        note['spotify_history'] = played;
        this._saveNote(note)
      })
      .catch(error => {
        this.setState({
          loading: false
        },() => {
          alert(error.message);
        })
      })
    })*/
  };

  onCheckSpotify = (_note) => {
    /*
    let session = Spotify.getSession();
    if(session === null) {
      // log into Spotify
      Spotify.login().then((loggedIn) => {
        if(loggedIn) {
          // logged in
          session = Spotify.getSession();
          this._getRecentlyPlayed(session, note);
        }else {
          // cancelled
          this._saveNote(note)
        }
      }).catch((error) => {
        this.setState({
          loading: false
        },() => {
          alert(error.message);
        })
      });
    }else {
      this._getRecentlyPlayed(session, note);
    }*/
  };

  _procSpotifySaveNote = (note) => {
    /*
    if(this.props.spotifyEnabled === true) {
      this.onCheckSpotify(note);
    }else {*/
    this._saveNote(note);
    // }
  };

  render() {
    const {introText, loading, howSleep, goBedTime, outBedTime} = this.state;
    const {isOpenedModal, isOpenedOthers, restUpJourney} = this.props;

    let w = Dimensions.get('window').width - 20;
    let h = (w * 305) / 404;
    let continueBtnText = 'Continue';
    let snoozeBtnText = 'Snooze';

    return (
      <>
        <LoadingOverlay loading={loading} />
        <ModalWrapper
          containerStyle={styles.modalContainer}
          style={[
            styles.modal,
            introText === '' && {backgroundColor: 'transparent'},
          ]}
          onRequestClose={this.onCloseModal}
          shouldAnimateOnRequestClose={true}
          shouldCloseOnOverlayPress={false}
          visible={isOpenedModal === true && isOpenedOthers === false}
          position={'top'}>
          <View style={styles.modalInner}>
            {introText !== '' && (
              <>
                <View style={styles.jimmyWrapper}>
                  <Image
                    source={
                      restUpJourney.guide === 'jimmy'
                        ? ImageConstants.icons.jimmy
                        : ImageConstants.icons.robot
                    }
                    style={styles.jimmyIcon}
                  />
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                  <Text style={styles.questionText}>{introText}</Text>
                </View>
              </>
            )}
            <View
              style={[
                styles.answerWrapper,
                introText === '' && {marginTop: 0},
              ]}>
              {introText ? (
                <ImageBackground
                  imageStyle={{width: w, height: h}}
                  style={styles.noteBg}
                  source={ImageConstants.note}>
                  <View
                    style={{
                      marginTop: 35,
                      width: w - 30,
                      height: h - 85,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <TouchableOpacity onPress={this.onContinue}>
                      <Text style={[styles.laterBtnText, {fontSize: 24}]}>
                        {continueBtnText}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity
                    style={styles.laterBtn}
                    onPress={this.onSnooze}>
                    <Text style={[styles.laterBtnText]}>{snoozeBtnText}</Text>
                  </TouchableOpacity>
                </ImageBackground>
              ) : (
                <View style={styles.answerContainer}>
                  {/* <TextInput placeholder={'Enter answer here...'} placeholderTextColor={'#3079c195'} value={answer} onChangeText={this.onChangeAnswer} onKeyPress={this.onKeyPressAnswer} multiline={true} style={[{marginTop: 35, width: w-30, height: h-85}, styles.textWrapper, styles.noteText, {fontFamily: streamFont, fontSize: fontSize}]}/> */}
                  <View style={styles.restUpRow}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>
                        What time did you go to bed?
                      </Text>
                    </View>
                    <RNPickerSelect
                      placeholder={{}}
                      items={SELECT_TIMES}
                      onValueChange={(v) => {
                        this.setState({
                          goBedTime: v,
                        });
                      }}
                      value={goBedTime}
                      style={selectStyle}
                      useNativeAndroidPickerStyle={false}
                      textInputProps={selectTextInputStyle}
                    />
                  </View>

                  <View style={styles.restUpRow}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>
                        What time did you get out of bed?
                      </Text>
                    </View>
                    <RNPickerSelect
                      placeholder={{}}
                      items={SELECT_TIMES}
                      onValueChange={(v) => {
                        this.setState({
                          outBedTime: v,
                        });
                      }}
                      value={outBedTime}
                      style={selectStyle}
                      useNativeAndroidPickerStyle={false}
                      textInputProps={selectTextInputStyle}
                    />
                  </View>

                  <View style={styles.restUpRow}>
                    <View style={styles.switchLabel}>
                      <Text style={styles.labelText}>How did you sleep?</Text>
                    </View>
                    <RNPickerSelect
                      placeholder={{}}
                      items={SELECT_HOW_SLEEP}
                      onValueChange={(v) => {
                        this.setState({
                          howSleep: v,
                        });
                      }}
                      value={howSleep}
                      style={selectStyle}
                      useNativeAndroidPickerStyle={false}
                      textInputProps={selectTextInputStyle}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: 5,
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{}}>
                      <TouchableOpacity onPress={this.onSave}>
                        <Text style={[styles.laterBtnText, {fontSize: 24}]}>
                          Save
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                      style={styles.laterBtn}
                      onPress={this.onAskLater}>
                      <Text
                        style={[
                          styles.laterBtnText,
                          {fontFamily: 'PremiumUltra5'},
                        ]}>
                        Snooze
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )}
            </View>
          </View>
        </ModalWrapper>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    screenName: state.SettingReducer.screenName,
    streamFont: state.SettingReducer.streamFont,
    toneEnabled: state.SettingReducer.toneEnabled,
    spotifyEnabled: state.SettingReducer.spotifyEnabled,
    fontSize: state.SettingReducer.fontSize,
    snoozeTime: state.SettingReducer.snoozeTime,
    overrideSystemTime: state.SettingReducer.overrideSystemTime,
    tempSystemDateTime: state.SettingReducer.tempSystemDateTime,
    threeQJourneyEnabled: state.SettingReducer.threeQJourneyEnabled,
    restUpJourney: state.SettingReducer.restUpJourney,
    noteVisibility: state.SettingReducer.noteVisibility,
    location: state.SettingReducer.location,
    error: state.MongoReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLastNote: (req) =>
      dispatch(CommonActions.setLastNote(req.value, req.id)),
    createNote: (req) => dispatch(MongoActions.createNote(req.data, req.cb)),
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RestUpJourney);

const selectStyle = {
  viewContainer: {
    marginTop: 5,
    paddingVertical: 5,
    // paddingRight: 5,
    width: 100,
    borderWidth: 1,
    borderColor: '#CCC',
    backgroundColor: 'white',
    alignSelf: 'center',
  },
  inputAndroid: {
    backgroundColor: 'transparent',
  },
  iconContainer: {
    top: 5,
    right: 0,
  },
};

const selectTextInputStyle = {
  underlineColorAndroid: 'cyan',
  // marginRight: 20,
  textAlign: 'center',
};
