/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  ScrollView,
  ImageBackground,
  Image,
  Text,
  TouchableOpacity,
  Dimensions,
  Alert,
  TextInput,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import ImageView from 'react-native-image-view';
import ImageProgress from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import RNPickerSelect from 'react-native-picker-select';
import {Chevron} from 'react-native-shapes';
import storage from '@react-native-firebase/storage';
import uuid from 'uuid/v4';
import moment from 'moment';
import SettingConstants from '../../../constants/SettingConstants';
import LoadingOverlay from '../../../widgets/LoadingOverlay';
import Utils from '../../../utils';
import styles from '../_styles';
import {selectStyle, selectTextInputStyle} from '../../Settings/_styles';
import Colors from '../../../constants/Colors';

const note_bg = require('../../../../assets/images/notebook_bg.png');
const sync_bg = require('../../../../assets/images/awaiting_sync.png');
const trashIcon = require('../../../../assets/icons/trash.png');
const archiveIcon = require('../../../../assets/icons/archive.png');
const lockIcon = require('../../../../assets/icons/lock.png');
const addPhotoIcon = require('../../../../assets/icons/camera-white-1.png');
const ellipsesIcon = require('../../../../assets/icons/ellipses.png');
const closeIcon = require('../../../../assets/icons/glyph_close_32x32.png');
const globeIcon = require('../../../../assets/icons/globe.png');
const pencilIcon = require('../../../../assets/icons/pencil2.png');

export default class QuestionNote extends React.Component {
  constructor(props) {
    super(props);

    let selectedNote = props.note;
    console.log(selectedNote.imageURL);
    this.state = {
      loading: false,
      isClickedEllipses: false,
      isClickedTrash: false,
      isClickedLock: false,
      canSee: 'any',
      canRead: 'any',
      imageUri: '',
      imgSource: selectedNote.imageURL ? {uri: selectedNote.imageURL} : '',
      progress: 0,
      isImageViewVisible: false,
      isEditMode: false,
      inputNoteText: '',
    };
  }

  componentDidMount() {
    this._setPermission(this.props.note);
  }

  toggleEllipses = () => {
    this.setState(
      {
        isClickedEllipses: !this.state.isClickedEllipses,
      },
      () => {
        if (this.state.isClickedEllipses === false) {
          this.setState({
            isClickedTrash: false,
            isClickedLock: false,
          });
        }
      },
    );
  };

  _setPermission = (item) => {
    const {noteVisibility} = this.props;
    let type = 'standard';
    if (item.note.type) {
      type = item.note.type;
    }

    this.setState({
      canSee: item.note.permissions
        ? item.note.permissions.see
        : noteVisibility[type][0],
      canRead: item.note.permissions
        ? item.note.permissions.read
        : noteVisibility[type][1],
    });
  };

  toggleTrash = () => {
    this.setState({
      isClickedTrash: !this.state.isClickedTrash,
      isClickedLock: false,
    });
  };

  toggleEdit = () => {
    this.setState({
      inputNoteText: Utils.decrypt(this.props.note.note.note_orig),
      isEditMode: true,
    });
  };

  toggleLock = () => {
    this.setState({
      isClickedLock: !this.state.isClickedLock,
      isClickedTrash: false,
    });
  };

  handleArchive = () => {
    this.props.setLoading(true);

    this.props.archiveNote({
      data: this.props.note,
      cb: (res) => {
        global.eventEmitter.emit('NOTE_CREATED', {});
        this._initNoteState();
      },
    });
  };

  handleDelete = () => {
    let {failedQueue, note} = this.props;
    failedQueue.forEach((element, index) => {
      if (element._id === note._id) {
        failedQueue.splice(index, 1);
        this._initNoteState();
        return this.props.setFailedQueue({
          data: failedQueue,
        });
      }
    });
  };

  _initNoteState = () => {
    this.setState(
      {
        isClickedEllipses: false,
        isClickedTrash: false,
        isClickedLock: false,
      },
      () => {
        this.props.initNote();
      },
    );
  };

  handleLock = () => {
    this.props.setLoading(true);
    this.props.updateNotePermissions({
      data: this.props.note,
      permissions: {
        see: this.state.canSee,
        read: this.state.canRead,
      },
      cb: (res) => {
        this._initNoteState();
      },
    });
  };

  onCloseImage = () => {
    this.setState({
      isImageViewVisible: false,
    });
  };

  onDeleteImage = () => {
    Alert.alert(
      'Remove from note?',
      '',
      [
        {
          text: 'Remove',
          onPress: () => {
            this.onCloseImage();
            this.props.updateNoteImage({
              note_id: this.props.note._id,
              image_url: '',
              cb: (res) => {
                console.log(res);
                this.setState({
                  imgSource: '',
                  imageUri: '',
                });
              },
            });
          },
        },
        {
          text: 'Cancel',
          onPress: () => {
            // this.onCloseImage()
          },
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  };

  openPhotoModal = () => {
    this.setState({
      isImageViewVisible: true,
    });
  };

  onAddPhoto = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        // alert('You cancelled image picker 😟');
      } else if (response.error) {
        console.log('And error occured: ', response.error);
      } else {
        console.log(response);
        const source = {uri: response.uri};
        this.setState({
          // imgSource: source,
          imageUri: response.uri,
        });
        const ext = this.state.imageUri.split('.').pop(); // Extract image extension
        const filename = `${uuid()}.${ext}`; // Generate unique name
        this.setState({uploading: true});
        storage()
          .ref(`notes/images/${filename}`)
          .putFile(this.state.imageUri)
          .on(
            storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
              let state = {};
              state = {
                ...state,
                progress:
                  (snapshot.bytesTransferred / snapshot.totalBytes) * 100, // Calculate progress percentage
              };
              if (snapshot.state === storage.TaskState.SUCCESS) {
                console.log(snapshot);
                this.props.updateNoteImage({
                  note_id: this.props.note._id,
                  image_url: snapshot.downloadURL,
                  cb: (res) => {
                    console.log(res);
                    state = {
                      ...state,
                      uploading: false,
                      imgSource: source,
                      imageUri: '',
                      progress: 0,
                    };
                    this.setState(state, () => {
                      this.toggleEllipses();
                    });
                  },
                });
              }
              this.setState(state);
            },
            (error) => {
              // unsubscribe();
              // eslint-disable-next-line no-alert
              alert('Sorry, Try again.');
            },
          );
      }
    });
  };

  onUpdateNoteText = () => {
    console.log(' *** on update *** ');
    const {note} = this.props;
    console.log(note);
    const {inputNoteText} = this.state;
    // call API with inputNoteText
    console.log('text=', inputNoteText);

    this.props.setLoading(true);
    let selectedNote = JSON.parse(JSON.stringify(note.note));

    let origText = inputNoteText;
    let text = origText;
    let tags = Utils.getTags(origText);
    if (tags) {
      tags.forEach((tag, i) => {
        tags[i] = tag.substr(1);
        text = text.replace(tag + ' ', '');
      });
      text = text.replace('#' + tags[tags.length - 1], '');
    }

    selectedNote.note_orig = Utils.encrypt(origText);
    selectedNote.note_text = Utils.encrypt(text);
    selectedNote.note_tags = tags;
    selectedNote.replacesNoteId = note._id;
    selectedNote.replacedNoteTimestamp = moment()
      .tz(this.props.location)
      .format();

    console.log(selectedNote);
    this.props.createNote({
      data: selectedNote,
      cb: (r) => {
        console.log(r);
        this.props.archiveNote({
          data: {
            _id: note._id,
          },
          cb: (_res) => {
            console.log(_res);
            global.eventEmitter.emit('NOTE_CREATED', {});
            this._initNoteState();
          },
        });
      },
    });
  };

  render() {
    const {note, fontSize} = this.props;
    const {
      isClickedTrash,
      isClickedLock,
      isClickedEllipses,
      canSee,
      canRead,
      progress,
      uploading,
      imgSource,
      isImageViewVisible,
      isEditMode,
      inputNoteText,
    } = this.state;

    const w = Dimensions.get('window').width - 20;
    const h = (w * 305) / 404;

    let selectedNote = null;

    if (note) {
      selectedNote = note.note;
    }

    let isFailed = selectedNote.failed === true;
    let selectedFont = 'PremiumUltra5';
    let selectedColor = Colors.scheme.blue;
    if (selectedNote) {
      if (selectedNote.font) {
        selectedFont = selectedNote.font;
      }
      if (selectedNote.color) {
        selectedColor = selectedNote.color;
      }
    }

    // type ControlType = React.Component<{onPress: () => void}> | null | boolean
    return (
      <>
        <LoadingOverlay
          loading={uploading}
          message={`Uploading: ${progress.toFixed(0)}%`}
        />
        <ImageView
          images={[
            {
              source: imgSource,
            },
          ]}
          imageIndex={0}
          isVisible={isImageViewVisible}
          controls={{
            close: () => {
              return (
                <TouchableOpacity
                  onPress={this.onCloseImage}
                  style={styles.closeImgBtn}>
                  <Image source={closeIcon} />
                </TouchableOpacity>
              );
            },
          }}
          // onClose={this.onCloseImage}
          renderFooter={(currentImage) => (
            <TouchableOpacity
              style={styles.removeImgBtn}
              onPress={this.onDeleteImage}>
              <Image
                source={trashIcon}
                resizeMethod={'resize'}
                style={{
                  tintColor: 'white',
                  width: 35,
                  height: 35,
                  resizeMode: 'contain',
                }}
              />
            </TouchableOpacity>
          )}
        />

        <View style={{paddingHorizontal: 10}}>
          <ImageBackground
            imageStyle={{width: w, height: h}}
            style={[styles.noteBg, {height: h - 30}]}
            source={isFailed ? sync_bg : note_bg}>
            <View style={{width: w - 30, flex: 1}}>
              {isClickedLock === false ? (
                <>
                  {isEditMode === true ? (
                    <>
                      <View style={[{marginTop: 35}, styles.textWrapper]}>
                        <TextInput
                          multiline={true}
                          value={inputNoteText}
                          style={{
                            fontFamily: selectedFont,
                            fontSize: fontSize,
                            color: selectedColor,
                            height: 140,
                          }}
                          onChangeText={(v) => {
                            this.setState({inputNoteText: v});
                          }}
                        />
                      </View>
                      <View style={[styles.noteActions, {width: w - 80}]}>
                        <TouchableOpacity
                          onPress={this.onUpdateNoteText}
                          style={styles.archiveSaveBtn}>
                          <Text style={styles.archiveSaveText}>Save</Text>
                        </TouchableOpacity>
                        <View style={{width: 10}} />
                        <TouchableOpacity
                          onPress={() => this.setState({isEditMode: false})}>
                          <Text
                            style={[styles.cancelBtnText, {color: '#4F6A92'}]}>
                            Cancel
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </>
                  ) : (
                    <ScrollView style={[{marginTop: 35}, styles.textWrapper]}>
                      <Text
                        style={[
                          styles.noteText,
                          {
                            fontFamily: selectedFont,
                            fontSize: fontSize,
                            color: selectedColor,
                          },
                        ]}>
                        {selectedNote.note_orig !== ''
                          ? Utils.decrypt(selectedNote.note_orig)
                          : ''}
                      </Text>
                    </ScrollView>
                  )}
                </>
              ) : (
                <View style={{flex: 1, marginLeft: 70, marginTop: 30}}>
                  <Text style={styles.lockText}>
                    Who can see icons and "tones" for this note in their stream?
                  </Text>
                  <RNPickerSelect
                    placeholder={{}}
                    items={SettingConstants.SELECT_VISIBLE_USERS}
                    onValueChange={(v) => {
                      this.setState({canSee: v});
                    }}
                    style={selectStyle}
                    value={canSee}
                    useNativeAndroidPickerStyle={false}
                    textInputProps={selectTextInputStyle}
                    Icon={() => {
                      return <Chevron size={1.5} color="gray" />;
                    }}
                  />
                  <Text style={styles.lockText}>
                    Who can read the text of this note?
                  </Text>
                  <RNPickerSelect
                    placeholder={{}}
                    items={SettingConstants.SELECT_VISIBLE_USERS}
                    onValueChange={(v) => {
                      this.setState({canRead: v});
                    }}
                    style={selectStyle}
                    value={canRead}
                    useNativeAndroidPickerStyle={false}
                    textInputProps={selectTextInputStyle}
                    Icon={() => {
                      return <Chevron size={1.5} color="gray" />;
                    }}
                  />
                </View>
              )}
            </View>
            <View
              style={{flexDirection: 'column', width: w - 23, paddingTop: 5}}>
              {isClickedTrash && (
                <View style={[styles.noteActions, {width: w - 80}]}>
                  {isFailed ? (
                    <TouchableOpacity onPress={this.handleDelete}>
                      <Text style={styles.archiveBtnText}>DELETE</Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      onPress={this.handleArchive}
                      style={styles.archiveSaveBtn}>
                      <Text style={styles.archiveSaveText}>Archive</Text>
                    </TouchableOpacity>
                  )}
                  <View style={{width: 10}} />
                  <TouchableOpacity onPress={this.toggleTrash}>
                    <Text style={[styles.cancelBtnText, {color: '#4F6A92'}]}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                </View>
              )}

              {isClickedLock && (
                <View style={[styles.noteActions, {width: w - 80}]}>
                  <TouchableOpacity
                    onPress={this.handleLock}
                    style={styles.archiveSaveBtn}>
                    <Text style={styles.archiveSaveText}>Save</Text>
                  </TouchableOpacity>
                  <View style={{width: 10}} />
                  <TouchableOpacity onPress={this.toggleLock}>
                    <Text style={[styles.cancelBtnText, {color: '#4F6A92'}]}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingLeft: 50,
                  height: 38,
                }}>
                <View
                  style={{
                    height: 38,
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  {selectedNote.permissions.read === 'any' && (
                    <Image source={globeIcon} style={styles.globeIcon} />
                  )}
                  <TouchableOpacity onPress={this.openPhotoModal}>
                    {imgSource !== '' && (
                      <ImageProgress
                        source={imgSource}
                        indicator={Progress.Circle}
                        indicatorProps={{
                          size: 30,
                          borderWidth: 2,
                          color: '#3079c1',
                          unfilledColor: 'rgba(200, 200, 200, 0.2)',
                        }}
                        style={{width: 38, height: 38, resizeMode: 'contain'}}
                      />
                    )}
                  </TouchableOpacity>
                </View>
                <View
                  style={[
                    styles.noteIcons,
                    isClickedEllipses && {
                      borderWidth: 1,
                      backgroundColor: '#ffffff50',
                      justifyContent: 'space-between',
                    },
                  ]}>
                  {isClickedEllipses && (
                    <>
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity onPress={this.toggleEdit}>
                          <Image
                            source={pencilIcon}
                            resizeMethod={'resize'}
                            style={[
                              styles.trashIcon,
                              {marginRight: 5, tintColor: '#333'},
                            ]}
                          />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.toggleTrash}>
                          <Image
                            source={archiveIcon}
                            resizeMethod={'resize'}
                            style={[
                              styles.trashIcon,
                              {marginRight: 5, tintColor: '#333'},
                            ]}
                          />
                        </TouchableOpacity>
                        {!isFailed && (
                          <TouchableOpacity onPress={this.toggleLock}>
                            <Image
                              source={lockIcon}
                              resizeMethod={'resize'}
                              style={[styles.trashIcon, {tintColor: 'black'}]}
                            />
                          </TouchableOpacity>
                        )}
                        <TouchableOpacity
                          onPress={this.onAddPhoto}
                          style={{marginLeft: 3}}>
                          <Image
                            source={addPhotoIcon}
                            resizeMethod={'resize'}
                            style={[
                              styles.trashIcon,
                              {tintColor: 'black', width: 34},
                            ]}
                          />
                        </TouchableOpacity>
                      </View>
                    </>
                  )}
                  <TouchableOpacity onPress={this.toggleEllipses}>
                    <Image
                      source={ellipsesIcon}
                      resizeMethod={'resize'}
                      style={[
                        styles.ellipsesIcon,
                        isClickedEllipses && {opacity: 1},
                      ]}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </ImageBackground>
        </View>
      </>
    );
  }
}
