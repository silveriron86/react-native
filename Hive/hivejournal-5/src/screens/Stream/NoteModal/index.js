/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  ScrollView,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';

import MoonCloud from '../MoonCloud';
import Utils from '../../../utils';
import styles from '../_styles';
import Colors from '../../../constants/Colors';
import VerticalStream from '../Reader/VerticalStream';
import HorizontalStream from '../Reader/HorizontalStream';
import ToDoReader from '../Reader/Todo/ToDoReader';
import Front from '../Reader/HorizontalStream/Front';
import LoadingOverlay from '../../../widgets/LoadingOverlay';

const closeIcon = require('../../../../assets/icons/glyph_close_32x32.png');

export default class NoteModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alignType: 2,
      isSelectedNote: true,
      selectedNoteIndex: 0,
      selectedTodoItem: null,
      processing: false,
    };
  }

  onShowStreams = (type) => {
    this.setState({
      alignType: type,
    });
  };

  onSetIndex = (index) => {
    this.setState(
      {
        selectedNoteIndex: index,
      },
      () => {
        if (index === -1) {
          // offline
          // const {failedQueue, note} = this.props;
          // console.log(note);
        } else {
          this.props.setNote(this.filterNotes()[index]);
        }
        global.eventEmitter.emit('EDIT_MODE', {mode: false});
      },
    );
  };

  filterNotes = () => {
    const {searchText, searchTags} = this.props.searchArgs;
    return this.props.mongoNotes.filter((n) => {
      if (n.archived !== true) {
        let retSearchText = true;
        if (searchText !== '') {
          let text = Utils.decrypt(n.note.note_text);
          retSearchText = text.toLowerCase().indexOf(searchText) >= 0;
        }

        let retSearchTags = false;
        if (searchTags.length === 0) {
          retSearchTags = true;
        } else {
          if (n.note.note_tags && n.note.note_tags.length > 0) {
            searchTags.forEach((t) => {
              if (n.note.note_tags.indexOf(t) >= 0) {
                retSearchTags = true;
              }
            });
          }
        }
        return retSearchText && retSearchTags;
      }
      return false;
    });
  };

  goEditScreen = (item) => {
    const index = this.filterNotes().findIndex((note) => {
      return note._id === item._id;
    });

    this.setState({
      selectedNoteIndex: index,
      selectedTodoItem: item,
    });
  };

  render() {
    const {
      alignType,
      selectedNoteIndex,
      selectedTodoItem,
      processing,
    } = this.state;
    const {note, streamFont, fontSize, loading} = this.props;
    let selectedNote = null;
    if (note) {
      selectedNote = note.note;
    }

    let isFailed = selectedNote.failed === true;
    let selectedFont = 'PremiumUltra5';
    let selectedColor = Colors.scheme.blue;
    if (selectedNote) {
      if (selectedNote.font) {
        selectedFont = selectedNote.font;
      }
      if (selectedNote.color) {
        selectedColor = selectedNote.color;
      }
    }

    return (
      <>
        {selectedNote.type === '3Q' ? (
          <View
            style={[
              styles.q3note,
              {height: Dimensions.get('window').height - 80},
            ]}>
            <ScrollView style={{height: '100%'}}>
              {selectedNote['3Q_data'].entries.map((entry, iii) => {
                return (
                  <View key={`3q-note-${iii}`}>
                    <View style={[styles.entryRow, {marginTop: 10}]}>
                      <View style={styles.entryCol}>
                        <Text
                          style={{
                            fontFamily: selectedFont,
                            fontSize: 16,
                            color: selectedColor,
                            textAlign: 'right',
                          }}>
                          {'<:'}
                        </Text>
                      </View>
                      <View style={styles.entryCol} />
                    </View>
                    <View style={[styles.entryRow, {marginBottom: 10}]}>
                      <View style={styles.entryCol}>
                        <Text
                          style={{
                            fontFamily: selectedFont,
                            fontSize: 16,
                            color: selectedColor,
                            textAlign: 'right',
                          }}>
                          {Utils.decrypt(entry.a_good)}
                        </Text>
                      </View>
                      <View style={styles.entryCol}>
                        <Text
                          style={{
                            fontFamily: selectedFont,
                            fontSize: 16,
                            color: '#979797',
                            textAlign: 'left',
                          }}>
                          {Utils.decrypt(entry.a_reason)}
                          {'\n'}
                          {':>'}
                        </Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </ScrollView>

            <TouchableOpacity
              style={{position: 'absolute', right: 10, marginTop: 10}}
              onPress={this.props.initNote}>
              <Image source={closeIcon} />
            </TouchableOpacity>
            <View style={[styles.divider, {height: '100%'}]} />
          </View>
        ) : isFailed === false && selectedNote.type === 'sleep' ? (
          <MoonCloud note={note} fontSize={fontSize} isOther={false} />
        ) : (
          <View
            style={{
              position: 'relative',
              width: '100%',
              height: Utils.getClientHeight() - 40,
              justifyContent: 'center',
              flexDirection: 'column',
              backgroundColor: 'rgba(255, 255, 255)',
              overflow: 'hidden',
            }}>
            <View style={{flex: 1}}>
              <View style={{position: 'relative'}}>
                {alignType === 1 ? (
                  <View
                    style={{
                      position: 'absolute',
                      width: '100%',
                      height: Dimensions.get('window').height - 60,
                      justifyContent: 'center',
                      backgroundColor: 'rgba(0, 0, 0, 0.8)',
                    }}>
                    {note.note.main_tag === 'todo' ? (
                      <ToDoReader
                        index={selectedNoteIndex}
                        // slides={this.filterNotes()}
                        // loading={loading}
                        // onEndReached={this.props.onPullNotes}
                        // onShowStreams={this.onShowStreams}
                        // goEditScreen={this.goEditScreen}
                        {...this.props}
                      />
                    ) : (
                      <VerticalStream
                        isOwn={true}
                        index={selectedNoteIndex}
                        slides={this.filterNotes()}
                        streamFont={streamFont}
                        fontSize={fontSize}
                        loading={loading}
                        onEndReached={this.props.onPullNotes}
                        onShowStreams={this.onShowStreams}
                      />
                    )}
                  </View>
                ) : (
                  <HorizontalStream
                    ref="horizontalStream"
                    isOwn={true}
                    selected={note}
                    setIndex={this.onSetIndex}
                    slides={this.filterNotes()}
                    insertedTags={this.props.insertedTags}
                    loading={loading}
                    // profiles={profiles}
                    onEndReached={this.props.onPullNotes}
                    initNote={this.props.initNote}
                    alignType={alignType}
                    onShowStreams={this.onShowStreams}
                    {...this.props}
                  />
                )}
              </View>
            </View>

            {selectedTodoItem && (
              <Front
                isTodo={true}
                index={selectedNoteIndex}
                item={selectedTodoItem}
                setLoading={(v) => this.setState({processing: v})}
                {...this.props}
                initNote={() => this.setState({selectedTodoItem: null})}
              />
            )}
            <LoadingOverlay loading={processing} />
          </View>
        )}
      </>
    );
  }
}
