/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {View, SafeAreaView} from 'react-native';
import FooterTrigger from '../../widgets/FooterTrigger';
import Colors from '../../constants/Colors';
import ToDoReader from '../Stream/Reader/Todo/ToDoReader';
import MongoActions from '../../actions/MongoActions';
import SettingActions from '../../actions/SettingActions';
import JournalCover from '../Notes/JournalCover';
// import LoadingOverlay from '../../widgets/LoadingOverlay';

class ToDoScreen extends React.Component {
  onShowJournalSelector = () => {
    this.journalCoverRef.toggleJournalSelector();
  };

  onSelectCover = (index) => {
    this.journalCoverRef.hideJournalSelector();
    global.eventEmitter.emit('CAME_FROM_TODO', {index});
    this.props.navigation.navigate('FeedStack');
  };

  onChangeCover = (_type, index) => {
    global.eventEmitter.emit('CAME_FROM_TODO', {index});
  };

  render() {
    const {tags} = this.props;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.scheme.black}}>
        <View style={{flex: 1}}>
          <View style={{flex: 1}}>
            <ToDoReader {...this.props} />
          </View>
          <JournalCover
            selectorStyle={{marginTop: 0}}
            ref={(ref) => (this.journalCoverRef = ref)}
            isFirstVisit={false}
            tags={tags}
            from="todo"
            index={2}
            onPress={this.onSelectCover}
            // onChange={this.onChangeCover}
            onSelectJournal={this.onSelectCover}
            setSettings={this.props.setSettings}
          />
          <FooterTrigger
            screen="todo"
            flowJourney={this.props.flowJourney}
            defaultJournal="todo"
            isOpenedOwnNote={false}
            mode={2}
            editMode={false}
            color={Colors.scheme.blue}
            onShowJournalSelector={this.onShowJournalSelector}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tags: state.SettingReducer.tags,
    todoNotes: state.MongoReducer.todoNotes,
    location: state.SettingReducer.location,
    streamFont: state.SettingReducer.streamFont,
    fontSize: state.SettingReducer.fontSize,
    noteVisibility: state.SettingReducer.noteVisibility,
    screenName: state.SettingReducer.screenName,
    flowJourney: state.SettingReducer.flowJourney,
    generalPreferences: state.SettingReducer.generalPreferences,
    accessToken: state.CommonReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateNoteParam: (req) =>
      dispatch(MongoActions.updateNoteParam(req.note_id, req.param, req.cb)),
    unArchiveNote: (req) =>
      dispatch(MongoActions.unArchiveNote(req.note_id, req.cb)),
    archiveNote: (req) => dispatch(MongoActions.archiveNote(req.data, req.cb)),
    createNote: (req) => dispatch(MongoActions.createNote(req.data, req.cb)),
    getToDoNotes: (req) =>
      dispatch(MongoActions.getToDoNotes(req.page, req.cb, req.size)),
    updateTodoSort: (req) =>
      dispatch(MongoActions.updateTodoSort(req.data, req.cb)),
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoScreen);
