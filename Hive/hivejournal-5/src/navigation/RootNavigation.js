import * as React from 'react';
import {StackActions} from '@react-navigation/native';

export const navigationRef = React.createRef();

export function navigate(name, params) {
  console.log(navigationRef);
  navigationRef.current?.navigate(name, params);
}

export function push(...args) {
  navigationRef.current?.dispatch(StackActions.push(...args));
}

export function setParams(params) {
  navigationRef.current?.setParams(params);
}

export function getParams() {
  return navigationRef.current?.params;
}

export function setVisibleFooter(footer) {
  navigationRef.current?.setParams({
    footer,
  });
}

export function getVisibleFooter() {
  const isVisibleFooter =
    navigationRef.current?.getCurrentRoute().params?.footer ?? false;
  return isVisibleFooter;
}
