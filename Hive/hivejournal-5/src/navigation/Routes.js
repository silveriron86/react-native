/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  Image,
  YellowBox,
  View,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

//Tab screens
import FeedScreen from '../screens/FeedScreen';
import HomeScreen from '../screens/HomeScreen';
import NotesScreen from '../screens/Notes';
import StreamScreen from '../screens/Stream';
import SettingsScreen from '../screens/Settings';
import Passcode from '../screens/Settings/Passcode';
import Journeys from '../screens/Settings/Journeys';
import Premium from '../screens/Settings/Premium';
import Personality from '../screens/Settings/Personality';
import Tags from '../screens/Settings/Tags';
import Shortcut from '../screens/Settings/Shortcut';
import EditTagType from '../screens/Settings/Tags/EditTagType';
import EditFlow from '../screens/Settings/Tags/EditFlow';
import RestUp from '../screens/Settings/RestUp';
import AdminSettings from '../screens/Settings/AdminSettings';
import ThreeQ from '../screens/Settings/ThreeQ';
import Visibility from '../screens/Settings/Visibility';
import FontEdit from '../screens/Settings/FontEdit';
import Profile from '../screens/Settings/Profile';
import InputPinCode from '../screens/Settings/InputPinCode';
import ToDoScreen from '../screens/ToDo';
import WalkthroughScreen from '../screens/WalkthroughScreen';
import WalkthroughCopilotScreen from '../screens/WalkthroughCopilotScreen';
import FlowScreen from '../screens/Flow';
import FlowJourney from '../screens/Settings/FlowJourney';
import InitialScreen from '../screens/InitialScreen';
import Colors from '../constants/Colors';
import {navigationRef} from './RootNavigation';
import * as RootNavigation from './RootNavigation';
import Utils from '../utils';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

YellowBox.ignoreWarnings([
  'Warning: isMounted(...) is deprecated',
  'Module RCTImageLoader',
]);
YellowBox.ignoreWarnings(['Class RCTCxxModule']);

const onTabClick = (navigation, route) => {
  console.log(route);
  let routeName = route.name;
  console.log('NEW TAB = ', route.name);
  global.eventEmitter.emit('NEW_TAB', routeName);
  AsyncStorage.setItem('CURRENT_TAB', routeName);

  if (routeName === 'SettingsStack') {
    RootNavigation.setVisibleFooter(true);
    // } else {
    //   navigation.setParams({
    //     footer: false,
    //   });
  }
  // defaultHandler();
};

function HomeStackScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Notes"
        component={NotesScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Stream"
        component={StreamScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

function FeedStackScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Feed"
        component={FeedScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

function SettingsStackScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Settings"
        component={SettingsScreen}
        options={({navigation, route}) => ({
          headerBackTitle: '',
          title: 'Settings',
          headerShown:
            (route.params?.accessToken ?? false) &&
            !(route.params?.locked ?? false),
        })}
      />
      <Stack.Screen
        name="Passcode"
        component={Passcode}
        options={({navigation, route}) => ({
          title: 'Passcode',
          headerShown: !(route.params?.locked ?? false),
        })}
      />
      <Stack.Screen
        name="Journeys"
        component={Journeys}
        options={{
          title: 'Journeys',
          headerStyle: {
            backgroundColor: '#606060',
          },
          headerBackTitle: '',
          headerTintColor: '#a5a0a1',
          headerTitleColor: '#a5a0a1',
        }}
      />
      <Stack.Screen name="Premium" component={Premium} />
      <Stack.Screen
        name="Personality"
        component={Personality}
        options={{title: 'Personality Type', headerBackTitle: ''}}
      />
      <Stack.Screen
        name="Tags"
        component={Tags}
        options={{title: 'Journals', headerBackTitle: ''}}
      />
      <Stack.Screen name="Shortcut" component={Shortcut} />
      <Stack.Screen
        name="TagsWithoutAnim"
        component={Tags}
        options={{title: 'Journals', headerBackTitle: ''}}
      />
      <Stack.Screen
        name="EditTagType"
        component={EditTagType}
        options={{title: 'Edit Journal', headerBackTitle: ''}}
      />
      <Stack.Screen
        name="EditFlow"
        component={EditFlow}
        options={{
          title: 'Edit Flow Tags',
          headerBackTitle: '',
        }}
      />
      <Stack.Screen
        name="RestUp"
        component={RestUp}
        options={{title: 'Rest Up', headerBackTitle: ''}}
      />
      <Stack.Screen
        name="ThreeQ"
        component={ThreeQ}
        options={{title: '3 Questions', headerBackTitle: ''}}
      />
      <Stack.Screen
        name="Visibility"
        component={Visibility}
        options={({navigation, route}) => ({
          title: 'Note Type Visibility',
          headerShown: !(route.params?.locked ?? false),
        })}
      />
      <Stack.Screen
        name="Font"
        component={FontEdit}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={({navigation, route}) => ({
          title: 'Profile',
          headerShown: !(route.params?.locked ?? false),
        })}
      />
      <Stack.Screen
        name="InputPinCode"
        component={InputPinCode}
        options={{title: 'Passcode'}}
      />
      <Stack.Screen
        name="FlowJourney"
        component={FlowJourney}
        options={{title: 'Flow', headerBackTitle: ''}}
      />
      <Stack.Screen name="Admin" component={AdminSettings} />
    </Stack.Navigator>
  );
}
function TabScreen() {
  return (
    <Tab.Navigator
      lazy={false}
      initialRouteName="FeedStack"
      tabBarOptions={{
        showLabel: false, // hide labels
        inactiveTintColor: '#CACACA',
        activeTintColor: Colors.scheme.blue,
        style: {
          backgroundColor: Colors.scheme.black,
          // height: 48,
          borderTopWidth: 0.001,
          borderTopColor: '#CACACA',
          marginBottom: RootNavigation.getVisibleFooter() ? 0 : -100,
        },
        tabStyle: {
          borderRightWidth: 0,
          borderRightColor: '#BDBDBD',
        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let image = null;
          if (route.name === 'HomeStack') {
            image = (
              <Image
                style={{width: 35, height: 35}}
                source={
                  focused
                    ? require('../../assets/icons/home_select.png')
                    : require('../../assets/icons/home_deselect.png')
                }
              />
            );
          } else if (route.name === 'Flow') {
            image = (
              <Image
                style={[styles.flowIcon]}
                source={
                  focused
                    ? require('../../assets/icons/flow-select-v1.png')
                    : require('../../assets/icons/flow-deselect-v1.png')
                }
              />
            );
          } else if (route.name === 'FeedStack') {
            image = (
              <Image
                style={{width: 35, height: 35, resizeMode: 'contain'}}
                source={
                  focused
                    ? require('../../assets/icons/feed_select.png')
                    : require('../../assets/icons/feed_deselect.png')
                }
              />
            );
          } else if (route.name === 'ToDo') {
            image = (
              <Image
                style={{width: 33, height: 33}}
                source={require('../../assets/icons/todays-focus-icon.png')}
              />
            );
          } else if (route.name === 'SettingsStack') {
            image = (
              <Image
                style={{width: 25, height: 25}}
                source={
                  focused
                    ? require('../../assets/icons/menu_select.png')
                    : require('../../assets/icons/menu_deselect.png')
                }
              />
            );
          }

          return (
            <View
              style={[
                {borderWidth: focused ? 2 : 0},
                styles.footerIconWrapper,
              ]}>
              {image}
            </View>
          );
        },
      })}>
      <Tab.Screen
        name="HomeStack"
        component={HomeStackScreen}
        listeners={({navigation, route}) => ({
          tabPress: (e) => {
            onTabClick(navigation, route);
          },
        })}
        options={{tabBarVisible: RootNavigation.getVisibleFooter()}}
      />
      <Tab.Screen
        name="Flow"
        component={FlowScreen}
        listeners={({navigation, route}) => ({
          tabPress: (e) => {
            onTabClick(navigation, route);
          },
        })}
        options={{tabBarVisible: RootNavigation.getVisibleFooter()}}
      />
      <Tab.Screen
        name="FeedStack"
        component={FeedStackScreen}
        listeners={({navigation, route}) => ({
          tabPress: (e) => {
            onTabClick(navigation, route);
          },
        })}
        options={{tabBarVisible: RootNavigation.getVisibleFooter()}}
      />
      <Tab.Screen
        name="ToDo"
        component={ToDoScreen}
        listeners={({navigation, route}) => ({
          tabPress: (e) => {
            onTabClick(navigation, route);
          },
        })}
        options={{tabBarVisible: RootNavigation.getVisibleFooter()}}
      />
      <Tab.Screen
        options={{tabBarVisible: RootNavigation.getVisibleFooter()}}
        listeners={({navigation, route}) => ({
          tabPress: (e) => {
            onTabClick(navigation, route);
          },
        })}
        name="SettingsStack"
        component={SettingsStackScreen}
      />
    </Tab.Navigator>
  );
}

export default function Routes() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator>
        <Stack.Screen
          name="TabScreen"
          component={TabScreen}
          options={{headerShown: false, animationEnabled: false}}
        />
        <Stack.Screen name="FontEdit" component={FontEdit} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  flowIconWrapper: {
    borderRadius: 20,
    width: 100,
    height: 100,
    borderWidth: 2,
    borderColor: '#4b5962',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flowIcon: {
    width: 45,
    height: 45,
    resizeMode: 'contain',
  },
  footerIconWrapper: {
    width: 45,
    height: 45,
    borderRadius: 45,
    // borderRadius: 20,
    // width: 100,
    // height: 100,
    // borderWidth: 2,
    borderColor: '#4b5962',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
