/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable no-bitwise */
import DeviceInfo from 'react-native-device-info';
import CryptoJS from 'crypto-js';
import moment from 'moment-timezone';
import {Dimensions} from 'react-native';
// import Spotify from 'rn-spotify-sdk';
import SettingConstants from '../constants/SettingConstants';
import AsyncStorage from '@react-native-community/async-storage';
import {showMessage} from 'react-native-flash-message';
import {GoogleAnalyticsTracker} from 'react-native-google-analytics-bridge';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';

const chars =
  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
const dt_format = 'YYYY-MM-DD HH:mm ZZ';

let tracker = new GoogleAnalyticsTracker('UA-151682243-1');
let Utils = {
  encrypt: (str) => {
    let cipherText = CryptoJS.AES.encrypt(
      str,
      SettingConstants.SECRET_KEY,
    ).toString();
    return cipherText;
  },
  decrypt: (str) => {
    let bytes = CryptoJS.AES.decrypt(str, SettingConstants.SECRET_KEY);
    let originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
  },
  btoa: (input) => {
    let str = input;
    let output = '';

    for (
      let block = 0, charCode, i = 0, map = chars;
      str.charAt(i | 0) || ((map = '='), i % 1);
      output += map.charAt(63 & (block >> (8 - (i % 1) * 8)))
    ) {
      charCode = str.charCodeAt((i += 3 / 4));

      if (charCode > 0xff) {
        throw new Error(
          "'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.",
        );
      }

      block = (block << 8) | charCode;
    }

    return output;
  },
  shuffle: (len) => {
    let o = Utils.arrayRange(0, len);
    for (
      let j, x, i = o.length;
      i;
      j = parseInt(Math.random() * i, 10), x = o[--i], o[i] = o[j], o[j] = x
    ) {}
    return o;
  },
  arrayRange: (start, len) => {
    var arr = new Array(len);
    for (var i = 0; i < len; i++, start++) {
      arr[i] = start;
    }
    return arr;
  },
  nowUTC: (props) => {
    const {overrideSystemTime, tempSystemDateTime} = props;
    if (overrideSystemTime === true) {
      return tempSystemDateTime;
    }

    return moment().utc().format(dt_format);
  },
  toUTC: (dt) => {
    // convert date & time (dt) to UTC
    dt = moment(dt, dt_format);
    return moment.utc(dt).format(dt_format);
  },
  nowTimezone: (props) => {
    const {location, overrideSystemTime, tempSystemDateTime} = props;
    if (overrideSystemTime === true) {
      return Utils.toTimezone(tempSystemDateTime, location);
    }
    return moment().tz(location).format(dt_format);
  },
  toTimezone: (dt, tz) => {
    // convert the date & time(dt) to the timezone(tz)
    return moment(dt, dt_format).tz(tz).format(dt_format);
  },
  nowMoment: (props) => {
    const {location, overrideSystemTime, tempSystemDateTime} = props;
    let now = moment().tz(location);
    if (overrideSystemTime) {
      now = moment(tempSystemDateTime, dt_format);
    }
    return now;
  },
  format: (dt) => {
    return dt.format(dt_format);
  },
  capitalize: (str) => {
    if (!str) {
      return str;
    }
    return str.charAt(0).toUpperCase() + str.slice(1);
  },
  getTags: (text) => {
    // eslint-disable-next-line prettier/prettier
    let exp = new RegExp('#[0-9a-z][-s0-9a-z]+s?', 'gi');
    return text.match(exp);
  },
  getWords: (text) => {
    let new_words = text.split(' ');
    return new_words.length;
  },
  async initSpotify(cb) {
    // let session = Spotify.getSession();
    // if(session) {
    // 	cb();
    // }else {
    // 	// initialize Spotify if it hasn't been initialized yet
    // 	if(!await Spotify.isInitializedAsync()) {
    // 		console.log('initialize spotify');
    // 		// initialize spotify
    // 		const spotifyOptions = {
    // 			"clientID": ApiConstants.getSpotifyApi().client_id,
    // 			"sessionUserDefaultsKey":"SpotifySession",
    // 			"redirectURL":"hivejournal://auth",
    // 			"scopes":["user-read-private", "playlist-read", "playlist-read-private", "streaming", "user-read-recently-played"],
    // 		};
    // 		const loggedIn = await Spotify.initialize(spotifyOptions);
    // 		console.log('loggedIn = ', loggedIn);
    // 		// update UI state
    // 		cb()
    // 		// handle initialization
    // 		if(loggedIn) {
    // 			// this.goToPlayer();
    // 		}
    // 	}
    // 	else {
    // 		console.log('logged in');
    // 		// update UI state
    // 		cb()
    // 	}
    // }
  },
  moveInArray: (arr, oldIndex, newIndex) => {
    if (newIndex >= arr.length) {
      var k = newIndex - arr.length + 1;
      while (k--) {
        arr.push(undefined);
      }
    }
    arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
    return arr.slice(0);
  },
  getRandomInt: (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },
  getTimes: () => {
    var x = 15; //minutes interval
    var times = []; // time array
    var tt = 0; // start time
    var ap = ['am', 'pm']; // AM-PM

    //loop to increment the time and push results in array
    for (var i = 0; tt < 24 * 60; i++) {
      var hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
      var mm = tt % 60; // getting minutes of the hour in 0-55 format
      var hh_str = ('0' + (hh % 12)).slice(-2);
      var mm_str = ('0' + mm).slice(-2);
      times[i] = {
        label:
          (hh_str === '00' ? '12' : hh_str) +
          ':' +
          mm_str +
          ap[Math.floor(hh / 12)], // pushing data in array in [00:00 - 12:00 AM/PM format]
        value: ('0' + hh).slice(-2) + ':' + mm_str,
      };
      tt = tt + x;
    }

    let times1 = times.slice();
    let times2 = times.slice();
    times = times2.splice(48).concat(times1.splice(0, 48));
    return times;
  },
  getIdealBedTime: (wakeTime, sleepAmount) => {
    let formattedWaked = moment(`${moment().format('YYYY-MM-DD')} ${wakeTime}`);
    let bedTime = formattedWaked
      .subtract(parseInt(sleepAmount, 10), 'hours')
      .format('HH:mm');
    return bedTime;
  },
  getMoonPhase: (year, month, day) => {
    var c = 0;
    var e = 0;
    var jd = 0;
    var b = 0;
    if (month < 3) {
      year--;
      month += 12;
    }

    ++month;
    c = 365.25 * year;
    e = 30.6 * month;
    jd = c + e + day - 694039.09; //jd is total days elapsed
    jd /= 29.5305882; //divide by the moon cycle
    b = parseInt(jd, 10); //int(jd) -> b, take integer part of jd
    jd -= b; //subtract integer part to leave fractional part of original jd
    b = Math.round(jd * 8); //scale fraction from 0-8 and round

    if (b >= 8) {
      b = 0; //0 and 8 are the same so turn 8 into 0
    }

    // 0 => New Moon
    // 1 => Waxing Crescent Moon
    // 2 => Quarter Moon
    // 3 => Waxing Gibbous Moon
    // 4 => Full Moon
    // 5 => Waning Gibbous Moon
    // 6 => Last Quarter Moon
    // 7 => Waning Crescent Moon

    return b;
  },
  getDiffHours: (startTime, endTime) => {
    return (
      moment(endTime, 'HH:mm').diff(moment(startTime, 'HH:mm')) / 1000 / 3600
    );
  },
  nativeBaseDoesHeaderCorrectlyAlready: () => {
    if (Utils.getInsetBottom() > 0) {
      return true;
    }
    return false;
  },
  saveFailedQueue: (service, note) => {
    showMessage({
      message: 'Saved it!',
      description: `We’ll sync that note when all of our network resources are online (${service} and possibly others aren’t reachable right now).`,
      type: 'success',
      duration: 3000,
    });
    if (note) {
      AsyncStorage.getItem('USER_ID', (_err, user_id) => {
        note.failed = true;
        note.user_id = user_id;

        AsyncStorage.getItem('FAILED_QUEUE', (__err, queue) => {
          let updated = [];
          if (queue) {
            updated = JSON.parse(queue);
          }

          updated.push({
            _id: Date.now().toString(),
            note: note,
          });
          // AsyncStorage.setItem('FAILED_QUEUE', JSON.stringify(updated))
          global.eventEmitter.emit('FAILED_QUEUE', updated);
        });
      });
    }
  },
  getFlowPastHours: (timeSinceLastFlowEntry) => {
    if (timeSinceLastFlowEntry === null) {
      return 0;
    }
    return Math.abs(
      moment.utc().diff(moment(timeSinceLastFlowEntry, dt_format)) /
        1000 /
        3600,
    );
  },
  getFlowBorder: (flowJourney) => {
    const MAX_BORDER = 5;
    let border = 0;

    if (/*flowJourney.enabled*/ false && flowJourney.timeSinceLastFlowEntry) {
      let pastHours = Utils.getFlowPastHours(
        flowJourney.timeSinceLastFlowEntry,
      );
      pastHours *= flowJourney.cloudsMultiplier;
      if (pastHours >= flowJourney.cloudsStartAfterHours) {
        if (flowJourney.cloudsFullAfterHours < pastHours) {
          border = MAX_BORDER;
        } else {
          border = parseFloat(
            (
              (MAX_BORDER * (flowJourney.cloudsFullAfterHours - pastHours)) /
              (flowJourney.cloudsFullAfterHours -
                flowJourney.cloudsStartAfterHours)
            ).toFixed(1),
          );
        }
      }
      if (border > MAX_BORDER) {
        border = MAX_BORDER;
      }
    }
    return border;
  },
  getFlowCloudsOpacity: (flowJourney) => {
    const MAX_OPACITY = 1;
    let opacity = 0;

    if (/*flowJourney.enabled*/ false && flowJourney.timeSinceLastFlowEntry) {
      let pastHours = Utils.getFlowPastHours(
        flowJourney.timeSinceLastFlowEntry,
      );
      pastHours *= flowJourney.cloudsMultiplier;

      if (pastHours >= flowJourney.cloudsStartAfterHours) {
        if (flowJourney.cloudsFullAfterHours < pastHours) {
          opacity = MAX_OPACITY;
        } else {
          opacity = parseFloat(
            (
              (flowJourney.cloudsFullAfterHours - pastHours) /
              (flowJourney.cloudsFullAfterHours -
                flowJourney.cloudsStartAfterHours)
            ).toFixed(1),
          );
        }
      }
      if (opacity > MAX_OPACITY) {
        opacity = MAX_OPACITY;
      }
    }
    return opacity;
  },

  trackScreenView: (screen) => {
    // tracker.trackScreenView(screen);
    tracker.trackEvent('ScreenView', screen);
  },

  trackEvent: (action, name) => {
    tracker.trackEvent(action, name);
  },

  getClientHeight: () => {
    const bottomPadding = Utils.getInsetBottom();
    const topPadding = Utils.getInsetTop();
    return Dimensions.get('window').height - bottomPadding - topPadding;
  },

  getInsetBottom: () => {
    return StaticSafeAreaInsets.safeAreaInsetsBottom;
  },

  getInsetTop: () => {
    return StaticSafeAreaInsets.safeAreaInsetsTop;
  },
};

export default Utils;
