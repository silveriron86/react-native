import AsyncStorage from '@react-native-community/async-storage';
import MongoConstants from '../constants/MongoConstants';

const initMongoState = {
  mongoUser: null,
  mongoNote: null,
  mongoNotes: [],
  todoNotes: [],
  mongoOtherNotes: [],
  unEncryptedNotes: [],
  otherSingleUserNotes: [],
  failedQueue: [],
  error: null,
};
const initialState = JSON.parse(JSON.stringify(initMongoState));

// AsyncStorage.removeItem('FAILED_QUEUE')
AsyncStorage.getItem('FAILED_QUEUE', (_err, queue) => {
  initialState.failedQueue = JSON.parse(queue);
});

function MongoReducer(state = initialState, action) {
  switch (action.type) {
    case MongoConstants.RESET:
      console.log('**** reset mongo');
      console.log(Object.assign({}, initialState, initMongoState));
      return Object.assign({}, initialState, initMongoState);

    case MongoConstants.SET_FAILED_QUEUE:
      AsyncStorage.setItem('FAILED_QUEUE', JSON.stringify(action.response));
      return Object.assign({}, state, {
        failedQueue: action.response,
        error: null,
      });

    case MongoConstants.CREATE_USER_SUCCESS:
      return Object.assign({}, state, {
        mongoUser: action.response,
        error: null,
      });

    case MongoConstants.CREATE_USER_ERROR:
      return Object.assign({}, state, {
        mongoUser: {},
        error: action.error,
      });

    case MongoConstants.CREATE_NOTE_SUCCESS:
      return Object.assign({}, state, {
        mongoNote: action.response,
        error: null,
      });

    case MongoConstants.CREATE_NOTE_ERROR:
      return Object.assign({}, state, {
        mongoNote: {},
        error: action.error,
      });

    case MongoConstants.GET_NOTES_SUCCESS:
      let mongoNotes =
        state.mongoNotes.length > 0 ? state.mongoNotes.slice(0) : [];
      if (action.page === 1) {
        mongoNotes = action.response;
      } else if (action.response.length > 0) {
        let noMore = false;
        if (mongoNotes.length > 0) {
          if (
            mongoNotes.findIndex(_item => {
              return _item._id === action.response[0]._id;
            }) >= 0
          ) {
            noMore = true;
          }
        }

        if (!noMore) {
          mongoNotes = mongoNotes.concat(action.response);
        }
      }

      return Object.assign({}, state, {
        mongoNotes: mongoNotes,
        error: null,
      });

    case MongoConstants.GET_TODOS_SUCCESS:
      let todoNotes =
        state.todoNotes.length > 0 ? state.todoNotes.slice(0) : [];
      if (action.page === 1) {
        todoNotes = action.response;
      } else if (action.response.length > 0) {
        let noMore = false;
        if (todoNotes.length > 0) {
          if (
            todoNotes.findIndex(_item => {
              return _item._id === action.response[0]._id;
            }) >= 0
          ) {
            noMore = true;
          }
        }

        if (!noMore) {
          todoNotes = todoNotes.concat(action.response);
        }
      }

      return Object.assign({}, state, {
        todoNotes: todoNotes,
        error: null,
      });

    case MongoConstants.GET_NOTES_ERROR:
      return Object.assign({}, state, {
        // mongoNotes: {},
        error: action.error,
      });

    case MongoConstants.GET_NOTES_OTHER_SINGLE_SUCCESS:
      let otherSingleUserNotes =
        state.otherSingleUserNotes.length > 0
          ? state.otherSingleUserNotes.slice(0)
          : [];
      if (action.page === 1) {
        otherSingleUserNotes = action.response;
      } else if (action.response.length > 0) {
        let noMore = false;
        if (otherSingleUserNotes.length > 0) {
          if (
            otherSingleUserNotes.findIndex(_item => {
              return _item._id === action.response[0]._id;
            }) >= 0
          ) {
            noMore = true;
          }
        }

        if (!noMore) {
          otherSingleUserNotes = otherSingleUserNotes.concat(action.response);
        }
      }

      return Object.assign({}, state, {
        otherSingleUserNotes,
        error: null,
      });

    case MongoConstants.GET_EXCEPT_NOTES_SUCCESS:
      let mongoOtherNotes =
        state.mongoOtherNotes.length > 0 ? state.mongoOtherNotes.slice(0) : [];
      if (action.page === 1) {
        mongoOtherNotes = action.response;
      } else if (action.response.length > 0) {
        let noMore = false;
        if (mongoOtherNotes.length > 0) {
          if (
            mongoOtherNotes.findIndex(_item => {
              return _item._id === action.response[0]._id;
            }) >= 0
          ) {
            noMore = true;
          }
        }

        if (!noMore) {
          mongoOtherNotes = mongoOtherNotes.concat(action.response);
        }
      }

      return Object.assign({}, state, {
        mongoOtherNotes,
        error: null,
      });

    case MongoConstants.UPDATE_NOTE_IMAGE_SUCCESS:
      mongoNotes = state.mongoNotes.slice(0);
      mongoNotes.forEach((item, i) => {
        if (item._id === action.note_id) {
          item.imageURL = action.image_url;
        }
      });
      return Object.assign({}, state, {
        mongoNotes: mongoNotes,
        error: null,
      });

    case MongoConstants.UPDATE_NOTE_PERMISSIONS_SUCCESS:
      mongoNotes = state.mongoNotes.slice(0);
      mongoNotes.forEach((item, i) => {
        if (item._id === action.id) {
          item.note.permissions = action.permissions;
        }
      });
      return Object.assign({}, state, {
        mongoNotes: mongoNotes,
        error: null,
      });

    case MongoConstants.GET_UNENCRYPTED_NOTES_SUCCESS:
      return Object.assign({}, state, {
        unEncryptedNotes: action.response,
        error: null,
      });

    case MongoConstants.GET_EUNENCRYPTED_NOTES_ERROR:
      return Object.assign({}, state, {
        unEncryptedNotes: [],
        error: action.error,
      });

    case MongoConstants.UPDATE_NOTE_VIDEO_SUCCESS:
      mongoNotes = state.mongoNotes.slice(0);
      mongoNotes.forEach((item, i) => {
        if (item._id === action.id) {
          item.note.videoURL = action.videoURL;
        }
      });
      return Object.assign({}, state, {
        mongoNotes: mongoNotes,
        error: null,
      });

    case MongoConstants.UPDATE_NOTE_PARAM_SUCCESS:
      mongoNotes = state.mongoNotes.slice(0);
      // mongoNotes.forEach((item, i) => {
      //   if (item._id === action.id) {
      //     item.note.videoURL = action.videoURL;
      //   }
      // });
      return Object.assign({}, state, {
        mongoNotes: mongoNotes,
        error: null,
      });

    default:
      return state;
  }
}

export default MongoReducer;
