import AsyncStorage from '@react-native-community/async-storage';
import CommonConstants from '../constants/CommonConstants';

const initCommonState = {
  accessToken: 'none',
  userData: null,
  lastNote: null,
};
const initialState = JSON.parse(JSON.stringify(initCommonState));

// AsyncStorage.getItem('ACCESS_TOKEN', (_err, token) => {
//   console.log('*** InitialState accessToken', token);
//   initialState.accessToken = token ? token : null;
// });

// AsyncStorage.getItem('USER', (_err, data) => {
//   if (data) {
//     initialState.userData = JSON.parse(data);
//   }
// });

function CommonReducer(state = initialState, action) {
  switch (action.type) {
    case CommonConstants.RESET:
      console.log('**** reset common');
      console.log(
        Object.assign({}, initialState, {
          userData: null,
          lastData: null,
          accessToken: null,
        }),
      );

      return Object.assign({}, initialState, {
        userData: null,
        lastData: null,
        accessToken: null,
      });

    case CommonConstants.SET_ACCESS_TOKEN:
      return Object.assign({}, state, {
        accessToken: action.response,
      });

    case CommonConstants.SET_USER:
      AsyncStorage.setItem('USER', JSON.stringify(action.response));
      return Object.assign({}, state, {
        userData: action.response,
      });

    case CommonConstants.SET_LAST_NOTE:
      return Object.assign({}, state, {
        lastNote: {
          id: action.id,
          note: action.response,
        },
      });

    default:
      return state;
  }
}

export default CommonReducer;
