import {combineReducers} from 'redux';
import CommonReducer from './CommonReducer';
import MongoReducer from './MongoReducer';
import SettingReducer from './SettingReducer';

export default combineReducers({
  CommonReducer,
  MongoReducer,
  SettingReducer,
});
