import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';
import axios from 'axios';
import ScreenNameConstants from '../constants/ScreenNameConstants';
import SettingConstants from '../constants/SettingConstants';
import ApiConstants from '../constants/ApiConstants';
import Utils from '../utils';
import Tags from '../constants/Tags';

// console.log(DeviceInfo.getTimezone())
const deviceTimeZone = RNLocalize.getTimeZone();
const initialState = {
  screenName: _getScreenName(),
  spotifyEnabled: false,
  toneEnabled: true,
  personalityEnabled: true,
  streamFont: 'PremiumUltra5',
  tonePack: 'animals-1',
  location: deviceTimeZone, // user timezone
  fontSize: 24,
  locations: [],
  timezone: deviceTimeZone, // admin timezone
  snoozeTime: SettingConstants.SNOOZE_TIME, // 15 mins
  cloudsStartTime: SettingConstants.CLOUDS_START_TIME,
  cloudsMultiplier: SettingConstants.CLOUDS_MULTIPLIER,
  q3SavePeriod: SettingConstants.Q3_SAVE_PERIOD,
  overrideSystemTime: false,
  tempSystemDateTime: null,
  lastSavedTime: null,
  threeQOverridesEnabled: false,
  threeQJourneyEnabled: false, //true,
  guide: 'jimmy',
  hope: 7,
  energy: 7,
  flowLabels: {
    hope: 'satisfaction',
    energy: 'energy',
  },
  flowDescriptionPack: 0,
  restUpJourney: {
    enabled: false,
    checkInAt: 9, // 9AM
    guide: 'jimmy',
    lastSavedTime: null,
    sleepAmount: 8,
    wakeTime: '06:00',
  },
  flowJourney: {
    enabled: false, //true,
    override: false,
    cloudsMultiplier: 1,
    timeSinceLastFlowEntry: null,
    cloudsStartAfterHours:
      SettingConstants.defaultFlowJourney.cloudsStartAfterHours,
    cloudsFullAfterHours:
      SettingConstants.defaultFlowJourney.cloudsFullAfterHours,
  },
  passcode: {
    enabled: false,
    number: '',
    after: 10,
    locked: false,
    journalLocked: false,
    // notesLock: false,
    flowLock: false,
  },
  noteVisibility: {
    standard: ['any', 'invites'],
    '3Q': ['any', 'any'],
    rating: ['team', 'me'],
    sleep: ['me', 'guides-mine'],
  },
  generalPreferences: {
    convertTagEnabled: false,
    autoSaveEnabled: false,
    showBackgroundImg: true,
  },
  depthAndSize: SettingConstants.defaultDepthSize,
  doorway: 0,
  fcmToken: '',
  tags: Tags,
  selectedFont: 'none',
  user: {
    birthdate: '',
    over13: false,
    phone: '',
    verifyMe: false,
    verifyTokenSent: false,
    validatedPhone: true,
    acceptedTermsSMS: false,
  },
  profiles: {
    type: [
      {
        mbti: [],
      },
    ],
  },
  qtyTotalUniqueDaysAppWasOpened: 0,
  appLastNoteSavedDate: null,
  ratings: {
    ios: {
      lastRequestDate: null,
    },
  },
};

function SettingReducer(state = initialState, action) {
  let res = null;
  switch (action.type) {
    case SettingConstants.SET_RESET:
      console.log('SET_RESET', action.response);
      AsyncStorage.clear();
      return Object.assign({}, initialState, {});

    case SettingConstants.SET_RESTORE:
      console.log('SET_RESPONSE', action.response);
      let restored = Object.assign({}, initialState, action.response);
      if (typeof action.response.restUpJourney.sleepAmount === 'undefined') {
        restored.restUpJourney['sleepAmount'] = 8;
        restored.restUpJourney['wakeTime'] = '06:00';
      }
      return restored;

    case SettingConstants.SET_SPOTIFY_ENABLED:
      res = Object.assign({}, state, {
        spotifyEnabled: action.response,
      });
      break;

    case SettingConstants.SET_TONE_ENABLED:
      res = Object.assign({}, state, {
        toneEnabled: action.response,
      });
      break;

    case SettingConstants.SET_PERSONALITY_ENABLED:
      res = Object.assign({}, state, {
        personalityEnabled: action.response,
      });
      break;

    case SettingConstants.SET_STREAM_FONT:
      res = Object.assign({}, state, {
        streamFont: action.response,
      });
      break;

    case SettingConstants.SET_LOCATION:
      res = Object.assign({}, state, {
        location: action.response,
      });
      break;

    case SettingConstants.SET_TONE_PACK:
      res = Object.assign({}, state, {
        tonePack: action.response,
      });
      break;

    case SettingConstants.SET_FONT_SIZE:
      res = Object.assign({}, state, {
        fontSize: action.response,
      });
      break;

    case SettingConstants.SET_TIMEZONE:
      res = Object.assign({}, state, {
        timezone: action.response,
      });
      break;

    case SettingConstants.SET_Q3_SAVE_PERIOD:
      res = Object.assign({}, state, {
        q3SavePeriod: action.response,
      });
      break;

    case SettingConstants.SET_SNOOZE_TIME:
      res = Object.assign({}, state, {
        snoozeTime: action.response,
      });
      break;

    case SettingConstants.SET_CLOUDS_START_TIME:
      res = Object.assign({}, state, {
        cloudsStartTime: action.response.toString(),
      });
      break;

    case SettingConstants.SET_CLOUDS_MULTIPLIER:
      res = Object.assign({}, state, {
        cloudsMultiplier: action.response,
      });
      break;

    case SettingConstants.SAVE_LOCATIONS_SUCCESS:
      let isExist = false;
      let locs = state.locations.slice(0);
      if (locs.length > 0) {
        locs.forEach((loc, i) => {
          if (loc.ip === action.response.ip) {
            isExist = true;
          }
        });
      }

      if (!isExist) {
        locs = locs.concat(action.response);
      }
      let last_timezone = locs[locs.length - 1].time_zone.id;

      let obj = {
        locations: locs.splice(0),
      };
      if (state.location === deviceTimeZone) {
        obj['location'] = last_timezone;
      }

      res = Object.assign({}, state, obj);
      break;

    case SettingConstants.SET_OVERRIDE_SYSTEM_TIME:
      res = Object.assign({}, state, {
        overrideSystemTime: action.response,
      });
      break;

    case SettingConstants.SET_TEMP_SYSTEM_DATETIME:
      res = Object.assign({}, state, {
        tempSystemDateTime: action.response,
      });
      break;

    case SettingConstants.SET_THREE_THINGS_SAVED:
      res = Object.assign({}, state, {
        lastSavedTime: action.response,
      });
      break;

    case SettingConstants.SET_3Q_JOURNEY_ENABLED:
      res = Object.assign({}, state, {
        threeQJourneyEnabled: action.response,
      });
      break;

    case SettingConstants.SET_3Q_OVERRIDES_ENABLED:
      res = Object.assign({}, state, {
        threeQOverridesEnabled: action.response,
      });
      break;

    case SettingConstants.SET_GUIDE:
      res = Object.assign({}, state, {
        guide: action.response,
      });
      break;

    case SettingConstants.SET_HOPE:
      res = Object.assign({}, state, {
        hope: action.response,
      });
      break;

    case SettingConstants.SET_ENERGY:
      res = Object.assign({}, state, {
        energy: action.response,
      });
      break;

    case SettingConstants.SET_FLOW_LABELS:
      res = Object.assign({}, state, {
        flowLabels: action.response,
      });
      break;

    case SettingConstants.SET_FLOW_DESCRIPTION_PACK:
      res = Object.assign({}, state, {
        flowDescriptionPack: action.response,
      });
      break;

    case SettingConstants.SET_REST_UP_JOURNEY:
      res = Object.assign({}, state, {
        restUpJourney: action.response,
      });
      break;

    case SettingConstants.SET_FLOW_JOURNEY:
      res = Object.assign({}, state, {
        flowJourney: action.response,
      });
      break;

    case SettingConstants.SET_PASSCODE:
      res = Object.assign({}, state, {
        passcode: action.response,
      });
      break;

    case SettingConstants.SET_NOTE_VISIBILITY:
      res = Object.assign({}, state, {
        noteVisibility: action.response,
      });
      break;

    case SettingConstants.SET_GENERAL_PREFERENCES:
      res = Object.assign({}, state, {
        generalPreferences: action.response,
      });
      break;

    case SettingConstants.SET_DEPTH_SIZE:
      res = Object.assign({}, state, {
        depthAndSize: action.response,
      });
      break;
    case SettingConstants.SET_SCREEN_NAME:
      res = Object.assign({}, state, {
        screenName: action.response,
      });
      break;
    case SettingConstants.SET_DOORWAY:
      res = Object.assign({}, state, {
        doorway: action.response,
      });
      break;
    case SettingConstants.SET_FCM_TOKEN:
      res = Object.assign({}, state, {
        fcmToken: action.response,
      });
      break;
    case SettingConstants.SET_TAGS:
      res = Object.assign({}, state, {
        tags: action.response,
      });
      break;
    case SettingConstants.SET_SELECTED_FONT:
      res = Object.assign({}, state, {
        selectedFont: action.response,
      });
      break;
    case SettingConstants.SET_USER:
      res = Object.assign({}, state, {
        user: action.response,
      });
      break;
    case SettingConstants.SET_PROFILES:
      res = Object.assign({}, state, {
        profiles: action.response,
      });
      break;

    case SettingConstants.SET_QTY_TOTAL_UNIQUE_DAYS:
      res = Object.assign({}, state, {
        qtyTotalUniqueDaysAppWasOpened: action.response,
      });
      break;

    case SettingConstants.SET_LAST_NOTE_SAVED_DATE:
      res = Object.assign({}, state, {
        appLastNoteSavedDate: action.response !== null ? action.response : null,
      });
      break;

    case SettingConstants.SET_RATINGS:
      res = Object.assign({}, state, {
        ratings: action.response,
      });
      break;

    default:
      return state;
  }

  console.log('* SAVED selectedFont', action.type, res.selectedFont);
  AsyncStorage.setItem('USER_SETTINGS', JSON.stringify(res));
  updateSettings(res);
  return res;
}

function updateSettings(data) {
  // this will be removed when launch the app
  if (typeof data.tags[0].noteLocked === 'undefined') {
    data.tags.forEach((t) => {
      t.locked = t.type === 'todo' ? false : true;
      t.noteLocked = false;
    });
  }
  data.tags.forEach((t) => {
    if (typeof t.background === 'number') {
      t.background = [t.background];
    }
  });

  if (typeof data.qtyTotalUniqueDaysAppWasOpened === 'undefined') {
    data.qtyTotalUniqueDaysAppWasOpened = 0;
    data.appLastNoteSavedDate = null;
    data.ratings = {
      ios: {
        lastRequestDate: null,
      },
    };
  }

  AsyncStorage.getItem('DB_USER_ID', (_err, user_id) => {
    if (_err) {
      return;
    }

    if (user_id) {
      let api = ApiConstants.getMongoApi();
      let url =
        api.url +
        'service-http-users-settings-update-v1/incoming_webhook/webhook-http-users-settings-update-v1?secret=' +
        api.secret;
      url += '&user_id=' + user_id;
      axios({
        method: 'POST',
        url: url,
        headers: {
          'Content-Type': 'application/json',
        },
        data: data,
      }).catch((error) => {
        console.log(error.response);
      });
    }
  });
}

function _getScreenName() {
  let adjective = Utils.capitalize(
    ScreenNameConstants.adjectives[
      Utils.getRandomInt(0, ScreenNameConstants.adjectives.length - 1)
    ],
  );
  let color =
    ScreenNameConstants.colors[
      Utils.getRandomInt(0, ScreenNameConstants.colors.length - 1)
    ];
  let animal =
    ScreenNameConstants.animals[
      Utils.getRandomInt(0, ScreenNameConstants.animals.length - 1)
    ];
  return `${adjective} ${color} ${animal}`;
}

export default SettingReducer;
