import React from 'react';
import {StyleSheet, View, StatusBar, SafeAreaView} from 'react-native';
import Colors from '../constants/Colors';
import GifLoader from '../widgets/GifLoader';

export default class EmptyView extends React.Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="blue" barStyle="light-content" />
        <View style={styles.screen}>
          <GifLoader />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.scheme.gray,
  },
  screen: {
    flex: 1,
    backgroundColor: Colors.scheme.gray,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
