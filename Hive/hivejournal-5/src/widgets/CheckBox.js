import React from 'react';
import {StyleSheet, View, Image, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const boxImg = require('../../assets/icons/checkbox_white.png');
const checkImg = require('../../assets/icons/checkmark_white.png');

export default class Checkbox extends React.Component {
  render() {
    const {checked, type, onToggle} = this.props;

    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.checkBtn} onPress={onToggle}>
          <Image source={boxImg} />
          {checked && <Image source={checkImg} style={styles.checkImg} />}
        </TouchableOpacity>
        <Text style={styles.text}>{`Save with last ${type} entry`}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
  },
  text: {
    color: 'white',
    fontFamily: 'PremiumUltra5',
    fontSize: 17,
    fontWeight: '900',
    marginTop: 10,
  },
  checkBtn: {
    width: 50,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  checkImg: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
});
