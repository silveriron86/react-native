/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  Animated,
  Image,
  TouchableOpacity,
  View,
  ImageBackground,
  Text,
  Dimensions,
  Keyboard,
} from 'react-native';
import Utils from '../utils';
import Colors from '../constants/Colors';
import SearchModal from './SearchModal';
import {CommonActions} from '@react-navigation/native';
import * as RootNavigation from '../navigation/RootNavigation';

const toggleTagsIcon = require('../../assets/icons/blue_arrow_hive_down.png');
const backgroundAnim = require('../../assets/icons/triangle-pulsing-2.gif');
const featherIconBlue = require('../../assets/icons/icon-compose-blue-1.png');
const featherIconGray = require('../../assets/icons/icon-compose-gray-1.png');
const todayFocusIcon = require('../../assets/icons/todays-focus-icon.png');
const bookOpenIconBlue = require('../../assets/icons/icon-read-blue-1.png');
const bookOpenIconGray = require('../../assets/icons/icon-read-gray-1.png');
const searchIcon = require('../../assets/icons/search.png');

export default class FooterTrigger extends React.Component {
  constructor(props) {
    super(props);
    this.rotateValue = new Animated.Value(1);
    this.state = {
      journal: props.defaultJournal,
      isOpenedSelector: false,
      searchModalVisible: false,
      border: 0,
    };
  }

  componentDidMount() {
    this.mount = true;
    global.eventEmitter.addListener('NEW_TAB', () => {
      RootNavigation.setVisibleFooter(false);

      this.rotateValue = new Animated.Value(1);
      Animated.timing(this.rotateValue, {
        toValue: 1,
        duration: 1,
        useNativeDriver: true,
      }).start();
    });

    global.eventEmitter.addListener('CHANGED_COVER', (data) => {
      if (this.mount) {
        this.setState({
          journal: data.type,
        });
      }
    });

    global.eventEmitter.addListener('TOGGLE_JOURNAL_SELECTOR', (data) => {
      if (this.mount) {
        this.setState({
          isOpenedSelector: data.opened,
        });
      }
    });

    setInterval(() => {
      this._setFlowBorder();
    }, 1000 * 60 * 10);

    setTimeout(() => {
      this._setFlowBorder();
    }, 1000);
  }

  componentWillUnmount() {
    this.mount = false;
  }

  onToggleSearchModal = () => {
    this.setState({
      searchModalVisible: !this.state.searchModalVisible,
    });
  };

  onSearch = (param) => {
    console.log(param);
    global.eventEmitter.emit('CHANGED_COVER', {
      type: param.journal,
      index: param.selectedIndex,
      searchText: param.searchText,
      searchTags: param.tags,
    });
    Keyboard.dismiss();
    this.onToggleSearchModal();
  };

  _setFlowBorder = () => {
    if (!this.mount) {
      return;
    }
    this.setState({
      border: Utils.getFlowBorder(this.props.flowJourney),
    });
  };

  toggleFooter = () => {
    console.log(this.props);
    let currentVal = RootNavigation.getVisibleFooter();
    Animated.timing(this.rotateValue, {
      toValue: currentVal ? 1 : 0,
      duration: 300,
      useNativeDriver: true,
    }).start();

    RootNavigation.setVisibleFooter(!currentVal);
  };

  onShowAll = () => {
    if (!this.mount) {
      return;
    }
    global.eventEmitter.emit('DESELECT_JOURNEY', {});
    this.setState({
      journal: 'All',
    });
  };

  onModeButtonTap = (mode) => {
    if (mode === 3) {
      // visit todo list
      this.props.onVisitTodo();
    } else {
      this.props.onSelectMode(mode);
    }
    this.toggleFooter();
  };

  render() {
    const {
      screen,
      isOpenedOwnNote,
      mode,
      editMode,
      tags,
      // currentNote,
      // onSelectMode,
      searchText,
      searchTags,
    } = this.props;
    const {journal, isOpenedSelector, border, searchModalVisible} = this.state;
    const spin = this.rotateValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
    });
    let currentVal = RootNavigation.getVisibleFooter();
    console.log(currentVal);
    let isTodoFocus = screen === 'todo';
    // screen === 'stream' &&
    // currentNote &&
    // currentNote.note.main_tag === 'todo';

    return (
      // <View>
      <>
        {!isTodoFocus && screen !== 'settings' && (
          <View
            style={{
              zIndex: 9999,
              backfaceVisibility: 'hidden',
              flexDirection: 'row',
              alignItems: 'center',
              alignContent: 'center',
              justifyContent: 'center',
            }}>
            {/* <Text>hi</Text> */}
            {RootNavigation.getVisibleFooter() && (
              <>
                {/* <View>
                  {mode === 1 && (
                    <TouchableOpacity
                      style={styles.leftIconButtonCompose}
                      onPress={() => this.onModeButtonTap(3)}>
                      <Image
                        source={todayFocusIcon}
                        style={[styles.leftIconBlue, {width: 50, height: 50}]}
                      />
                    </TouchableOpacity>
                  )}
                </View> */}
                <View style={{marginHorizontal: 5}}>
                  <TouchableOpacity
                    style={styles.leftIconButtonCompose}
                    onPress={() => this.onModeButtonTap(2)}>
                    {mode === 1 && (
                      <Image
                        source={featherIconBlue}
                        style={[
                          styles.leftIconBlue,
                          // RootNavigation.getVisibleFooter()
                          //   ? {opacity: 1}
                          //   : {opacity: 0},
                        ]}
                      />
                    )}
                    {mode === 2 && (
                      <Image
                        source={featherIconGray}
                        style={[
                          styles.leftIconGray,
                          // RootNavigation.getVisibleFooter()
                          //   ? {opacity: 1}
                          //   : {opacity: 0},
                        ]}
                      />
                    )}
                  </TouchableOpacity>
                </View>
                <View style={{maxHeight: 50, maxWidth: 50}}>
                  <TouchableOpacity
                    style={[styles.leftIconButtonRead]}
                    onPress={() => this.onModeButtonTap(1)}>
                    {mode === 2 && (
                      <Image
                        source={bookOpenIconBlue}
                        style={[
                          styles.leftIconBlue,
                          // RootNavigation.getVisibleFooter()
                          //   ? {opacity: 1}
                          //   : {opacity: 0},
                        ]}
                      />
                    )}

                    {mode === 1 && (
                      <Image
                        source={bookOpenIconGray}
                        style={[
                          styles.leftIconGray,
                          // RootNavigation.getVisibleFooter()
                          //   ? {opacity: 1}
                          //   : {opacity: 0},
                        ]}
                      />
                    )}
                  </TouchableOpacity>
                </View>
              </>
            )}
          </View>
        )}
        <View style={styles.container}>
          <View style={{width: 100}}>
            {screen === 'stream' && (
              <TouchableOpacity
                style={styles.searchButton}
                onPress={this.onToggleSearchModal}>
                <Image source={searchIcon} style={styles.searchIcon} />
              </TouchableOpacity>
            )}
          </View>
          <View style={{position: 'relative'}}>
            <View style={{overflow: 'hidden'}}>
              {border > 0 ? (
                <ImageBackground
                  style={{width: 40, height: 40}}
                  imageStyle={
                    currentVal === true
                      ? {
                          transform: [{rotate: '250deg'}],
                          marginTop: -4,
                          marginLeft: 5,
                        }
                      : {
                          transform: [{rotate: '70deg'}],
                          marginTop: 4,
                          marginLeft: -4,
                        }
                  }
                  source={backgroundAnim}>
                  <Animated.View style={[{transform: [{rotate: spin}]}]}>
                    <TouchableOpacity
                      style={styles.footerToggleBtn}
                      onPress={this.toggleFooter}>
                      <Image
                        source={toggleTagsIcon}
                        style={styles.toggleIcon}
                      />
                    </TouchableOpacity>
                  </Animated.View>
                </ImageBackground>
              ) : (
                <Animated.View style={[{transform: [{rotate: spin}]}]}>
                  <TouchableOpacity
                    style={styles.footerToggleBtn}
                    onPress={this.toggleFooter}>
                    <Image source={toggleTagsIcon} style={styles.toggleIcon} />
                  </TouchableOpacity>
                </Animated.View>
              )}
            </View>
          </View>
          <View style={{width: 100}}>
            {screen === 'stream' &&
              isOpenedOwnNote &&
              isOpenedSelector &&
              journal !== 'All' && (
                <TouchableOpacity
                  style={styles.showAllButton}
                  onPress={this.onShowAll}>
                  <Text style={styles.showAllText}>Read all</Text>
                </TouchableOpacity>
              )}

            {((screen === 'notes' && currentVal === false) ||
              (screen === 'stream' && isOpenedOwnNote && editMode === false) ||
              screen === 'todo') && (
              <TouchableOpacity
                style={styles.rightButton}
                onPress={this.props.onShowJournalSelector}>
                <Text style={styles.rightButtonText} numberOfLines={1}>
                  {journal === '3questions'
                    ? 'Three Questions'
                    : screen === 'todo'
                    ? 'todo'
                    : journal}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
        {screen === 'stream' && (
          <SearchModal
            visible={searchModalVisible}
            tags={tags}
            defaultJournal={journal}
            defaultText={searchText}
            defaultTags={searchTags}
            onToggle={this.onToggleSearchModal}
            onSearch={this.onSearch}
          />
        )}
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.scheme.black,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    zIndex: 9999,
  },
  footerToggleBtn: {
    width: 40,
    height: 40,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  toggleIcon: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    tintColor: Colors.scheme.blue,
  },
  colorPickBtn: {
    width: 20,
    height: 20,
    margin: 3,
    borderWidth: 1,
  },
  selectedBtn: {
    width: 25,
    height: 25,
  },
  colorPickers: {
    flexDirection: 'row',
    position: 'absolute',
    right: 10,
    alignItems: 'flex-end',
  },
  leftIconButtonRead: {
    // position: 'absolute',
    // left: 80,
    // top: -80,
    width: 50,
    height: 50,
    // paddingTop: 2,
    // // alignItems: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIconButtonCompose: {
    // position: 'absolute',
    // left: 0,
    // top: -80,
    // width: 80,
    // height: 80,
    // paddingTop: 2,
    // // alignItems: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIconBlue: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    opacity: 1,
  },
  leftIconGray: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    opacity: 1,
  },
  rightButton: {
    // position: 'absolute',
    right: 5,
  },
  rightButtonText: {
    fontFamily: 'PremiumUltra26',
    fontSize: 25,
    color: 'white',
    maxWidth: Dimensions.get('window').width / 2 - 30,
    paddingHorizontal: 5,
    textAlign: 'right',
  },
  showAllButton: {
    // position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    // marginTop: -35,
    borderWidth: 1,
    borderColor: '#29abe2',
    borderRadius: 5,
    width: 110,
    height: 30,
    left: 0,
    // marginLeft: -125,
  },
  showAllText: {
    textAlign: 'center',
    fontSize: 17,
    fontFamily: 'PremiumUltra5',
    color: '#29abe2',
  },
  searchButton: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchIcon: {
    width: 35,
    height: 35,
    tintColor: Colors.scheme.blue,
  },
});
