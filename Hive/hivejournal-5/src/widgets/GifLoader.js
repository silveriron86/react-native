import React from 'react';
import {StyleSheet, Image} from 'react-native';
import gifIcon from '../../assets/icons/ezgif.com-gif-maker.gif';

// eslint-disable-next-line no-undef
export default (GifLoader = props => {
  // eslint-disable-next-line react/react-in-jsx-scope
  return <Image source={gifIcon} style={styles.gifIcon} />;
});

const styles = StyleSheet.create({
  gifIcon: {
    width: 250,
    height: 250,
  },
});
