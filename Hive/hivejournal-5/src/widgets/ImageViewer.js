/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, TouchableOpacity, Image, Alert} from 'react-native';
import ImageView from 'react-native-image-view';

const closeIcon = require('../../assets/icons/glyph_close_32x32.png');
const trashIcon = require('../../assets/icons/trash.png');

export default class ImageViewer extends React.Component {
  onDeleteImage = () => {
    Alert.alert(
      'Are you sure to remove this image from note?',
      '',
      [
        {
          text: 'Remove',
          onPress: () => {
            this.props.onToggle();
            this.props.onDelete();
          },
        },
        {
          text: 'Cancel',
          onPress: () => {
            // this.onCloseImage()
          },
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  };

  render() {
    const {url, visible, onToggle} = this.props;
    return (
      <ImageView
        images={[
          {
            source: {
              uri: url,
            },
          },
        ]}
        imageIndex={0}
        isVisible={visible}
        controls={{
          close: () => {
            return (
              <TouchableOpacity onPress={onToggle} style={styles.closeImgBtn}>
                <Image source={closeIcon} />
              </TouchableOpacity>
            );
          },
        }}
        renderFooter={currentImage => (
          <TouchableOpacity
            style={styles.removeImgBtn}
            onPress={this.onDeleteImage}>
            <Image
              source={trashIcon}
              resizeMethod={'resize'}
              style={{
                tintColor: 'white',
                width: 35,
                height: 35,
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
        )}
      />
    );
  }
}

const styles = StyleSheet.create({
  closeImgBtn: {
    marginTop: 25,
    marginRight: 20,
    alignSelf: 'flex-end',
  },
  removeImgBtn: {
    marginBottom: 65,
    marginRight: 20,
    alignSelf: 'flex-end',
  },
});
