/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import {CustomPicker} from 'react-native-custom-picker';

const jimmyIcon = require('../../assets/images/jimmy-hipster_1.png');
const robotIcon = require('../../assets/images/robot.png');
const options = [
  {
    icon: jimmyIcon,
    label: 'Jimmy',
    value: 1,
  },
  {
    icon: robotIcon,
    label: 'Q',
    value: 2,
  },
];

export default class CustomSelector extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedItem: props.value === 'jimmy' ? options[0] : options[1],
    };
  }

  render() {
    const {selectedItem} = this.state;

    return (
      <View
        style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
        <CustomPicker
          value={selectedItem}
          placeholder={'Please select a guide...'}
          options={options}
          getLabel={item => item.label}
          fieldTemplate={this.renderField}
          optionTemplate={this.renderOption}
          headerTemplate={this.renderHeader}
          footerTemplate={this.renderFooter}
          modalStyle={styles.modalWrapper}
          onValueChange={value => {
            this.setState({
              selectedItem: value,
            });
            this.props.onValueChange(value.label.toLowerCase());
          }}
        />
      </View>
    );
  }

  renderHeader() {
    return (
      <View style={styles.headerFooterContainer}>
        <Text>Please select a guide...</Text>
      </View>
    );
  }

  renderFooter(action) {
    return <View style={{height: 15}} />;
  }

  renderField(settings) {
    const {selectedItem, defaultText, getLabel} = settings;
    return (
      <View style={styles.container}>
        <View>
          {!selectedItem && (
            <Text style={[styles.text, {color: 'grey'}]}>{defaultText}</Text>
          )}
          {selectedItem && (
            <View style={[styles.innerContainer, {justifyContent: 'flex-end'}]}>
              {/* <TouchableOpacity style={styles.clearButton} onPress={clear}>
                <Text style={{ color: '#fff' }}>Clear</Text>
              </TouchableOpacity> */}
              <View style={styles.iconWrapper}>
                <Image source={selectedItem.icon} style={styles.icon} />
              </View>
              <Text style={[styles.text, {marginLeft: 5}]}>
                {getLabel(selectedItem)}
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }

  renderOption(settings) {
    const {item, getLabel} = settings;
    return (
      <View style={styles.optionContainer}>
        <View style={styles.innerContainer}>
          <Image source={item.icon} style={styles.icon} />
          <Text style={styles.text}>{getLabel(item)}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 0,
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    marginLeft: 10,
  },
  headerFooterContainer: {
    padding: 10,
    alignItems: 'center',
  },
  clearButton: {
    backgroundColor: 'grey',
    borderRadius: 5,
    marginRight: 10,
    padding: 5,
  },
  optionContainer: {
    padding: 10,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  optionInnerContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  box: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  icon: {
    width: 50,
    height: 45,
    resizeMode: 'contain',
  },
  modalWrapper: {
    borderRadius: 5,
  },
});
