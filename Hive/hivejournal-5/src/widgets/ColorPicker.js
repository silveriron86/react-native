/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import Colors from '../constants/Colors';

export default class ColorPicker extends React.Component {
  render() {
    const {color, canPickBlack, vertical} = this.props;

    return (
      <View
        style={[styles.colorPickers, vertical && {justifyContent: 'flex-end'}]}>
        <TouchableOpacity
          onPress={() => this.props.onPickColor(Colors.scheme.blue)}
          style={[
            styles.colorPickBtn,
            color === Colors.scheme.blue && styles.selectedBtn,
            {backgroundColor: Colors.scheme.blue, borderColor: 'white'},
          ]}
        />
        <TouchableOpacity
          onPress={() => this.props.onPickColor('white')}
          style={[
            styles.colorPickBtn,
            color === 'white' && styles.selectedBtn,
            {backgroundColor: 'white', borderColor: '#ccc'},
          ]}
        />
        {canPickBlack ? (
          <TouchableOpacity
            onPress={() => this.props.onPickColor('black')}
            style={[
              styles.colorPickBtn,
              color === 'black' && styles.selectedBtn,
              {backgroundColor: 'black', borderColor: 'white'},
            ]}
          />
        ) : (
          <TouchableWithoutFeedback
            disabled={true}
            style={[
              styles.colorPickBtn,
              {backgroundColor: 'black', borderColor: 'white'},
            ]}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  colorPickBtn: {
    width: 20,
    height: 20,
    margin: 3,
    borderWidth: 1,
  },
  selectedBtn: {
    width: 25,
    height: 25,
  },
  colorPickers: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
    // backgroundColor: 'rgba(0,0,0,0.5)',
    width: '100%',
  },
});
