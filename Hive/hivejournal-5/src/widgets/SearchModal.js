/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Image,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  StyleSheet,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {Chevron} from 'react-native-shapes';
import ModalWrapper from 'react-native-modal-wrapper';
import notesStyles from '../screens/Notes/_styles';
import FooterTags from '../screens/Notes/FooterTags';
import Colors from '../constants/Colors';
// import Utils from '../utils';

const searchIcon = require('../../assets/icons/search.png');

export default class SearchModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: -1,
      searchText: props.defaultText,
      journal: props.defaultJournal,
      insertedTags: props.defaultTags,
    };
  }

  onInsertTag = tag => {
    let insertedTags = JSON.parse(JSON.stringify(this.state.insertedTags));
    let found = insertedTags.indexOf(tag);
    if (found > -1) {
      insertedTags.splice(found, 1);
    } else {
      insertedTags.push(tag);
    }
    this.setState({
      insertedTags: insertedTags,
    });
  };

  onSearch = () => {
    const {insertedTags, journal, selectedIndex, searchText} = this.state;
    this.props.onSearch({
      journal,
      selectedIndex,
      tags: insertedTags,
      searchText,
    });
  };

  onCancel = () => {
    const {defaultJournal, defaultText, defaultTags} = this.props;
    this.setState({
      journal: defaultJournal,
      searchText: defaultText,
      insertedTags: defaultTags,
    });
    this.props.onToggle();
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.defaultJournal !== this.props.defaultJournal) {
      this.setState({
        journal: nextProps.defaultJournal,
      });
    }
  }

  render() {
    const {visible, tags} = this.props;
    const {insertedTags, journal, selectedIndex, searchText} = this.state;

    let items = [{label: 'All', value: 'All'}];
    tags.forEach(t => {
      const label = t.type === '3questions' ? 'three questions' : t.type;
      items.push({
        label,
        value: t.type,
      });
    });

    return (
      <ModalWrapper
        containerStyle={[
          notesStyles.modalContainer,
          {justifyContent: 'flex-end', marginHorizontal: 0},
        ]}
        style={[
          notesStyles.modal,
          {
            height: '70%',
            borderRadius: 0,
            backgroundColor: 'rgba(0,0,0,0.8)',
          },
        ]}
        onRequestClose={this.props.onToggle}
        shouldAnimateOnRequestClose={true}
        shouldCloseOnOverlayPress={true}
        visible={visible}
        position={'bottom'}>
        <View style={{flex: 1, paddingVertical: 30, paddingHorizontal: 20}}>
          <TextInput
            style={{
              fontFamily: 'PremiumUltra26',
              fontSize: 24,
              color: 'white',
              height: '30%',
              borderWidth: 1,
              borderColor: 'white',
              borderRadius: 10,
              padding: 15,
            }}
            value={searchText}
            onChangeText={v => this.setState({searchText: v})}
            autoCapitalize={'none'}
            enablesReturnKeyAutomatically={true}
            multiline={true}
            placeholder="Type the text to search here..."
            placeholderTextColor="#ddd"
          />
          <View style={{flex: 1, paddingTop: 20}}>
            <View
              style={{
                paddingBottom: 5,
                marginBottom: 10,
                // borderBottomWidth: 1,
                // borderBottomColor: '#aaa',
                flexDirection: 'row',
              }}>
              <Text style={styles.labelText}>Journal: </Text>
              <RNPickerSelect
                placeholder={{}}
                items={items}
                onValueChange={v => {
                  const index = this.props.tags.findIndex(_item => {
                    return _item.type === v;
                  });
                  console.log(v, index);
                  this.setState({
                    selectedIndex: index,
                    journal: v,
                    insertedTags: [],
                  });
                }}
                style={selectStyle}
                value={journal}
                useNativeAndroidPickerStyle={false}
                textInputProps={selectTextInputStyle}
                Icon={() => {
                  return <Chevron size={1.5} color="gray" />;
                }}
              />
            </View>
            <View style={{flex: 1}}>
              <FooterTags
                fontSize={20}
                selectedIndex={selectedIndex}
                insertedTags={insertedTags}
                allTags={this.props.tags ? this.props.tags : []}
                newTags={[]}
                note={''}
                insertTag={this.onInsertTag}
                isComposed={true}
                isFullHeight={true}
              />
            </View>
          </View>
          <View
            style={{
              width: '100%',
              marginTop: 15,
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <TouchableOpacity style={styles.goBtn} onPress={this.onSearch}>
              <Image source={searchIcon} style={styles.searchIcon} />
              <Text style={styles.goBtnText}>Search</Text>
            </TouchableOpacity>
            <View style={{width: 10}} />
            <TouchableOpacity
              style={[styles.goBtn, {borderColor: '#aaa'}]}
              onPress={this.onCancel}>
              <Text style={[styles.goBtnText, {color: '#aaa'}]}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ModalWrapper>
    );
  }
}

const styles = StyleSheet.create({
  searchIcon: {
    width: 25,
    height: 25,
    marginRight: 5,
    tintColor: Colors.scheme.blue,
  },
  goBtn: {
    borderColor: '#29abe2',
    borderWidth: 1,
    borderRadius: 20,
    paddingVertical: 0,
    width: 120,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  goBtnText: {
    fontFamily: 'PremiumUltra26',
    color: Colors.scheme.blue,
    fontSize: 20,
    marginTop: 6,
  },
  labelText: {
    fontFamily: 'PremiumUltra26',
    color: Colors.scheme.blue,
    fontSize: 22,
    marginRight: 10,
  },
});

export const selectStyle = {
  viewContainer: {
    paddingVertical: 6,
    maxWidth: 200,
    backgroundColor: 'transparent',
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 8,
  },
  iconContainer: {
    top: 7,
    right: 0,
    color: 'white',
  },
};

export const selectTextInputStyle = {
  underlineColorAndroid: 'cyan',
  marginRight: 20,
  marginTop: 2,
  color: 'white',
};
