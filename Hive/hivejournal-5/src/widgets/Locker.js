/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {AppState, View, NativeModules, Appearance} from 'react-native';
import {connect} from 'react-redux';
import PINCode from '@haskkor/react-native-pincode';
import AsyncStorage from '@react-native-community/async-storage';
import {SettingActions} from '../actions';
import Utils from '../utils';
import * as RootNavigation from '../navigation/RootNavigation';

class Locker extends React.Component {
  constructor(props) {
    super(props);
    this.lockerTimer = null;
    this.state = {
      tab: '',
      appearance: Appearance.getColorScheme(),
      isFromBackground: false, // do not show the lock screen while the person is in the middle of composing a note (but if they were composing a note and the app went to the background and was re-opened to that "compose / in progress note", then show the lock screen if the timer says it is time to show it
    };
  }

  componentDidMount() {
    this.mount = true;
    AppState.addEventListener('change', this._handleAppStateChange);

    AsyncStorage.getItem('FIRST_TIME', (_err, started) => {
      console.log('FIRST_TIME = ', started);
      if (started === 'true') {
        AsyncStorage.removeItem('FIRST_TIME');
        AsyncStorage.getItem('USER_SETTINGS', (__err, userSettings) => {
          console.log(userSettings);
          if (userSettings) {
            this.props.setSettings({
              type: 'RESTORE',
              value: JSON.parse(userSettings),
            });
          }
        });
      }
    });

    global.eventEmitter.addListener('NEW_TAB', (tab) => {
      if (tab === 'HomeStack') {
        Utils.trackScreenView('User views Stream page');
      } else if (tab === 'SettingsStack') {
        Utils.trackScreenView('User views Settings page');
      } else if (tab === 'Flow') {
        Utils.trackScreenView('User views Flow page');
      } else if (tab === 'FeedStack') {
        Utils.trackScreenView('User views Notes page');
      }

      this.setState(
        {
          tab: tab,
        },
        () => {
          if (tab !== 'FeedStack' && tab !== '') {
            // For compose note screen, no need lock except for coming from background.
            this.doLock();
          }
        },
      );
    });

    setInterval(() => {
      const appearance = Appearance.getColorScheme();
      if (this.mount && this.state.appearance !== appearance) {
        this.setState({
          appearance,
        });
      }
    }, 1000 * 5); // 10 seconds
  }

  doLock = () => {
    const {passcode} = this.props;
    console.log('** do Lock', passcode.locked);
    if (passcode.locked === false) {
      if (this.lockerTimer) {
        clearTimeout(this.lockerTimer);
      }

      this.lockerTimer = setTimeout(() => {
        this.toggleLock(true);
      }, passcode.after * 60 * 1000);
    }
  };

  componentWillUnmount() {
    this.mount = false;
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (!this.mount) {
      return;
    }

    if (nextAppState === 'active') {
      const {IntentModule} = NativeModules;
      IntentModule.isFromToday((res) => {
        if (res === 'YES') {
          RootNavigation.navigate('ToDo');
          IntentModule.removeFromToday();
        } else {
          this.setState(
            {
              isFromBackground: true,
            },
            () => {
              setTimeout(() => {
                AsyncStorage.getItem('CURRENT_TAB', (_err, tab) => {
                  console.log('*** Current Tab: ', tab);
                  if (tab !== 'ToDo') {
                    RootNavigation.navigate('FeedStack');
                  }
                });
              }, 300);
            },
          );
        }
      });
    }
    this.doLock();
  };

  toggleLock = (v) => {
    if (!this.mount) {
      return;
    }

    let data = JSON.parse(JSON.stringify(this.props.passcode));
    data.locked = v;
    this.props.setSettings({type: 'PASSCODE', value: data});

    if (v === false) {
      this.setState({
        isFromBackground: false,
      });
    }
  };

  render() {
    const {passcode, children, currentJournal} = this.props;
    const {tab, appearance, isFromBackground} = this.state;
    const isDark = appearance === 'dark';
    const modeTextColor = isDark ? 'white' : 'black';
    const modeBgColor = isDark ? 'black' : 'white';
    const isNoteScreen = tab === 'FeedStack' || tab === '';
    // When the passcode is enabled, always protect the "stream" page and the "settings" page. But give the user 2 additional toggle options, of whether they also want to protect the "note entry" screen and/or the "self-rating (satisfaction/energy) screen"
    if (
      passcode.locked === true &&
      passcode.enabled === true &&
      passcode.number !== '' &&
      ((passcode.flowLock === true && tab === 'Flow') ||
        // (passcode.notesLock === true && tab === 'FeedStack') ||
        // tab === 'FeedStack' ||
        (typeof currentJournal !== 'undefined' &&
          currentJournal.noteLocked === true) ||
        (tab !== 'Flow' && !isNoteScreen) ||
        (isNoteScreen && isFromBackground))
    ) {
      return (
        <View style={{flex: 1, backgroundColor: modeBgColor}}>
          <PINCode
            titleEnter={'Unlock Hive Journal'}
            storedPin={passcode.number}
            status={'enter'}
            touchIDDisabled={true}
            stylePinCodeColorTitle={modeTextColor}
            stylePinCodeColorSubtitle={modeTextColor}
            onClickButtonLockedPage={() => {}}
            stylePinCodeButtonCircle={{
              backgroundColor: isDark ? '#1A1A1C' : 'rgb(242, 245, 251)',
            }}
            handleResultEnterPin={(result) => {
              if (result === passcode.number) {
                this.toggleLock(false);
              }
            }}
          />
        </View>
      );
    }

    return children;
  }
}

const mapStateToProps = (state) => {
  return {
    passcode: state.SettingReducer.passcode,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Locker);
