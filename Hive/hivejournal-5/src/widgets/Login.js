/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  SafeAreaView,
  Image,
  Button,
} from 'react-native';
import appleAuth, {
  AppleButton,
  AppleAuthCredentialState,
  AppleAuthRequestOperation,
  AppleAuthRequestScope,
} from '@invertase/react-native-apple-authentication';
import AsyncStorage from '@react-native-community/async-storage';
import Auth0 from 'react-native-auth0';
import moment from 'moment';
import LoadingOverlay from '../widgets/LoadingOverlay';
import ApiConstants from '../constants/ApiConstants';
import {MongoActions, CommonActions, SettingActions} from '../actions';
import DummyData from '../constants/DummyData';
import Colors from '../constants/Colors';

const logoIcon = require('../../assets/icons/logo.png');
const auth0 = new Auth0(ApiConstants.getAuthApi());
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  componentDidMount() {
    this.mount = true;
  }

  componentWillUnmount() {
    this.mount = false;
  }

  onAppleButtonPress = async () => {
    try {
      if (!this.mount) {
        return;
      }
      this.setState({
        loading: true,
      });
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: AppleAuthRequestOperation.LOGIN,
        requestedScopes: [
          AppleAuthRequestScope.EMAIL,
          AppleAuthRequestScope.FULL_NAME,
        ],
      });
      const credentialState = await appleAuth.getCredentialStateForUser(
        appleAuthRequestResponse.user,
      );
      if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
        const {user, email, fullName} = appleAuthRequestResponse;
        const {givenName, familyName} = fullName;
        const name = givenName ? `${givenName} ${familyName}` : 'Apple user';
        const generatedEmail = email || `${user}@icloud.com`;
        // alert(user + ' *** ' + name + ' *** ' + generatedEmail);
        this._callLoginProcess(
          {
            type: 'Apple',
            sub: user,
            familyName: familyName ? familyName : '',
            givenName: givenName ? givenName : '',
            name: name,
            email: generatedEmail,
            emailVerified: true,
            nickname: name,
            picture: '',
            locale: 'en',
            updatedAt: moment().format(),
          },
          user,
        );
      }
    } catch (error) {
      console.log(error);
      if (this.mount) {
        this.setState({
          loading: false,
        });
      }
    }
  };

  onLogin = () => {
    if (!this.mount) {
      return;
    }
    this.setState(
      {
        loading: true,
      },
      () => {
        auth0.webAuth
          .authorize({
            scope: 'openid profile email',
            audience: 'https://hive-point-seven.auth0.com/userinfo',
            prompt: 'login',
          })
          .then((credentials) => {
            auth0.auth
              .userInfo({token: credentials.accessToken})
              .then((user) => {
                console.log(user);
                this._callLoginProcess(user, credentials.accessToken);
              })
              .catch((error) => {
                console.log(error);
                if (this.mount) {
                  this.setState({
                    loading: false,
                  });
                }
              });
          })
          .catch((error) => {
            console.log(error);
            if (this.mount) {
              this.setState({
                loading: false,
              });
            }
          });
      },
    );
  };

  _callLoginProcess = (user, token) => {
    AsyncStorage.setItem('USER_ID', user.sub);
    this.props.setUser({
      value: user,
    });

    if (
      user.email === 'hive-bot1@gmx.com' ||
      user.email === 'iloveaustralia@gmx.com'
    ) {
      this.props.setSettings({
        type: 'NOTE_VISIBILITY',
        value: DummyData.bots[user.email],
      });
    }

    this.props.createUser({
      data: {
        user_id: user.sub,
        family_name: user.familyName,
        given_name: user.givenName,
        name: user.name,
        email: user.email,
        email_verified: user.emailVerified,
        nickname: user.nickname,
        picture: user.picture,
        locale: user.locale,
        updated_at: user.updatedAt,
      },
      cb: (res) => {
        if (typeof res.error === 'undefined') {
          if (res.insertedId) {
            // First log in time, it will return insertedId instead of _id
            AsyncStorage.setItem('DB_USER_ID', res.insertedId);
            this.props.setSettings({
              type: 'SELECTED_FONT',
              value: false,
            });
          } else {
            console.log(res);
            AsyncStorage.setItem('DB_USER_ID', res._id);
            if (res.userSettings) {
              if (res.userSettings.selectedFont === 'none') {
                res.userSettings.selectedFont = false;
              }
              AsyncStorage.setItem(
                'USER_SETTINGS',
                JSON.stringify(res.userSettings),
              );
              console.log('*** user settings ***');
              console.log(res.userSettings);
              this.props.setSettings({
                type: 'RESTORE',
                value: res.userSettings,
              });

              if (
                typeof res.userSettings.journalCover === 'undefined' ||
                res.userSettings.journalCover === -1
              ) {
                this.props.setSettings({
                  type: 'JOURNAL_COVER',
                  value: 0,
                });
              }
            }
          }
        }

        AsyncStorage.getItem('fcmToken', (_err, fcmToken) => {
          if (fcmToken) {
            this.props.setSettings({
              type: 'FCM_TOKEN',
              value: fcmToken,
            });
          }
        });

        this.props.saveLocations({
          cb: () => {
            if (this.mount) {
              this.setState({
                loading: false,
              });
            }
          },
        });
        this.props.setAccessToken({
          value: token,
        });
      },
    });
  };

  onLogout = () => {
    this.props.setAccessToken({value: null});
    this.props.setUser({value: null});
  };

  render() {
    const {loading} = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="blue" barStyle="light-content" />
        <View style={styles.screen}>
          <LoadingOverlay loading={loading} />
          <View style={styles.branchContainer}>
            <Image source={logoIcon} style={styles.logoIcon} />
            <TouchableOpacity onPress={this.onLogin} style={styles.authBtn}>
              <Text style={styles.authBtnColor}>.:. come on in .:.</Text>
            </TouchableOpacity>
            <AppleButton
              cornerRadius={10}
              style={{
                width: 160,
                height: 45,
              }}
              buttonStyle={AppleButton.Style.WHITE}
              buttonType={AppleButton.Type.SIGN_IN}
              onPress={this.onAppleButtonPress}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.MongoReducer.error,
    accessToken: state.CommonReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createUser: (req) => dispatch(MongoActions.createUser(req.data, req.cb)),
    setAccessToken: (req) => dispatch(CommonActions.setAccessToken(req.value)),
    setUser: (req) => dispatch(CommonActions.setUser(req.value)),
    setSettings: (req) =>
      dispatch(SettingActions.setSettings(req.type, req.value)),
    saveLocations: (req) => dispatch(SettingActions.saveLocations(req.cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.scheme.gray,
  },
  screen: {
    flex: 1,
    backgroundColor: Colors.scheme.gray,
  },
  branchContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  authBtn: {
    marginTop: 30,
    marginBottom: 15,
    backgroundColor: Colors.scheme.blue,
    // height: 40,
    paddingHorizontal: 10,
    justifyContent: 'center',
    borderRadius: 10,
    width: 160,
    height: 45,
  },
  authBtnColor: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  logoIcon: {
    width: 300,
    height: 300,
  },
});
