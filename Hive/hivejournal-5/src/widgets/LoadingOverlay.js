import React from 'react';
import {StyleSheet, View, Text, Dimensions} from 'react-native';
import GifLoader from './GifLoader';
import Colors from '../constants/Colors';

// eslint-disable-next-line no-undef
export default (LoadingOverlay = props => {
  const {loading, message, containerStyle} = props;

  return loading ? (
    <View style={[styles.loading, containerStyle]}>
      <GifLoader />
      {message && <Text style={styles.message}>{message}</Text>}
    </View>
  ) : null;
});

const styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    zIndex: 99999,
  },
  message: {
    color: Colors.scheme.blue,
    fontSize: 17,
    marginTop: 10,
  },
});
