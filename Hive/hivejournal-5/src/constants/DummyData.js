module.exports = {
  threeThings: {
    jimmy: {
      questions: [
        [
          'What went well today?',
          'Tell me something good about your day',
          'What was one of the best things that happened today?',
          'What was the highlight of your day today?',
          'Looking back on today, what went well?',
        ],
        [
          'Ok. How come?',
          'Interesting. Why do you think that is?',
          'Cool. What’s the most likely reason that happened?',
          'Nice! What do you think the cause of that was?',
          'Sounds good. Why do you think that happened?',
        ],
      ],
      introTexts: [
        [
          'Hey, do you have a minute?',
          'Is this a good time?',
          'Got a moment to chat?',
          "Hey, it's me. Got a second?",
          'Hi. Can I ask you something?',
          "I've got a quick question.",
        ],
        [
          'Cool! Ready for another one?',
          'Nice. Can we do number 2?',
          'Alright, got it. Give me another one!',
          "Ok, so that's one... ready for a second round?",
        ],
        [
          'Alright, last one, you ready?',
          'Ok, just one more.',
          'Awesome. How about number 3?',
          "Ok, so that's two... ready for the final one?",
          'Nice! On to the third and final round!',
        ],
      ],
      congratsText:
        "Hey awesome, thanks for letting me know! I'll see you tomorrow night",
    },
    q: {
      questions: [
        [
          "What was one Good Outcome from today's functions?",
          'What was one Positive Condition worth noting from today?',
          'Please record the first in a series of Outputs Worth Noting from the previous 24 hours:',
          'In reviewing the consolidated logs from today, what ranked as highly positive?',
        ],
        [
          'That sounds like a good outcome. What should we log as the basis for that?',
          'Double-plus good. What inputs produced that result?',
          ':) Please record your reasoning for that outcome:',
          'Superb. What conditions create that output?',
        ],
      ],
      introTexts: [
        [
          'Your daemon just woke me up. Can we interface?',
          'Beep boop. The time is now. Ready for me?',
          'Please, enter my chat session, lovely human!',
          "Hey, it's Q. Can you spare a few thousand milliseconds?",
          'Welcome to the Q hour. Can I get your logs for the day, captain?',
          'Query for you. Available cycles?',
        ],
        [
          'Double-plus good! Ready for another one?',
          'Nice. Can we perform an additional repetition?',
          'Copy that. Give me more, magnificent human!',
        ],
        [
          'Final round. Begin!',
          'Binary-speaking, we have 1 more.',
          ":) In binary terms, let's finish with number 11",
          'One final cycle.',
        ],
      ],
      congratsText:
        "Spectacular session, I am grateful for the data! Let's converse again tomorrow, same UTC, same channel!",
    },
    continueBtnTexts: ['Sure', 'Yeah', 'Yep', 'Of Course', 'Yes'],
    snoozeBtnTexts: [
      'Maybe later',
      'Not now. Talk soon?',
      'Not right now',
      'In a little bit',
      'Hit me up in a few minutes',
      'Later',
    ],
  },

  restUp: {
    jimmy: {
      introTexts: ['Can I ask how you slept last night?', 'How did you sleep?'],
    },
    q: {
      introTexts: [
        'May I log the sleep factors for you?',
        'You have rebooted. How long were you offline?',
      ],
    },
  },

  adminUsers: [
    'google-oauth2|111660039092673580212',
    'google-oauth2|101016042412080450721',
    'google-oauth2|104749481844376154446',
  ],

  bots: {
    'hive-bot1@gmx.com': {
      standard: ['any', 'invites'],
      '3Q': ['any', 'any'],
      rating: ['team', 'me'],
      sleep: ['me', 'guides-mine'],
    },
    'iloveaustralia@gmx.com': {
      standard: ['any', 'any'],
      '3Q': ['any', 'any'],
      rating: ['me', 'me'],
      sleep: ['any', 'me'],
    },
  },
};
