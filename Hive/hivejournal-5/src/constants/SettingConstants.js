module.exports = {
  SECRET_KEY: 'HIVE-SECURE-KEY',
  SET_SPOTIFY_ENABLED: 'SET_SPOTIFY_ENABLED',
  SET_TONE_ENABLED: 'SET_TONE_ENABLED',
  SET_PERSONALITY_ENABLED: 'SET_PERSONALITY_ENABLED',
  SET_LOCATION: 'SET_LOCATION',
  SET_STREAM_FONT: 'SET_STREAM_FONT',
  SET_FONT_SIZE: 'SET_FONT_SIZE',
  SET_TONE_PACK: 'SET_TONE_PACK',
  SET_TIMEZONE: 'SET_TIMEZONE',
  SET_3Q_JOURNEY_ENABLED: 'SET_3Q_JOURNEY_ENABLED',
  SET_GUIDE: 'SET_GUIDE',
  SET_SNOOZE_TIME: 'SET_SNOOZE_TIME',
  SET_3Q_OVERRIDES_ENABLED: 'SET_3Q_OVERRIDES_ENABLED',
  SET_Q3_SAVE_PERIOD: 'SET_Q3_SAVE_PERIOD',
  SET_OVERRIDE_SYSTEM_TIME: 'SET_OVERRIDE_SYSTEM_TIME',
  SET_TEMP_SYSTEM_DATETIME: 'SET_TEMP_SYSTEM_DATETIME',
  SET_THREE_THINGS_SAVED: 'SET_THREE_THINGS_SAVED',
  SET_CLOUDS_START_TIME: 'SET_CLOUDS_START_TIME',
  SET_CLOUDS_MULTIPLIER: 'SET_CLOUDS_MULTIPLIER',
  SET_RESET: 'SET_RESET',
  SET_RESTORE: 'SET_RESTORE',
  SET_HOPE: 'SET_HOPE',
  SET_ENERGY: 'SET_ENERGY',
  SET_FLOW_LABELS: 'SET_FLOW_LABELS',
  SET_FLOW_DESCRIPTION_PACK: 'SET_FLOW_DESCRIPTION_PACK',
  SET_REST_UP_JOURNEY: 'SET_REST_UP_JOURNEY',
  SET_FLOW_JOURNEY: 'SET_FLOW_JOURNEY',
  SET_PASSCODE: 'SET_PASSCODE',
  SET_NOTE_VISIBILITY: 'SET_NOTE_VISIBILITY',
  SET_DEPTH_SIZE: 'SET_DEPTH_SIZE',
  SET_SCREEN_NAME: 'SET_SCREEN_NAME',
  SET_DOORWAY: 'SET_DOORWAY',
  SET_GENERAL_PREFERENCES: 'SET_GENERAL_PREFERENCES',
  SET_TAGS: 'SET_TAGS',
  SAVE_LOCATIONS_SUCCESS: 'SAVE_LOCATIONS_SUCCESS',
  SAVE_LOCATIONS_ERROR: 'SAVE_LOCATIONS_ERROR',
  SET_FCM_TOKEN: 'SET_FCM_TOKEN',
  SET_SELECTED_FONT: 'SET_SELECTED_FONT',
  SET_USER: 'SET_USER',
  SET_PROFILES: 'SET_PROFILES',
  SET_QTY_TOTAL_UNIQUE_DAYS: 'SET_QTY_TOTAL_UNIQUE_DAYS',
  SET_LAST_NOTE_SAVED_DATE: 'SET_LAST_NOTE_SAVED_DATE',
  SET_RATINGS: 'SET_RATINGS',
  VERIFY_SUCCESS: 'VERIFY_SUCCESS',
  VERIFY_ERROR: 'VERIFY_ERROR',
  MIN_WORDS: 100,
  MAX_WORDS: 10000,
  TONE_PACKS: {
    'animals-1': {
      anger: require('../../assets/images/animals-1/tiger.png'),
      joy: require('../../assets/images/animals-1/monkey.png'),
      tentative: require('../../assets/images/animals-1/panda.png'),
      sadness: require('../../assets/images/animals-1/bear.png'),
      confident: require('../../assets/images/animals-1/lion.png'),
      analytical: require('../../assets/images/animals-1/cat.png'),
      fear: require('../../assets/images/animals-1/sheep.png'),
    },
    'origami-1': {
      anger: require('../../assets/images/origami-1/crab.png'),
      joy: require('../../assets/images/origami-1/butterfly.png'),
      tentative: require('../../assets/images/origami-1/fish.png'),
      sadness: require('../../assets/images/origami-1/frog.png'),
      confident: require('../../assets/images/origami-1/bird.png'),
      analytical: require('../../assets/images/origami-1/swan.png'),
      fear: require('../../assets/images/origami-1/shrimp.png'),
    },
  },
  JIMMY_SCHEDULER_INTERVAL: 15000,
  THREE_QUESTIONS_DUE_END_TIME: '22', // 10pm

  Q3_SAVE_PERIOD: {
    start: '17:00',
    end: '21:00',
  },
  SNOOZE_TIME: '15',
  CLOUDS_START_TIME: '17',
  CLOUDS_MULTIPLIER: 2,

  SELECT_TONE_PACKS: [
    {
      label: 'Animals-1',
      value: 'animals-1',
    },
    {
      label: 'Origami-1',
      value: 'origami-1',
    },
  ],
  FLOW_DESCRIPTION_PACKS: [
    {
      // Description Pack 1:
      energy: [
        {
          name: 'energy1',
          ratingMin: 0,
          ratingMax: 3,
          descriptionText: 'Lowest amount of energy possible. Falling asleep.',
        },
        {
          name: 'energy2',
          ratingMin: 3.5,
          ratingMax: 5.5,
          descriptionText:
            'Very Low Energy. Wishing you were in bed right now.',
        },
        {
          name: 'energy3',
          ratingMin: 6.0,
          ratingMax: 7.5,
          descriptionText: 'Normal Energy Level.',
        },
        {
          name: 'energy4',
          ratingMin: 8.0,
          ratingMax: 10.0,
          descriptionText: 'Flow Level of Energy.',
        },
      ],
      hope: [
        {
          name: 'hope1',
          ratingMin: 0,
          ratingMax: 3,
          descriptionText: 'Life has never sucked this much.',
        },
        {
          name: 'hope2',
          ratingMin: 3.5,
          ratingMax: 5.5,
          descriptionText: 'Just feeling really low.',
        },
        {
          name: 'hope3',
          ratingMin: 6,
          ratingMax: 7.5,
          descriptionText: 'Fairly optimistic and satisfied.',
        },
        {
          name: 'hope4',
          ratingMin: 8,
          ratingMax: 10,
          descriptionText: 'Lots of gratitude, satisfaction, hope.',
        },
      ],
    },
    {
      // Description Pack 2:
      energy: [
        {
          name: 'energy1',
          ratingMin: 0,
          ratingMax: 3,
          descriptionText: 'Do not want to even move!',
        },
        {
          name: 'energy2',
          ratingMin: 3.5,
          ratingMax: 5.5,
          descriptionText: 'Moving super slow.',
        },
        {
          name: 'energy3',
          ratingMin: 6.0,
          ratingMax: 7.5,
          descriptionText: 'Normal speed.',
        },
        {
          name: 'energy4',
          ratingMin: 8.0,
          ratingMax: 10.0,
          descriptionText: 'Moving fast!',
        },
      ],
      hope: [
        {
          name: 'hope1',
          ratingMin: 0,
          ratingMax: 3,
          descriptionText: 'Super sad.',
        },
        {
          name: 'hope2',
          ratingMin: 3.5,
          ratingMax: 5.5,
          descriptionText: 'Kinda sad.',
        },
        {
          name: 'hope3',
          ratingMin: 6,
          ratingMax: 7.5,
          descriptionText: 'Feeling good.',
        },
        {
          name: 'hope4',
          ratingMin: 8,
          ratingMax: 10,
          descriptionText: 'Feeling great!',
        },
      ],
    },
  ],
  HOPE_LABELS: [
    'satisfaction',
    'hope',
    'optimism',
    'gratitude',
    'peace',
    'outlook',
    'meaning',
    'direction',
  ],
  ENERGY_LABELS: ['energy', 'juice', 'drive', 'force', 'liveliness'],
  HOW_SLEEP: ['great', 'good', 'ok', 'bad', 'terrible'],
  SELECT_VISIBLE_USERS: [
    {label: 'Any hive user', value: 'any'},
    {label: 'My team only', value: 'team'},
    {label: 'My friends only', value: 'friends'},
    {label: 'Any guides', value: 'guides-any'},
    {label: 'My guides only', value: 'guides-mine'},
    {label: 'Only those I invite', value: 'invites'},
    {label: 'Only me', value: 'me'},
  ],

  spacingHorizontalMin: 10, // card spacing horizontal min
  spacingHorizontalMax: 50, // card spacing horizontal max
  verticalPositionMin: 10, // card vertical position min
  verticalPositionMax: 100, // card vertical position max
  sizeAtVerticalMin: 20, // card size at vertical min
  sizeAtVerticalMax: 100, // card size at vertical max
  defaultDepthSize: {
    enabled: false,
    hSpace: [10, 50],
    vPosition: [10, 100],
    vSize: [20, 100],
  },
  defaultFlowJourney: {
    cloudsStartAfterHours: 12,
    cloudsFullAfterHours: 24,
  },
  MOON_PHASE: [
    'New Moon',
    'Waxing Crescent Moon',
    'Quarter Moon',
    'Waxing Gibbous Moon',
    'Full Moon',
    'Waning Gibbous Moon',
    'Last Quarter Moon',
    'Waning Crescent Moon',
  ],
  SELECT_DOORWAYS: [
    {
      label: 'Doorway - 1',
      value: 0,
    },
    {
      label: 'Archway - 1',
      value: 1,
    },
    {
      label: 'Archway - 2',
      value: 2,
    },
    {
      label: 'Archway - 3',
      value: 3,
    },
    {
      label: 'Archway - 4',
      value: 4,
    },
    {
      label: 'Archway - 5',
      value: 5,
    },
    {
      label: 'Archway - 6',
      value: 6,
    },
    {
      label: 'Oculus - 1',
      value: 7,
    },
    {
      label: 'Oculus - 2',
      value: 8,
    },
    {
      label: 'Oculus - 3',
      value: 9,
    },
  ],
};
