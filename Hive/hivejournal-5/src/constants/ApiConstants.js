let _inv = {
  DEV: {
    auth0: {
      domain: 'hive-point-seven.auth0.com',
      clientId: 'dYpmvExNnJ3kMxD6XOFuY0wfOfiOIxd5',
    },
    tone_analyzer: {
      url: 'https://gateway.watsonplatform.net/tone-analyzer/api',
      key: 'S8JukQpuRvYOxNAZTo2sRwB2GzFq_u6wgWFKLlCabi_h',
      version: '2017-09-21',
    },
    personality_insights: {
      url: 'https://gateway.watsonplatform.net/personality-insights/api',
      key: 'kB8BRthLUd5Fk1iAqqrUzVIFl3XdZu5TQG3Tq2TXqqI-',
      version: '2017-09-21',
    },
    spotify: {
      url: 'https://api.spotify.com/v1',
      client_id: '71585070a1b541f09580c9521bb20a97',
      client_secret: 'e54b703eeeae42eb816e4485486e9a74',
    },
    mongo: {
      url:
        'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/stitch-app-1-cdzgt/service/',
      secret: 'gmu4V2Mgmu4V2MCzST5CzST5',
    },
    ipstack: {
      url: 'http://api.ipstack.com/',
      key: '512bb80e1eca1b8c5e024d4ed26412e8',
    },
  },
  PRODUCTION: {
    auth0: {
      domain: 'hive-point-seven.auth0.com',
      clientId: 'plxNHCej610iIwzoFrAKbpwzwW1T9wVI',
    },
    tone_analyzer: {
      url: 'https://gateway.watsonplatform.net/tone-analyzer/api',
      key: 'S8JukQpuRvYOxNAZTo2sRwB2GzFq_u6wgWFKLlCabi_h',
      version: '2017-09-21',
    },
    personality_insights: {
      url: 'https://gateway.watsonplatform.net/personality-insights/api',
      key: 'kB8BRthLUd5Fk1iAqqrUzVIFl3XdZu5TQG3Tq2TXqqI-',
      version: '2017-09-21',
    },
    spotify: {
      url: 'https://api.spotify.com/v1',
      client_id: '71585070a1b541f09580c9521bb20a97',
      client_secret: 'e54b703eeeae42eb816e4485486e9a74',
    },
    mongo: {
      url:
        'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/stitch-app-1-cdzgt/service/',
      secret: 'gmu4V2Mgmu4V2MCzST5CzST5',
    },
    ipstack: {
      url: 'http://api.ipstack.com/',
      key: '512bb80e1eca1b8c5e024d4ed26412e8',
    },
  },
  ENVIRONMENT: 'DEV',
};

module.exports = {
  getToneAnalyzerApi: () => {
    let data = _inv[_inv.ENVIRONMENT].tone_analyzer;
    return data;
  },
  getPersonalityInsightsApi: () => {
    let data = _inv[_inv.ENVIRONMENT].personality_insights;
    return data;
  },
  getSpotifyApi: () => {
    let data = _inv[_inv.ENVIRONMENT].spotify;
    return data;
  },
  getAuthApi: () => {
    let data = _inv[_inv.ENVIRONMENT].auth0;
    return data;
  },
  getMongoApi: () => {
    let data = _inv[_inv.ENVIRONMENT].mongo;
    return data;
  },
  getIpStackApi: () => {
    let data = _inv[_inv.ENVIRONMENT].ipstack;
    return data;
  },
};
