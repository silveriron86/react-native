export default {
  scheme: {
    blue: '#4fc7ff',
    gray: '#252525',
    black: '#111111',
  },

  tones: {
    anger: '#E80521',
    fear: '#325E2B',
    joy: '#FFD629',
    sadness: '#086DB2',
    analytical: '#DDDDDD',
    confident: '#592684',
    tentative: '#1AE5CD',
    none: '#E0E0E0',
  },
};
