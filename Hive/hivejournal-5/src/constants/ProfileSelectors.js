export const ProfileSelectors = {
  type: [
    {
      mbti: {
        source: [
          {
            name: '16personalities',
            url: 'https://www.16personalities.com/personality-types',
            profileTypesLabel: 'Personality Types',
            categories: [
              {
                name: 'analysts',
                displayName: 'Analysts',
                profileTypes: [
                  {
                    mainTypeShortName: 'intj',
                    mainTypeLabel: 'INTJ',
                    mainTypeImage: require('../../assets/images/personality/intj-architect.png'),
                    typeUrl: 'https://www.16personalities.com/intj-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ], // subTypes
                  },
                  {
                    mainTypeShortName: 'intp',
                    mainTypeLabel: 'INTP',
                    mainTypeImage: require('../../assets/images/personality/intp-logician.png'),
                    typeUrl: 'https://www.16personalities.com/intp-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'entj',
                    mainTypeLabel: 'ENTJ',
                    mainTypeImage: require('../../assets/images/personality/entj-commander.png'),
                    typeUrl: 'https://www.16personalities.com/entj-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'entp',
                    mainTypeLabel: 'ENTP',
                    mainTypeImage: require('../../assets/images/personality/entp-debater.png'),
                    typeUrl: 'https://www.16personalities.com/entp-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'infj',
                    mainTypeLabel: 'INFJ',
                    mainTypeImage: require('../../assets/images/personality/infj-advocate.png'),
                    typeUrl: 'https://www.16personalities.com/infj-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'infp',
                    mainTypeLabel: 'INFP',
                    mainTypeImage: require('../../assets/images/personality/infp-mediator.png'),
                    typeUrl: 'https://www.16personalities.com/infp-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'enfj',
                    mainTypeLabel: 'ENFJ',
                    mainTypeImage: require('../../assets/images/personality/enfj-protagonist.png'),
                    typeUrl: 'https://www.16personalities.com/enfj-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'enfp',
                    mainTypeLabel: 'ENFP',
                    mainTypeImage: require('../../assets/images/personality/enfp-campaigner.png'),
                    typeUrl: 'https://www.16personalities.com/enfp-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'istj',
                    mainTypeLabel: 'ISTJ',
                    mainTypeImage: require('../../assets/images/personality/istj-logistician.png'),
                    typeUrl: 'https://www.16personalities.com/istj-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'isfj',
                    mainTypeLabel: 'ISFJ',
                    mainTypeImage: require('../../assets/images/personality/isfj-defender.png'),
                    typeUrl: 'https://www.16personalities.com/isfj-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'estj',
                    mainTypeLabel: 'ESTJ',
                    mainTypeImage: require('../../assets/images/personality/estj-executive.png'),
                    typeUrl: 'https://www.16personalities.com/estj-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'esfj',
                    mainTypeLabel: 'ESFJ',
                    mainTypeImage: require('../../assets/images/personality/esfj-consul.png'),
                    typeUrl: 'https://www.16personalities.com/esfj-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'istp',
                    mainTypeLabel: 'ISTP',
                    mainTypeImage: require('../../assets/images/personality/istp-virtuoso.png'),
                    typeUrl: 'https://www.16personalities.com/istp-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'isfp',
                    mainTypeLabel: 'ISFP',
                    mainTypeImage: require('../../assets/images/personality/isfp-adventurer.png'),
                    typeUrl: 'https://www.16personalities.com/isfp-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'estp',
                    mainTypeLabel: 'ESTP',
                    mainTypeImage: require('../../assets/images/personality/estp-entrepreneur.png'),
                    typeUrl: 'https://www.16personalities.com/estp-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                  {
                    mainTypeShortName: 'esfp',
                    mainTypeLabel: 'ESFP',
                    mainTypeImage: require('../../assets/images/personality/esfp-entertainer.png'),
                    typeUrl: 'https://www.16personalities.com/esfp-personality',
                    requireSubType: true,
                    subTypes: [
                      {
                        subTypeShortName: 'A',
                        subTypeLabel: 'Assertive',
                      },
                      {
                        subTypeShortName: 'T',
                        subTypeLabel: 'Turbulent',
                      },
                    ],
                  },
                ], // profileTypes
              },
            ], // categories
          },
        ], // sources
      },
    },
  ], // types
}; // profileSelectors
