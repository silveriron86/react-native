/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Provider} from 'react-redux';
import {Alert, ScrollView, Text, TouchableOpacity} from 'react-native';
import Colors from './constants/Colors';
import {SafeAreaView} from 'react-native-safe-area-context';
// import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import EventEmitter from '../node_modules/react-native/Libraries/vendor/emitter/EventEmitter';
import messaging from '@react-native-firebase/messaging';
import ErrorBoundary from 'react-native-error-boundary';
import AppNavigator from './navigation/Routes';

import store from './store';

const CustomFallback = (props) => (
  <SafeAreaView>
    <ScrollView style={{paddingHorizontal: 20, marginVertical: 20}}>
      <Text style={{fontSize: 42}}>Oops!</Text>
      <Text style={{fontSize: 32, fontWeight: 'bold', marginBottom: 10}}>
        There's an error
      </Text>
      <Text>{props.error.toString()}</Text>
      <TouchableOpacity
        onPress={props.resetError}
        style={{
          backgroundColor: Colors.scheme.blue,
          padding: 8,
          marginTop: 10,
        }}>
        <Text style={{color: 'white', fontSize: 16, textAlign: 'center'}}>
          Try again!
        </Text>
      </TouchableOpacity>
    </ScrollView>
  </SafeAreaView>
);

AsyncStorage.setItem('FIRST_TIME', 'true');

export default class Root extends React.Component {
  UNSAFE_componentWillMount() {
    this.eventEmitter = new EventEmitter();
    global.eventEmitter = this.eventEmitter;
  }

  async componentDidMount() {
    this.requestPermission();

    // When the application is running, but in the background
    messaging().onNotificationOpenedApp((remoteMessage) => {
      this.showAlert(remoteMessage);
    });

    // When the application is opened from a quit state
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        if (remoteMessage) {
          this.showAlert(remoteMessage);
        }
      });

    messaging().onMessage(async (remoteMessage) => {
      this.showAlert(remoteMessage);
    });
    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      this.showAlert(remoteMessage);
    });
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log('*** Saved fcmToken = ', fcmToken);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      console.log('*** New fcmToken = ', fcmToken);
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }

    messaging().onTokenRefresh((token) => {
      if (token) {
        AsyncStorage.setItem('fcmToken', token);
      }
    });
  }

  async requestPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
      this.getToken();
    }
  }

  onMessageReceived = (message) => {
    console.log('Received: ', message);
  };

  showAlert(remoteMessage) {
    const {title, body} = remoteMessage.notification;
    Alert.alert(
      title,
      body,
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  }

  render() {
    return (
      <ErrorBoundary FallbackComponent={CustomFallback}>
        <Provider store={store}>
          <AppNavigator />
        </Provider>
      </ErrorBoundary>
    );
  }
}
