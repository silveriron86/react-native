import CommonConstants from '../constants/CommonConstants';
import AsyncStorage from '@react-native-community/async-storage';

let CommonActions = {
  reset: function() {
    return {
      type: CommonConstants.RESET,
    };
  },
  setAccessToken: function(value) {
    if (value === null) {
      AsyncStorage.removeItem('ACCESS_TOKEN');
    } else {
      AsyncStorage.setItem('ACCESS_TOKEN', value);
    }
    return {
      response: value,
      type: CommonConstants.SET_ACCESS_TOKEN,
    };
  },
  setUser: function(value) {
    if (value === null) {
      AsyncStorage.removeItem('USER');
    } else {
      AsyncStorage.setItem('USER', JSON.stringify(value));
    }
    return {
      response: value,
      type: CommonConstants.SET_USER,
    };
  },
  setLastNote: function(value, id) {
    return {
      id: id,
      response: value,
      type: CommonConstants.SET_LAST_NOTE,
    };
  },
};

export default CommonActions;
