import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import MongoConstants from '../constants/MongoConstants';
import ApiConstants from '../constants/ApiConstants';
import axios from 'axios';

let MongoActions = {
  reset: function () {
    return {
      type: MongoConstants.RESET,
    };
  },
  setFailedQueue: function (data) {
    return {
      response: data,
      type: MongoConstants.SET_FAILED_QUEUE,
    };
  },
  createUserError: function (error) {
    return {
      error,
      type: MongoConstants.CREATE_USER_ERROR,
    };
  },
  createUserSuccess: function (response) {
    return {
      response,
      type: MongoConstants.CREATE_USER_SUCCESS,
    };
  },
  createUser: function (data, cb) {
    return (dispatch) => {
      let api = ApiConstants.getMongoApi();
      axios({
        method: 'POST',
        // url: api.url + 'service-http-users-create-v2/incoming_webhook/webhook-users-create-v2-1?secret=' + api.secret,
        url:
          api.url +
          'service-http-users-create-v2/incoming_webhook/webhook-users-create-v3?secret=' +
          api.secret,
        headers: {
          'Content-Type': 'application/json',
        },
        data: data,
      })
        .then((response) => {
          console.log(response);
          dispatch(this.createUserSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.createUserError(error));
          cb({error: error});
        });
    };
  },

  createNoteError: function (error) {
    return {
      error,
      type: MongoConstants.CREATE_NOTE_ERROR,
    };
  },
  createNoteSuccess: function (response) {
    return {
      response,
      type: MongoConstants.CREATE_NOTE_SUCCESS,
    };
  },
  createNote: function (data, cb) {
    console.log('***** Create Note');
    return (dispatch) => {
      let api = ApiConstants.getMongoApi();
      NetInfo.fetch().then((state) => {
        if (state.isConnected === false) {
          //offline
          dispatch(this.createNoteError(true));
          cb({error: true});
        } else {
          console.log(data);
          axios({
            method: 'POST',
            url:
              api.url +
              'service-http-notes-create/incoming_webhook/webhook-notes-create?secret=' +
              api.secret,
            headers: {
              'Content-Type': 'application/json',
            },
            data: data,
          })
            .then((response) => {
              dispatch(this.createNoteSuccess(response.data));
              cb(response.data);
            })
            .catch((error) => {
              console.log(error);
              dispatch(this.createNoteError(error));
              cb({error: error});
            });
        }
      });
    };
  },

  getNotesError: function (error) {
    return {
      error,
      type: MongoConstants.GET_NOTES_ERROR,
    };
  },
  getNotesSuccess: function (response, page, main_tag) {
    return {
      response,
      page,
      type:
        main_tag === 'todo'
          ? MongoConstants.GET_TODOS_SUCCESS
          : MongoConstants.GET_NOTES_SUCCESS,
    };
  },
  getNotes: function (page_num, cb, page_size = 10, exclude = null) {
    // console.log('*** get Notes', exclude);
    return (dispatch) => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected === false) {
          //offline
          dispatch(this.getNotesError(true));
          cb({error: true});
        } else {
          AsyncStorage.getItem('USER_ID', (_err, user_id) => {
            let api = ApiConstants.getMongoApi();
            let url =
              api.url +
              // 'service-http-notes-get-by-user/incoming_webhook/webhook-getUserNotes-v1?secret=' +
              'service-http-notes-get-by-user/incoming_webhook/webhook_getUserNotes_exclusions_v1?secret=' +
              api.secret;
            url +=
              '&user_id=' +
              user_id +
              '&page_num=' +
              page_num +
              '&page_size=' +
              page_size;

            if (exclude) {
              url += '&exclude_main_tags=' + exclude;
            }

            axios({
              method: 'GET',
              url: url,
              headers: {
                'Content-Type': 'application/json',
              },
            })
              .then((response) => {
                let res = response.data;
                if (!Array.isArray(res)) {
                  res = [];
                }
                dispatch(this.getNotesSuccess(res, page_num, ''));
                cb(res);
              })
              .catch((error) => {
                dispatch(this.getNotesError(error));
                cb({error: error});
              });
          });
        }
      });
    };
  },

  archiveNoteError: function (error) {
    return {
      error,
      type: MongoConstants.ARCHIVE_NOTE_ERROR,
    };
  },
  archiveNoteSuccess: function (response) {
    return {
      response,
      type: MongoConstants.ARCHIVE_NOTE_SUCCESS,
    };
  },
  archiveNote: function (note, cb) {
    return (dispatch) => {
      let api = ApiConstants.getMongoApi();
      let url =
        api.url +
        'service-http-notes-archive-v1/incoming_webhook/webhook-http-notes-archive-v1?secret=' +
        api.secret;
      url += '&note_id=' + note._id;

      axios({
        method: 'POST',
        url: url,
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          console.log(response.data);
          dispatch(this.archiveNoteSuccess(note._id));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.archiveNoteError(error));
          cb({error: error});
        });
    };
  },

  unArchiveNoteError: function (error) {
    return {
      error,
      type: MongoConstants.UN_ARCHIVE_NOTE_ERROR,
    };
  },
  unArchiveNoteSuccess: function (response) {
    return {
      response,
      type: MongoConstants.UN_ARCHIVE_NOTE_SUCCESS,
    };
  },
  unArchiveNote: function (note_id, cb) {
    return (dispatch) => {
      let api = ApiConstants.getMongoApi();
      let url =
        api.url +
        'service-http-notes-archive-v1/incoming_webhook/webhook_http_notes_archive_v2?secret=' +
        api.secret;
      url += '&note_id=' + note_id + '&archive=' + false;
      console.log(note_id, url);

      axios({
        method: 'POST',
        url: url,
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          console.log(response.data);
          dispatch(this.unArchiveNoteSuccess(note_id));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.unArchiveNoteError(error));
          cb({error: error});
        });
    };
  },

  updateNotePermissionsError: function (error) {
    return {
      error,
      type: MongoConstants.UPDATE_NOTE_PERMISSIONS_ERROR,
    };
  },
  updateNotePermissionsSuccess: function (id, permissions) {
    return {
      id,
      permissions,
      type: MongoConstants.UPDATE_NOTE_PERMISSIONS_SUCCESS,
    };
  },
  updateNotePermissions: function (note, permissions, cb) {
    return (dispatch) => {
      let api = ApiConstants.getMongoApi();
      let url =
        api.url +
        'service-http-notes-permissions-update-v1/incoming_webhook/webhook_http_notes_permissions_update_v2?secret=' +
        api.secret;
      url += '&note_id=' + note._id;

      axios({
        method: 'POST',
        url: url,
        headers: {
          'Content-Type': 'application/json',
        },
        data: permissions,
      })
        .then((response) => {
          console.log(response.data);
          dispatch(this.updateNotePermissionsSuccess(note._id, permissions));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.updateNotePermissionsError(error));
          cb({error: error});
        });
    };
  },

  getExceptNotesError: function (error) {
    return {
      error,
      type: MongoConstants.GET_EXCEPT_NOTES_ERROR,
    };
  },
  getExceptNotesSuccess: function (response, page) {
    return {
      response,
      page,
      type: MongoConstants.GET_EXCEPT_NOTES_SUCCESS,
    };
  },
  getExceptNotes: function (page_num, cb) {
    return (dispatch) => {
      AsyncStorage.getItem('USER_ID', (_err, user_id) => {
        let api = ApiConstants.getMongoApi();
        let url =
          api.url +
          'service-http-notes-get-except-for-user/incoming_webhook/webhook-getNotesOthers-v3?secret=' +
          api.secret;
        url +=
          '&user_id=' + user_id + '&page_num=' + page_num + '&page_size=10';

        axios({
          method: 'GET',
          url: url,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            let res = response.data;
            console.log(res);
            if (!Array.isArray(res)) {
              res = [];
            }

            dispatch(this.getExceptNotesSuccess(res, page_num));
            cb(res);
          })
          .catch((error) => {
            dispatch(this.getExceptNotesError(error));
            cb({error: error});
          });
      });
    };
  },

  getNotesOtherSingleError: function (error) {
    return {
      error,
      type: MongoConstants.GET_NOTES_OTHER_SINGLE_ERROR,
    };
  },
  getNotesOtherSingleSuccess: function (response, page) {
    return {
      response,
      page,
      type: MongoConstants.GET_NOTES_OTHER_SINGLE_SUCCESS,
    };
  },
  getNotesOtherSingle: function (note_userid, page_num, cb) {
    return (dispatch) => {
      AsyncStorage.getItem('USER_ID', (_err, user_id) => {
        let api = ApiConstants.getMongoApi();
        let url =
          api.url +
          'service-http-notes-other-user-single-v1/incoming_webhook/webhook-getNotesOtherSingle-v1?secret=' +
          api.secret;
        url +=
          '&currentLoggedInUserId=' +
          user_id +
          '&page_num=' +
          page_num +
          '&page_size=10&viewNotesOfUserId=' +
          note_userid;

        axios({
          method: 'GET',
          url: url,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            let res = response.data;
            if (!Array.isArray(res)) {
              res = [];
            }
            dispatch(this.getNotesOtherSingleSuccess(res, page_num));
            cb(res);
          })
          .catch((error) => {
            dispatch(this.getNotesOtherSingleError(error));
            cb({error: error});
          });
      });
    };
  },

  updateNoteImageError: function (error) {
    return {
      error,
      type: MongoConstants.UPDATE_NOTE_IMAGE_ERROR,
    };
  },
  updateNoteImageSuccess: function (note_id, image_url) {
    return {
      note_id,
      image_url,
      type: MongoConstants.UPDATE_NOTE_IMAGE_SUCCESS,
    };
  },
  updateNoteImage: function (note_id, image_url, cb) {
    return (dispatch) => {
      let api = ApiConstants.getMongoApi();
      let url =
        api.url +
        'service-http-notes-update-image-v1/incoming_webhook/webhook_v1?secret=' +
        api.secret;
      url +=
        '&note_id=' + note_id + '&imageURL=' + encodeURIComponent(image_url);
      axios({
        method: 'POST',
        url: url,
      })
        .then((response) => {
          dispatch(this.updateNoteImageSuccess(note_id, image_url));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.updateNoteImageError(error));
          cb({error: error});
        });
    };
  },

  getFilteredNotes: function (main_tags, page_num, cb, page_size = 10) {
    console.log('---- getFilteredNotes ----', main_tags);
    return (dispatch) => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected === false) {
          //offline
          dispatch(this.getNotesError(true));
          cb({error: true});
        } else {
          AsyncStorage.getItem('USER_ID', (_err, user_id) => {
            let api = ApiConstants.getMongoApi();
            let url =
              api.url +
              'service-http-notes-get-by-user/incoming_webhook/webhook_getUserNotes_filterByMainTags_v1?secret=' +
              api.secret +
              '&user_id=' +
              user_id +
              '&page_num=' +
              page_num +
              '&page_size=' +
              page_size +
              '&main_tags=' +
              main_tags;

            axios({
              method: 'GET',
              url: url,
              headers: {
                'Content-Type': 'application/json',
              },
            })
              .then((response) => {
                let res = response.data;
                if (!Array.isArray(res)) {
                  res = [];
                }
                dispatch(this.getNotesSuccess(res, page_num));
                cb(res);
              })
              .catch((error) => {
                dispatch(this.getNotesError(error));
                cb({error: error});
              });
          });
        }
      });
    };
  },

  getToDoNotes: function (page_num, cb, page_size = 500) {
    return (dispatch) => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected === false) {
          //offline
          dispatch(this.getNotesError(true));
          cb({error: true});
        } else {
          AsyncStorage.getItem('USER_ID', (_err, user_id) => {
            let api = ApiConstants.getMongoApi();
            let url =
              api.url +
              'service-http-notes-get-by-user/incoming_webhook/webhook_getUserNotes_filterByMainTags_v1?secret=' +
              api.secret +
              '&user_id=' +
              user_id +
              '&page_num=' +
              page_num +
              '&page_size=' +
              page_size +
              '&main_tags=todo';
            console.log('url = ', url);

            axios({
              method: 'GET',
              url: url,
              headers: {
                'Content-Type': 'application/json',
              },
            })
              .then((response) => {
                let res = response.data;
                if (!Array.isArray(res)) {
                  res = [];
                }
                dispatch(this.getNotesSuccess(res, page_num, 'todo'));
                cb(res);
              })
              .catch((error) => {
                dispatch(this.getNotesError(error));
                cb({error: error});
              });
          });
        }
      });
    };
  },

  getUnencryptedNotesError: function (error) {
    return {
      error,
      type: MongoConstants.GET_UNENCRYPTED_NOTES_ERROR,
    };
  },
  getUnencryptedNotesSuccess: function (response) {
    return {
      response,
      type: MongoConstants.GET_UNENCRYPTED_NOTES_SUCCESS,
    };
  },
  getUnencryptedNotes: function (cb) {
    return (dispatch) => {
      AsyncStorage.getItem('USER_ID', (_err, user_id) => {
        let api = ApiConstants.getMongoApi();
        let url =
          api.url +
          'service-http-notes-get-by-user/incoming_webhook/webhook_getNotesByUserId_unencrypted_v1?secret=' +
          api.secret +
          '&user_id=' +
          // 'auth0|5dc9780f5267770e7c70185d';
          user_id;

        axios({
          method: 'GET',
          url: url,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            let res = response.data;
            if (!Array.isArray(res)) {
              res = [];
            }

            dispatch(this.getUnencryptedNotesSuccess(res));
            cb(res);
          })
          .catch((error) => {
            dispatch(this.getUnencryptedNotesError(error));
            cb({error: error});
          });
      });
    };
  },

  updateNoteVideoError: function (error) {
    return {
      error,
      type: MongoConstants.UPDATE_NOTE_VIDEO_ERROR,
    };
  },
  updateNoteVideoSuccess: function (id, videoURL) {
    return {
      id,
      videoURL,
      type: MongoConstants.UPDATE_NOTE_VIDEO_SUCCESS,
    };
  },
  updateNoteVideoURL: function (note_id, videoURL, cb) {
    return (dispatch) => {
      let api = ApiConstants.getMongoApi();
      let url =
        api.url +
        'service-http-notes-video-update-v1/incoming_webhook/webhook-http-notes-video-update-v1?secret=' +
        api.secret;
      url += '&note_id=' + note_id;

      axios({
        method: 'POST',
        url: url,
        headers: {
          'Content-Type': 'application/json',
        },
        data: videoURL,
      })
        .then((response) => {
          console.log(response.data);
          dispatch(this.updateNoteVideoSuccess(note_id, videoURL));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.updateNoteVideoError(error));
          cb({error: error});
        });
    };
  },

  updateNoteParamError: function (error) {
    return {
      error,
      type: MongoConstants.UPDATE_NOTE_PARAM_ERROR,
    };
  },
  updateNoteParamSuccess: function (id, videoURL) {
    return {
      id,
      videoURL,
      type: MongoConstants.UPDATE_NOTE_PARAM_SUCCESS,
    };
  },
  updateNoteParam: function (note_id, data, cb) {
    return (dispatch) => {
      let api = ApiConstants.getMongoApi();
      let url =
        api.url +
        'service-htpp-notes-params-update/incoming_webhook/webhook-http-notes-params-update-v1?secret=' +
        api.secret;
      url += '&note_id=' + note_id;

      const keys = Object.keys(data);
      keys.forEach((key) => {
        if (key === 'color') {
          data[key] = data[key].replace('#', '%23');
        }
        url += `&${key}=${data[key]}`;
      });

      axios({
        method: 'PATCH',
        url: url,
        headers: {
          'Content-Type': 'application/json',
        },
        data: data,
      })
        .then((response) => {
          console.log(response.data);
          dispatch(this.updateNoteParamSuccess(note_id, data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.updateNoteParamError(error));
          cb({error: error});
        });
    };
  },

  updateTodoSortError: function (error) {
    return {
      error,
      type: MongoConstants.UPDATE_TODO_SORT_ERROR,
    };
  },
  updateTodoSortSuccess: function (response) {
    return {
      response,
      type: MongoConstants.UPDATE_TODO_SORT_SUCCESS,
    };
  },
  updateTodoSort: function (data, cb) {
    return (dispatch) => {
      AsyncStorage.getItem('DB_USER_ID', (_err, user_id) => {
        let api = ApiConstants.getMongoApi();
        let url =
          api.url +
          'service-http-todo-sort-update-v1/incoming_webhook/webhook-http-todo-sort-update-v1?secret=' +
          api.secret;
        url += '&user_id=' + user_id;

        axios({
          method: 'POST',
          url: url,
          headers: {
            'Content-Type': 'application/json',
          },
          data: data,
        })
          .then((response) => {
            console.log(response.data);
            dispatch(this.updateTodoSortSuccess(response));
            cb(response.data);
          })
          .catch((error) => {
            dispatch(this.updateTodoSortError(error));
            cb({error: error});
          });
      });
    };
  },
};

export default MongoActions;
