import axios from 'axios';
import publicIP from 'react-native-public-ip';
import SettingConstants from '../constants/SettingConstants';
import ApiConstants from '../constants/ApiConstants';

let SettingActions = {
  setSettings: function(type, value) {
    return {
      response: value,
      type: SettingConstants[`SET_${type}`],
    };
  },

  saveLocationError: function(error) {
    return {
      error,
      type: SettingConstants.SAVE_LOCATIONS_ERROR,
    };
  },
  saveLocationSuccess: function(response) {
    return {
      response,
      type: SettingConstants.SAVE_LOCATIONS_SUCCESS,
    };
  },
  saveLocations: function(cb) {
    return dispatch => {
      publicIP().then(ip => {
        let api = ApiConstants.getIpStackApi();
        axios({
          method: 'GET',
          url: api.url + ip + '?access_key=' + api.key + '&format=1',
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then(response => {
            if (response.status === 200) {
              dispatch(this.saveLocationSuccess(response.data));
              cb(response.data);
            }
          })
          .catch(error => {
            dispatch(this.saveLocationError(error.response.data));
            cb(error.response.data);
          });
      });
    };
  },
  verifyError: function(error) {
    return {
      error,
      type: SettingConstants.VERIFY_ERROR,
    };
  },
  verifySuccess: function(response) {
    return {
      response,
      type: SettingConstants.VERIFY_SUCCESS,
    };
  },
  verify: function(cb) {
    return dispatch => {
      dispatch(this.verifySuccess(null));
      cb(null);
    };
  },
};

export default SettingActions;
