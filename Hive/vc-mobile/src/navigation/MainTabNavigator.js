import React from 'react';
import { AsyncStorage, Image, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createDrawerNavigator } from 'react-navigation';
import InviteScreen from '../screens/invite/InviteScreen';
import LikesScreen from '../screens/likes/LikesScreen';
import ProfileScreen from '../screens/profile/ProfileScreen';
import LeaderBoardScreen from '../screens/profile/LeaderBoardScreen';
import TranscriptScreen from '../screens/profile/TranscriptScreen';
import ReviewsScreen from '../screens/profile/ReviewsScreen';
import ContactScreen from '../screens/profile/ContactScreen';

import ProjectsScreen from '../screens/Projects/ProjectsScreen';
import ShareModal from '../screens/Projects/ShareModal';
import SearchScreen from '../screens/Search/SearchScreen';
import CalendarScreen from '../screens/Search/CalendarScreen';
import RequestScreen from '../screens/Search/RequestScreen';
import ProjectDetailsScreen from '../screens/Search/ProjectDetailsScreen';
import MyProjectDetails from '../screens/Projects/MyProjectDetails';

import HomeScreen from '../screens/Home/HomeScreen';
import SettingsScreen from '../screens/settings/SettingsScreen';
import PricingScreen from '../screens/PricingScreen';
import FindVolunteersScreen from '../screens/FindVolunteersScreen';
import SafetyScreen from '../screens/commons/SafetyScreen';
import ContactUsScreen from '../screens/ContactUsScreen';
import PrivacyPolicyScreen from '../screens/commons/PrivacyPolicyScreen';
import TermsOfServiceScreen from '../screens/commons/TermsOfServiceScreen';
import SideMenu from './SideMenu';
import ProjectDetailsShowScreen from '../screens/Search/ProjectDetailsShowScreen';
import NavigationVars from '../navigation/NavigationVars';

const leftArrow = require('../../assets/icons/left_arrow_white.png');
const Left = ({ onPress }) => (
  <TouchableOpacity onPress={onPress} style={styles.backBtn}>
    <Image source={leftArrow}/>
  </TouchableOpacity>
);
const ShareBtn = ({ onPress }) => (
  <TouchableOpacity onPress={onPress} style={styles.shareBtn}>
    <Image source={require('../../assets/icons/share.png')}/>
  </TouchableOpacity>
);
const HomeStack = createStackNavigator({
  Home: HomeScreen,
  Search: SearchScreen,
  Calendar: {
    screen: CalendarScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <Left onPress={() => navigation.goBack()} />,
      headerStyle: styles.calendarViewHeader,
      headerTitleStyle: styles.calendarViewTitle,
    })
  },
  Request: {
    screen: RequestScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <Left onPress={() => navigation.goBack()} />,
      headerStyle: styles.calendarViewHeader,
      headerTitleStyle: styles.calendarViewTitle,
    })
  },
  ProjectDetails: {
    screen: ProjectDetailsScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <Left onPress={() => navigation.goBack()} />,
      headerStyle: styles.calendarViewHeader,
      headerTitleStyle: styles.calendarViewTitle,
      headerRight: <ShareBtn onPress={() => navigation.navigate('Share')}/>,
    })
  },
  Share: {
    screen: ShareModal,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  ProjectDetailsShow: {
    screen: ProjectDetailsShowScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <Left onPress={() => navigation.goBack()} />,
      headerStyle: styles.calendarViewHeader,
      headerTitleStyle: styles.calendarViewTitle,
      headerRight: <ShareBtn onPress={() => navigation.navigate('Share', {data: navigation.getParam('data')})}/>,
    })    
  }
});

// ************************
// Notice this change here
// ************************

const onTabClick = ({ navigation, defaultHandler }) => {
  AsyncStorage.setItem('PREV_TAB', navigation.state.routeName);
  defaultHandler();
}

// HomeStack.navigationOptions = {
//   tabBarLabel: 'Search',
//   tabBarIcon: ({ focused }) => (
//     <Image
//       style={{ marginBottom: -3, width: 50, height: 50 }}
//       source={require('../../assets/icons/tabs/30_search.png')}
//     />
//   ),
// };

const InviteStack = createStackNavigator({
  Invite: InviteScreen
});

const LikesStack = createStackNavigator({
  Likes: LikesScreen,
});

LikesStack.navigationOptions = {
  tabBarLabel: 'Likes',
  tabBarIcon: ({ focused }) => (
    <Image
      style={[{ marginBottom: -3, width: 40, height: 40, tintColor: '#CACACA' }, focused && {tintColor: '#1D75BD'}]}
      source={require('../../assets/icons/tabs/32_like.png')}
    />
  ),
  tabBarOnPress: onTabClick
};

const ProjectsStack = createStackNavigator({
  Projects: ProjectsScreen,
  MyProjectDetails: {
    screen: MyProjectDetails,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <Left onPress={() => navigation.goBack()} />,
      headerStyle: styles.calendarViewHeader,
      headerTitleStyle: styles.calendarViewTitle,
      headerRight: <ShareBtn onPress={() => navigation.navigate('Share')}/>,
    })
  }
});

const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
  LeaderBoard: LeaderBoardScreen,
  Transcript: TranscriptScreen,
  Reviews: ReviewsScreen,
  Contact: {
    screen: ContactScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Contact',
      headerLeft: <Left onPress={() => navigation.goBack()} />,
      headerStyle: styles.calendarViewHeader,
      headerTitleStyle: styles.calendarViewTitle
    })
  }
});

class InviteWrapper extends React.Component {
  static router = InviteStack.router;
  static navigationOptions = {
    tabBarLabel: 'Invite',
    tabBarIcon: ({ focused }) => (
      <Image
        style={[{ marginBottom: -3, width: 45, height: 45, tintColor: '#CACACA' }, focused && {tintColor: '#1D75BD'}]}
        source={require('../../assets/icons/tabs/31_invite.png')}
      />
    ),
    tabBarOnPress: ({ navigation, defaultHandler }) => {
      console.log(navigation);
      defaultHandler();
    }    
  };
  render() {
    return (
        <InviteStack navigation={this.props.navigation} screenProps ={{rootNavigation: this.props.navigation}}></InviteStack>
    );
  }
}

class ProfileStackWrapper extends React.Component {
  static router = ProfileStack.router;
  static navigationOptions =  ({navigation}) => {
    return {
      tabBarLabel: 'Profile',
      tabBarIcon: ({ focused }) => (
        <Image
          style={[{ marginBottom: -3, width: 45, height: 45, tintColor: '#CACACA' }, focused && {tintColor: '#1D75BD'}]}
          source={require('../../assets/icons/tabs/34_profile.png')}
        />
      ),
      tabBarOnPress: onTabClick   
    }
  };

  render() {
    const {navigation} = this.props;
    return (
        <ProfileStack navigation={navigation} screenProps ={{rootNavigation: navigation}}></ProfileStack>
    );
  }
}

class HomeStackWrapper extends React.Component {
  static router = HomeStack.router;
  static navigationOptions = ({ navigation }) => {
      let { routeName } = navigation.state.routes[navigation.state.index];
      let navigationOptions = {};
  
      navigationOptions = {
          tabBarLabel: 'Search',
          tabBarIcon: ({ focused }) => (
              <Image
                  style={[{ marginBottom: -3, width: 45, height: 45, tintColor: '#CACACA' }, focused && {tintColor: '#1D75BD'}]}
                  source={require('../../assets/icons/tabs/30_search.png')}
              />
          ),
          tabBarOnPress: onTabClick
      };
  
      if (routeName === 'Request') {
          navigationOptions.tabBarVisible = false;
      }
      if (routeName === 'ProjectDetailsShow') {
          navigationOptions.tabBarVisible = false;
      }
  
      return navigationOptions;
  };

  render() {
    const {navigation} = this.props;
    const {rootNavigation} = this.props.screenProps;
    return (
        <HomeStack navigation={navigation} screenProps ={{parentNavigation: navigation, rootNavigation: rootNavigation}}></HomeStack>
    );
  }
}

class ProjectStackWrapper extends React.Component {
  static router = ProjectsStack.router;
   static navigationOptions =  ({navigation}) => {
    return {
      tabBarLabel: 'Projects',
      tabBarIcon: ({ focused }) => (
        <Image
            style={[{ marginBottom: -3, width: 45, height: 45, tintColor: '#CACACA' }, focused && {tintColor: '#1D75BD'}]}
            source={require('../../assets/icons/tabs/33_projects.png')}
        />
      ),
      tabBarOnPress: ({ navigation, defaultHandler }) => {
        AsyncStorage.setItem('PREV_TAB', navigation.state.routeName);
        NavigationVars.updateMyProjectsList = true;
        defaultHandler();
      }
    }
  };

  render() {
    const {navigation} = this.props;
    return (
      <ProjectsStack navigation={navigation} screenProps={{rootNavigation: navigation}}></ProjectsStack>
    );
  }
}

const TabNavigator = createBottomTabNavigator({
  HomeStackWrapper,
  InviteWrapper,
  LikesStack,
  ProjectStackWrapper,
  ProfileStackWrapper,
},{
  tabBarOptions: {
    inactiveTintColor: '#CACACA',
    activeTintColor: '#1D75BD',
    allowFontScaling: false,
    style: {
      height: 48,
      borderTopWidth: 2,
      borderTopColor: '#1D75BD'
    },
    tabStyle: {
      borderRightWidth: 1,
      borderRightColor: '#BDBDBD'
    }
  }
});

const SettingsStack = createStackNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  NotificationPreferences: {
    screen: ContactScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Contact',
      headerLeft: <Left onPress={() => navigation.goBack()} />,
      headerStyle: styles.calendarViewHeader,
      headerTitleStyle: styles.calendarViewTitle
    })
  }
});

export default createDrawerNavigator({
  Home: TabNavigator,
  Settings: SettingsScreen,
  // Pricing: PricingScreen,
  // FindVolunteers: FindVolunteersScreen,
  Safety: SafetyScreen,
  // ContactUs: ContactUsScreen,
  // PrivacyPolicy: PrivacyPolicyScreen,
  // TermsOfServiceScreen: TermsOfServiceScreen,
},
{
  contentComponent: SideMenu,
  drawerWidth: 307,
  default: 'Home',
});


const styles = StyleSheet.create({
  calendarViewHeader: {
    backgroundColor: '#1D75BD'
  },
  calendarViewTitle: {
    color: 'white',
    fontSize: 16
  },
  backBtn: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: 48,
    height: 41,
    zIndex: 9999,
    justifyContent: 'center',
    alignItems: 'center'
  },
  shareBtn: {
    marginRight: 12
  }
});
