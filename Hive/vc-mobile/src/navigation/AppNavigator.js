import React from 'react';
import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import Router from './Router';
import InitScreen from '../screens/InitScreen';
import LoginScreen from '../screens/LoginScreen';
import SelectTypeScreen from '../screens/Signup/SelectTypeScreen';
import AddParentScreen from '../screens/Signup/AddParentScreen';
import CreateAccountScreen from '../screens/Signup/CreateAccountScreen';
import ThankYouScreen from '../screens/Signup/ThankYouScreen';
import AddStudentScreen from '../screens/Signup/AddStudentScreen';

class MainTabNavigatorWrapper extends React.Component {
  static router = MainTabNavigator.router;
  render() {
    const {navigation} = this.props
    return (
        <MainTabNavigator navigation={ navigation } screenProps ={{rootNavigation: navigation}}></MainTabNavigator>
    );
  }
}

export default createAppContainer(createSwitchNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  Main: MainTabNavigatorWrapper,
  Router: Router,
  Init: InitScreen,
  SelectType: SelectTypeScreen,
  AddParent: AddParentScreen,
  CreateAccount: CreateAccountScreen,
  AddStudent: AddStudentScreen,
  ThankYou: ThankYouScreen,
  Login: LoginScreen,
}, {
  initialRouteName: "Router",
}));