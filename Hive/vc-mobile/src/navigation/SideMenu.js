import React, {Component} from 'react';
import {NativeModules, AsyncStorage, ScrollView, Text, View, StyleSheet, TouchableOpacity, Image, Alert, Linking} from 'react-native';
import Utils from '../utils'
import { SearchActions, LikesActions, CreateProjectModalActions, OrganizationActions, ProjectsActions } from '../actions';
import Config from 'react-native-config'
import { StackActions, NavigationActions } from 'react-navigation'
import axios from 'axios'
import General from '../constants/General';
import {LoadingOverlay} from '../components'
import { connect } from 'react-redux';

export class SideMenu extends Component {  
  navigate = (routeName) => {
    this.props.navigation.navigate(routeName);
  }

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }  

  closeDrawer = () => {
    this.props.navigation.closeDrawer();
  }

  resetApp = async () => {
    await this.props.resetAppSearch();
    await this.props.resetAppLikes();
    await this.props.resetAppCreateProjectModal();
    await this.props.resetAppOrganization();
    await this.props.resetAppProjects();
    return;
  }

  logOut = () => {
    const { navigation } = this.props;
    
    this.setState({
      loading: true
    }, () => {
      NativeModules.OAuth.logout();
      AsyncStorage.getItem('oio_auth', (err, auth) => {
        if(auth) {
          let data = JSON.parse(auth);
          let url = 'https://oauth.io/api/usermanagement/user/logout?k=' + Config.OAUTH_KEY + '&token=' + data.token;
          
          let options = {
            method: 'POST',
            url: url
          }
          axios(options).then(response => {
              console.log(response);
              if(response.status === 200) {
                this.setState({
                  loading: false
                }, () => {                
                  navigation.closeDrawer();
                  AsyncStorage.clear();
                  
                  this.resetApp().then (
                    navigation.navigate('Login')); 

                })
              }
          })
          .catch(error => {
            console.log(error);
            this.setState({
              loading: false
            }, () => {            
              Alert.alert('Error', 'Logout failed');
            })
          }) 
        }
      });
    })
  }

  render () {
    const {loading} = this.state;
    let rows = [];
    for(let key in this.props.descriptors) {
      if(key != 'Home') {
        let row = this.props.descriptors[key];
        let title = (row.options.title) ? row.options.title : row.key;
        rows.push(
          <TouchableOpacity key={`item-${key}`} style={styles.item} onPress={()=> this.navigate(row.state.routeName)}>
            <Text style={styles.itemText}>{title}</Text>
          </TouchableOpacity>
        );
      }
    }

    // -- for initial app submission, link to website pages for:
    // FindVolunteers: FindVolunteersScreen,
    // ContactUs: ContactUsScreen,
    // PrivacyPolicy: PrivacyPolicyScreen,
    // TermsOfServiceScreen: TermsOfServiceScreen,
    rows.push(
      <View key='item-extra'>
        <TouchableOpacity key='item-find' style={styles.item} onPress={() => Linking.openURL('https://volunteercrowd.com/home')}>
          <Text style={styles.itemText}>Find Volunteers</Text>
        </TouchableOpacity>

        <TouchableOpacity key='item-contactus' style={styles.item} onPress={() => Linking.openURL('https://volunteercrowd.com/home')}>
          <Text style={styles.itemText}>Contact Us</Text>
        </TouchableOpacity>

        <TouchableOpacity key='item-privacy' style={styles.item} onPress={() => Linking.openURL('https://volunteercrowd.com/privacy-policy')}>
          <Text style={styles.itemText}>Privacy Policy</Text>
        </TouchableOpacity>
        
        <TouchableOpacity key='item-terms' style={styles.item} onPress={() => Linking.openURL('https://volunteercrowd.com/terms')}>
          <Text style={styles.itemText}>Terms of Service</Text>
        </TouchableOpacity>

        <TouchableOpacity key='item-logout' style={styles.item} onPress={this.logOut}>
          <Text style={styles.itemText}>Log Out</Text>
        </TouchableOpacity>
      </View>

    );    

    return (
      <View style={styles.container}>
        <LoadingOverlay loading={loading}/>
        <TouchableOpacity onPress={this.closeDrawer}>
          <Image source={require('../../assets/icons/cross_blue.png')}/>
        </TouchableOpacity>
        <ScrollView style={styles.itemsWrapper}>
          {rows}
          <Text style={styles.version}>
            VERSION {General.BUILD_VERSION}
          </Text>
        </ScrollView>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
      resetAppSearch: () => dispatch(SearchActions.resetApp()),
      resetAppLikes: () => dispatch(LikesActions.resetApp()),
      resetAppCreateProjectModal: () => dispatch(CreateProjectModalActions.resetApp()),
      resetAppOrganization: () => dispatch(OrganizationActions.resetApp()),
      resetAppProjects: () => dispatch(ProjectsActions.resetApp()),
  }
};

// https://stackoverflow.com/questions/35443167/dispatch-is-not-a-function-when-argument-to-maptodispatchtoprops-in-redux
// use "null" if no "mapStateToProps"
export default connect(null, mapDispatchToProps)(SideMenu);


const styles = StyleSheet.create({
  container: {
    padding: 18,
    paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 58 : 18,
    flex: 1
  },
  item: {
    borderBottomWidth: 1,
    borderBottomColor: '#E0E2EE',
    height: 50,
    justifyContent: 'center'
  },
  itemText: {
    marginTop: 2,
    color: '#303030',
    fontSize: 14
  },
  itemsWrapper: {
    paddingTop: 5
  },
  version: {
    color: '#CACACA',
    fontSize: 10,
    marginTop: 15
  }
});