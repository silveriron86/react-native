import React from 'react';
import { ActivityIndicator, AsyncStorage, View, StyleSheet, ImageBackground } from 'react-native';

const bgImg = require('../../assets/images/splash_bg.png');
class Router extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true
    }
  }

  componentWillMount = () => {
    const { navigation } = this.props;
    AsyncStorage.getItem('oio_auth', (err, auth) => {
      if(auth) {
        navigation.navigate('Main', JSON.parse(auth))
      }else {
        navigation.navigate('Init')
      }
    });
  }

  render() {
    const { loading } = this.state;
    return (
      <ImageBackground source={bgImg} style={styles.container}>
        {
          loading && 
          <ActivityIndicator size={'large'} color="#ffffff" />
        }
      </ImageBackground>        
    )
  }
}

export default Router;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
});