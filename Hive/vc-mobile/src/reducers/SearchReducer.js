import ApiConstants from '../constants/ApiConstants';

const initialState = {
  filteredListData: null,
  filteredListError: null,
  likedError: null,
  likedData: null
};

function SearchReducer(state = initialState, action) {
  switch (action.type) {    
    case ApiConstants.RESET_APP:
      return initialState;

    case ApiConstants.GET_FILTER_LIST_SUCCESS:
      return Object.assign({}, state, {
        filteredListError: null,
        filteredListData: action.response
      });      

    case ApiConstants.GET_FILTER_LIST_ERROR:
      return Object.assign({}, state, {
        filteredListError: action.error,
        filteredListData: null
      }); 

    case ApiConstants.POST_LIKES_ERROR:
      return Object.assign({}, state, {
        filteredListError: action.error,
        filteredListData: null
      });       

    case ApiConstants.POST_LIKES_SUCCESS:
      return Object.assign({}, state, {
        likedError: null,
        likedData: action.response
      });         

    default:
      return state;
  }
}

export default SearchReducer;
