import ApiConstants from '../constants/ApiConstants';

const initialState = {
  error: null,
  showCongratsModal: false,
  isProjectModalOpen: false,
  isCauseModalOpen: false,
  isConfirmModalOpen: false,
  isProjectAddHours: false,
  isOrgAddHours: false,
  addHourStatus: false,
  sfProjectId: '',
  sfOrgId: '',
  reload: false,
  description: '',
  startDate: '',
  endDate: '',
  startTime: '',
  endTime: '',
  projectCause: '',
  projectType: '',
  projectTypeKey: '',
  checked: true,
  orgTitle: '',
  projectTitle: '',
  contactFirstName: '',
  contactLastName: '',
  contactEmail: '',
  address: '',
  city: '',
  state: '',
  zipCode: ''
};

function CreateProjectModalReducer(state = initialState, action) {
  switch (action.type) {
    
    case ApiConstants.RESET_APP:
      return initialState;

    case ApiConstants.CREATE_PROJECT_TOGGLE_MODAL:
      return Object.assign({}, state, {...state, ...action.data
      });
    case ApiConstants.HIDE_CONGRATS_MODAL:
        return Object.assign({}, state, {...state, showCongratsModal: false});
    
    case ApiConstants.POST_PROJECT_SUBMISSIONS_SUCCESS:
      return Object.assign({}, state, {
        error: null,
        showCongratsModal: true,
        isProjectModalOpen: false,
        isCauseModalOpen: false,
        isConfirmModalOpen: false,
        isProjectAddHours: false,
        isOrgAddHours: false,
        addHourStatus: false,
        sfProjectId: '',
        sfOrgId: '',
        reload: true,
        description: '',
        startDate: '',
        endDate: '',
        startTime: '',
        endTime: '',
        projectCause: '',
        projectType: '',
        projectTypeKey: '',
        checked: true,
        orgTitle: '',
        projectTitle: '',
        contactFirstName: '',
        contactLastName: '',
        contactEmail: '',
        address: '',
        city: '',
        state: '',
        zipCode: ''
      });
    case ApiConstants.POST_PROJECT_SUBMISSIONS_ERROR:
          return Object.assign({}, state, {error: action.error});     
    default:
      return state;
  }
}

export default CreateProjectModalReducer;
