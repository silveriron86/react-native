import {
    combineReducers
} from "redux";
import SearchReducer from "./SearchReducer";
import LikesReducer from "./LikesReducer";
import ProjectsReducer from "./ProjectsReducer";
import CreateProjectModalReducer from "./CreateProjectModalReducer";

export default combineReducers({
    SearchReducer,
    LikesReducer,
    ProjectsReducer,
    CreateProjectModalReducer
})