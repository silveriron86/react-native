import ApiConstants from '../constants/ApiConstants';

const initialState = {
  likesListData: {},
  likesListError: null
};

function LikesReducer(state = initialState, action) {
  switch (action.type) {
    case ApiConstants.RESET_APP:
      return initialState;
      
    case ApiConstants.GET_LIKES_LIST_SUCCESS:
      return Object.assign({}, state, {
        likesListError: null,
        likesListData: action.response
      });      

    case ApiConstants.GET_LIKES_LIST_ERROR:
      return Object.assign({}, state, {
        likesListError: action.error,
        likesListData: {}
      }); 
    
    // case ApiConstants.POST_LIKES_SUCCESS:
    //   let item = action.response.item;  
    //   let data = JSON.parse(JSON.stringify(state.likesListData));

    //   let isNew = true;
    //   for(let i=0; i<data.length; i++) {
    //     if(data[i].id === item.id) {
    //       isNew = false;
    //       data.splice(i, 1);
    //       break;
    //     }
    //   }

    //   if(isNew){
    //     item.liked = true;
    //     data.push(item);
    //   }

    //   return Object.assign({}, state, {
    //     likesListError: null,
    //     likesListData: data
    //   }); 
    default:
      return state;
  }
}

export default LikesReducer;
