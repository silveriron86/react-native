import ApiConstants from '../constants/ApiConstants';

const initialState = {
  projectsListData: {},
  projectsListError: null,
  projectsOrgsList: null,
  projectsOrgsListError: null,
  submissionsTotalHours: '...',
  showLoaderOverlay: false,
};

function ProjectsReducer(state = initialState, action) {
  console.log(`--action.response--:ProjectsReducer.js:${JSON.stringify(action.response)}`);

  switch (action.type) {
    case ApiConstants.RESET_APP:
      return initialState;
      
    case ApiConstants.GET_PROJECTS_LIST_SUCCESS:
      return Object.assign({}, state, {
        projectsListError: null,
        projectsListData: action.response
      });      

    case ApiConstants.GET_PROJECTS_LIST_ERROR:
      return Object.assign({}, state, {
        projectsListError: action.error,
        projectsListData: {}
      }); 
    
    case ApiConstants.POST_PROJECT_LIKES_SUCCESS:
      let id = action.response.id;
      let data = JSON.parse(JSON.stringify(state.projectsListData));
      for(let i=0; i<data.length; i++) {
        if(data[i].id === id) {
          data[i].liked = !data[i].liked;
          break;
        }
      }
      return Object.assign({}, state, {
        projectsListError: null,
        projectsListData: data
      });
      
    case ApiConstants.GET_PROJECTS_ORG_BY_ID_SUCCESS:
      // return Object.assign({}, state, {projectsOrgsList: action.response, projectsOrgsListError: null, projectsListData: {}, projectsHoursTotal: }); 
      return Object.assign({}, state, {projectsOrgsList: action.response.items, projectsOrgsListError: null, projectsListData: {}, submissionsTotalHours: action.response.submissionsTotalHours}); 

    case ApiConstants.GET_PROJECTS_ORG_BY_ID_ERROR:
      return Object.assign({}, state, {projectsOrgsList: null, projectsOrgsListError: action.error, projectsListData: {}});
    case ApiConstants.SHOW_LOADER_OVERLAY:
        return Object.assign({}, state, {...state, showLoaderOverlay: true});
    case ApiConstants.HIDE_LOADER_OVERLAY:
        return Object.assign({}, state, {...state, showLoaderOverlay: false});
      
    default:
      return state;
  }
}

export default ProjectsReducer;
