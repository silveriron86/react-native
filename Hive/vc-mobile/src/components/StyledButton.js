import React from 'react';
import {
  StyleSheet,
} from 'react-native';
import { Button } from 'react-native-elements';
import Colors from '../constants/Colors';

export const PrimaryButton = (props) => (
  <Button
    title={props.title}
    titleStyle={styles.defaultButtonTitle}
    buttonStyle={[styles.defaultButton, props.style]}
    onPress={props.onPress}
  />
)

export const TextButton = (props) => (
  <Button
    title={props.title}
    titleStyle={styles.textButtonTitle}
    buttonStyle={styles.textButton}
    onPress={props.onPress}
  />
)

const styles = StyleSheet.create({
  defaultButtonTitle: {
    fontSize: 16,
    fontFamily: 'Gotham-Medium',
    color: 'white',
  },
  defaultButton: {
    backgroundColor: Colors.tintColor,
    borderRadius: 8,
    width: 250,
    height: 40,
  },
  textButtonTitle: {
    fontSize: 16,
    fontFamily: 'GothamRounded-Medium',
    color: Colors.tintColor,
  },
  textButton: {
    backgroundColor: 'transparent',
    borderWidth: 0,
  }
});