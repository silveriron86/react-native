import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';

const triangleIcon = require('../../assets/icons/tooltip_triangle.png');
const infoIcon = require('../../assets/icons/tooltip.png');
import { GothamNotRoundedBookText } from './StyledText';

export default class Tooltip extends React.Component {  
  render() {
    const { text, visible, iconStyle, onToggle } = this.props;
    
    return (
        <>
            {
                visible &&
                <View style={styles.ttWrapper}>
                    <TouchableWithoutFeedback onPress={onToggle}>
                        <View style={styles.ttBG}>
                        <GothamNotRoundedBookText style={styles.ttText}>{text}</GothamNotRoundedBookText>
                        <Image source={triangleIcon} style={styles.triangleIcon}/>
                        </View>            
                    </TouchableWithoutFeedback>
                </View>
            }
            <TouchableOpacity style={[styles.infoBtn, iconStyle]} onPress={onToggle}>
                <Image source={infoIcon} style={styles.infoIcon}/>
            </TouchableOpacity>
        </>
    )
  }
};
  
const styles = StyleSheet.create({
  infoBtn: {
    position: 'absolute',
    marginTop: 1,
    left: 0,
    width: 30,
    height: 36,
    justifyContent: 'center'
  },  
  infoIcon: {
    width: 17,
    height: 17
  },
  ttWrapper: {
    position: 'absolute',
    width: '100%',
    marginBottom: 8,
    bottom: 50
  },
  ttBG: {
    width: '100%',
    borderRadius: 3,
    borderBottomLeftRadius: 0,
    backgroundColor: '#FAFDFF',
    shadowColor: '#CACACA',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 4
  },
  ttText: {
    fontSize: 14,
    color: '#303030',
    lineHeight: 17,
    paddingHorizontal: 18,
    paddingTop: 12,
    paddingBottom: 10
  },
  triangleIcon: {
    position: 'absolute',
    bottom: -15,
    left: -4
  }
});