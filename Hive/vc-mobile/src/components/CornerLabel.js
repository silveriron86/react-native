import React from "react";
import {
    StyleSheet,
    View,
    Text,
} from "react-native";

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        justifyContent: "flex-end",
    },
    label: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "red",
        opacity: 0.3,
    },
    text: {
        color: "#fff",
        fontSize: 12,
    },
});

class CornerLabel extends React.Component {

    static defaultProps = {
        alignment: "left",
    };

    constructor(props) {
        super(props);
        this.state = {};
        const cornerRadius = 80;
        this._labelHeight = Math.sqrt(Math.pow(cornerRadius, 2) / 4);
        this._labelWidth = this._labelHeight * 6;
        let originOffset = Math.sqrt(Math.pow(this._labelHeight / 2, 2) / 0.25);
        let labelHorizontalPosition = -this._labelWidth / 2 + originOffset;
        let labelVerticalPosition = -this._labelHeight / 2 + originOffset;
        if (props.alignment === "left") {
            this._labelPosition = {left: labelHorizontalPosition, top: labelVerticalPosition};
            this._labelTransform = {transform: [{rotate: "-45deg"}]};
        }
        else {
            this._labelPosition = {right: labelHorizontalPosition, top: labelVerticalPosition};
            this._labelTransform = {transform: [{rotate: "45deg"}]};
        }
    }

    render() {
        return (
            <View style={[styles.container,
                this._labelPosition,
                this._labelTransform,
                {width: this._labelWidth, height: this._labelHeight,},
            ]}>
                <View style={[styles.label,
                    {height: this._labelHeight},
                    this.props.style,
                ]}>
                     {this._renderChildren()}
                 </View>
            </View>

        );
    }

    _renderChildren() {
        return React.Children.map(this.props.children, (child) => {
            if (!React.isValidElement(child)) {
                return <Text style={[styles.text, this.props.textStyle]}>{child}</Text>;
            }
            return child;
        });
    }

}
export default CornerLabel;