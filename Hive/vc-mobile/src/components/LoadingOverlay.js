import React from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  ActivityIndicator
} from 'react-native';

export default LoadingOverlay = (props) => {
  const { loading } = props;
  
  return loading ? 
    (
        <View style={styles.loading}>
            <ActivityIndicator size={'large'} color='#FFFFFF'/>
        </View>
    ) : null
};
  
const styles = StyleSheet.create({
    loading: {
        position: 'absolute',
        borderWidth: 1,
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "rgba(0, 0, 0, 0.3)",
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        zIndex: 99999
    }
});