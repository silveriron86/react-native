import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';

export default SocialButton = (props) => {
  const { type, onPress } = props;
  let image = require('../../assets/icons/social/facebook.png');
  switch(type) {
    case 'twitter':
      image = require('../../assets/icons/social/twitter.png');
      break;
    case 'google':
      image = require('../../assets/icons/social/google.png');
      break;
    case 'email':
      image = require('../../assets/icons/social/email.png');
      break;
    case 'instagram':
      image = require('../../assets/icons/social/instagram.png');
      break;
    case 'snapchat':
      image = require('../../assets/icons/social/snapchat.png');
      break;
    case 'facebook_blue':
        image = require('../../assets/icons/social/facebook_blue.png');
        break;
    case 'google_blue':
        image = require('../../assets/icons/social/google_blue.png');
        break;
    case 'instagram_blue':
        image = require('../../assets/icons/social/instagram_blue.png');
        break;
    case 'snapchat_blue':
        image = require('../../assets/icons/social/snapchat_blue.png');
        break;
    case 'email_blue':
        image = require('../../assets/icons/social/email_blue.png');
        break;
  }
  return (
    <TouchableOpacity onPress={onPress} style={styles.button}>
      <Image
        style={styles.image}
        source={image}
      />
    </TouchableOpacity>
  )
};

const styles = StyleSheet.create({
  image: {
    width: 40,
    height: 38,
  },
  button: {
    borderRadius: 20,
  }
});