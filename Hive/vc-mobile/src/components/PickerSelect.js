import React from 'react';
import {
  StyleSheet,
  View,
  Image
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

const bArrow = require('../../assets/icons/bottom_arrow.png');

export default PickerSelect = (props) => {
  const { placeholder, items, onChange, value } = props;
  
  return (
    <View style={styles.selectWrapper}>
      <RNPickerSelect
        placeholder={{
          label: placeholder,
          value: null,
          color: '#606060'
        }}
        placeholderTextColor="#606060"
        items={items}
        onValueChange={(value) => onChange(value)}
        style={{
          paddingLeft: 13,
          inputAndroid: {
            backgroundColor: 'transparent',
          },
          iconContainer: {
            top: 5,
            right: 12,
          },
        }}
        value={value}
        useNativeAndroidPickerStyle={false}
        textInputProps={{ underlineColorAndroid: 'cyan' }}
        Icon={() => {
          return <Image source={bArrow}/>
        }}
      />
    </View>    
  )
};
  
const styles = StyleSheet.create({
  selectWrapper: {
    width: '100%',
    height: 50,
    paddingLeft: 12,
    paddingTop: 20,
    borderBottomColor: 'gray', 
    borderBottomWidth: 1.5
  }
});