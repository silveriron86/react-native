import React from 'react';
import {
  View,
  StyleSheet
} from 'react-native';
import DatePicker from 'react-native-datepicker';

const dateIcon = require('../../assets/icons/calendar.png');
const timeIcon = require('../../assets/icons/clock.png');
import Tooltip from './Tooltip';

export default class DateInput extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      isVisibleTooltip: false
    }
  }

  toggleTooltip = () => {
    this.setState({
      isVisibleTooltip: !this.state.isVisibleTooltip
    });
  }

  hideTooltip = () => {
    this.setState({
      isVisibleTooltip: false
    })
  }

  render() {
    const { date, placeholder, onChange, style, placeholderTextColor, noIcon, noLeftPad, mode, format, tooltip } = this.props;
    const { isVisibleTooltip } = this.state;
    
    return (
      <View style={[
          styles.wrapper, 
          {paddingHorizontal: style ? 0: 20}
      ]}>
        <View style={{width: '100%', position: 'relative'}}>
          <>           
            <DatePicker
              style={[styles.datepicker, style]}
              date={date}
              mode={mode ? mode : 'date'}
              placeholder={placeholder}
              format={format ? format : "MM-DD-YYYY"}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              iconSource={mode === 'time' ? timeIcon : dateIcon}
              customStyles={{
                dateIcon: noIcon ? {width: 0, height: 0} : {
                  tintColor: '#1D75BD',
                  width: 22,
                  height: 22,
                  right: 5,
                  position: 'absolute'               
                },
                dateInput: {
                  marginLeft: 0,
                  borderWidth: 0,
                  borderBottomWidth: 2,
                  borderBottomColor: '#606060',
                  alignItems: 'flex-start',    
                  paddingLeft: tooltip ? 31 : 13,
                  height: 50
                },
                dateText:{
                  textAlign: 'left'
                },
                placeholderText: {
                  color: placeholderTextColor ? placeholderTextColor : '#8E8E8E',
                  fontSize: 14,
                  textAlign: 'left',
                  position: 'absolute',
                  left: tooltip ? 28 : 10
                }
              }}
              onOpenModal={this.hideTooltip}
              onPressMask={this.hideTooltip}
              onDateChange={(date) => {onChange(date)}}
            />
            {
              tooltip && 
              <Tooltip text={tooltip} visible={isVisibleTooltip} onToggle={this.toggleTooltip}/>
            }             
          </>
        </View>
      </View>
    )
  }
};
  
const styles = StyleSheet.create({
  calendarIcon: {
    tintColor: '#1D75BD',
    width: 30,
    height: 30
  },
  wrapper: {
    width: '100%',
    position: 'relative',
    alignItems: 'center',
    marginTop: 0
  },
  datepicker: {
    width: '100%',
    height: 50,
    alignSelf: 'center'
  },
  infoBtn: {
    position: 'absolute',
    marginTop: 1,
    left: 0,
    width: 30,
    height: 36,
    justifyContent: 'center'
  },  
  infoIcon: {
    width: 17,
    height: 17
  },
  ttWrapper: {
    position: 'absolute',
    width: '100%',
    marginBottom: 8,
    bottom: 50
  },
  ttBG: {
    width: '100%',
    borderRadius: 3,
    borderBottomLeftRadius: 0,
    backgroundColor: '#FAFDFF',
    shadowColor: '#CACACA',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 4
  },
  ttText: {
    fontSize: 14,
    color: '#303030',
    lineHeight: 17,
    paddingHorizontal: 18,
    paddingTop: 12,
    paddingBottom: 10
  },
  triangleIcon: {
    position: 'absolute',
    bottom: -15,
    left: -4
  }
});