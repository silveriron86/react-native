import React from 'react';
import { View, StyleSheet, Platform, TouchableHighlight, Text } from 'react-native';

class DefaultMarker extends React.Component {
  render() {
    return (
      <TouchableHighlight>
        <View
          style={
            this.props.enabled
              ? [
                  styles.markerStyle,
                  this.props.markerStyle,
                  this.props.pressed && styles.pressedMarkerStyle,
                  this.props.pressed && this.props.pressedMarkerStyle,
                ]
              : [styles.markerStyle, styles.disabled]
          }
        >
          <Text style={styles.value}>{this.props.currentValue}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  markerStyle: {
    height: 24,
    width: 24,
    borderRadius: 12,
    borderWidth: 3,
    borderColor: '#CACACA50',
    backgroundColor: '#FFFFFF',
    overflow: 'visible'
  },
  pressedMarkerStyle: {
    ...Platform.select({
      ios: {},
      android: {
        height: 20,
        width: 20,
        borderRadius: 20,
      },
    }),
  },
  disabled: {
    backgroundColor: '#d3d3d3',
  },
  value: {
    color: '#CACACA',
    fontSize: 10,
    marginTop: 21,
    position: 'absolute',
    width: 16,
    textAlign: 'center'
  }
});

export default DefaultMarker;
