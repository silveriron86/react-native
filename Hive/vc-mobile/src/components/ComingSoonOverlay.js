import React from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

import { GothamMediumText, GothamNotRoundedBookText } from './StyledText';

export default ComingSoonOverlay = (props) => {
  return (
    <View style={[styles.comingSoonOverlay, props.style]}>
      <GothamMediumText style={styles.comingSoonTitle}>
          COMING <GothamMediumText style={{color: '#8DC63F'}}>SOON!</GothamMediumText>
      </GothamMediumText>
      <GothamNotRoundedBookText style={styles.comingSoonDescription}>New features are in the works and we’re exciting to share them with our users.</GothamNotRoundedBookText>
      <GothamNotRoundedBookText style={{paddingTop: 20}}>Keep checking back!</GothamNotRoundedBookText>
    </View>
  )
};
  
const styles = StyleSheet.create({
  comingSoonOverlay: {
    backgroundColor: 'rgba(255, 255, 255, 0.85)',        
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 14,
    alignItems: 'center',
    zIndex: 9999
  },
  comingSoonTitle: {
      color: '#1D75BD',
      fontSize: 32,
      marginTop: 98
  },
  comingSoonDescription: {
      marginTop: 10,
      color: '#303030',
      fontSize: 14,
      lineHeight: 22,
      paddingHorizontal: 20,
      textAlign: 'center'
  }
});

