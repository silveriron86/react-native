import React from 'react';
import { Text, TextInput } from 'react-native';

export class GothamMediumText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'GothamRounded-Medium' }]} />;
  }
}

export class GothamBookText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'GothamRounded-Book' }]} />;
  }
}

export class GothamBoldText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'GothamRounded-Bold' }]} />;
  }
}

export class GothamNotRoundedBookText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'Gotham-Book' }]} />;
  }
}

export class GothamNotRoundedMediumText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'Gotham-Medium' }]} />;
  }
}

export class QueenClubsText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'Queen of Clubs' }]} />;
  }
}

export class GothamBookTextInput extends React.Component {
    render() {
        return <TextInput {...this.props} style={[this.props.style, { fontFamily: 'GothamRounded-Book' }]} />;
    }
}

export class GothamNotRoundedMediumTextInput extends React.Component {
  render() {
      return <TextInput {...this.props} style={[this.props.style, { fontFamily: 'Gotham-Medium' }]} />;
  }
}