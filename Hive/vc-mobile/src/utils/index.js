import DeviceInfo from 'react-native-device-info';
import env from 'react-native-config';
import Moment from 'moment';

let Utils = {
  age: (birthday) => {
    let birth = new Date(birthday);
    let now = new Date();
    let beforeBirth = ((() => {birth.setDate(now.getDate());birth.setMonth(now.getMonth()); return birth.getTime()})() < birth.getTime()) ? 0 : 1;
    return now.getFullYear() - birth.getFullYear() - beforeBirth;
  },
	validateEmail: (email) => {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	},  	
  serialize: (obj) => {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  },
  nativeBaseDoesHeaderCorrectlyAlready() {
    // if (!DeviceInfo.hasNotch()) {
    //   return true
    // }
    
    const model = DeviceInfo.getModel()

    switch (model) {
      case "iPhone X":
      case "iPhone XS":
      case "iPhone XR":
      case "iPhone XS Max":
        return true
    }
    return false
  },

  formatDate: (date, validFormat) => Moment(date).format(validFormat),

  newDateObject: (theTime, theDate) => {

    // ***** CONVERT TIME LOG FORMAT TO DATE OBJECT 
    // theTime = "05:24"
    // theDate = "2019-07-12"
  
    // Date(year, month, day, hours, minutes, seconds, milliseconds)
    const year = theDate.split("-")[0]
    const month = theDate.split("-")[1]
    const day = theDate.split("-")[2]
    const hours = theTime.split(":")[0]
    const minutes = theTime.split(":")[1]
    const seconds = 0
    const milliseconds = 0
  
    const dateObject = new Date(year, month, day, hours, minutes, seconds, milliseconds)
  
    return dateObject
  },  
  
  hoursBetweenDates: (startDateTime, endDateTime) => {
    var difference = endDateTime.valueOf() - startDateTime.valueOf();
    var differenceHours = difference/1000/60/60; // Convert milliseconds to hours
    const decimals = 1;
    return +(Math.round(differenceHours + "e+" + decimals) + "e-" + decimals);
  }
    


};

export default Utils;
