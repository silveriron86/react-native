const CAUSES = [
    {
      name: "Animals",
      image: require('../../assets/icons/causes/animals.png')
    },
    {
      name: "Arts & Culture",
      image: require('../../assets/icons/causes/arts_culture.png')
    },
    {
      name: "Community",
      image: require('../../assets/icons/causes/community.png')
    },
    {
      name: "Disaster Relief",
      image: require('../../assets/icons/causes/disaster_relief.png')
    },
    {
      name: "Education",
      image: require('../../assets/icons/causes/education.png')
    },
    {
      name: "Elderly",
      image: require('../../assets/icons/causes/elderly.png')
    },
    {
      name: "Environment",
      image: require('../../assets/icons/causes/environment.png')
    },
    {
      name: "Health",
      image: require('../../assets/icons/causes/health.png')
    },
    {
      name: "Homeless",
      image: require('../../assets/icons/causes/homeless.png')
    },
    {
      name: "Hunger",
      image: require('../../assets/icons/causes/hunger.png')
    },
    {
      name: "Int'l",
      image: require('../../assets/icons/causes/intl.png')
    },
    {
      name: "Lgbtq",
      image: require('../../assets/icons/causes/lgbtq.png')
    },  
    {
      name: "Mental Health",
      image: require('../../assets/icons/causes/mental_health.png')
    },
    {
      name: "Military",
      image: require('../../assets/icons/causes/military.png')
    },
    {
      name: "Refugee",
      image: require('../../assets/icons/causes/refugee.png')
    },
    {
      name: "Religion",
      image: require('../../assets/icons/causes/religion.png')
    },
    {
      name: "Sports",
      image: require('../../assets/icons/causes/sports.png')
    },
    {
      name: "Special Needs",
      image: require('../../assets/icons/causes/special_needs.png')
    },
    {
      name: "STEM",
      image: require('../../assets/icons/causes/research.png')
    },
    {
      name: "Youth",
      image: require('../../assets/icons/causes/children.png')
    },
    {
      name: "",
      image: null
    }
  ];

  export default {CAUSES};