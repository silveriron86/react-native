const tintColor = '#1D75BD';

export default {
  tintColor,
  greyColor: '#a6a6a6',
  tabIconDefault: '#ccc',
  commonPageBackground: '#F8F8F8',
  settingsBackground: '#F1F1F1',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  white: '#fff',
  black: '#000',
  profileNameText: '#7FCFED',
  transcriptNameText: '#8DC442',
  dateTimeTileText: '#8DC63F',
  titleText: '#303030',
  detailsText: '#777777',
  deepSkyBlue: '#24B6EC',
  lightGrey: '#CACACA',
};
