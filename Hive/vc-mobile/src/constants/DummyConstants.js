module.exports = {
  TEMP_USER_ID: '452CE3D8-5912-4EAB-8315-01E0F33C32A7',
  filterList: [
    {
      id: 1,
      title: 'Dog & Kitty City1',
      description: 'Care for shelter cats. Play with brush, feed, and love cats. Photog…',
      image: 'https://www.gstatic.com/webp/gallery/1.jpg',
      location: 'Dallas, TX',
      date: 'Sep 10, 2019',
      time: '8-6pm',
      rating: 5,
      liked: true,
      type: require('../../assets/icons/causes/animals.png')
    },
    {
      id: 2,
      title: 'Elderly Care2',
      description: 'Volunteer with care for the elderly. Read, play games, cook, organize…',
      image: 'https://www.gstatic.com/webp/gallery/2.jpg',
      location: 'Dallas, TX',
      rating: 3.5,
      liked: true,
      type: require('../../assets/icons/causes/elderly.png')
    },
    {
      id: 3,
      title: 'Kids that Kare3',
      description: 'What’s more fun than kids? Start from 2yr and up. Games, meals, …',
      image: 'https://www.gstatic.com/webp/gallery/3.jpg',
      location: 'Dallas, TX',
      date: 'Jan 10, 2019',
      time: '8-6pm',
      rating: null,
      type: require('../../assets/icons/causes/children.png')
    }
  ],

  likesList: [
    {
      id: 1,
      title: 'Dog & Kitty City1',
      description: 'Care for shelter cats. Play with brush, feed, and love cats. Photog…',
      image: 'https://www.gstatic.com/webp/gallery/1.jpg',
      location: 'Dallas, TX',
      date: 'Sep 10, 2019',
      time: '8-6pm',
      rating: 5,
      liked: true,
      type: require('../../assets/icons/causes/animals.png')
    },
    {
      id: 2,
      title: 'Elderly Care2',
      description: 'Volunteer with care for the elderly. Read, play games, cook, organize…',
      image: 'https://www.gstatic.com/webp/gallery/2.jpg',
      location: 'Dallas, TX',
      rating: 3.5,
      liked: true,
      type: require('../../assets/icons/causes/elderly.png')
    }
  ],

  projectsList: [
    {
      id: 3,
      title: 'Dog & Kitty City3',
      description: 'Care for shelter cats. Play with brush, feed, and love cats. Photog…',
      image: 'https://www.gstatic.com/webp/gallery/1.jpg',
      location: 'Dallas, TX',
      date: 'Sep 10, 2019',
      time: '8-6pm',
      rating: 5,
      liked: false,
      status: 'CHECK-IN',
      type: require('../../assets/icons/causes/animals.png')
    },
    {
      id: 4,
      title: 'Elderly Care4',
      description: 'Volunteer with care for the elderly. Read, play games, cook, organize…',
      image: 'https://www.gstatic.com/webp/gallery/2.jpg',
      location: 'Dallas, TX',
      rating: 3.5,
      date: 'Sep 10, 2019',
      time: '8-6pm',      
      liked: false,
      status: 'SHIFT CONFIRMED',
      type: require('../../assets/icons/causes/elderly.png')
    },
    {
      id: 5,
      title: 'Kids that Kare5',
      description: 'What’s more fun than kids? Start from 2yr and up. Games, meals, …',
      image: 'https://www.gstatic.com/webp/gallery/3.jpg',
      location: 'Dallas, TX',
      date: 'Jan 10, 2019',
      time: '8-6pm',
      rating: 4,
      liked: false,
      status: 'COMPLETED',
      type: require('../../assets/icons/causes/children.png')
    }    
  ],

  leaderBoardList: [
    {
      location: 'Amy, New York',
      score: 723
    },
    {
      location: 'Steven Dallas TX',
      score: 650
    },
    {
      location: 'Mike, Tustin AZ',
      score: 478
    },
    {
      location: 'Sarah, Jacksonville FL',
      score: 423
    },
    {
      location: 'Jessica, Atlanta GA',
      score: 415
    },
    {
      location: 'Kevin, Washington DC',
      score: 320
    }
  ],
  transcriptList: [
    {
      id: 1,
      title: 'Kids That Kare',
      date: 'Sept 10, 2018',
      hours: 4,
      status: 'pending',
      verified: false,
      type: require('../../assets/icons/causes/animals.png'),
      totalHours: 69,
      label: 'Animals'
    },
    {
      id: 2,
      title: 'TeenLife FEstival',
      date: 'Aug 20, 2018',
      hours: 4.5,
      verified: true,
      type: require('../../assets/icons/causes/mental_health.png'),
      totalHours: 41,
      label: 'Mental Health'
    },
    {
      id: 3,
      title: 'Navy Thrift Store',
      date: 'July 3, 2018',
      hours: 2,
      verified: false,
      type: require('../../assets/icons/causes/military.png'),
      totalHours: 24,
      label: 'Military'
    },
    {
      id: 4,
      title: 'Second Harvest Food Bank',
      date: 'June 12, 2018',
      hours: 2.5,
      verified: true,
      type: require('../../assets/icons/causes/special_needs.png'),
      totalHours: 12,
      label: 'Elderly'
    },
    {
      id: 5,
      title: 'Parents for Parents',
      date: 'May 5, 2018',
      hours: 3.5,
      verified: false,
      type: require('../../assets/icons/causes/elderly.png'),
      totalHours: 2,
      label: 'Disaster Relief'
    },
    {
      id: 6,
      title: 'Grown & Learn',
      date: 'Apr 18, 2018',
      hours: 5,
      verified: true,
      type: require('../../assets/icons/causes/environment.png'),
      totalHours: 1,
      label: 'Health'
    }
  ],
  myReviewsList: [
    {
      id: 1,
      title: 'TeenLife Festival',
      date: 'Oct 11, 2018',
      user: 'Antonia Lee',
      role: 'project Coordinator',
      review: 'Since it was a big event, I wasn’t able to talk with the volunteers one-on-one. But Jennifer was a useful addition to the team and helped the registration booth run smoothly.'
    },
    {
      id: 2,
      title: 'Second Harvest Food Bank',
      date: 'Aug 20, 2018',
      user: 'peter davies',
      role: 'organization owner',
      review: 'We loved having Jennifer on our team for the few hours she was available. There was a lot of information, but she took to it quickly and was able to interact and work with others well.'
    },
    {
      id: 3,
      title: 'Parents For Parents',
      date: 'May 3, 2018',
      user: 'Ramiro Alvarado',
      role: 'project Coordinator',
      review: 'We appreciated having her participate in our event. Hopefully if she has the time next year, we would enjoy having her help us again.'
    }
  ]
};