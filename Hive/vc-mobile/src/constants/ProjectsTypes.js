const TYPES = [
    {
        label: 'Abroad',
        value: 'Abroad',
    },
    {
        label: 'Campaign',
        value: 'Campaign',
    },
    {
        label: 'Drive',
        value: 'Drive',
    },
    {
        label: 'Education',
        value: 'Education',
    },
    {
        label: 'Event',
        value: 'Event',
    },
    {
        label: 'Virtual Volunteering',
        value: 'Volunteering',
    },
    {
        label: 'Volunteer Internship',
        value: 'Internship',
    },
];

export default TYPES; 