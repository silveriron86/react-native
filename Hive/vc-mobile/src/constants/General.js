export default {
    BUILD_VERSION: '2.001',
    PASSWORD_COMPLEXITY_PHRASE: 'Passwords must be at least 12 characters long.',
    SHARE_URL_FROM_INVITE_SCR: 'https://apple.co/2Ko8gJ3'
}