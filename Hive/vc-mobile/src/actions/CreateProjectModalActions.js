import ApiConstants from '../constants/ApiConstants';
import { AsyncStorage } from 'react-native';
import axios from 'axios';
import DummyConstants from '../constants/DummyConstants';

let CreateProjectModalActions = {

  resetApp: function(response) {
    return {
      response,
      type: ApiConstants.RESET_APP
    };
  },  

};

export default CreateProjectModalActions;