import ApiConstants from '../constants/ApiConstants';
import { AsyncStorage } from 'react-native';
import axios from 'axios';
import DummyConstants from '../constants/DummyConstants';
import Config from 'react-native-config'

let SearchActions = {
  
  resetApp: function(response) {
    console.debug("SearchActions:resetApp");
    return {
      response,
      type: ApiConstants.RESET_APP
    };
  },
  
  getFilterListError: function(error) {
    return {
      error,
      type: ApiConstants.GET_FILTER_LIST_ERROR
    };
  },

  getFilterListSuccess: function(response) {
    return {
      response,
      type: ApiConstants.GET_FILTER_LIST_SUCCESS
    };
  },

  getFilterList: function(data, cb) {
    return dispatch => {      
      AsyncStorage.getItem('oio_auth', (err, auth) => {
        
        let logData = Config.API_HOST
          + 'projects/search'
          + JSON.stringify(data)
          console.debug(`URL ------------ ${Config.API_HOST}`);
        console.log(`--getFilterList-- ${logData}`)

        data.sfContactId = JSON.parse(auth).user.sfContactId

        let options = {
          method: 'POST',
          url: Config.API_HOST + 'projects/search',
          headers: {
            "Authorization": JSON.parse(auth).token,
            "Content-Type": "application/json",
            "cache-control": "no-cache"
          },
          data: data
        }
  
        axios(options).then(response => {
          if(response.status === 200) {
            // console.log(`--SearchActions.js:response.data--${JSON.stringify(response.data)}`)
            // console.log(`--SearchActions.js:response.data.projects--${JSON.stringify(response.data.projects)}`)
            dispatch(this.getFilterListSuccess(response.data.projects));
            cb(response.data);
          }
        })
        .catch(error => {
          console.log(error.response.data);
          if(error.response.data.code === "AUTHORIZATION_REQUIRED") {
            // logout
          }
          dispatch(this.getFilterListError(error.response.data));
          cb(error.response.data);
        })
      });      
    };    
  },

  getProjectError: function(error) {
    return {
      error,
      type: ApiConstants.GET_PROJECT_ERROR
    };
  },

  getProjectSuccess: function(response) {
    return {
      response,
      type: ApiConstants.GET_PROJECT_SUCCESS
    };
  },  
  getProject: function(id, cb) {
    return dispatch => {      
      AsyncStorage.getItem('oio_auth', (err, auth) => {
        let data = JSON.parse(auth);
        let options = {
          method: 'GET',
          url: Config.API_HOST + 'projects/' + id,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': data.token
          }
        }
  
        axios(options).then(response => {
          if(response.status === 200) {
            console.log(response.data)
            dispatch(this.getProjectSuccess(response.data.projects));
            cb(response.data);
          }
        })
        .catch(error => {
          dispatch(this.getProjectError(error.response.data));
          cb(error.response.data);
        })
      });      
    };  
  },

  postLikesError: function(error) {
    return {
      error,
      type: ApiConstants.POST_LIKES_ERROR
    };
  },

  postLikesSuccess: function(response) {
    // return;
    return {
      response,
      type: ApiConstants.POST_LIKES_SUCCESS
    };
  },

  postLikes: function(item, cb){

    return dispatch => {      
      AsyncStorage.getItem('oio_auth', (err, auth) => { 
        const sfContactId = JSON.parse(auth).user.sfContactId;
        const isAlreadyLiked = item.sfProjectId ? item.isProjectLikedByUser : item.isOrgLikedByUser
        console.log({
          logSource: '---SearchActions:postLikes',
          sfOrgId: item.sfOrgId,
          sfProjectId: item.sfProjectId,
          sfContactId: sfContactId,
          isAlreadyLiked: isAlreadyLiked,
        });
        console.log(Config.API_HOST + 'likes');
        axios({
          method: isAlreadyLiked ? 'DELETE' : 'POST',
          url: Config.API_HOST + 'likes',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            sfOrgId: item.sfOrgId,
            sfProjectId: item.sfProjectId,
            sfContactId: sfContactId,
          }
        }).then(response => {
          if(response.status === 200) {
            console.log(`---SearchActions:postLikes:success:response.data:${JSON.stringify(response.data)}`)
            dispatch(this.postLikesSuccess(response.data.projects));
            cb(response.data);
          }
        })
        .catch(error => {
          // dispatch(this.postLikesError(error.response.data));
          // cb(error.response.data);
          dispatch(this.postLikesError('error caught: SearchActions.postLikes'));
          cb('error caught: SearchActions.postLikes');
        })
      });      
    };        
  },

  
  postSubscriptionsSuccess: function(response) {
    return {
      response,
      type: ApiConstants.POST_SUBSCRIPTIONS_SUCCESS
    };
  },

  postSubscriptionsError: function(response) {
    return {
      response,
      type: ApiConstants.POST_SUBSCRIPTIONS_ERROR
    };
  },

  postSubscriptions : function(sfOrgId, sfProjectId, sfContactId, isActive, type, token){
    let subscription = { sfContactId: sfContactId,
                         sfOrgId: sfOrgId,
                         isActive: isActive,
                         type: type
                       };
    if(sfProjectId){
      subscription = { sfContactId: sfContactId,
        sfOrgId: sfOrgId,
        isActive: isActive,
        sfProjectId: sfProjectId,
        type: type
      };
    }

    return dispatch => {
      axios({
        method: 'POST',
        url: Config.API_HOST + 'subscriptions',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token
        },
        data:{
          subscription: subscription,
          defaultPreferences : {
          enableEmail: false,
          enablePush: true,
          enableSMS: false
      } 
        }
      }).then(response => {
        if(response.status === 200) {
          console.debug("response",response.data);
          dispatch(this.postSubscriptionsSuccess(response.data));
        }
      })
      .catch(error => {
        console.debug(error);
        dispatch(this.postSubscriptionsError(error.response.data));
      });
    };
  }
};

export default SearchActions;