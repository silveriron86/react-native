import ApiConstants from '../constants/ApiConstants';
import { AsyncStorage } from 'react-native';
import axios from 'axios';
import DummyConstants from '../constants/DummyConstants';

let LikesActions = {

  resetApp: function(response) {
    return {
      response,
      type: ApiConstants.RESET_APP
    };
  },
  
  getLikesListError: function(error) {
    return {
      error,
      type: ApiConstants.GET_LIKES_LIST_ERROR
    };
  },

  getLikesListSuccess: function(response) {
    return {
      response,
      type: ApiConstants.GET_LIKES_LIST_SUCCESS
    };
  },

  getLikesList: function(cb){
    return dispatch => {
      let response = DummyConstants.likesList;
      dispatch(this.getLikesListSuccess(response));
      cb(response);			
    };    
  },  

  postLikesError: function(error) {
    return {
      error,
      type: ApiConstants.POST_LIKES_ERROR
    };
  },

  postLikesSuccess: function(response) {
    return {
      response,
      type: ApiConstants.POST_LIKES_SUCCESS
    };
  },

  postLikes: function(item, cb){
    return dispatch => {
      let response = {
        item: item
      };
      dispatch(this.postLikesSuccess(response));
      cb(response);	
    };    
  }
};

export default LikesActions;