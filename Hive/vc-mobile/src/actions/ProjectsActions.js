import ApiConstants from '../constants/ApiConstants';
import DummyConstants from '../constants/DummyConstants';
import axios from 'axios';
import Config from 'react-native-config';
import OrganizationsActions from '../actions/OrganizationsActions';
import Utils from '../utils';

const ProjectsActions = {

  resetApp: function(response) {
    return {
      response,
      type: ApiConstants.RESET_APP
    };
  },
  
  getProjectsListError: function(error) {
    return {
      error,
      type: ApiConstants.GET_PROJECTS_LIST_ERROR
    };
  },

  getProjectsListSuccess: function(response) {
    return {
      response,
      type: ApiConstants.GET_PROJECTS_LIST_SUCCESS
    };
  },

  getProjectsList: function(cb){
    return dispatch => {
      let response = DummyConstants.projectsList;
      dispatch(this.getProjectsListSuccess(response));
      cb(response);			
    };    
  },

  postLikesSuccess: function(response) {
    return {
      response,
      type: ApiConstants.POST_PROJECT_LIKES_SUCCESS
    };
  },

  postLikes: function(id, cb){
    return dispatch => {
      let response = {
        id: id
      };
      dispatch(this.postLikesSuccess(response));
      cb(response);	
    };    
  }, 
  

  postProjectError: function(error) {
    return {
      error,
      type: ApiConstants.POST_PROJECT_SUBMISSIONS_ERROR
    };
  },

  postProjectSuccess: function(response) {
    return {
      response,
      type: ApiConstants.POST_PROJECT_SUBMISSIONS_SUCCESS
    };
  },

  hideCongratsModal: function(response) {
    return {
      response,
      type: ApiConstants.HIDE_CONGRATS_MODAL
    }
  },

  hideLoaderOverlay: function(response) {
    return {
      response,
      type: ApiConstants.HIDE_LOADER_OVERLAY
    }
  },

  showLoaderOverlay: function(response) {
    return {
      response,
      type: ApiConstants.SHOW_LOADER_OVERLAY
    }
  },

  getProjectsOrgByUserIdSuccess: function(response) {
    return {
      response,
      type: ApiConstants.GET_PROJECTS_ORG_BY_ID_SUCCESS
    };
  },

  getProjectsOrgByUserIdError: function(response) {
    return {
      response,
      type: ApiConstants.GET_PROJECTS_ORG_BY_ID_ERROR
    };
  },

  //Create project Submission
  postCreateProjectSubmissions : function(request){
    const {token, sfContactId, orgTitle, projectTitle, projectCause, projectType, contactFirstName, contactLastName,
      contactEmail, address, city, state, zipCode, description, startDate, startTime, endDate, endTime, optInRatingReview,
      optInValidation, sfProjectId, sfOrgId} = request;
    console.debug(sfProjectId, sfOrgId);
    return dispatch => {
      axios({
        method: 'POST',
        url: Config.API_HOST + 'projectsubmissions',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token
        },
        data:{sfContactId: sfContactId,
          orgName: orgTitle,
          projectName: projectTitle,
          cause: projectCause.name,
          type: projectType,
          projectContactFirstName: contactFirstName,
          projectContactLastName: contactLastName,
          projectContactEmail: contactEmail,
          street: address,
          city: city,
          state: state,
          zipcode: zipCode,
          volunteerSummary: description,
          startDate: startDate, 
          startTime: startTime,
          endDate: endDate,
          endTime: endTime,
          optInRatingReview: optInRatingReview,
          optInValidation: optInValidation,
        },
      }).then(response => {
        if(response.status === 200) {
          dispatch(this.postProjectSuccess(response.data));
        }else{
          dispatch(this.postProjectError(response.data));
        }
      })
      .catch(error => {
        dispatch(this.postProjectError(error.response.data));
      });
    };
  },

  //Get projects submissions
  getProjectSubmissions : async function(request){
    const {userId, token} = request;
      return axios({
        method: 'GET',
        url: Config.API_HOST + 'accounts/' + userId,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token
        }
      }).then(response => {
        if(response.status === 200) {
          console.debug("response",response.data);
          return response.data;
        }
      });
  },
  
  //Get all subscriptions info
  postSubscriptionsSearchByContactId : async function(request){
    const { sfContactId, token } = request;
      return axios({
        method: 'POST',
        url: Config.API_HOST + 'subscriptions/search',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token
        },
        data:{
          sfContactIds: [sfContactId]
        }
      }).then(response => {
        if(response.status === 200) {
          console.debug("postSubscriptionsSearchByContactId:response:",response.data);
          return response.data;
        }
      });
  },

  //Get the project by SF Id
  getProject : async function(request){
    const { sfProjectId, sfContactId } = request;
    return axios({
        method: 'GET',
        url: Config.API_HOST + 'projects/' + sfProjectId + `?sfContactId=${sfContactId}`,
        headers: {
          'Content-Type': 'application/json',
        }
      }).then(response => {
        if(response.status === 200) {
          return response.data.project;
        }
      });
  },

  //Get projects subscriptions
  getProjectsById: async function(request) {
    const { projectSubscriptions, sfContactId } = request;
    let projects = [];
    //sorting subscription
    let subscriptions = projectSubscriptions.subscriptions.sort((a,b) => a.sfSubscriptionId > b.sfSubscriptionId ? -1 : a.sfSubscriptionId < b.sfSubscriptionId ? 1 : 0);
    for(item of subscriptions){
        if(item.sfProjectId){
          let project = await this.getProject({ sfProjectId: item.sfProjectId, sfContactId: sfContactId });
          projects.push(project);
        }
      }
    return projects;
  },

  //Get all projects subscriptions, project submissions and organizations subscriptions
  getProjectsOrgByUserId: function(request, callback){
    return async dispatch => {
      try{
        const {sfContactId, token, userId} = request;
        let projectSubmissions = [];
        let projectSubscriptions = [];
        let orgSubscriptions = [];
        let totalHours = 0;
        
        let projectSubmissionsRequest = await this.getProjectSubmissions(request);
        projectSubmissions = projectSubmissionsRequest.submittedProjects;

        let subscriptionsResponse = await this.postSubscriptionsSearchByContactId(request);
        projectSubscriptions = await this.getProjectsById({projectSubscriptions : subscriptionsResponse, sfContactId: sfContactId});

        orgSubscriptions = await OrganizationsActions.getOrganizationsById({projectSubscriptions : subscriptionsResponse, sfContactId: sfContactId});
        
        //sorting submissions results
        projectSubmissions = projectSubmissions.sort((a,b) => a.sfProjectSubmissionId > b.sfProjectSubmissionId ? -1 : a.sfProjectSubmissionId < b.sfProjectSubmissionId ? 1 : 0);

        projectSubmissions.forEach(item => {
          item.screenType = 'SUBMISSION';
          
          if (item.startTime && item.startDate && item.endTime && item.endDate) {
            const startDateTime = Utils.newDateObject(item.startTime, item.startDate)
            const endDateTime = Utils.newDateObject(item.endTime, item.endDate)
            const between = Utils.hoursBetweenDates(startDateTime, endDateTime)
            // console.log(`There are ${between} hours between ${startDateTime} and ${endDateTime}.`)  
            totalHours += between;
            // console.log(`Total hours is now: ${totalHours}`)
          }

        });
        projectSubscriptions.forEach((item) => {
          item.screenType = 'PROJECTSUBSCRIPTION';
        });    
        orgSubscriptions.forEach((item) => {
          item.screenType = 'ORGSUBSCRIPTION';
        });
        
        const projectsOrgsSubmissions = {
          'items':projectSubscriptions.concat(orgSubscriptions, projectSubmissions),
          'submissionsTotalHours':totalHours
        };
        // let result = projectSubscriptions.concat(orgSubscriptions, projectSubmissions);
        let result = projectsOrgsSubmissions;

        dispatch(this.getProjectsOrgByUserIdSuccess(result));
        callback(result);

      }catch(error){
        dispatch(this.getProjectsOrgByUserIdError(error));
        callback(error);
      }
    };
  }
};

export default ProjectsActions;