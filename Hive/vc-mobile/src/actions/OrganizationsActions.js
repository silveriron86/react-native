import ApiConstants from '../constants/ApiConstants';
import axios from 'axios';
import Config from 'react-native-config'

const OrganizationsActions = {

    resetApp: function(response) {
      return {
        response,
        type: ApiConstants.RESET_APP
      };
    },
  
    getOrganizationsByIdError: function(error) {
        return {
          error,
          type: ApiConstants.GET_ORG_BY_ID_ERROR
        };
      },
    
      getOrganizationsByIdSuccess: function(response) {
        return {
          response,
          type: ApiConstants.GET_ORG_BY_ID_SUCCESS
        };
      },

    getOrgById: async function(request){
      const {sfOrgId, sfContactId} = request;
      return axios({
                  method: 'GET',
                  url: Config.API_HOST + 'organizations/' + sfOrgId + `?sfContactId=${sfContactId}`,
                  headers: {
                    'Content-Type': 'application/json',
                  }
                }).then(response => {
                  if(response.status === 200) {
                    return response.data.organization
                  }
                });
    },

    getOrganizationsById: async function(request) {
        const { projectSubscriptions, sfContactId } = request;
        let organizations = [];
        //sorting subscriptions
        let subscriptions = projectSubscriptions.subscriptions.sort((a,b) => a.sfSubscriptionId > b.sfSubscriptionId ? -1 : a.sfSubscriptionId < b.sfSubscriptionId ? 1 : 0); 
        for(item of subscriptions){
          if(item.sfOrgId && !item.sfProjectId){
            let org = await this.getOrgById({sfOrgId: item.sfOrgId, sfContactId: sfContactId});
            organizations.push(org);
          } 
        }
        return organizations;
    }
};

export default OrganizationsActions;