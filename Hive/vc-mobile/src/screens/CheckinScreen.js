import React from 'react';
import {
  StyleSheet,
  View,
  Button,
  Image,
  ImageBackground,
} from 'react-native';
import {PrimaryButton} from "../components";

const imgLogo = require('../../assets/images/logo.png');
const imgBkg = require('../../assets/images/bkground2.jpg');
export default class CheckinScreen extends React.Component {
  render() {
    return (
      <ImageBackground source={imgBkg} style={{width: '100%', height: '100%'}} imageStyle={{opacity: 0.24}}>
        <View style={styles.container}>
          <Image style={styles.logo} source={imgLogo} />
          <PrimaryButton
            onPress={() => this.props.navigation.navigate('Login')}
            title="Sign in"
          />
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    opacity: 0.8,
  },  
  logo: {
    marginTop: 20,
    width: '50%',
    height: 100,
    resizeMode: 'contain',
  }
});
