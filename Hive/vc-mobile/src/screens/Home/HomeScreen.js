import React from 'react';
import {
    Alert,
    AsyncStorage,
    Image,
    ImageBackground,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';

import { GothamBookTextInput, GothamMediumText, PrimaryButton, GothamNotRoundedMediumText, GothamNotRoundedBookText, CornerLabel } from '../../components';

import { connect } from 'react-redux';
import Colors from '../../constants/Colors';
import HomeNavBar from './HomeNavBar';
import ModalWrapper from 'react-native-modal-wrapper';
import CausesModal from './CausesModal';
import Geocoder from 'react-native-geocoding';
// import { Permissions } from 'expo';
import Permissions from 'react-native-permissions';
import ApiConstants from '../../constants/ApiConstants';
import Config from 'react-native-config'

import { LikesActions, SearchActions } from '../../actions';

const imgBkg = require('../../../assets/images/background8.png');
const rightArrow = require('../../../assets/icons/right_arrow.png');

// let GOOGLE_API_KEY = "AIzaSyDcba65dNLqVHn7-Ff0c4tOvwc_T5WZhRQ";
let GOOGLE_API_KEY = "AIzaSyBPXJsRJvbnV_cr-XH2HF9vQtHzIob7LHU";

let geoTimeoutHandler = null;
class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      isCausesPanelOpen: false,
      zipCode: '',
      location: null,
      locationPermission: null,
      causes: []
    };
    this._retrieveData();
  }

  showCausesPanel = () => {
    this.setState({
      isCausesPanelOpen: true
    })
  };

  hideCausesPanel = () => {
    this.loadCauses();
    this.setState({
      isCausesPanelOpen: false
    })
  };

  loadCauses = () => {
    AsyncStorage.getItem('SELECTED_CAUSES', (err, causes) => {
      if(causes) {
        this.setState({
          causes: JSON.parse(causes)
        })
      }
    });
  }

  _storeData = async (zipCode) => {
      try {
          await AsyncStorage.setItem('ZIP_CODE', zipCode);
          this.setState({ zipCode: zipCode });
      } catch (error) {
          // Error saving data
      }
  };

  _retrieveData = async () => {
      try {
          const value = await AsyncStorage.getItem('ZIP_CODE');
          if (value !== null) {
              // We have data!!
              this.setState({zipCode: value});
          }
      } catch (error) {
          // Error retrieving data
      }
  };

  _getLocationAsync = async () => {
    Permissions.check('location', {type: 'always'}).then(response => {
      this.setState({ locationPermission: response }, ()=> {
        if (this.state.locationPermission === 'undetermined' || this.state.locationPermission === 'denied') {
          Permissions.request('location', { type: 'always' }).then(response => {
            this.setState({locationPermission: response}, ()=> {
              this._checkPermissions();
            });
          });
        }
        this._checkPermissions();
      })
    });
  };

  _checkPermissions = () => {
    if (this.state.locationPermission === 'authorized') {
      AsyncStorage.getItem('ZIP_CODE', (err, value) => {
        if (value === null) {
          Alert.alert(
            'Allow "VolunteerCrowd" to access your location.',
            'VolunteerCrowd will use your location to help you find great opportunities and check in and out of volunteer projects.',
            [
              {
                text: "DON'T ALLOW",
                style: 'cancel',
              },
              {
                text: 'OK', onPress: () => {
                  this.accessLocation();
                }
              },
            ],
            {cancelable: false},
          );
        } else {
          this.accessLocation();
        }
      })
    } else if (this.state.locationPermission !== 'authorized') {

    }              
  }

  accessLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        console.log(position);
        const location = JSON.stringify(position);
        var latitude=position.coords.latitude;
        var longitude=position.coords.longitude;

        AsyncStorage.setItem('latitude', latitude.toString());
        AsyncStorage.setItem('longitude', longitude.toString());

        // Position Geocoding
        Geocoder.init(GOOGLE_API_KEY); //google api key
        Geocoder.from(latitude,longitude)
          .then(json => {
            var addressComponent = json.results[0].address_components;
            for (i = 0; i < addressComponent.length; i++) {
              var types=addressComponent[i].types;
              if(types.indexOf("postal_code") > -1){
                this._storeData(addressComponent[i].short_name);
              }
            }
          })
          .catch(error => console.warn(error));

        this.setState({ location });
      },
      error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000 }
    );
  };

  componentDidMount = () => {
    Geocoder.init(GOOGLE_API_KEY);

    this.loadCauses();
    this.props.getLikesList({
      cb: (res) => {}
    });

    AsyncStorage.setItem('PREV_TAB', 'HomeStackWrapper');
  }

  onTouchStartZipCode = () => {
    const { zipCode } = this.state;
    if(zipCode) {
      return;
    }

    this._getLocationAsync()
  }

  onChangeZipCode = (zipCode) => {
    console.log(zipCode);
    AsyncStorage.setItem('ZIP_CODE', zipCode)
    this.setState({
      zipCode: zipCode
    }, () => {
      if(geoTimeoutHandler) {
        clearTimeout(geoTimeoutHandler);
      }
      
      if(zipCode.length > 1) {
        geoTimeoutHandler = setTimeout(() => {
          Geocoder.from(zipCode)
            .then(json => {
              if(json.status === 'OK') {
                var location = json.results[0].geometry.location;
                console.log(location);
                AsyncStorage.setItem('latitude', location.lat.toString());
                AsyncStorage.setItem('longitude', location.lng.toString());                
              }
            })
            .catch(error => {
              AsyncStorage.setItem('latitude', '');
              AsyncStorage.setItem('longitude', '');   
              console.warn(error)
            });
        }, 200);
      }
    })
  }

  render() {
    const { navigation } = this.props;
    const { isCausesPanelOpen, zipCode, causes } = this.state;

    let causeRows = [];
    if(causes.length > 0) {
      causes.forEach((cause, index) => {
        if(index < 2) {
          causeRows.push(
            <View key={`cause-${index}`} style={styles.causeCol}>
              <View style={styles.causeImgWrapper}>
                <Image source={cause.image} style={styles.causeImg}/>
              </View>
              <GothamNotRoundedMediumText style={styles.causeName}>{cause.name}</GothamNotRoundedMediumText>            
            </View>
          );
        }
      })
    }
    
    return (
      <View>
        <StatusBar backgroundColor="blue" barStyle="light-content" />
        <View style={{width: '100%', height: '100%'}}>
          <HomeNavBar
            onLeftButtonPress={() => navigation.toggleDrawer()}
            onRightButtonPress={() => navigation.navigate('Profile')}
          />
          <View style={styles.container}>
            { Config.ENVIRONMENT === 'DEV' && 
              <View style={styles.cornerLabelView}>
                <CornerLabel>DEV</CornerLabel> 
              </View>
            }
            <GothamMediumText
              style={styles.titleText}>
              FIND VOLUNTEER{"\n"}
              <GothamMediumText style={{color: '#8DC63F'}}>OPPORTUNITIES</GothamMediumText>
            </GothamMediumText>
            <View style={[styles.chooseWrapper,{paddingHorizontal: 20, marginTop: 20}]}>
                <GothamBookTextInput onTouchStart={this.onTouchStartZipCode} style={styles.textInput} placeholder={'Zip code'}
                           placeholderTextColor="#606060" onChangeText = {this.onChangeZipCode}
                           value={this.state.zipCode}/>
            </View>
            <TouchableOpacity style={styles.chooseWrapper} onPress={this.showCausesPanel}>
              {
                causes.length > 0 ? 
                <View style={[styles.textInput, styles.causesRowsWrapper, {marginBottom: 0}]}>
                  <View style={{width: 318, height: 40, paddingTop: 7, flexDirection: 'row', overflow: 'hidden'}}>
                    {causeRows}
                    {
                      causes.length > 2 &&
                      <GothamNotRoundedBookText style={styles.moreText}>+{causes.length - 2} more</GothamNotRoundedBookText>
                    }
                  </View>
                </View>
                :
                <GothamBookTextInput style={styles.textInput} placeholder={'Choose your favorite causes'} placeholderTextColor="#606060"/>
              }
              <View style={styles.rightArrow}>
                <Image source={rightArrow} style={causes.length === 0 && {tintColor: 'gray'}}/>
              </View>
            </TouchableOpacity>
            <ImageBackground source={imgBkg} style={styles.bg}>
              <PrimaryButton
                style={styles.searchBtn}
                onPress={() => navigation.navigate('Search', {zipCode: this.state.zipCode})}
                title="SEARCH"
              />            
            </ImageBackground>
          </View>
        </View>

        <ModalWrapper
          containerStyle={{ flexDirection: 'row', justifyContent: 'flex-end' }}
          onRequestClose={this.hideCausesPanel}
          shouldAnimateOnRequestClose={true}
          position="right"
          visible={isCausesPanelOpen}>
          <CausesModal hideCausesPanel={this.hideCausesPanel}></CausesModal>
        </ModalWrapper>        
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    LikesListData: state.LikesReducer.LikesListData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getLikesList: (request) => dispatch(LikesActions.getLikesList(request.cb))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  row: {
    borderWidth: 1,
    borderRadius: 1,
    marginVertical: 5,
    marginHorizontal: 10,
    padding: 5,
  },
  avatar: {
    width: 100,
    height: 100,
  },
  largeRow: {
    flexDirection: 'row',
  },
  column: {
    paddingHorizontal: 10,
    justifyContent: 'space-between'
  },
  titleText: {
    marginTop: 0,
    textAlign: 'center',
    fontSize: 32,
    lineHeight: 32,
    color: Colors.tintColor,
  },
  locationText: {
    marginTop: 10,
    marginBottom: 20,
  },
  icon: {
    width: 24,
    height: 24,
  },
  textInput: {
    width: 334,
    paddingHorizontal: 20,
    marginBottom: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#606060',
    height: 50,
    color: '#606060'
  },
  selectedCauses: {

  },
  chooseWrapper: {
    position: 'relative',
    alignItems: 'center'
  },
  rightArrow: {
    position: 'absolute',
    right: -15,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bg: {
    width: '100%', 
    flex: 1, 
    alignItems: 'center',
    paddingTop: 0,
    marginTop: 20,
    position: 'relative',
    alignItems: 'center'
  },
  searchBtn: {
    marginTop: -10
  },
  textInputZipCode: {
      width: 334,
      paddingHorizontal: 20,
      paddingLeft:35,
      marginBottom: 10,
      borderBottomWidth: 2,
      borderBottomColor: '#606060',
      height: 50,
      color: '#606060'
  },
  locationIcon: {
      position: 'absolute',
      left: 20,
      width: 30,
      height: 50,
      alignItems: 'center',
      justifyContent: 'center',
  },
  causeCol: {
    marginRight: 8,
    flexDirection: 'row',
    borderRadius: 15,
    backgroundColor: '#FAFDFF',
    shadowColor: '#CACACA',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 1,
    paddingRight: 15,
    height: 30
  },
  causeImgWrapper: {
    width: 30,
    height: 30,
    padding: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#CACACA'
  },
  causeImg: {
    resizeMode: 'stretch',
    maxWidth: 18,
    maxHeight: 18
  },
  causeName: {
    marginLeft: 8,
    color: '#303030',
    fontSize: 10,
    textTransform: 'uppercase',
    marginTop: 10
  },
  causesRowsWrapper: {
    flexDirection: 'row', 
    paddingHorizontal: 0, 
    alignItems: 'center'
  },
  moreText: {
    fontSize: 15,
    color: '#606060',
    marginTop: 7
  },
  cornerLabelView: {
    position: 'absolute',
    marginTop: -100,
    flexDirection: 'row',
    width: '100%',
  }
});
