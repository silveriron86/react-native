import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Header } from 'react-native-elements'; 
import Colors from '../../constants/Colors';
const imgLogo = require('../../../assets/images/logo.png');
const imgMenuIcon = require('../../../assets/icons/menuButton.png');
const imgProfileIcon = require('../../../assets/icons/dashboard/profile.png');

const LeftButton = (props) => (
  <TouchableOpacity onPress={props.onPress} style={styles.leftButton}>
    <Image source={imgMenuIcon} style={styles.leftButtonIcon} />
  </TouchableOpacity>
);

const RightButton = (props) => (
  <TouchableOpacity onPress={props.onPress} style={styles.rightButton}>
    <Image source={imgProfileIcon} style={styles.rightButtonIcon} />
  </TouchableOpacity>
);

export default HomeNavBar = (props) => {
  const { onLeftButtonPress, onRightButtonPress } = props;
  return (
    <Header
      backgroundColor='white'
      leftComponent={<LeftButton onPress={onLeftButtonPress} />}
      containerStyle={styles.container}
      // centerComponent={<Image source={imgLogo} style={styles.centerLogo} />}
      // rightComponent={<RightButton onPress={onRightButtonPress} />}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 0,
    // height: 50,
    alignItems: 'center',
    // paddingTop: 0,
    // paddingLeft: 0
  },
  leftButton: {
    width: 46,
    height: 45,
    marginLeft: -10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftButtonIcon: {
    width: 22,
    height: 16,
    resizeMode: 'contain',
  },
  rightButton: {
    width: 36,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightButtonIcon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
  centerLogo: {
    width: 101.27,
    height: 41,
    resizeMode: 'contain',
  }
});

 