import React from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import Utils from '../../utils';
import Causes from '../../constants/Causes';


const leftArrow = require('../../../assets/icons/left_arrow_white.png');

export default class CausesModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: []
    }
  }

  onSelectCause = (cause) => {
    let list = this.state.selected;
    let isExist = false;
    let index = 0;

    for (let i = 0; i < list.length; i++) {
      if (list[i].name === cause.name) {
        isExist = true;
        index = i;
        break;
      }
    }
    if(isExist) {
      list.splice(index, 1);
    }else {
      list.push(cause);
    }
    this.setState({
      selected: list
    },()=> {
      AsyncStorage.setItem('SELECTED_CAUSES', JSON.stringify(this.state.selected));
    });
  }

  isSelected = (cause) => {
    let list = this.state.selected;
    for (let i = 0; i < list.length; i++) {
      if (list[i].name === cause.name) {
          return true;
      }
    }

    return false;
  }

  componentWillMount() {
    AsyncStorage.getItem('SELECTED_CAUSES', (err, causes) => {
      if(causes) {
        this.setState({
          selected: JSON.parse(causes)
        })
      }
    });
  }

  render() {
    let rows = [];
    let cols = [];
    Causes.CAUSES.forEach((cause, index) => {
      if(index > 0 && index % 3 == 0) {
        rows.push(
          <View style={styles.causesWrapper} key={`cause-row-${index}`}>
            {cols}
          </View>
        );
        cols = [];
      }

      let isSelected = this.isSelected(cause);
      cols.push(
        <TouchableOpacity key={`cause-${index}`} style={styles.causeItem} onPress={()=> this.onSelectCause(cause)}>
          <View style={[cause.name && styles.imgWrapper, isSelected && styles.selectedImgWrapper]}>
            <Image source={cause.image} style={isSelected && styles.selectedImg}/>
          </View>
          <Text style={styles.causeText}>{cause.name}</Text>
        </TouchableOpacity>
      )      
    });

    rows.push(
      <View style={styles.causesWrapper} key={`cause-row-last`}>
        {cols}
      </View>
    );    

    return (
      <ScrollView style={styles.container}>
        <StatusBar hidden={!Utils.nativeBaseDoesHeaderCorrectlyAlready()}/>
        <View style={styles.padHeader}></View>
        <View style={styles.modalHeader}>
          <TouchableOpacity
            style={styles.leftArrowBtn}
            onPress={this.props.hideCausesPanel}>
            <Image source={leftArrow}/>
          </TouchableOpacity>          
          <Text style={styles.headerText}>Causes</Text>
        </View>
        <View style={styles.modalWrapper}>
          {rows}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: 307
    },
    padHeader: {
      height: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
      backgroundColor: '#1D75BD'
    },    
    leftArrowBtn: {
      position: 'absolute',
      left: 0,
      top: 0,
      width: 48,
      height: 41,
      zIndex: 9999,
      justifyContent: 'center',
      alignItems: 'center'
    },
    modalHeader: {
      backgroundColor: '#1D75BD',
      height: 41,
      position: 'relative',
      justifyContent: 'center'
    },
    headerText: {
      color: 'white',
      fontSize: 16,
      textAlign: 'center'
    },
    modalWrapper: {
      padding: 11
    },
    causesWrapper: {
      flexDirection: 'row',
      justifyContent: 'space-between'
    },  
    causeItem: {
      width: 92,
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 15
    },  
    imgWrapper: {
      width: 60,
      height: 60,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 30,
      borderWidth: 2,
      borderColor: '#CACACA'
    },
    causeText: {
      color: '#333333',
      fontSize: 10,
      textAlign: 'center',
      marginTop: 8,
      textTransform: 'uppercase'
    },
    selectedImgWrapper: {
      backgroundColor: '#609ed1'
    },
    selectedImg: {
      tintColor: 'white'
    }
});

 