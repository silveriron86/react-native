import React from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import Colors from "../../constants/Colors";
import Utils from '../../utils';
import { GothamNotRoundedMediumText, PrimaryButton, GothamBookTextInput } from '../../components';

const leftArrow = require('../../../assets/icons/left_arrow_white.png');

export default class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  handleConfirm = () => {

  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={!Utils.nativeBaseDoesHeaderCorrectlyAlready()}/>
        <View style={styles.padHeader}></View>
        <View style={styles.modalHeader}>
          <TouchableOpacity
            style={styles.leftArrowBtn}
            onPress={this.props.hidePanel}>
            <Image source={leftArrow}/>
          </TouchableOpacity>          
          <GothamNotRoundedMediumText style={styles.headerText}>Change Password</GothamNotRoundedMediumText>
        </View>
        <View style={styles.modalWrapper}>
          <View style={styles.contentWrapper}>
            <GothamBookTextInput
              placeholderTextColor="#CACACA"
              placeholder="Current Password"
              secureTextEntry="true"
              style={styles.textInput}/>
            <GothamBookTextInput
              placeholderTextColor="#CACACA"
              secureTextEntry="true"
              placeholder="New Password"
              style={styles.textInput}/>
            
            <GothamBookTextInput
              placeholderTextColor="#CACACA"
              secureTextEntry="true"
              placeholder="Confirm New Password"
              style={styles.textInput}/>

            <View style={styles.bottom}>
              <PrimaryButton style={styles.confirmBtn} onPress={this.handleConfirm} title="CONFIRM" />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      backgroundColor: '#F1F1F1'
    },
    padHeader: {
      height: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
      backgroundColor: '#1D75BD'
    },    
    leftArrowBtn: {
      position: 'absolute',
      left: 0,
      top: 0,
      width: 48,
      height: 41,
      zIndex: 9999,
      justifyContent: 'center',
      alignItems: 'center'
    },
    modalHeader: {
      backgroundColor: '#1D75BD',
      height: 41,
      position: 'relative',
      justifyContent: 'center'
    },
    headerText: {
      color: 'white',
      fontSize: 16,
      textAlign: 'center',
      marginTop: 5
    },
    modalWrapper: {
      flex: 1,
      height: '100%'
    },
    contentWrapper: {
      position: 'relative',
      flex: 1,
      height: '100%',
      margin: 20,
      marginBottom: 0,
      borderRadius: 2,
      backgroundColor: '#FFFFFF',
      shadowColor: '#CACACA',
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 1,
      shadowRadius: 2,
      elevation: 4,
      paddingTop: 30
    },
    bottom: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'flex-end',
      marginBottom: 50
    },
    textInput: {
      marginBottom: 10,
      height: 50,
      marginHorizontal: 26,
      paddingHorizontal: 13,
      fontSize: 15,
      color: Colors.titleText,
      borderBottomWidth: 2,
      borderBottomColor: '#606060'
    }
});

 