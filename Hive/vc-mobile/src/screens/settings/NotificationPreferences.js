import React from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import RoundCheckbox from 'rn-round-checkbox';
import Utils from '../../utils';
import { GothamNotRoundedMediumText, PrimaryButton, GothamBookText } from '../../components';

const leftArrow = require('../../../assets/icons/left_arrow_white.png');
const checkedImg = require('../../../assets/icons/checked.png')
const unCheckedImg = require('../../../assets/icons/unchecked.png')

export default class NotificationPreferences extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked1: true,
      checked2: false,
      checked3: false
    }
  }

  handleConfirm = () => {

  }

  render() {
    const {checked1, checked2, checked3} = this.state;
    return (
      <View style={styles.container}>
        <StatusBar hidden={!Utils.nativeBaseDoesHeaderCorrectlyAlready()}/>
        <View style={styles.padHeader}></View>
        <View style={styles.modalHeader}>
          <TouchableOpacity
            style={styles.leftArrowBtn}
            onPress={this.props.hidePanel}>
            <Image source={leftArrow}/>
          </TouchableOpacity>          
          <GothamNotRoundedMediumText style={styles.headerText}>Notification Preferences</GothamNotRoundedMediumText>
        </View>
        <View style={styles.modalWrapper}>
          <View style={styles.contentWrapper}>
            <TouchableOpacity style={{flexDirection: 'row', marginTop: 45}} 
            // onPress={()=>{this.setState({checked1: !this.state.checked1})}}
            >
              <View style={styles.editIconView}>
                <Image source={checked1 ? checkedImg : unCheckedImg} style={styles.checkedImg}/>
              </View>
              <View style={styles.checkTextView}>
                <GothamBookText style={styles.checkBoxText}>PUSH NOTIFICATION</GothamBookText>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={{flexDirection: 'row', marginTop: 15}} 
            // onPress={()=>{this.setState({checked2: !this.state.checked2})}}
            >
              <View style={styles.editIconView}>
                <Image source={checked2 ? checkedImg : unCheckedImg} style={styles.checkedImg}/>
              </View>
              <View style={styles.checkTextView}>
                <GothamBookText style={styles.checkBoxText}>EMAIL</GothamBookText>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={{flexDirection: 'row', marginTop: 15}} 
            // onPress={()=>{this.setState({checked3: !this.state.checked3})}}
            >
              <View style={styles.editIconView}>
                <Image source={checked3 ? checkedImg : unCheckedImg} style={styles.checkedImg}/>
              </View>
              <View style={styles.checkTextView}>
                <GothamBookText style={styles.checkBoxText}>TEXT MESSAGE</GothamBookText>
              </View>
            </TouchableOpacity>                        

            <View style={styles.bottom}>
              <PrimaryButton style={styles.confirmBtn} onPress={this.handleConfirm} title="CONFIRM" />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      backgroundColor: '#F1F1F1'
    },
    padHeader: {
      height: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
      backgroundColor: '#1D75BD'
    },    
    leftArrowBtn: {
      position: 'absolute',
      left: 0,
      top: 0,
      width: 48,
      height: 41,
      zIndex: 9999,
      justifyContent: 'center',
      alignItems: 'center'
    },
    modalHeader: {
      backgroundColor: '#1D75BD',
      height: 41,
      position: 'relative',
      justifyContent: 'center'
    },
    headerText: {
      color: 'white',
      fontSize: 16,
      textAlign: 'center',
      marginTop: 5
    },
    modalWrapper: {
      flex: 1,
      height: '100%'
    },
    contentWrapper: {
      position: 'relative',
      flex: 1,
      height: '100%',
      margin: 20,
      marginBottom: 0,
      borderRadius: 2,
      backgroundColor: '#FFFFFF',
      shadowColor: '#CACACA',
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 1,
      shadowRadius: 2,
      elevation: 4
    },
    bottom: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'flex-end',
      marginBottom: 50
    },
    editIconView: {
      width: 24,
      height: 24,
      borderWidth: 0,
      marginLeft: 26,
      marginRight: 12,
      justifyContent: 'center',
      alignItems: 'center'
    },
    checkBoxText: {
      color: 'black',
      fontSize: 12,
      lineHeight: 15
    },
    checkedImg: {
      width: 24,
      height: 24
    },
    checkTextView: {
      flex: 1,
      justifyContent: 'center',
      paddingTop: 5
    }    
});