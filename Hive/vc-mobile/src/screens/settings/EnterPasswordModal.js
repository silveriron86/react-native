import React from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import Colors from "../../constants/Colors";
import Utils from '../../utils';
import { GothamNotRoundedMediumText, PrimaryButton, GothamBookTextInput } from '../../components';

const leftArrow = require('../../../assets/icons/left_arrow_white.png');

export default class EnterPasswordModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  handleConfirm = () => {

  }

  render() {
    const { toChangeText, hidePanel } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.modalHeader}>
          <GothamNotRoundedMediumText style={styles.headerText}>Enter your password to change {toChangeText}</GothamNotRoundedMediumText>
        </View>
        <View style={styles.contentWrapper}>
          <GothamBookTextInput
            placeholderTextColor="#CACACA"
            placeholder="Password"
            secureTextEntry="true"
            style={styles.textInput}/>

          <View style={styles.bottom}>
            <TouchableOpacity style={styles.btn} onPress={hidePanel}>
              <GothamNotRoundedMediumText style={styles.btnText}>CANCEL</GothamNotRoundedMediumText>
            </TouchableOpacity>
            <View style={{width: 2}}></View>
            <TouchableOpacity style={styles.btn} onPress={hidePanel}>
              <GothamNotRoundedMediumText style={styles.btnText}>SUBMIT</GothamNotRoundedMediumText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },  
    modalHeader: {
      backgroundColor: '#1D75BD',
      height: 66,
      position: 'relative',
      justifyContent: 'center',
      alignItems: 'center'
    },
    headerText: {
      width: 182,
      color: 'white',
      fontSize: 18,
      textAlign: 'center',
      letterSpacing: -0.24,
      marginTop: 5
    },
    contentWrapper: {
      position: 'relative',
      paddingTop: 16,
      paddingBottom: 20,
      backgroundColor: '#FFFFFF',
      alignItems: 'center',
      justifyContent: 'center'
    },
    bottom: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 10
    },
    btn: {
      flex: 1,
      backgroundColor: '#F1F1F1',
      alignItems: 'center',
      justifyContent: 'center',
      height: 50
    },
    btnText: {      
      color: '#1D75BD',
      fontSize: 14,
      marginTop: 5
    },
    textInput: {
      marginBottom: 10,
      height: 50,
      width: 218,
      paddingHorizontal: 13,
      fontSize: 15,
      color: Colors.titleText,
      borderBottomWidth: 2,
      borderBottomColor: '#606060'
    }
});

 