import React from 'react';
import {Dimensions, Image, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View, Linking} from 'react-native';
import {GothamBookText, GothamNotRoundedMediumTextInput, GothamNotRoundedMediumText} from '../../components';
import ModalWrapper from 'react-native-modal-wrapper';
import NotificationPreferences from './NotificationPreferences';
import Colors from "../../constants/Colors";
import Utils from '../../utils';
import ChangePassword from './ChangePassword';
import EnterPasswordModal from './EnterPasswordModal';

const topBackground = require('../../../assets/images/top_background.png');
const profileImage = require('../../../assets/icons/profile/avatar_blue.png');
const editIcon = require('../../../assets/icons/edit_icon.png');
const rightArrow = require('../../../assets/icons/black_arrow.png');

export default class SettingsScreen extends React.Component {
    static navigationOptions = {
        title: 'Account Settings'
    }

    constructor(props) {
        super(props);
        this.showHidePassword = this.showHidePassword.bind(this);

        this.state = {
            emailAddress: '',
            mobileNo: '',
            changePassword: '',
            showPassword: true,
            type: 'parent',
            isNpPanelOpen: false,
            isCpPanelOpen: false,
            isCpPanelOpen: false
        }
    }

    toggleType = () => {
        this.setState({
            type: (this.state.type === 'parent') ? 'child' : 'parent'
        });
    }

    showHidePassword = () => {
        this.setState({showPassword: !this.state.showPassword});
    };

    showNpPanel = () => {
        this.setState({
            isNpPanelOpen: true
        }) 
    }
    hideNpPanel = () => {
        this.setState({
            isNpPanelOpen: false
        })
    }

    showCpPanel = () => {
        this.setState({
            isCpPanelOpen: true
        }) 
    }

    hideCpPanel = () => {
        this.setState({
            isCpPanelOpen: false
        })
    }  

    hidePwdModal = () => {
        this.setState({
            isPwdModalOpen: false
        })
    }

    tryChange = (text) => {
        this.setState({
            toChangeText: text,
            isPwdModalOpen: true
        })
    }

    render() {
        const {emailAddress, changePassword, mobileNo, type, isNpPanelOpen, isCpPanelOpen, isPwdModalOpen, toChangeText} = this.state;
        const {navigation} = this.props;
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.insideView}>
                        <StatusBar hidden={true}/>
                        <View style={styles.topProfileView}>
                            <Image style={[styles.background]} source={topBackground}/>
                        </View>
                        <View style={styles.profileIconView}>
                            <Image source={profileImage}/>
                        </View>
                        <View style={styles.profileNameView}>
                            <GothamBookText
                                style={styles.profileNameText}>
                                {/* Jennifer */}
                            </GothamBookText>

                            <TouchableOpacity
                                style={styles.profileEditIcon}>
                                {/* <Image source={editIcon}/> */}
                            </TouchableOpacity>
                        </View>
                        {/* <GothamBookText
                            style={styles.freeAccountText}>
                            Free Account
                        </GothamBookText>
                        <GothamBookText
                            style={styles.upgradeText}>
                            Upgrade to VolunteerCrowd PLUS
                        </GothamBookText> */}

                        <View style={styles.detailsWrapperView}>
                            <GothamBookText
                                style={styles.settingsText}>
                                ACCOUNT SETTINGS
                            </GothamBookText>
                            <View style={styles.detailItemsView}>
                                <TouchableOpacity style={styles.inputRow} onPress={()=>this.tryChange('email')}>
                                    {/* <GothamNotRoundedMediumTextInput
                                        placeholderTextColor="#303030"
                                        placeholder="Email"
                                        onChangeText={(emailAddress) => this.setState({emailAddress})}
                                        value={emailAddress}
                                        keyboardType="email-address"
                                        style={styles.textInput}
                                    /> */}
                                    {/* <View style={styles.borderView}>
                                        <GothamNotRoundedMediumText style={[styles.textInput, {paddingTop: 16, marginBottom: 0}]}>
                                            {this.state.emailAddress ? 'Email: ' + this.state.emailAddress : ''}
                                        </GothamNotRoundedMediumText>

                                    </View> */}
                                    {/* <View style={styles.inputEditIcon}> */}
                                        {/* <Image source={editIcon}/> */}
                                    {/* </View>        */}
                                </TouchableOpacity>                                  
                                <TouchableOpacity style={styles.inputRow} onPress={()=>this.tryChange('mobile')}>
                                    {/* <GothamNotRoundedMediumTextInput
                                        placeholderTextColor="#303030"
                                        placeholder="Mobile Number"
                                        onChangeText={(mobileNo) => this.setState({mobileNo})}
                                        value={mobileNo}
                                        style={styles.textInput}
                                    /> */}
                                    {/* <View style={styles.borderView}>
                                        <GothamNotRoundedMediumText style={[styles.textInput, {paddingTop: 16, marginBottom: 0}]}>Mobile Number</GothamNotRoundedMediumText>
                                    </View>
                                    <View style={styles.inputEditIcon}>
                                        <Image source={editIcon}/>
                                    </View>                                 */}
                                </TouchableOpacity>
                                <View style={[styles.textActionRowWrapper, styles.borderView]}>
                                    <TouchableOpacity style={styles.actionItemView} 
                                    // onPress={this.showCpPanel}
                                    onPress={() => Linking.openURL('https://volunteercrowd.com/')}
                                    >
                                        <GothamNotRoundedMediumText style={styles.actionItemText}>Change Password (Contact Support)</GothamNotRoundedMediumText>
                                        <View style={styles.rightArrow}>
                                            <Image source={rightArrow}/>
                                        </View>
                                    </TouchableOpacity>
                                </View>                                 
                                <View style={styles.textActionRowWrapper}>
                                    <TouchableOpacity style={styles.actionItemView} onPress={this.showNpPanel}>
                                        <GothamNotRoundedMediumText style={styles.actionItemText}>Notification preferences</GothamNotRoundedMediumText>
                                        <View style={styles.rightArrow}>
                                            <Image source={rightArrow}/>
                                        </View>
                                    </TouchableOpacity>
                                </View>                                
                                                  
                            </View>
                        </View>
                        {/* <View style={styles.actionTextWrapperView}>
                            <TouchableOpacity style={styles.actionItemView}>
                                <GothamNotRoundedMediumText style={styles.actionItemText}>Volunteer Profile</GothamNotRoundedMediumText>
                                <View style={styles.rightArrow}>
                                    <Image source={rightArrow}/>
                                </View>
                            </TouchableOpacity>
                        </View> */}
                        {/* <View style={styles.actionTextWrapperView}>
                            <TouchableOpacity style={styles.actionItemView} onPress={this.toggleType}>
                                <GothamNotRoundedMediumText style={styles.actionItemText}>{(type === 'parent') ? 'Parent' : 'Child'}</GothamNotRoundedMediumText>
                                <View style={styles.rightArrow}>
                                    <Image source={rightArrow}/>
                                </View>
                            </TouchableOpacity>
                        </View> */}
                        <TouchableOpacity onPress={() => navigation.goBack()} style={styles.closeBtn}>
                            <Image source={require('../../../assets/icons/cross_blue.png')}/>
                        </TouchableOpacity>
                    </View>
                </ScrollView>

                <ModalWrapper
                    containerStyle={{ flexDirection: 'row', justifyContent: 'flex-end' }}
                    onRequestClose={this.hideNpPanel}
                    shouldAnimateOnRequestClose={true}
                    position="right"
                    visible={isNpPanelOpen}>
                    <NotificationPreferences hidePanel={this.hideNpPanel}></NotificationPreferences>
                </ModalWrapper>

                <ModalWrapper
                    style={{ width: 270, height: 203, borderRadius: 8, overflow: 'hidden' }}
                    onRequestClose={this.hidePwdModal}
                    shouldAnimateOnRequestClose={true}
                    visible={isPwdModalOpen}>
                    <EnterPasswordModal toChangeText={toChangeText} hidePanel={this.hidePwdModal}></EnterPasswordModal>
                </ModalWrapper>

                <ModalWrapper
                    containerStyle={{ flexDirection: 'row', justifyContent: 'flex-end' }}
                    onRequestClose={this.hideCpPanel}
                    shouldAnimateOnRequestClose={true}
                    position="right"
                    visible={isCpPanelOpen}>
                    <ChangePassword hidePanel={this.hideCpPanel}></ChangePassword>
                </ModalWrapper>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.settingsBackground,
    },
    insideView: {
        paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
    },
    topProfileView: {        
        position: 'absolute',
        left: 0,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: Dimensions.get('window').width,        
    },
    background: {
        width: Dimensions.get('window').width,
        height: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 340 : 300,
        resizeMode: 'cover',
        position: 'absolute'
    },
    profileIconView: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 35,
        marginBottom: 10,
    },
    profileNameView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
    },
    profileNameText: {
        textAlign: 'center',
        fontSize: 14,
        marginLeft: 20,
        marginTop: 3,
        color: Colors.titleText
    },
    profileEditIcon: {
        marginLeft: 10,
    },
    freeAccountText: {
        textAlign: 'center',
        fontSize: 14,
        marginBottom: 5,
        color: Colors.titleText
    },
    upgradeText: {
        textAlign: 'center',
        fontSize: 15,
        fontWeight: 'bold',
        color: Colors.deepSkyBlue,
    },
    detailsWrapperView: {
        // paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
        flex: 1,
        marginTop: 120,
    },
    settingsText: {
        fontSize: 10,
        left: 25,
        color: Colors.lightGrey,
        marginBottom: 7,
    },
    detailItemsView: {
        marginLeft: 25,
        marginRight: 25,
        paddingVertical: 3,
        backgroundColor: Colors.white,
        borderRadius: 3,
        alignItems: 'center',
        shadowColor: '#CACACA',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 4    
    },
    textInput: {
        marginBottom: 8,
        width: Dimensions.get('window').width - 80,
        height: 50,
        paddingVertical: 10,
        fontSize: 14,
        fontWeight: 'bold',
        color: Colors.titleText,
        borderBottomWidth: 1,
        borderBottomColor: '#F1F1F1'
    },
    textActionRowWrapper: {
        width: Dimensions.get('window').width - 80,
        height: 50,
        fontSize: 14
    },
    actionText: {
        color: Colors.titleText,
        fontSize: 14,
        fontWeight: 'bold'
    },
    actionTextWrapperView: {
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 25,
        marginRight: 25,
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
    actionItemView: {
        width: Dimensions.get('window').width - 80,
        height: 50,
        paddingVertical: 5,
        fontSize: 14,
        color: Colors.titleText,
        flexDirection: 'row',
        alignItems: 'center',
    },
    actionItemText: {
        color: Colors.titleText,
        fontSize: 14,
        fontWeight: 'bold',
    },
    rightArrow: {
        position: 'absolute',
        right: 1,
        width: 30,
        height: 50,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    closeBtn: {
        position: 'absolute',
        left: 17,
        top: 27,
    },
    inputRow: {
        position: 'relative', 
        justifyContent: 'center'
    },
    inputEditIcon: {
        position: 'absolute',
        right: 0,
        width: 30,
        height: 30,
        alignItems: 'flex-end',
        marginTop: -25
    },
    borderView: { 
        borderBottomWidth: 1, 
        borderBottomColor: '#F1F1F1', 
        marginBottom: 8 
    }
});