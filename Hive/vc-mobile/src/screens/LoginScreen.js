import React from 'react';
import {
    Alert,
    Image,
    ImageBackground,
    KeyboardAvoidingView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    NativeModules,
    AsyncStorage,
    Linking
} from 'react-native';
import axios from 'axios';

import {GothamBookText, PrimaryButton, SocialButton, LoadingOverlay, GothamMediumText, GothamNotRoundedMediumText, CornerLabel} from '../components';
import Utils from '../utils';
import ApiConstants from '../constants/ApiConstants';
import Config from 'react-native-config'
const imgLogo = require('../../assets/images/logo.png');
const imgBkg = require('../../assets/images/background1.png');

export default class LoginScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.showHidePassword = this.showHidePassword.bind(this);
        this.state = {
            socialType: 'oauth',
            showPassword: true,
            loading: false,
            apiCallCount: 0
        }
    }

    state = {
        email: '',
        password: '',
    };

    handleLogin = () => {
        const { email, password } = this.state;
        console.log(email);
        if(!email) {
            Alert.alert('Error', 'Please enter your email address.');
            return;
        }

        if(!Utils.validateEmail(email)) {
            Alert.alert('Error', 'Please enter a valid email address');
            return;
        }
        
        if(!password) {
            Alert.alert('Error', 'Please enter your password.');
            return;
        }

        this.setState({
            loading: true,
            socialType: 'oauth',
            apiCallCount: 0
        },() => {
            let options = {
                method: 'POST',
                url: 'https://oauth.io/api/usermanagement/signin?k=' + Config.OAUTH_KEY,
                data: {
                    email: email,
                    password: password,
                    as_jwt: 'true'
                }
            }
            axios(options).then(response => {
                console.log(response);
                if(response.status === 200) {
                    console.log(response.data)
                    this._oauthLogin(response.data);
                }
            })
            .catch(error => {
                console.log(error.response.data);
                this.setState({
                    loading: false
                }, () => {
                    Alert.alert('Error', 'Login failed');
                })
            })            
        }) 
    };

    handleSocialLogin = (type) => {
        this.setState({
            socialType: type
        },() => {
            console.log(type);
            NativeModules.OAuth.open(type); 
            this.updateStatus();  
        })
    }    

    updateStatus = () => {
        NativeModules.OAuth.getStatus(res => {
            console.log(res);      
            if(res.access_token) {
                this._oauthLogin(res);
            }else {
                Alert.alert('Error', 'Login Failed');
            }
        })
    }    

    _oauthLogin = (data) => {
        console.log(data);
        if(data.data && data.data.token) {
            // for social, this token doesn't exist.
            AsyncStorage.setItem('guest_token', data.data.token);
        }
        console.log('Call proxy API to sign in: accounts/signIn');
        let provider = this.state.socialType;
        let options = {
            method: 'POST',
            url: Config.API_HOST + 'accounts/signIn?provider=' + provider,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            data: data
        }

        axios(options).then(response => {
            if(response.status === 200) {
                console.log(response);
                console.log(response.data)
                this.setState({
                    loading: false
                }, () => {
                    AsyncStorage.setItem('oio_auth', JSON.stringify(response.data));
                    this.props.navigation.navigate('Main', response.data);                        
                })                
            }
        })
        .catch(error => {
            console.log(error.response.data);
            if(provider === 'oauth' && this.state.apiCallCount === 0) {
                this.setState({
                    apiCallCount: 1
                },() => {
                    this._oauthLogin(data);
                })
            }else {
                this.setState({
                    apiCallCount: 0,
                    loading: false
                }, () => {
                    Alert.alert('Error', 'Login failed');
                })
            }
        })        
    }

    handleInit = () => {
        this.props.navigation.navigate("Init");
    };

    showHidePassword() {
        this.setState({showPassword: !this.state.showPassword});
    };

    render() {
        const {email, password, showPassword, loading} = this.state;
        return (
            <ImageBackground source={imgBkg} style={{width: '100%', height: '100%'}}>
                { Config.ENVIRONMENT === 'DEV' && <CornerLabel>DEV</CornerLabel> }
                <LoadingOverlay loading={loading}/>
                <KeyboardAvoidingView style={styles.container}>
                    <Image style={styles.logo} source={imgLogo}/>
                    <View style={styles.titleTextWrapper}>
                        <GothamBookText style={styles.readyText}>Ready to be inspired?</GothamBookText>
                    </View>
                    <GothamBookText style={styles.descriptionText}>VolunteerCrowd finds amazing volunteer projects for students.</GothamBookText>
                    <View style={styles.inputView}>
                        <TextInput
                            autoCapitalize={'none'}
                            placeholderTextColor="#606060"
                            placeholder="Email Address"
                            onChangeText={(email) => this.setState({email})}
                            value={email}
                            keyboardType="email-address"
                            style={styles.textInput}
                        />
                    </View>
                    <View style={styles.inputView}>
                        <TextInput
                            autoCapitalize={'none'}
                            style={styles.textInput}
                            placeholderTextColor="gray"
                            placeholder="Password"
                            onChangeText={(password) => this.setState({password})}
                            secureTextEntry={showPassword}
                            value={password}
                        />

                        <TouchableOpacity onPress={this.showHidePassword} style={styles.showPassBtn} value={!showPassword}>
                            <GothamNotRoundedMediumText style={styles.showPassText}>{showPassword ? 'Show' : 'Hide'}</GothamNotRoundedMediumText>
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity style={styles.forgetPassWrapper} onPress={() => Linking.openURL('https://volunteercrowd.com/')}>
                        <GothamNotRoundedMediumText style={styles.forgetPassText}>Forgot My Password</GothamNotRoundedMediumText>
                    </TouchableOpacity>

                    <PrimaryButton onPress={this.handleLogin} title="SIGN IN" />
                    <GothamNotRoundedMediumText
                        style={styles.orText}>
                        Or sign in with
                    </GothamNotRoundedMediumText>
                    <View style={styles.socialRow}>
                        <View style={styles.socialButtonWrapper}>
                            <SocialButton type='facebook_blue' onPress={()=>this.handleSocialLogin('facebook')}/>
                        </View>
                        <View style={styles.socialButtonWrapper}>
                            <SocialButton type='google_blue' onPress={()=>this.handleSocialLogin('google')}/>
                        </View>
                        <View style={styles.socialButtonWrapper}>
                            <SocialButton type='instagram_blue' onPress={()=>this.handleSocialLogin('instagram')}/>
                        </View>
                        <View style={styles.socialButtonWrapper}>
                            <SocialButton type='snapchat_blue' onPress={()=>this.handleSocialLogin('snapchat')}/>
                        </View>
                        {/* <View style={styles.socialButtonWrapper}>
                            <SocialButton type='email_blue'/>
                        </View> */}
                    </View>

                    <GothamNotRoundedMediumText
                        style={styles.lastText}>
                        New to VolunteerCrowd?
                    </GothamNotRoundedMediumText>
                    <TouchableOpacity onPress={this.handleInit}>
                        <GothamNotRoundedMediumText style={[styles.lastText, {color: '#24B6EC'}]}>
                            Create an Account
                        </GothamNotRoundedMediumText>
                    </TouchableOpacity>
                </KeyboardAvoidingView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0
    },
    logo: {
        marginTop: 25,
        width: 148,
        height: 56,
        resizeMode: 'contain',
    },
    textInput: {
        height: 50, 
        borderBottomColor: '#606060', 
        borderBottomWidth: 1.5,
        width: '100%',
        paddingHorizontal: 13,
        marginTop: 8,
        paddingBottom: 0        
    },
    inputView: {
        position: 'relative',
        width: '100%',
        paddingHorizontal: 21
    },
    showPassBtn: {
        position: 'absolute',
        right: 21,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 12,
        paddingVertical: 18
    },
    showPassText: {
        color: '#24B6EC',
        fontSize: 13
    },
    forgetPassWrapper: {
        width: 334,
        height: 20,
        marginTop: 9,
        marginBottom: 20,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    forgetPassText: {
        color: '#24B6EC',
        fontSize: 14
    },
    titleTextWrapper: {
        marginTop: 35,
        marginBottom: 20,
    },
    signinBtn: {
        backgroundColor: '#1D75BD',
        width: 250,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        marginBottom: 40,
    },
    signinText: {
        color: 'white',
        fontSize: 20
    },
    socialRow: {
        width: '100%',
        height: 38,
        marginTop: 5,
        marginBottom: 37,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    socialText: {
        color: 'white',
        fontSize: 24,
    },
    socialButtonWrapper: {
        marginLeft: 10,
    },
    readyText: {
        textAlign: 'center', 
        fontSize: 20, 
        color: '#303030',
        lineHeight: 26,
        textTransform: 'uppercase'
    },
    descriptionText: {
        textAlign: 'center', 
        fontSize: 18, 
        color: '#333333',
        lineHeight: 22,
        width: 290
    },
    lastText: {
        textAlign: 'center', 
        fontSize: 14, 
        lineHeight: 23, 
        color: '#303030'
    },
    orText: {
        textAlign: 'center', 
        fontSize: 14, 
        marginTop: 44, 
        color: '#303030'
    }
});
