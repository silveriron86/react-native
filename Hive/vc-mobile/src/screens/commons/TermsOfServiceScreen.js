import React from 'react';
import {ScrollView, StatusBar, StyleSheet, View} from 'react-native';

import {GothamBookText,} from '../../components/index';

import Colors from '../../constants/Colors';
import CommonNavBar from './CommonNavBar';

export default class TermsOfServiceScreen extends React.Component {
    static navigationOptions = {
        header: null,
        title: 'Terms of Service',
    };

    render() {
        const {navigation} = this.props;
        return (
            <View style={styles.mainView}>
                <StatusBar backgroundColor="blue" barStyle="light-content"/>
                <CommonNavBar
                    onLeftButtonPress={() => navigation.toggleDrawer()}
                    title="Terms of Service"
                    onBack={() => navigation.goBack()}
                />
                <ScrollView>
                    <View style={styles.container}>
                        <GothamBookText
                            style={styles.textTitle}>
                            Conditions of Use
                        </GothamBookText>
                        <View style={styles.padding}></View>

                        <GothamBookText
                            style={styles.textDetails}>
                            We will provide their services to you, which are subject to the conditions stated below in this document. Every time you visit this app, use its services or make a purchase, you accept the following conditions. This is why we urge you to read them carefully.
                        </GothamBookText>
                        <GothamBookText
                            style={styles.textTitle}>
                            Privacy Policy
                        </GothamBookText>
                        <View style={styles.padding}></View>
                        <GothamBookText
                            style={styles.textDetails}>
                            Before you continue using our app we advise you to read our Privacy Policy regarding our user data collection. It will help you better understand our practices.
                        </GothamBookText>
                        <GothamBookText
                            style={styles.textTitle}>
                            Communications
                        </GothamBookText>
                        <View style={styles.padding}></View>

                        <GothamBookText
                            style={styles.textDetails}>
                            The entire communication with us is electronic. Every time you send us an email or visit our app, you are going to be communicating with us. You hereby consent to receive communications from us. If you subscribe to the news from our app, you are going to receive regular emails from us. We will continue to communicate with you by posting news and notices on our website and by sending you emails. You also agree that all notices, disclosures, agreements and other communications we provide to you electronically meet the legal requirements that such communications be in writing.
                        </GothamBookText>
                        <GothamBookText
                            style={styles.textTitle}>
                            Applicable Law
                        </GothamBookText>
                        <View style={styles.padding}></View>
                        <GothamBookText
                            style={styles.textDetails}>
                            By visiting this app, you agree that the laws of the [your location], without regard to principles of conflict laws, will govern these terms of service, or any dispute of any sort that might come between [name] and you, or its business partners and associates.
                        </GothamBookText>
                    </View>
                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: Colors.commonPageBackground,
        alignItems: 'center',
    },
    container: {
        margin: 15,
        backgroundColor: Colors.white,
        borderRadius: 8,
        // shadowOffset: {width: 10, height: 10},
        // shadowColor: 'black',
        // shadowOpacity: 1,
        // elevation: 3,
    },
    textTitle: {
        fontSize: 14,
        fontWeight: 'bold',
        margin: 10,
        color: Colors.titleText,
    },
    padding: {
        height: 0.5,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: Colors.black,
    },
    textDetails: {
        fontSize: 11,
        margin: 10,
        color: Colors.detailsText
    },
});
