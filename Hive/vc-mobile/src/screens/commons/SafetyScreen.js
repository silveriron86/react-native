import React from 'react';
import {ScrollView, StatusBar, StyleSheet, View} from 'react-native';

import {GothamBookText,} from '../../components/index';

import Colors from '../../constants/Colors';
import CommonNavBar from './CommonNavBar';

export default class SafetyScreen extends React.Component {
    static navigationOptions = {
        header: null,
        title: 'Safety',
    };

    render() {
        const {navigation} = this.props;
        return (
            <View style={styles.mainView}>
                <StatusBar backgroundColor="blue" barStyle="light-content"/>
                <CommonNavBar
                    onLeftButtonPress={() => navigation.toggleDrawer()}
                    title="Safety"
                    onBack={() => navigation.goBack()}
                />
                <ScrollView>
                    <View style={styles.container}>
                        <GothamBookText
                            style={styles.textDetails}>
                            We are committed to ensuring that your information is secure. In order to prevent
                            unauthorized access or disclosure we have put in place suitable physical, electronic and
                            managerial procedures to safeguard and secure the information we collect online.
                        </GothamBookText>
                        <GothamBookText
                            style={styles.textTitle}>
                            Controlling your personal information
                        </GothamBookText>
                        <View style={styles.padding}></View>
                        <GothamBookText
                            style={styles.textDetails}>
                            You may choose to restrict the collection or use of your personal information in the
                            following ways:{"\n"}{"\n"}
                            •.whenever you are asked to fill in a form on the
                            website, look for the box that you can click to
                            indicate that you do not want the information to be
                            used by anybody for direct marketing purposes{"\n"}{"\n"}
                            • if you have previously agreed to us using your
                            personal information for direct marketing
                            purposes, you may change your mind at any time
                            by writing to or emailing us at [email address]{"\n"}{"\n"}
                            We will not sell, distribute or lease your personal information to third parties unless we
                            have your permission or are required by law. We may use your personal information to send
                            you promotional information about third parties which we think you may find interesting if
                            you tell us that you wish this to happen.{"\n"}{"\n"}
                            You may request details of personal information which we hold about you under the Data
                            Protection Act 1998. A small fee will be payable. If you would like a copy of the
                            information held on you please write to [address].{"\n"}{"\n"}
                            If you believe that any information we are holding on you is incorrect or incomplete, please
                            write to or email us as soon as possible, at the above address. We will promptly correct any
                            information found to be incorrect.
                        </GothamBookText>
                    </View>
                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: Colors.commonPageBackground,
        alignItems: 'center',
    },
    container: {
        margin: 15,
        backgroundColor: Colors.white,
        borderRadius: 8,
        // shadowOffset: {width: 10, height: 10},
        // shadowColor: 'black',
        // shadowOpacity: 1,
        // elevation: 3,
    },
    textTitle: {
        fontSize: 14,
        fontWeight: 'bold',
        margin: 10,
        color: Colors.titleText,
    },
    padding: {
        height: 0.5,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: Colors.black,
    },
    textDetails: {
        fontSize: 11,
        margin: 10,
        color: Colors.detailsText
    },
});
