import React from 'react';
import {ScrollView, StatusBar, StyleSheet, View} from 'react-native';

import {GothamBookText,} from '../../components/index';

import Colors from '../../constants/Colors';
import CommonNavBar from './CommonNavBar';

export default class PrivacyPolicyScreen extends React.Component {
    static navigationOptions = {
        header: null,
        title: 'Privacy Policy',
    };

    render() {
        const {navigation} = this.props;
        return (
            <View style={styles.mainView}>
                <StatusBar backgroundColor="blue" barStyle="light-content"/>
                <CommonNavBar
                    onLeftButtonPress={() => navigation.toggleDrawer()}
                    title="Privacy Policy"
                    onBack={() => navigation.goBack()}
                />
                <ScrollView>
                    <View style={styles.container}>
                        <GothamBookText
                            style={styles.textDetails}>
                            This privacy policy sets out how VolunteerCrowd uses and protects any information that you give VolunteerCrowd when you use this website.{"\n"}{"\n"}
                            VolunteerCrowd is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.{"\n"}{"\n"}
                            VolunteerCrowd may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from January 01, 2019.{"\n"}
                        </GothamBookText>
                        <GothamBookText
                            style={styles.textTitle}>
                            What we collect
                        </GothamBookText>
                        <View style={styles.padding}></View>
                        <GothamBookText
                            style={styles.textDetails}>
                            * name and job title{"\n"}{"\n"}
                            * contact information including email address{"\n"}{"\n"}
                            * demographic information such as postcode, preferences and interests{"\n"}{"\n"}
                            * other information relevant to customer surveys and/or offers{"\n"}{"\n"}
                        </GothamBookText>
                        <GothamBookText
                            style={styles.textTitle}>
                            What we do with the information we gather
                        </GothamBookText>
                        <View style={styles.padding}></View>
                        <GothamBookText
                            style={styles.textDetails}>
                            We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:{"\n"}{"\n"}

                            Internal record keeping.{"\n"}{"\n"}
                            We may use the information to improve our products and services.{"\n"}{"\n"}
                            We may periodically send promotional email about new products, special offers or other information which we think you may find interesting using the email address which you have provided.{"\n"}{"\n"}
                            From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail.{"\n"}{"\n"}
                            We may use the information to customize the website according to your interests.{"\n"}{"\n"}
                            We may provide your information to our third party partners for marketing or promotional purposes.{"\n"}{"\n"}
                            We will never sell your information.{"\n"}{"\n"}
                        </GothamBookText>
                    </View>
                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: Colors.commonPageBackground,
        alignItems: 'center',
    },
    container: {
        margin: 15,
        backgroundColor: Colors.white,
        borderRadius: 8,
        // shadowOffset: {width: 10, height: 10},
        // shadowColor: 'black',
        // shadowOpacity: 1,
        // elevation: 3,
    },
    textTitle: {
        fontSize: 14,
        fontWeight: 'bold',
        margin: 10,
        color: Colors.titleText,
    },
    padding: {
        height: 0.5,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: Colors.black,
    },
    textDetails: {
        fontSize: 11,
        margin: 10,
        color: Colors.detailsText
    },
});
