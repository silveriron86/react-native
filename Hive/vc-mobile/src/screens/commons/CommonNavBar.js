import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import Colors from "../../constants/Colors";
import {GothamMediumText} from "../../components";
const leftArrow = require('../../../assets/icons/left_arrow_white.png');

function CommonNavBar(props) {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => props.onBack()} style={styles.backButton}>
                <Image source={leftArrow}/>                
            </TouchableOpacity>

            <View style={styles.title}>
                <GothamMediumText style={styles.titleText}>{props.title}</GothamMediumText>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingTop: 26,
        paddingLeft: 10,
        paddingRight: 21,
        backgroundColor: Colors.tintColor
    },
    backButton: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50
    },
    title: {
        flex: 1,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: Colors.white
    }
});

export default CommonNavBar;
 