import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    StatusBar,
    Dimensions,
    TouchableOpacity
} from 'react-native';

import SendSMS from 'react-native-sms';
import email from 'react-native-email';
import { ShareDialog } from 'react-native-fbsdk';
import { GothamBoldText, GothamNotRoundedBookText } from '../../components';
import Utils from '../../utils';
import General from '../../constants/General';

export default class InviteFriendsModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shareLinkContent: {
                contentType: "link",
                contentUrl: General.SHARE_URL_FROM_INVITE_SCR
            }            
        }
    }

    onShare = (type) => {
        let subject = 'Check out this super-easy volunteer project app called VolunteerCrowd!'
        let body = `Hey, I just wanted to share this app with you called VolunteerCrowd. You can find and track new volunteer projects super easy!! ${this.state.shareLinkContent.contentUrl} Check it out and pass it along!`
        if(type === 'facebook') {
            this.shareLinkWithShareDialog();          
        }else if(type === 'email') {
            email([], {
                subject: subject,
                body: body
            }).catch(console.error)
        }else if(type === 'sms') {
            SendSMS.send({
                body: body,
                recipients: [],
                successTypes: ['sent', 'queued'],
                allowAndroidSendWithoutReadPermission: true
            }, (completed, cancelled, error) => {         
                console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);         
            });            
        }
    }

    shareLinkWithShareDialog() {
        var tmp = this;
        console.log(this.state.shareLinkContent);
        ShareDialog.canShow(this.state.shareLinkContent).then(
            function(canShow) {
                console.log(canShow, tmp.state.shareLinkContent);
                if (canShow) {
                    return ShareDialog.show(tmp.state.shareLinkContent);
                }
            }
        ).then(
            function(result) {
                if (result.isCancelled) {
                    console.log('Share cancelled');
                } else {
                    console.log('Share success with postId: '
                    + result.postId);
                }
            },
            function(error) {
                console.log('Share fail with error: ' + error);
            }
        );
    }    

    render() {
        return (
        <View style={styles.container}>
            <StatusBar hidden={true} />
            <Image style={styles.background} source={require('../../../assets/images/invite_bg.jpg')} />
            <Image style={styles.background} source={require('../../../assets/images/gradient_bg.png')} />

            <View style={styles.topRadius}>
                <Image style={[styles.background, styles.rounded]} source={require('../../../assets/images/top_rounded.png')} />
            </View>
            <View style={styles.modalWrapper}>
                <View style={styles.title}>
                    <GothamBoldText style={styles.titleText}>GIVE BACK TOGETHER, INVITE FRIENDS TO VOLUNTEER</GothamBoldText>
                </View>
                <Image style={styles.groupIcon} source={require('../../../assets/icons/group2.png')} />

                <View style={styles.socialActions}>
                    <TouchableOpacity style={styles.actionRowWrapper} onPress={()=>this.onShare('facebook')}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/facebook.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Share on Facebook</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity>

                    {/* <TouchableOpacity style={styles.actionRowWrapper}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/instagram.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Share on Instagram</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.actionRowWrapper}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/snapchat.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Share on Snapchat</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity> */}

                    <TouchableOpacity style={styles.actionRowWrapper} onPress={()=>this.onShare('email')}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/email.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Email</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.actionRowWrapper} onPress={()=>this.onShare('sms')}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/text.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Text</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>

            <TouchableOpacity onPress={this.props.hidePanel} style={styles.closeBtn}>
                <Image source={require('../../../assets/icons/close.png')} />
            </TouchableOpacity>     
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: Dimensions.get('window').width,
      alignItems: 'center'
    },
    closeBtn: {
        position: 'absolute',
        left: 17,
        top: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 16
    },  
    background: {
        width: '100%',
        height: 287,
        resizeMode: 'cover',
        position: 'absolute'
    },
    rounded: {
        width: Dimensions.get('window').width,
        height: 499,
        resizeMode: 'stretch'
    },
    title: {
        width: 339,
        marginTop: 50
    },
    titleText: {
        color: 'white',
        fontSize: 22,
        lineHeight: 26,
        textTransform: 'uppercase',
        textAlign: 'center'
    },
    topRadius: {
        borderWidth: 1,
        borderColor: 'red',
        position: 'absolute',
        left: 0,
        marginTop: 172,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    modalWrapper: {
        flex: 1,
        alignItems: 'center'
    },
    groupIcon: {
        top: 3
    },
    actionRowWrapper: {
        borderBottomWidth: 1,
        borderBottomColor: '#E0E2EE',
        justifyContent: 'flex-start',
        width: Dimensions.get('window').width - 42,
        paddingTop: 12,
        paddingBottom: 8
    },
    actionRow: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    actionIcon: {
        tintColor: '#1D75BD',
        width: 40,
        height: 38,
        marginLeft: 23
    },
    actionText: {
        color: '#303030',
        fontSize: 16,
        marginLeft: 20,
        marginTop: 5,
        lineHeight: 20.2
    },
    socialActions: {
        marginTop: 40
    }
});

 