import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import { Calendar } from 'react-native-calendars';
import { ScrollView } from 'react-native-gesture-handler';
import {PrimaryButton} from "../../components";

const SELECTED_DATE_COLOR = '#4A90E2';
const SELECTED_DATE_STYLE = {
    container: {
        backgroundColor: SELECTED_DATE_COLOR,
        borderWidth: 2,
        borderColor: '#CACACA'
    },
    text: {
        color: 'white',
        marginTop: 7
    }
};

export default class CalendarScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: navigation.getParam('data').title,
    });

    constructor(props) {
      super(props);
      this.state = {
        
      };
    }

    onPressNext = () => {
        const nav = this.props.navigation;
        nav.navigate('Request', {
            data: nav.getParam('data')
        });
    }

    componentWillMount() {
        // let data = this.props.navigation.getParam('data');
        // console.log(data);
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Calendar
                        markingType={'custom'}
                        markedDates={{
                            '2019-02-13': {
                                selected: true, 
                                customStyles: SELECTED_DATE_STYLE
                            },
                            '2019-02-15': {
                                selected: true, 
                                customStyles: SELECTED_DATE_STYLE
                            },
                            '2019-02-16': {
                                selected: true, 
                                customStyles: SELECTED_DATE_STYLE
                            },
                            '2019-02-22': {
                                selected: true, 
                                customStyles: SELECTED_DATE_STYLE
                            }
                        }}
                        style={{
                            marginTop: 10,
                            paddingLeft: 0,
                            paddingRight: 0,
                        }}
                        // Specify theme properties to override specific styles for calendar parts. Default = {}
                        theme={{
                            'stylesheet.calendar.header': {
                                week: {
                                    marginTop: 7,
                                    flexDirection: 'row',
                                    justifyContent: 'space-around',
                                    backgroundColor: '#8DC63F',
                                    height: 27                                
                                },
                                monthText: {
                                    textTransform: 'uppercase',
                                    color: '#606060',
                                    fontSize: 16
                                },
                                dayHeader: {
                                    fontSize: 12,
                                    textTransform: 'uppercase',
                                    color: 'white',
                                    marginTop: 7
                                }
                            },
                            backgroundColor: '#ffffff',
                            calendarBackground: '#ffffff',
                            textSectionTitleColor: '#ffffff',
                            selectedDayTextColor: '#ffffff',
                            todayTextColor: '#00adf5',
                            dayTextColor: '#2d4150',
                            textDisabledColor: '#d9e1e8',
                            arrowColor: '#1D75BD',
                            monthTextColor: '#606060',
                            textMonthFontWeight: 'bold',
                            textDayFontSize: 12,
                            textMonthFontSize: 16,
                            textDayHeaderFontSize: 16
                        }}
                    /> 

                    <Text style={styles.availableDaysText}>Available Shifts for Selected Days</Text>  

                    <Text style={styles.availableDate}>NOVEMBER 15, 2018</Text>
                    <ScrollView horizontal={true} style={styles.scrollView}>
                        <TouchableOpacity style={[styles.timeRangerBtn, {backgroundColor: '#8DC63F', marginLeft: 21}]}>
                            <Text style={[styles.timeRangerText, {color: 'white'}]}>11:00 - 12:00 PM</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[styles.timeRangerBtn]}>
                            <Text style={styles.timeRangerText}>11:00 - 12:00 PM</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[styles.timeRangerBtn, {marginRight: 21}]}>
                            <Text style={styles.timeRangerText}>11:00 - 12:00 PM</Text>
                        </TouchableOpacity>
                    </ScrollView>

                    <Text style={styles.availableDate}>NOVEMBER 21, 2018</Text>
                    <ScrollView horizontal={true} style={styles.scrollView}>
                        <TouchableOpacity style={[styles.timeRangerBtn, {marginLeft: 21}]}>
                            <Text style={styles.timeRangerText}>11:00 - 12:00 PM</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[styles.timeRangerBtn, {backgroundColor: '#8DC63F'}]}>
                            <Text style={[styles.timeRangerText, {color: 'white'}]}>11:00 - 12:00 PM</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[styles.timeRangerBtn, {marginRight: 21}]}>
                            <Text style={styles.timeRangerText}>11:00 - 12:00 PM</Text>
                        </TouchableOpacity>
                    </ScrollView>

                    <Text style={styles.availableDate}>NOVEMBER 23, 2018</Text>
                    <ScrollView horizontal={true} style={styles.scrollView}>
                        <TouchableOpacity style={[styles.timeRangerBtn, {marginLeft: 21}]}>
                            <Text style={styles.timeRangerText}>11:00 - 12:00 PM</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[styles.timeRangerBtn]}>
                            <Text style={styles.timeRangerText}>11:00 - 12:00 PM</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[styles.timeRangerBtn, {marginRight: 21, backgroundColor: '#8DC63F'}]}>
                            <Text style={[styles.timeRangerText, {color: 'white'}]}>11:00 - 12:00 PM</Text>
                        </TouchableOpacity>
                    </ScrollView>
                    <View style={styles.paddingArea}></View>
                </ScrollView>

                <PrimaryButton onPress={this.onPressNext} style={styles.nextBtn}
                               title="Next" />
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    availableDaysText: {
        color: '#303030',
        fontSize: 14,
        textAlign: 'center',
        marginTop: 20
    },
    availableDate: {
        fontSize: 10,
        color: '#303030',
        marginLeft: 21,
        marginTop: 20
    },
    timeRangerBtn: {
        backgroundColor: 'white',
        width: 138,
        height: 40,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000000',
        shadowOpacity: 0.3,
        shadowOffset: {width: 1, height: 1},
        elevation: 5,
        marginRight: 11
    },
    timeRangerText: {
        color: '#303030',
        fontSize: 13
    },
    scrollView: {
        paddingTop: 10,
        height: 52
    },
    paddingArea: {
        width: '100%',
        height: 50
    },
    nextText: {
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 16
    },
    nextBtn: {
        backgroundColor: '#1D75BD',
        width: 250,
        height: 40,
        position: 'absolute',
        bottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        left: '50%',
        marginLeft: -125
    }
});
