import React from 'react';
import {StatusBar, StyleSheet, Image, TouchableOpacity, View, Linking} from 'react-native';
import MapView from 'react-native-maps';
import openMap from 'react-native-open-maps';
import Permissions from 'react-native-permissions';
import { GothamNotRoundedMediumText } from '../../components';

const latLng = {latitude: 37.78825, longitude: -122.4324};
export default class MapModal extends React.Component {
    state = {
        mapRegion: {latitude: 37.78825, longitude: -122.4324, latitudeDelta: 0.0922, longitudeDelta: 0.0421},
        locationResult: null,
        location: {coords: latLng},
        locationPermission: null,
    };
    _handleMapRegionChange = mapRegion => {
        this.setState({mapRegion});
    };
    _getLocationAsync = async () => {
        // Permissions.request('location', {type: 'always'}).then(response => {
            // status = response;
            // this.setState({locationPermission: response});
        // });

        // console.log(this.state.locationPermission)
        // if (this.state.locationPermission === 'authorized') {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const initialPosition = JSON.stringify(position);
                    console.log(initialPosition)
                    this.setState({
                        location: {
                            coords: {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude
                            }
                        }
                    });
                },
                (error) => alert(error.message),
                {enableHighAccuracy: true, timeout: 5000, maximumAge: 10000}
            );
        // }
    };

    componentDidMount() {
        this._getLocationAsync();
    }

    handleConfirm = () => {
        this.props.handleConfirm();
        openMap(latLng);
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar hidden={true}/>
                <View style={styles.header}>
                    <GothamNotRoundedMediumText style={styles.headerText}>Project Address</GothamNotRoundedMediumText>
                </View>
                <View style={{backgroundColor: 'white', padding: 18}}>
                    <MapView
                        style={{alignSelf: 'stretch', height: 300}}
                        region={{
                            latitude: this.state.location.coords.latitude,
                            longitude: this.state.location.coords.longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421
                        }}
                        onRegionChange={this._handleMapRegionChange}
                    >
                        <MapView.Marker
                            coordinate={this.state.location.coords}
                            // image={require('../assets/pin.png')}
                        />
                    </MapView>
                </View>
                <TouchableOpacity onPress={this.handleConfirm} style={styles.confirmBtn}>
                    <GothamNotRoundedMediumText style={styles.confirmText}>OPEN IN MAPS</GothamNotRoundedMediumText>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'center',
        marginTop: 37
    },
    header: {
        backgroundColor: '#1D75BD',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50
    },
    headerText: {
        marginTop: 3,
        fontSize: 18,
        letterSpacing: -0.24,
        color: 'white'
    },
    confirmBtn: {
        backgroundColor: '#F1F1F1',
        height: 50,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    confirmText: {
        color: '#1D75BD',
        fontSize: 14
    }
});
