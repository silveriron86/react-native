import React from 'react';
import {Alert, Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import FirstStepModal from './FirstStepModal';
import RoundCheckbox from 'rn-round-checkbox';
import {GothamBookText, GothamMediumText, PrimaryButton} from "../../components";
import Colors from "../../constants/Colors";

export default class RequestScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: navigation.getParam('data').title,
    });

    constructor(props) {
        super(props);
        this.state = {
            isPanelOpen: false,
            // isSelectedPush: false,
            isSelectedEmail: false,
            isSelectedText: false,
            data: null
        };
    }

    onSendRequest = () => {
        // if (this.state.isSelectedPush === true) {
        //     Alert.alert(
        //         '"VolunteerCrowd" would like to send you push notifications.',
        //         'You can receive updates about volunteer projects, include new opportunities, confirmations and reminders. Change your alert preference in settings',
        //         [
        //             {
        //                 text: 'Cancel',
        //                 style: 'cancel',
        //             },
        //             {text: 'OK', onPress: () => this.setState({isPanelOpen: true})},
        //         ],
        //         {cancelable: false},
        //     );
        // } else {
            this.setState({isPanelOpen: true})
        // }
    };

    hidePanel = () => {
        this.setState({
            isPanelOpen: false
        })
    };

    checkUncheckPush = (params) => {
        this.setState({isSelectedPush: !this.state.isSelectedPush});
    };

    checkUncheckEmail = (params) => {
        this.setState({isSelectedEmail: !this.state.isSelectedEmail});
    };

    checkUncheckText = (params) => {
        this.setState({isSelectedText: !this.state.isSelectedText});
    };

    componentDidMount() {
        let data = this.props.navigation.getParam('data');
        this.setState({
            data: data
        })
    }

    render() {
        const { isPanelOpen, data } = this.state;

        return (
            <View style={styles.container}>
                <Image style={styles.background} source={require('../../../assets/images/background4.png')}/>
                <View style={styles.wrapper}>
                    <GothamMediumText style={styles.headerTextTitle}>I’M IN!</GothamMediumText>
                    {
                        data &&
                        <View style={styles.center}>
                            <View style={styles.causeItem}>
                                <View style={styles.imgWrapper}>
                                    <Image source={data.type}/>
                                </View>
                            </View>
                        </View>
                    }
                    <View style={styles.table}>
                        <View style={styles.row}>
                            <View style={styles.col}>
                                <GothamMediumText style={styles.dateTimeLabelText}>DATE SELECTED</GothamMediumText>
                                <GothamBookText style={styles.dateTimeValueText}>November 13, 2018</GothamBookText>
                            </View>
                            <View style={styles.col}>
                                <GothamMediumText style={styles.dateTimeLabelText}>TIME SELECTED</GothamMediumText>
                                <GothamBookText style={styles.dateTimeValueText}>11:00 - 12:00 PM</GothamBookText>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.col}>
                                <GothamMediumText style={styles.dateTimeLabelText}>DATE SELECTED</GothamMediumText>
                                <GothamBookText style={styles.dateTimeValueText}>November 13, 2018</GothamBookText>
                            </View>
                            <View style={styles.col}>
                                <GothamMediumText style={styles.dateTimeLabelText}>TIME SELECTED</GothamMediumText>
                                <GothamBookText style={styles.dateTimeValueText}>9:00 - 10:00 AM</GothamBookText>
                            </View>
                        </View>
                    </View>

                    <GothamMediumText style={styles.textTitle}>How should we update you about your volunteer
                        projects?</GothamMediumText>
                    {/*<Text style={styles.text}>How should we update you about your volunteer projects?</Text>*/}
                    <View>
                        {/*<ImageBackground*/}
                        {/*resizeMode={'cover'} // stretch or cover*/}
                        {/*style={styles.textAreaContainer} // must be passed from the parent, the number may vary depending upon your screen size*/}
                        {/*source={topBackground}>*/}
                        <View>
                            <View style={{flexDirection: 'row', marginTop: 15}}>
                                <RoundCheckbox
                                    size={24}
                                    checked={this.state.isSelectedPush}
                                    iconColor="#fff"
                                    backgroundColor="#8DC442"
                                    onValueChange={(newValue) => {
                                        this.checkUncheckPush();
                                    }}
                                />
                                <GothamBookText style={{marginLeft: 10, marginTop: 6}}>PUSH
                                    NOTIFICATION</GothamBookText>
                            </View>
                            <View style={{flexDirection: 'row', marginTop: 5}}>
                                <RoundCheckbox
                                    size={24}
                                    checked={this.state.isSelectedEmail}
                                    iconColor="#fff"
                                    backgroundColor="#8DC442"
                                    onValueChange={(newValue) => {
                                        this.checkUncheckEmail();
                                    }}
                                />
                                <GothamBookText style={{marginLeft: 10, marginTop: 6}}>EMAIL</GothamBookText>
                            </View>
                            <View style={{flexDirection: 'row', marginTop: 5}}>
                                <RoundCheckbox
                                    size={24}
                                    checked={this.state.isSelectedText}
                                    iconColor="#fff"
                                    backgroundColor="#8DC442"
                                    onValueChange={(newValue) => {
                                        this.checkUncheckText();
                                    }}
                                />
                                <GothamBookText style={{marginLeft: 10, marginTop: 6}}>TEXT MESSAGE</GothamBookText>
                            </View>

                        </View>
                        {/*</ImageBackground>*/}
                    </View>

                    <View style={styles.footer}>
                        <PrimaryButton onPress={this.onSendRequest} style={styles.nextBtn}
                                       title="SEND REQUEST" />

                    </View>
                </View>

                <ModalWrapper
                    containerStyle={{flexDirection: 'row', justifyContent: 'flex-end'}}
                    onRequestClose={() => this.setState({isPanelOpen: false})}
                    shouldAnimateOnRequestClose={true}
                    position="right"
                    visible={isPanelOpen}>
                    <FirstStepModal 
                        hidePanel={this.hidePanel} 
                        navigation={this.props.navigation}
                        description="We’ll alert you when your shift is confirmed."/>
                </ModalWrapper>                 
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper: {
        margin: 20,
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 2,
        shadowColor: '#CACACA',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 1,
        elevation: 4,
        padding: 22
    },
    background: {
        resizeMode: 'cover',
        position: 'absolute'
    },
    title: {
        color: Colors.titleText,
        fontSize: 20,
        textAlign: 'center'
    },
    headerTextTitle: {
        color: Colors.titleText,
        fontSize: 20,
        textAlign: 'center',
    },
    dateTimeLabelText: {
        color: Colors.dateTimeTileText,
        fontSize: 10,
    },
    dateTimeValueText: {
        color: Colors.titleText,
        fontSize: 14,
    },
    textTitle: {
        color: Colors.titleText,
        fontSize: 14,
    },
    footer: {
        width: '100%',
        alignItems: 'center'
    },
    nextText: {
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 16,
        fontWeight: 'bold'
    },
    nextBtn: {
        backgroundColor: '#1D75BD',
        width: 250,
        height: 40,
        marginTop: 68,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8
    },
    causeItem: {
        width: 92,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
        marginBottom: 15
    },
    imgWrapper: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        borderWidth: 2,
        borderColor: '#CACACA'
    },
    causeImg: {
        width: 36,
        height: 36
    },
    center: {
        width: '100%',
        alignItems: 'center'
    },
    textAreaContainer: {
        borderColor: '#CACACA',
        borderWidth: 1,
        paddingHorizontal: 15,
        paddingVertical: 8,
        borderRadius: 4,
        marginTop: 6,
    },
    textArea: {
        height: 117,
        justifyContent: "flex-start"
    },
    table: {
        height: 98,
        paddingBottom: 10
    },
    label: {
        color: '#8DC63F',
        fontSize: 10,
        textTransform: 'uppercase'
    },
    text: {
        color: '#303030',
        fontSize: 14
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    },
    col: {
        flex: 1
    }
});
