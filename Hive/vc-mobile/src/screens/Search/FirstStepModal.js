import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    StatusBar,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import InviteFriendsModal from './InviteFriendsModal';
import {PrimaryButton} from "../../components";

export default class FirstStepModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isPanelOpen: false,
        };
    }

    onInviteFriends = () => {
        this.setState({
            isPanelOpen: true
        })
    }

    hideInvitePanel = () => {
        this.setState({
            isPanelOpen: false
        })
    } 

    goProfile = () => {        
        this.props.navigation.navigate('Profile');
    }

    render() {
        const { isPanelOpen } = this.state;
        const { description } = this.props;
        
        return (
            <View style={styles.container}>
                <StatusBar hidden={true} />
                <Image style={styles.background} source={require('../../../assets/images/background5.png')} />
                <Image style={styles.background} source={require('../../../assets/images/gradient_bg.png')} />

                <View style={styles.topRadius}>
                    <Image style={[styles.background, styles.rounded]} source={require('../../../assets/images/top_rounded.png')} />
                </View>

                <View style={styles.modalWrapper}>
                    <View style={styles.title}>
                        <Text style={styles.titleText}>great job in taking the</Text>
                        <Text style={[styles.titleText, {color: '#8DC63F'}]}>first step</Text>
                    </View>

                    <Text style={styles.p}>{description}</Text>
                </View>

                <Image style={styles.rectangle} source={require('../../../assets/images/rectangle4.png')} />
                <View style={styles.footer}>
                    <Text style={styles.label}>WHAT HAPPENS NEXT?</Text>
                    <PrimaryButton onPress={this.onInviteFriends} style={styles.nextBtn}
                                   title="INVITE FRIENDS" />
                    <PrimaryButton onPress={() => 
                            {
                             this.props.hidePanel()
                             this.goProfile()
                            }
                        } 
                        style={[styles.nextBtn, {width: 305}]}
                                   title="EXPLORE VOLUNTEER PROFILE" />
                </View>
                <ModalWrapper
                    containerStyle={{ flexDirection: 'row', justifyContent: 'flex-end' }}
                    onRequestClose={() => this.setState({ isPanelOpen: false })}
                    shouldAnimateOnRequestClose={true}
                    position="right"
                    visible={isPanelOpen}>
                    <InviteFriendsModal hidePanel={this.hideInvitePanel}></InviteFriendsModal>
                </ModalWrapper>    

                <TouchableOpacity onPress={this.props.hidePanel} style={styles.closeBtn}>
                    <Image source={require('../../../assets/icons/close.png')} />
                </TouchableOpacity>                              
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: Dimensions.get('window').width,
      alignItems: 'center'
    },
    closeBtn: {
        position: 'absolute',
        left: 17,
        top: 27,
        zIndex: 99999
    },  
    background: {
        width: '100%',
        position: 'absolute'
    },
    title: {
        width: 230,
        marginTop: 57
    },
    titleText: {
        color: '#1D75BD',
        fontSize: 32,
        textTransform: 'uppercase',
        textAlign: 'center'
    },
    rounded: {
        width: Dimensions.get('window').width,
        resizeMode: 'stretch'
    },    
    topRadius: {
        borderWidth: 1,
        borderColor: 'red',
        position: 'absolute',
        left: 0,
        marginTop: 172
    },
    modalWrapper: {
        flex: 1,
        alignItems: 'center'
    },
    p: {
        color: '#303030',
        fontSize: 16,
        marginTop: 80
    },
    rectangle: {
        resizeMode: 'cover',
        position: 'absolute',
        left: '50%',
        marginLeft: -167,
        marginTop: 400,
        width: 334,
        height: 162
    },
    footer: {
        position: 'absolute',
        marginTop: 412,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        fontSize: 18,
        color: '#303030',
        marginBottom: 5
    },
    nextText: {
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 16
    },
    nextBtn: {
        backgroundColor: '#1D75BD',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        marginTop: 10
    } 
});

 