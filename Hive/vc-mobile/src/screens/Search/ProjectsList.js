// NEEDS REFACTORING
// https://techcoast.atlassian.net/browse/VC-642
// Currently the ProjectList screen is being re-used in 3 screens.
// It should be re-factored so that there is a "card" component, 
// and 3 separate screens (SearchResults, Likes, Projects) that use that component

import React from 'react';
// import { AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Text,
  FlatList,
  Image,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import StarRating from 'react-native-star-rating';
import ImageProgress from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import {
  Grayscale
} from 'react-native-color-matrix-image-filters';

import { GothamNotRoundedMediumText, GothamNotRoundedBookText, CornerLabel } from '../../components';
import ApiConstants from '../../constants/ApiConstants';
const dateIcon = require('../../../assets/icons/calendar.png');
const clockIcon = require('../../../assets/icons/clock_gray.png');
const arrowBlueIcon = require('../../../assets/icons/arrow_blue.png');
const projBottomBg = require('../../../assets/images/proj_bottom_bg.png');
const pinIcon = require('../../../assets/icons/location_icon_blue.png');
import { SearchActions } from '../../actions';

SAMPLE_STATUSES = ['CHECK-IN', 'CHECK OUT', 'CONFIRMED', 'SHOULD_CONFIRM1', 'SHOULD_CONFIRM2', 'COMPLETED'];

const LikeImage = (props) => (
  <>
  {
    (props.item.isOrgLikedByUser || props.item.isProjectLikedByUser) ?
    <Image source={require('../../../assets/icons/liked_filled.png')} style={styles.likesImg}/>
    :
    <Image source={require('../../../assets/icons/heart_unliked_1.png')} style={styles.likesImg}/>
  }
  </>
)

// export default class ProjectsList extends React.Component {
export class ProjectsList extends React.Component {

  renderRow = (item, index) => {

    const { handleSelectRow, handleReviewNow, goProjectDetails, handleLike, data, isSearch, isLike, isProject } = this.props;

    // console.log(`--item--:ProjectsList.js:${JSON.stringify(item)}`)
    // console.log(`--data--:ProjectsList.js:${JSON.stringify(data)}`)

    let isCompleted = (isProject === true) && (item.status === 'COMPLETED');

    const itemNameMaxDisplayLength = 22
    let itemName = item.name ? item.name : item.orgName
    const itemNameOrig = itemName
    if (itemName.length > itemNameMaxDisplayLength) {
      itemName = itemName.substring(0,itemNameMaxDisplayLength-3) + '...'
    }

    const itemName2MaxDisplayLength = 30
    let itemName2 = (itemNameOrig === item.name) ? item.orgName : ''
    if (itemName2.length > itemName2MaxDisplayLength) {
      itemName2 = itemName2.substring(0,itemName2MaxDisplayLength-3) + '...'
    }

    item.status = SAMPLE_STATUSES[index % SAMPLE_STATUSES.length]

    const itemStreet = item.street ? item.street : (item.orgStreet ? item.orgStreet : '' )
    const itemZipcode = item.zipcode ? item.zipcode : (item.orgZipcode ? item.orgZipcode : '' )
    const itemState = item.state ? item.state : (item.orgState ? item.orgState : '' )
    const itemCity = item.city ? item.city : (item.orgCity ? item.orgCity : '' )

    // disabled b/c of crowding issues (VC-683)
    // const itemDescription = item.description ? item.description : (item.missionStatement ? item.missionStatement : '' )
    const itemDescription = ''

    return (
      <View style={[styles.rowWrapper, (isSearch === false) && (index > 0) && {marginTop: 26}]}>
        {
          (isSearch === false) && 
          <View style={isLike === true && {opacity: 0}}>
            {
              item.status &&
              <>
              {
                (item.status === 'CHECK-IN') ?
                <GothamNotRoundedMediumText style={[styles.statusText, {color: '#DB5939'}]}>
                  CHECK-IN
                  <GothamNotRoundedMediumText style={[styles.statusText, {color: '#CACACA'}]}> - CHECK OUT</GothamNotRoundedMediumText>
                </GothamNotRoundedMediumText>
                :
                item.status === 'CHECK OUT' ?
                <GothamNotRoundedMediumText style={[styles.statusText, {color: '#CACACA'}]}>
                  CHECK-IN
                  <GothamNotRoundedMediumText style={[styles.statusText, {color: '#DB5939'}]}> - CHECK OUT</GothamNotRoundedMediumText>
                </GothamNotRoundedMediumText>
                :
                item.status === 'COMPLETED' ?
                <GothamNotRoundedMediumText style={[styles.statusText, {color: '#9b9c9d'}]}>{item.status}</GothamNotRoundedMediumText>
                :
                item.status === 'CONFIRMED' ?
                <GothamNotRoundedMediumText style={[styles.statusText, {color: '#8DC63F'}]}>{item.status}</GothamNotRoundedMediumText>
                :
                item.status === 'SHOULD_CONFIRM1' ?
                <GothamNotRoundedMediumText style={[styles.statusText, {color: '#24B6EC'}]}>PLEASE CONFIRM</GothamNotRoundedMediumText>
                :
                item.status === 'SHOULD_CONFIRM2' ?
                <GothamNotRoundedMediumText style={[styles.statusText, {color: '#606060'}]}>PLEASE CONFIRM</GothamNotRoundedMediumText>
                :
                <></>
              }
              </>
            }
          </View>
        }

        {
          isSearch === false &&
          <View style={index === 0 ? styles.firstDot : styles.dot}></View>
        }
        
        <TouchableOpacity style={[
          styles.row, 
          (index === data.length-1) && styles.lastRow,
          isProject === true && (item.status === 'CHECK-IN' || item.status === 'CHECK OUT') && styles.checkIn,
          isProject === true && item.status === 'CONFIRMED' && styles.confirmed,
          isProject === true && item.status === 'SHOULD_CONFIRM1' && styles.shouldConfirm1,
          isProject === true && item.status === 'SHOULD_CONFIRM2' && styles.shouldConfirm2
        ]} 
        onPress={()=> handleSelectRow(item)}
        >
          <View style={styles.largeRow}>
            <View>
              <View style={styles.avatar}>
                {
                  item.nteeCodeImgURL && 
                  <>
                  {
                    (isCompleted) ?
                      <Grayscale>
                        <ImageProgress source={{uri: item.nteeCodeImgURL}} indicator={Progress.Circle} style={styles.avatarImg} />
                      </Grayscale>
                    :
                      <ImageProgress source={{uri: item.nteeCodeImgURL}} indicator={Progress.Circle} style={styles.avatarImg} />
                  }                  
                  </>
                }
              </View>
              <Image source={projBottomBg} style={styles.projBottom}/>
              <View style={styles.typeWrapper}>
                <View style={styles.type}>
                {
                  item.causeIconURL &&                
                  <ImageProgress
                    source={{uri: item.causeIconURL}}
                    indicator={Progress.Circle}
                    indicatorProps={{
                      size: 30
                    }}
                    imageStyle={(isCompleted === true) ? {tintColor: '#9b9c9d'} : null}
                    style={styles.typeImg} />
                }
                </View>
                <GothamNotRoundedMediumText style={styles.causeName}>{item.cause}</GothamNotRoundedMediumText>
              </View>
            </View>
            <View style={styles.column}>
              <GothamNotRoundedMediumText style={[styles.rowTitle, isCompleted && {color: '#9b9c9d'}]}>{itemName}</GothamNotRoundedMediumText>
              <TouchableOpacity style={[styles.likePlusBtn, item.liked === true && {shadowOpacity: 0}]}
                onPress={()=>handleLike(item, index)}
                // onPress={()=>this.handleLike(item, index)}
              >
                {
                (isCompleted) ? 
                <Grayscale style={{position: 'absolute', left: -7, top: -8}}>
                <LikeImage item={item}/>
                </Grayscale> 
                : 
                <LikeImage item={item}/>
                }                
                <GothamNotRoundedMediumText style={
                  (item.isOrgLikedByUser || item.isProjectLikedByUser) ?
                    [styles.likedCnt, (isCompleted) && {color: '#9b9c9d'}]  
                  :
                    [styles.unLikedCnt, (isCompleted) && {color: '#9b9c9d'}]
                }>
                    {item.sfProjectId ? item.projectTotalLikes : item.orgTotalLikes}
                </GothamNotRoundedMediumText>
 
              </TouchableOpacity>

              {
                (typeof item.rating === 'undefined' || item.rating === null) ?
                <View style={styles.ratingWrap}>
                  {/* <TouchableOpacity onPress={handleReviewNow} style={styles.reviewNowBtn}>
                    <GothamNotRoundedMediumText style={[styles.comingBtnText, {textAlign: 'left', marginTop: 3}]}>Review Now</GothamNotRoundedMediumText>
                  </TouchableOpacity> */}
                  <GothamNotRoundedMediumText style={styles.notYetRated}>NOT YET RATED</GothamNotRoundedMediumText>
                </View>
                :
                <View style={styles.ratingWrap}>
                  <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={item.rating}
                    starSize={14}
                    starStyle={styles.star}
                    containerStyle={styles.ratingContainer}              
                    fullStarColor={isCompleted ? '#9b9c9d' : '#f5a643'}
                    disabled
                  />
                  <GothamNotRoundedBookText style={styles.ratingText}>{item.rating.toFixed(1)}</GothamNotRoundedBookText>
                </View>              
              }

              <View>
              { itemName2.length > 0 &&
                  <Text>
                    <GothamNotRoundedMediumText style={[styles.rowTitle2]}>{itemName2}</GothamNotRoundedMediumText>
                  </Text>
              }
              </View>  

              {/* <Text style={[styles.smallText, styles.rowDescription]} ellipsizeMode='tail' numberOfLines={2}>{itemDescription}</Text> */}
              <Text numberOfLines={1}></Text>
              {
                itemCity && itemState &&
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 2}}>
                  {
                    (isCompleted) ? <Grayscale><Image source={pinIcon}/></Grayscale> : <Image source={pinIcon}/>
                  }
                  {/* <GothamNotRoundedBookText style={[styles.smallText, styles.locationText]}></GothamNotRoundedBookText> */}
                  <Text style={[styles.smallText, styles.locationText]} ellipsizeMode='tail' numberOfLines={1}>
                    {/* {item.street} {item.city},{item.state && ` ${item.state}`} {item.zipcode} */}
                    {itemStreet} {itemCity},{itemState && ` ${itemState}`} {itemZipcode}
                  </Text>
                </View>
              }              
              <Text numberOfLines={1}></Text>
              <View style={{height: 33}}>
              {
                item.sfProjectId ?
                <View style={styles.dateTime}>
                  {
                    (item.shifts && item.shifts.length && item.shifts[0].startDate) ?
                    <View style={styles.dtRow}>
                      <GothamNotRoundedBookText style={[styles.smallText, styles.dateText]}>{moment(item.shifts[0].startDate).format('MMMM D, YYYY')}</GothamNotRoundedBookText>
                    </View> : null
                  }
                  {/* <View style={styles.dtIcon} style={[styles.dtRow, {marginTop: 0}]}>
                    <GothamNotRoundedBookText style={[styles.smallText, styles.dateText]}>{item.shifts[0].startTime}</GothamNotRoundedBookText>
                  </View> */}
                  <TouchableOpacity style={styles.comingBtn} onPress={()=>handleSelectRow(item)}>
                    <GothamNotRoundedMediumText style={[styles.comingBtnText, isCompleted && {color: '#9b9c9d'}]}>I want to volunteer</GothamNotRoundedMediumText>
                    <Image style={[styles.arrowIcon, isCompleted && {tintColor: '#9b9c9d'}]} source={arrowBlueIcon}/>
                  </TouchableOpacity>
                </View>
                :
                <View style={styles.comingSoon}>
                  <GothamNotRoundedBookText style={styles.smallText}>Opportunities coming soon</GothamNotRoundedBookText>
                  <TouchableOpacity style={styles.comingBtn} onPress={()=>handleSelectRow(item)}>
                    <GothamNotRoundedMediumText style={[styles.comingBtnText, isCompleted && {color: '#9b9c9d'}]}>Get alerts for future projects</GothamNotRoundedMediumText>
                    <Image style={[styles.arrowIcon, isCompleted && {tintColor: '#9b9c9d'}]} source={arrowBlueIcon}/>
                  </TouchableOpacity>
                </View>
              }
              </View>
            </View>
          </View>
          {
            isCompleted &&
            <View style={styles.overlayView}></View>
          }
        </TouchableOpacity>

        {/* <View style={index === 0 ? styles.firstLine : styles.vLine}></View> */}

        {
          (isSearch === false || isLike === true) &&
          <>
          {
            (index === 0) ?
            <LinearGradient
              colors={['#8DC63F', '#1D75BD']}
              style={styles.firstLine}
              start={{x: 0, y: 0}}
              end={{x: 0, y: 1}}
            />
            :
            <View style={styles.vLine}></View>
          }
          </>
        }        
      </View>
    );
  }

  onValueChange(value) {
    this.setState({ distance: value });
  }

  multiSliderValuesChange = values => {
    this.setState({
      multiSliderValue: values,
    });
  };  

  apply = () => {

  }

  _keyExtractor = (item, index) => {
    return `project-${index}`;
  };

  render() {
    const { data, style } = this.props;
       
    return (
      <FlatList 
        style={[styles.scrollContainer, style]}
        keyExtractor={this._keyExtractor}
        data={data}
        renderItem={({item, index}) => this.renderRow(item, index)}
      />
    );
  }
}

const mapStateToProps = (state) => {
  // const { items } = state
  // return {
  //   items: items
  // };
  return {}
};

const mapDispatchToProps = dispatch => {
  return {
    // postLikes: (request) => dispatch(SearchActions.postLikes(request.item, request.toggle, request.cb)),
    postLikes: (request) => dispatch(SearchActions.postLikes(request.item, request.cb)),
    // postLikes: (request) => dispatch(LikesActions.postLikes(request.item, request.cb)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectsList);


const styles = StyleSheet.create({
  rowWrapper: {
    marginTop: 1,
    marginBottom: 10,
    marginHorizontal: 0,
  },
  row: {
    // borderWidth: 1,
    position: 'relative',
    backgroundColor: 'white',
    shadowColor: '#CACACA',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 4,
    borderRadius: 3,
    padding: 5,
    height: 149,
  },
  lastRow: {
    marginBottom: 15
  },
  avatar: {
    width: 109,
    height: 110,
    borderRadius: 3,
    overflow: 'hidden'
  },
  avatarImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover'
  },
  projBottom: {
    position: 'absolute',
    bottom: 0,
    width: 109,
    height: 48
  },
  largeRow: {
    flexDirection: 'row',
    height: 142,
    position: 'relative',
  },
  column: {
    flex: 1, 
    paddingHorizontal: 10,
    paddingBottom: 7
  },
  timeText: {
    marginTop: 10,
  },
  locationText: {
    fontFamily: 'Gotham-Book',
    marginLeft: 10,
    marginTop: 3,
    fontSize: 11,
    lineHeight: 15
  },
  scrollContainer: {
    paddingTop: 2,
    paddingHorizontal: 19,
    flex: 1
  },
  ratingWrap: {
    flexDirection: 'row',
    marginTop: 7,
    marginBottom: 7,
    marginLeft: 0,
    height: 14
  },  
  reviewNowBtn: {
    width: '100%',
    height: 14,
    alignItems: 'flex-start',
    // justifyContent: 'center'
  },
  ratingContainer: {
    justifyContent: 'flex-start',
    height: 14,
    paddingBottom: 0,
    overflow: 'hidden',
    marginTop: -5
  },
  star: {
    marginRight: 2
  },
  ratingText: {
    color: '#777777',
    fontSize: 11,
    marginLeft: 7,
    marginTop: -8
  },
  rowTitle: {
    color: '#8DC63F',
    fontSize: 14,
    fontWeight: 'bold'
  },
  rowTitle2: {
    color: '#000000',
    fontSize: 11,
    fontWeight: 'bold',
    marginTop: -5
  },
  smallText: {
    color: '#777777',
    fontSize: 11
  },
  rowDescription: {
    marginTop: 0,
    lineHeight: 15,
    fontSize: 11,
    fontFamily: 'Gotham-Book'
  },
  dtIcon: {
    marginRight: 7
  },
  dateTime: {
    marginTop: 0,
    marginLeft: 65
  },
  dtRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 0
  },
  dateText: {
    flex: 1,
    marginTop: 0
  },
  multipleDates: {
    marginTop: 0
  },
  comingSoon: {
    marginTop: 5,
    marginLeft: 20
  },
  comingBtn: {
    marginTop: 3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  comingBtnText: {
    fontSize: 11,
    color: '#24B6EC'
  },
  arrowIcon: {
    marginLeft: 5,
    marginTop: -3
  },
  typeWrapper: {
    position: 'absolute',
    top: 80,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  type: {
    width: 35,
    height: 35,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 17.5,
    borderWidth: 1,
    borderColor: '#CACACA',
    // marginLeft: -3,
    // marginTop: -3
  },
  typeImg: {
    resizeMode: 'center',
    width: 25,
    height: 25
  },
  likePlusBtn: {
    position: 'absolute',
    right: 15,
    top: 9,
    alignItems: 'center',
    justifyContent: 'center'
  },
  checkIn: {
    borderWidth: 0.75,
    borderColor: '#DB5939'
  },
  confirmed: {
    borderWidth: 0.75,
    borderColor: '#8DC63F'
  },
  shouldConfirm1: {
    borderWidth: 0.75,
    borderColor: '#24B6EC'
  },
  shouldConfirm2: {
    borderWidth: 0.75,
    borderColor: '#606060'
  },
  overlayView: {
    position: 'absolute', 
    width: Dimensions.get('window').width - 38, 
    height: 149, 
    backgroundColor: '#FAFDFF30', 
    borderRadius: 2
  },
  statusText: {
    fontSize: 10,
    textAlign: 'right',
    marginBottom: 7
  },
  dot: {
    position: 'absolute',
    width: 5,
    height: 5,
    borderRadius: 2.5,
    backgroundColor: '#CACACA',
    marginLeft: -12,
    marginTop: 5
  },
  firstDot: {
    position: 'absolute',
    width: 11,
    height: 11,
    borderRadius: 5.5,
    borderWidth: 3,
    backgroundColor: '#8DC63F',
    borderColor: '#CACACA50',
    marginLeft: -15,
    marginTop: 0
  },
  vLine: {
    width: 1,
    height: 192,
    position: 'absolute',
    backgroundColor: '#CACACA',
    marginLeft: -10,
    marginTop: 15
  },
  firstLine: {
    height: 192,
    position: 'absolute',
    width: 2,
    marginLeft: -10.5,
    marginTop: 15,
    backgroundColor: '#8DC63F',
  },
  likesImg: {
    width: 35,
    height: 32,
    position: 'absolute',
    // opacity: 0.4
  },
  causeName: {
    fontSize: 10,
    textTransform: 'uppercase',
    color: '#303030',
    marginTop: 6
  },
  likedCnt: {
    fontSize: 12,
    color: '#ffffff',
    marginLeft: 1
  },
  unLikedCnt: {
    fontSize: 12,
    color: '#24B6EC',
    marginLeft: 1
  },
  notYetRated: {
    color: '#777777',
    fontSize: 10
  }
});
