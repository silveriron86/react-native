import React from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

function DateInput (props) {
  return (
    <View style={styles.column}>
      <Text style={styles.title}>{props.title}</Text>
      <Text style={styles.text}>{props.text}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  column: {
    paddingHorizontal: 10,
    justifyContent: 'space-between'
  },
  title: {

  }
});

export default DateInput;
 