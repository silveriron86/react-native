import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image
} from 'react-native';
const leftArrow = require('../../../assets/icons/left_arrow_white.png');

function SearchNavBar (props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => props.onBack()} style={styles.backButton}>
        <Image source={leftArrow} style={{tintColor: '#1574b8'}}/>
        <View style={styles.padding}></View>
        <View style={styles.title}>
          <Text style={styles.titleText}>{props.title}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 26,
    paddingLeft: 5,
    paddingRight: 21
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50
  },
  padding: {
    width: 7,
    height: 50,
  },
  title: {
    flex: 1,
    height: 50,
    paddingLeft: 14,
    borderBottomWidth: 2.5,
    borderBottomColor: '#606060',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: 15
  }
});

export default SearchNavBar;
 