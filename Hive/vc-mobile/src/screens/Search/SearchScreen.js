import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  TouchableOpacity
} from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import moment from 'moment';
import { StackActions, NavigationActions } from 'react-navigation'
import { SearchActions, LikesActions } from '../../actions';
import SearchNavBar from './SearchNavBar'; 
import { 
  DateInput,
  MultiSlider,
  TextButton,
  GothamBookText,
  GothamNotRoundedBookText,
  GothamNotRoundedMediumText,
  LoadingOverlay,
  CornerLabel
} from '../../components';
import ApiConstants from '../../constants/ApiConstants';
import ProjectsList from './ProjectsList';
import Config from 'react-native-config'
const DEFAULT_LAT = 33.665337;
const DEFAULT_LNG = -117.737832;

class SearchScreen extends React.Component {
  static navigationOptions = {
      // headerTitle instead of title
      header: null
  };

  state = {
    loading: true,
    apiCallCount: 0,
    causes: [],
    distance: 25,
    filter: '',
    multiSliderValue: [0, 25],
    fromDate: moment().format('MM-DD-YYYY'),
    toDate: moment().add(6, 'months').format('MM-DD-YYYY'),
  };

  handleSelectRow = (item) => {
    console.log(item);
    this.props.navigation.navigate('ProjectDetailsShow', {
      data: item
    });
  }

  goProjectDetails = (item) => {
    this.props.navigation.navigate('ProjectDetails', {
      data: item
    });
  }

  handleReviewNow = () => {

  }

  onChangeDate = (dt, type) => {
    console.log(dt, type);
    let state = this.state;
    state[type] = dt;
    this.setState(state);
    // AsyncStorage.setItem(type, dt);
  }

  handleLike = (item) => {
    this.setState({
      loading: true
    },()=> {    
      this.props.postLikes({
        item: item,
        cb: () => {
          this._getList();
        }
      }); 
    });
  }

  onValueChange(value) {
    this.setState({ distance: value });
  }

  multiSliderValuesChange = values => {
    this.setState({
      multiSliderValue: values,
    });
    AsyncStorage.setItem('DISTANCES', JSON.stringify(values));
  };  

  apply = () => {
    this.setState({
      loading: true
    }, ()=> {
      this._getList()
    })
  }

  componentWillMount() {
    AsyncStorage.getItem('DISTANCES', (err, distances) => {
      if(distances) {
        this.setState({
          multiSliderValue: JSON.parse(distances)
        })
      }
    });
  }  

  componentDidMount = () => {
    AsyncStorage.getItem('SELECTED_CAUSES', (err, val) => {
      let causes = [];
      let arr = JSON.parse(val)
      if(arr && arr.length) {
        arr.forEach(item => {
          causes.push(item.name);
        })          
      }
      this.setState({
        loading: true,
        causes: causes
      }, ()=> {
        this._getList()          
      })
    });    
  }

  _getList = () => {
    const { causes, multiSliderValue } = this.state;
    AsyncStorage.getItem('latitude', (err, latitude) => {
      AsyncStorage.getItem('longitude', (err, longitude) => {
        let data = {
          // radius: multiSliderValue[1]
          // radius: (latitude && longitude && multiSliderValue[1]) ? multiSliderValue[1] : 10000,
          // latitude: latitude ? latitude : DEFAULT_LAT,
          // longitude: longitude ? longitude : DEFAULT_LNG,
          latitude: latitude ? latitude : 0,
          longitude: longitude ? longitude : 0,
        }

        if(latitude && longitude && multiSliderValue[1]) {
          data['radius'] = multiSliderValue[1]
        }
        if(causes.length > 0) {
          data['causes'] = causes
        }

        this.props.getList({
          data: data,
          cb: (res) => {
            if(res.error) {
              if(this.state.apiCallCount === 0) {
                this.setState({
                  apiCallCount: 1
                },() => {
                  this._getList();
                })
              }else {
                this.setState({
                  apiCallCount: 0,
                  loading: false
                }, () => {
                  Alert.alert('Error', res.error.message);
                })
              }
            }else {
              this.setState({
                loading: false
              });
            }
          }
        });        
      });
    });
  }  

  render() {
    const { filter, causes, multiSliderValue, fromDate, toDate, loading } = this.state;
    const { navigation, filteredListData } = this.props;
    
    return (
      <View style={styles.container}>
        { Config.ENVIRONMENT === 'DEV' && <CornerLabel>DEV</CornerLabel> }
        <LoadingOverlay loading={loading}/>
        <SearchNavBar
          title={navigation.getParam('zipCode')}
          onBack={() => navigation.goBack()}
        />
        <View style={styles.content}>
          <View style={{marginTop: 8}}>
            <GothamNotRoundedBookText style={styles.txtFilters}>Filters</GothamNotRoundedBookText>
            <View style={styles.filterRow}>
              <Button
                title="Distance"
                onPress={() => this.setState({ filter: 'distance'})}
                buttonStyle={[styles.filterButton, filter === 'distance' ? styles.selectedButtonStyle : styles.unSelectedButtonStyle]}
                titleStyle={[styles.filterButtonTitle, filter === 'distance' ? styles.selectedButtonTitleStyle : styles.unSelectedButtonTitleStyle]}
              />
              {/* <Button
                title={causes.length > 0 ? `(${causes.length}) Causes` : 'Causes'}
                onPress={() => this.setState({ filter: 'dates'})}
                buttonStyle={[styles.filterButton, filter === 'dates' ? styles.selectedButtonStyle : styles.unSelectedButtonStyle, {marginLeft: 7}]}
                titleStyle={[styles.filterButtonTitle, filter === 'dates' ? styles.selectedButtonTitleStyle : styles.unSelectedButtonTitleStyle]}
              /> */}
            </View>
          </View>
          {
            filter === 'distance' && 
            <View style={styles.slider}>
              <MultiSlider
                values={[
                  multiSliderValue[0],
                  multiSliderValue[1],
                ]}
                sliderLength={Dimensions.get('window').width - 55}
                onValuesChange={this.multiSliderValuesChange}
                min={0}
                max={50}
                step={1}
                allowOverlap
                snapped
              />
              {
                (multiSliderValue[1] < 50) &&
                <Text style={styles.lastSliderValue}>50</Text>  
              }
            </View>  
          }          
          {
            filter === 'dates' && <View style={styles.filterRow}>
              <View style={{flex: 1}}>
                <Text style={styles.fromTo}>From</Text>
                <DateInput 
                  style={styles.dateContainer} 
                  date={fromDate} 
                  format="MM-DD-YYYY"
                  noIcon={true}
                  onChange={(dt)=>this.onChangeDate(dt, 'fromDate')}/>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.fromTo}>To</Text>
                <DateInput 
                  style={styles.dateContainer} 
                  date={toDate} 
                  format="MM-DD-YYYY"
                  noIcon={true}
                  onChange={(dt)=>this.onChangeDate(dt, 'toDate')}/>
              </View>
            </View>
          }
          {
            (filter !== '') &&
            <View style={[styles.applyBtn, filter === 'dates' && {marginTop: -15}]}>
              <TouchableOpacity onPress={this.apply} style={styles.btnApply}>
                <GothamNotRoundedMediumText style={styles.txtApply}>Apply</GothamNotRoundedMediumText>
              </TouchableOpacity>
            </View>
          }
        </View>
        {
          filteredListData &&
            <ProjectsList 
              data={filteredListData}
              isSearch={true}
              handleSelectRow={this.handleSelectRow} 
              handleReviewNow={this.handleReviewNow}
              handleLike={this.handleLike}
              goProjectDetails={this.goProjectDetails}
            /> 
        }
        {/* {
          loading &&
            <View style={{marginTop: 30}}>
              <ActivityIndicator size="large"/>
            </View>
        }         */}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    filteredListData: state.SearchReducer.filteredListData,   
    filteredListError: state.SearchReducer.filteredListError,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getList: (request) => dispatch(SearchActions.getFilterList(request.data, request.cb)),
    postLikes: (request) => dispatch(SearchActions.postLikes(request.item, request.cb)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    // flex: 1,
    paddingHorizontal: 19,
  },
  slider: {
    marginLeft: 8,
    width: '100%',
    flexDirection: 'row',
    position: 'relative'
  },
  filterRow: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  filterButton: {
    width: 114,
    height: 31,    
    marginRight: 10,
    shadowOffset:{  width: 0,  height: 0 },
    shadowColor: '#757575',
    shadowOpacity: 0.62,
    shadowRadius: 2,
    borderRadius: 3
  },
  filterButtonTitle: {
    fontSize: 12,
    fontFamily: 'Gotham-Medium'
  },
  selectedButtonStyle: {
    backgroundColor: '#8ac74c',
  },
  unSelectedButtonStyle: {
    backgroundColor: 'white',
  },
  selectedButtonTitleStyle: {
    color: 'white',
  },
  unSelectedButtonTitleStyle: {
    color: 'grey',
  },
  lastSliderValue: {
    position: 'absolute',
    right: 10,
    color: '#CACACA',
    fontSize: 10,
    marginTop: 38
  },
  applyBtn: {
    marginTop: -4,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  dateContainer: {
    marginTop: 5,
    marginBottom: 0,
    width: '100%'
  },
  fromTo: {
    color: '#303030',
    fontSize: 10
  },
  txtFilters: {
    color: '#303030',
    fontSize: 14,
    lineHeight: 22
  },
  btnApply: {
    paddingTop: 11,
    paddingBottom: 21
  },
  txtApply: {
    fontSize: 14,
    color: '#24B6EC'
  }
});
