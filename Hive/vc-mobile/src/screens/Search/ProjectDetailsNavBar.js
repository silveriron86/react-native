import React from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import Colors from "../../constants/Colors";
import {GothamMediumText} from "../../components";
const leftArrow = require('../../../assets/icons/left_arrow_white.png');

function ProjectDetailsNavBar(props) {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => props.onBack()} style={styles.backButton}>
                <Image source={leftArrow}/>
            </TouchableOpacity>

            <View style={styles.title}>
                <GothamMediumText style={styles.titleText}>{props.title}</GothamMediumText>
            </View>

            <TouchableOpacity onPress={()=> {console.log("Share pressed")}} style={styles.shareBtn}>
                <Image source={require('../../../assets/icons/share.png')}/>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingTop: 26,
        paddingLeft: 10,
        paddingRight: 21,
        backgroundColor: Colors.tintColor
    },
    backButton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: 48,
        height: 44
    },
    title: {
        flex: 1,
        height: 50,
        marginLeft: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: Colors.white
    },
    shareBtn: {
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default ProjectDetailsNavBar;
 