import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    StatusBar,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import { Calendar } from 'react-native-calendars';
import moment from 'moment';

const SELECTED_DATE_COLOR = '#4A90E2';
const SELECTED_DATE_STYLE = {
    container: {
        backgroundColor: SELECTED_DATE_COLOR,
        borderWidth: 2,
        borderColor: '#CACACA'
    },
    text: {
        color: 'white',
        marginTop: 7
    }
};
export default class CalendarModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dates: [
                '2019-02-13',
                '2019-02-15',
                '2019-02-16',
                '2019-02-22',
            ]
        };
    }

    onDayPress = (day) => {
        let date = moment(day.timestamp).format('YYYY-MM-DD');
        let dates = this.state.dates;
        let index = dates.indexOf(date);
        
        if(index >= 0) {
            dates.splice(index, 1);
        }else {
            dates.push(date);
        }

        this.setState({
            dates
        });
    }

    render() {
        const { dates } = this.state
        let markedDates = {};
        dates.forEach(day => {
            markedDates[day] = {
                selected: true, 
                customStyles: SELECTED_DATE_STYLE
            }
        })

        return (
            <View style={styles.container}>
                <StatusBar hidden={true} />
                <View style={styles.header}>
                    <Text style={styles.headerText}>Don’t miss the next opportunity!</Text>
                </View>
                <Text style={styles.description}>Select dates when you’d like to volunteer. We’ll alert you when there are new projects. </Text>
                <Calendar
                    markingType={'custom'}
                    markedDates={markedDates}
                    onDayPress={this.onDayPress}
                    style={{
                        marginTop: 10,
                        paddingHorizontal: 9,
                        height: 267
                    }}
                    // Specify theme properties to override specific styles for calendar parts. Default = {}
                    theme={{
                        'stylesheet.calendar.header': {
                            week: {
                                marginTop: 7,
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                backgroundColor: '#8DC63F',
                                height: 27                                
                            },
                            monthText: {
                                textTransform: 'uppercase',
                                color: '#606060',
                                fontSize: 16
                            },
                            dayHeader: {
                                fontSize: 12,
                                textTransform: 'uppercase',
                                color: 'white',
                                marginTop: 7
                            }
                        },
                        backgroundColor: '#ffffff',
                        calendarBackground: '#ffffff',
                        textSectionTitleColor: '#ffffff',
                        selectedDayTextColor: '#ffffff',
                        todayTextColor: '#00adf5',
                        dayTextColor: '#2d4150',
                        textDisabledColor: '#d9e1e8',
                        arrowColor: '#1D75BD',
                        monthTextColor: '#606060',
                        textMonthFontWeight: 'bold',
                        textDayFontSize: 12,
                        textMonthFontSize: 16,
                        textDayHeaderFontSize: 16
                    }}
                />   
                <TouchableOpacity onPress={this.props.handleConfirm} style={styles.confirmBtn}>
                    <Text style={styles.confirmText}>CONFIRM</Text>
                </TouchableOpacity>               
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'center',
    },
    header: {
        backgroundColor: '#1D75BD',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50
    },
    headerText: {
        marginTop: 3,
        fontSize: 18,
        color: 'white'
    },
    description: {
        padding: 25,
        color: '#303030',
        fontSize: 14
    },
    confirmBtn: {
        backgroundColor: '#F1F1F1',
        height: 50,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    confirmText: {
        color: '#1D75BD',
        fontSize: 16
    }
});

 