import React from 'react';
import { connect } from 'react-redux';
import {Image, Dimensions, ImageBackground, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View, AsyncStorage ,Linking, AlertIOS} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import FirstStepModal from './FirstStepModal';
import CalendarModal from './CalendarModal';
import StarRating from 'react-native-star-rating';
import {GothamBookText, GothamMediumText, GothamNotRoundedMediumText, GothamNotRoundedBookText, LoadingOverlay, CornerLabel} from "../../components";
import {Tooltip} from 'react-native-elements';
import ProjectDetailsNavBar from "./ProjectDetailsNavBar";
import ImageProgress from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import moment from 'moment';
import MapModal from "./MapModal";
import { LikesActions, SearchActions } from '../../actions';
import { asyncStorage } from 'reactotron-react-native';
import ApiConstants from '../../constants/ApiConstants';
const detailsArrow = require('../../../assets/icons/details_arrow.png');
const detailsArrowUp = require('../../../assets/icons/details_arrow_up.png');
const rightBlueArrow = require('../../../assets/icons/arrow_blue.png');
const heartIcon = require('../../../assets/icons/heart.png');
const fbIcon = require('../../../assets/icons/social/facebook_blue.png');
const twIcon = require('../../../assets/icons/social/twitter_blue.png');
const snapchatIcon = require('../../../assets/icons/social/snapchat_blue.png');
const googleIcon = require('../../../assets/icons/social/google_blue.png');
const instIcon = require('../../../assets/icons/social/instagram_blue.png');
const linkedInIcon = require('../../../assets/icons/social/linkedin.png');
// const topBgWithProj = require('../../../assets/images/profile_top_background.png')
// const topBgNoProj = require('../../../assets/images/noproj_bg.png')
import Config from 'react-native-config'
const LikeImage = (props) => (
    <>
    {
      (props.item.isOrgLikedByUser || props.item.isProjectLikedByUser) ?
      <Image source={require('../../../assets/icons/liked_filled.png')} style={styles.likesImg}/>
      :
      <Image source={require('../../../assets/icons/heart_unliked_1.png')} style={styles.likesImg}/>
    }
    </>
  )

export class ProjectDetailsShowScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: (navigation.getParam('data').name ? navigation.getParam('data').name : navigation.getParam('data').orgName),
        title2: (navigation.getParam('data').name ? navigation.getParam('data').orgName : ''),
    });

    constructor(props) {
        super(props);
        this.state = {
            isPanelOpen: false,
            isModalOpen: false,
            isModalMapOpen: false,
            loading: false,
            isDescriptionLong: false,
        };
    }

    hidePanel = () => {
        this.setState({
            isPanelOpen: false
        });
    };

    handleConfirm = () => {
        console.log('handle confirm');
        this.setState({
            isModalOpen: false,
            isPanelOpen: true
        });
    };

    _sendSubscriptions = async () => {
        try{
            let authUserJSON = JSON.parse(await AsyncStorage.getItem('oio_auth'));
            let  userJSON = authUserJSON.user;
            const { navigation } = this.props;
            let data = navigation.getParam('data');
            let type;
            if(data.sfProjectId){
                type = "Project Interest";
            }else{
                type = "Project Status Updates";
            }

            this.props.postSubscriptions(data.sfOrgId,data.sfProjectId,userJSON.sfContactId,authUserJSON.token,type);
            
        }catch(error){
          console.debug(error);
        }
    };

    sendMe = async () => {
        const { navigation } = this.props;
        let project = navigation.getParam('data');
        let response = await this._sendSubscriptions();
        if(project.date) {
            // if there are available dates/shifts for a project, it should bring up the "choose shifts" screen.
            navigation.navigate('Calendar', {
                data: project
            });
        }else {
            // if there are NO available dates/shifts for a project, it should bring up the "notify me" screen.
            // this.setState({isModalOpen: true});
            // (VC-350)
            this.setState({isPanelOpen: true})
        }
    };

    closeMap = () => {
        this.setState({isModalMapOpen: false});
    };

    openMap = () => {
        this.setState({isModalMapOpen: true});
    };

    toggleDescriptionLong = () => {
        this.setState({isDescriptionLong: !this.state.isDescriptionLong })
    };

    handleLike = (item) => {
        // return;
        this.setState({
            loading: true
        },()=> {
            this.props.postLikes({
                item: item,
                cb: () => {
                    this.setState({
                        loading: false
                    })
                    // Works on both iOS and Android
                    AlertIOS.alert(`Thanks! We've saved it and it will appear on your next search.`);

                    // RACE CONDITION? SF isn't updating the "isLiked" soon enough?
                    // this.loadProjects();

                    // const loadProjects = this.loadProjects();
                    // setTimeout(loadProjects, 5000);

                    // var that = this; 
                    // setTimeout( function() { that.loadProjects(); }, 5000);

                    // this.props.getProject({
                    //     id: item.sfProjectId,
                    //     cb: (res) => {
                    //         console.log(res);
                    //         this.setState({
                    //             loading: false
                    //         })
                    //     }
                    // })
                }
            });
        })
    }    

    openURL = (url) => {
        console.log(url);
        if(url === null) {
            return;
        }

        if (url.indexOf("http://") != 0 && url.indexOf("https://") != 0) {
            url = 'http://' + url;
        }        
        
        Linking.openURL(url);
    }

    componentDidMount() {
        this.loadProjects();
    }

    loadProjects = () => {
        const { navigation } = this.props;
        const data = navigation.getParam('data');
        let sfProjectId = data.sfProjectId;
        if(sfProjectId) {
            this.props.getProject({
                id: sfProjectId,
                cb: (res) => {
                    console.log(res);
                    this.setState({
                        loading: false
                    })
                }
            })
        }        
    }

    render() {
        const {isPanelOpen, isModalOpen, isModalMapOpen, loading, isDescriptionLong} = this.state;
        const {navigation} = this.props;
        const data = navigation.getParam('data');
        console.log(data);
        // let topBackground = (data.shifts.length > 0) ? topBgWithProj : topBgNoProj;

        let title = data.name ? data.name : data.orgName
        let title2 = (title === data.name) ? data.orgName : ''

        const description = data.description ? data.description : (data.missionStatement ? data.missionStatement : '' )

        const descriptionMax = 120;
        let descriptionShort = description.substring(0, descriptionMax); 

        (description.length > descriptionMax) ? (descriptionShort += '...') : null; 

        const street = data.street ? data.street : (data.orgStreet ? data.orgStreet : '' )
        const zipcode = data.zipcode ? data.zipcode : (data.orgZipcode ? data.orgZipcode : '' )
        const state = data.state ? data.state : (data.orgState ? data.orgState : '' )
        const city = data.city ? data.city : (data.orgCity ? data.orgCity : '' )
    
        const websiteURL = data.sfProjectId ? (data.projectURL ? data.projectURL : null) 
        : (data.orgWebsite ? data.orgWebsite : null)

        let reqRows = [];
        if(data.requirements && data.requirements.length > 0) {
            data.requirements.forEach((req, index) => {
                console.log(req.requirementIconUrl, req.requirementName);
                let reqName = req.requirementName;
                let linkUrl = null

                if(reqName === 'Training Required') {
                    reqName = 'Training';
                }

                if(reqName === 'Waiver' || reqName === 'Training') {
                    linkUrl = (
                        <TouchableOpacity style={styles.rightWrapper} onPress={() => this.openURL(data.waiverURL)}>
                            <GothamNotRoundedMediumText style={styles.commonTopText}>{(reqName === 'Waiver') ? reqName : reqName + ' Materials'}</GothamNotRoundedMediumText>
                            <Image style={{marginLeft: 5}} source={rightBlueArrow}/>
                        </TouchableOpacity>
                    )
                }

                reqRows.push(
                    <View style={styles.commonWrapperView} key={`req-${index}`}>
                        <View style={styles.pinIconWrap}>
                            <Image source={{uri: req.requirementIconUrl}}
                                style={styles.reqIcon}/>
                        </View>
                        <GothamBookText style={styles.commonMediumText}>{reqName}</GothamBookText>
                        {linkUrl}
                    </View>
                );                
            });
        }
        return (
            <View style={styles.container}>
                <LoadingOverlay loading={loading}/>
                <StatusBar backgroundColor="blue" barStyle="light-content"/>
                <ScrollView>
                    { data.nteeCodeImgURL && 
                        <ImageProgress
                        source={{uri: data.nteeCodeImgURL}}
                        indicator={Progress.Bar}
                        indicatorProps={{
                            marginTop: -150
                        }}
                        style={styles.topBackground} />                
                    }
                    { Config.ENVIRONMENT === 'DEV' && <CornerLabel>DEV</CornerLabel> }
                    <ImageBackground 
                        source={require('../../../assets/images/rectangle_background.png')} 
                        imageStyle={{height: 510, resizeMode: 'stretch'}}
                        style={{marginTop: 125, width: '100%'}} >
                        <View style={styles.topView}>
                            <View style={styles.causeView}>
                                <View style={styles.causeIcon}>
                                    {
                                        data.causeIconURL &&
                                        <ImageProgress
                                        source={{uri: data.causeIconURL}}
                                        indicator={Progress.Circle}
                                        style={styles.causeImg} />
                                    }                                      
                                </View>
                            </View>
                            {
                                <TouchableOpacity style={styles.likeIcon} disabled={data.shifts.length === 0} style={{opacity: 0}}>
                                    <Image source={require('../../../assets/icons/like_plus36.png')} style={data.shifts.length === 0 && {opacity: 0}}/>
                                </TouchableOpacity>
                            }
                        </View>
                        <View style={styles.causeNameView}>
                            <GothamNotRoundedMediumText style={styles.causeName}>{data.cause}</GothamNotRoundedMediumText>
                        </View>
                        <View style={styles.wrapper}>
                            <View style={styles.content}>
                                <View style={{flex: 1}}>
                                    <View style={styles.viewTitle}>
                                        <GothamMediumText style={styles.title}>{title}</GothamMediumText>
                                    </View>
                                    <TouchableOpacity style={[styles.likePlusBtn, data.liked === true && {shadowOpacity: 0}]} onPress={()=>this.handleLike(data)}>
                                        <LikeImage item={data}/>
                                        {/* <GothamNotRoundedMediumText style={styles.likedCnt}> */}
                                        <GothamNotRoundedMediumText style={
                                            (data.isOrgLikedByUser || data.isProjectLikedByUser) ?
                                                [styles.likedCnt]  
                                            :
                                                [styles.unLikedCnt]
                                        }>
                                            {data.sfProjectId ? data.projectTotalLikes : data.orgTotalLikes}
                                        </GothamNotRoundedMediumText>
                                    </TouchableOpacity>                               
                                    {
                                        (data.rating) ?
                                        <View style={styles.ratingWrap}>
                                            <StarRating
                                                disabled={false}
                                                maxStars={5}
                                                rating={data.rating}
                                                starSize={14}
                                                starStyle={styles.star}
                                                containerStyle={styles.ratingContainer}
                                                fullStarColor={'#f5a643'}
                                                emptyStarColor={'#F1F1F1'}
                                                emptyStar={'star'}
                                                disabled
                                            />
                                            <GothamNotRoundedMediumText style={styles.ratingText}>{data.rating.toFixed(1)}</GothamNotRoundedMediumText>
                                        </View>
                                        :
                                        <GothamNotRoundedMediumText style={styles.notYetRated}>NOT YET RATED</GothamNotRoundedMediumText>
                                    }

                                    <GothamBookText style={styles.title2}>
                                        {title2}
                                    </GothamBookText>

                                    <GothamBookText style={styles.description}>
                                        { isDescriptionLong ? description : descriptionShort } 
                                    </GothamBookText>
                                    
                                    <View style={styles.descriptionCaretView}>
                                        <TouchableOpacity onPress={this.toggleDescriptionLong}>
                                            <Image source={ isDescriptionLong ? detailsArrowUp : detailsArrow }/>
                                        </TouchableOpacity>                            
                                    </View>

                                    <ModalWrapper
                            style={styles.CalendarModalWrapper}
                            onRequestClose={() => this.setState({isModalOpen: false})}
                            visible={isModalOpen}>
                            <CalendarModal handleConfirm={this.handleConfirm}/>
                        </ModalWrapper>




                                    {/* <GothamMediumText style={styles.addressLabel}>{data.shifts.length > 0 ? 'PROJECT' : 'ORGANIZATION'} ADDRESS</GothamMediumText> */}
                                    {
                                        // data.city && data.state &&
                                        city && state &&
                                        <TouchableOpacity onPress={this.openMap} style={styles.address}>
                                            <Image source={require('../../../assets/icons/location_icon_blue.png')}
                                                    style={styles.pinIcon}/>
                                            <GothamBookText style={styles.addressText}>{street} {city},{state && ` ${state}`} {zipcode}</GothamBookText>
                                        </TouchableOpacity>     
                                    }
                                    {

                                        (data.facebookProfileURL || data.twitterProfileURL || data.snapchatProfileURL || data.googleProfileURL || data.instagramProfileURL || data.linkedinProfileURL || websiteURL) &&
                                        <View style={styles.socialBtnsView}>    
                                            {
                                                data.facebookProfileURL &&
                                                <TouchableOpacity style={styles.socialBtn} onPress={() => this.openURL(data.facebookProfileURL)}>
                                                    <Image source={fbIcon} style={styles.socialIcon}/>
                                                </TouchableOpacity>
                                            }
                                            {
                                                data.twitterProfileURL &&
                                                <TouchableOpacity style={styles.socialBtn} onPress={() => this.openURL(data.twitterProfileURL)}>
                                                    <Image source={twIcon} style={styles.socialIcon}/>
                                                </TouchableOpacity>
                                            }
                                            {
                                                data.snapchatProfileURL &&
                                                <TouchableOpacity style={styles.socialBtn} onPress={() => this.openURL(data.snapchatProfileURL)}>
                                                    <Image source={snapchatIcon} style={styles.socialIcon}/>
                                                </TouchableOpacity>
                                            }
                                            {
                                                data.googleProfileURL &&
                                                <TouchableOpacity style={styles.socialBtn} onPress={() => this.openURL(data.googleProfileURL)}>
                                                    <Image source={googleIcon} style={styles.socialIcon}/>
                                                </TouchableOpacity>
                                            }
                                            {
                                                data.instagramProfileURL &&
                                                <TouchableOpacity style={styles.socialBtn} onPress={() => this.openURL(data.instagramProfileURL)}>
                                                    <Image source={instIcon} style={styles.socialIcon}/>
                                                </TouchableOpacity>
                                            }
                                            {
                                                data.linkedinProfileURL &&
                                                <TouchableOpacity style={styles.socialBtn} onPress={() => this.openURL(data.linkedinProfileURL)}>
                                                    <Image source={linkedInIcon} style={styles.socialIcon}/>
                                                </TouchableOpacity>     
                                            }
                                            {
                                                websiteURL &&
                                                <TouchableOpacity style={styles.websiteBtn} onPress={() => this.openURL(websiteURL)}>
                                                    <GothamNotRoundedMediumText style={styles.websiteBtnText}>Website</GothamNotRoundedMediumText>
                                                    <Image source={rightBlueArrow}/>
                                                </TouchableOpacity>                                                                           
                                            }

                                        </View> 
                                    }  
                                    <View style={styles.infoView}>
                                        <Tooltip height={120} width={250} backgroundColor='#fafdff'
                                                 popover={
                                                     <Text>
                                                         <GothamBookText style={styles.toolTipText}>Requesting reviews
                                                             is a great way to show colleges and employers your
                                                             strengths and skills.
                                                         </GothamBookText>
                                                     </Text>
                                                 }>
                                            <Image source={require('../../../assets/icons/info_icon_blue.png')}
                                                   style={styles.infoIcon}/>
                                        </Tooltip>

                                        <GothamBookText style={styles.infoText}>You can request a recommendation for your volunteer transcript</GothamBookText>
                                    </View>  

                                    {
                                        data.sfProjectId && (data.volunteersNeeded > 0) &&
                                        <View style={styles.volunteerCountView}>
                                            <Tooltip height={90} width={250} backgroundColor='#fafdff'
                                                    popover={
                                                        <Text>
                                                            <GothamBookText style={styles.toolTipText}>Bring your friends
                                                                to
                                                                help!</GothamBookText>{"\n"}
                                                            <GothamMediumText style={styles.toolTipTextBlue}>Share this
                                                                opportunity </GothamMediumText>
                                                            <Image source={rightBlueArrow}/>
                                                        </Text>
                                                    }>
                                                <GothamBookText style={styles.volunteerCountText}>{data.volunteersNeeded}</GothamBookText>
                                            </Tooltip>

                                            <GothamMediumText style={styles.volunteerCountTitle}>VOLUNTEERS
                                                NEEDED</GothamMediumText>
                                        </View>                                                
                                    }                                                                                                                                          
                                    {
                                        (data.sfProjectId) && 
                                        <GothamMediumText style={styles.addressLabel}>ABOUT THIS PROJECT</GothamMediumText>
                                    }
                                    {
                                        (data.sfProjectId) &&
                                        <Tooltip height={120} width={250} backgroundColor='#fafdff'
                                                popover={
                                                    <Text>
                                                        <GothamBookText style={styles.toolTipText}>Supporting an
                                                            anti-bullying, mental health awareness, or special needs
                                                            through actions.
                                                        </GothamBookText>
                                                    </Text>
                                                }>
                                            <View style={styles.campainView}>
                                                <GothamNotRoundedMediumText style={styles.campainText}>{data.type}</GothamNotRoundedMediumText>
                                            </View>
                                        </Tooltip>
                                    }
                                    
                                    {
                                        (data.sfProjectId && data.deadlineDate) ?
                                        <GothamNotRoundedBookText style={styles.deadlineText}>Deadline to sign up
                                            <GothamNotRoundedMediumText style={styles.deadlineDate}> {moment(data.deadlineDate).format('M/D/YYYY')}</GothamNotRoundedMediumText>
                                        </GothamNotRoundedBookText>
                                        :
                                        <View style={{width: '100%', height: 30}}></View>
                                    }
                                    {/* : <GothamNotRoundedMediumText style={styles.onGoingText}>Ongoing</GothamNotRoundedMediumText> */}
                                </View>
                            </View>
                            <View style={styles.btns}>
                                <TouchableOpacity style={[styles.btn, styles.rightBtn]} onPress={this.sendMe}>
                                    <GothamNotRoundedMediumText style={styles.checkBtnText}>
                                        {data.sfProjectId ? 'I WANT TO VOLUNTEER' : 'SEND ME NEW PROJECT DATES'}
                                    </GothamNotRoundedMediumText>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.shutterlockView}>
                            <GothamMediumText style={styles.shutterlockText}>INSPIRING PHOTO BY <GothamMediumText
                                style={{color: '#24B6EC'}}>SHUTTERSTOCK</GothamMediumText></GothamMediumText>
                        </View>
                        {
                            (data.sfProjectId) && (data.specialInstructions || data.minimumAge !== null || data.userProvidedURL !== null || (data.requirements && data.requirements.length > 0)) &&
                            <View style={[styles.wrapper, styles.secondBlock]}>
                                <GothamMediumText style={styles.requirementText}>Requirements</GothamMediumText>
                                {
                                    (data.minimumAge !== null) && 
                                    <View style={styles.commonWrapperView}>
                                        <View style={styles.pinIconWrap}>
                                            <GothamMediumText style={styles.minimumTextCount}>{data.minimumAge}</GothamMediumText>
                                        </View>
                                        <GothamBookText style={styles.commonText}>Minimum Age</GothamBookText>
                                    </View>
                                }
                                {reqRows}
                                {
                                    data.userProvidedURL !== null &&
                                    <View style={styles.commonWrapperView}>
                                        <GothamBookText style={styles.commonMediumText}>Other Information</GothamBookText>
                                        <TouchableOpacity style={styles.rightWrapper} onPress={() => this.openURL(data.userProvidedURL)}>
                                            <GothamNotRoundedMediumText style={styles.commonTopText}>View</GothamNotRoundedMediumText>
                                            <Image style={{marginLeft: 5}} source={rightBlueArrow}/>
                                        </TouchableOpacity>
                                    </View>
                                }
                                {
                                    data.specialInstructions &&
                                    <>
                                        <GothamMediumText style={styles.reqText}>Special Instructions</GothamMediumText>
                                        <GothamNotRoundedBookText style={styles.reqDetailsText}>{data.specialInstructions}</GothamNotRoundedBookText>
                                    </>
                                }
                            </View>
                        }

                        <ModalWrapper
                            containerStyle={{flexDirection: 'row', justifyContent: 'flex-end'}}
                            onRequestClose={() => this.setState({isPanelOpen: false})}
                            shouldAnimateOnRequestClose={true}
                            position="right"
                            visible={isPanelOpen}>
                            <FirstStepModal
                                hidePanel={this.hidePanel}
                                navigation={this.props.navigation}
                                description="We'll alert you to new volunteer opportunities."/>
                        </ModalWrapper>

                        <ModalWrapper
                            style={styles.CalendarModalWrapper}
                            onRequestClose={() => this.setState({isModalOpen: false})}
                            visible={isModalOpen}>
                            <CalendarModal handleConfirm={this.handleConfirm}/>
                        </ModalWrapper>

                        <ModalWrapper
                            style={styles.CalendarModalWrapper}
                            onRequestClose={() => this.setState({isModalMapOpen: false})}
                            visible={isModalMapOpen}>
                            <TouchableOpacity onPress={this.closeMap} style={styles.closeBtn}>
                                <Image source={require('../../../assets/icons/close.png')} />
                            </TouchableOpacity>
                            <MapModal handleConfirm={this.closeMap}/>
                        </ModalWrapper>
                    </ImageBackground>

                </ScrollView>
            </View>
        );
    }
};

const mapStateToProps = (state) => {
    return {
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
        // postLikes: (request) => dispatch(SearchActions.postLikes(request.item, request.toggle, request.cb)),
        postLikes: (request) => dispatch(SearchActions.postLikes(request.item, request.cb)),

        // DEFINITELY DOES NOT WORK
        // postLikes: (request) => dispatch(LikesActions.postLikes(request.item, request.cb)),

        getProject: (request) => dispatch(SearchActions.getProject(request.id, request.cb)),
        postSubscriptions:(sfOrgId,sfProjectId,sfContactId,token,type) => dispatch(SearchActions.postSubscriptions(sfOrgId, sfProjectId, sfContactId, true, type, token))
    }
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetailsShowScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    topBackground: {
        width: '100%',
        height: 300,
        position: 'absolute',
        resizeMode: 'stretch'
    },
    topView: {
        flexDirection: 'row',
    },
    causeView: {
        flex: 1,
    },
    causeIcon: {
        left: Dimensions.get('window').width / 2 - 30,
        position: 'absolute',
        width: 60,
        height: 60,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 30,
        borderWidth: 2,
        borderColor: '#CACACA',
        marginTop: -6
    },
    causeImg: {
        width: 45,
        height: 45,
        resizeMode: 'center'
    },
    likeIcon: {
        // justifyContent: 'center',
        // alignItems: 'center',
        marginTop: -3,
        marginRight: 20,
    },
    wrapper: {
        margin: 20,
        marginTop: 35,
        marginBottom: 10,
        backgroundColor: 'white',
        borderRadius: 2,
        shadowColor: '#CACACA',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 1,
        elevation: 1,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    content: {
        paddingVertical: 11,
        paddingHorizontal: 20,
        paddingBottom: 0,
        flexDirection: 'row'
    },
    ratingWrap: {
        flexDirection: 'row',
        marginTop: 9,
        marginBottom: 5
    },
    ratingContainer: {
        justifyContent: 'flex-start'
    },
    star: {
        marginRight: 2
    },
    ratingText: {
        flex: 1,
        color: '#303030',
        fontSize: 10,
        marginLeft: 7,
        marginTop: 3
    },
    websiteText: {
        color: '#24B6EC',
        fontSize: 11,
        marginRight: 7,
        marginTop: 1
    },
    description: {
        color: '#1D75BD',
        fontSize: 11,
        marginTop: 5,
        lineHeight: 16,
        textTransform: 'uppercase'
    },
    descriptionDetailsIcon: {
        flex: 1,
    },
    volunteerCountView: {
        marginTop: 10,
        flexDirection: 'row',
    },
    commonToolTip: {
        margin: 5,
        backgroundColor: '#FAFDFF',
    },
    toolTipText: {
        fontSize: 14,
        lineHeight: 19,
        color: '#303030',
    },
    toolTipTextBlue: {
        fontSize: 13,
        lineHeight: 17,
        color: '#24B6EC',
    },
    volunteerCountText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#24B6EC',
    },
    volunteerCountTitle: {
        flex: 1,
        color: '#303030',
        justifyContent: 'center',
        fontSize: 10,
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 5,
    },
    addressLabel: {
        color: '#303030',
        fontSize: 10,
        marginTop: 12
    },
    address: {
        marginTop: 4,
        marginLeft: 0,
        flexDirection: 'row',
        alignItems: 'center'
    },
    pinIcon: {
        // resizeMode: 'stretch',
        // marginLeft: -3
    },
    reqIcon: {
        width: 35,
        height: 35,
        resizeMode: 'center'
    },
    infoIcon: {
        marginTop: -5
    },
    addressText: {
        color: '#777777',
        fontSize: 11,
        marginLeft: 10,
        lineHeight: 15,
        marginTop: 3
    },
    campainView: {
        width: 'auto',
        alignSelf: 'flex-start',
        marginTop: 8,
        height: 30,
        paddingHorizontal: 11,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FAFDFF',
        borderRadius: 5,
        shadowColor: '#CACACA',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 4,
    },
    campainText: {
        color: '#303030',
        fontSize: 10,
    },
    infoView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        marginLeft: 0
    },
    infoText: {
        color: '#777777',
        fontSize: 11,
        marginLeft: 5,
        marginTop: 3
    },
    deadlineText: {
        color: '#303030',
        fontSize: 11,
        lineHeight: 15,
        marginTop: 10,
        marginBottom: 10
    },
    deadlineDate: {
        color: '#303030',
        fontSize: 11
    },
    btns: {
        flexDirection: 'row'
    },
    btn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50
    },
    rightBtn: {
        backgroundColor: '#F1F1F1',
        borderBottomRightRadius: 8
    },
    checkBtnText: {
        color: '#1D75BD',
        fontSize: 14
    },
    shutterlockView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5
    },
    shutterlockText: {
        color: '#CACACA',
        fontSize: 10,
    },
    requirementText: {
        color: '#303030',
        fontSize: 14,
        marginLeft: 5,
    },
    commonWrapperView: {
        marginLeft: 5,
        marginRight: 5,
        paddingTop: 3,
        paddingBottom: 3,
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.3,
        borderBottomColor: '#E0E2EE',
    },
    minimumTextCount: {
        color: '#8DC63F',
        fontSize: 17,
        marginTop: 5
    },
    commonText: {
        color: '#303030',
        fontSize: 14,
        marginLeft: 10,
        marginTop: 5
    },
    commonMediumText: {
        flex: 1,
        color: '#303030',
        fontSize: 14,
        marginLeft: 10,
        marginTop: 5
    },
    rightWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 3
    },
    commonTopText: {
        color: '#24B6EC',
        fontSize: 13,
        marginTop: 2
    },
    reqText: {
        color: '#303030',
        fontSize: 14,
        lineHeight: 19,
        marginTop: 11,
    },
    reqDetailsText: {
        color: '#777777',
        fontSize: 11,
        lineHeight: 15,
        marginTop: 5,
    },
    viewTitle: {
        paddingRight: 30
    },
    title: {
        color: '#8DC63F',
        fontSize: 16,
        marginTop: 3
    },
    title2: {
        color: '#000000',
        fontSize: 12,
        marginTop: 16,
        textTransform: 'uppercase',
        fontWeight: 'bold'
    },
    secondBlock: {
        marginTop: 0,
        paddingVertical: 20,
        paddingHorizontal: 13,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
    },
    CalendarModalWrapper: {
        // width: 335,
        backgroundColor: 'transparent',
        width: Dimensions.get('window').width - 40,
        borderRadius: 8,
        overflow: 'hidden',
        justifyContent: 'flex-start'
    },
    likesView: {
        position: 'absolute',
        flexDirection: 'row',
        marginTop: 10,
        right: 10
    },
    likesText: {
        fontSize: 10,
        lineHeight: 9,
        color: 'white',
        marginLeft: 2
    },
    socialBtnsView: {
        flexDirection: 'row',
        marginTop: 3
    },
    socialBtn: {
        marginTop: 7,
        marginRight: 10
    },
    socialIcon: {
        tintColor: '#24B6EC',
        width: 22,
        height: 20
    },
    websiteBtn: {
        marginTop: 10,
        flexDirection: 'row'
    },
    websiteBtnText: {
        fontSize: 11,
        color: '#24B6EC',
        marginTop: 2,
        marginRight: 5
    },
    onGoingText: {
        fontSize: 11,
        color: '#303030',
        lineHeight: 15,
        paddingVertical: 15
    },
    notYetRated: {
        color: '#777777',
        fontSize: 10,
        marginTop: 8
    },
    causeNameView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 35
    },
    causeName: {
        color: '#000000',
        fontSize: 14,
        textTransform: 'uppercase',
    },
    pinIconWrap: {
        width: 35,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeBtn: {
        justifyContent: 'flex-end',
        position: 'absolute',
        right: 0,
        width: 27,
        height: 27,
        zIndex: 999999
    },
    likePlusBtn: {
        position: 'absolute',
        right: 5,
        top: 9,
        alignItems: 'center',
        justifyContent: 'center'
    },    
    likesImg: {
        width: 35,
        height: 32,
        position: 'absolute'
    },    
    likedCnt: {
        fontSize: 12,
        color: '#FFFFFF',
        marginLeft: 1
    },
    unLikedCnt: {
        fontSize: 12,
        color: '#24B6EC',
        marginLeft: 1
    },
    descriptionCaretView: {
        flex: 1,
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
});