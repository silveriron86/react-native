import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import FirstStepModal from './FirstStepModal';
import CalendarModal from './CalendarModal';
import StarRating from 'react-native-star-rating';
import ImageProgress from 'react-native-image-progress';
import * as Progress from 'react-native-progress';

export default class ProjectDetailsScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: navigation.getParam('data').name,
    });

    constructor(props) {
      super(props);
      this.state = {
        isPanelOpen: false, 
        isModalOpen: false
      };
    }

    hidePanel = () => {
        this.setState({
            isPanelOpen: false
        });
    }

    handleConfirm = () => {
        console.log('handle confirm');
        this.setState({
            isModalOpen: false,
            isPanelOpen: true
        });
    }    

    sendMe = () => {
        this.setState({ isModalOpen: true });
    }

    render() {
        const { navigation } = this.props;
        const { isPanelOpen, isModalOpen } = this.state;
        const data = navigation.getParam('data');

        let title = data.name ? data.name : data.orgName
        let title2 = (title === data.name) ? data.orgName : ''

        const description = data.description ? data.description : (data.missionStatement ? data.missionStatement : '' )

        const street = data.street ? data.street : (data.orgStreet ? data.orgStreet : '' )
        const zipcode = data.zipcode ? data.zipcode : (data.orgZipcode ? data.orgZipcode : '' )
        const state = data.state ? data.state : (data.orgState ? data.orgState : '' )
        const city = data.city ? data.city : (data.orgCity ? data.orgCity : '' )

        return (
            <ScrollView style={styles.container}>        
                <ImageProgress
                    source={{uri: data.nteeCodeImgURL}}
                    indicator={Progress.Bar}
                    style={{width: '100%', height: 300}} />                
                <View style={styles.wrapper}>
                    <View style={styles.content}>
                        <View style={styles.imgWrapper}>
                            <ImageProgress
                                source={{uri: data.causeIconURL}}
                                indicator={Progress.Circle}
                                style={styles.causeImg} />                               
                        </View>
                        <View style={{flex: 1}}>
                            <Text style={styles.title}>{title}</Text>
                            <Text style={styles.description}>{description}</Text>
                            <Text style={styles.addressLabel}>Address</Text>
                            <View style={styles.address}>
                                <Image source={require('../../../assets/icons/ionicons_svg_ios-pin.png')} style={styles.pinIcon}/>
                                <Text style={[styles.addressText]} ellipsizeMode='tail' numberOfLines={2}>
                                    {street} {city},{state} {zipcode}
                                </Text>
                            </View>
                            <View style={styles.ratersComment}>
                                <View style={styles.address}>
                                    <Image source={require('../../../assets/icons/ionicons_svg_ios-heart.png')} style={styles.heartIcon}/>
                                    <Text style={styles.addressText}>Raters: 22</Text>
                                </View>
                                <View style={[styles.address, {marginLeft: 43}]}>
                                    <Image source={require('../../../assets/icons/ionicons_svg_ios-text.png')} style={styles.heartIcon}/>
                                    <Text style={styles.addressText}>Comment: 20</Text>
                                </View>
                            </View>
                            <View style={styles.ratingWrap}>
                                <StarRating
                                    disabled={false}
                                    maxStars={5}
                                    rating={data.rating}
                                    starSize={14}
                                    starStyle={styles.star}
                                    containerStyle={styles.ratingContainer}              
                                    fullStarColor={'#f5a643'}
                                    disabled
                                />     
                                <Text style={styles.ratingText}>{data.rating && data.rating.toFixed(1)}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.btns}>
                        {/* <TouchableOpacity style={[styles.btn, styles.leftBtn]}>
                            <Text style={styles.locationBtnText}>LOCATION ADDED</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btn, styles.rightBtn]}>
                            <Text style={styles.checkBtnText}>CHECK-OUT</Text>
                        </TouchableOpacity> */}
                        <TouchableOpacity style={[styles.btn, styles.rightBtn]} onPress={this.sendMe}>
                            <Text style={styles.checkBtnText}>Send me new project dates</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={[styles.wrapper, styles.secondBlock]}>
                    <Text style={styles.reqTitle}>Requirements</Text>
                    <Text style={styles.reqText}>When a new project is listed, we’ll send you all the details for a great experience.</Text>                    
                    {/* <Text style={styles.reqText}>
                        <Text style={{fontSize: 14}}>Adult Volunteer Requirements{'\n'}(18 Years or Older){'\n'}</Text>
                        • Minimum time commitment of 2 hours or more {'\n'}
                        • Regular weekly three-hour shift{'\n'}
                        • Eligible for direct patient contact areas{'\n'}
                        • Must complete required vaccinations, as determined in the one-on-one interview upon acceptance to the volunteer program{'\n'}{'\n'}
                        <Text style={{fontSize: 14}}>Teen Volunteer Requirements{'\n'}(Minimum 16 Years){'\n'}</Text>
                        • Minimum time commitment of 2 hours or more{'\n'}
                        • Regular weekly three-hour shift{'\n'}
                        • Eligible for limited, non-patient contact areas{'\n'}
                        • Parent/guardian permission required{'\n'}
                        • Must complete required vaccinations, as determined in the one-on-one interview upon acceptance to the volunteer program
                    </Text> */}
                </View>

                <Text style={styles.notes}>Legal Copy for Organizations and Groups: Legal Copy for Organizations and Groups:  Legal Copy for Organizations and Groups: Legal Copy for Organizations and Groups: Legal Copy for Organizations and Groups:</Text>

                <ModalWrapper
                    containerStyle={{ flexDirection: 'row', justifyContent: 'flex-end' }}
                    onRequestClose={() => this.setState({ isPanelOpen: false })}
                    shouldAnimateOnRequestClose={true}
                    position="right"
                    visible={isPanelOpen}>
                    <FirstStepModal 
                        hidePanel={this.hidePanel}
                        navigation={this.props.navigation}
                        description="We'll alert you to new volunteer opportunities."/>
                </ModalWrapper>

                <ModalWrapper
                    style={styles.CalendarModalWrapper}
                    onRequestClose={() => this.setState({ isModalOpen: false })}
                    visible={isModalOpen}>
                    <CalendarModal handleConfirm={this.handleConfirm}/>
                </ModalWrapper>                       
            </ScrollView>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 2,
        shadowColor: '#CACACA',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 1,
        elevation: 4,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    content: {
        padding: 11,
        paddingBottom: 0,
        flexDirection: 'row'
    },
    imgWrapper: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        borderWidth: 2,
        borderColor: '#CACACA',
        marginRight: 10
    },
    causeImg: {
        width: 36,
        height: 36
    },
    center: {
        width: '100%',
        alignItems: 'center'
    }, 
    title: {
        color: '#8DC63F',
        fontSize: 16
    },
    description: {
        color: '#1D75BD',
        fontSize: 11,
        marginTop: 5,
        lineHeight: 17,
        textTransform: 'uppercase'
    },
    addressLabel: {
        color: '#303030',
        fontSize: 10,
        marginTop: 11
    },  
    ratingWrap: {
        flexDirection: 'row',
        marginTop: 9,
        marginBottom: 20
    },  
    ratingContainer: {
        justifyContent: 'flex-start'
    },
    star: {
        marginRight: 2
    },
    ratingText: {
        color: '#777777',
        fontSize: 11,
        marginLeft: 7,
        marginTop: 1
    },
    address: {
        marginTop: 4,
        flexDirection: 'row',
        alignItems: 'center'
    },
    pinIcon: {
        tintColor: '#8DC63F',
        width: 21,
        height: 21,
        resizeMode: 'stretch',
        marginLeft: -3
    },
    addressText: {
        color: '#777777',
        fontSize: 11,
        marginLeft: 10
    },
    ratersComment: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    heartIcon: {
        tintColor: '#8DC63F',
        width: 16,
        height: 16
    },
    locationBtn: {
        backgroundColor: '#8DC63F',
        flex: 1,   
    },
    btns: {
        flexDirection: 'row'
    },
    btn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50
    },
    leftBtn: {
        backgroundColor: '#8DC63F', 
        borderBottomLeftRadius: 8
    },
    rightBtn: {
        backgroundColor: '#F1F1F1', 
        borderBottomRightRadius: 8
    },
    locationBtnText: {
        color: 'white',
        fontSize: 14
    },
    checkBtnText: {
        color: '#1D75BD',
        fontSize: 14
    },
    reqTitle: {
        fontFamily: 'Queen of Clubs',
        color: '#8DC63F',
        fontSize: 28,
        letterSpacing: 0.93,
        textTransform: 'uppercase'
    },
    reqText: {
        color: '#303030',
        fontSize: 13,
        lineHeight: 19,
        marginTop: 11
    },
    secondBlock: {
        marginTop: 0, 
        paddingVertical: 20, 
        paddingHorizontal: 13,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0        
    },
    notes: {
        fontSize: 11,
        color: '#777777',
        paddingHorizontal: 20,
        paddingBottom: 20,
        lineHeight: 18
    },
    CalendarModalWrapper: {
        width: 335,
        borderRadius: 8,
        overflow: 'hidden',
        justifyContent: 'flex-start'
    }
});