import React from 'react';
import { AsyncStorage, View } from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import InviteFriendsModal from '../Search/InviteFriendsModal';

export default class InviteScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    state = {
        modalVisible: true
    }

    hideInvitePanel = () => {   
        this.setState({
            modalVisible: false
        }, ()=> {
            AsyncStorage.getItem('PREV_TAB', (err, tab) => {
                console.log(tab);
                this.props.screenProps.rootNavigation.navigate(tab ? tab : 'HomeStackWrapper');
            })            
        })        
    };

    componentDidMount() {
        this.load()
        this.props.navigation.addListener('willFocus', this.load)
    }

    load = () => {
        this.setState({
            modalVisible: true
        })
    }

    render() {
        const { modalVisible } = this.state;
        return (
            <View>
                <ModalWrapper
                    containerStyle={{flexDirection: 'row', justifyContent: 'flex-end'}}
                    onRequestClose={this.hideInvitePanel}
                    // shouldAnimateOnRequestClose={false}
                    // position="right"
                    visible={modalVisible}>
                    <InviteFriendsModal hidePanel={this.hideInvitePanel}></InviteFriendsModal>
                </ModalWrapper>
            </View>
        );
    }
}