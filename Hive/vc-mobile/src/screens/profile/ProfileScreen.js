import React from 'react';
import {AsyncStorage, Image, ImageBackground, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View, Platform} from 'react-native';
import { PermissionsAndroid } from 'react-native';
import { withNavigationFocus } from "react-navigation";
import Contacts from 'react-native-contacts';
import { connect } from 'react-redux';
import { ProjectsActions } from '../../actions';
import {GothamBookText, GothamMediumText, CornerLabel, LoadingOverlay, PrimaryButton} from '../../components';

import Colors from '../../constants/Colors';
import ProfileNavBar from './ProfileNavBar';

const imgBkg = require('../../../assets/images/profile_bg.jpg');
const volunteerScoreArrow = require('../../../assets/icons/arrow_blue.png');
const transcriptScoreArrow = require('../../../assets/icons/arrow_white.png');
const radiusBg = require('../../../assets/images/radius_bottom_bg.png');
import ApiConstants from '../../constants/ApiConstants';
import Config from 'react-native-config'
import NavigationVars from '../../navigation/NavigationVars';
import { AlertIOS } from 'react-native';

class ProfileScreen extends React.Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            userId: '',
            sfContactId: '',
            user: null,
            showCongratsModal: props.showCongratsModal,
            displayLoader: props.showLoaderOverlay,
        };
    }

    goPage = (page) => {
        const { navigation } = this.props;
        if(page === 'Contact') {
            if(Platform.OS === 'ios') {
                Contacts.getAll((err, contacts) => {
                    navigation.navigate(page, {
                        contacts: contacts
                    })                
                })
            }else {
                PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                    {
                        'title': 'Contacts',
                        'message': 'This app would like to view your contacts.'
                    }
                ).then(() => {
                    Contacts.getAll((err, contacts) => {
                        navigation.navigate(page, {
                            contacts: contacts
                        })                
                    }) 
                })
            }
        }else {
            navigation.navigate(page);
        }
    }

    getAuthData = async() => {
        try{
            let authUserJSON = JSON.parse(await AsyncStorage.getItem('oio_auth'));
            let  userJSON = authUserJSON.user;
            this.setState({
            token: authUserJSON.token,
            userId: userJSON.id,
            sfContactId: userJSON.sfContactId,
            user: userJSON,
            },);
        }catch(error){
            console.debug(error);
        }
    }

    getProjectsOrgs = async () => {
        try{
          let request = {
            token: this.state.token,
            sfContactId: this.state.sfContactId,
            userId: this.state.userId
          };
          await this.props.showLoaderOverlay();
          await this.props.getProjectsOrgsList({request: request, callback: () => {}})
          this.props.hideLoaderOverlay();
          } 
        catch(error){
          this.props.hideLoaderOverlay();
          console.debug(error);
        }
      }

    goProjects = () => {     
        this.props.navigation.navigate('Projects')
    }

    componentDidMount() {
        this.getAuthData().then(()=>this.getProjectsOrgs());
    }

    componentDidUpdate(prevProps) {
        // https://reactnavigation.org/docs/en/function-after-focusing-screen.html
        if (prevProps.isFocused !== this.props.isFocused) {
          if (this.props.isFocused) {
            this.getAuthData().then(()=>this.getProjectsOrgs());
          }
        }
    }

    render() {
        const { user } = this.state;
        const { navigation, showCongratsModal } = this.props;
        return (
            <ScrollView>

                <LoadingOverlay loading={this.props.displayLoader}/>

                <StatusBar backgroundColor="blue" barStyle="light-content"/>
                <View style={styles.headerImgWrapper}>
                    <Image source={imgBkg} style={styles.headerImg}/>
                    <Image source={radiusBg} style={styles.radiusBg}/>
                </View>

                <ProfileNavBar
                    onLeftButtonPress={() => navigation.toggleDrawer()}
                />
                <View style={styles.container}>
                { Config.ENVIRONMENT === 'DEV' && 
                    <View style={styles.cornerLabelView}>
                        <CornerLabel>DEV</CornerLabel> 
                    </View>
                }
                    <TouchableOpacity style={styles.avatarIconView}>
                        <View>
                            <Image source={require('../../../assets/icons/profile/avatar_blue_white_bg.png')}
                            />
                        </View>
                    </TouchableOpacity>


                    { showCongratsModal &&
                        <View style={styles.congratsModal}>
                            <View>
                                <GothamMediumText style={styles.congratsHeader}>
                                    GREAT VOLUNTEER WORK!
                                </GothamMediumText>                                        
                            </View>
                            <View>
                                <GothamMediumText style={styles.congratsText}>
                                    We updated your total hours.
                                </GothamMediumText>                                        
                            </View>
                            <View>
                                <GothamMediumText style={styles.congratsText}>
                                    Click ADD HOURS to include more projects.
                                </GothamMediumText>                                        
                            </View>
                            <View>
                                <PrimaryButton
                                    onPress={() => this.props.hideCongratsModal()}
                                    style={styles.buttonCongrats}
                                    title="OK"
                                />                                             
                            </View>
                        </View>
                    }

                    {
                        user &&
                        <TouchableOpacity style={styles.profileScoreDetails} onPress={this.goProjects}>
                            <GothamMediumText style={styles.profileScore}>
                                {this.props.totalHours}
                            </GothamMediumText>
                            <GothamMediumText
                                style={styles.profileScoreDetailsText}>
                                {/* {user.lastname}'s  */}
                                My
                                Total Volunteer Hours
                            </GothamMediumText>
                            {/* <View style={styles.rightArrow}>
                                <Image source={volunteerScoreArrow}/>
                            </View> */}
                        </TouchableOpacity>
                    }
                    <View style={{backgroundColor: Colors.transcriptNameText, width: '100%'}}>
                        <TouchableOpacity style={styles.transcriptNameDetails} onPress={()=>this.goPage('Transcript')}>
                            <GothamMediumText
                                style={styles.transcriptNameDetailsText}>
                                MY VOLUNTEER TRANSCRIPT
                            </GothamMediumText>

                            <View style={styles.rightArrow}>
                                <Image source={transcriptScoreArrow}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.boardItemsWrapper}>
                        <TouchableOpacity style={styles.boardItems} onPress={()=>this.goPage('LeaderBoard')}>
                            <View style={styles.imgWrapper}>
                                <Image source={require('../../../assets/icons/profile/leaderboard_blue.png')}
                                />
                                <GothamBookText style={styles.imageText}>
                                    LEADERBOARD
                                </GothamBookText>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boardItems} onPress={this.goProjects}>
                            <View style={styles.imgWrapper}>
                                <Image source={require('../../../assets/icons/profile/add_hours_blue.png')}
                                />
                                <GothamBookText style={styles.imageText}>
                                    ADD HOURS
                                </GothamBookText>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.boardItemsWrapper}>
                        <TouchableOpacity style={styles.boardItems} onPress={()=>this.goPage('Contact')}>
                            <View style={styles.imgWrapper}>
                                <Image source={require('../../../assets/icons/profile/my_crowd_blue.png')}
                                />
                                <GothamBookText style={styles.imageText}>
                                    MY CROWDS
                                </GothamBookText>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boardItems} onPress={()=>this.goPage('Reviews')}>
                            <View style={styles.imgWrapper}>
                                <Image source={require('../../../assets/icons/profile/review_blue.png')}
                                />
                                <GothamBookText style={styles.imageText}>
                                    REVIEWS
                                </GothamBookText>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    return {
      totalHours: state.ProjectsReducer.submissionsTotalHours,
      showCongratsModal: state.CreateProjectModalReducer.showCongratsModal,
      displayLoader: state.ProjectsReducer.showLoaderOverlay,
      projectsOrgsList: state.ProjectsReducer.projectsOrgsList,   
      projectsOrgsListError: state.ProjectsReducer.projectsOrgsListError,
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      getProjectsOrgsList: (request) => dispatch(ProjectsActions.getProjectsOrgByUserId(request.request, request.callback)),
      hideCongratsModal: () => dispatch(ProjectsActions.hideCongratsModal()),
      showLoaderOverlay: () => dispatch(ProjectsActions.showLoaderOverlay()),
      hideLoaderOverlay: () => dispatch(ProjectsActions.hideLoaderOverlay()),
    }
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(ProfileScreen));

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    buttonCongrats: {
        marginTop: 30,
    },
    congratsModal: {
        height: '50%',
        width: '80%',
        backgroundColor: 'white',
        borderRadius: 2,
        shadowColor: '#CACACA',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 1,
        padding: 40,
        position: 'absolute',
        alignItems: 'center',
        // elevation: 4,
        flex: 1,
        zIndex: 99998,
      },
    congratsHeader: {
        textAlign: 'center',
        fontSize: 18,
        color: Colors.tintColor,
    },
    congratsText: {
        textAlign: 'center',
        fontSize: 14,
        color: Colors.deepSkyBlue,
        marginTop: 20,
    },
      headerImgWrapper: {
        position: 'absolute',
        width: '100%', 
        height: 205
    },
    headerImg: {
        width: '100%',
        height: 205,
        resizeMode: 'cover',
        opacity: 0.3,
    },
    radiusBg: {
        width: '100%',
        height: 27,
        resizeMode: 'stretch',
        position: 'absolute',
        bottom: 0
    },   
    avatarIconView: {
        justifyContent: 'center',
        paddingTop: 40,
        marginBottom: 10,
    },
    profileScoreWrapper: {
        width: '100%',
        position: 'relative',
        alignItems: 'center'
    },
    chooseWrapper: {
        width: '100%',
        position: 'relative',
        alignItems: 'center'
    },
    profileScore: {
        textAlign: 'center',
        fontSize: 35,
        color: Colors.tintColor,
    },
    profileScoreDetails: {
        position: 'relative',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
    },
    profileScoreDetailsText: {
        textAlign: 'center',
        fontSize: 16,
        color: Colors.deepSkyBlue,
    },
    transcriptNameDetails: {
        alignSelf: 'center',
        position: 'relative',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    transcriptNameDetailsText: {
        textAlign: 'center',
        fontSize: 16,
        color: Colors.white,
    },
    rightArrow: {
        alignSelf: 'flex-end',
        position: 'absolute',
        right: -38,
        top: -2,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    boardItemsWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    boardItems: {
        width: 150,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
        marginBottom: 15
    },
    imgWrapper: {
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageText: {
        color: Colors.black,
        fontSize: 12,
        textAlign: 'center',
        marginTop: 8,
        textTransform: 'uppercase'
    },
    cornerLabelView: {
        position: 'absolute',
        marginTop: -100,
        flexDirection: 'row',
        width: '100%',
      },
});
