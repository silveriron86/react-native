import React from 'react';
import {Image, Text, Dimensions, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View} from 'react-native';
import {GothamBookText, GothamBoldText, GothamNotRoundedMediumText, QueenClubsText, GothamNotRoundedBookText, GothamMediumText} from '../../components';
import StarRating from 'react-native-star-rating';
import DummyConstants from '../../constants/DummyConstants';
import Utils from '../../utils';

const imgBkg = require('../../../assets/images/myreviews_bg.jpg');
const orgImgBkg = require('../../../assets/images/organizationratings_bg.jpg');
const radiusBg = require('../../../assets/images/radius_bg.png');
const imgAvatar = require('../../../assets/icons/profile/avatar_blue.png');
const bArrow = require('../../../assets/icons/bottom_arrow.png');
const imgChecked = require('../../../assets/icons/checked_symbol.png');
const imgChat = require('../../../assets/icons/chat2.png');

export default class ReviewsScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    state = {
        tabIndex: 1
    }

    onPressRow = () => {
        
    }

    onSelectTab = (tabIndex) => {
        this.setState({tabIndex});
    }

    render() {
        const { tabIndex } = this.state;
        const { navigation } = this.props;

        let rows = [];
        let portfolioRows = [];
        let last = DummyConstants.transcriptList.length-1;
        let ssRows = []; //skill and strengths
        if(tabIndex === 0) {
            DummyConstants.transcriptList.forEach((item, index) => {
                item.rating = 5;
                rows.push(
                    <TouchableOpacity key={`row-${index}`} style={[styles.row, index === 0 && {marginTop: 6}, index === last && {marginBottom: 6, borderBottomWidth: 0}]} onPress={()=>this.onPressRow(item)}>
                        <View style={styles.leftItem}>
                            <View style={styles.typeWrapper}><Image source={item.type} style={styles.typeImg}/></View>
                            <View style={{flexDirection: 'column', marginLeft: 13}}>
                                <GothamNotRoundedMediumText style={styles.rowTitleText}>{item.title}</GothamNotRoundedMediumText>
                                <View style={{flexDirection: 'row'}}>
                                    <View style={styles.ratingWrap}>
                                        <StarRating
                                            disabled={false}
                                            maxStars={5}
                                            rating={item.rating}
                                            starSize={14}
                                            starStyle={styles.star}
                                            containerStyle={styles.ratingContainer}              
                                            fullStarColor={'#f5a643'}
                                            disabled
                                        />     
                                        <Text style={styles.ratingText}>{item.rating.toFixed(1)}</Text>
                                    </View>                                
                                    <GothamNotRoundedBookText style={styles.rowInfoText}>{item.date}</GothamNotRoundedBookText>
                                    <GothamNotRoundedBookText style={styles.rowInfoText}>{item.hours} hrs</GothamNotRoundedBookText>
                                </View>
                            </View>
                        </View>
                        <TouchableOpacity>
                            <Image source={bArrow} style={{tintColor: '#606060', marginTop: 32}}/>
                        </TouchableOpacity>
                    </TouchableOpacity>
                )
            });
        }else {
            DummyConstants.myReviewsList.forEach((item, index) => {
                portfolioRows.push(
                    <View key={`review-${index}`} style={styles.reviewRow}>
                        <Image source={imgAvatar} style={styles.reviewAvatar}/>
                        <View style={{width: 15}}></View>
                        <View style={{flex: 1}}>
                            <GothamNotRoundedMediumText style={styles.reviewTitle}>{item.title}</GothamNotRoundedMediumText>
                            <GothamNotRoundedBookText style={styles.reviewDate}>{item.date}</GothamNotRoundedBookText>
                            <GothamNotRoundedMediumText style={styles.reviewUserInfo}>{item.user} - {item.role}</GothamNotRoundedMediumText>
                            <GothamNotRoundedBookText style={styles.reviewContent}>{item.review}</GothamNotRoundedBookText>
                        </View>
                    </View>
                )
            });

            let skillsStrenthes = [
                {
                    strength: 14,
                    skill: 'Focus on Tasks'
                },
                {
                    strength: 10,
                    skill: 'Strong Work Ethic'
                },
                {
                    strength: 7,
                    skill: 'Good communicator'
                },
                {
                    strength: 4,
                    skill: 'Quality of Work'
                }
            ];

            let ssCols = [];
            skillsStrenthes.forEach((ss, i) => {
                ssCols.push(
                    <View style={styles.skillView} key={`ss-col-${i}`}>
                        <View style={styles.strengthView}>
                            <GothamNotRoundedMediumText style={styles.strengthText}>{ss.strength}</GothamNotRoundedMediumText>
                        </View>
                        <GothamNotRoundedMediumText style={styles.skillText}>{ss.skill}</GothamNotRoundedMediumText>
                    </View>
                );
                if(i % 2 == 1) {
                    ssRows.push(
                        <View style={styles.ssRow} key={`ss-row-${i}`}>{ssCols}</View>
                    )
                    ssCols = [];
                }
            });
            if(skillsStrenthes.length % 2 === 1) {
                ssRows.push(
                    <View style={styles.ssRow}>{ssCols}</View>
                )
            }
        }


        return (
            <View style={styles.container}>
                <View style={styles.container}>
                    <StatusBar hidden={true}/>
                    <View style={styles.header}>
                        {
                            (tabIndex === 1) ? 
                            <View style={styles.headerImgWrapper}>
                                <Image source={imgBkg} style={styles.headerImg}></Image>
                                <View style={styles.innerFrame}></View>
                                <Image source={radiusBg} style={styles.radiusBg}/>
                            </View>
                            :
                            <View style={[styles.headerImgWrapper, {opacity: 1}]}>
                                <Image source={orgImgBkg} style={[styles.headerImg, {opacity: 0.74, overlayColor: '#33333332'}]}></Image>
                                <Image source={radiusBg} style={styles.radiusBg}/>
                            </View>                            
                        }
                        <View style={styles.title}>
                            <GothamMediumText style={styles.titleText}>{tabIndex === 0 ? 'SHARE YOUR\nEXPERIENCE' : 'MY REVIEWS'}</GothamMediumText>
                        </View>
                    </View>
                    <TouchableOpacity onPress={()=>navigation.goBack()} style={styles.closeBtn}>
                        <Image source={require('../../../assets/icons/close.png')} />
                    </TouchableOpacity>
                    <View style={styles.tabBar}>
                        <TouchableOpacity style={[styles.tabBtn, tabIndex===0 && styles.selectedTab]} onPress={()=>this.onSelectTab(0)}>
                            <QueenClubsText style={[styles.tabText, tabIndex===0 && styles.selected]}>ORGANIZATION RATINGS</QueenClubsText>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.tabBtn, tabIndex===1 && styles.selectedTab]} onPress={()=>this.onSelectTab(1)}>
                            <QueenClubsText style={[styles.tabText, tabIndex===1 && styles.selected]}>MY REVIEWS</QueenClubsText>
                        </TouchableOpacity>
                    </View>
                    {
                        tabIndex === 0 &&
                        <View style={{width: '100%', flex: 1,position: 'relative'}}>
                            <ScrollView style={styles.listView}>
                                <View onStartShouldSetResponder={() => true}>{rows}</View>
                            </ScrollView>
                            <ComingSoonOverlay/>
                        </View>
                    }
                    {
                        tabIndex === 1 &&
                        <View style={[styles.container, {position: 'relative'}]}>
                            <View style={styles.totals}>
                                <View style={styles.totalCol}>
                                    <GothamNotRoundedMediumText style={styles.totalNumber}>38</GothamNotRoundedMediumText>
                                    <GothamBookText style={styles.totalLabel}>Organizations</GothamBookText>
                                </View>
                                <View style={[styles.totalCol, {width: 72}]}></View>
                                <View style={styles.totalCol}>
                                    <GothamNotRoundedMediumText style={styles.totalNumber}>10</GothamNotRoundedMediumText>
                                    <GothamBookText style={styles.totalLabel}>Reviews</GothamBookText>
                                </View>
                            </View>
                            <GothamBookText style={styles.portfolioTitle}>Skills and strengths</GothamBookText>
                            <View style={{marginTop: 16}}>{ssRows}</View>
                            <View style={{height: 10, width: '100%'}}>
                                <TouchableOpacity style={{position: 'absolute', right: 33}}>
                                    <Image source={bArrow}/>
                                </TouchableOpacity>                            
                            </View>
                            <View style={styles.hr}></View>
                            <GothamNotRoundedMediumText style={styles.myReviewsText}>My Reviews</GothamNotRoundedMediumText>
                            <ScrollView style={styles.listView}>{portfolioRows}</ScrollView>  
                            <ComingSoonOverlay/>
                        </View>
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        width: '100%'
    },
    headerImgWrapper: {
        width: '100%', 
        height: 166,
        opacity: 0.7
    },
    headerImg: {
        width: '100%',
        height: 166,
        resizeMode: 'cover'
    },
    radiusBg: {
        width: '100%',
        height: 27,
        resizeMode: 'stretch',
        position: 'absolute',
        bottom: 0
    },
    innerFrame: {
        position: 'absolute',
        width: '100%',
        height: 166,
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, .18)'
    },
    header: {
        width: '100%',
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center'        
    },
    title: {
        // height: 166,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center'
    },  
    titleText: {
        fontSize: 22,
        color: 'white'
    },
    closeBtn: {
        position: 'absolute',
        left: 17,
        top: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 18,
        width: 27,
        height: 27
    },
    tabBar: {
        marginTop: 5,
        width: 287,
        paddingHorizontal: 18,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    tabBtn: {
        borderBottomWidth: 4,
        borderBottomColor: 'white'
    },
    selectedTab: {
        borderBottomColor: '#1D75BD'
    },
    tabText: {
        color: '#CACACA',
        fontSize: 24,
        letterSpacing: 0.5
    },
    selected: {
        color: '#1D75BD'
    },
    showText: {
        color: '#1D75BD',
        fontSize: 14,
        lineHeight: 23
    },
    showAllBox: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        marginRight: 22,
        marginTop: 14,
        height: 30
    },
    bArrow: {
        width: 23,
        height: 23,
        marginLeft: 11,
        justifyContent: 'center',
        paddingTop: 2
    },
    listView: {
        width: '100%',
        flex: 1,
        paddingHorizontal: 21
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#E0E2EE',
        paddingHorizontal: 0,
        height: 72
    },  
    leftItem: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
    },
    typeWrapper: {
        width: 44,
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 22,
        borderWidth: 1,
        borderColor: '#CACACA',
        backgroundColor: 'white'
    },
    typeImg: {
        resizeMode: 'contain',
        maxWidth: 24,
        maxHeight: 24
    },
    checkedImg: {
        width: 20,
        height: 24,
        resizeMode: 'center',
        marginRight: 9
    },
    chatImg: {
        width: 31,
        height: 31
    },
    rowTitleText: {
        color: '#8DC63F',
        fontSize: 14,
        marginTop: 25,
        marginBottom: 0
    },
    rowInfoText: {
        fontSize: 11,
        color: '#777777',
        marginTop: 9,
        marginLeft: 10
    },
    ratingWrap: {
        flexDirection: 'row',
        marginTop: 7,
        marginBottom: 20
    },  
    ratingContainer: {
        justifyContent: 'flex-start'
    },
    star: {
        marginRight: 2
    },
    ratingText: {
        color: '#777777',
        fontSize: 11,
        marginLeft: 7,
        marginTop: 1
    },    
    verifyText: {
        color: '#8DC63F',
        fontSize: 18,
        letterSpacing: 0.72
    },
    totals: {
        flexDirection: 'row',
        marginTop: 14,
        height: 82,
        width: '100%',
        backgroundColor: '#8DC63F',
        justifyContent: 'center'
    },
    totalCol: {
        // flex: 1,
        // justifyContent: 'center',
        alignItems: 'center'
    },
    totalNumber: {
        marginTop: 16,
        fontSize: 30,
        color: 'white'
    },
    totalLabel: {
        fontSize: 11,
        color: 'white',
        textAlign: 'center'
    },
    portfolioTitle: {
        marginTop: 20,
        color: '#333333',
        fontSize: 18,
        textAlign: 'center',
        textTransform: 'uppercase'
    },
    pRow: {
        // flexDirection: 'column',
        // justifyContent: 'flex-start',
        // alignItems: 'center',
        marginHorizontal: 8
    },
    chartRow: {
        flexDirection :'row'
    },
    bar: {
        marginTop: 3,
        backgroundColor: '#1D75BD',
        height: 37
    },
    skillView: {
        backgroundColor: '#FAFDFF',
        borderRadius: 8,
        shadowColor: '#CACACA',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 4,   
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10
    },
    strengthView: {
        backgroundColor: '#8DC63F',
        width: 30,
        height: 30,
        borderTopLeftRadius: 8,
        borderBottomLeftRadius: 8,
        alignItems: 'center',
        justifyContent: 'center'        
    },
    strengthText: {
        fontSize: 11,
        color: 'white',
        alignSelf: 'center'
    },  
    skillText: {
        fontSize: 11,
        color: '#303030',
        paddingHorizontal: 7
    },
    ssRow: {
        flexDirection: 'row',
        marginBottom: 10
    },
    hr: {
        marginHorizontal: 21,
        marginVertical: 20,
        width: Dimensions.get('window').width - 42,
        height: 1,
        backgroundColor: '#F1F1F1'
    },
    myReviewsText: {
        width: '100%',
        color: '#303030',
        fontSize: 14,
        lineHeight: 22,
        marginLeft: 42,
        textAlign: 'left'
    },
    reviewRow: {
        // marginHorizontal: 20,
        flexDirection: 'row',
        marginTop: 13,
        marginBottom: 7
    },
    reviewAvatar: {
        width: 33,
        height: 33
    },
    reviewTitle: {
        fontSize: 14,
        color: '#8DC63F'
    },
    reviewDate: {
        fontSize: 11,
        color: '#777777',
        marginTop: 7
    },
    reviewUserInfo: {
        marginTop: 8,
        fontSize: 10,
        color: '#606060',
        textTransform: 'uppercase'
    },
    reviewContent: {
        fontSize: 11,
        color: '#777777',
        lineHeight: 15,
        marginTop: 6 
    }
});
