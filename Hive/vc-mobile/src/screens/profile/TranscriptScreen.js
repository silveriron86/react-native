import React from 'react';
import {AsyncStorage, Image, Dimensions, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View} from 'react-native';
import {GothamBookText, GothamBoldText, GothamNotRoundedMediumText, QueenClubsText, GothamNotRoundedBookText, ComingSoonOverlay} from '../../components';
import DummyConstants from '../../constants/DummyConstants';
import Utils from '../../utils';
import reactNativeImageProgress from 'react-native-image-progress';

const imgBkg = require('../../../assets/images/transcript_bg.jpg');
const imgAvatar = require('../../../assets/icons/profile/avatar_blue.png');
const bArrow = require('../../../assets/icons/bottom_arrow.png');
const rArrow = require('../../../assets/icons/black_arrow.png');
const imgChecked = require('../../../assets/icons/checked_symbol.png');
const imgVerified = require('../../../assets/icons/verified.png');
const imgChat = require('../../../assets/icons/chat2.png');
const radiusBg = require('../../../assets/images/radius_bg.png');

export default class TranscriptScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    state = {
        tabIndex: 0
    }

    onPressRow = () => {
        
    }

    onSelectTab = (tabIndex) => {
        this.setState({tabIndex});
    }

    render() {
        const { tabIndex } = this.state;
        const { navigation } = this.props;

        let rows = [];
        let portfolioRows = [];
        let last = DummyConstants.transcriptList.length-1;
        if(tabIndex === 0) {
            DummyConstants.transcriptList.forEach((item, index) => {
                rows.push(
                    <TouchableOpacity key={`row-${index}`} style={[styles.row, index === 0 && {marginTop: 6}, index === last && {marginBottom: 6, borderBottomWidth: 0}]} onPress={()=>this.onPressRow(item)}>
                        <View style={styles.leftItem}>
                            <View style={styles.typeWrapper}><Image source={item.type} style={styles.typeImg}/></View>
                            <View style={{flexDirection: 'column', marginLeft: 13}}>
                                <GothamNotRoundedMediumText style={styles.rowTitleText}>{item.title}</GothamNotRoundedMediumText>
                                <View style={{flexDirection: 'row'}}>
                                <GothamNotRoundedBookText style={[styles.rowInfoText, {width: 106}]}>{item.date}</GothamNotRoundedBookText>                                
                                </View>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{marginRight: 20}}>
                                <GothamNotRoundedBookText style={[styles.rowInfoText, {textAlign: 'right', marginTop: 7, marginBottom: 5}]}>{item.hours} hrs</GothamNotRoundedBookText>
                                { 
                                    item.verified ? 
                                    <View style={{flexDirection: 'row'}}>
                                        <GothamNotRoundedMediumText style={styles.verifiedText}>VERIFIED</GothamNotRoundedMediumText>
                                        <Image source={imgVerified} style={styles.checkedImg}/>
                                    </View> 
                                    :
                                    (item.status === 'pending') ?
                                    <GothamNotRoundedMediumText style={[styles.verifiedText, {color: '#606060'}]}>PENDING</GothamNotRoundedMediumText>
                                    :
                                    <TouchableOpacity>
                                        <GothamNotRoundedMediumText style={styles.verifyText}>VERIFY</GothamNotRoundedMediumText>
                                    </TouchableOpacity>
                                }
                            </View>
                            <View><Image source={rArrow} style={styles.rArrow}/></View>
                        </View>
                    </TouchableOpacity>
                )
            });
        }else {
            DummyConstants.transcriptList.forEach((item, index) => {
                let w = (Dimensions.get('window').width - 58) / 100 * item.totalHours;
                if(w < 26) w = 26;
                portfolioRows.push(
                    <View key={`row-${index}`} style={[styles.pRow, index === 0 && {marginTop: 6}, index === last && {marginBottom: 6, borderBottomWidth: 0}]}>
                        <View style={styles.chartRow}>
                            <View style={[styles.bar, {width: w}]}></View>
                            <View style={[styles.typeWrapper, {marginLeft: -20}]}><Image source={item.type} style={styles.typeImg}/></View>
                        </View>
                        <GothamNotRoundedBookText style={styles.rowInfoText}>{item.label} - {item.totalHours} hrs</GothamNotRoundedBookText>
                    </View>
                )
            });
        }

        return (
            <View style={styles.container}>
                <View style={styles.container}>
                    <StatusBar hidden={true}/>
                    <View style={styles.header}>
                        <View style={styles.headerImgWrapper}>
                            <Image source={imgBkg} style={styles.headerImg}/>
                            <View style={styles.innerFrame}></View>
                            <Image source={radiusBg} style={styles.radiusBg}/>
                        </View>
                        <View style={styles.title}>
                            <GothamBoldText style={styles.titleText}>TRANSCRIPT</GothamBoldText>
                        </View>
                    </View>
                    <TouchableOpacity onPress={()=>navigation.goBack()} style={styles.closeBtn}>
                        <Image source={require('../../../assets/icons/close.png')} />
                    </TouchableOpacity>
                    <View style={styles.tabBar}>
                        <TouchableOpacity style={[styles.tabBtn, tabIndex===0 && styles.selectedTab]} onPress={()=>this.onSelectTab(0)}>
                            <QueenClubsText style={[styles.tabText, tabIndex===0 && styles.selected]}>TRANSCRIPT</QueenClubsText>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.tabBtn, tabIndex===1 && styles.selectedTab]} onPress={()=>this.onSelectTab(1)}>
                            <QueenClubsText style={[styles.tabText, tabIndex===1 && styles.selected]}>PORTFOLIO</QueenClubsText>
                        </TouchableOpacity>
                    </View>
                    {
                        tabIndex === 0 &&
                        <View style={{width: '100%', flex: 1, position: 'relative'}}>
                            <TouchableOpacity style={styles.showAllBox}>
                                <GothamNotRoundedMediumText style={styles.showText}>Show all</GothamNotRoundedMediumText>
                                <View style={styles.bArrow}><Image source={bArrow} style={{tintColor: '#24B6EC'}}/></View>
                            </TouchableOpacity>                        
                            <ScrollView style={styles.listView}>
                                <View onStartShouldSetResponder={() => true}>{rows}</View>
                            </ScrollView>
                            <ComingSoonOverlay/>
                        </View>
                    }
                    {
                        tabIndex === 1 &&
                        <View style={[styles.container, {position: 'relative'}]}>
                            <View style={styles.totals}>
                                <View style={styles.totalCol}>
                                    <GothamNotRoundedMediumText style={styles.totalNumber}>290</GothamNotRoundedMediumText>
                                    <GothamBookText style={styles.totalLabel}>Volunteer{'\n'}Hours</GothamBookText>
                                </View>
                                <View style={styles.totalCol}>
                                    <GothamNotRoundedMediumText style={styles.totalNumber}>8</GothamNotRoundedMediumText>
                                    <GothamBookText style={styles.totalLabel}>Causes</GothamBookText>
                                </View>
                                <View style={styles.totalCol}>
                                    <GothamNotRoundedMediumText style={styles.totalNumber}>290</GothamNotRoundedMediumText>
                                    <GothamBookText style={styles.totalLabel}>Organizations{'\n'}erviced</GothamBookText>
                                </View>
                            </View>
                            <GothamBookText style={styles.portfolioTitle}>CAUSES & TOTAL HOURS</GothamBookText>
                            <ScrollView style={styles.listView}>{portfolioRows}</ScrollView>
                            <ComingSoonOverlay/>
                        </View>
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        width: '100%'
    },
    headerImgWrapper: {
        opacity: 0.7, 
        width: '100%', 
        height: 166
    },
    headerImg: {
        width: '100%',
        height: 166,
        resizeMode: 'cover',
        opacity: 0.74,
    },
    radiusBg: {
        width: '100%',
        height: 27,
        resizeMode: 'stretch',
        position: 'absolute',
        bottom: 0
    },
    innerFrame: {
        position: 'absolute',
        width: '100%',
        height: 166,
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, .35)'
    },
    header: {
        width: '100%',
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center'        
    },
    title: {
        // height: 166,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center'
    },  
    titleText: {
        fontSize: 22,
        color: 'white'
    },
    closeBtn: {
        position: 'absolute',
        left: 17,
        top: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 18,
        width: 27,
        height: 27
    },
    tabBar: {
        marginTop: 5,
        width: 260,
        paddingHorizontal: 18,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    tabBtn: {
        borderBottomWidth: 4,
        borderBottomColor: 'white'
    },
    selectedTab: {
        borderBottomColor: '#1D75BD'
    },
    tabText: {
        color: '#CACACA',
        fontSize: 24,
        letterSpacing: 0.5
    },
    selected: {
        color: '#1D75BD'
    },
    showText: {
        color: '#24B6EC',
        fontSize: 14,
        lineHeight: 23
    },
    showAllBox: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        marginRight: 22,
        marginTop: 14,
        height: 30
    },
    bArrow: {
        width: 23,
        height: 23,
        marginLeft: 11,
        justifyContent: 'center',
        paddingTop: 2
    },
    listView: {
        width: '100%',
        flex: 1,
        paddingHorizontal: 21
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#E0E2EE',
        paddingHorizontal: 7,
        height: 72
    },  
    leftItem: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
    },
    typeWrapper: {
        width: 44,
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 22,
        borderWidth: 1,
        borderColor: '#CACACA',
        backgroundColor: 'white'
    },
    typeImg: {
        resizeMode: 'contain',
        maxWidth: 24,
        maxHeight: 24
    },
    checkedImg: {
        marginLeft: 3,
        width: 10,
        height: 9,
        resizeMode: 'stretch',
        tintColor: '#CACACA'
    },
    verifiedText: {
        fontSize: 11,
        color: '#CACACA'
    },  
    chatImg: {
        width: 31,
        height: 31
    },
    rowTitleText: {
        color: '#8DC63F',
        fontSize: 14,
        marginTop: 2,
        marginBottom: 5
    },
    rowInfoText: {
        fontSize: 11,
        color: '#777777'
    },
    verifyText: {
        color: '#DB5939',
        fontSize: 11
    },
    totals: {
        flexDirection: 'row',
        marginTop: 14,
        height: 91,
        width: '100%',
        backgroundColor: '#8DC63F'
    },
    totalCol: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center'
    },
    totalNumber: {
        marginTop: 16,
        fontSize: 30,
        color: 'white'
    },
    totalLabel: {
        fontSize: 11,
        color: 'white',
        textAlign: 'center'
    },
    portfolioTitle: {
        marginTop: 18,
        color: '#333333',
        fontSize: 18,
        textAlign: 'center'
    },
    pRow: {
        // flexDirection: 'column',
        // justifyContent: 'flex-start',
        // alignItems: 'center',
        marginHorizontal: 8
    },
    chartRow: {
        flexDirection :'row'
    },
    bar: {
        marginTop: 3,
        backgroundColor: '#1D75BD',
        height: 37
    },
    rArrow: {
        tintColor: '#606060',
        width: 8,
        height: 14,
        resizeMode: 'stretch',
        marginTop: 13
    }
});
