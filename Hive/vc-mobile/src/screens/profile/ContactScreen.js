import React from 'react';
import {Image, FlatList, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View} from 'react-native';
import {GothamBookText, GothamBoldText, GothamNotRoundedMediumText, GothamMediumText} from '../../components';

const checkedImg = require('../../../assets/icons/checked.png')
const unCheckedImg = require('../../../assets/icons/unchecked.png')
const avatarImg = require('../../../assets/icons/profile/avatar_blue.png')

export default class ContactScreen extends React.Component {
    state = {
        selected: []
    }

    isChecked = (item) => {
        let list = this.state.selected;
        for (let i = 0; i < list.length; i++) {
          if (list[i].recordID === item.recordID) {
              return true;
          }
        }    
        return false;
    }

    toggleCheckBox = (item) => {
        let list = this.state.selected;
        let isExist = false;
        let index = 0;
    
        for (let i = 0; i < list.length; i++) {
          if (list[i].recordID === item.recordID) {
            isExist = true;
            index = i;
            break;
          }
        }

        if(isExist) {
          list.splice(index, 1);
        }else {
          list.push(item);
        }
        this.setState({
          selected: list
        });
    }

    renderRow = (item, index) => {
        let checked = this.isChecked(item);
        return (
            <View style={styles.rowWrapper}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    {
                        item.hasThumbnail === true ?
                        <Image source={{uri: item.thumbnailPath}} style={styles.avatarImg}/>
                        :
                        <Image source={avatarImg} style={styles.avatarImg}/>
                    }
                    <GothamBookText style={{marginTop: 5}}>{item.givenName} {item.familyName}</GothamBookText>
                </View>
                <TouchableOpacity style={styles.checkBox} onPress={()=> this.toggleCheckBox(item)}>
                    <Image source={checked ? checkedImg : unCheckedImg} style={styles.checkedImg}/>
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        const { selected } = this.state;
        const { navigation } = this.props;
        let contacts = navigation.getParam('contacts');

        return (
            <View style={styles.container}>
                <FlatList 
                    style={styles.scrollContainer}
                    data={contacts}
                    extraData={this.state}
                    keyExtractor={contact => contact.recordID.toString()}
                    renderItem={({item, index}) => this.renderRow(item, index)}
                />                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    scrollContainer: {
        width: '100%',
        paddingVertical: 15,
        paddingHorizontal: 21,
        flex: 1
    },
    rowWrapper: {
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: '#E0E2EE',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    checkedImg: {
        width: 24,
        height: 24
    },
    checkBox: {
        width: 48,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatarImg: {
        width: 33,
        height: 33,
        borderRadius: 16.5,
        marginHorizontal: 10
    }
});
