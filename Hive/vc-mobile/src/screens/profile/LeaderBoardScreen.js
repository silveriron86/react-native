import React from 'react';
import {AsyncStorage, Image, TouchableWithoutFeedback, ImageBackground, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View} from 'react-native';
import {GothamBookText, GothamBoldText, GothamNotRoundedMediumText, GothamMediumText, ComingSoonOverlay} from '../../components';
import DummyConstants from '../../constants/DummyConstants';
import Utils from '../../utils';

const imgBkg = require('../../../assets/images/leaderboard_bg.jpg');
const imgPopover = require('../../../assets/images/popover.png');
const imgAvatar = require('../../../assets/icons/profile/avatar_blue.png');
const radiusBg = require('../../../assets/images/radius_bg.png');

export default class LeaderBoardScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    state = {
        isVisibleTooltip: false,
        user: null
    }

    onPressBody = () => {
        this.setState({
            isVisibleTooltip: false
        });
    }

    showTooltip = () => {
        this.setState({
            isVisibleTooltip: !this.state.isVisibleTooltip
        });
    }

    onPressRow = (item) => {
        this.onPressBody();
    }

    componentDidMount() {
        AsyncStorage.getItem('oio_auth', (err, auth) => {
            if(auth) {
                let user = JSON.parse(auth).user;
                this.setState({user});
            }
        });
    }

    render() {
        const { isVisibleTooltip, user } = this.state;
        const { navigation } = this.props;

        let rows = [];
        let last = DummyConstants.leaderBoardList.length-1;
        DummyConstants.leaderBoardList.forEach((item, index) => {
            rows.push(
                <TouchableOpacity key={`row-${index}`} style={[styles.row, index === 0 && {marginTop: 6}, index === last && {marginBottom: 6}]} onPress={()=>this.onPressRow(item)}>
                    <View style={styles.leftItem}>
                        <Image source={imgAvatar} style={styles.imgAvatar}/>
                        <GothamBookText style={styles.rowLocationText}>{item.location}</GothamBookText>
                    </View>
                    <GothamNotRoundedMediumText style={styles.rowScoreText}>{item.score}</GothamNotRoundedMediumText>
                </TouchableOpacity>
            )
        });

        return (
            <TouchableWithoutFeedback style={styles.container} onPress={this.onPressBody}>
                <View style={styles.container}>
                    <StatusBar hidden={true}/>
                    <View style={styles.header}>
                        <View style={styles.headerImgWrapper}>
                            <Image source={imgBkg} style={styles.headerImg}/>
                            <View style={styles.innerFrame}></View>
                            <Image source={radiusBg} style={styles.radiusBg}/>
                        </View>                        
                        
                        <View style={styles.title}>
                            <GothamBoldText style={styles.titleText}>LEADERBOARD</GothamBoldText>
                        </View>
                    </View>
                    <TouchableOpacity onPress={()=>navigation.goBack()} style={styles.closeBtn}>
                        <Image source={require('../../../assets/icons/close.png')} />
                    </TouchableOpacity>
                    {
                        isVisibleTooltip &&
                        <View onPress={this.showTooltip} style={styles.popoverView}>
                            <Image source={imgPopover} style={styles.popoverBkg}/>
                            <GothamBookText style={styles.tooltipText}>Your Volunteer Impact Score shows everything you do to support your community, from volunteering to recruiting{"\n"}volunteers to rating your experience.</GothamBookText>
                        </View>                    
                    }

                    <View style={{position: 'relative', width: '100%', flex: 1}}>
                        <View style={{position: 'relative', zIndex: 9}}>
                            <GothamNotRoundedMediumText style={styles.totalPriceText}>350.5</GothamNotRoundedMediumText>
                            {
                                isVisibleTooltip &&
                                <Image source={require('../../../assets/icons/triangle.png')} style={[styles.tooltipBtn, styles.triangleImg]}/>
                            }
                            <TouchableOpacity onPress={this.showTooltip} style={styles.tooltipBtn}>
                                <Image source={require('../../../assets/icons/tooltip.png')} style={styles.tooltipImg}/>
                            </TouchableOpacity>
                        </View>
                        {
                            user &&
                            <GothamMediumText style={styles.scoreText}>{user.lastname}’s Volunteer Score</GothamMediumText>
                        }
                        
                        <View style={{paddingHorizontal: 15, width: '100%', marginTop: 12}}>
                            <View style={styles.topBorder}></View>
                        </View>
                        <View style={styles.dashboardView}>
                            <View style={styles.flex}>
                                <GothamNotRoundedMediumText style={styles.greenText}>290</GothamNotRoundedMediumText>
                                <GothamMediumText style={styles.grayText}>HOURS{"\n"}VOLUNTEERED</GothamMediumText>
                            </View>
                            <View style={styles.divider}></View>
                            <View style={styles.flex}>
                                <GothamNotRoundedMediumText style={styles.greenText}>15</GothamNotRoundedMediumText>
                                <GothamMediumText style={styles.grayText}>PROJECTS{"\n"}SHARED</GothamMediumText>
                            </View>
                            <View style={styles.divider}></View>
                            <View style={styles.flex}>
                                <GothamNotRoundedMediumText style={styles.greenText}>10</GothamNotRoundedMediumText>
                                <GothamMediumText style={styles.grayText}>PROJECTS{"\n"}REVIEWED</GothamMediumText>
                            </View>
                            <View style={styles.divider}></View>
                            <View style={styles.flex}>
                                <GothamNotRoundedMediumText style={styles.greenText}>102</GothamNotRoundedMediumText>
                                <GothamMediumText style={styles.grayText}>VOLUNTEERS{"\n"}RECRUITED</GothamMediumText>
                            </View>
                        </View>
                        <ScrollView style={styles.scrollView}><View onStartShouldSetResponder={() => true}>{rows}</View></ScrollView>
                        <ComingSoonOverlay style={{top: 0, paddingTop: 50}}/>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    header: {
        width: '100%',
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center'        
    },
    headerImgWrapper: {
        width: '100%', 
        height: 166
    },
    headerImg: {
        width: '100%',
        height: 166,
        resizeMode: 'cover',
        opacity: 0.66,
    },
    radiusBg: {
        width: '100%',
        height: 27,
        resizeMode: 'stretch',
        position: 'absolute',
        bottom: 0
    },
    innerFrame: {
        position: 'absolute',
        width: '100%',
        height: 166,
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, .22)'
    },  
    title: {
        // height: 166,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center'
    },  
    titleText: {
        fontSize: 22,
        color: 'white'
    },
    closeBtn: {
        position: 'absolute',
        left: 17,
        top: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 18,
        width: 27,
        height: 27
    },
    totalPriceText: {
        paddingTop: 5,
        fontSize: 30,
        lineHeight: 22,
        color: '#1D75BD',
        textAlign: 'center'
    },
    scoreText: {
        color: '#1D75BD',
        fontSize: 16,
        lineHeight: 22,
        textAlign: 'center'
    },
    topBorder: {
        width: '100%',
        height: 1,
        backgroundColor: '#E0E2EE'
    },
    dashboardView: {
        width: '100%',
        height: 84,
        paddingHorizontal: 10,
        marginBottom: 5,
        backgroundColor: 'white',
        shadowColor: '#ADADAD',
        shadowOffset: { width: 2, height: 4 },
        shadowOpacity: 0.19,     
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        zIndex: 8
    },
    scrollView: {
        width: '100%',
        flex: 1,
        paddingHorizontal: 21
    },
    flex: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    greenText: {
        color: '#8DC63F',
        fontSize: 19
    },
    grayText: {
        color: '#CACACA',
        fontSize: 11,
        textAlign: 'center',
        marginTop: 6
    },
    divider: {
        width: 1,
        height: 69,
        backgroundColor: '#E0E2EE',
        marginHorizontal: 5
    },
    imgAvatar: {
        width: 33,
        height: 33,
        marginRight: 9
    },
    rowLocationText: {
        color: '#777777',
        fontSize: 13,
        marginTop: 5
    },
    rowScoreText: {
        color: '#8DC63F',
        fontSize: 13,
        marginTop: 6
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#E0E2EE',
        height: 50,
        paddingHorizontal: 11
    },
    leftItem: {
        flexDirection: 'row',   
        alignItems: 'center'   
    },
    tooltipText: {
        color: '#303030',
        fontSize: 14,
        paddingHorizontal: 18,
        paddingVertical: 10,
        lineHeight: 19
    },
    tooltipImg: {
        width: 17,
        height: 17
    },
    tooltipBtn: {
        position: 'absolute',
        right: 0,
        marginRight: -39,
        marginTop: -3,
        padding: 6,
        zIndex: 999
    },
    triangleImg: {
        marginTop: -18,
        marginRight: -36
    },
    popoverView: {
        position: 'absolute',
        top: 50,
        width: 342,
        height: 108,
        zIndex: 999
    },
    popoverBkg: {
        position: 'absolute'
    }
});
