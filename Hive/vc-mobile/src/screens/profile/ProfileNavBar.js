import React from 'react';
import {Image, StyleSheet, TouchableOpacity,} from 'react-native';
import {Header} from 'react-native-elements';

const imgMenuIcon = require('../../../assets/icons/menu_button_white.png');

const LeftButton = (props) => (
    <TouchableOpacity onPress={props.onPress} style={styles.leftButton}>
        <Image source={imgMenuIcon} style={styles.leftButtonIcon}/>
    </TouchableOpacity>
);

export default ProfileNavBar = (props) => {
    const {onLeftButtonPress} = props;
    return (
        <Header
            backgroundColor='transparent'
            containerStyle={styles.container}
            leftComponent={<LeftButton onPress={onLeftButtonPress}/>}
        />
    );
}

const styles = StyleSheet.create({
    container: {
        borderBottomWidth: 0
    },    
    leftButton: {
        width: 46,
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
    },
    leftButtonIcon: {
        width: 23,
        height: 16,
        resizeMode: 'contain',
    },
});

 