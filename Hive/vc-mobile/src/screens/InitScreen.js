import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  ImageBackground,
  TouchableOpacity,
  NativeModules,
  AsyncStorage
} from 'react-native';

import { GothamBookText,
  GothamNotRoundedMediumText,
  QueenClubsText,
  SocialButton,
  TextButton,
  CornerLabel
} from '../components';
import Utils from '../utils';
import ApiConstants from '../constants/ApiConstants';
import Config from 'react-native-config'

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

const imgLogo = require('../../assets/images/logo.png');
const imgBkg = require('../../assets/images/bkground2.jpg');

export default class InitScreen extends React.Component {
  state = {
    result: null,
    socialType: ''
  }

  handleSocialLogin = (type) => {
    this.setState({
      socialType: type
    },() => {
      NativeModules.OAuth.open(type); 
      this.updateStatus();  
    })
  }

  updateStatus = () => {
    NativeModules.OAuth.getStatus(user => {
      console.log(user);
      AsyncStorage.setItem('SOCIAL_LOGIN', JSON.stringify(user));
      // this.props.navigation.navigate('Home');
      this.goSelectType();
    })
  }

  handleEmailLogin = () => {
    AsyncStorage.removeItem('SOCIAL_LOGIN');
    this.goSelectType();
  }

  goSelectType = () => {
    this.props.navigation.navigate('SelectType', {
      socialType: this.state.socialType
    });
  }

  render() {
    return (
      <ImageBackground source={imgBkg} style={{width: '100%', height: '100%'}} imageStyle={{opacity: 0.24}}>
        <View style={styles.container}>
          { Config.ENVIRONMENT === 'DEV' && <CornerLabel>DEV</CornerLabel> }
          <Image style={styles.logo} source={imgLogo} />
          <GothamBookText style={styles.title}>Help others.{"\n"}<GothamBookText style={{color: '#8DC63F'}}>Go far in life.</GothamBookText></GothamBookText>
          <GothamNotRoundedMediumText style={styles.description}>Find, schedule, share, and rate{'\n'}volunteer opportunities. FREE</GothamNotRoundedMediumText>
          <View style={styles.socialRow}>
            <QueenClubsText style={styles.socialText}>JOIN WITH</QueenClubsText>
            <View style={styles.socialButtonWrapper}>
              <SocialButton type='facebook' onPress={()=>this.handleSocialLogin('facebook')} />
            </View>
            <View style={styles.socialButtonWrapper}>
              <SocialButton type='google' onPress={()=>this.handleSocialLogin('google')} />
            </View>
             <View style={styles.socialButtonWrapper}>
              <SocialButton type='instagram' onPress={()=>this.handleSocialLogin('instagram')} />
            </View>
            <View style={styles.socialButtonWrapper}>
              <SocialButton type='snapchat' onPress={()=>this.handleSocialLogin('snapchat')} />
            </View>
            <View style={styles.socialButtonWrapper}>
              <SocialButton type='email' onPress={this.handleEmailLogin} />
            </View>
          </View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={styles.signin}>Sign In</Text>
          </TouchableOpacity>
          {this.state.result ? (
          <Text>{JSON.stringify(this.state.result)}</Text>
        ) : null}
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0
  },
  logo: {
    marginTop: 25,
    width: 148,
    height: 56,
    resizeMode: 'contain',
  },
  slogan: {
    marginTop: 150,
    width: 284,
    height: 100,
    resizeMode: 'contain',
    marginBottom: 18,
  },
  socialRow: {
    width: '100%',
    height: 87,
    backgroundColor: '#1D75BD',
    marginTop: 36,
    marginBottom: 33,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  socialText: {
    color: 'white',
    marginTop: 2,
    fontSize: 24
  },
  socialButtonWrapper: {
    marginLeft: 10,
  },
  title: {
    fontSize: 45,
    color: '#1D75BD',
    lineHeight: 50,
    marginTop: 111,
    letterSpacing: 0,
    // maxFontSizeMultiplier: 1, // not supported until RN 0.58+
    // allowFontScaling: false, // getting "not supported"
  },
  description: {
    textAlign: 'center', 
    marginTop: 20, 
    fontSize: 20, 
    lineHeight: 26,
    color: '#333333',
    fontWeight: '500'
  },
  signin: {
    color: '#24B6EC',
    fontSize: 14,
    fontWeight: '500'
  },
});
