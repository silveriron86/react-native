import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  View,
  Alert
} from 'react-native';
import axios from 'axios';
import ApiConstants from '../../constants/ApiConstants';
import Utils from '../../utils';
import { PrimaryButton, QueenClubsText, GothamNotRoundedBookText, GothamBookText, LoadingOverlay } from '../../components';
import DummyConstants from '../../constants/DummyConstants'

const imgBkg = require('../../../assets/images/bkground2.jpg');

export default class ThankYouScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }

  onThankyou = () => {
    const { navigation } = this.props;
    let data =navigation.getParam('data');

    this.setState({
      loading: true
    }, () => {
      let user_id = DummyConstants.TEMP_USER_ID;
      console.log(`${Config.API_HOST}accounts/${user_id}/relateds`, data);
      let options = {
        method: 'POST',
        url: `${Config.API_HOST}accounts/${user_id}/relateds`,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        data: data
      }
  
      axios(options).then(response => {
        console.log(' --- Account/{id}/relateds (POST) ---');
        if(response.status === 200) {
          console.log(response.data)
          this.setState({
            loading: false
          }, () => {        
            navigation.navigate('Init')
          })
        }
      })
      .catch(error => {
        console.log(error.response.data);
        this.setState({
          loading: false
        }, () => {      
          Alert.alert('Error', error.response.data.error.message, {text: 'OK', onPress: () => navigation.goBack(null)});
        });
      })  
    }) 
  }
  render() {
    const { loading } = this.state;
    return (
      <View style={styles.container}>
        <LoadingOverlay loading={loading}/>
        <ImageBackground source={imgBkg} style={{width: '100%', height: '100%'}} imageStyle={{opacity: 0.1}}>
          <View style={[styles.center, styles.titleWrapper]}>
            <QueenClubsText style={styles.title}>We know how hard it is to{"\n"} manage parents.</QueenClubsText>
          </View>
          <View style={styles.center}>
            <GothamNotRoundedBookText style={styles.p}>So we’ll take it from here.</GothamNotRoundedBookText>
            <GothamNotRoundedBookText style={styles.p}>After your parents have created your volunteer profile, you can receive notifications for volunteer opportunities, project reminders, and invite your friends to join you.</GothamNotRoundedBookText>
          </View>
          <View style={styles.continueBtn}>
            <PrimaryButton
                onPress={this.onThankyou}
                title="THANK YOU!"
            />          
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0
  },
  titleWrapper: {
    backgroundColor: '#8DC63F',
    paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
    height: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 169 : 129,
    justifyContent: 'center',
    alignItems: 'center'
  },  
  title: {
    textAlign: 'center',
    fontSize: 36,
    lineHeight: 33,
    color: 'white',
    letterSpacing: 0.8,
    marginTop: 17
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  p: {
    width: 322,
    marginTop: 21,
    textAlign: 'center',
    fontSize: 16,
    color: '#303030',
    lineHeight: 20
  },
  continueBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 48
  }
});

 