import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  View,
  Text,
  TextInput,
  NativeModules,
  AsyncStorage,
  Alert
} from 'react-native';
import Config from 'react-native-config';
import axios from 'axios';
import { TextInputMask } from 'react-native-masked-text'
import Utils from '../../utils';

import { PrimaryButton, DateInput, PickerSelect, LoadingOverlay, Tooltip } from '../../components';

const imgBkg = require('../../../assets/images/bkground2.jpg');

const STUDENT_GRADES = [
  {
      label: '12th Grade - graduating 2020',
      value: '2020'
  },
  {
    label: '11th Grade - graduating 2021',
    value: '2021'
  },
  {
    label: '10th Grade - graduating 2022',
    value: '2022'
  },
  {
    label: '9th Grade - graduating 2023',
    value: '2023'
  },
  {
    label: '8th Grade - graduating 2024',
    value: '2024'
  },
  {
    label: '7th Grade - graduating 2025',
    value: '2025'
  },
  {
    label: '6th Grade - graduating 2026',
    value: '2026'
  },
  {
    label: '5th Grade - graduating 2027',
    value: '2027'
  },
  {
    label: '4th Grade - graduating 2028',
    value: '2028'
  },
  {
    label: '3rd Grade - graduating 2029',
    value: '2029'
  },
  {
    label: '2nd Grade - graduating 2030',
    value: '2030'
  },
  {
    label: '1st Grade - graduating 2031',
    value: '2031'
  },
  {
    label: 'Kindergarten - graduating 2032',
    value: '2032'
  }
];

export default class AddStudentScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      student_email: '',
      student_firstname: '',
      student_lastname: '',
      student_mobile: '',
      student_birthday: '',
      student_grade: '',
      loading: false,
      isVisibleTooltip: false
    }
  }

  onChangeDate = (date) => {
    this.setState({
      isVisibleTooltip: false,
      student_birthday: date
    });
  }

  _onSelect = ( item ) => {
    console.log(item);
  };

  handleSelect = (grade) => {
    this.setState({
      student_grade: grade
    })
  }

  handleContinue = () => {
    const { 
      student_email,
      student_firstname,
      student_lastname,
      student_mobile,
      student_birthday,
      student_grade
    } = this.state;
    const { navigation } = this.props;

    if(!student_email) {
        Alert.alert('Error', "Please enter the student's email address.");
        return;
    }

    if(!Utils.validateEmail(student_email)) {
        Alert.alert('Error', "Please enter the student's valid email address.");
        return;
    }

    if(!student_firstname) {
      Alert.alert('Error', "Please enter the student's first name.");
      return;
    }
    
    if(!student_lastname) {
      Alert.alert('Error', "Please enter the student's last name.");
      return;
    }     

    if(student_mobile && student_mobile.length < 14) {
      Alert.alert('Error', "Please enter a valid mobile phone number.");
      return;      
    }
    
    if(!student_grade) {
      Alert.alert('Error', "Please enter the student's grade.");
      return;
    }     
    
    if(!student_birthday) {
      Alert.alert('Error', "Please enter the student's birthday.");
      return;
    }    

    let param = navigation.getParam('data');
    let user = param.user;
    let token = param.token;
    let data = {
      accountName: user.accountName,
      firstname: student_firstname,
      lastname: student_lastname,
      email: student_email.toLowerCase(),
      mobile: student_mobile,
      gradYear: student_grade,
      birthdate: student_birthday,
      type: 'Parent'
    }

    this.setState({
      loading: true
    }, () => {
      let options = {
        method: 'POST',
        url: `${Config.API_HOST}accounts/${user.id}/relateds`,
        headers: {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
        data: data
      }
  
      axios(options).then(response => {
        if(response.status === 200) {
          this.setState({
            loading: false
          }, () => {        
            AsyncStorage.setItem('oio_auth', JSON.stringify(param));
            this.props.navigation.navigate('Main', param);
          })
        }
      })
      .catch(error => {
        console.log(error, error.response.data);
        this.setState({
          loading: false
        }, () => {      
          Alert.alert('Error', 'Creating failed');
        });
      })  
    })
  }

  toggleTooltip = () => {
    this.setState({
      isVisibleTooltip: !this.state.isVisibleTooltip
    });
  }

  hideTooltip = () => {
    this.setState({
      isVisibleTooltip: false
    })
  }

  render() {
    const { 
      student_email,
      student_firstname,
      student_lastname,
      student_mobile,
      student_birthday,
      student_grade,
      loading,
      isVisibleTooltip
    } = this.state;

    return (
      <View style={styles.container}>
        <ImageBackground source={imgBkg} style={{width: '100%', height: '100%'}} imageStyle={{opacity: 0.1}}>
          <LoadingOverlay loading={loading}/>
          <Text style={styles.title}>ADD A STUDENT</Text>
          <View style={styles.center}>
            <Text style={styles.p}>Parents and guardians will receive volunteer project alerts.</Text>
          </View>
          <View style={styles.center}>
            <View style={styles.form}>
              <TextInput style={styles.textInput} placeholder="Student's Email" placeholderTextColor="#606060" keyboardType={'email-address'} onChangeText={(student_email) => this.setState({student_email})} autoCapitalize={'none'} onFocus={this.hideTooltip}/>       
              <TextInput style={styles.textInput} placeholder="First Name" placeholderTextColor="#606060"  onChangeText={(student_firstname) => this.setState({student_firstname})} onFocus={this.hideTooltip}/>       
              <TextInput style={styles.textInput} placeholder="Last Name" placeholderTextColor="#606060"  onChangeText={(student_lastname) => this.setState({student_lastname})} onFocus={this.hideTooltip}/>       
              <View style={{position: 'relative', width: '100%'}}>
                <TextInputMask
                  type={'custom'}
                  options={{
                    mask: '(999) 999-9999'
                  }}
                  value={this.state.text}
                  onChangeText={text => {
                    this.setState({
                      student_mobile: text
                    })
                  }}
                  keyboardType={'phone-pad'} 
                  style={[styles.textInput, {paddingLeft: 28}]}
                  placeholder="Student Mobile Phone (optional)"
                  placeholderTextColor="#606060"
                  onFocus={this.hideTooltip}
                  onBlur={this.hideTooltip}
                />
                <Tooltip iconStyle={{marginTop: 14}} text='Add phone to receive SMS updates about volunteer projects.' visible={isVisibleTooltip} onToggle={this.toggleTooltip}/>
              </View>
              <PickerSelect placeholder="Student's Grade" items={STUDENT_GRADES} value={student_grade} onChange={this.handleSelect}/>
              <View style={styles.dateContainer}>
                <DateInput   
                  style={{width: '100%'}}               
                  placeholderTextColor='#606060'
                  date={student_birthday} 
                  placeholder='Student Birthday MM-DD-YYYY' 
                  tooltip='We collect birthdays for different legal reasons.'
                  onChange={this.onChangeDate}/>
              </View>  
            </View>
          </View>
          <View style={styles.continueBtn}>
            <PrimaryButton
                onPress={this.handleContinue}
                title="CONTINUE"
            />
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0
  },
  title: {
    marginTop: 35,
    textAlign: 'center',
    fontSize: 20,
    color: '#8DC63F',
    textTransform: 'uppercase'
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  p: {
    width: 322,
    marginTop: 10,
    textAlign: 'center',
    fontSize: 14,
    color: '#303030',
  },
  form: {
    width: 334,
    justifyContent: 'center',
    alignItems: 'center',
  },
  continueBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 45
  },
  textInput: {
    height: 50, 
    borderBottomColor: 'gray', 
    borderBottomWidth: 1.5,
    width: '100%',
    paddingHorizontal: 13,
    marginTop: 8,
    paddingBottom: 0
  },
  dateContainer: {
    // marginLeft: -37, 
    width: 334, 
    marginTop: 15
  },
  addBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 10
  },
  addBtnText: {
    color: '#24B6EC',
    fontSize: 13
  },
  optionalText: {
    fontSize: 10,
    color: '#8E8E8E',
    textAlign: 'left',
    marginTop: 10
  },
  explanationText: {
    fontSize: 11,
    fontStyle: 'italic',
    color: 'gray',
    textTransform: 'uppercase',
    alignSelf: 'flex-start',
    marginTop: 5
  },
});