import React from 'react';
import {
    Alert,
    Image,
    ImageBackground,
    KeyboardAvoidingView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    AsyncStorage,
    NativeModules
} from 'react-native';
import axios from 'axios';

import {GothamBookText, PrimaryButton, LoadingOverlay, CornerLabel} from '../../components';
import Utils from '../../utils';
import ApiConstants from '../../constants/ApiConstants';
import General from '../../constants/General';
const imgLogo = require('../../../assets/images/logo.png');
const imgBkg = require('../../../assets/images/bkground2.jpg');
import Config from 'react-native-config'

export default class CreateAccountScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.showHidePassword = this.showHidePassword.bind(this);
        this.state = {
            email: '',
            password: '',
            firstname: '',
            lastname: '',            
            showPassword: true,
            loading: false
        }
    }

    handleSignin = () => {
        this.props.navigation.navigate("Login");
    };

    openPrivaciy = () => {
        console.log('privacy policy');
    }

    openTerms = () => {
        console.log('terms');
    }

    showHidePassword() {
        this.setState({showPassword: !this.state.showPassword});
    };

    handleAccept = () => {
        const { email, password, firstname, lastname } = this.state;
        const { navigation } = this.props;
        if(!firstname) {
            Alert.alert('', 'Please enter your first name.');
            return;
        }
        
        if(!lastname) {
            Alert.alert('', 'Please enter your last name.');
            return;
        }  

        if(!email) {
            Alert.alert('', 'Please enter your email address.');
            return;
        }

        if(!Utils.validateEmail(email)) {
            Alert.alert('', 'Please enter a valid email address.');
            return;
        }
        
        if(!password) {
            Alert.alert('', 'Please enter a password.');
            return;
        }      

        let data = {
            email: email.toLowerCase(),
            password: password,
            firstname: firstname,
            lastname: lastname,
            birthdate: navigation.getParam('birthdate'),
            type: navigation.getParam('type')
        };

        this.setState({
            loading: true
        }, () => {
            NativeModules.OAuth.signup(Utils.serialize(data)); 
            NativeModules.OAuth.getStatus(res => {
                if(res.status === "success") {
                    let options = {
                        method: 'POST',
                        url: 'https://oauth.io/api/usermanagement/signin?k=' + Config.OAUTH_KEY,
                        data: {
                            email: email.toLowerCase(),
                            password: password,
                            as_jwt: 'true'
                        }
                    }
                    axios(options).then(response => {
                        if(response.status === 200) {
                            this._oauthLogin(response.data, data);
                        }
                    })
                    .catch(error => {
                        console.log(error.response.data);
                        this.setState({
                            loading: false
                        }, () => {                                
                            Alert.alert('Error', 'Login failed');
                        })
                    })
                }else {
                    this.setState({
                        loading: false
                    }, () => {
                        Alert.alert('Error', res.message.replace('[railsUm] Error: [railsUm] ', ''));
                    })
                }                    
            })
        })
    }

    _oauthLogin = (data, payload) => {
        let provider = 'oauth';
        let options = {
            method: 'POST',
            url: Config.API_HOST + 'accounts/signIn?provider=' + provider,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            data: data
        }

        axios(options).then(response => {
            if(response.status === 200) {
                AsyncStorage.getItem('SIGNUP_TYPE', (err, type) => {
                    let nav = this.props.navigation;
                    axios({
                        method: 'PATCH',
                        url: Config.API_HOST + 'accounts/' + 
                        response.data.user.id,
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: response.data.token
                        },
                        data: {
                            birthdate: payload.birthdate,
                            type: payload.type
                        }
                    }).then(res => {
                        console.log(res.data);
                        this.setState({
                            loading: false
                        }, () => {            
                            if(type === '2') {
                                // parent
                                nav.navigate("AddStudent", {
                                    data: response.data
                                });
                            }else {
                                AsyncStorage.setItem('oio_auth', JSON.stringify(response.data));
                                nav.navigate('Main', response.data);
                            }
                        })
                    })
                    .catch(error => {
                        console.log(error.response.data);
                        this.setState({
                            loading: false
                        }, () => {
                            Alert.alert('Error', 'Signup failed');
                        })
                    })  
                })
            }
        })
        .catch(error => {
            console.log(error.response.data);
            this.setState({
                loading: false
            }, () => {              
                Alert.alert('Error', 'Login failed');
            })
        })        
    }    

    render() {
        const {username, password, loading} = this.state;
        return (
            <ImageBackground source={imgBkg} style={{width: '100%', height: '100%'}} imageStyle={{opacity: 0.1}}>
                { Config.ENVIRONMENT === 'DEV' && <CornerLabel>DEV</CornerLabel> }
                <KeyboardAvoidingView style={styles.container}>
                    <LoadingOverlay loading={loading}/>

                    <Image style={styles.logo} source={imgLogo}/>
                    <View style={styles.titleTextWrapper}>
                        <GothamBookText
                            style={{textAlign: 'center', fontSize: 20}}>
                            LET'S CREATE AN ACCOUNT
                        </GothamBookText>
                    </View>
                    
                    <View style={styles.center}>
                        <View style={styles.form}>
                            <TextInput style={styles.textInput} placeholder="First Name" placeholderTextColor="#606060"  onChangeText={(firstname) => this.setState({firstname})}/>
                            <TextInput style={styles.textInput} placeholder="Last Name" placeholderTextColor="#606060"  onChangeText={(lastname) => this.setState({lastname})}/>       
                            <TextInput style={styles.textInput} placeholder="Email Address" placeholderTextColor="#606060" keyboardType={'email-address'} onChangeText={(email) => this.setState({email})} autoCapitalize={'none'}/>
                            <View style={styles.pwdInputRow}>
                                <TextInput
                                    style={styles.textInput}
                                    placeholderTextColor="#606060"
                                    placeholder="Password"
                                    onChangeText={(password) => this.setState({password})}
                                    secureTextEntry={this.state.showPassword}
                                    autoCapitalize={'none'}
                                    value={password}
                                />

                                <TouchableOpacity style={styles.showPassBtn} onPress={this.showHidePassword} value={!this.state.showPassword}>
                                    <Text style={styles.showPassText}>{this.state.showPassword ? 'Show' : 'Hide'}</Text>
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.passwordComplexityText}>
                                {General.PASSWORD_COMPLEXITY_PHRASE}
                            </Text>                            
                        </View>
                    </View>
                    <View style={styles.continueBtn}>
                        <PrimaryButton
                            onPress={this.handleAccept}
                            title="ACCEPT & GET STARTED"
                        />          
                    </View> 

                    <Text style={styles.alreadyHaveText}>Already have an account?</Text>
                    <TouchableOpacity onPress={this.handleSignin} style={styles.signinBtn}>
                        <Text style={styles.signinText}>Sign In</Text>
                    </TouchableOpacity>

                    <View style={styles.footer}>
                        <Text style={styles.byTapping}>By tapping Accept & Get Started, you acknowledge that you have read the <Text style={styles.textLink} onPress={this.openPrivaciy}>Privacy Policy</Text> and agree to the <Text style={styles.textLink} onPress={this.openTerms}>Terms of Service</Text>.</Text>
                    </View>
                </KeyboardAvoidingView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0
    },
    logo: {
        marginTop: 35,
        width: 148,
        height: 56,
        resizeMode: 'contain',
    },
    showPassBtn: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 12,
        paddingVertical: 18
    },
    showPassText: {
        color: '#24B6EC',
        fontSize: 13
    },
    titleTextWrapper: {
        marginTop: 35,
        marginBottom: 20,
    },
    alreadyHaveText: {
        fontSize: 14,
        color: '#303030'
    },
    passwordComplexityText: {
        fontSize: 11,
        fontStyle: 'italic',
        color: 'gray',
        textTransform: 'uppercase',
        alignSelf: 'flex-start',
        marginTop: 5
    },
    signinText: {
        fontSize: 14,
        color: '#24B6EC'
    },
    byTapping: {
        fontSize: 11,
        lineHeight: 18,
        color: '#333333'
    },
    textLink: {
        color: '#1D75BD'
    },
    footer: {
        paddingHorizontal: 32,
        marginTop: 40
    },
    continueBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 19,
        marginBottom: 31
    },  
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    form: {
        width: 334,
        justifyContent: 'center',
        alignItems: 'center',
    },
    pwdInputRow: {
        position: 'relative',
        width: '100%'
    },
    textInput: {
        height: 50, 
        borderBottomColor: 'gray', 
        borderBottomWidth: 1.5,
        width: '100%',
        paddingHorizontal: 13,
        marginTop: 8,
        paddingBottom: 0
    },
    signinBtn: {
        paddingVertical: 5
    }       
});
