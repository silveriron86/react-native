import React from 'react';
import {
  Alert,
  ImageBackground,
  StyleSheet,
  View,
  Image, 
  Text,
  TextInput,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import Utils from '../../utils';
import { PrimaryButton, DateInput, RadioForm, GothamBookText, GothamNotRoundedBookText } from '../../components';

const imgBkg = require('../../../assets/images/bkground2.jpg');
const plusImg = require('../../../assets/icons/plus.png');
const optsParentType = [
  {
    label: 'MOM',
    value: '1'
  },
  {
    label: 'DAD',
    value: '2'
  },
  {
    label: 'GUARDIAN',
    value: '3'
  }
];

export default class AddParentScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      parentType: '',
      email: '',
      firstname: '',
      lastname: '',
      parent_mobile: ''
    }
  }

  handleSubmit = () => {
    const { parentType, email, firstname, lastname, parent_mobile } = this.state;
    const { navigation } = this.props;
    if(!parentType) {
      Alert.alert('', 'Please select Mom, Dad or Guardian.');
      return;
    }
    if(!email) {
      Alert.alert('', 'Please enter an email address.');
      return;
    }
    if(!Utils.validateEmail(email)) {
      Alert.alert('', 'Please enter a valid email address.');
      return;
    }
    if(!firstname) {
        Alert.alert('', 'Please enter a first name.');
        return;
    }
    if(!lastname) {
      Alert.alert('', 'Please enter a last name.');
      return;
    }
    if(!parent_mobile || parent_mobile.length < 14) {
      Alert.alert('', 'Please enter a valid phone number.');
      return;
    }
  
    let data = {
        // parentType: parentType, // This type(mom, dad, guardian) value will be used later
        type: 'Parent',
        email: email.toLowerCase(),
        firstname: firstname,
        lastname: lastname,
        phone: parent_mobile,
        birthdate: navigation.getParam('birthdate')
    };
    
    navigation.navigate('ThankYou', {
      data: data
    })
}

  _onSelectParentType = ( parentType ) => {
    console.log(`Selected parentType:${JSON.stringify(parentType)}`);
    this.setState({
      parentType: parentType.value
    })
  };

  render() {
    const { date } = this.state;

    return (
      <View style={styles.container}>
        <ImageBackground source={imgBkg} style={{width: '100%', height: '100%'}} imageStyle={{opacity: 0.1}}>
          <GothamBookText style={styles.title}>ADD PARENT OR GUARDIAN</GothamBookText>
          <View style={styles.center}>
            <GothamNotRoundedBookText style={styles.p}>Students under 13 must have a parent or guardian on the account to schedule projects.</GothamNotRoundedBookText>
          </View>
          <View style={[styles.center, {marginTop: 30, marginBottom: 28}]}>
            <RadioForm
              style={{ width: Dimensions.get('window').width - 40 }}
              dataSource={optsParentType}
              itemShowKey="label"
              itemRealKey="value"
              circleSize={24}
              initial={-1}
              formHorizontal={true}
              labelHorizontal={true}
              innerColor='#8E8E8E'
              outerColor='#8E8E8E'
              onPress={(item) => this._onSelectParentType(item)}
            />   
          </View>
          <View style={styles.center}>
            <View style={styles.form}>
              <TextInput style={styles.textInput} placeholder="Parent or Guardian Email" keyboardType={'email-address'} placeholderTextColor="#606060" onChangeText={(email) => this.setState({email})} autoCapitalize={'none'}/>
              <TextInput style={styles.textInput} placeholder="First Name" placeholderTextColor="#606060" onChangeText={(firstname) => this.setState({firstname})}/>       
              <TextInput style={styles.textInput} placeholder="Last Name" placeholderTextColor="#606060" onChangeText={(lastname) => this.setState({lastname})}/>          

              <TextInputMask
                type={'custom'}
                options={{
                  mask: '(999) 999-9999'
                }}
                value={this.state.text}
                onChangeText={text => {
                  this.setState({
                    parent_mobile: text
                  })
                }}
                keyboardType={'phone-pad'} 
                style={styles.textInput}
                placeholder="Parent Mobile Phone"
                placeholderTextColor="#606060"
              />                        
            </View>
          </View>
          <View style={styles.continueBtn}>
            <PrimaryButton
              onPress={this.handleSubmit}
              // onPress={() => this.props.navigation.navigate('ThankYou')}
                title="CONTINUE"
            />
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0
  },
  title: {
    marginTop: 35,
    textAlign: 'center',
    fontSize: 20,
    color: '#8DC63F',
    textTransform: 'uppercase'
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  p: {
    width: 322,
    marginTop: 10,
    textAlign: 'center',
    fontSize: 14,
    color: '#303030',
  },
  form: {
    width: 334,
    justifyContent: 'center',
    alignItems: 'center',
  },
  continueBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 45
  },
  textInput: {
    fontFamily: 'Gotham-Book',
    fontSize: 15,
    height: 50, 
    borderBottomColor: '#606060', 
    borderBottomWidth: 2,
    width: '100%',
    paddingHorizontal: 13,
    marginTop: 8,
    paddingBottom: 0
  },
  dateContainer: {
    // marginLeft: -37, 
    width: 334, 
    marginTop: 15
  },
  addBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 15
  },
  addBtnText: {
    fontFamily: 'Gotham-Medium',
    color: '#24B6EC',
    fontSize: 13,
    marginTop: 3
  },
  plusImg: {
    marginLeft: 15, 
    marginTop: 0,
    width: 15, 
    height: 15
  }
});

 