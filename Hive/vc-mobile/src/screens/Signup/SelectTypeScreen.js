import React from 'react';
import {
  Alert,
  ImageBackground,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import ApiConstants from '../../constants/ApiConstants';
import Config from 'react-native-config'
import axios from 'axios';
import moment from 'moment';
import Utils from '../../utils';
import { PrimaryButton, DateInput, LoadingOverlay, CornerLabel } from '../../components';
const leftArrow = require('../../../assets/icons/left_arrow_white.png');

const imgBkg = require('../../../assets/images/bkground2.jpg');
const TYPES = [
  {
    id: 1,
    name: "K-12 Student",
    image: require('../../../assets/icons/role/21_K-12-Student.png')
  },
  {
    id: 2,
    name: "Parent",
    image: require('../../../assets/icons/role/22_Parent.png')
  },
  {
    id: 3,
    name: "College Student",
    image: require('../../../assets/icons/role/23_College-Student.png')
  },
  {
    id: 4,
    name: "Educator",
    image: require('../../../assets/icons/role/24_Educator.png')
  },
  {
    id: 5,
    name: "Volunteer Org",
    image: require('../../../assets/icons/role/25_Volunteer_Transcript.png')
  }
];

export default class SelectTypeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedType: 0,
      birthday: '',
      age: 0,
      loading: false
    }
  }

  onContinue = () => {
    const { birthday } = this.state;
    if(!birthday) {
      Alert.alert('', 'Please enter your birthday.');
      return;
    }
    AsyncStorage.setItem('SIGNUP_TYPE', this.state.selectedType.toString());

    AsyncStorage.getItem('SOCIAL_LOGIN', (err, socialLogin) => {
      let data = {
        birthdate: moment(this.state.birthday, 'MM-DD-YYYY').format('YYYY-MM-DD')
      };
      if(this.state.selectedType != 1 && this.state.age <= 13){
        Alert.alert('','Looks like you are under 13. Please select K-12 Student.');
        return;
      }
      if(this.state.selectedType == 1 && this.state.age < 13) {
        this.props.navigation.navigate('AddParent', data);
      }else {
        if(socialLogin) {
          // login with social
          this.setState({
            loading: true
          }, ()=> {
            this._oauthLogin(JSON.parse(socialLogin));
          })
        }else {
          data['type'] = TYPES[this.state.selectedType-1].name.replace('-', '');
          this.props.navigation.navigate('CreateAccount', data);
        }
      }
    });
  }

  _oauthLogin = (data) => {
    let provider = this.props.navigation.getParam('socialType');
    let options = {
        method: 'POST',
        url: Config.API_HOST + 'accounts/signIn?provider=' + provider,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        data: data
    }
    axios(options).then(response => {
        if(response.status === 200) {
          AsyncStorage.setItem('oio_auth', JSON.stringify(response.data));

          // update birthdate and type
          axios({
            method: 'PATCH',
            url: Config.API_HOST + 'accounts/' + response.data.user.id,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: response.data.token
            },
            data: {
              birthdate: moment(this.state.birthday, 'MM-DD-YYYY').format('YYYY-MM-DD'),
              type: TYPES[this.state.selectedType-1].name.replace('-', '')
            }
          }).then(response => {
            console.log(response.data);
            this.setState({
              loading: false
            }, () => {            
              this.props.navigation.navigate('Home', response.data);
            })
          })
          .catch(error => {
            console.log(error.response.data);
            this.setState({
                loading: false
            }, () => {
                Alert.alert('Error', 'Signup failed');
            })
          })           
        }
    })
    .catch(error => {
        console.log(error.response.data);
        this.setState({
            loading: false
        }, () => {
            Alert.alert('Error', 'Signup failed');
        })
    })        
  }  

  onSelectType = (type) => {
    this.setState({
      selectedType: type
    })
  }

  onChangeDate = (birthday) => {
    const birthdayFormatted = birthday.replace(/-/g, "/");
    const age = Utils.age(birthdayFormatted)

    this.setState({
      birthday: birthday,
      age: age
    });
  }

  render() {
    const { selectedType, birthday, loading } = this.state;
    let rows = [];
    let cols = []
    TYPES.forEach((type, index) => {
      if(index > 0 && index % 3 == 0) {
        rows.push(
          <View style={styles.typesWrapper} key={`type-row-${index}`}>
            {cols}
          </View>
        );
        cols = [];
      }

      let isSelected = (selectedType === type.id);
      cols.push(
        <TouchableOpacity key={`type-${index}`} style={styles.typeItem} onPress={()=> this.onSelectType(type.id)}>
          <View style={[type.name && styles.imgWrapper, isSelected && styles.selectedImgWrapper]}>
            <Image source={type.image} style={[styles.img, isSelected && styles.selectedImg]}/>
          </View>
          <Text style={styles.typeText}>{type.name}</Text>
        </TouchableOpacity>
      )      
    });

    rows.push(
      <View style={styles.typesWrapper} key={`type-row-last`}>
        {cols}
      </View>
    );
    
    let placeholder = '';
    if(selectedType) {
      placeholder = (selectedType === 1) ? 'Student' : TYPES[selectedType - 1].name;
    }

    return (
      <View style={styles.container}>
        { Config.ENVIRONMENT === 'DEV' && <CornerLabel>DEV</CornerLabel> }
        <LoadingOverlay loading={loading}/>
        <View style={styles.containerBack}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Init')}>
            <Image source={leftArrow} style={{tintColor: '#1574b8'}}/>
          </TouchableOpacity>
        </View>
        <ImageBackground source={imgBkg} style={{width: '100%', height: '100%'}} imageStyle={{opacity: 0.1}}>
          <Text style={styles.title}>I AM A...</Text>
          {rows}
          {
            selectedType ?
            <View style={{marginTop: 50}}>
              {
                [1,2,3,4,5].includes(selectedType) &&
                <DateInput 
                  date={birthday} 
                  placeholder={`Birthday`} 
                  tooltip='We collect birthdays for different legal reasons.'
                  onChange={this.onChangeDate}/>
              }
              <View style={styles.continueBtn}>
                <PrimaryButton
                  onPress={this.onContinue}
                  title="CONTINUE"
                />          
              </View>              
            </View>
            :
            null
          }
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
  },
  containerBack: {
    paddingTop: 26,
    paddingLeft: 5,
    paddingRight: 21
  },  
  title: {
    marginTop: 30,
    textAlign: 'center',
    fontSize: 20,
    color: '#303030',
    textTransform: 'uppercase'
  },
  typesWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 28
  },  
  typeItem: {
    width: 115,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0
  },  
  imgWrapper: {
    width: 80,
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40,
    borderWidth: 2,
    borderColor: '#CACACA',
    backgroundColor: 'white'
  },
  typeText: {
    color: '#333333',
    fontSize: 10,
    textAlign: 'center',
    marginTop: 8,
    textTransform: 'uppercase'
  },
  selectedImgWrapper: {
    backgroundColor: '#609ed1'
  },
  selectedImg: {
    tintColor: 'white'
  },
  img: {
    width: 55,
    height: 55
  },
  continueBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 38
  }
});

 