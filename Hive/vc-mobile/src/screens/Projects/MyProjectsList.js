import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, FlatList, Image, Dimensions } from 'react-native';
import { SearchActions } from '../../actions';
import Causes from '../../constants/Causes';
import ProjectSubmissionListItem from '../../screens/Projects/ProjectSubmissionListItem';
import ProjectListItem from '../../screens/Projects/ProjectListItem';

SAMPLE_STATUSES = ['CHECK-IN', 'CHECK OUT', 'CONFIRMED', 'SHOULD_CONFIRM1', 'SHOULD_CONFIRM2', 'COMPLETED'];

const LikeImage = (props) => (
  <>
  {
    (props.item.isOrgLikedByUser || props.item.isProjectLikedByUser) ?
    <Image source={require('../../../assets/icons/liked_filled.png')} style={styles.likesImg}/>
    :
    <Image source={require('../../../assets/icons/heart_unliked_1.png')} style={styles.likesImg}/>
  }
  </>
)

export class MyProjectsList extends React.Component {

  renderRow = (item, index) => {
    const { addHours, goProjectDetails, data, isSearch, isLike, isProject, openMap } = this.props;
    let isCompleted = (isProject === true) && (item.status === 'COMPLETED');
    let itemName = (item.screenType === 'SUBMISSION') ?  item.projectName : item.name ? item.name : item.orgName;

    if(itemName){
      if (itemName.length > 22) {
        itemName = itemName.substring(0,19) + '...'
      }
    }

    let itemName2 = (itemName === item.orgName) ? '' : item.orgName; 
    if(itemName2){
       if (itemName2.length > 30) {
        itemName2 = itemName2.substring(0,27) + '...'
      }
    }

    
    let itemDescription = (item.screenType === 'SUBMISSION') ? item.volunteerSummary : (item.screenType === 'PROJECTSUBSCRIPTION') ? item.description : item.missionStatement;
    if(itemDescription){
      if(itemDescription.length > 50){
        itemDescription = itemDescription.substring(0,47) + '...'
      }
    }


    item.status = SAMPLE_STATUSES[index % SAMPLE_STATUSES.length]

    const itemZipcode = item.zipcode ? item.zipcode : (item.orgZipcode ? item.orgZipcode : '' )
    const itemState = item.state ? item.state : (item.orgState ? item.orgState : '' )
    const itemCity = item.city ? item.city : (item.orgCity ? item.orgCity : '' )
    let itemImageUrl;
    if(item.nteeCodeImgURL){
      itemImageUrl = item.nteeCodeImgURL
    }else{
      itemImageUrl = 'https://volunteercrowd--c.documentforce.com/servlet/servlet.ImageServer?id=0151U000001WYBu&oid=00D1U0000013VdE&lastMod=1556775654000';
    }

    let causeImg = Causes.CAUSES.find((it) => it.name === item.cause).image;

    return (
      (item.screenType === 'SUBMISSION') ? <ProjectSubmissionListItem
        isSearch={isSearch}
        index={index}
        isLike={isLike}
        item={item}
        isCompleted={isCompleted}
        itemImageUrl={itemImageUrl}
        causeImg={causeImg}
        itemName={itemName}
        itemName2={itemName2}
        itemDescription={itemDescription}
        itemStreet={item.street}
        itemCity={itemCity}
        itemState={itemState}
        itemZipcode={itemZipcode}
        goProjectDetails={goProjectDetails}
        dataLength={data.length}
      >
      </ProjectSubmissionListItem>
      :<ProjectListItem
          isSearch={isSearch}
          index={index}
          isLike={isLike}
          item={item}
          isCompleted={isCompleted}
          itemImageUrl={itemImageUrl}
          causeImg={causeImg}
          itemName={itemName}
          itemName2={itemName2}
          itemDescription={itemDescription}
          itemStreet={item.street}
          itemCity={itemCity}
          itemState={itemState}
          itemZipcode={itemZipcode}
          addHours={addHours}
          dataLength={data.length}
          goProjectDetails={goProjectDetails}
          openMap={openMap}
      >
      </ProjectListItem>
    );
  }

  _keyExtractor = (item, index) => {
    return `project-${index}`;
  };

  render() {
    const { data, style } = this.props;
    return (
      <FlatList 
        style={[styles.scrollContainer, style]}
        keyExtractor={this._keyExtractor}
        data={data}
        renderItem={({item, index}) => this.renderRow(item, index)}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = dispatch => {
  return {
    postLikes: (request) => dispatch(SearchActions.postLikes(request.item, request.cb)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(MyProjectsList);

const styles = StyleSheet.create({
  row: {
    position: 'relative',
    backgroundColor: 'white',
    shadowColor: '#CACACA',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 4,
    borderRadius: 3,
    padding: 5,
    height: 149,
  },
  scrollContainer: {
    paddingTop: 2,
    paddingHorizontal: 19,
    flex: 1
  },
  typeWrapper: {
    position: 'absolute',
    top: 80,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  likesImg: {
    width: 35,
    height: 32,
    position: 'absolute',
  },
});
