import React from 'react';
import { connect } from 'react-redux';
import { KeyboardAvoidingView, ScrollView, ImageBackground, StyleSheet, StatusBar, View, Text, TextInput, Dimensions, Image, TouchableOpacity, Alert } from 'react-native';
import {PrimaryButton} from '../../components';
import Utils from '../../utils';
import ApiConstants from '../../constants/ApiConstants';
const imgBkg = require('../../../assets/images/background9.jpg');
const checkedImg = require('../../../assets/icons/checked.png');
const unCheckedImg = require('../../../assets/icons/unchecked.png');
const pinIcon = require('../../../assets/icons/location_icon_blue.png');

class AddProjectHoursModal extends React.Component {

  
  state = {
    checked: this.props.CreateProjectModalState.checked,
    orgTitle: this.props.CreateProjectModalState.orgTitle,
    projectTitle: this.props.CreateProjectModalState.projectTitle,
    contactFirstName: this.props.CreateProjectModalState.contactFirstName,
    contactLastName: this.props.CreateProjectModalState.contactLastName,
    contactEmail: this.props.CreateProjectModalState.contactEmail,
    address: this.props.CreateProjectModalState.address,
    city: this.props.CreateProjectModalState.city,
    state: this.props.CreateProjectModalState.state,
    zipCode: this.props.CreateProjectModalState.zipCode
  };

  toggleCheckBox = () => {
    this.setState({
      checked: !this.state.checked
    });
  }

  onConfirm = () => {
    const { contactFirstName, contactLastName, contactEmail } = this.state;

    if(!contactFirstName) {
      Alert.alert('', "Please enter a contact's first name.");
      return;
    }
    if(!contactLastName) {
      Alert.alert('', "Please enter a contact's last name.");
      return;
    }        
    if(!contactEmail) {
      Alert.alert('', "Please enter a contact's email.");
      return;
    }
    if(!Utils.validateEmail(contactEmail)) {
      Alert.alert('', "Please enter a valid contact's email.");
      return;
    }
    this.props.toggleModalAction({
      isProjectAddHours: false,
      isConfirmModalOpen: true,
      contactFirstName: this.state.contactFirstName,
      contactLastName: this.state.contactLastName,
      contactEmail: this.state.contactEmail,
      checked: this.state.checked,
    });
  }

  hidePanel = () => {
    this.props.toggleModalAction({
      isProjectModalOpen: false,
      isProjectAddHours: false,
      isCauseModalOpen: false,
      isConfirmModalOpen: false,
      description: '',
      startDate: '',
      endDate: '',
      startTime: '',
      endTime: '',
      projectCause: '',
      projectType: '',
      checked: true,
      orgTitle: '',
      projectTitle: '',
      contactFirstName: '',
      contactLastName: '',
      contactEmail: '',
      address: '',
      city: '',
      state: '',
      zipCode: ''
    });
  }

  render() {
    const { checked } = this.state;
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" keyboardVerticalOffset={-400} enabled>
          <StatusBar hidden={!Utils.nativeBaseDoesHeaderCorrectlyAlready()}/>
          <View style={styles.padHeader}></View>
          <View style={styles.header}>
            <TouchableOpacity onPress={this.hidePanel} style={styles.cancelBtn}>
              <Image source={require('../../../assets/icons/left_arrow.png')}/>
            </TouchableOpacity>
            <View><Text style={styles.headerTitle}>Add Volunteer Hours</Text></View>
            <TouchableOpacity onPress={this.hidePanel} style={styles.cancelBtn}>
              <Text style={styles.cancelText}>Cancel</Text>
            </TouchableOpacity>
          </View>
          
          <ImageBackground source={imgBkg} style={{width: '100%', height: Dimensions.get('window').height, flex: 1}} imageStyle={{opacity: 0.5}}>
          <ScrollView>
            <View style={styles.wrapper}>
              <Text style={styles.title}>track your project</Text>
              <Text style={styles.title2}>About the Organization</Text>
              <View style={styles.form}>
                <View style={styles.editIconView}>
                    <Image source={this.props.CreateProjectModalState.projectCause.image} style={styles.typeImg}/>
                </View>
                <View style={styles.inputsView}>

                  <View style={styles.textInfo}>
                    <Text style={styles.textLabel}>organization</Text>
                    <Text style={styles.textValue}>{this.state.orgTitle}</Text>
                  </View>

                  <View style={styles.textInfo}>
                    <Text style={styles.textLabel}>project</Text>
                    <Text style={styles.textValue}>{this.state.projectTitle}</Text>
                  </View>

                  <View style={styles.infoContact}>
                    <Text style={styles.textLabel}>project contact</Text>
                    <TextInput style={styles.textInput} placeholder="Contact's First Name" placeholderTextColor="#606060" onChangeText={(contactFirstName) => this.setState({contactFirstName})}>
                      {this.state.contactFirstName}
                    </TextInput>

                    <TextInput style={styles.textInput} placeholder="Contact's Last Name" placeholderTextColor="#606060" onChangeText={(contactLastName) => this.setState({contactLastName})}>
                      {this.state.contactLastName}
                    </TextInput>
                  </View>

                  <View style={styles.emailContact}>
                    <Text style={styles.textLabel}>contact to confirm hours</Text>
                    <TextInput keyboardType={'email-address'} style={styles.textInput} placeholder="Contact's Email" placeholderTextColor="#606060" onChangeText={(contactEmail) => this.setState({contactEmail})}>
                      {this.state.contactEmail}
                    </TextInput>
                  </View>

                  <View style={styles.textInfo}>
                    <Text style={styles.textLabel}>special instructions</Text>
                    <View style={{flexDirection: 'row'}}>
                      <Image source={pinIcon} style={{marginTop: 10, marginRight: 5}}/>
                      <Text style={styles.textValue}>{this.state.address}, {this.state.city}, {this.state.state}, {this.state.zipCode}</Text>
                    </View>
                  </View>

                </View>
              </View>

              <TouchableOpacity style={{flexDirection: 'row', marginTop: 10}} onPress={this.toggleCheckBox}>
                <View style={[styles.editIconView, {borderWidth: 0}]}>
                  <Image source={checked ? checkedImg : unCheckedImg} style={styles.checkedImg}/>
                </View>
                <View style={styles.checkTextView}>
                  <Text style={styles.checkBoxText}>Yes, I would like to receive a volunteer rating and review.</Text>
                </View>
              </TouchableOpacity>
              <View style={styles.actionBtn}>
                <PrimaryButton
                  onPress={this.onConfirm}
                  title="CONFIRM"
                />
              </View>            
            </View>              
            </ScrollView>
          </ImageBackground>


      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    CreateProjectModalState: state.CreateProjectModalReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleModalAction: (data) => dispatch(dispatch => (dispatch({type: ApiConstants.CREATE_PROJECT_TOGGLE_MODAL, data})))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddProjectHoursModal);

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  padHeader: {
    height: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
    backgroundColor: '#1D75BD'
  },
  wrapper: {
    margin: 20,
    padding: 14,
    backgroundColor: 'white',
    borderRadius: 2,
    shadowColor: '#CACACA',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 1,
    elevation: 4,
    flex: 1
  },
  title: {
    color: '#1D75BD',
    fontSize: 20,
    textTransform: 'uppercase',
    textAlign: 'center'
  },
  title2:{
    fontSize: 16,
    textAlign: 'left',
    marginTop: 30
  },
  actionBtn: {
    justifyContent: 'center',
    alignItems: 'center',  
    marginTop: 19  
  },
  form: {
    flexDirection: 'row',
    marginTop: 30
  },
  editIconView: {
    width: 44,
    height: 44,
    borderRadius: 22,
    borderWidth: 2,
    borderColor: '#CACACA',
    marginRight: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    height: 50, 
    borderBottomColor: 'gray', 
    borderBottomWidth: 1.5,
    width: '100%',
    paddingHorizontal: 13,
    marginTop: 10,
    paddingBottom: 0
  },
  infoContact:{
    height: 150, 
    width: '98%',
    paddingHorizontal: 13,
    marginTop: 5,
    flexDirection: 'column',
    paddingBottom: 0
  },
  emailContact:{
    height: 80, 
    width: '98%',
    paddingHorizontal: 13,
    marginTop: 5,
    flexDirection: 'column',
    paddingBottom: 0
  },
  textInfo: {
    height: 60, 
    width: '98%',
    paddingHorizontal: 13,
    marginTop: 10,
    flexDirection: 'column',
    paddingBottom: 0
  }, 
  textLabel:{
    color:'#8DC63F',
    fontSize: 12,
    textAlign: 'left',
    textTransform: 'uppercase',
  },
  textValue:{
    fontSize: 14,
    textAlign: 'left',
    marginTop: 5,
    color: 'grey'
  }, 
  inputsView: {
    flex: 1,
    marginTop: -5
  },
  header: {
    backgroundColor: '#1D75BD',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  cancelBtn: {
    height: 40,
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelText: {
    fontSize: 14,
    color: 'white'
  },
  headerTitle: {
    color: 'white',
    fontSize: 16
  },
  checkBoxText: {
    color: 'black',
    fontSize: 12,
    lineHeight: 15
  },
  checkedImg: {
    width: 24,
    height: 24
  },
  checkTextView: {
    flex: 1,
    paddingTop: 10
  },
  inputIOS: {
    fontSize: 16,
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
},
typeImg: {
  resizeMode: 'contain',
  width: '80%',
  height: '80%',
},
inputWrapper: {
  flex: 1,
  marginTop: 10,
  paddingHorizontal: 15,
  paddingTop: 20,
  borderBottomWidth: 2,
  borderBottomColor: 'grey',
  height: 50,
  color: 'gray'
}, 
});
