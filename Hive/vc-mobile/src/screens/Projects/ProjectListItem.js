import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity,
    Dimensions
  } from 'react-native';
import { GothamNotRoundedMediumText, GothamNotRoundedBookText} from '../../components';
import StarRating from 'react-native-star-rating';
import ImageProgress from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import LinearGradient from 'react-native-linear-gradient';
const arrowBlueIcon = require('../../../assets/icons/arrow_blue.png');
const projBottomBg = require('../../../assets/images/proj_bottom_bg.png');
const pinIcon = require('../../../assets/icons/location_icon_blue.png');

export default class ProjectListItem extends React.Component{

    render(){
        return (
            <View style={[styles.rowWrapper, (this.props.isSearch === false) && (this.props.index > 0) && {marginTop: 26}]}>
            {
              (this.props.isSearch === false) && 
              <View style={this.props.isLike === true && {opacity: 0}}>  
                <GothamNotRoundedMediumText style={[styles.statusText, {color: '#8DC63F'}]}>REQUESTED</GothamNotRoundedMediumText>
              </View>
            }
    
            {
              this.props.isSearch === false &&
              <View style={this.props.item.screenType === 'PROJECTSUBSCRIPTION' ? styles.firstDot : styles.dot}></View>
            }
            
            <TouchableOpacity style={[
              styles.row, 
              (this.props.index === this.props.dataLength-1) && styles.lastRow,
              {color:'#9b9c9d'}
            ]} 
            onPress={()=> this.props.goProjectDetails(this.props.item)}
            >
              <View style={styles.largeRow}>
                <View>
                  <View style={styles.avatar}>
                    {
                    this.props.item.nteeCodeImgURL && 
                      <ImageProgress source={{uri: this.props.item.nteeCodeImgURL}} indicator={Progress.Circle} style={styles.avatarImg} />                   
                    }
                  </View>
                  <Image source={projBottomBg} style={styles.projBottom}/>
                  <View style={styles.typeWrapper}>
                    <View style={styles.type}>
                          <Image source={this.props.causeImg} style={styles.typeImg} />
                    </View>
                    <GothamNotRoundedMediumText style={styles.causeName}>{this.props.item.cause}</GothamNotRoundedMediumText>
                  </View>
                </View>
                <View style={styles.column}>
                  <GothamNotRoundedMediumText style={styles.rowTitle}>{this.props.itemName}</GothamNotRoundedMediumText>
                  {
                    (typeof this.props.item.rating === 'undefined' || this.props.item.rating === null) ?
                    <View style={styles.ratingWrap}>
                      <GothamNotRoundedMediumText style={styles.notYetRated}>NOT YET RATED</GothamNotRoundedMediumText>
                    </View>
                    :
                    <View style={styles.ratingWrap}>
                      <StarRating
                        disabled={false}
                        maxStars={5}
                        rating={this.props.item.rating}
                        starSize={14}
                        starStyle={styles.star}
                        containerStyle={styles.ratingContainer}              
                        fullStarColor={this.props.isCompleted ? '#9b9c9d' : '#f5a643'}
                        disabled
                      />
                      <GothamNotRoundedBookText style={styles.ratingText}>{this.props.item.rating.toFixed(1)}</GothamNotRoundedBookText>
                    </View>              
                  }
    
                  <View>
                  { this.props.itemName2.length > 0 &&
                      <Text>
                        <GothamNotRoundedMediumText style={[styles.rowTitle2]}>{this.props.itemName2}</GothamNotRoundedMediumText>
                      </Text>
                  }
                  </View>  
                  <Text numberOfLines={1}></Text>
                  {
                    this.props.itemCity && this.props.itemState &&
                    <TouchableOpacity onPress={this.props.openMap}>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 2}}> 
                          <Image source={pinIcon}/>
                          <View style={{flexDirection: 'column'}}>
                              {
                                this.props.itemStreet &&                        
                                  <Text style={[styles.smallText, styles.locationText]} ellipsizeMode='tail' numberOfLines={2}>
                                      {this.props.itemStreet}
                                  </Text>
                              }
                              <Text style={[styles.smallText, styles.locationText]} ellipsizeMode='tail' numberOfLines={1}>
                                {this.props.itemCity}, {this.props.itemState}
                              </Text>
                              <Text style={[styles.smallText, styles.locationText]} ellipsizeMode='tail' numberOfLines={1}>
                                  {this.props.itemZipcode}
                              </Text>
                          </View>
                        </View>
                    </TouchableOpacity>  
                  }              
                  <TouchableOpacity style={styles.comingBtn} onPress={()=>this.props.addHours(this.props.item)}>
                    <GothamNotRoundedMediumText style={styles.comingBtnText}>ADD HOURS</GothamNotRoundedMediumText>
                    <Image style={styles.arrowIcon} source={arrowBlueIcon}/>
                  </TouchableOpacity>                 
                </View>
              </View>
              {
                this.props.isCompleted &&
                <View style={styles.overlayView}></View>
              }
            </TouchableOpacity>
            {
              (this.props.isSearch === false || this.props.isLike === true) &&
              <>
              {
                (this.props.item.screenType === 'PROJECTSUBSCRIPTION') ?
                <LinearGradient
                  colors={['#8DC63F', '#1D75BD']}
                  style={styles.firstLine}
                  start={{x: 0, y: 0}}
                  end={{x: 0, y: 1}}
                />
                :
                <View style={styles.vLine}></View>
              }
              </>
            }        
          </View>
        );
    }
  }

  const styles = StyleSheet.create({
    rowWrapper: {
      marginTop: 1,
      marginBottom: 10,
      marginHorizontal: 0,
    },
    row: {
      position: 'relative',
      backgroundColor: 'white',
      shadowColor: '#CACACA',
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 1,
      shadowRadius: 2,
      elevation: 4,
      borderRadius: 3,
      padding: 5,
      height: 155,
    },
    lastRow: {
      marginBottom: 15
    },
    avatar: {
      width: 109,
      height: 110,
      borderRadius: 3,
      overflow: 'hidden'
    },
    avatarImg: {
      width: '100%',
      height: '100%',
      resizeMode: 'cover'
    },
    projBottom: {
      position: 'absolute',
      bottom: 0,
      width: 109,
      height: 48
    },
    largeRow: {
      flexDirection: 'row',
      height: 142,
      position: 'relative',
    },
    column: {
      flex: 1, 
      paddingHorizontal: 10,
      paddingBottom: 7
    },
    locationText: {
      fontFamily: 'Gotham-Book',
      marginLeft: 10,
      marginTop: 1,
      fontSize: 10,
      lineHeight: 11
    },
    ratingWrap: {
      flexDirection: 'row',
      marginTop: 7,
      marginBottom: 7,
      marginLeft: 0,
      height: 14
    },  
    ratingContainer: {
      justifyContent: 'flex-start',
      height: 14,
      paddingBottom: 0,
      overflow: 'hidden',
      marginTop: -5
    },
    star: {
      marginRight: 2
    },
    ratingText: {
      color: '#777777',
      fontSize: 11,
      marginLeft: 7,
      marginTop: -8
    },
    rowTitle: {
      color: '#8DC63F',
      fontSize: 14,
      fontWeight: 'bold'
    },
    rowTitle2: {
      color: '#000000',
      fontSize: 11,
      fontWeight: 'bold',
      marginTop: -5
    },
    smallText: {
      color: '#777777',
      fontSize: 11
    },
    dateTime: {
      marginTop: 0,
      marginLeft: 65
    },
    dtRow: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginTop: 0
    },
    dateText: {
      flex: 1,
      marginTop: 0
    },
    comingSoon: {
      marginTop: 3,
      marginLeft: 20
    },
    comingBtn: {
      marginTop: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    comingBtnText: {
      fontSize: 11,
      color: '#24B6EC'
    },
    arrowIcon: {
      marginLeft: 5,
      marginTop: -3
    },
    typeWrapper: {
      position: 'absolute',
      top: 80,
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    type: {
      width: 35,
      height: 35,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 17.5,
      borderWidth: 1,
      borderColor: '#CACACA'
    },
    typeImg: {
      resizeMode: 'contain',
      width: '80%',
      height: '80%',
    },
    overlayView: {
      position: 'absolute', 
      width: Dimensions.get('window').width - 38, 
      height: 149, 
      backgroundColor: '#FAFDFF30', 
      borderRadius: 2
    },
    statusText: {
      fontSize: 10,
      textAlign: 'right',
      marginBottom: 7
    },
    dot: {
      position: 'absolute',
      width: 5,
      height: 5,
      borderRadius: 2.5,
      backgroundColor: '#CACACA',
      marginLeft: -12,
      marginTop: 5
    },
    firstDot: {
      position: 'absolute',
      width: 11,
      height: 11,
      borderRadius: 5.5,
      borderWidth: 3,
      backgroundColor: '#8DC63F',
      borderColor: '#CACACA50',
      marginLeft: -15,
      marginTop: 0
    },
    vLine: {
      width: 1,
      height: 192,
      position: 'absolute',
      backgroundColor: '#CACACA',
      marginLeft: -10,
      marginTop: 15
    },
    firstLine: {
      height: 192,
      position: 'absolute',
      width: 2,
      marginLeft: -10.5,
      marginTop: 15,
      backgroundColor: '#8DC63F',
    },
    causeName: {
      fontSize: 10,
      textTransform: 'uppercase',
      color: '#303030',
      marginTop: 6
    },
    notYetRated: {
      color: '#777777',
      fontSize: 10
    }
  });
  
