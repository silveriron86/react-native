import React from 'react';
import { KeyboardAvoidingView, ScrollView, Alert, ImageBackground, StyleSheet, StatusBar, View, Text, Dimensions, Image, TouchableOpacity} from 'react-native';
import {PrimaryButton, PickerSelect} from '../../components';
import { connect } from 'react-redux';
import ApiConstants from '../../constants/ApiConstants';
import Utils from '../../utils';
import Causes from '../../constants/Causes';
import ProjectsTypes from  '../../constants/ProjectsTypes';
const imgBkg = require('../../../assets/images/background9.jpg');

const types = ProjectsTypes;

class AddProjectCauseModal extends React.Component {
  state = {
    projectCause: this.props.CreateProjectModalState.projectCause,
    projectType: this.props.CreateProjectModalState.projectType,
    projectTypeKey: this.props.CreateProjectModalState.projectTypeKey,
  };

  onSelectCause = (cause) => {
    this.setState({projectCause : cause});
  }

  isSelected = (cause) => {
    if(this.state.projectCause == cause)
      return true;
    return false;
  }

  onConfirm = () => {
    const { projectCause, projectType } = this.state;
    if(!projectCause) {
      Alert.alert('', 'Please enter a project cause.');
      return;
    }
    if(!projectType) {
      Alert.alert('', 'Please enter a project type.');
      return;
    }

    this.props.toggleModalAction({
      isProjectModalOpen: false,
      isCauseModalOpen: false,
      isConfirmModalOpen: true,
      projectCause: this.state.projectCause,
      projectType: this.state.projectType,
      projectTypeKey: this.state.projectTypeKey
    });
  }

  hidePanel = () => {
    this.props.toggleModalAction({
      isProjectModalOpen: true,
      isCauseModalOpen: false,
      isConfirmModalOpen: false
    });
  }

  handleSelect = (typeKey) => {
      this.setState({
          projectType: types.find(t => t.value === typeKey).label,
          projectTypeKey: typeKey
        })
  }

  cancel = () => {
    this.props.toggleModalAction({
      isProjectModalOpen: false,
      isCauseModalOpen: false,
      isConfirmModalOpen: false,
      isProjectAddHours: false,
      sfProjectId: '',
      sfOrgId: '',
      description: '',
      startDate: '',
      endDate: '',
      startTime: '',
      endTime: '',
      projectCause: '',
      projectType: '',
      projectTypeKey: '',
      checked: true,
      orgTitle: '',
      projectTitle: '',
      contactFirstName: '',
      contactLastName: '',
      contactEmail: '',
      address: '',
      city: '',
      state: '',
      zipCode: ''
    });
  }

  render() {
    console.debug(this.state.projectCause);
    let rows = [];
    let cols = [];
    Causes.CAUSES.forEach((cause, index) => {
      if(index > 0 && index % 4 == 0) {
        rows.push(
          <View style={styles.causesWrapper} key={`cause-row-${index}`}>
            {cols}
          </View>
        );
        cols = [];
      }

      let isSelected = this.isSelected(cause);
      cols.push(
        !this.props.CreateProjectModalState.isOrgAddHours 
        ? 
        <TouchableOpacity key={`cause-${index}`} style={styles.causeItem} onPress={()=> this.onSelectCause(cause)}>
          <View style={[cause.name && styles.imgWrapper, isSelected && styles.selectedImgWrapper]}>
            <Image source={cause.image} style={isSelected && styles.selectedImg}/>
          </View>
          <Text style={styles.causeText}>{cause.name}</Text>
        </TouchableOpacity> 
        : 
        <View key={`cause-${index}`} style={styles.causeItem}>
            <View style={[cause.name && styles.imgWrapper, isSelected && styles.selectedImgWrapper]}>
            <Image source={cause.image} style={isSelected && styles.selectedImg}/>
          </View>
          <Text style={styles.causeText}>{cause.name}</Text>
        </View>
      )      
    });

    rows.push(
      <View style={styles.causesWrapper} key={`cause-row-last`}>
        {cols}
      </View>
    );   

    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" keyboardVerticalOffset={-400} enabled>
          <StatusBar hidden={!Utils.nativeBaseDoesHeaderCorrectlyAlready()}/>
          <View style={styles.padHeader}></View>
          <View style={styles.header}>
            <TouchableOpacity onPress={this.hidePanel} style={styles.cancelBtn}>
              <Image source={require('../../../assets/icons/left_arrow.png')}/>
            </TouchableOpacity>
            <View><Text style={styles.headerTitle}>Add Volunteer Hours</Text></View>
            <TouchableOpacity onPress={this.cancel} style={styles.cancelBtn}>
              <Text style={styles.cancelText}>Cancel</Text>
            </TouchableOpacity>
          </View>
          
          <ImageBackground source={imgBkg} style={{width: '100%', height: Dimensions.get('window').height + 50, flex: 1}} imageStyle={{opacity: 0.5}}>
          <ScrollView>
            <View style={styles.wrapper}>
              <Text style={styles.title}>PROJECT DETAILS</Text>
              <Text style={styles.informationText}>What cause did you support?</Text>
              <View>
                  <View style={styles.modalWrapper}>
                    {rows}
                  </View>
              </View>
                <Text style={styles.informationText}>What type of volunteer project is this?</Text>
                <PickerSelect placeholder='Choose project type'
                        items={types}
                        onChange={this.handleSelect}
                        value={this.state.projectTypeKey}>
                </PickerSelect>
              <View style={styles.actionBtn}>
                <PrimaryButton
                  onPress={this.onConfirm}
                  title="CONFIRM"
                />
              </View>            
            </View>              
            </ScrollView>
          </ImageBackground>
          
      </KeyboardAvoidingView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    CreateProjectModalState: state.CreateProjectModalReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleModalAction: (data) => dispatch(dispatch => (dispatch({type: ApiConstants.CREATE_PROJECT_TOGGLE_MODAL, data})))
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(AddProjectCauseModal);

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  padHeader: {
    height: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
    backgroundColor: '#1D75BD'
  },
  wrapper: {
    margin: 20,
    padding: 14,
    backgroundColor: 'white',
    borderRadius: 2,
    shadowColor: '#CACACA',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 1,
    elevation: 4,
    flex: 1
  },
  title: {
    color: '#1D75BD',
    fontSize: 20,
    textTransform: 'uppercase',
    textAlign: 'center'
  },
  actionBtn: {
    justifyContent: 'center',
    alignItems: 'center',  
    marginTop: 19  
  },
  form: {
    flexDirection: 'row',
    marginTop: 14
  },

  inputWrapper: {
    width: 300,
    paddingHorizontal: 20,
    marginBottom: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#606060',
    height: 80,
    color: '#606060'
  }, 

  inputIOS: {
    height: 50,
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
},
  inputsView: {
    flex: 1,
    marginTop: -15
  },
  header: {
    backgroundColor: '#1D75BD',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  cancelBtn: {
    height: 40,
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelText: {
    fontSize: 14,
    color: 'white'
  },
  headerTitle: {
    color: 'white',
    fontSize: 16
  },
  causesWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }, 
  causeItem: {
    width: 75,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },  
  imgWrapper: {
    width: 50,
    height: 50,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    borderWidth: 2,
    borderColor: '#CACACA'
  },
  causeText: {
    color: '#333333',
    fontSize: 8,
    textAlign: 'center',
    marginTop: 7,
    textTransform: 'uppercase'
  },
  selectedImgWrapper: {
    backgroundColor: '#609ed1'
  },
  selectedImg: {
    tintColor: 'white'
  },
  informationText:{
    marginTop: 10,
    marginBottom: 10,
    fontSize: 14
  }

});
