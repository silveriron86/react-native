import React from 'react';
import { connect } from 'react-redux';
import { KeyboardAvoidingView, ScrollView, Alert, ImageBackground, StyleSheet, StatusBar, View, Text, TextInput, Dimensions, Image, TouchableOpacity } from 'react-native';
import {PrimaryButton} from '../../components';
import Utils from '../../utils';
import ApiConstants from '../../constants/ApiConstants';
import States from '../../constants/States';
import Picker from 'react-native-picker-select';
const imgBkg = require('../../../assets/images/background9.jpg');
const editIcon = require('../../../assets/icons/edit_icon.png');
const checkedImg = require('../../../assets/icons/checked.png')
const unCheckedImg = require('../../../assets/icons/unchecked.png')

class AddProjectModal extends React.Component {
  state = {
    checked: this.props.CreateProjectModalState.checked,
    orgTitle: this.props.CreateProjectModalState.orgTitle,
    projectTitle: this.props.CreateProjectModalState.projectTitle,
    contactFirstName: this.props.CreateProjectModalState.contactFirstName,
    contactLastName: this.props.CreateProjectModalState.contactLastName,
    contactEmail: this.props.CreateProjectModalState.contactEmail,
    address: this.props.CreateProjectModalState.address,
    city: this.props.CreateProjectModalState.city,
    state: this.props.CreateProjectModalState.state,
    zipCode: this.props.CreateProjectModalState.zipCode
  };

  toggleCheckBox = () => {
    this.setState({
      checked: !this.state.checked
    });
  }

  onConfirm = () => {
    const { orgTitle, projectTitle, contactFirstName, contactLastName, contactEmail, address, city, state, zipCode } = this.state;
    if(!orgTitle) {
      Alert.alert('', 'Please enter an organization title.');
      return;
    }
    if(!projectTitle) {
      Alert.alert('', 'Please enter a project title.');
      return;
    }
    if(!contactFirstName) {
      Alert.alert('', "Please enter a contact's first name.");
      return;
    }
    if(!contactLastName) {
      Alert.alert('', "Please enter a contact's last name.");
      return;
    }        
    if(!contactEmail) {
      Alert.alert('', "Please enter a contact's email.");
      return;
    }
    if(!Utils.validateEmail(contactEmail)) {
      Alert.alert('', "Please enter a valid contact's email.");
      return;
    }
    if(!address) {
        Alert.alert('', 'Please enter an address.');
        return;
    }
    if(!city) {
      Alert.alert('', 'Please enter a city.');
      return;
    }
    if(!state) {
      Alert.alert('', 'Please enter a state.');
      return;
    }
    if(!zipCode) {
      Alert.alert('', 'Please enter a zip code.');
      return;
    }

    this.props.toggleModalAction({
      isProjectModalOpen: false,
      isCauseModalOpen: !this.props.CreateProjectModalState.isProjectAddHours,
      isConfirmModalOpen: this.props.CreateProjectModalState.isProjectAddHours,
      checked: this.state.checked,
      orgTitle: this.state.orgTitle,
      projectTitle: this.state.projectTitle,
      contactFirstName: this.state.contactFirstName,
      contactLastName: this.state.contactLastName,
      contactEmail: this.state.contactEmail,
      address: this.state.address,
      city: this.state.city,
      state: this.state.state,
      zipCode: this.state.zipCode
    });
  }

  hidePanel = () => {
    this.props.toggleModalAction({
      isProjectModalOpen: false,
      isCauseModalOpen: false,
      isConfirmModalOpen: false,
      description: '',
      startDate: '',
      endDate: '',
      startTime: '',
      endTime: '',
      projectCause: '',
      projectType: '',
      checked: true,
      orgTitle: '',
      projectTitle: '',
      contactFirstName: '',
      contactLastName: '',
      contactEmail: '',
      address: '',
      city: '',
      state: '',
      zipCode: ''
    });
  }

  render() {
    const { checked } = this.state;

    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" keyboardVerticalOffset={-400} enabled>
          <StatusBar hidden={!Utils.nativeBaseDoesHeaderCorrectlyAlready()}/>
          <View style={styles.padHeader}></View>
          <View style={styles.header}>
            <TouchableOpacity onPress={this.hidePanel} style={styles.cancelBtn}>
              <Image source={require('../../../assets/icons/left_arrow.png')}/>
            </TouchableOpacity>
            <View><Text style={styles.headerTitle}>Add Volunteer Hours</Text></View>
            <TouchableOpacity onPress={this.hidePanel} style={styles.cancelBtn}>
              <Text style={styles.cancelText}>Cancel</Text>
            </TouchableOpacity>
          </View>
          
          <ImageBackground source={imgBkg} style={{width: '100%', height: Dimensions.get('window').height, flex: 1}} imageStyle={{opacity: 0.5}}>
          <ScrollView>
            <View style={styles.wrapper}>
              <Text style={styles.title}>ADD your project</Text>
              <View style={styles.form}>
                <View style={styles.editIconView}>
                    <Image source={editIcon}/>
                </View>
                <View style={styles.inputsView}>
                  <TextInput style={styles.textInput} placeholder="Organization Title" placeholderTextColor="#606060" onChangeText={(orgTitle) => this.setState({orgTitle})} editable={!this.props.CreateProjectModalState.isOrgAddHours} selectTextOnFocus={!this.props.CreateProjectModalState.isOrgAddHours}>
                    {this.state.orgTitle}
                  </TextInput>
                  <TextInput style={styles.textInput} placeholder="Project Title" placeholderTextColor="#606060" onChangeText={(projectTitle) => this.setState({projectTitle})}>
                    {this.state.projectTitle}
                  </TextInput>
                  <TextInput style={styles.textInput} placeholder="Contact's First Name" placeholderTextColor="#606060" onChangeText={(contactFirstName) => this.setState({contactFirstName})}>
                    {this.state.contactFirstName}
                  </TextInput>
                  <TextInput style={styles.textInput} placeholder="Contact's Last Name" placeholderTextColor="#606060" onChangeText={(contactLastName) => this.setState({contactLastName})}>
                    {this.state.contactLastName}
                  </TextInput>
                  <TextInput keyboardType={'email-address'} style={styles.textInput} placeholder="Contact's Email" placeholderTextColor="#606060" onChangeText={(contactEmail) => this.setState({contactEmail})}>
                    {this.state.contactEmail}
                  </TextInput>
                  <TextInput style={styles.textInput} placeholder="Address" placeholderTextColor="#606060" onChangeText={(address) => this.setState({address})}>
                    {this.state.address}
                  </TextInput>
                  <TextInput style={styles.textInput} placeholder="City" placeholderTextColor="#606060" onChangeText={(city) => this.setState({city})}>
                    {this.state.city}
                  </TextInput>
                  <View style={{flexDirection: 'row'}}>
                    <View style={styles.inputWrapper}>
                      <Picker placeholder={{label: 'Choose State', value: null}}
                        items={States.STATES}
                        onValueChange={(value) => {
                            this.setState({
                                state: value,
                              });
                            }}
                        style={styles.inputIOS}
                        value={this.state.state}
                        placeholderTextColor='#606060'>
                      </Picker>
                    </View>
                    <View style={{width: 21}}></View>
                    <View style={{flex: 1}}>
                      <TextInput keyboardType={'number-pad'} style={styles.textInput} placeholder="Zip Code" placeholderTextColor="#606060" onChangeText={(zipCode) => this.setState({zipCode})}>
                        {this.state.zipCode}
                      </TextInput>
                    </View>
                  </View>
                </View>
              </View>

              <TouchableOpacity style={{flexDirection: 'row', marginTop: 10}} onPress={this.toggleCheckBox}>
                <View style={[styles.editIconView, {borderWidth: 0}]}>
                  <Image source={checked ? checkedImg : unCheckedImg} style={styles.checkedImg}/>
                </View>
                <View style={styles.checkTextView}>
                  <Text style={styles.checkBoxText}>Yes, I would like to receive a volunteer rating and review.</Text>
                </View>
              </TouchableOpacity>
              <View style={styles.actionBtn}>
                <PrimaryButton
                  onPress={this.onConfirm}
                  title="CONFIRM"
                />
              </View>            
            </View>              
            </ScrollView>
          </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    CreateProjectModalState: state.CreateProjectModalReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleModalAction: (data) => dispatch(dispatch => (dispatch({type: ApiConstants.CREATE_PROJECT_TOGGLE_MODAL, data})))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddProjectModal);

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  padHeader: {
    height: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
    backgroundColor: '#1D75BD'
  },
  wrapper: {
    margin: 20,
    padding: 14,
    backgroundColor: 'white',
    borderRadius: 2,
    shadowColor: '#CACACA',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 1,
    elevation: 4,
    flex: 1
  },
  title: {
    color: '#1D75BD',
    fontSize: 20,
    textTransform: 'uppercase',
    textAlign: 'center'
  },
  actionBtn: {
    justifyContent: 'center',
    alignItems: 'center',  
    marginTop: 19  
  },
  form: {
    flexDirection: 'row',
    marginTop: 14
  },
  editIconView: {
    width: 44,
    height: 44,
    borderRadius: 22,
    borderWidth: 2,
    borderColor: '#CACACA',
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    height: 50, 
    borderBottomColor: 'gray', 
    borderBottomWidth: 1.5,
    width: '100%',
    paddingHorizontal: 13,
    marginTop: 10,
    paddingBottom: 0
  },  
  inputsView: {
    flex: 1,
    marginTop: -15
  },
  header: {
    backgroundColor: '#1D75BD',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  cancelBtn: {
    height: 40,
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelText: {
    fontSize: 14,
    color: 'white'
  },
  headerTitle: {
    color: 'white',
    fontSize: 16
  },
  checkBoxText: {
    color: 'black',
    fontSize: 12,
    lineHeight: 15
  },
  checkedImg: {
    width: 24,
    height: 24
  },
  checkTextView: {
    flex: 1,
    paddingTop: 10
  },
  inputIOS: {
    fontSize: 16,
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
},
inputWrapper: {
  flex: 1,
  marginTop: 10,
  paddingHorizontal: 15,
  paddingTop: 20,
  borderBottomWidth: 2,
  borderBottomColor: 'grey',
  height: 50,
  color: 'gray'
}, 
});
