import React from 'react';
import { Image, StyleSheet, Dimensions, View, StatusBar, Text, TouchableOpacity, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { ProjectsActions, LikesActions, SearchActions} from '../../actions';
import HomeNavBar from '../Home/HomeNavBar';
import { GothamMediumText, GothamNotRoundedMediumText, CornerLabel, LoadingOverlay } from '../../components';
import Colors from '../../constants/Colors';
import MyProjectsList from './MyProjectsList';
import AddProjectModal from './AddProjectModal';
import AddProjectHoursModal from './AddProjectHoursModal';
import AddProjectCauseModal from './AddProjectCauseModal';
import ConfirmProjectModal from './ConfirmProjectModal';
import ModalWrapper from 'react-native-modal-wrapper';
import ApiConstants from '../../constants/ApiConstants';
const plusIcon = require('../../../assets/icons/plus.png');
import Config from 'react-native-config';
import MapModal from "../Search/MapModal";
import Causes from '../../constants/Causes';
import ProjectsTypes from  '../../constants/ProjectsTypes';
import NavigationVars from '../../navigation/NavigationVars';
import { withNavigationFocus } from "react-navigation";

class ProjectsScreen extends React.Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
        token: '',
        userId: '',
        sfContactId: '',
        contactFirstName: '',
        contactLastName: '',
        contactEmail: '',
    };
  }

  getAuthData = async() => {
    try{
      let authUserJSON = JSON.parse(await AsyncStorage.getItem('oio_auth'));
      let  userJSON = authUserJSON.user;
      this.setState({
        token: authUserJSON.token,
        userId: userJSON.id,
        sfContactId: userJSON.sfContactId,
        isModalMapOpen: false
      },);
    }catch(error){
      console.debug(error);
    }
  }

  closeMap = () => {
    this.setState({isModalMapOpen: false});
  };

  openMap = () => {
    this.setState({isModalMapOpen: true});
  };

  handleAddProject = () => {
    this.props.toggleModalAction({
      isProjectModalOpen: true,
      isCauseModalOpen: false,
      isConfirmModalOpen: false,
      isProjectAddHours: false,
      isOrgAddHours: false,
      isProjectAddHours: false,
      addHourStatus: false,
    });
  }

  handleLike = (item) => {
    this.props.postProjectLikes({
      id: item.id,
      cb: () => {
        this.props.postLikes({
          item: item,
          toggle: true,
          cb: () => {}
        });        
      }
    });
  }

  addHours = async (item) => {
      this.props.toggleModalAction({
        isProjectModalOpen: item.sfProjectId ? false : true,
        isCauseModalOpen: false,  
        isConfirmModalOpen: false,
        checked: true,
        isProjectAddHours: item.sfProjectId ? true : false,
        isOrgAddHours: (item.sfOrgId && !item.sfProjectId) ? true : false,
        addHourStatus: item.sfProjectId ? true : false,
        sfProjectId: item.sfProjectId ? item.sfProjectId : '',
        sfOrgId: item.sfOrgId ? item.sfOrgId : '',
        orgTitle: item.orgName,
        projectTitle: item.name ? item.name : '',
        address: item.street,
        city: item.city,
        state: item.state,
        zipCode: item.zipcode,
        projectCause: item.cause ? Causes.CAUSES.find(c => c.name === item.cause): '',
        projectType: item.type ? item.type: '',
        projectTypeKey: item.type ? ProjectsTypes.find(t => t.label === item.type).value : '',
        contactFirstName: item.contactFirstName,
        contactLastName: item.contactLastName,
        contactEmail: item.contactEmail,
      });
  }

  goProjectDetails = (item) => {
    this.props.navigation.navigate('MyProjectDetails', {
      data: item
    });
  }

  handleReviewNow = () => {

  }

  getProjectsOrgs = async() => {
    try{
      let request = {
        token: this.state.token,
        sfContactId: this.state.sfContactId,
        userId: this.state.userId
      };
      this.props.showLoaderOverlay();
      this.props.getProjectsOrgsList({request: request, callback : (res)=>{
        if(res){
          this.props.hideLoaderOverlay();
        }
      }});

    } catch(error){
      console.debug(error);
      await this.props.hideLoaderOverlay();
    }
  }
  
  componentDidMount () {
    this.getAuthData().then(()=>this.getProjectsOrgs())
  }
  componentDidUpdate(prevProps) {
    // https://reactnavigation.org/docs/en/function-after-focusing-screen.html
    if (prevProps.isFocused !== this.props.isFocused) {
      if (this.props.isFocused) {
        this.getAuthData().then(()=>this.getProjectsOrgs());
      }
    }
  }

  render() {
    const { navigation, projectsOrgsList } = this.props;
    const modalState = this.props.createProjectModalState;
    const { isModalMapOpen } = this.state;
    return (
      <View style={styles.container}>
        { Config.ENVIRONMENT === 'DEV' && 
          <View style={styles.cornerLabelView}>
            <CornerLabel>DEV</CornerLabel> 
          </View>
        }
        <LoadingOverlay loading={this.props.displayLoader}/>

        <StatusBar backgroundColor="blue" barStyle="light-content" />
        <HomeNavBar
          onLeftButtonPress={() => navigation.toggleDrawer()}
          onRightButtonPress={() => navigation.navigate('Profile')}
        />
        <GothamMediumText
          style={styles.titleText}>
          CURRENT <Text style={{color: '#8DC63F'}}>PROJECTS</Text>
        </GothamMediumText>    
        <Text style={styles.titleText}>
        
        </Text>  
        {
        <View>
          <View>
            <TouchableOpacity style={styles.addBtn} onPress={this.handleAddProject}>
              <GothamNotRoundedMediumText style={styles.addBtnText}>Add Hours</GothamNotRoundedMediumText>
              <Image source={plusIcon} style={styles.plusIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.smallTextAddHoursView}>
            <GothamNotRoundedMediumText style={styles.smallTextAddHours}>
              (for a different project)
            </GothamNotRoundedMediumText>      
          </View>
        </View>
        }

        
        {
          projectsOrgsList &&
          <MyProjectsList
            data={projectsOrgsList}
            isSearch={false}
            isProject={true}
            addHours={this.addHours}
            handleReviewNow={this.handleReviewNow}
            handleLike={this.handleLike}
            goProjectDetails={this.goProjectDetails}
            openMap={this.openMap}
          />
        }
  
        {modalState.isProjectModalOpen && <ModalWrapper
          containerStyle={{flexDirection: 'row'}}
          shouldAnimateOnRequestClose={true}
          position="right"
          visible={true}>
          <AddProjectModal/>
        </ModalWrapper>}

        {modalState.isProjectAddHours && <ModalWrapper
          containerStyle={{flexDirection: 'row'}}
          shouldAnimateOnRequestClose={true}
          position="right"
          visible={true}>
          <AddProjectHoursModal/>
        </ModalWrapper>}

        {modalState.isCauseModalOpen && <ModalWrapper
          containerStyle={{flexDirection: 'row'}}
          shouldAnimateOnRequestClose={true}
          position="right"
          visible={true}>
          <AddProjectCauseModal/>
        </ModalWrapper>}

        {modalState.isConfirmModalOpen && <ModalWrapper
          containerStyle={{flexDirection: 'row'}}
          shouldAnimateOnRequestClose={true}
          position="right"
          visible={true}>
          <ConfirmProjectModal 
            hideLoaderOverlay={this.props.hideLoaderOverlay} 
            showLoaderOverlay={this.props.showLoaderOverlay} 
            navigation={this.props.navigation}/>
        </ModalWrapper>}

        {isModalMapOpen && <ModalWrapper
            style={styles.CalendarModalWrapper}
            onRequestClose={() => this.setState({isModalMapOpen: false})}
            visible={true}>
            <TouchableOpacity onPress={this.closeMap} style={styles.closeBtn}>
                <Image source={require('../../../assets/icons/close.png')} />
            </TouchableOpacity>
            <MapModal handleConfirm={this.closeMap}/>
        </ModalWrapper>}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    createProjectModalState: state.CreateProjectModalReducer,
    projectsOrgsList: state.ProjectsReducer.projectsOrgsList,   
    projectsOrgsListError: state.ProjectsReducer.projectsOrgsListError,
    displayLoader: state.ProjectsReducer.showLoaderOverlay,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getProjectsOrgsList: (request) => dispatch(ProjectsActions.getProjectsOrgByUserId(request.request, request.callback)),
    postLikes: (request) => dispatch(SearchActions.postLikes(request.item, request.toggle, request.cb)),
    toggleModalAction: (data) => dispatch(dispatch => (dispatch({type: ApiConstants.CREATE_PROJECT_TOGGLE_MODAL, data}))),
    showLoaderOverlay: () => dispatch(ProjectsActions.showLoaderOverlay()),
    hideLoaderOverlay: () => dispatch(ProjectsActions.hideLoaderOverlay()),
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(ProjectsScreen));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  titleText: {
    marginTop: 13,
    textAlign: 'center',
    fontSize: 22,
    lineHeight: 26,
    color: Colors.tintColor,
  },
  addBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 8,
    marginBottom: 15
  },
  addBtnText: {
    color: '#24B6EC',
    fontSize: 17
  },
  plusIcon: {
    marginLeft: 10,
    marginTop: -3
  },
  cornerLabelView: {
    position: 'absolute',
    marginTop: -100,
    flexDirection: 'row',
    width: '100%',
  },
  CalendarModalWrapper: {
    backgroundColor: 'transparent',
    width: Dimensions.get('window').width - 40,
    borderRadius: 8,
    overflow: 'hidden',
    justifyContent: 'flex-start'
  },
  smallTextAddHoursView: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: -10,
  },
  smallTextAddHours: {
    color: '#999999',
    fontSize: 11,
  },
});
