import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    Dimensions
  } from 'react-native';
import { GothamNotRoundedMediumText, GothamNotRoundedBookText} from '../../components';
import ImageProgress from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import { Grayscale } from 'react-native-color-matrix-image-filters';
const projBottomBg = require('../../../assets/images/proj_bottom_bg.png');
const checkImg = require('../../../assets/images/checkmark_gray-02.png');

import Utils from '../../utils';

  export default class ProjectSubmissionListItem extends React.Component{

    render (){
        const startDateTime = Utils.newDateObject(this.props.item.startTime, this.props.item.startDate)
        const endDateTime = Utils.newDateObject(this.props.item.endTime, this.props.item.endDate)
        const shiftDurationHours = Utils.hoursBetweenDates(startDateTime, endDateTime)

        return (
            <View style={[styles.rowWrapper, (this.props.isSearch === false) && (this.props.index > 0) && {marginTop: 26}]}>
               
                <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>          
                    <GothamNotRoundedMediumText style={[styles.statusText, {color: '#9b9c9d'}]}>COMPLETED</GothamNotRoundedMediumText>
                    <Image source={checkImg} style={styles.checkImg} />
                </View>
                <View style={this.props.index === 0 ? styles.firstDot : styles.dot}></View>        
                <View style={[
                styles.row, 
                (this.props.index === this.props.dataLength-1) && styles.lastRow,
                {color: '#9b9c9d'}, 
                ]} 
                >
                <View style={styles.largeRow}>
                    <View>
                    <View style={styles.avatar}>     
                        <Grayscale>
                           <ImageProgress source={{uri: this.props.itemImageUrl}} indicator={Progress.Circle} style={styles.avatarImg} />
                        </Grayscale>
                    </View>
                    <Image source={projBottomBg} style={styles.projBottom}/>
                    <View style={styles.typeWrapper}>
                    <View style={styles.type}>
                          <Grayscale>
                            <Image source={this.props.causeImg} style={styles.typeImg} />
                          </Grayscale>     
                    </View>
                        <GothamNotRoundedMediumText style={styles.causeName}>{this.props.item.cause}</GothamNotRoundedMediumText>
                    </View>
                    </View>
                    <View style={styles.column}>
                    <GothamNotRoundedMediumText style={[styles.rowTitle, {color: '#9b9c9d'} ]}>{this.props.itemName}</GothamNotRoundedMediumText>
                    <GothamNotRoundedMediumText style={[styles.rowTitle2, {color: '#9b9c9d'}]}>{this.props.itemName2}</GothamNotRoundedMediumText>
                    
                      <View style={{flexDirection: 'column', marginTop: 15}}>
                         {
                            this.props.itemStreet &&                        
                              <Text style={[styles.smallText, styles.locationText]} ellipsizeMode='tail' numberOfLines={2}>
                                  {this.props.itemStreet}
                               </Text>
                          }
                              <Text style={[styles.smallText, styles.locationText]} ellipsizeMode='tail' numberOfLines={1}>
                                {this.props.itemCity}, {this.props.itemState}
                              </Text>
                              <Text style={[styles.smallText, styles.locationText]} ellipsizeMode='tail' numberOfLines={1}>
                                  {this.props.itemZipcode}
                              </Text>
                       </View>
                       <GothamNotRoundedBookText style={[styles.smallText, styles.dateText]}>{moment(this.props.item.startDate).format('MMMM D, YYYY') + ' - ' + moment(this.props.item.endDate).format('MMMM D, YYYY')}</GothamNotRoundedBookText>
                       <GothamNotRoundedBookText style={[styles.smallText, styles.timeText]}>{moment(this.props.item.startTime, 'HH:mm').format('hh:mm a') + ' - ' + moment(this.props.item.endTime, 'HH:mm').format('hh:mm a')}</GothamNotRoundedBookText>

                        <View style={styles.hoursCard}>
                          <GothamNotRoundedMediumText style={styles.comingBtnText}>
                            {shiftDurationHours} HOUR{(shiftDurationHours === 1) ? '' : 'S'}
                          </GothamNotRoundedMediumText>
                        </View>

                </View>
                </View>
                    <View style={styles.overlayView}></View>
                </View>
                {
                (this.props.isSearch === false || this.props.isLike === true) &&
                <>
                {
                    (this.props.index === 0) ?
                    <LinearGradient
                    colors={['#8DC63F', '#1D75BD']}
                    style={styles.firstLine}
                    start={{x: 0, y: 0}}
                    end={{x: 0, y: 1}}
                    />
                    :
                    <View style={styles.vLine}></View>
                }
                </>
                }        
            </View>
        );
    }
    
  };

  const styles = StyleSheet.create({
    rowWrapper: {
      marginTop: 1,
      marginBottom: 10,
      marginHorizontal: 0,
    },
    row: {
      position: 'relative',
      backgroundColor: 'white',
      shadowColor: '#CACACA',
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 1,
      shadowRadius: 2,
      elevation: 4,
      borderRadius: 3,
      padding: 5,
      height: 155,
    },
    lastRow: {
      marginBottom: 15
    },
    avatar: {
      width: 109,
      height: 110,
      borderRadius: 3,
      overflow: 'hidden'
    },
    avatarImg: {
      width: '100%',
      height: '100%',
      resizeMode: 'cover'
    },
    projBottom: {
      position: 'absolute',
      bottom: 0,
      width: 109,
      height: 48
    },
    largeRow: {
      flexDirection: 'row',
      height: 142,
      position: 'relative',
    },
    column: {
      flex: 1, 
      paddingHorizontal: 10,
      paddingBottom: 7
    },
    timeText: {
      marginTop: 10,
    },
    locationText: {
      fontFamily: 'Gotham-Book',
      marginTop: 1,
      fontSize: 10,
      lineHeight: 11
    },
    rowTitle: {
      color: '#8DC63F',
      fontSize: 14,
      fontWeight: 'bold'
    },
    rowTitle2: {
      color: '#000000',
      fontSize: 11,
      fontWeight: 'bold',
      marginTop: 5
    },
    smallText: {
      color: '#777777',
      fontSize: 11,
      marginTop: 5
    },
    dateText: {
      flex: 1,
      marginTop: 5
    },
    typeWrapper: {
      position: 'absolute',
      top: 80,
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    type: {
      width: 35,
      height: 35,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 17.5,
      borderWidth: 1,
      borderColor: '#CACACA',
    },
    typeImg: {
      resizeMode: 'contain',
      width: 30,
      height: 30
    },
    checkImg: {
      marginLeft: 2
    },
    overlayView: {
      position: 'absolute', 
      width: Dimensions.get('window').width - 38, 
      height: 149, 
      backgroundColor: '#FAFDFF30', 
      borderRadius: 2
    },
    statusText: {
      fontSize: 10,
      textAlign: 'right',
      marginBottom: 7
    },
    dot: {
      position: 'absolute',
      width: 5,
      height: 5,
      borderRadius: 2.5,
      backgroundColor: '#CACACA',
      marginLeft: -12,
      marginTop: 5
    },
    firstDot: {
      position: 'absolute',
      width: 11,
      height: 11,
      borderRadius: 5.5,
      borderWidth: 3,
      backgroundColor: '#8DC63F',
      borderColor: '#CACACA50',
      marginLeft: -15,
      marginTop: 0
    },
    vLine: {
      width: 1,
      height: 192,
      position: 'absolute',
      backgroundColor: '#CACACA',
      marginLeft: -10,
      marginTop: 15
    },
    firstLine: {
      height: 192,
      position: 'absolute',
      width: 2,
      marginLeft: -10.5,
      marginTop: 15,
      backgroundColor: '#8DC63F',
    },
    causeName: {
      fontSize: 10,
      textTransform: 'uppercase',
      color: '#303030',
      marginTop: 6
    },
    hoursCard: {
      marginTop: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    comingBtnText: {
      fontSize: 11,
      // color: '#24B6EC',
      color: '#9B9C9D',
    },
  });
  