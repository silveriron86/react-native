import React from 'react';
import { connect } from 'react-redux';
import { Alert, ImageBackground, StyleSheet, StatusBar, View, Text, TextInput, Dimensions, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { PrimaryButton, DateInput } from '../../components';
import {ProjectsActions} from '../../actions';
import ApiConstants from '../../constants/ApiConstants';
import Utils from '../../utils';
import moment from 'moment';
const imgBkg = require('../../../assets/images/background9.jpg');
import NavigationVars from '../../navigation/NavigationVars';

class ConfirmProjectModal extends React.Component {
  constructor(props){
    super(props);
  }

    state = {
      description: '',
      startDate: '',
      endDate: '',
      startTime: '',
      endTime: ''
    };


  hidePanel = () => {
    if(this.props.CreateProjectState.addHourStatus){
      this.props.toggleModalAction({
        isProjectAddHours: true,
        isProjectModalOpen: false,
        isCauseModalOpen: false,
        isConfirmModalOpen: false
      });
    }else{
      this.props.toggleModalAction({
        isProjectModalOpen: false,
        isCauseModalOpen: true,
        isConfirmModalOpen: false
      });
    }  
  }

  cleanStatus = () =>{
    this.props.toggleModalAction({
      isProjectModalOpen: false,
      isCauseModalOpen: false,
      isConfirmModalOpen: false,
      isProjectAddHours: false,
      addHourStatus: false,
      sfProjectId: '',
      sfOrgId: '',
      description: '',
      startDate: '',
      endDate: '',
      startTime: '',
      endTime: '',
      projectCause: '',
      projectType: '',
      checked: true,
      orgTitle: '',
      projectTitle: '',
      contactFirstName: '',
      contactLastName: '',
      contactEmail: '',
      address: '',
      city: '',
      state: '',
      zipCode: ''
    });
  }

  cancel = () => {
    this.cleanStatus();
  }


  _createProject = async (cb) => {
    try{
        let authUserJSON = JSON.parse(await AsyncStorage.getItem('oio_auth'));
        let  userJSON = authUserJSON.user;
        let required = {
          token: authUserJSON.token, 
          sfContactId: userJSON.sfContactId,
          orgTitle: this.props.CreateProjectState.orgTitle, 
          projectTitle: this.props.CreateProjectState.projectTitle, 
          projectCause: this.props.CreateProjectState.projectCause, 
          projectType: this.props.CreateProjectState.projectType, 
          contactFirstName: this.props.CreateProjectState.contactFirstName, 
          contactLastName: this.props.CreateProjectState.contactLastName,
          contactEmail: this.props.CreateProjectState.contactEmail, 
          address: this.props.CreateProjectState.address, 
          city: this.props.CreateProjectState.city, 
          state: this.props.CreateProjectState.state, 
          zipCode: this.props.CreateProjectState.zipCode, 
          description: this.state.description, 
          startDate: this.state.startDate,
          startTime: this.state.startTime, 
          endDate: this.state.endDate, 
          endTime: this.state.endTime, 
          optInRatingReview: this.props.CreateProjectState.checked, 
          optInValidation: this.props.CreateProjectState.checked,
          sfProjectId: this.props.CreateProjectState.sfProjectId,
          sfOrgId: this.props.CreateProjectState.sfOrgId
        }
        return this.props.postCreateProjectSubmissionsAction(required);
    }catch(error){
      console.debug(error);
    }
  };

  onChangeDate = (date, type) => {
    if(type === 'startTime' || type === 'endTime'){
      date = moment(date, 'hh:mm a').format('HH:mm');
    }
    let state = this.state;
    state[type] = date;
    this.setState(state);
  }

  onConfirm = async () => {
    const { description, startDate, endDate, startTime, endTime } = this.state;
    if(!description) {
      Alert.alert('', 'Please enter a description.');
      return;
    }
    if(!startDate) {
      Alert.alert('', 'Please enter a start date.');
      return;
    }
    if(!endDate) {
      Alert.alert('', 'Please enter an end date.');
      return;
    }
    if(!startTime) {
      Alert.alert('', 'Please enter a start time.');
      return;
    }
    if(!endTime) {
      Alert.alert('', 'Please enter an end time.');
      return;
    }
    if(
      (moment.utc(startDate).format('YYYY-MM-DD') > moment.utc().format('YYYY-MM-DD')) ||
      (moment.utc(endDate).format('YYYY-MM-DD') > moment.utc().format('YYYY-MM-DD'))
      ){
      Alert.alert('', 'The start and end dates cannot be in the future.');
      return;
    }    
    if(moment.utc(startDate).format('YYYY-MM-DD') > moment.utc(endDate).format('YYYY-MM-DD')){
      Alert.alert('', 'The end date must be equal to or after the start date.');
      return;
    }
    if(moment.utc(startDate).format('YYYY-MM-DD') == moment.utc(endDate).format('YYYY-MM-DD')){
      if(startTime >= endTime){
        Alert.alert('','The start date and time must be before the end date and time');
        return;
      }
    }

    const maxShiftHours = 8; 

    const startDateTime = Utils.newDateObject(startTime, startDate)
    const endDateTime = Utils.newDateObject(endTime, endDate)
    const shiftDurationHours = Utils.hoursBetweenDates(startDateTime, endDateTime)

    if(shiftDurationHours > maxShiftHours){
      Alert.alert('',`Shifts cannot be more than ${maxShiftHours} hours in length. (This shift is ${shiftDurationHours} hours long.)`);
      return;
    }

    this.props.showLoaderOverlay();
    await this._createProject();    
    this.props.navigation.navigate('Profile');
  }

  render() {
    const { startDate, endDate, startTime, endTime } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar hidden={!Utils.nativeBaseDoesHeaderCorrectlyAlready()}/>
        <View style={styles.padHeader}></View>
        <View style={styles.header}>
          <TouchableOpacity onPress={this.hidePanel} style={styles.cancelBtn}>
            <Image source={require('../../../assets/icons/left_arrow.png')}/>
          </TouchableOpacity>
          <View><Text style={styles.headerTitle}>Add Volunteer Hours</Text></View>
          <TouchableOpacity onPress={this.cancel} style={styles.cancelBtn}>
            <Text style={styles.cancelText}>Cancel</Text>
          </TouchableOpacity>
        </View>
        <ImageBackground source={imgBkg} style={{width: '100%', flex: 1}} imageStyle={{opacity: 0.5}}>
          <View style={styles.wrapper}>
            <Text style={styles.title}>Describe your project</Text>
            <Text style={[styles.text, {marginTop: 29}]}>What was your impact? Describe it below.</Text>
            <TextInput multiline ={true} 
              style={styles.textInput} 
              placeholderTextColor='#606060'
              placeholder="How will this be used? The volunteer organization will confirm these details and it will be added to your volunteer transcript."
              onChangeText={(description) => this.setState({description})}
              blurOnSubmit={true}
              />
            <Text style={[styles.text, {marginTop: 27, marginBottom: 3}]}>Add, change or confirm dates and times.</Text>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <DateInput 
                  style={styles.dateContainer} 
                  placeholderColr='#606060'
                  date={startDate} 
                  placeholder='YYYY-MM-DD' 
                  format={'YYYY-MM-DD'}
                  noLeftPad={true}
                  onChange={(val)=>this.onChangeDate(val, 'startDate')}/>
              </View>
              <View style={{width: 20}}></View>
              <View style={{flex: 1}}>
                <DateInput 
                  style={styles.dateContainer} 
                  placeholderColr='#606060'
                  date={endDate} 
                  placeholder='YYYY-MM-DD'
                  format={'YYYY-MM-DD'} 
                  noLeftPad={true}
                  onChange={(val)=>this.onChangeDate(val, 'endDate')}/>
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <DateInput 
                  style={styles.dateContainer} 
                  placeholderColr='#606060'
                  date={startTime} 
                  mode={'time'}
                  placeholder='XX:XX am/pm' 
                  format={'hh:mm a'}
                  noLeftPad={true}
                  onChange={(val)=>this.onChangeDate(val, 'startTime')}/>
              </View>
              <View style={{width: 20}}></View>
              <View style={{flex: 1}}>
                <DateInput 
                  style={styles.dateContainer} 
                  placeholderColr='#606060'
                  date={endTime} 
                  mode={'time'}
                  placeholder='XX:XX am/pm' 
                  format={'hh:mm a'}
                  noLeftPad={true}
                  onChange={(val)=>this.onChangeDate(val, 'endTime')}/>           
              </View>
            </View>            
            <View style={styles.actionBtn}>
              <PrimaryButton
                onPress={this.onConfirm}
                title="CONFIRM MY PROJECT"
              />
            </View>            
          </View>              
        </ImageBackground> 
        {this.props.CreateProjectState.error && Alert.alert('',`An error occurred when trying to create the project ERROR CODE: ${this.props.CreateProjectState.error.error.statusCode}` )}      
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    CreateProjectState: state.CreateProjectModalReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
      toggleModalAction: (data) => dispatch(dispatch => (dispatch({type: ApiConstants.CREATE_PROJECT_TOGGLE_MODAL, data}))),
      postCreateProjectSubmissionsAction: (required) => dispatch(ProjectsActions.postCreateProjectSubmissions(required))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmProjectModal);

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    flex: 1
  },
  padHeader: {
    height: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 0,
    backgroundColor: '#1D75BD'
  },  
  wrapper: {
    margin: 20,
    padding: 14,
    backgroundColor: 'white',
    borderRadius: 2,
    shadowColor: '#CACACA',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 1,
    elevation: 4,
    flex: 1
  },
  title: {
    color: '#1D75BD',
    fontSize: 20,
    textTransform: 'uppercase',
    textAlign: 'center'
  },
  actionBtn: {
    justifyContent: 'center',
    alignItems: 'center',  
    marginTop: 113  
  },
  header: {
    backgroundColor: '#1D75BD',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  cancelBtn: {
    height: 40,
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelText: {
    fontSize: 14,
    color: 'white'
  },
  headerTitle: {
    color: 'white',
    fontSize: 16
  },
  text: {
    color: '#303030',
    fontSize: 14,
    lineHeight: 22
  },
  textInput: {
    fontSize: 15,
    lineHeight: 21,
    padding: 10,
    borderRadius: 4,
    borderColor: '#CACACA',
    borderWidth: 1,
    height: 120,
    width: '100%',
    marginTop: 5
  },
  dateContainer: {
    width: '100%',
    marginTop: 10
  }
});
