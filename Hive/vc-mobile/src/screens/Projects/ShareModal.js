import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    StatusBar,
    Dimensions,
    TouchableOpacity,
    AsyncStorage,
    Alert
} from 'react-native';
import SendSMS from 'react-native-sms';
import email from 'react-native-email';
import ImageProgress from 'react-native-image-progress';
import * as Progress from 'react-native-progress';

import { ShareApi, ShareDialog } from 'react-native-fbsdk';

import { GothamNotRoundedMediumText, GothamNotRoundedBookText } from '../../components';
import Utils from '../../utils';
import { GothamBoldText } from '../../components';

import General from '../../constants/General';

export default class ShareModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shareLinkContent: {
                contentType: "link",
                contentUrl: "https://volunteercrowd.com"
            },
            shareChannel : "",
        }
    }

    _buildUrl = async () => {
        const { navigation } = this.props;
        let data = navigation.getParam('data');
        let name = data.orgName.toLowerCase().replace(/\s/g, '');
        let city = data.orgCity.toLowerCase().replace(/\s/g, '');
        let state = data.orgState.toLowerCase().replace(/\s/g, '');
        let cause = data.cause.toLowerCase().replace(/\s/g, '');
            let authUserJSON = JSON.parse(await AsyncStorage.getItem('oio_auth'));
            let  userJSON = authUserJSON.user;
            let contactId;
            if(userJSON){
                contactId = userJSON.sfContactId;
            }else{
                contactId = "none";
            }
            this.setState({shareLinkContent:{contentType : "link", contentUrl : "https://volunteercrowd.com"}});

            // ORG LINK
            // ex: https://volunteercrowd.com/volunteer-opportunities/joshua-tree-ca/stem/0011U00000Os211QAB
            // let completeUrl = `${this.state.shareLinkContent.contentUrl}/${city}-${state}/${cause}/${name}?organizationId=${data.sfOrgId}&sfContactId=${contactId}&shareChannel=${this.state.shareChannel}&sharedFromBuild=${General.BUILD_VERSION}&sharedFromApp=mobile`;
            let completeUrl = `${this.state.shareLinkContent.contentUrl}/volunteer-opportunities/${city}-${state}/${cause}/${data.sfOrgId}?organizationId=${data.sfOrgId}&sfContactId=${contactId}&shareChannel=${this.state.shareChannel}&sharedFromApp=mobile&sharedFromBuild=${General.BUILD_VERSION}`;

            // PROJECT LINK
            // ex: https://www.volunteercrowd.com/volunteer-opportunities/bakersfield-ca/community/youth-2-leaders-education-foundation/7011U000000E9ZnQAK
            if(data.sfProjectId){
                // completeUrl = `${this.state.shareLinkContent.contentUrl}/${city}-${state}/${cause}/${name}?organizationId=${data.sfOrgId}&sfContactId=${contactId}&shareChannel=${this.state.shareChannel}&sharedFromBuild=${General.BUILD_VERSION}&sharedFromApp=mobile&projectId=${data.sfProjectId}`;
                completeUrl = `${this.state.shareLinkContent.contentUrl}/volunteer-opportunities/${city}-${state}/${cause}/${name}/${data.sfProjectId}?organizationId=${data.sfOrgId}&projectId=${data.sfProjectId}&sfContactId=${contactId}&shareChannel=${this.state.shareChannel}&sharedFromApp=mobile&sharedFromBuild=${General.BUILD_VERSION}`;
            }
            this.setState({shareLinkContent:{contentType : "link", contentUrl : completeUrl}});
    }
    goBack = () => {
        this.props.navigation.goBack();
    }

    onShare = async (type) => {
        let subject = 'Check out this super-easy volunteer project app called VolunteerCrowd!'
        if(type === 'facebook') {
            this.setState({shareChannel : "facebook"});
            await this._buildUrl();
            this.shareLinkWithShareDialog();
            // Share using the share API.
            // let tmp = this;                
            // ShareApi.canShare(this.state.shareLinkContent).then(
            //     function(canShare) {
            //         if (canShare) {
            //             return ShareApi.share(tmp.state.shareLinkContent, '/me', 'Some message.');
            //         }
            //     }
            // ).then(
            //     function(result) {
            //         console.log('Share with ShareApi success.');
            //     },
            //     function(error) {
            //         console.log('Share with ShareApi failed with error: ' + error);
            //     }
            // );            
        }else if(type === 'email') {
            this.setState({shareChannel : "email"});
            await this._buildUrl();
            let body = `Hey, I just wanted to share this app with you called VolunteerCrowd. You can find and track new volunteer projects super easy!! ${this.state.shareLinkContent.contentUrl} Check it out and pass it along!`
            email([], {
                subject: subject, 
                body: body
            }).catch(console.error)
        }else if(type === 'sms') {
            this.setState({shareChannel : "SMS"});
            await this._buildUrl();
            let body = `Hey, I just wanted to share this app with you called VolunteerCrowd. You can find and track new volunteer projects super easy!! ${this.state.shareLinkContent.contentUrl} Check it out and pass it along!`
            SendSMS.send({
                body: body,
                recipients: [],
                successTypes: ['sent', 'queued'],
                allowAndroidSendWithoutReadPermission: true
            }, (completed, cancelled, error) => {         
                console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);         
            });            
        }else {

        }
    }

    shareLinkWithShareDialog() {
        var tmp = this;
        console.log(this.state.shareLinkContent);
        ShareDialog.canShow(this.state.shareLinkContent).then(
            function(canShow) {
                console.log(canShow, tmp.state.shareLinkContent);
                if (canShow) {
                    return ShareDialog.show(tmp.state.shareLinkContent);
                }
            }
        ).then(
            function(result) {
                if (result.isCancelled) {
                    console.log('Share cancelled');
                } else {
                    console.log('Share success with postId: '
                    + result.postId);
                }
            },
            function(error) {
                console.log('Share fail with error: ' + error);
            }
        );
    }    

    render() {
        const { navigation } = this.props;
        let item = navigation.getParam('data');

        return (
        <View style={styles.container}>
            <StatusBar hidden={true} />
            {/* <Image style={styles.background} source={require('../../../assets/images/share_background.png')} /> */}
            <ImageProgress
                source={{uri: item.nteeCodeImgURL}}
                indicator={Progress.Bar}
                indicatorProps={{
                    marginTop: -150
                }}
                style={[styles.background, {height: 300}]} />
            <View style={styles.innerFrame}></View>

            <View style={styles.topRadius}>
                <Image style={[styles.background, styles.rounded]} source={require('../../../assets/images/top_rounded.png')} />
            </View>
            <View style={styles.modalWrapper}>
                <View style={styles.title}>
                    <GothamBoldText style={styles.titleText}>you do amazing things, do them with your friends</GothamBoldText>
                </View>
                {/* <Image style={styles.groupIcon} source={require('../../../assets/icons/group2.png')} /> */}
                <View style={styles.typeWrapper}>
                    <View style={styles.type}>
                    {
                        item.causeIconURL &&                
                        <ImageProgress
                            source={{uri: item.causeIconURL}}
                            indicator={Progress.Circle}
                            style={styles.typeImg} />
                    }
                    </View>
                    <GothamNotRoundedMediumText style={styles.causeName}>{item.cause}</GothamNotRoundedMediumText>
                </View>

                <View style={styles.socialActions}>
                    <TouchableOpacity style={styles.actionRowWrapper} onPress={()=>this.onShare('facebook')}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/facebook.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Share on Facebook</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity>

                    {/* <TouchableOpacity style={styles.actionRowWrapper} onPress={()=>this.onShare('instagram')}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/instagram.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Share on Instagram</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.actionRowWrapper} onPress={()=>this.onShare('snapchat')}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/snapchat.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Share on Snapchat</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity> */}

                    <TouchableOpacity style={styles.actionRowWrapper} onPress={()=>this.onShare('email')}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/email.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Email</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.actionRowWrapper} onPress={()=>this.onShare('sms')}>
                        <View style={styles.actionRow}>
                            <Image source={require('../../../assets/icons/social/text.png')} style={styles.actionIcon} />
                            <GothamNotRoundedBookText style={styles.actionText}>Text</GothamNotRoundedBookText>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>

            <TouchableOpacity onPress={this.goBack} style={styles.closeBtn}>
                <Image source={require('../../../assets/icons/close.png')} />
            </TouchableOpacity>     
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: Dimensions.get('window').width,
      alignItems: 'center'
    },
    closeBtn: {
        position: 'absolute',
        left: 17,
        top: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 40 : 16
    },  
    innerFrame: {
        position: 'absolute',
        width: '100%',
        height: 300,
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, .22)'
    },      
    background: {
        width: '100%',
        resizeMode: 'cover',
        position: 'absolute'
    },
    rounded: {
        width: Dimensions.get('window').width,
        height: 499,
        resizeMode: 'stretch'
    },
    title: {
        // width: 339,
        marginTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 70 : 57
    },
    titleText: {
        color: 'white',
        fontSize: 22,
        lineHeight: 26,
        textTransform: 'uppercase',
        textAlign: 'center'
    },
    topRadius: {
        borderWidth: 1,
        borderColor: 'red',
        position: 'absolute',
        left: 0,
        marginTop: 162,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    modalWrapper: {
        flex: 1,
        alignItems: 'center'
    },
    groupIcon: {
        top: 10
    },
    actionRowWrapper: {
        borderBottomWidth: 1,
        borderBottomColor: '#E0E2EE',
        justifyContent: 'flex-start',
        width: Dimensions.get('window').width - 42,
        paddingTop: 12,
        paddingBottom: 8
    },
    actionRow: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    actionIcon: {
        tintColor: '#1D75BD',
        width: 40,
        height: 38,
        marginLeft: 23
    },
    actionText: {
        color: '#303030',
        fontSize: 16,
        marginLeft: 20,
        marginTop: 5,
        lineHeight: 20.2
    },
    socialActions: {
        marginTop: 40
    },
    typeWrapper: {
        marginTop: Utils.nativeBaseDoesHeaderCorrectlyAlready() ? 10 : 20,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    type: {
        width: 100,
        height: 100,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#CACACA'
    },
    typeImg: {
        resizeMode: 'center',
        width: 60,
        height: 60
    },    
    causeName: {
        fontSize: 16,
        color: '#8DC63F',
        marginTop: 13
    }
});

 