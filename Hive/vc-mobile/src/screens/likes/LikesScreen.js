import React from 'react';
import { StyleSheet, View, StatusBar, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { LikesActions, SearchActions, ProjectsActions } from '../../actions';
import HomeNavBar from '../Home/HomeNavBar';
import { GothamBookText, GothamMediumText, CornerLabel } from '../../components';
import Colors from '../../constants/Colors';
import ProjectsList from '../Search/ProjectsList';
import ApiConstants from '../../constants/ApiConstants';
import Config from 'react-native-config'

class LikesScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    isPanelOpen: false
  };

  handleLike = (item) => {
    this.props.postLikes({
      item: item,
      toggle: false,
      cb: () => {}
    });
  }

  handleSelectRow = (item) => {
    this.props.navigation.navigate('Calendar', {
      data: item
    });  
  }

  goProjectDetails = (data) => {
    this.props.navigation.navigate('ProjectDetails', {
      data: data
    });
  }

  handleReviewNow = () => {

  }

  componentDidMount = () => {
    // this.props.getList({
    //   cb: (res) => {
    //     console.log(res);
    //   }
    // });
  }

  render() {
    const { navigation, likesListData, filteredListData } = this.props;
    // const { navigation, filteredListData } = this.props;
    // const { navigation } = this.props;

    // TEMP NULLIFY UNTIL APIs ARE READY
    const nullFilteredListData = null

    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="blue" barStyle="light-content" />
        <HomeNavBar
          onLeftButtonPress={() => navigation.toggleDrawer()}
        />
        { Config.ENVIRONMENT === 'DEV' && 
          <View style={styles.cornerLabelView}>
            <CornerLabel>DEV</CornerLabel> 
          </View>
        }
        <GothamMediumText
          style={styles.titleText}>
          PROJECTS <Text style={{color: '#8DC63F'}}>I LOVE</Text>
        </GothamMediumText>      
        <Text style={styles.titleText}>
          Coming Soon
        </Text>

        {
          // likesListData &&
          // filteredListData &&
          nullFilteredListData &&

          <ProjectsList 
            // data={likesListData}
            // data={filteredListData}
            data={nullFilteredListData}
            isLike={true}
            isSearch={false}
            handleSelectRow={this.handleSelectRow} 
            handleReviewNow={this.handleReviewNow}
            handleLike={this.handleLike}
            goProjectDetails={this.goProjectDetails}
          />         
        }
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    filteredListData: state.SearchReducer.filteredListData,
    // filteredListData: state.SearchReducer.nullFilteredListData,
    likesListData: state.LikesReducer.likesListData,   
    likesListError: state.LikesReducer.likesListData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getList: (request) => dispatch(SearchActions.getFilterList(request.data, request.cb)),
    getList: (request) => dispatch(LikesActions.getLikesList(request.cb)),
    postLikes: (request) => dispatch(SearchActions.postLikes(request.item, request.toggle, request.cb))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(LikesScreen);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  titleText: {
    marginTop: 13,
    marginBottom: 40,
    textAlign: 'center',
    fontSize: 22,
    lineHeight: 26,
    color: Colors.tintColor,
  },
  addBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 10
  },
  addBtnText: {
    color: '#24B6EC',
    fontSize: 13
  },
  cornerLabelView: {
    position: 'absolute',
    marginTop: -100,
    flexDirection: 'row',
    width: '100%',
  }
});
