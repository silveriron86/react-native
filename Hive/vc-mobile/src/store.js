import { createStore, applyMiddleware } from 'redux';
import Reactotron from '../ReactotronConfig'
import logger from 'redux-logger';
import rootReducer from './reducers/rootReducer';
import {
    composeWithDevTools
} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';

const middleware = [promiseMiddleware, thunk, logger];
const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(...middleware),
        Reactotron.createEnhancer()
        // applyMiddleware(...middleware, Reactotron.createEnhancer()),
    )
);


export default store;