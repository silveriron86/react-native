import Reactotron, { asyncStorage } from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'

// Reactotron
const reactotron = Reactotron
  .configure({
    name: "React Native VolunteerCrowd"
  })
  .use(reactotronRedux())
  .use(asyncStorage())
  .useReactNative({
    asyncStorage: true, // there are more options to the async storage.
    networking: { // optionally, you can turn it off with false.
      ignoreUrls: /symbolicate/
    },
    editor: false, // there are more options to editor
    errors: { veto: (stackFrame) => false }, // or turn it off with false
    overlay: false, // just turning off overlay
  })
  .connect();
  export default reactotron