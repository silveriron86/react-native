

### COMMON BUILD ERRORS AND SOLUTIONS


'FBSDKCoreKit/FBSDKCoreKit.h' file not found
--------------------------------------------
Solution: https://nsulistiyawan.github.io/2016/07/05/Fix-FBSDKCoreKitFBSDKCoreKith-file-not-found-on-React-Native.html


'FBSDKShareKit/FBSDKShareKit.h' file not found
--------------------------------------------
per: https://stackoverflow.com/questions/35590903/xcode-always-says-fbsdksharekit-fbsdksharekit-h-file-not-found
Select Libraries > RCTFBSDK.xcodeproj
Select Build Settings > Framework Search Paths
Set or Replace the ~/Documents/FacebookSDK to $(HOME)/Documents/FacebookSDK (with parentheses!)
Clean the build folder
Build
