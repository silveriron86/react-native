#!/bin/bash
### fixes "double-conversion" errors after a fresh build
### per: https://stackoverflow.com/questions/55466599/xcode10-build-input-file-double-conversion-cannot-be-found
cd node_modules/react-native/scripts && ./ios-install-third-party.sh && cd ../../../

cd node_modules/react-native/third-party/glog-0.3.5/ && ../../scripts/ios-configure-glog.sh && cd ../../../../