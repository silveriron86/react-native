/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

#import "React/RCTBridgeModule.h"
@interface RCT_EXTERN_MODULE(OAuth, NSObject)
  RCT_EXTERN_METHOD(open:(NSString*)type)
  RCT_EXTERN_METHOD(signup:(NSString*)data)
  RCT_EXTERN_METHOD(logout)
  RCT_EXTERN_METHOD(getStatus:(RCTResponseSenderBlock)callback)
@end

int main(int argc, char * argv[]) {
  @autoreleasepool {
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
