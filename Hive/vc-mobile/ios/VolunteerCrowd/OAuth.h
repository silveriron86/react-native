//
//  OAuth.h
//  VolunteerCrowd
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import <OAuthiOS/OAuthiOS.h>

NS_ASSUME_NONNULL_BEGIN

@interface OAuth : NSObject<OAuthIODelegate>

- (void)open:(NSString *)type;
- (void)signup:(NSString *)data;
- (void)logout;
- (void)callUserAPI:(NSString*)type data:(NSString*)data;
- (void)doCall:(NSString *)url data:(NSString*)data;
- (void)doCallback:(NSData *)data;

// Handles the results of a successful authentication
- (void)didReceiveOAuthIOResponse:(OAuthIORequest *)request;

// Handle errors in the case of an unsuccessful authentication
- (void)didFailWithOAuthIOError:(NSError *)error;

@property (nonatomic) RCTResponseSenderBlock callback;

@end

NS_ASSUME_NONNULL_END
