//
//  OAuth.m
//  VolunteerCrowd
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "OAuth.h"

@implementation OAuth


// THIS SETTING DOES NOT MATTER PROD/DEV... IT IS OVERRIDDEN BY THE .ENV CONFIG FILES 
// PROD
NSString *const APP_KEY = @"KJli2sxd99FA4FLH2D0baibU2Sg";
// DEV
// NSString *const APP_KEY = @"CFrJx_z_AEbE2otYYdxRLxx5hXQ";

- (instancetype)init
{
  self = [super init];
  if (self) {
    
  }
  return self;
}

- (void)open:(NSString *)type{
  dispatch_async(dispatch_get_main_queue(), ^(void) {
    OAuthIOModal *oauthioModal = [[OAuthIOModal alloc] initWithKey:APP_KEY delegate:self];
    NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
    [options setObject:@"false" forKey:@"cache"];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36", @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    [oauthioModal showWithProvider:type options:options];
  });
}

- (void)signup:(NSString *)data {
  [self callUserAPI:@"signup" data:data];
}

- (void)logout {
  // delete all cookies of UIWebView
  NSHTTPCookie *cookie;
  NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
  for (cookie in [storage cookies]) {
    [storage deleteCookie:cookie];
  }
  [[NSUserDefaults standardUserDefaults] synchronize];
  
  [[NSURLSession sharedSession] resetWithCompletionHandler:^{
    // Do something once it's done.
  }];
}

- (void)callUserAPI:(NSString*)type data:(NSString*)data {
  NSString *url = [NSString stringWithFormat:@"https://oauth.io/api/usermanagement/%@?k=%@", type, APP_KEY];
  [self doCall:url data:data];
}

- (void)doCall:(NSString *)url data:(NSString*)data {
  dispatch_async(dispatch_get_main_queue(), ^(void) {
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"cache-control": @"no-cache" };
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    if([data isEqualToString:@""] == NO) {
      NSData* postData = [data dataUsingEncoding:NSUTF8StringEncoding];
      [request setHTTPMethod:@"POST"];
      [request setAllHTTPHeaderFields:headers];
      [request setHTTPBody:postData];
    }
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                  if (error) {
                                                    NSDictionary *dictRes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                             @"error", @"status",
                                                                             [error localizedDescription], @"error",
                                                                             nil];
                                                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictRes options:NSJSONWritingPrettyPrinted error:nil];
                                                    [self doCallback:jsonData];
                                                  } else {
                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                    NSLog(@"%@", httpResponse);
                                                    [self doCallback:data];
                                                  }
                                                }];
    [dataTask resume];
  });
}

- (void)doCallback:(NSData *)data {
  NSMutableArray* params = [NSMutableArray new];
  [params addObject:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil]];
  self.callback(params);
}

- (void)getStatus:(RCTResponseSenderBlock) callback{
  self.callback = callback;
}

+ (BOOL)requiresMainQueueSetup{
  return true;
}

- (void)didAuthenticateServerSide:(NSString *)body andResponse:(NSURLResponse *)response {
  NSLog(@"%@",@"didAuthenticateServerSide");
}

- (void)didFailAuthenticationServerSide:(NSString *)body andResponse:(NSURLResponse *)response andError:(NSError *)error {
  NSLog(@"%@",@"didFailAuthenticationServerSide");
}

- (void)didFailWithOAuthIOError:(NSError *)error {
  NSLog(@"%@",@"didFailWithOAuthIOError");
  
}

- (void)didReceiveOAuthIOCode:(NSString *)code {
  NSLog(@"%@",@"didReceiveOAuthIOCode");
}

- (void)didReceiveOAuthIOResponse:(OAuthIORequest *)request {
  NSDictionary *credentials = [request getCredentials];
  NSLog(@"%@", credentials);
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:credentials options:NSJSONWritingPrettyPrinted error:nil];
  [self doCallback:jsonData];
//  [request me:nil success:^(NSDictionary *output, NSString *body, NSHTTPURLResponse *httpResponse) {
//    NSData *jsonData =  [body dataUsingEncoding:NSUTF8StringEncoding];
//    [self doCallback:jsonData];
//  }];
}

@end


