import React from 'react';
import { NativeModules, Platform, StatusBar, StyleSheet, View, AppState, Text } from 'react-native';
import SplashScreen from 'react-native-splash-screen'
import AppNavigator from './src/navigation/AppNavigator';

// import PushNotification from 'react-native-push-notification';
// import PushController from './src/controllers/PushController';

import { Provider } from 'react-redux';
import store from './src/store';


if(__DEV__) {

  // for the Reactotron Debugger App ( do not use both )
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'))

  // for the React Native Debugger App ( do not use both )
  // https://github.com/jhen0409/react-native-debugger/blob/master/docs/debugger-integration.md#debugging-tips
  // NativeModules.DevSettings.setIsDebuggingRemotely(true)
}

// Hide warning
console.disableYellowBox = true;

export default class App extends React.Component {

  constructor(props) {
    super(props);

    // *** Uncomment to test local notifications ***
    // this.handleAppStateChange = this.handleAppStateChange.bind(this);
    this.state = {
      seconds: 5,
    };
  }

  componentDidMount() {
    setTimeout(function() {
      SplashScreen.hide();
    }, 500);

    // *** Uncomment to test local notifications ***
    // AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
    // *** Uncomment to test local notifications ***
    // AppState.removeEventListener('change', this.handleAppStateChange);
  }

  // *** Uncomment to test local notifications ***  
  // handleAppStateChange(appState) {
  //   if (appState === 'background') {
  //     let date = new Date(Date.now() + (this.state.seconds * 1000));

  //     PushNotification.localNotificationSchedule({
  //       message: "My Notification Message",
  //       date: date,
  //     });
  //   }
  // }

  render() {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <Provider store = {store}>
          <AppNavigator />
        </Provider>
        {/* <PushController /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
