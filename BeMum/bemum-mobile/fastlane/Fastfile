# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins

PROJECT_NAME = "BeMum"
XCODE_PROJECT = "./ios/#{PROJECT_NAME}.xcodeproj"
XCODE_WORKSPACE = "./ios/#{PROJECT_NAME}.xcworkspace"
OUTPUT_DIRECTORY = "./fastlane/builds"
IPA_DIRECTORY = "#{OUTPUT_DIRECTORY}/#{PROJECT_NAME}.ipa"

platform :ios do

  desc 'Bump CFBundleVersion
  https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundleversion
  https://docs.fastlane.tools/actions/latest_testflight_build_number/'
  def increment_build_number
    new_build_number = (latest_testflight_build_number + 1).to_s
    
    set_info_plist_value(path: "ios/BeMum/Info.plist", key: "CFBundleVersion", value: new_build_number)
    set_info_plist_value(path: "ios/BeMumTests/Info.plist", key: "CFBundleVersion", value: new_build_number)

    return new_build_number
  end

  desc 'Bump CFBundleShortVersionString
   https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundleshortversionstring
   https://docs.fastlane.tools/actions/latest_testflight_build_number/'
  def increment_version_number
    # we do this to set the context --> SharedValues::LATEST_TESTFLIGHT_VERSION
    latest_testflight_build_number
    version = lane_context[SharedValues::LATEST_TESTFLIGHT_VERSION].split('.').map{|i| i.to_i}
    version.length == 3 ? version[2] = version.last + 1 : version[2] = 0
    new_version = version.map{|i| i.to_s}.join('.')
    
    set_info_plist_value(path: "ios/BeMum/Info.plist", key: "CFBundleShortVersionString", value: new_version)
    set_info_plist_value(path: "ios/BeMumTests/Info.plist", key: "CFBundleShortVersionString", value: new_version)

    return new_version
  end

  lane :local do
    match
    # http://docs.fastlane.tools/actions/gym/
    gym(
      configuration: 'Release',
      scheme: PROJECT_NAME,
      workspace: XCODE_WORKSPACE,
      silent: true,
      export_method: 'app-store',
      output_directory: OUTPUT_DIRECTORY,
      output_name: "#{PROJECT_NAME}.ipa"
    )
  end

  # This lane is used on github actions
  lane :beta do
    # https://docs.fastlane.tools/actions/setup_ci/
    setup_ci
    xcode_select("/Applications/Xcode_11.7.app")
    # https://docs.fastlane.tools/actions/app_store_connect_api_key/
    app_store_connect_api_key(
      key_id: ENV['APPLE_KEY_ID'],
      issuer_id: ENV['APPLE_ISSUER_ID'],
      key_content: ENV['APPLE_KEY_CONTENT']     
    )
    
    # https://docs.fastlane.tools/actions/match/
    match(
      git_basic_authorization: ENV['GIT_BASIC_AUTHORIZATION'],
      type: "appstore",
      readonly: is_ci
    )

    build_number = increment_build_number
    version_number = increment_version_number

    # http://docs.fastlane.tools/actions/gym/
    gym(
      configuration: 'Release',
      export_method: 'app-store',
      scheme: PROJECT_NAME,
      workspace: XCODE_WORKSPACE,
      silent: true,
      output_directory: OUTPUT_DIRECTORY,
      output_name: "#{PROJECT_NAME}.ipa"
    )

    # https://docs.fastlane.tools/actions/upload_to_testflight/
    upload_to_testflight(
      username: ENV["APPLE_ID"],
      skip_waiting_for_build_processing: true,
      app_identifier: ENV["APP_IDENTIFIER"],
      team_id: ENV["APPSTORE_TEAM_ID"],
      ipa: IPA_DIRECTORY
    )

    git_commit(
      path: 'ios',
      message: "build(ios): bump CFBundleVersion to #{build_number} & CFBundleShortVersionString to #{version_number}"
    )
    push_to_git_remote

    slack(
      message: "Successfully distributed a new iOS beta build: #{version_number} (#{build_number})",
      slack_url: ENV["SLACK_URL"]
    )
  end

  lane :release do
    # https://docs.fastlane.tools/actions/setup_ci
    setup_ci
    xcode_select("/Applications/Xcode_11.7.app")
    # https://docs.fastlane.tools/actions/app_store_connect_api_key/
    app_store_connect_api_key(
      key_id: ENV['APPLE_KEY_ID'],
      issuer_id: ENV['APPLE_ISSUER_ID'],
      key_content: ENV['APPLE_KEY_CONTENT']     
    )

    # https://docs.fastlane.tools/actions/match/
    match(
      git_basic_authorization: ENV['GIT_BASIC_AUTHORIZATION'],
      type: "appstore",
      readonly: is_ci
    )

    build_number = increment_build_number
    version_number = increment_version_number
    
    # http://docs.fastlane.tools/actions/gym/
    gym(
      configuration: 'Release',
      export_method: 'app-store',
      scheme: PROJECT_NAME,
      workspace: XCODE_WORKSPACE,
      silent: true,
      output_directory: OUTPUT_DIRECTORY,
      output_name: "#{PROJECT_NAME}.ipa"
    )

    upload_to_app_store(
      username: ENV["APPLE_ID"],
      app_identifier: ENV["APP_IDENTIFIER"],
      team_id: ENV["APPSTORE_TEAM_ID"],
      ipa: IPA_DIRECTORY,
      submit_for_review: false,
      run_precheck_before_submit: false,
      force: true
    )

    git_commit(
      path: 'ios', message: "build(ios): bump CFBundleVersion to #{build_number} & CFBundleShortVersionString to #{version_number}"
    )
    push_to_git_remote
    slack(
      message: "Successfully distributed a new App Store build",
      slack_url: ENV["SLACK_URL"]
    )
  end
end

platform :android do

  # Increment `versionName` in gradle build file
  # see https://developer.android.com/studio/publish/versioning'
  def increment_version_name
    file_content = File.read('../android/app/build.gradle')
    
    # Look for something like `versionName "3.0.0"`
    version_name_regex = /versionName\s+("\d+.\d+.\d+")/
    version_name = file_content[version_name_regex, 1]
    if version_name.nil? 
      throw "versionName not valid in android/app/build.gradle"
    end

    # Bump it to 3.0.1
    _tmp = version_name.tr('"', '').split('.').map{|number| number.to_i}
    _tmp[-1] = _tmp.last + 1
    new_version_name = '"' + _tmp.map{|number| number.to_s}.join('.') + '"'
    
    file_content[version_name_regex, 1] = new_version_name
    file = File.new('../android/app/build.gradle', 'w')
    file.write(file_content)
    file.close

    return new_version_name
  end

  # Increment `versionCode` in gradle build file
  # see https://developer.android.com/studio/publish/versioning'
  def increment_version_code
    file_content = File.read('../android/app/build.gradle')

    version_code_regex = /versionCode\s+(\d+)/
    version_code = file_content[version_code_regex, 1].to_i
    if version_code.nil? 
      throw "versionCode not valid in android/app/build.gradle"
    end
    
    new_version_code = (version_code + 1).to_s
    file_content[version_code_regex, 1] = new_version_code
    file = File.new('../android/app/build.gradle', 'w')
    file.write(file_content)
    file.close

    return new_version_code
  end
   

  # Produce apk file
  def build_android
      store_password = ENV["BEMUM_PASSWORD"]
      key_password = ENV["BEMUM_PASSWORD"]
      releaseFilePath = File.join(Dir.pwd, "BeMumKey")
      # https://docs.fastlane.tools/actions/gradle/
      gradle(
        task: 'clean',
        project_dir: 'android'
      )
      gradle(
        task: 'assemble',
        project_dir: 'android',
        build_type: 'Release',
        print_command: false,
        properties: {
          "android.injected.signing.store.file" => releaseFilePath,
          "android.injected.signing.store.password" => store_password,
          "android.injected.signing.key.alias" => "key0",
          "android.injected.signing.key.password" => key_password,
        }
      )
  end

  desc "Local build"
  lane :local do
      build_android
  end


  desc "Build and upload to the Play Store (internal test)"
  lane :beta do
      version_name = increment_version_name
      version_code = increment_version_code
      build_android

      # https://docs.fastlane.tools/actions/upload_to_play_store/
      upload_to_play_store(track: 'internal')


      commit_message = "build(android): bump versionCode to #{version_code}, versionName to #{version_name}"
      git_commit(path: 'android/app/build.gradle', message: commit_message) 
      push_to_git_remote(
        remote: "origin",          # optional, default: "origin"
        local_branch: git_branch,  # optional, aliased by "branch", default is set to current branch
        remote_branch: git_branch, # optional, default is set to local_branch
        force: false,              # optional, default: false
        force_with_lease: false,   # optional, default: false
        tags: false,               # optional, default: true
        no_verify: false,          # optional, default: false
        set_upstream: true         # optional, default: false
      )

      slack(
        message: "BeMum Android app uploaded to Play Store Internal Track : #{version_name} (#{version_code})",
        success: true,
        slack_url: ENV["SLACK_URL"],
      )
  end

  desc "Build and upload to the Play store (Production)"
  lane :production do
      version_name = increment_version_name
      version_code = increment_version_code
      build_android

      # https://docs.fastlane.tools/actions/upload_to_play_store/
      upload_to_play_store

      commit_message = "build(android): bump versionCode to #{version_code}, versionName to #{version_name}"
      git_commit(path: 'android/app/build.gradle', message: commit_message) 
      push_to_git_remote(
        remote: "origin",          # optional, default: "origin"
        local_branch: git_branch,  # optional, aliased by "branch", default is set to current branch
        remote_branch: git_branch, # optional, default is set to local_branch
        force: false,              # optional, default: false
        force_with_lease: false,   # optional, default: false
        tags: false,               # optional, default: true
        no_verify: false,          # optional, default: false
        set_upstream: true         # optional, default: false
      )

      
      slack(
        message: "BeMum Android app uploaded to Play Store: : #{version_name} (#{version_code})",
        success: true,
        slack_url: ENV["SLACK_URL"],
      )
  end
end
