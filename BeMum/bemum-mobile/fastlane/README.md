fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios local
```
fastlane ios local
```
Bump CFBundleVersion
  https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundleversion
  https://docs.fastlane.tools/actions/latest_testflight_build_number/

Bump CFBundleShortVersionString
   https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundleshortversionstring
   https://docs.fastlane.tools/actions/latest_testflight_build_number/
### ios beta
```
fastlane ios beta
```

### ios release
```
fastlane ios release
```


----

## Android
### android local
```
fastlane android local
```
Local build
### android beta
```
fastlane android beta
```
Build and upload to the Play Store (internal test)
### android production
```
fastlane android production
```
Build and upload to the Play store (Production)

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
