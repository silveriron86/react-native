const {default: Utils} = require('.');
const {ApiConstants} = require('../constants');
const SignupStress = require('../constants/Questionnaires/SignupStress');

describe('Utilities', () => {
  it('Capitalize', () => {
    expect(Utils.capitalize('test')).toEqual('Test');
  });

  it('Get Header for Login and Forgot APIs', () => {
    const Host = ApiConstants.getApiHost();
    const expectedHeader1 = {
      'Content-Type': 'application/json',
      'Content-Length': 14,
      Host,
      'X-Bemum-Client': 'customer-mobile-client',
    };
    expect(Utils.getHeader({test: '123'})).toEqual(expectedHeader1);

    const expectedHeader2 = {
      'Content-Type': 'application/json',
      'Content-Length': 0,
      Host,
      'X-Bemum-Client': 'customer-mobile-client',
    };
    expect(Utils.getHeader()).toEqual(expectedHeader2);
  });

  it('Get error message by checking value range', () => {
    const min = 1;
    const max = 10;
    expect(Utils.checkValueRange(min, max, 11)).toEqual(
      'La valeur doit être inférieure à 10',
    );
    expect(Utils.checkValueRange(min, max, 0)).toEqual(
      'La valeur doit être supérieure à 1',
    );
    expect(Utils.checkValueRange(min, max, '')).toEqual(
      'La valeur doit être comprise entre 1 et 10',
    );
  });

  it('Pad an integer with zeros on the left', () => {
    const num = 13;
    const len = 5;
    expect(Utils.padNumber(num, len)).toEqual('00013');
  });

  it('Format number with 1 digit after the decimal separator', () => {
    expect(Utils.formatNumber(3)).toEqual('3,0');
    expect(Utils.formatNumber(3.5)).toEqual('3,5');
  });
});

describe('Questions', () => {
  it('Get question value', () => {
    const questions = [
      {
        question_id: 'WEIGHT',
        required: true,
        ui: 'stepper',
        value: 65,
      },
      {
        question_id: 'WAIST',
        required: true,
        ui: 'stepper',
        value: 60,
      },
    ];
    expect(Utils._getQuestionValue(questions, 'WAIST')).toEqual(60);
    expect(Utils._getQuestionValue(questions, 'CONSUMPTION_TEA')).toEqual(null);
  });

  it('Check the visibility of the question', () => {
    const questions = SignupStress;
    const type = 'signup';
    const form_id = 'stress';
    const props = signupData;
    const data = {};

    const question1 = SignupStress.find(q => q.question_id === 'ANXIOUS');
    expect(
      Utils.checkVisibleQuestion(
        data,
        questions,
        question1,
        props,
        type,
        form_id,
      ),
    ).toEqual(true);

    const question2 = SignupStress.find(
      q => q.question_id === 'ALCOHOL_RELAXED',
    );
    expect(
      Utils.checkVisibleQuestion(
        data,
        questions,
        question2,
        props,
        type,
        form_id,
      ),
    ).toEqual(false);
  });
});

const signupData = {
  signup_stressFormData: {
    title: 'Stress',
    form_id: 'stress',
    survey_id: 'stress',
    questions: [
      {
        question_id: 'STRESSED',
        required: true,
        ui: 'switch',
        label_question: 'Vous sentez-vous stressée en ce moment ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      {
        question_id: 'ANXIOUS',
        required: true,
        ui: 'switch',
        label_question: 'Vous sentez-vous anxieuse ou angoissée ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      {
        question_id: 'STRESSED_ON_WAKING',
        required: true,
        ui: 'switch',
        label_question:
          "Vous réveillez-vous avec un sentiment d'oppression ou de boule dans le ventre ?",
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      {
        question_id: 'STRESS_PERSONNAL_OR_JOB',
        required: true,
        ui: 'switch',
        label_question:
          'Êtes-vous préoccupée actuellement, au niveau professionnel ou personnel ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      {
        question_id: 'STRESSFUL_EVENT',
        required: false,
        ui: '',
        label_question:
          'Quels évènements parmi ceux-ci avez vous vécu ces douze derniers mois ?',
        value: null,
      },
      {
        question_id: 'STRESSFUL_EVENT_PROFESSIONAL',
        required: true,
        ui: 'checkbox',
        label_question: 'Changement professionnel',
        value: false,
      },
      {
        question_id: 'STRESSFUL_EVENT_MARRIAGE',
        required: true,
        ui: 'checkbox',
        label_question: 'Mariage',
        value: false,
      },
      {
        question_id: 'STRESSFUL_EVENT_DIVORCE',
        required: true,
        ui: 'checkbox',
        label_question: 'Divorce',
        value: false,
      },
      {
        question_id: 'STRESSFUL_EVENT_BEREAVEMENT',
        required: true,
        ui: 'checkbox',
        label_question: 'Deuil',
        value: false,
      },
      {
        question_id: 'STRESSFUL_EVENT_HARASSMENT',
        required: true,
        ui: 'checkbox',
        label_question: 'Harcèlement',
        value: false,
      },
      {
        question_id: 'STRESSFUL_EVENT_MOVE',
        required: true,
        ui: 'checkbox',
        label_question: 'Déménagement',
        value: false,
      },
      {
        question_id: 'STRESSFUL_EVENT_FINANCIAL',
        required: true,
        ui: 'checkbox',
        label_question: 'Problèmes financiers',
        value: false,
      },
      {
        question_id: 'STRESSFUL_EVENT_SURGERY',
        required: true,
        ui: 'checkbox',
        label_question: 'Chirurgie',
        value: false,
      },
      {
        question_id: 'STRESSFUL_EVENT_ACCIDENT',
        required: true,
        ui: 'checkbox',
        label_question: 'Accident',
        value: false,
      },
      {
        question_id: 'DESIRE_TO_EAT',
        required: true,
        ui: 'switch',
        label_question:
          'Avez-vous souvent envie ou besoin de manger au cours de la journée sans sensation de faim ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: true,
      },
      {
        question_id: 'DESIRE_TO_EAT_MOMENT',
        required: true,
        label_question: 'À quel moment de la journée ?',
        ui: 'radio',
        ui_config: {
          choices: [
            {
              label: "Essentiellement en fin d'après midi ou en soirée",
              value: 0,
            },
            {
              label: "Cela peut être n'importe quand dans la journée",
              value: 1,
            },
          ],
        },
        conditions: [
          {
            parentQuestionId: 'DESIRE_TO_EAT',
            operator: '$eq',
            value: true,
          },
        ],
        value: 1,
      },
      {
        question_id: 'SNACKING',
        required: true,
        ui: 'switch',
        label_question:
          'Vous arrive-t-il régulièrement de répondre à cette envie et de grignoter sans sensation de faim ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        conditions: [
          {
            parentQuestionId: 'DESIRE_TO_EAT',
            operator: '$eq',
            value: true,
          },
        ],
        value: true,
      },
      {
        question_id: 'ALCOHOL_RELAXED',
        required: true,
        ui: 'switch',
        label_question:
          "Vous sentez-vous détendue après avoir bu de l'alcool ?",
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        conditions: [
          {
            parentQuestionId: 'ALCOHOL',
            operator: '$eq',
            value: true,
          },
          {
            parentQuestionId: 'ALCOHOL_WEEKLY_QTY',
            operator: '$gte',
            value: 14,
          },
        ],
        conditions_assoc: 'AND',
        value: true,
      },
      {
        question_id: 'MED_SPASMOPHILIA',
        required: true,
        ui: 'switch',
        label_question:
          'Avez-vous des épisodes de tétanie ou de spasmophilie ?',
        tooltip: 'Contractions importantes et prolongées des muscles.',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      {
        question_id: 'MED_TINNITUS',
        required: true,
        ui: 'switch',
        label_question: 'Avez-vous des acouphènes ?',
        tooltip:
          "Sensation auditive de bourdonnement ou tintement qui n'est pas causée par un son extérieur.",
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      {
        question_id: 'MED_PALPITATIONS',
        required: true,
        ui: 'switch',
        label_question: 'Faites-vous régulièrement de la tachycardie ?',
        tooltip:
          'Trouble du rythme cardiaque qui consiste en un accélération des battements du coeur (plus de 100 battements par minute).',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      {
        question_id: 'MED_HAIR_LOSS',
        required: true,
        ui: 'switch',
        label_question: 'Avez-vous constaté une perte anormale de cheveux ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: true,
      },
      {
        question_id: 'MED_CRAMPS',
        required: true,
        ui: 'switch',
        label_question: 'Avez-vous fréquemment des crampes ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
    ],
    data: {
      '0': {
        question_id: 'STRESSED',
        required: true,
        ui: 'switch',
        label_question: 'Vous sentez-vous stressée en ce moment ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      '1': {
        question_id: 'ANXIOUS',
        required: true,
        ui: 'switch',
        label_question: 'Vous sentez-vous anxieuse ou angoissée ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      '2': {
        question_id: 'STRESSED_ON_WAKING',
        required: true,
        ui: 'switch',
        label_question:
          "Vous réveillez-vous avec un sentiment d'oppression ou de boule dans le ventre ?",
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      '3': {
        question_id: 'STRESS_PERSONNAL_OR_JOB',
        required: true,
        ui: 'switch',
        label_question:
          'Êtes-vous préoccupée actuellement, au niveau professionnel ou personnel ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      '4': {
        question_id: 'STRESSFUL_EVENT',
        required: false,
        ui: '',
        label_question:
          'Quels évènements parmi ceux-ci avez vous vécu ces douze derniers mois ?',
        value: null,
      },
      '5': {
        question_id: 'STRESSFUL_EVENT_PROFESSIONAL',
        required: true,
        ui: 'checkbox',
        label_question: 'Changement professionnel',
        value: false,
      },
      '6': {
        question_id: 'STRESSFUL_EVENT_MARRIAGE',
        required: true,
        ui: 'checkbox',
        label_question: 'Mariage',
        value: false,
      },
      '7': {
        question_id: 'STRESSFUL_EVENT_DIVORCE',
        required: true,
        ui: 'checkbox',
        label_question: 'Divorce',
        value: false,
      },
      '8': {
        question_id: 'STRESSFUL_EVENT_BEREAVEMENT',
        required: true,
        ui: 'checkbox',
        label_question: 'Deuil',
        value: false,
      },
      '9': {
        question_id: 'STRESSFUL_EVENT_HARASSMENT',
        required: true,
        ui: 'checkbox',
        label_question: 'Harcèlement',
        value: false,
      },
      '10': {
        question_id: 'STRESSFUL_EVENT_MOVE',
        required: true,
        ui: 'checkbox',
        label_question: 'Déménagement',
        value: false,
      },
      '11': {
        question_id: 'STRESSFUL_EVENT_FINANCIAL',
        required: true,
        ui: 'checkbox',
        label_question: 'Problèmes financiers',
        value: false,
      },
      '12': {
        question_id: 'STRESSFUL_EVENT_SURGERY',
        required: true,
        ui: 'checkbox',
        label_question: 'Chirurgie',
        value: false,
      },
      '13': {
        question_id: 'STRESSFUL_EVENT_ACCIDENT',
        required: true,
        ui: 'checkbox',
        label_question: 'Accident',
        value: false,
      },
      '14': {
        question_id: 'DESIRE_TO_EAT',
        required: true,
        ui: 'switch',
        label_question:
          'Avez-vous souvent envie ou besoin de manger au cours de la journée sans sensation de faim ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: true,
      },
      '15': {
        question_id: 'DESIRE_TO_EAT_MOMENT',
        required: true,
        label_question: 'À quel moment de la journée ?',
        ui: 'radio',
        ui_config: {
          choices: [
            {
              label: "Essentiellement en fin d'après midi ou en soirée",
              value: 0,
            },
            {
              label: "Cela peut être n'importe quand dans la journée",
              value: 1,
            },
          ],
        },
        conditions: [
          {
            parentQuestionId: 'DESIRE_TO_EAT',
            operator: '$eq',
            value: true,
          },
        ],
        value: 1,
      },
      '16': {
        question_id: 'SNACKING',
        required: true,
        ui: 'switch',
        label_question:
          'Vous arrive-t-il régulièrement de répondre à cette envie et de grignoter sans sensation de faim ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        conditions: [
          {
            parentQuestionId: 'DESIRE_TO_EAT',
            operator: '$eq',
            value: true,
          },
        ],
        value: true,
      },
      '17': {
        question_id: 'ALCOHOL_RELAXED',
        required: true,
        ui: 'switch',
        label_question:
          "Vous sentez-vous détendue après avoir bu de l'alcool ?",
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        conditions: [
          {
            parentQuestionId: 'ALCOHOL',
            operator: '$eq',
            value: true,
          },
          {
            parentQuestionId: 'ALCOHOL_WEEKLY_QTY',
            operator: '$gte',
            value: 14,
          },
        ],
        conditions_assoc: 'AND',
        value: true,
      },
      '18': {
        question_id: 'MED_SPASMOPHILIA',
        required: true,
        ui: 'switch',
        label_question:
          'Avez-vous des épisodes de tétanie ou de spasmophilie ?',
        tooltip: 'Contractions importantes et prolongées des muscles.',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      '19': {
        question_id: 'MED_TINNITUS',
        required: true,
        ui: 'switch',
        label_question: 'Avez-vous des acouphènes ?',
        tooltip:
          "Sensation auditive de bourdonnement ou tintement qui n'est pas causée par un son extérieur.",
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      '20': {
        question_id: 'MED_PALPITATIONS',
        required: true,
        ui: 'switch',
        label_question: 'Faites-vous régulièrement de la tachycardie ?',
        tooltip:
          'Trouble du rythme cardiaque qui consiste en un accélération des battements du coeur (plus de 100 battements par minute).',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
      '21': {
        question_id: 'MED_HAIR_LOSS',
        required: true,
        ui: 'switch',
        label_question: 'Avez-vous constaté une perte anormale de cheveux ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: true,
      },
      '22': {
        question_id: 'MED_CRAMPS',
        required: true,
        ui: 'switch',
        label_question: 'Avez-vous fréquemment des crampes ?',
        ui_config: {
          choices: [
            {
              label: 'Oui',
              value: true,
            },
            {
              label: 'Non',
              value: false,
            },
          ],
        },
        value: false,
      },
    },
  },
};
