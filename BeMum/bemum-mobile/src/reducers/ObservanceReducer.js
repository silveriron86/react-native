import {ObservanceConstants} from '../constants';

const initialState = {
  observances: null,
  observance: null,
  error: null,
};

function ObservanceReducer(state = initialState, action) {
  switch (action.type) {
    case ObservanceConstants.GET_OBSERVANCE_LIST_SUCCESS:
      return Object.assign({}, state, {
        observances: action.response,
        error: null,
      });

    case ObservanceConstants.GET_OBSERVANCE_LIST_ERROR:
      return Object.assign({}, state, {
        observances: null,
        error: action.error,
      });

    case ObservanceConstants.GET_OBSERVANCE_SUCCESS:
      return Object.assign({}, state, {
        observance: action.response,
        error: null,
      });

    case ObservanceConstants.GET_OBSERVANCE_ERROR:
      return Object.assign({}, state, {
        observance: null,
        error: action.error,
      });

    default:
      return state;
  }
}

export default ObservanceReducer;
