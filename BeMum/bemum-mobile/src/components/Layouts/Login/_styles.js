import {StyleSheet} from 'react-native';
import {Colors} from '../../../constants';
// import {MAIN_BACKGROUND} from '../../../constants/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: Colors.theme.LIGHT,
    // backgroundColor: MAIN_BACKGROUND,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  logoWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 98,
  },
  imgLogo: {
    height: 55,
    resizeMode: 'contain',
    // tintColor: Colors.theme.DARK,
  },
  inputWrapper: {
    width: '100%',
    minHeight: 45 + 24,
  },
  input: {
    height: 45,
    color: 'black',
    paddingHorizontal: 27,
    borderWidth: 1,
    borderColor: Colors.DARK_GREY,
    borderRadius: 25,
  },
  inputError: {
    color: 'red',
    marginTop: -8,
  },
  btnWrapper: {
    width: '100%',
    alignItems: 'center',
    marginTop: 25,
  },
  button: {
    backgroundColor: Colors.theme.DARK,
    borderColor: 'transparent',
    borderRadius: 24,
    height: 47,
    width: '100%',
    paddingHorizontal: 20,
    // paddingVertical: 10,
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
    // shadowColor: 'black',
    // shadowOpacity: 0.2,
    // shadowOffset: {width: 2, height: 4},
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    color: 'white',
    fontSize: 16,
  },
  screenBackground: {
    width: '100%',
    flex: 1,
  },
  forgotDescText: {
    color: 'black',
    fontSize: 15,
    width: '100%',
  },
  msgWrapper: {
    marginTop: 20,
  },
  msg: {
    color: '#FC706F',
    fontSize: 13,
    lineHeight: 15,
  },
  buttonView: {
    marginTop: -14,
    alignItems: 'flex-end',
    width: '100%',
  },
  // btnLostkey: {
  //   alignItems: 'flex-end',
  //   borderWidth: 1,
  // },
});
