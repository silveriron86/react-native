/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {ScrollView, View, TouchableOpacity, Text} from 'react-native';
import moment from 'moment';
import 'moment/locale/fr';
import Utils from '../../../../../utils';
import styles from '../_styles';

export default class DatesSelector extends React.Component {
  constructor(props) {
    super(props);
    // moment.locale('fr');
  }

  render() {
    const {dates, onSelect} = this.props;
    let btnCols = [];
    for (let i = 0; i < 9; i++) {
      let dt = moment().add(i, 'day');
      let dateText = dt.format('DD/MM');
      let weekDayText = dt.format('dddd');
      let selected = dates.indexOf(dt.format('YYYY-MM-DD')) >= 0;
      weekDayText = Utils.capitalize(weekDayText);

      if (i === 0) {
        weekDayText = 'Aujourd’hui';
      } else if (i === 1) {
        weekDayText = 'Demain';
      }

      btnCols.push(
        <TouchableOpacity
          key={`date-${i}`}
          style={[
            styles.dateSelBtn,
            selected ? styles.selectedDateSelBtn : null,
            i === 8 && {marginRight: 45},
          ]}
          onPress={() => onSelect(dt)}>
          {i > 1 ? (
            <Text style={[styles.dateText, selected ? {color: 'white'} : null]}>
              {dateText}
            </Text>
          ) : (
            <Text style={[styles.dateText, selected ? {color: 'white'} : null]}>
              {weekDayText}
            </Text>
          )}
        </TouchableOpacity>,
      );
    }

    return (
      <View style={styles.dateSelContainer}>
        <Text style={{marginBottom: 10, fontFamily: 'Poppins-Regular'}}>
          Ajout des produits pour :
        </Text>
        <ScrollView horizontal={true} style={styles.dateSelWrapper}>
          <View style={{flexDirection: 'row'}}>{btnCols}</View>
        </ScrollView>
      </View>
    );
  }
}
