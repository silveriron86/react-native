import React from 'react';
import {View, Text} from 'react-native';
import commonStyles from '../../Styles';

export default class GeneralUseConditionScreen extends React.Component {
  render() {
    return (
      <View style={commonStyles.container}>
        <Text>Condition générale d'utilisation</Text>
      </View>
    );
  }
}
