import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    padding: 0,
  },
  links: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  profileLink: {
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
    width: '100%',
    padding: 24,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  linkText: {
    color: '#333',
    fontSize: 18,
  },
  logoutButton: {
    margin: 20,
  },
  notificationRow: {
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'center',
    paddingVertical: 22,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#EAEAEA',
    width: '100%',
    paddingHorizontal: 20,
  },
  arrowText: {
    fontSize: 30,
    color: '#888',
    marginTop: -4,
    marginLeft: 15,
  },
});
