/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {SafeAreaView, ScrollView, Text, Image, View} from 'react-native';
import {connect} from 'react-redux';
import commonStyles from '../../../Styles';
import ImageProgress from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import {PrimaryButton, LoadingOverlay, MyAlert} from '../../../../Widgets';
import {Calories} from '../_widgets';
import styles from '../_styles';
import Utils from '../../../../../utils';
import LikesButtons from '../../../../Widgets/Buttons/LikesButtons';
import {
  RecipeActions,
  MealPlanningActions,
  AccountActions,
} from '../../../../../actions';
import ActionButtons from '../../Courses/_widgets/ActionButtons';

const circleIcon = require('../../../../../../assets/icons/choix2.png');
const userIcon = require('../../../../../../assets/icons/tab4.png');

class AlimentationMenuDetailsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.alert = React.createRef();
    this.state = {
      loading: props.selected ? true : false,
      recipe: null,
    };
  }

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  onChoose = () => {
    const {mealPlanning, route} = this.props;
    const {data, type} = route.params;
    console.log('*** Menu Details = ', mealPlanning, data, type);
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.updateMealPlanning({
          mealPlanningId: mealPlanning.id,
          data:
            type === 'lunch'
              ? {selectedLunch: 'REGULAR'}
              : {selectedDiner: 'REGULAR'},
          cb: res => {
            console.log(res);
            this.setState(
              {
                loading: false,
              },
              () => {
                if (res.success) {
                  global.eventEmitter.emit('UPDATE_MEALPLANNING', {
                    currentDate: '',
                  });
                  this.goBack();
                } else {
                  this.alert.current.show('Erreur', 'Une erreur est survenue');
                }
              },
            );
          },
        });
      },
    );
  };

  componentDidMount() {
    Utils.logEvent({
      screenName: 'AlimentationMenuDetails',
      className: 'AlimentationMenuDetailsScreen',
    });

    const {selected} = this.props;
    // const {data} = this.props.route.params;
    // console.log('<', selected, ' : ', data, '>');
    if (selected) {
      setTimeout(() => {
        this.props.getRecipe({
          recipeId: selected.id,
          cb: res => {
            console.log('**************', res);
            this.setState({
              loading: false,
              recipe: res,
            });
          },
        });
      }, 1000);
    }
  }

  onDislike = index => {
    const {recipe} = this.state;
    if (index === 2) {
      this.props.navigation.navigate('FoodExclusion');
    } else {
      let data = null;
      if (index === 3) {
        data = {
          reason: 'too-frequent',
        };
      } else if (index === 4) {
        data = {
          reason: 'badly-written',
        };
      }
      this.setState(
        {
          loading: true,
        },
        () => {
          this.props.dislike({
            recipeId: recipe.id,
            data,
            cb: res => {
              console.log(res);
              this.setState(
                {
                  loading: false,
                },
                () => {
                  const alreadyDisliked =
                    typeof res.error !== 'undefined' &&
                    res.error.response.status === 409;
                  const success = res.success;

                  if (alreadyDisliked) {
                    this.alert.current.show(
                      'Déjà enregistré',
                      'Les changements seront visibles sur vos prochains menus.',
                    );
                  } else {
                    this.alert.current.show(
                      success ? 'Compris !' : 'Une erreur est survenue',
                      success
                        ? 'Cette recette ne vous sera plus proposée.'
                        : "Nous n'avons pas pu prendre en compte votre demande.",
                    );
                  }
                },
              );
            },
          });
        },
      );
    }
  };

  render() {
    const {loading, recipe} = this.state;
    const {selected} = this.props;
    console.log('***** selected', selected);
    // const {data} = this.props.route.params;
    let ingredients = [];
    let steps = [];
    if (recipe) {
      recipe.ingredients.forEach(ing => {
        // ingredients.push(`${ing.ingredient.name} ${ing.qty} ${ing.unit.name}`);
        ingredients.push(ing);
      });
      if (recipe.steps) {
        steps = JSON.parse(recipe.steps);
      }
    }
    console.log('* menu details');

    return (
      <>
        <ScrollView style={commonStyles.flex1}>
          <SafeAreaView style={commonStyles.container}>
            <View style={{padding: 24, paddingTop: 40}}>
              {recipe !== null && (
                <ImageProgress
                  source={{
                    uri:
                      recipe !== '' && recipe !== null ? recipe.imageUrl : '',
                  }}
                  indicator={ProgressBar}
                  style={styles.choixColImg}>
                  <LikesButtons isDetail={false} onDislike={this.onDislike} />
                </ImageProgress>
              )}
              <Text
                style={[styles.headerTitle, {marginTop: 0, textAlign: 'left'}]}>
                {selected ? selected.name : ''}
              </Text>
              {recipe !== null && (
                <>
                  <View style={[styles.headerInfos, {marginTop: 20}]}>
                    <Image source={circleIcon} style={styles.infoIcons} />
                    <Text style={styles.infoText}>
                      {recipe.timings.preparation / 60 / 1000} min
                    </Text>
                    <View style={styles.w20} />
                    <Image source={userIcon} style={styles.infoIcons} />
                    <Text style={styles.infoText}>{recipe.servings} pers</Text>
                  </View>
                  <Calories title="" recipe={recipe} />
                  <View style={styles.section}>
                    <Text style={commonStyles.H3}>
                      {'Ingrédients'.toUpperCase()}
                    </Text>
                    {ingredients.map((ingre, index) => (
                      <ActionButtons
                        key={`ingre-${index}`}
                        defaultValue={ingre.qty}
                        name={ingre.ingredient.name}
                        unit={ingre.unit ? ingre.unit.name : ''}
                        readOnly={true}
                      />
                    ))}
                  </View>
                  <View style={[styles.section]}>
                    <Text style={commonStyles.H3}>INSTRUCTIONS</Text>
                    {steps.map((inst, index) => (
                      <View style={styles.instRow} key={`inst-${index}`}>
                        <Text style={commonStyles.H1}>0{index + 1}</Text>
                        <View style={styles.w20} />
                        <View style={{flex: 1}}>
                          <Text style={[commonStyles.TextSecondary]}>
                            {inst.instructions}
                          </Text>
                        </View>
                      </View>
                    ))}
                  </View>
                  <View>
                    <PrimaryButton
                      onPress={this.onChoose}
                      title="Choisir ce menu"
                    />
                  </View>
                </>
              )}
            </View>
          </SafeAreaView>
        </ScrollView>
        <LoadingOverlay loading={loading} />
        <MyAlert ref={this.alert} />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    mealPlanning: state.MealPlanningReducer.mealPlanning,
    recipe: state.RecipeReducer.recipe,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRecipe: req => dispatch(RecipeActions.get(req.recipeId, req.cb)),
    dislike: req =>
      dispatch(RecipeActions.dislike(req.recipeId, req.data, req.cb)),
    updateMealPlanning: req =>
      dispatch(
        MealPlanningActions.update(req.mealPlanningId, req.data, req.cb),
      ),
    updateIngredientsToAvoid: req =>
      dispatch(AccountActions.updateIngredientsToAvoid(req.data, req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AlimentationMenuDetailsScreen);
