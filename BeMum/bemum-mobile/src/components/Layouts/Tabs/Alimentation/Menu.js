/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ImageProgress from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import commonStyles from '../../Styles';
import styles from './_styles';
import {RecipeActions} from '../../../../actions';
import Utils from '../../../../utils';

class AlimentationMenuScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      appetizerRecipe: null,
      dishRecipe: null,
      dessertRecipe: null,
    };
  }

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  onChoose = () => {
    this.goBack();
  };

  componentDidMount() {
    Utils.logEvent({
      screenName: 'AlimentationMenu',
      className: 'AlimentationMenuScreen',
    });

    const {type, mealPlanning} = this.props.route.params;
    const appetizer = `${type}Appetizer`;
    const dish = `${type}Dish`;
    const dessert = `${type}Dessert`;

    if (mealPlanning[appetizer] !== '' && mealPlanning[appetizer] !== null) {
      this._getRecipe(mealPlanning[appetizer].id, 'appetizer');
    }

    if (mealPlanning[dish] !== '' && mealPlanning[dish] !== null) {
      this._getRecipe(mealPlanning[dish].id, 'dish');
    }

    if (mealPlanning[dessert] !== '' && mealPlanning[dessert] !== null) {
      this._getRecipe(mealPlanning[dessert].id, 'dessert');
    }
  }

  _getRecipe = (recipeId, type) => {
    this.props.getRecipe({
      recipeId,
      cb: res => {
        let state = {...this.state};
        state[`${type}Recipe`] = res;
        this.setState(state);
      },
    });
  };

  goMenuDetail = (name, data) => {
    const {navigation} = this.props;
    const {type, mealPlanning} = this.props.route.params;
    const appetizer = `${type}Appetizer`;
    const dish = `${type}Dish`;
    const dessert = `${type}Dessert`;

    navigation.navigate('AlimentationMenuDetails', {
      routeName: name,
      data,
      type,
      recipes: [
        mealPlanning[appetizer],
        mealPlanning[dish],
        mealPlanning[dessert],
      ],
    });
  };

  render() {
    // const {navigation} = this.props;
    const {dessertRecipe, dishRecipe, appetizerRecipe} = this.state;
    const {type, mealPlanning} = this.props.route.params;
    // const insetTop = Platform.OS === 'android' ? 0 : Utils.getInsetTop();

    const appetizer = `${type}Appetizer`;
    const dish = `${type}Dish`;
    const dessert = `${type}Dessert`;
    // console.log('* menu screen = ', mealPlanning, type, dessert);
    return (
      <ScrollView style={commonStyles.flex1}>
        {/* <Header
          style={[
            styles.headerBg,
            {height: insetTop > 20 ? 100 + insetTop : 120},
          ]}>
          <Text
            style={[
              commonStyles.H1,
              {textAlign: 'center'},
              insetTop > 20 && {marginTop: insetTop - 8},
            ]}>
            Menu du déjeuner
          </Text>
          <BackButton
            navigation={navigation}
            style={insetTop > 20 && {top: insetTop}}
          />

          <View
            style={[
              styles.headerInfos,
              {justifyContent: 'center', marginTop: 10},
            ]}>
            <Image source={circleIcon} style={styles.infoIcons} />
            <Text style={styles.infoText}>15 min</Text>
            <View style={styles.w20} />
            <Image source={userIcon} style={styles.infoIcons} />
            <Text style={styles.infoText}>2 pers</Text>
          </View>
        </Header> */}
        <SafeAreaView style={commonStyles.container}>
          <View style={{padding: 24, paddingTop: 0}}>
            {/* <Calories title="" /> */}
            {mealPlanning[appetizer] !== '' &&
              mealPlanning[appetizer] !== null && (
                <>
                  <Text
                    style={[
                      commonStyles.H2,
                      styles.alimentTitle,
                      {marginTop: 40},
                    ]}>
                    Entrée
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      this.goMenuDetail(
                        'EntreeMenuStack',
                        mealPlanning[appetizer],
                      )
                    }>
                    <ImageProgress
                      source={{
                        uri: mealPlanning[appetizer].imageUrl,
                      }}
                      indicator={ProgressBar}
                      style={styles.choixColImg}
                    />
                  </TouchableOpacity>
                  <View>
                    {/* <LikesButtons style={{marginTop: -35, marginRight: -20}} /> */}
                    <Text style={styles.choixColTitle}>
                      {mealPlanning[appetizer].name}
                    </Text>
                    {appetizerRecipe !== null && (
                      <Text style={styles.choixColValue}>
                        Preparation{' '}
                        {appetizerRecipe.timings.preparation / 60 / 1000} min,
                        cuisson {appetizerRecipe.timings.cooking / 60 / 1000}{' '}
                        min
                      </Text>
                    )}
                  </View>
                </>
              )}

            {mealPlanning[dish] !== '' && mealPlanning[dish] !== null && (
              <>
                <Text
                  style={[
                    commonStyles.H2,
                    styles.alimentTitle,
                    {marginTop: 40},
                  ]}>
                  Plat
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.goMenuDetail('PlatMenuStack', mealPlanning[dish])
                  }>
                  <ImageProgress
                    source={{
                      uri: mealPlanning[dish].imageUrl,
                    }}
                    indicator={ProgressBar}
                    style={styles.choixColImg}
                  />
                </TouchableOpacity>
                <View>
                  {/* <LikesButtons style={{marginTop: -35, marginRight: -20}} /> */}
                  <Text style={styles.choixColTitle}>
                    {mealPlanning[dish].name}
                  </Text>
                  {dishRecipe !== null && (
                    <Text style={styles.choixColValue}>
                      Preparation {dishRecipe.timings.preparation / 60 / 1000}{' '}
                      min, cuisson {dishRecipe.timings.cooking / 60 / 1000} min
                    </Text>
                  )}
                </View>
              </>
            )}

            {mealPlanning[dessert] !== '' && mealPlanning[dessert] !== null && (
              <>
                <Text
                  style={[
                    commonStyles.H2,
                    styles.alimentTitle,
                    {marginTop: 40},
                  ]}>
                  Dessert
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.goMenuDetail('DessertMenuStack', mealPlanning[dessert])
                  }>
                  <ImageProgress
                    source={{
                      uri: mealPlanning[dessert].imageUrl,
                    }}
                    indicator={ProgressBar}
                    style={styles.choixColImg}
                  />
                </TouchableOpacity>

                <View>
                  {/* <LikesButtons style={{marginTop: -35, marginRight: -20}} /> */}
                  <Text style={styles.choixColTitle}>
                    {mealPlanning[dessert].name}
                  </Text>
                  {dessertRecipe !== null && (
                    <Text style={styles.choixColValue}>
                      Preparation{' '}
                      {dessertRecipe.timings.preparation / 60 / 1000} min,
                      cuisson {dessertRecipe.timings.cooking / 60 / 1000} min
                    </Text>
                  )}
                </View>
              </>
            )}

            {/* <View>
              <PrimaryButton
                onPress={this.onChoose}
                title="Choisir ce menu"
                style={{marginTop: 40}}
              />
            </View> */}
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    recipe: state.RecipeReducer.recipe,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRecipe: req => dispatch(RecipeActions.get(req.recipeId, req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AlimentationMenuScreen);
