/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text} from 'react-native';
import styles from '../_styles';
import commonStyles from '../../../Styles';

export default class Calories extends React.Component {
  render() {
    // let vitamins = [];
    const {title, recipe} = this.props;
    let kcal = 0;
    let lipides = 0;
    let proteines = 0;
    let glucides = 0;
    if (recipe) {
      if (recipe.caloriesPerServing) {
        kcal = recipe.caloriesPerServing;
      }
      if (recipe.nutrients) {
        // vitamins = [];
        recipe.nutrients.forEach(v => {
          if (v.nutrient.name === 'Protéines') {
            proteines = v.grams;
          } else if (v.nutrient.name === 'Lipides') {
            lipides = v.grams;
          }
          if (v.nutrient.name === 'Glucides') {
            glucides = v.grams;
          }
          // if (v.nutrient.name && v.nutrient.name.indexOf('Vitamine') >= 0) {
          //   vitamins.push(v.nutrient.name);
          // }
        });
      }
    }

    return (
      <View style={[styles.section, {marginBottom: 0}]}>
        {title !== '' && <Text style={commonStyles.H3}>{title}</Text>}
        <View style={styles.valeurs}>
          <View style={styles.valeurCol}>
            <Text style={commonStyles.Legend}>Calories</Text>
            <Text style={[commonStyles.H1, commonStyles.textCenter]}>
              {parseInt(kcal, 10)}
            </Text>
          </View>
          <View style={styles.valeurCol}>
            <Text style={commonStyles.Legend}>Lipides</Text>
            <Text style={[commonStyles.H1, commonStyles.textCenter]}>
              {parseInt(lipides, 10)}
              <Text style={commonStyles.Legend}>g</Text>
            </Text>
          </View>
          <View style={styles.valeurCol}>
            <Text style={commonStyles.Legend}>Protéines</Text>
            <Text style={[commonStyles.H1, commonStyles.textCenter]}>
              {parseInt(proteines, 10)}
              <Text style={commonStyles.Legend}>g</Text>
            </Text>
          </View>
          <View style={styles.valeurCol}>
            <Text style={commonStyles.Legend}>Glucides</Text>
            <Text style={[commonStyles.H1, commonStyles.textCenter]}>
              {parseInt(glucides, 10)}
              <Text style={commonStyles.Legend}>g</Text>
            </Text>
          </View>
        </View>

        {/* <View style={styles.vitamins}>
          {vitamins.map((v, index) => (
            <View style={styles.vitaminCol} key={`vitamin-${index}`}>
              <Text style={[commonStyles.Legend, {color: Colors.theme.DARK}]}>
                {v}
              </Text>
            </View>
          ))}
        </View> */}
      </View>
    );
  }
}
