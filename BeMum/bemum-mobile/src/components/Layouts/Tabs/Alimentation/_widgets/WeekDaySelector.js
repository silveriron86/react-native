/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, TouchableOpacity, Text, Platform, Animated} from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';
import {CalendarList, LocaleConfig} from 'react-native-calendars';
import moment from 'moment';
import 'moment/locale/fr';
import commonStyles from '../../../Styles.js';
import styles from '../_styles';
import Colors from '../../../../../constants/Colors';

LocaleConfig.locales.fr = {
  monthNames: [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre',
  ],
  monthNamesShort: [
    'Janv.',
    'Févr.',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juil.',
    'Août',
    'Sept.',
    'Oct.',
    'Nov.',
    'Déc.',
  ],
  dayNames: [
    'Dimanche',
    'Lundi',
    'Mardi',
    'Mercredi',
    'Jeudi',
    'Vendredi',
    'Samedi',
  ],
  dayNamesShort: ['DIM', 'LUN', 'MAR', 'MER', 'JEU', 'VEN', 'SAM'],
  today: "Aujourd'hui",
};
LocaleConfig.defaultLocale = 'fr';

export default class WeekDaySelector extends React.Component {
  constructor(props) {
    super(props);
    moment.locale('fr');
    this.animatedValue = new Animated.Value(120);
    this.scaleAnimation = new Animated.Value(120 / 326);
    this.moving = false;
    this.prevY = 0;
    this.state = {
      isCalendarOpened: false,
      startDate: moment().startOf('week'),
    };
  }

  toggleExpand = () => {
    this.setState({
      isCalendarOpened: !this.state.isCalendarOpened,
    });
  };

  onSelectDate = date => {
    const newDate = moment(date.dateString);
    const selectedDate = newDate;
    this.setState(
      {
        // isCalendarOpened: false,
        startDate: newDate.clone().startOf('week'),
      },
      () => {
        this.props.onSelect(selectedDate);
        this.onSwipeUp();
      },
    );
  };

  onSwipeUp = () => {
    if (!this.state.isCalendarOpened) {
      return;
    }
    if (Platform.OS === 'ios') {
      this.moving = true;
      Animated.timing(this.animatedValue, {
        toValue: 120,
        // delay: 300,
        duration: 400,
        useNativeDriver: false,
      }).start();

      setTimeout(() => {
        this.toggleExpand();
        this.moving = false;
      }, 400);
    } else {
      this.toggleExpand();
    }
  };

  onSwipeDown = () => {
    if (this.state.isCalendarOpened) {
      return;
    }

    this.toggleExpand();
    if (Platform.OS === 'ios') {
      this.moving = true;
      Animated.timing(this.animatedValue, {
        toValue: 326,
        // delay: 300,
        duration: 400,
        useNativeDriver: false,
      }).start();
      setTimeout(() => {
        this.moving = false;
      }, 400);
    }
  };

  onResponderStart = evt => {
    console.log('start');
    if (Platform.OS === 'android') {
      return;
    }

    this.moving = false;
    // this.props.setTouchStart();
    this.prevY = evt.nativeEvent.locationY;
  };

  onResponderMove = evt => {
    console.log('move', this.moving, this.prevY, evt.nativeEvent.locationY);
    if (Platform.OS === 'android' || this.moving) {
      return;
    }

    if (this.prevY > evt.nativeEvent.locationY) {
      // swipe up
      this.onSwipeUp();
    } else if (this.prevY === evt.nativeEvent.locationY) {
      // for android
    } else {
      // swipe down
      this.onSwipeDown();
    }
  };

  onResponderRelease = () => {
    if (Platform.OS === 'android') {
      return;
    }
    console.log('end');
    // this.props.setTouchEnd();
    this.prevY = 0;
  };

  _renderToggleBtn = () => {
    const btnWrapper = (
      <View
        style={{
          width: 80,
          height: 30,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            backgroundColor: '#FCEBE8',
            height: 8,
            width: '100%',
            borderRadius: 5,
          }}
        />
      </View>
    );

    if (Platform.OS === 'android') {
      return (
        <GestureRecognizer
          onSwipeUp={state => this.onSwipeUp(state)}
          onSwipeDown={state => this.onSwipeDown(state)}
          config={{
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80,
          }}
          style={styles.calendarExpandBtn}>
          {btnWrapper}
        </GestureRecognizer>
      );
    }

    return (
      <View
        style={styles.calendarExpandBtn}
        onStartShouldSetResponder={() => true}
        onResponderStart={this.onResponderStart}
        onResponderMove={this.onResponderMove}
        onResponderRelease={this.onResponderRelease}>
        {btnWrapper}
      </View>
    );
  };

  render() {
    const WEEKS = ['DIM', 'LUN', 'MAR', 'MER', 'JEU', 'VEN', 'SAM'];
    const {currentDate, onSelect} = this.props;
    const {startDate, isCalendarOpened} = this.state;
    const currentWDay = currentDate.format('D');

    let cols = [];

    for (let i = 0; i < 7; i++) {
      const dt = moment(startDate).add(i, 'day');
      const weekDay = dt.format('d');
      const selected = currentWDay === dt.format('D');

      cols.push(
        <TouchableOpacity
          underlayColor="#fff"
          onPress={() => onSelect(dt)}
          key={`date_${i}`}
          style={[styles.dateCol, selected && styles.selectedDateBtn]}>
          <Text style={commonStyles.smallText}>{WEEKS[weekDay]}</Text>
          <View style={styles.dateBtn}>
            <Text
              style={[
                styles.dateBtnText,
                selected && styles.selectedDateBtnText,
              ]}>
              {dt.format('D')}
            </Text>
          </View>
        </TouchableOpacity>,
      );
    }

    const selectedDot = {
      key: 'selected',
      color: 'red',
      selectedDotColor: 'blue',
    };
    const markedDates = {};
    markedDates[currentDate.format('YYYY-MM-DD')] = {
      dots: [selectedDot],
      selected: true,
      selectedColor: Colors.DARK_LINEN,
    };

    const selectorWrapper = (
      <>
        <View
          style={[
            styles.wdContainer,
            {
              height: isCalendarOpened === true ? 310 : 'auto',
              overflow: 'hidden',
            },
          ]}>
          {!isCalendarOpened && (
            <View
              style={{
                paddingHorizontal: 20,
                zIndex: 1,
                backgroundColor: 'white',
                paddingBottom: 32,
              }}>
              {/* <Text style={styles.weekInfo}>Juillet 2020 - Semaine 2</Text> */}
              <View style={[styles.dateSelector]}>{cols}</View>
            </View>
          )}

          {/* {!isCalendarOpened && this._renderToggleBtn()} */}
          {/* {isCalendarOpened && ( */}
          <View
            style={{
              position: 'absolute',
              backgroundColor: 'white',
              marginTop: 0,
              zIndex: 0,
              width: '100%',
            }}>
            <CalendarList
              // Callback which gets executed when visible months change in scroll view. Default = undefined
              onVisibleMonthsChange={months => {
                console.log('now these months are visible', months);
              }}
              // Max amount of months allowed to scroll to the past. Default = 50
              pastScrollRange={50}
              // Max amount of months allowed to scroll to the future. Default = 50
              futureScrollRange={50}
              // Enable or disable scrolling of calendar list
              scrollEnabled={true}
              // Enable or disable vertical scroll indicator. Default = false
              showScrollIndicator={true}
              onDayPress={this.onSelectDate}
              firstDay={1}
              selected={startDate.format('YYYY-MM-DD')}
              current={startDate.format('YYYY-MM-DD')}
              style={{
                height: 280,
                width: '100%',
              }}
              markedDates={markedDates}
              enableSwipeMonths={true}
            />
          </View>
          {/* )} */}
        </View>
        {this._renderToggleBtn()}
      </>
    );

    if (Platform.OS === 'android') {
      return (
        <View
          style={{
            height: isCalendarOpened ? 326 : 120,
            overflow: 'hidden',
            position: 'relative',
          }}>
          {selectorWrapper}
        </View>
      );
    }

    return (
      <Animated.View
        style={{
          height: this.animatedValue,
          overflow: 'hidden',
          position: 'relative',
        }}>
        {selectorWrapper}
      </Animated.View>
    );
  }
}
