/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {ScrollView, Image, View, Text} from 'react-native';
import Unorderedlist from 'react-native-unordered-list';
import moment from 'moment';
import commonStyles from '../../Styles';
import {PrimaryButton, MyAlert} from '../../../Widgets';
import styles from './_styles';

import choixIcon1 from '../../../../../assets/icons/pour1.png';
import choixIcon2 from '../../../../../assets/icons/pour2.png';
import choixIcon3 from '../../../../../assets/icons/pour3.png';
import groupImage from '../../../../../assets/icons/pours_group.png';
import {MealPlanningActions} from '../../../../actions';
import Utils from '../../../../utils';
// import Utils from '../../../../utils';

class AlimentationOutsideScreen extends React.Component {
  constructor(props) {
    super(props);
    this.alert = React.createRef();
    this.state = {
      loading: false,
      currentDate: moment(),
    };
  }

  componentDidMount() {
    Utils.logEvent({
      screenName: 'AlimentationOutside',
      className: 'AlimentationOutsideScreen',
    });
  }

  loadData = () => {
    //
  };

  onSelectDate = newDate => {
    if (newDate === null) {
      newDate = moment();
    }

    this.setState({currentDate: newDate}, () => {
      this.loadData();
    });
  };

  goDetail = () => {
    const {navigation} = this.props;
    navigation.navigate('AlimentationDetails');
  };

  goMenu = () => {
    const {navigation} = this.props;
    navigation.navigate('AlimentationMenu');
  };

  _onPress = () => {};

  onChoose = () => {
    const {mealPlanning, route} = this.props;
    const {data} = route.params;
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.updateMealPlanning({
          mealPlanningId: mealPlanning.id,
          data,
          cb: res => {
            this.setState(
              {
                loading: false,
              },
              () => {
                if (res.success) {
                  global.eventEmitter.emit('UPDATE_MEALPLANNING', {
                    currentDate: '',
                  });
                  this.props.navigation.goBack();
                } else {
                  this.alert.current.show('Erreur', 'Une erreur est survenue');
                }
              },
            );
          },
        });
      },
    );
  };

  render() {
    return (
      <ScrollView style={{backgroundColor: 'white'}}>
        <View style={[commonStyles.wrapper, {paddingTop: 0}]}>
          {/* <Text style={[commonStyles.H3, styles.outsideTitle]}>
            {'Objectifs du repas'.toUpperCase()}
          </Text> */}
          {/* <View style={{marginBottom: 20}}>
            {listData.map((item, index) => (
              <View key={`outside-col-${index}`}>
                <View style={[styles.choixCol, styles.outsideCol]}>
                  <Text style={[commonStyles.H1, styles.outsideNumber]}>
                    {Utils.padNumber(index + 1, 2)}
                  </Text>
                  <Text style={[styles.choixColTitle, commonStyles.textCenter]}>
                    {item.title}
                  </Text>
                  <Text style={[styles.choixColValue, commonStyles.textCenter]}>
                    {item.description}
                  </Text>
                </View>
              </View>
            ))}
          </View> */}

          <Text
            style={[
              commonStyles.Text,
              styles.outsideDescription,
              {marginTop: 25},
            ]}>
            Quel que soit l’endroit où vous allez prendre votre repas, l’idéal
            est que vous y trouviez l’association des composantes suivantes :
          </Text>

          <Text style={[commonStyles.H2, styles.alimentTitle, {marginTop: 30}]}>
            Pour l’apéro ou l’entrée
          </Text>
          <View style={[styles.flexMealBtn, {alignItems: 'flex-start'}]}>
            <View style={styles.outsideIconWrapper}>
              <Image source={choixIcon1} style={styles.choixIcon} />
            </View>
            <View style={styles.w20} />
            <View style={commonStyles.flex1}>
              <Text
                style={[
                  commonStyles.H4,
                  commonStyles.textLeft,
                  commonStyles.mt0,
                ]}>
                Privilégiez le non-transformé
              </Text>
              <Text style={[styles.choixColValue, commonStyles.textLeft]}>
                Évitez les plateaux de fromages ou charcuteries, et les
                grignotages de gâteaux apéritifs qui sont trop gras et salés.
              </Text>
            </View>
          </View>
          <Text
            style={[
              styles.choixColValue,
              commonStyles.textLeft,
              {marginTop: -5},
            ]}>
            Si vous en avez à disposition, privilégiez les oléagineux non salés,
            pour un apport en lipides de bonne qualité, ou les crudités, afin de
            faire le plein de minéraux et de vitamines.
          </Text>

          {/* <View
            style={{backgroundColor: '#E7E7E7', borderRadius: 15, height: 150}}
          /> */}

          <Text style={[commonStyles.H2, styles.alimentTitle, {marginTop: 30}]}>
            Pour le plat
          </Text>
          <View style={[styles.flexMealBtn, {alignItems: 'flex-start'}]}>
            <View style={styles.outsideIconWrapper}>
              <Image source={choixIcon2} style={styles.choixIcon} />
            </View>
            <View style={styles.w20} />
            <View style={commonStyles.flex1}>
              <Text
                style={[
                  commonStyles.H4,
                  commonStyles.textLeft,
                  commonStyles.mt0,
                ]}>
                Pensez 3/3
              </Text>
              <Text style={[styles.choixColValue, commonStyles.textLeft]}>
                Votre assiette doit idéalement être divisée en trois parties
                égales :
              </Text>
            </View>
          </View>

          <Unorderedlist>
            <Text
              style={[
                styles.choixColValue,
                commonStyles.textLeft,
                {marginTop: 0},
              ]}>
              1/3 de protéines : privilégiez les légumineuses, les viandes
              blanches ou le poisson.
            </Text>
          </Unorderedlist>
          <View style={{marginVertical: 10}}>
            <Unorderedlist>
              <Text
                style={[
                  styles.choixColValue,
                  commonStyles.textLeft,
                  {marginTop: 0},
                ]}>
                1/3 de légumes : pour un apport en fibres et vitamines.
              </Text>
            </Unorderedlist>
          </View>
          <Unorderedlist>
            <Text
              style={[
                styles.choixColValue,
                commonStyles.textLeft,
                {marginTop: 0},
              ]}>
              1/3 de féculents - produits céréaliers : idéalement complets.
              Attention aux quantités : limitez-vous à un féculent par repas,
              soit dans l’assiette, soit sous la forme de pain.
            </Text>
          </Unorderedlist>

          <Image
            source={groupImage}
            style={{
              marginTop: 30,
              width: '100%',
              height: 200,
              resizeMode: 'contain',
            }}
          />
          <Text
            style={[
              styles.choixColValue,
              commonStyles.textLeft,
              {marginTop: 10},
            ]}>
            Évitez les plats frits, marinés ou en sauce.
          </Text>
          <Text style={[styles.choixColValue, commonStyles.textLeft]}>
            Attention également aux sauces (ketchup, mayonnaise...), souvent
            trop grasses et sucrées. Évitez de re-saler votre assiette.
          </Text>

          <Text style={[commonStyles.H2, styles.alimentTitle, {marginTop: 40}]}>
            Pour le dessert
          </Text>
          <View style={[styles.flexMealBtn, {alignItems: 'flex-start'}]}>
            <View style={styles.outsideIconWrapper}>
              <Image source={choixIcon3} style={styles.choixIcon} />
            </View>
            <View style={styles.w20} />
            <View style={commonStyles.flex1}>
              <Text
                style={[
                  commonStyles.H4,
                  commonStyles.textLeft,
                  commonStyles.mt0,
                ]}>
                Privilégiez les fruits
              </Text>
              <Text style={[styles.choixColValue, commonStyles.textLeft]}>
                Les pâtisseries et gâteaux sont souvent trop riches en glucides
                et lipides.
              </Text>
            </View>
          </View>
          <Text
            style={[
              styles.choixColValue,
              commonStyles.textLeft,
              {marginTop: -5},
            ]}>
            Choisissez une salade de fruits (non-industrielle si possible), un
            yaourt nature ou un fromage blanc, ou des oléagineux si vous n'en
            avez pas consommé à un autre moment de la journée.
          </Text>

          <Text
            style={[
              commonStyles.H4,
              commonStyles.textLeft,
              commonStyles.mt0,
              {fontSize: 18, marginTop: 30},
            ]}>
            CONSEIL EXPERT
          </Text>
          <Text style={[styles.choixColValue, commonStyles.textLeft]}>
            Privilégiez les repas dont vous pouvez choisir la composition. Les
            menus sont alléchants par leur prix et les propositions de plats,
            mais incitent à manger plus, même si vous n'avez plus faim. Il est
            plus facile de maîtriser votre repas en choisissant à la carte.
          </Text>

          <Text style={[styles.choixColValue, commonStyles.textLeft]}>
            C’est également valable pour les repas pris en dehors du restaurant.
            Le fait maison reste la meilleure option pour contrôler la
            composition de votre repas et ainsi respecter au mieux les
            recommandations ci-dessus.
          </Text>

          <View style={{marginTop: 40}}>
            <PrimaryButton
              onPress={this.onChoose}
              title="Suivre ces recommandations"
            />
          </View>
        </View>
        <MyAlert ref={this.alert} />
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    mealPlanning: state.MealPlanningReducer.mealPlanning,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateMealPlanning: req =>
      dispatch(
        MealPlanningActions.update(req.mealPlanningId, req.data, req.cb),
      ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AlimentationOutsideScreen);
