/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {ScrollView, View, Text} from 'react-native';
import commonStyles from '../../../Styles';
import styles from '../_styles';
import {PrimaryButton} from '../../../../Widgets';

// Objectifs
export default class ObjectivesScreen extends React.Component {
  render() {
    const steps = [1, 2, 3, 4, 5];
    return (
      <ScrollView contentContainerStyle={commonStyles.wrapper}>
        {steps.map((step, index) => {
          return (
            <View style={styles.stepRow}>
              <Text style={styles.stepText}>Etape {step}</Text>
              <PrimaryButton title="Objectif 1" />
              <View style={{height: 5}} />
              <PrimaryButton title="Objectif 2" />
            </View>
          );
        })}
      </ScrollView>
    );
  }
}
