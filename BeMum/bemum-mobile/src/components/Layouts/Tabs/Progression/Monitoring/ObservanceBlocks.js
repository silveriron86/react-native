import React from 'react';
import {Image, View, Text, TouchableOpacity} from 'react-native';
import CommonStyles from '../../../Styles';
import styles from '../_styles';
const crossIcon = require('../../../../../../assets/icons/observations/cross.png');
const crossOnIcon = require('../../../../../../assets/icons/observations/cross_on.png');
const checkedIcon = require('../../../../../../assets/icons/observations/checked.png');
const checkedOnIcon = require('../../../../../../assets/icons/observations/checked_on.png');
const waveIcon = require('../../../../../../assets/icons/observations/wave.png');
const waveOnIcon = require('../../../../../../assets/icons/observations/wave_on.png');

export default class ObservanceBlocks extends React.Component {
  onPress = (type, value, current) => {
    console.log(type, value);
    this.props.onSave(type, value, current);
  };

  render() {
    const {observance} = this.props;
    if (observance === null) {
      return null;
    }

    const {foodSupplements, mealPlanning} = observance;
    return (
      <View style={styles.obBlocksRow}>
        <View style={[styles.mealFillBtn, styles.obBlock]}>
          <Text style={CommonStyles.H4}>Compléments</Text>
          <View style={styles.obButtons}>
            <TouchableOpacity
              style={styles.obButton}
              onPress={() =>
                this.onPress('food-supplements', 'no', foodSupplements)
              }>
              <Image
                source={foodSupplements === 'no' ? crossOnIcon : crossIcon}
                style={styles.obIcon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.obButton}
              onPress={() =>
                this.onPress('food-supplements', 'yes', foodSupplements)
              }>
              <Image
                source={foodSupplements === 'yes' ? checkedOnIcon : checkedIcon}
                style={styles.obIcon}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={[styles.mealFillBtn, styles.obBlock]}>
          <Text style={CommonStyles.H4}>Recettes</Text>
          <View style={styles.obButtons}>
            <TouchableOpacity
              style={styles.obButton}
              onPress={() => this.onPress('meal-planning', 'no', mealPlanning)}>
              <Image
                source={mealPlanning === 'no' ? crossOnIcon : crossIcon}
                style={styles.obIcon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.obButton}
              onPress={() =>
                this.onPress('meal-planning', 'partially', mealPlanning)
              }>
              <Image
                source={mealPlanning === 'partially' ? waveOnIcon : waveIcon}
                style={styles.obIcon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.obButton}
              onPress={() =>
                this.onPress('meal-planning', 'yes', mealPlanning)
              }>
              <Image
                source={mealPlanning === 'yes' ? checkedOnIcon : checkedIcon}
                style={styles.obIcon}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
