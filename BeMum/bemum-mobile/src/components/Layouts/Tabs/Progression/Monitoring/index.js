/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, ScrollView, AppState} from 'react-native';
import moment from 'moment';
import ActionSheet from 'react-native-actionsheet';
import commonStyles from '../../../Styles';
import {
  Wheel,
  RemplirButton,
  LoadingOverlay,
  WelcomeIntro,
} from '../../../../Widgets';
import SuiviLineChart from './Charts/LineChart';
import SuiviGaugeChart from './Charts/GaugeChart';
import SuiviImcChart from './Charts/ImcChart';
import SuiviTourDeChart from './Charts/TourDeChart';
import {connect} from 'react-redux';
import {
  AccountActions,
  MealPlanningActions,
  ObservanceActions,
} from '../../../../../actions';
import DaysSelector from '../../Alimentation/_widgets/DaysSelector';
import ObservanceBlocks from './ObservanceBlocks';
import ObservanceChart from './Charts/ObservanceChart';

const actionSheet2Data = [
  'Annuler',
  '[Nom de la recette 1]',
  '[Nom de la recette 2]',
  "J'ai suivi les recommandations",
  "J'ai mangé autre chose",
];

const PoidsQuestions = {
  question_id: 5, // WEIGHT
  ui: 'wheel',
  ui_config: {
    min: 40,
    max: 250,
    precision: 1,
    // precision: 0.1,
  },
  unit: {
    short_name: 'kg',
  },
  label_question: 'lorem',
  value: 60,
  required: true,
};

const TourDeQuestions = {
  question_id: 6, // HIPS, WAIST
  ui: 'wheel',
  ui_config: {
    min: 50,
    max: 150,
    precision: 1,
    // precision: 0.5,
  },
  unit: {
    short_name: 'cm',
  },
  label_question: 'lorem',
  value: 0,
  required: true,
};

const mealsConstants = [
  {
    label: 'Petit Déjeuner',
    selected: null,
    index: 0,
    order: 0,
  },
  {
    label: 'Déjeuner',
    selected: null,
    index: 1,
    order: 2,
  },
  {
    label: 'Collation',
    selected: null,
    index: 2,
    order: 1,
  },
  {
    label: 'Diner',
    selected: null,
    index: 3,
    order: 4,
  },
  {
    label: 'Collation',
    selected: null,
    index: 4,
    order: 3,
  },
  {
    label: 'Compléments',
    selected: null,
    index: 5,
    order: 5,
  },
];

const breakFastConstants = ['OPTION_1', 'OPTION_2', 'OTHER'];
const lunchDinerConstants = ['REGULAR', 'MINUTE', 'OUTSIDE', 'OTHER'];

// Suivi
class MonitoringScreen extends React.Component {
  constructor(props) {
    super(props);
    this.wheelRef = React.createRef();
    this.state = {
      // enableFollowup: false,
      currentDate: moment(),
      actionSheetData: null,
      selectedMealIndex: 0,
      wheelQuestion: null,
      loading: false,
      meals: JSON.parse(JSON.stringify(mealsConstants)),
      actionSheets: [
        [
          'Annuler',
          '[breakfast recipe 1]',
          '[breakfast recipe 2]',
          "J'ai pris un autre petit déjeuner",
        ],
        actionSheet2Data,
        [
          'Annuler',
          "J'ai pris ma collation ce jour",
          "Je n'ai pas pris ma collation ce jour",
        ],
        actionSheet2Data,
        [
          'Annuler',
          "J'ai pris ma collation ce jour",
          "Je n'ai pas pris ma collation ce jour",
        ],
        [
          'Annuler',
          "J'ai pris mes compléments ce jour",
          "Je n'ai pas pris mes compléments ce jour",
        ],
      ],
    };
  }

  onChooseMeal = v => {
    if (v === 0) {
      return;
    }
    let meals = this.state.meals;
    const {actionSheetData, selectedMealIndex} = this.state;
    const {mealPlanning} = this.props;
    meals[selectedMealIndex].selected = actionSheetData[v];
    let data = null;
    switch (selectedMealIndex) {
      case 0:
        data = {
          selectedBreakfast: breakFastConstants[v - 1],
        };
        break;

      case 1:
        data = {
          selectedLunch: lunchDinerConstants[v - 1],
        };
        break;

      case 2:
        data = {
          tookMorningSnack: [true, false][v - 1],
        };
        break;

      case 3:
        data = {
          selectedDiner: lunchDinerConstants[v - 1],
        };
        break;

      case 4:
        data = {
          tookAfternoonSnack: [true, false][v - 1],
        };
        break;

      case 5:
        data = {
          tookSupplements: [true, false][v - 1],
        };
        break;
    }

    if (data) {
      this.setState(
        {
          loading: true,
        },
        () => {
          this.props.updateMealPlanning({
            mealPlanningId: mealPlanning.id,
            data,
            cb: res => {
              this.setState({
                loading: false,
              });
              global.eventEmitter.emit('UPDATE_MEALPLANNING', {
                currentDate: this.state.currentDate.format('YYYY-MM-DD'),
              });
            },
          });
        },
      );
    }
    this.setState({meals});
  };

  onSelectDate = newDate => {
    if (newDate === null) {
      newDate = moment();
    }

    this.setState({currentDate: newDate}, () => {
      this.loadData();
    });
  };

  loadData = () => {
    this.setState(
      {
        loading: true,
      },
      () => {
        const date = this.state.currentDate.format('YYYY-MM-DD');
        this.props.getObservance({
          date,
          cb: () => {
            this.setState({loading: false});
          },
        });
        /*
        this.props.getMealPlanning({
          date,
          cb: res => {
            let meals = JSON.parse(JSON.stringify(mealsConstants));
            let actionSheets = JSON.parse(
              JSON.stringify(this.state.actionSheets),
            );
            if (res.success) {
              const data = res.data;
              if (data.breakfastOption1 && data.breakfastOption2) {
                actionSheets[0][1] = data.breakfastOption1.name;
                actionSheets[0][2] = data.breakfastOption2.name;
              }

              actionSheets[1][1] = data.lunchDish.name;
              actionSheets[1][2] = data.lunchMinute.name;

              actionSheets[3][1] = data.dinerDish.name;
              actionSheets[3][2] = data.dinerMinute.name;

              if (data.selectedBreakfast) {
                const found = breakFastConstants.indexOf(
                  data.selectedBreakfast,
                );
                meals[0].selected =
                  found >= 0 ? actionSheets[0][found + 1] : null;
              }
              if (data.selectedLunch) {
                const found = lunchDinerConstants.indexOf(data.selectedLunch);
                meals[1].selected =
                  found >= 0 ? actionSheets[1][found + 1] : null;
              }
              if (data.tookMorningSnack !== null) {
                const found = [true, false].indexOf(data.tookMorningSnack);
                meals[2].selected =
                  found >= 0 ? actionSheets[2][found + 1] : null;
              }
              if (data.selectedDiner) {
                const found = lunchDinerConstants.indexOf(data.selectedDiner);
                meals[3].selected =
                  found >= 0 ? actionSheets[3][found + 1] : null;
              }
              if (data.tookAfternoonSnack !== null) {
                const found = [true, false].indexOf(data.tookAfternoonSnack);
                meals[4].selected =
                  found >= 0 ? actionSheets[4][found + 1] : null;
              }
              if (data.tookSupplements !== null) {
                const found = [true, false].indexOf(data.tookSupplements);
                meals[5].selected =
                  found >= 0 ? actionSheets[5][found + 1] : null;
              }
            }
            this.setState({meals, actionSheets});
            this.props.getObservance({
              date,
              cb: () => {
                this.setState({loading: false});
              },
            });
          },
        });
        */
      },
    );
  };

  saveObservance = (type, value, current) => {
    this.setState(
      {
        loading: true,
      },
      () => {
        const date = this.state.currentDate.format('YYYY-MM-DD');
        const data = {type, value, date};
        if (current === null) {
          this.props.recordObservance({
            data,
            cb: () => {
              this._cbAfterRecord(date);
            },
          });
        } else {
          this.props.updateObservance({
            data,
            cb: () => {
              this._cbAfterRecord(date);
            },
          });
        }
      },
    );
  };

  _cbAfterRecord = date => {
    this.props.getObservancesList({
      cb: () => {},
    });
    this.props.getObservance({
      date,
      cb: () => {
        this.setState({loading: false});
      },
    });
  };

  goProfiling = () => {
    this.props.navigation.navigate('ProfilingCategory');
  };

  showActionSheet = index => {
    const {actionSheets, meals} = this.state;
    if (index < meals.length) {
      this.setState(
        {
          selectedMealIndex: index,
          actionSheetData: actionSheets[index],
        },
        () => {
          this.ActionSheet.show();
        },
      );
    }
  };

  handleSelectPoid = type => {
    this.setState(
      {
        wheelQuestion: null,
      },
      () => {
        this.setState(
          {
            wheelQuestion:
              type === 1 ? {...PoidsQuestions} : {...TourDeQuestions},
          },
          () => {
            this.wheelRef.current.triggerOpen();
          },
        );
      },
    );
  };

  _handleAppStateChange = nextAppState => {
    if (nextAppState === 'active') {
      this._getPatientData();
    }
  };

  componentDidMount() {
    this.mount = true;
    AppState.addEventListener('change', this._handleAppStateChange);

    setTimeout(() => {
      this.props.getMetrics({
        cb: res => {},
      });
      this.props.getNutrinomes({
        cb: res => {},
      });
    }, 30);

    this._getPatientData();
    setInterval(() => {
      this._getPatientData();
    }, 1000 * 60 * 60); // per 1 hour

    this.props.getObservancesList({
      cb: () => {},
    });
    this.onSelectDate(null);

    global.eventEmitter.addListener('VISITED_PROGRESSION', () => {
      this.props.getMetrics({
        cb: () => {},
      });
      this.props.getNutrinomes({
        cb: () => {},
      });
      this._getPatientData();
    });
  }

  _getPatientData = () => {
    if (!this.mount) {
      return;
    }

    this.props.getPatient({
      cb: patient => {
        console.log('*** patient = ', patient);
      },
    });
  };

  componentWillUnmount() {
    this.mount = false;
  }

  render() {
    const {actionSheetData, wheelQuestion, loading, currentDate} = this.state;
    const {patient, metrics, nutrinomes, observances, observance} = this.props;

    return (
      <View style={commonStyles.container}>
        <DaysSelector currentDate={currentDate} onSelect={this.onSelectDate} />
        <ScrollView style={{marginTop: /*130*/ 0}}>
          <View style={{position: 'absolute', width: 0, height: 0}}>
            {wheelQuestion !== null && (
              <Wheel
                ref={this.wheelRef}
                label={wheelQuestion.label_question}
                tooltip={wheelQuestion.tooltip}
                placeholder={`0 ${
                  wheelQuestion.unit ? wheelQuestion.unit.short_name : ''
                }`}
                config={wheelQuestion.ui_config}
                unit={wheelQuestion.unit}
                required={wheelQuestion.required}
                value={wheelQuestion.value}
                onChange={value => {
                  console.log('value =', value);
                }}
              />
            )}
          </View>

          <ObservanceBlocks
            observance={observance}
            onSave={this.saveObservance}
          />

          <View style={[{paddingTop: 0}, commonStyles.wrapper]}>
            <ObservanceChart
              observances={observances}
              handleSelectPoid={() => this.handleSelectPoid(2)}
            />

            {patient !== null && (
              <RemplirButton
                onPress={this.goProfiling}
                // disabled={!enableFollowup}
                disabled={patient.updateProfile !== true}
              />
            )}

            <SuiviGaugeChart nutrinomes={nutrinomes} />
            <SuiviLineChart
              metrics={metrics}
              handleSelectPoid={() => this.handleSelectPoid(1)}
            />

            <SuiviImcChart metrics={metrics} />
            <SuiviTourDeChart
              metrics={metrics}
              handleSelectPoid={() => this.handleSelectPoid(2)}
            />
          </View>
        </ScrollView>
        {actionSheetData !== null && (
          <ActionSheet
            ref={o => (this.ActionSheet = o)}
            // title={'Which one do you like ?'}
            options={actionSheetData}
            cancelButtonIndex={0}
            // destructiveButtonIndex={1}
            tintColor={'black'}
            onPress={v => {
              this.onChooseMeal(v);
            }}
          />
        )}
        {/* <CrispLoader hasTab={true} /> */}
        <LoadingOverlay loading={loading} />
        {!loading && <WelcomeIntro />}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    patient: state.AccountReducer.patient,
    metrics: state.AccountReducer.metrics,
    nutrinomes: state.AccountReducer.nutrinomes,
    signup_profileFormData: state.SurveyReducer.signup_profileFormData,
    followup_profileFormData: state.SurveyReducer.followup_profileFormData,
    mealPlanning: state.MealPlanningReducer.mealPlanning,
    observances: state.ObservanceReducer.observances,
    observance: state.ObservanceReducer.observance,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPatient: req => dispatch(AccountActions.get(req.cb)),
    getNutrinomes: req => dispatch(AccountActions.getNutrinomes(req.cb)),
    getMetrics: req => dispatch(AccountActions.getMetrics(req.cb)),
    getMealPlanning: req => dispatch(MealPlanningActions.get(req.date, req.cb)),
    updateMealPlanning: req =>
      dispatch(
        MealPlanningActions.update(req.mealPlanningId, req.data, req.cb),
      ),
    getObservancesList: req => dispatch(ObservanceActions.getList(req.cb)),
    getObservance: req => dispatch(ObservanceActions.get(req.date, req.cb)),
    recordObservance: req => dispatch(ObservanceActions.post(req.data, req.cb)),
    updateObservance: req =>
      dispatch(ObservanceActions.update(req.data, req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MonitoringScreen);
