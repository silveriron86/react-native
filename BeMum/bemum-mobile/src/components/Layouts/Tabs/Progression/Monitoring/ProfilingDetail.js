/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {ScrollView, SafeAreaView, View} from 'react-native';
import {
  Switch,
  Input,
  Radio,
  Date,
  Wheel,
  OutlineButton,
} from '../../../../Widgets';
import {Colors} from '../../../../../constants';
import CommonStyles from '../../../Styles';

class ProfilingDetailScreen extends React.Component {
  goNext = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  changeFormData = (question_id, value) => {
    console.log('* changeFormData', question_id, value);
  };

  componentDidMount() {
    const {route, navigation} = this.props;
    navigation.setOptions({
      headerTitle: route.params.category,
    });
  }

  render() {
    const itemColor = Colors.theme.DARK;
    const questions = [
      {
        question_id: 1,
        label_question: 'lorem',
        ui: 'switch',
        ui_config: {
          choices: [{label: 'Yes'}, {label: 'No'}],
        },
        tooltip: 'Test Switch',
        required: true,
      },
      {
        question_id: 2,
        ui: 'stepper',
        ui_config: {
          min: 0,
          max: 10,
          precision: 1,
        },
        unit: {
          short_name: '%',
        },
        label_question: 'lorem',
        tooltip: 'Test Stepper',
        value: 0,
        required: true,
      },
      {
        question_id: 3,
        ui: 'radio',
        ui_config: {
          choices: [
            {label: "Essentiellement en fin d'après midi ou en soirée"},
            {label: "Cela peut être n'importe quand dans la journée"},
          ],
        },
        label_question: 'lorem',
        value: 0,
        required: true,
      },
      {
        question_id: 4,
        ui: 'datepicker',
        ui_config: {
          max: '2020-12-31',
        },
        label_question: 'lorem',
        value: 'NULL',
        required: true,
      },
      {
        question_id: 5,
        ui: 'wheel',
        ui_config: {
          min: 1,
          max: 10,
          precision: 1,
        },
        unit: {
          short_name: '%',
        },
        label_question: 'lorem',
        value: 0,
        required: true,
      },
    ];
    return (
      <SafeAreaView style={[CommonStyles.container]}>
        <ScrollView contentContainerStyle={CommonStyles.wrapper}>
          {questions.map((question, idx) => {
            if (question.ui === 'switch') {
              return (
                <Switch
                  key={`question_${idx}`}
                  label={question.label_question}
                  data={question.ui_config.choices}
                  tooltip={question.tooltip}
                  required={question.required}
                  value={question.value}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              );
            }

            if (question.ui === 'stepper') {
              return (
                <Input
                  key={`question_${idx}`}
                  label={question.label_question}
                  placeholder={`0 ${
                    question.unit ? question.unit.short_name : ''
                  }`}
                  tooltip={question.tooltip}
                  min={question.ui_config.min}
                  max={question.ui_config.max}
                  precision={question.ui_config.precision}
                  required={question.required}
                  value={question.value}
                  suffix={question.unit ? ` ${question.unit.short_name}` : ''}
                  readOnly={false}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              );
            }

            if (question.ui === 'radio') {
              return (
                <Radio
                  key={`question_${idx}`}
                  itemColor={itemColor}
                  label={question.label_question}
                  tooltip={question.tooltip}
                  data={question.ui_config.choices}
                  required={question.required}
                  value={question.value}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              );
            }

            if (question.ui === 'datepicker') {
              return (
                <Date
                  key={`question_${idx}`}
                  label={question.label_question}
                  tooltip={question.tooltip}
                  max={question.ui_config.max}
                  value={question.value}
                  required={question.required}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              );
            }

            if (question.ui === 'wheel') {
              return (
                <Wheel
                  key={`question_${idx}`}
                  label={question.label_question}
                  tooltip={question.tooltip}
                  placeholder={`0 ${
                    question.unit ? question.unit.short_name : ''
                  }`}
                  config={question.ui_config}
                  unit={question.unit}
                  itemColor={itemColor}
                  required={question.required}
                  value={question.value}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              );
            }
            return null;
          })}
          <View style={{marginTop: 20}}>
            <OutlineButton onPress={this.goNext} title="Suivant" />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfilingDetailScreen);
