/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Grid, LineChart, XAxis, YAxis} from 'react-native-svg-charts';
import {Circle} from 'react-native-svg';
import styles from '../../_styles';
import commonStyles from '../../../../Styles';
import {Colors} from '../../../../../../constants';
import moment from 'moment';
import Utils from '../../../../../../utils';

export default class SuiviTourDeChart extends React.Component {
  state = {
    collapsed: true,
  };

  toggleExpand = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    const {metrics} = this.props;
    if (metrics === null) {
      return null;
    }

    const {collapsed} = this.state;
    let lineData1 = [];
    let lineData2 = [];
    let dates = [];
    if (typeof metrics.WAIST !== 'undefined') {
      metrics.WAIST.forEach(w => {
        lineData1.push(parseFloat(w.value));
        const label = moment(w.date).format('MM/YY');
        dates.push(dates.indexOf(label) < 0 ? label : '');
      });
    }

    if (typeof metrics.HIPS !== 'undefined') {
      metrics.HIPS.forEach(w => {
        lineData2.push(parseFloat(w.value));
        const dt = moment(w.date).format('MM/YY');
        if (dates.indexOf(dt) < 0) {
          dates.push(dt);
        }
      });
    }

    const lineData = [
      {data: lineData1, svg: {stroke: Colors.theme.DARK}},
      {data: lineData2, svg: {stroke: Colors.SKY}},
    ];
    const labels = [
      {color: Colors.theme.DARK, label: 'Tour de taille'},
      {color: Colors.SKY, label: 'Tour de hanches'},
    ];

    const XaxesSvg = {fontSize: 12, fill: 'grey'};
    const YaxesSvg = {fontSize: 14, fill: 'grey'};
    const verticalContentInset = {top: 10, bottom: 10, left: 20, right: 20};
    const xAxisHeight = 30;
    const Decorator1 = ({x, y}) => {
      return lineData1.map((value, index) => (
        <Circle
          key={index}
          cx={x(index)}
          cy={y(value)}
          r={4}
          stroke={Colors.theme.DARK}
          fill={Colors.theme.DARK}
        />
      ));
    };
    const Decorator2 = ({x, y}) => {
      return lineData2.map((value, index) => (
        <Circle
          key={index}
          cx={x(index)}
          cy={y(value)}
          r={4}
          stroke={Colors.SKY}
          fill={Colors.SKY}
        />
      ));
    };

    const waist = lineData1;
    let n = waist.length - 1;
    const waistDifference = n < 1 ? 0 : waist[n] - waist[0];

    const hips = lineData2;
    n = hips.length - 1;
    const hipsDifference = n < 1 ? 0 : hips[n] - hips[0];
    const currentWaist =
      waist.length > 0 ? Utils.formatNumber(waist[waist.length - 1]) : 0;

    const currentHips =
      hips.length > 0 ? Utils.formatNumber(hips[hips.length - 1]) : 0;
    return (
      <>
        {collapsed && (
          <TouchableOpacity
            style={[styles.chatArea, styles.marginRow]}
            onPress={this.toggleExpand}>
            <View style={styles.chartLabelRow}>
              <View style={styles.ChartLabels}>
                <Text style={commonStyles.H4}>
                  TOUR DE TAILLE
                  {/* <Text style={styles.lienChartUnit}>(cm)</Text> */}
                </Text>
                {waistDifference !== 0 && (
                  <Text style={styles.lienChartValue}>
                    {'   '}
                    {Utils.formatNumber(waistDifference)}cm *
                  </Text>
                )}
              </View>
              <Text
                style={[styles.ChartLabelValue, {fontFamily: 'AmericanaStd'}]}>
                {currentWaist}
                <Text style={styles.lienChartUnit}> cm</Text>
              </Text>
            </View>

            <View style={styles.chartLabelRow}>
              <View style={styles.ChartLabels}>
                <Text style={commonStyles.H4}>
                  TOUR DE HANCHE
                  {/* <Text style={styles.lienChartUnit}>(cm)</Text> */}
                </Text>
                {hipsDifference !== 0 && (
                  <Text style={styles.lienChartValue}>
                    {'   '}
                    {Utils.formatNumber(hipsDifference)}cm *
                  </Text>
                )}
              </View>
              <Text
                style={[styles.ChartLabelValue, {fontFamily: 'AmericanaStd'}]}>
                {currentHips}
                <Text style={styles.lienChartUnit}> cm</Text>
              </Text>
            </View>
          </TouchableOpacity>
        )}

        {!collapsed && (
          <View
            style={[styles.chatArea, styles.marginRow, {paddingBottom: 18}]}>
            <TouchableOpacity
              style={[styles.withBorderBottom, {paddingBottom: 0}]}
              onPress={this.toggleExpand}>
              <View style={styles.chartLabelRow}>
                <View style={styles.ChartLabels}>
                  <Text style={commonStyles.H4}>
                    TOUR DE TAILLE
                    {/* <Text style={styles.lienChartUnit}>(cm)</Text> */}
                  </Text>
                  {waistDifference !== 0 && (
                    <Text style={styles.lienChartValue}>
                      {'   '}
                      {Utils.formatNumber(waistDifference)}cm *
                    </Text>
                  )}
                </View>
                <Text
                  style={[
                    styles.ChartLabelValue,
                    {fontFamily: 'AmericanaStd'},
                  ]}>
                  {currentWaist}
                  <Text style={styles.lienChartUnit}> cm</Text>
                </Text>
              </View>

              <View style={[styles.chartLabelRow]}>
                <View style={styles.ChartLabels}>
                  <Text style={commonStyles.H4}>
                    TOUR DE HANCHE
                    {/* <Text style={styles.lienChartUnit}>(cm)</Text> */}
                  </Text>
                  {hipsDifference !== 0 && (
                    <Text style={styles.lienChartValue}>
                      {'   '}
                      {Utils.formatNumber(hipsDifference)}cm *
                    </Text>
                  )}
                </View>
                <Text
                  style={[
                    styles.ChartLabelValue,
                    {fontFamily: 'AmericanaStd'},
                  ]}>
                  {currentHips}
                  <Text style={styles.lienChartUnit}> cm</Text>
                </Text>
              </View>
            </TouchableOpacity>

            <Text style={[styles.lineChartInfo, styles.mb20]}>
              *Depuis le début du programme
            </Text>

            <View style={styles.imcChartWrapper}>
              <YAxis
                data={lineData1}
                style={{marginBottom: xAxisHeight}}
                contentInset={verticalContentInset}
                svg={YaxesSvg}
                numberOfTicks={6}
                min={60}
                max={180}
              />

              <View style={{flex: 1, marginLeft: 20, position: 'relative'}}>
                <LineChart
                  style={{flex: 1}}
                  data={lineData}
                  contentInset={verticalContentInset}
                  yMin={60}
                  yMax={180}>
                  <Grid />
                  <Decorator1 />
                  <Decorator2 />
                </LineChart>
                <XAxis
                  style={{
                    marginHorizontal: -20,
                    height: xAxisHeight,
                  }}
                  data={lineData1}
                  formatLabel={(value, index) => {
                    return dates[index];
                  }}
                  contentInset={{left: 40, right: 40}}
                  svg={XaxesSvg}
                />
              </View>
            </View>
            <View
              style={[
                styles.imcChartInfos,
                {flexDirection: 'column', marginBottom: 0},
              ]}>
              {labels.map((l, idx) => {
                return (
                  <View style={styles.imcChartInfoCol} key={`lbl-${idx}`}>
                    <View
                      key={`clr-${idx}`}
                      style={[
                        styles.colorDot,
                        {
                          marginRight: 10,
                          backgroundColor: l.color,
                        },
                      ]}
                    />
                    <Text style={commonStyles.labelSubTitle}>{l.label}</Text>
                  </View>
                );
              })}
            </View>

            {/* <PrimaryButton
              title="Mettre à jour le tour de taille"
              onPress={this.props.handleSelectPoid}
            />
            <PrimaryButton
              style={{marginTop: 16}}
              title="Mettre à jour le tour de hanches"
              onPress={this.props.handleSelectPoid}
            /> */}
          </View>
        )}
      </>
    );
  }
}
