/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {ScrollView, SafeAreaView} from 'react-native';
import CommonStyles from '../../../Styles';
import {QuestionnairesList} from '../../../../Widgets';
import {SurveyConstants} from '../../../../../constants';

class QuestionnairesScreen extends React.Component {
  goDetail = category => {
    const {navigation} = this.props;
    navigation.navigate('CategoryQuestion', {
      type: 'followup',
      category,
      readOnly: false,
    });
  };

  render() {
    return (
      <SafeAreaView style={CommonStyles.container}>
        <ScrollView contentContainerStyle={CommonStyles.wrapper}>
          <QuestionnairesList
            // InputtingSurveyIndex={9}
            categories={SurveyConstants.CATEGORIES}
            onPress={this.goDetail}
            containerStyle={{paddingTop: 0}}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    loadedSurveys: state.SurveyReducer.loadedSurveys,
    InputtingSurveyIndex: state.SurveyReducer.InputtingSurveyIndex,
    profileFormData: state.SurveyReducer.signup_profileFormData,
    healthFormData: state.SurveyReducer.signup_healthFormData,
    reproductive_healthFormData:
      state.SurveyReducer.signup_reproductive_healthFormData,
    physical_activityFormData:
      state.SurveyReducer.signup_physical_activityFormData,
    sleep_fatigueFormData: state.SurveyReducer.signup_sleep_fatigueFormData,
    stressFormData: state.SurveyReducer.signup_stressFormData,
    moral_wellbeingFormData: state.SurveyReducer.signup_moral_wellbeingFormData,
    memory_concentrationFormData:
      state.SurveyReducer.signup_memory_concentrationFormData,
    oxydative_stressFormData:
      state.SurveyReducer.signup_oxydative_stressFormData,
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QuestionnairesScreen);
