import React from 'react';
import {connect} from 'react-redux';
import {Text, ScrollView, SafeAreaView} from 'react-native';
import CommonStyles from '../Styles';
import styles from './_styles';
import {PrimaryButton} from '../../Widgets';

class SecondQuestionnairesScreen extends React.Component {
  goNext = () => {
    const {navigation} = this.props;
    navigation.navigate('FoodPreferences');
  };

  goDetail = category => {
    const {navigation} = this.props;
    navigation.navigate('CategoryQuestion', {type: 'signup', category});
  };

  render() {
    return (
      <SafeAreaView style={CommonStyles.container}>
        <ScrollView contentContainerStyle={CommonStyles.wrapper}>
          <Text style={styles.description}>
            Pnam in culpa idiota aliis pravitatis. Principium ponere culpam in
            se justum praeceptium. Neque improperes et aliis qui non perfecle
          </Text>
          {/* <QuestionnairesList categories={categories} onPress={this.goDetail} /> */}
          <PrimaryButton onPress={this.goNext} title="Valider" />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SecondQuestionnairesScreen);
