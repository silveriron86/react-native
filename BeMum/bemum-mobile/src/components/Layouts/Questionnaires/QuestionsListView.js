import React from 'react';
import {View} from 'react-native';
import {Switch, Input, Radio, Date, Wheel, Autocomplete} from '../../Widgets';
import {Colors} from '../../../constants';
import Utils from '../../../utils';
import styles from './_styles';

export default class QuestionsListView extends React.Component {
  render() {
    const itemColor = Colors.theme.DARK;
    const {data, questions, changeFormData} = this.props;

    return (
      <>
        {questions.map((question, idx) => {
          if (Utils.checkVisibleQuestion(data, questions, question) === true) {
            if (question.ui === 'switch') {
              return (
                <Switch
                  key={`question_${idx}`}
                  label={question.label_question}
                  data={question.ui_config.choices}
                  tooltip={question.tooltip}
                  required={question.required}
                  value={question.value}
                  onChange={value =>
                    changeFormData(question.question_id, value)
                  }
                />
              );
            }

            if (question.ui === 'stepper') {
              return (
                <Input
                  type="number"
                  key={`question_${idx}`}
                  label={question.label_question}
                  placeholder={`0 ${
                    question.unit ? question.unit.short_name : ''
                  }`}
                  tooltip={question.tooltip}
                  min={question.ui_config.min}
                  max={question.ui_config.max}
                  precision={question.ui_config.precision}
                  required={question.required}
                  value={question.value}
                  suffix={question.unit ? ` ${question.unit.short_name}` : ''}
                  readOnly={false}
                  onChange={value =>
                    changeFormData(question.question_id, value)
                  }
                />
              );
            }

            if (question.ui === 'radio') {
              return (
                <View style={styles.questionRow}>
                  <Radio
                    key={`question_${idx}`}
                    itemColor={itemColor}
                    label={question.label_question}
                    tooltip={question.tooltip}
                    data={question.ui_config.choices}
                    required={question.required}
                    value={question.value}
                    onChange={value =>
                      changeFormData(question.question_id, value)
                    }
                  />
                </View>
              );
            }

            if (question.ui === 'datepicker') {
              return (
                <Date
                  key={`question_${idx}`}
                  label={question.label_question}
                  tooltip={question.tooltip}
                  max={question.ui_config.max}
                  value={question.value}
                  required={question.required}
                  onChange={value =>
                    changeFormData(question.question_id, value)
                  }
                />
              );
            }

            if (question.ui === 'wheel') {
              return (
                <Wheel
                  key={`question_${idx}`}
                  label={question.label_question}
                  tooltip={question.tooltip}
                  placeholder={`0 ${
                    question.unit ? question.unit.short_name : ''
                  }`}
                  config={question.ui_config}
                  unit={question.unit}
                  itemColor={itemColor}
                  required={question.required}
                  value={question.value}
                  onChange={value =>
                    changeFormData(question.question_id, value)
                  }
                />
              );
            }

            if (question.ui === 'autocomplete') {
              return (
                <View style={styles.questionRow}>
                  <Autocomplete
                    key={`question_${idx}`}
                    itemColor={itemColor}
                    url={question.ui_config.path}
                    value={question.value ? question.value : []}
                    label={question.label_question}
                    navigation={this.props.navigation}
                    onChange={value =>
                      changeFormData(question.question_id, value)
                    }
                  />
                </View>
              );
            }
          }
          return null;
        })}
      </>
    );
  }
}
