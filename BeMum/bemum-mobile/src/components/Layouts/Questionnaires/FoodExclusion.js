import React from 'react';
import {connect} from 'react-redux';
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';
import {SafeAreaView} from 'react-native';
import CommonStyles from '../Styles';
import {Autocomplete, LoadingOverlay} from '../../Widgets';
import {Colors} from '../../../constants';
import {PrimaryButton} from '../../Widgets';
import commonStyles from '../Styles';
import {AccountActions, SurveyActions} from '../../../actions';

const question = {
  question_id: 'FOOD_EXCLUSION',
  required: false,
  label_question:
    'Au-delà de tout régime alimentaire spécifique, y a-t-il des aliments auxquels vous êtes allergique ou que vous ne consommez pas ?',
  ui: 'autocomplete',
  ui_config: {
    path: '',
  },
  value: [],
};

class FoodExclusion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      values: [],
    };
  }

  componentDidMount() {
    this.loadFoodPreference();
  }

  loadFoodPreference = () => {
    this.props.getForm({
      type: 'signup',
      survey_id: 'food-preference',
      form_id: 'preference_food',
      cb: () => {
        // console.log(foodForm);
        this.setState({
          loading: false,
          // values: foodForm['FOOD_EXCLUSION'],
        });
      },
    });
  };

  close = () => {
    this.props.navigation.goBack();
  };

  onChange = values => {
    console.log('*** values = ', values);
    this.setState({
      values,
    });
    // this.changeFormData(question.question_id, value)
  };

  onPressNext = () => {
    const {values} = this.state;
    this.setState({
      loading: true,
    });
    this.props.updateIngredientsToAvoid({
      data: values,
      cb: res => {
        console.log(res);
        this.setState(
          {
            loading: false,
          },
          () => {
            this.close();
          },
        );
      },
    });
  };

  render() {
    const {loading} = this.state;
    const {ingredients, preference_foodFormData} = this.props;
    const itemColor = Colors.theme.DARK;
    return (
      <SafeAreaView style={commonStyles.container}>
        <KeyboardAvoidingScrollView
          contentContainerStyle={CommonStyles.wrapper}>
          <Autocomplete
            ingredients={ingredients}
            itemColor={itemColor}
            url={question.ui_config.path}
            value={preference_foodFormData.data[1].value}
            label={question.label_question}
            navigation={this.props.navigation}
            from="dislike"
            onChange={this.onChange}
          />
          <PrimaryButton onPress={this.onPressNext} title="Valider" />
          <LoadingOverlay loading={loading} />
        </KeyboardAvoidingScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    preference_foodFormData: state.SurveyReducer.preference_foodFormData,
    ingredients: state.IngredientReducer.ingredients,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateIngredientsToAvoid: req =>
      dispatch(AccountActions.updateIngredientsToAvoid(req.data, req.cb)),
    getForm: request =>
      dispatch(
        SurveyActions.getForm(
          request.type,
          request.survey_id,
          request.form_id,
          request.cb,
        ),
      ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FoodExclusion);
