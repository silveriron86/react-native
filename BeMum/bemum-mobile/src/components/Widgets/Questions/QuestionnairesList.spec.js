import React from 'react';
import QuestionnairesList from './QuestionnairesList';
import {render, cleanup, fireEvent} from 'react-native-testing-library';
import {SurveyConstants} from '../../../constants';

afterEach(cleanup);

describe('<QuestionnairesList />', () => {
  it('should fire onPress events', () => {
    const onPress = jest.fn();
    const rendered = render(
      <QuestionnairesList
        InputtingSurveyIndex={5}
        categories={SurveyConstants.CATEGORIES}
        onPress={onPress}
      />,
    );
    const categoryButtons = rendered.getByTestId('category-stress');
    fireEvent(categoryButtons, 'onPress');
    expect(onPress).toHaveBeenCalledWith(SurveyConstants.CATEGORIES[5], false);
  });
});
