/* eslint-disable react/no-string-refs */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View} from 'react-native';
import styles from '../_styles';
import SimplePicker from 'react-native-simple-picker';
import commonStyles from '../../Layouts/Styles';

export default class Wheel extends React.Component {
  constructor(props) {
    super(props);
    const {config, unit, value} = props;
    let options = [];
    let labels = [];
    for (let i = config.min; i <= config.max; i += config.precision) {
      let label = `${i.toString()}${
        unit.short_name ? ' ' + unit.short_name : ''
      }`;
      options.push(i);
      labels.push(label);
    }
    this.state = {
      val: value,
      labels: labels,
      options: options,
    };
  }

  // componentWillReceiveProps = nextProps => {
  //   if (nextProps.value !== this.props.value) {
  //     this.setState({
  //       val: nextProps.value,
  //     });
  //   }
  // };

  onSelect = option => {
    this.setState(
      {
        val: option,
      },
      () => {
        this.props.onChange(option);
      },
    );
  };

  triggerOpen = () => {
    this.refs.picker.show();
  };

  render() {
    const {label, required, type, tooltip, unit, placeholder} = this.props;
    const {options, labels, val} = this.state;
    let initialOptionIndex = 0;
    if (val) {
      options.forEach((opt, index) => {
        if (opt === val) {
          initialOptionIndex = index;
        }
      });
    }

    return (
      <View style={styles.row}>
        <Text
          style={[commonStyles.H4, type === 'followup' && {color: '#292A3E'}]}>
          {label}
          {required && '*'}
        </Text>
        {tooltip && <Text style={styles.tooltipText}>{tooltip}</Text>}
        <View style={styles.inputWrapper} testID="wheel-wrapper">
          <View style={styles.selector}>
            <Text
              style={styles.differentSelectValue}
              onPress={() => {
                this.refs.picker.show();
              }}>
              {val
                ? `${val.toString()}${
                    unit.short_name ? ' ' + unit.short_name : ''
                  }`
                : placeholder}
            </Text>
          </View>

          <SimplePicker
            ref={'picker'}
            options={options}
            labels={labels}
            initialOptionIndex={initialOptionIndex}
            confirmText="Confirmer"
            cancelText="Annuler"
            onSubmit={option => {
              this.onSelect(option);
            }}
          />
        </View>
      </View>
    );
  }
}
