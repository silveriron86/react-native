/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, Text, View, TouchableOpacity, StyleSheet} from 'react-native';
// import {connect} from 'react-redux';
import {OutlineButton} from '..';
import {Colors} from '../../../constants';
import commonStyles from '../../Layouts/Styles';
// import AsyncStorage from '@react-native-community/async-storage';
// import {SurveyActions} from '../../actions';

export default class Autocomplete extends React.Component {
  constructor(props) {
    super(props);

    let values = this.props.value;
    if (values && typeof values === 'string') {
      values = [];
    }
    this.state = {
      // values: values.slice(0),
      selectedValues: values.slice(0),
    };
    // this.updateValue(values);
  }

  onChangeText = search => {
    this.setState({search});
    this._getSearchData(search);
  };

  openModal = () => {
    const {selectedValues} = this.state;
    this.props.navigation.navigate('AddProductModal', {
      selectedValues,
    });
  };

  onValidate = () => {
    let selectedValues = this.state.selectedValues.slice(0);

    this.setState({
      selectedValues,
    });
    this.updateValue(selectedValues);
    this.setModalVisible(false);
  };

  updateValue = values => {
    let ids = [];
    // let names = [];
    if (values.length > 0) {
      values.forEach(v => {
        const row =
          // this.props.from === 'dislike' ?
          {
            id: v.id,
            name: this._getName(v.id),
          };
        // : {
        //     id: v.id,
        //   };
        ids.push(row);
        // names.push(v.name);
      });
    }
    this.props.onChange(ids);
    // this.props.onChange(`[${ids.toString()}]`);
  };

  removeItem = idx => {
    this.state.selectedValues.splice(idx, 1);
    this.setState(
      {
        selectedValues: this.state.selectedValues,
      },
      () => {
        this.updateValue(this.state.selectedValues);
      },
    );
  };

  _getName = id => {
    const {ingredients} = this.props;
    if (!ingredients) {
      return '';
    }

    const found = ingredients.find(ing => {
      return ing.id === id;
    });
    return found ? found.name : 'Unknown';
  };

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps.value) !== JSON.stringify(this.props.value)) {
      this.setState({
        selectedValues: nextProps.value,
      });
    }
  }

  componentDidMount() {
    global.eventEmitter.addListener('ADDED_PRODUCTS', data => {
      this.setState(
        {
          selectedValues: data.products,
        },
        () => {
          this.updateValue(this.state.selectedValues);
        },
      );
    });
  }

  render() {
    const {label} = this.props;
    return (
      <View style={{paddingBottom: 10}}>
        <Text style={commonStyles.H4}>{label}</Text>
        {this.state.selectedValues.length > 0 && (
          <View style={{paddingTop: 15, paddingBottom: 5}}>
            {this.state.selectedValues.map((item, i) => {
              const valueName = item.name ? item.name : this._getName(item.id);
              if (valueName === 'Unknown') {
                return null;
              }
              return (
                <View style={styles.withColsRow} key={`ac_${i}`}>
                  {/* <View style={alimenStyles.ingreRect} /> */}
                  <Text style={commonStyles.TextSecondary}>{valueName}</Text>
                  {/* <View style={styles.acRowCol}>
                  <Text style={styles.text}>{item.name}</Text>
                </View> */}
                  <TouchableOpacity
                    style={styles.closeBtn}
                    underlayColor="#fff"
                    onPress={() => this.removeItem(i)}>
                    <Image
                      resizeMode="stretch"
                      source={require('../../../../assets/icons/close.png')}
                      style={styles.imgClose}
                    />
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
        )}

        <OutlineButton
          style={{marginVertical: 10, width: 110, marginBottom: 20}}
          title="+ Ajouter"
          onPress={() => this.openModal()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292A3E',
  },
  row: {
    marginTop: 16,
  },
  preferRowWrap: {
    backgroundColor: '#3E3F51',
    marginBottom: 1,
  },
  withColsRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 5,
  },
  closeBtn: {
    backgroundColor: Colors.theme.MAIN,
    width: 30,
    height: 30,
    marginTop: 8,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgClose: {
    width: 18,
    height: 18,
    tintColor: 'black',
  },
});
