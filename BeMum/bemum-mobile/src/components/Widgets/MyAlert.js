import React from 'react';
import {View, Modal, Text, TouchableOpacity, StyleSheet} from 'react-native';

export default class MyAlert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      title: '',
      msg: '',
    };
  }

  closeModal = () => {
    this.setState({visible: false, title: '', msg: ''});
  };

  show = (title, msg) => {
    this.setState({visible: true, title, msg});
  };

  render() {
    const {visible, title, msg} = this.state;

    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        // onRequestClose={() => {
        //   setModalVisible(!modalVisible);
        // }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.titleTextStyle}>{title}</Text>
            <Text style={styles.modalText}>{msg}</Text>
            <View style={styles.btnWrapper}>
              <TouchableOpacity
                style={[styles.button, styles.buttonClose]}
                onPress={this.closeModal}>
                <Text style={styles.textStyle}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    backgroundColor: '#00000090',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    marginHorizontal: 20,
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 25,
    paddingBottom: 15,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  buttonClose: {
    backgroundColor: 'transparent',
  },
  titleTextStyle: {
    fontFamily: 'Poppins-Medium',
    // fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
  },
  textStyle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    textAlign: 'center',
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    marginBottom: 10,
  },
  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});
