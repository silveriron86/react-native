import {StyleSheet, Dimensions} from 'react-native';
import Colors from '../../constants/Colors';

export default StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    zIndex: 99999,
  },
  button: {
    backgroundColor: Colors.theme.DARK,
    height: 47,
    borderRadius: 24,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  outline: {
    backgroundColor: 'transparent',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'black',
    width: 'auto',
  },
  red: {
    backgroundColor: Colors.RED,
  },
  green: {
    backgroundColor: Colors.GREEN,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  qCategories: {
    marginVertical: 15,
  },

  //Elements
  text: {
    fontSize: 15,
    lineHeight: 19,
    letterSpacing: 0.2,
    // color: 'white',
  },
  row: {
    // paddingLeft: 23,
    // paddingRight: 23,
    // marginTop: 16,
  },
  date: {
    width: '100%',
  },
  label: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
  },
  switchRowWrap: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  switchLabel: {
    paddingRight: 5,
  },
  switchWrap: {
    borderRadius: 32,
    paddingLeft: 4,
    paddingRight: 4,
    height: 32,
  },
  radioGroupWrap: {
    marginTop: 10,
    marginBottom: 10,
  },
  radioBtn: {
    marginTop: 10,
  },
  radioLabel: {
    fontSize: 15,
    // color: 'white',
    marginLeft: 8,
    marginTop: 0,
  },
  tooltipText: {
    fontStyle: 'italic',
    color: 'gray',
    fontSize: 14,
  },
  inputWrapper: {
    width: '100%',
    minHeight: 52,
  },
  input: {
    // color: 'white',
    color: 'black',
    fontSize: 15,
    paddingTop: 12,
    paddingLeft: 3,
    includeFontPadding: false,
    paddingBottom: 10,
    borderWidth: 1,
    borderColor: 'transparent',
    // borderBottomWidth: Platform.OS === 'ios' ? 1 : 0,
    // borderBottomColor: 'white',
  },
  inputError: {
    color: 'red',
    marginTop: -8,
  },
  selector: {
    width: '100%',
    paddingVertical: 10,
    marginTop: 8,
    marginBottom: 18,
    paddingBottom: 5,
    // borderBottomWidth: 1,
    // borderBottomColor: 'white',
  },
  differentSelectLabel: {
    fontSize: 15,
    color: '#9B9B9B',
  },
  differentSelectValue: {
    fontSize: 15,
    // color: 'white',
  },
  tagsWrapper: {
    flexDirection: 'row',
  },
  tagView: {
    flexDirection: 'row',
    backgroundColor: '#999',
    height: 38,
    paddingRight: 15,
    alignItems: 'center',
    marginRight: 10,
  },
  tagCloseBtn: {
    height: 35,
    width: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabCloseIcon: {
    width: 20,
    height: 20,
    tintColor: 'black',
  },
  tagText: {
    // color: 'white',
    fontSize: 15,
    marginLeft: -5,
  },
  checkRow: {
    marginTop: 12,
    flexDirection: 'row',
    // alignItems: 'center',
  },
  checkBox: {
    width: 20,
    height: 20,
    marginRight: 8,
  },
  redOutlineButton: {
    borderColor: '#BDBDBD',
    borderWidth: 1,
    borderRadius: 28.5,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
