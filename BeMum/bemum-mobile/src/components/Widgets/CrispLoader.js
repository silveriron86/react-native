/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Platform,
  SafeAreaView,
  TouchableOpacity,
  Text,
  Image,
  Dimensions,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';
import ModalWrapper from 'react-native-modal-wrapper';
import {WebView} from 'react-native-webview';
import Utils from '../../utils';

const messageIcon = require('../../../assets/icons/messaging.png');

export default class CrispLoader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      crispUrl: '',
    };
  }

  handleOpenCrisp = () => {
    // crispConversation
    AsyncStorage.getItem('CRISP_URL', (_err, crispUrl) => {
      console.log('crispUrl = ', crispUrl);
      this.setState({crispUrl, isOpen: true});
    });
  };

  render() {
    const {isOpen, crispUrl} = this.state;
    const {hidden, hasTab} = this.props;
    let windowHeight =
      Dimensions.get('window').height -
      Utils.getInsetTop() -
      Utils.getInsetBottom();
    if (Platform.OS === 'android') {
      windowHeight -= 24;
    }

    if (hidden === true) {
      return null;
    }

    const closeButton = (
      <TouchableOpacity
        onPress={() => this.setState({isOpen: false})}
        style={{
          position: 'absolute',
          margin: 20,
          right: 0,
        }}>
        <Text style={{color: 'white'}}>Close</Text>
      </TouchableOpacity>
    );

    const webView = (
      <WebView
        source={{uri: crispUrl}}
        style={{
          height: windowHeight,
        }}
      />
    );

    return (
      <>
        <ModalWrapper
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'black',
          }}
          visible={isOpen}>
          <SafeAreaView style={{flex: 1, position: 'relative'}}>
            {Platform.OS === 'android' ? (
              <View style={{flex: 1, position: 'relative'}}>
                {webView}
                {closeButton}
              </View>
            ) : (
              <KeyboardAvoidingScrollView style={{flex: 1}}>
                {webView}
                {closeButton}
              </KeyboardAvoidingScrollView>
            )}
          </SafeAreaView>
        </ModalWrapper>
        {/* <ActionButton
          style={{
            marginBottom: hasTab ? -20 : Utils.getInsetBottom() + 50,
            marginRight: -20,
          }}
          // buttonColor="rgba(231,76,60,1)"
          buttonColor="#5B45F3"
          onPress={this.handleOpenCrisp}>
          <Text style={{color: 'white'}}>Hello World</Text>
        </ActionButton> */}

        <TouchableOpacity
          style={{
            position: 'absolute',
            bottom: hasTab ? 30 : Utils.getInsetBottom() + 30,
            right: 30,
            backgroundColor: '#C8BFFB',
            width: 60,
            height: 60,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 30,
            borderWidth: 1,
            borderColor: '#AC8BC7',
            shadowColor: 'black',
            shadowOffset: {width: 0, height: 12},
            shadowOpacity: 0.1,
            elevation: 1,
          }}
          onPress={this.handleOpenCrisp}>
          <Image
            source={messageIcon}
            style={{width: '65%', resizeMode: 'contain'}}
          />
        </TouchableOpacity>
      </>
    );
  }
}
