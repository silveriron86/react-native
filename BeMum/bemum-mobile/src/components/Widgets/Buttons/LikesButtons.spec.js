import React from 'react';
import LikesButtons from './LikesButtons';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<LikesButtons />', () => {
  it('should fire onPress events', () => {
    const onDislike = jest.fn();
    const rendered = render(<LikesButtons onDislike={onDislike} />);
    const buttonComponent = rendered.getByTestId('likes-buttons');

    fireEvent(buttonComponent.children[1], 'press', 1);

    expect(onDislike).toHaveBeenCalledWith(1);
  });
});
