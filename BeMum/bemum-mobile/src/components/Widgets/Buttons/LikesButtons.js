/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
// import ModalWrapper from 'react-native-modal-wrapper';
// import ActionSheet from 'react-native-actionsheet';
// import ActionSheet from 'react-native-actionsheet';
import {ActionSheetCustom as ActionSheet} from 'react-native-actionsheet';
import Utils from '../../../utils';
// import Autocomplete from '../Elements/Autocomplete';
// const likeIcon = require('../../../../assets/icons/like.png');
const unlikeIcon = require('../../../../assets/icons/unlike.png');

export default class LikesButtons extends React.Component {
  onUnlike = () => {
    this.ActionSheet.show();
  };

  render() {
    const {style, isDetail} = this.props;
    const detailStyle = isDetail ? {bottom: 5} : {};
    return (
      <View
        testID="likes-buttons"
        style={[
          {
            position: 'absolute',
            flexDirection: 'row',
            bottom: 0,
            right: 15,
            zIndex: 9999,
          },
          style,
          detailStyle,
        ]}>
        <TouchableOpacity onPress={this.onUnlike}>
          <Image source={unlikeIcon} style={{width: 50, height: 50}} />
        </TouchableOpacity>
        {/* <TouchableOpacity>
          <Image source={likeIcon} style={{width: 50, height: 50}} />
        </TouchableOpacity> */}

        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          options={[
            <Text style={{fontWeight: 'bold', fontSize: 18}}>Annuler</Text>,
            'Je n’aime pas cette recette', // No body API in dislike api
            'Je n’aime pas un des ingrédients de la recette', // go to FOOD_EXCULSION
            'Cette recette est proposée trop souvent', // reason: too-frequent
            'La recette est mal rédigée', // reason: badly-written
          ]}
          cancelButtonIndex={0}
          // destructiveButtonIndex={1}
          tintColor={'black'}
          onPress={v => {
            if (v === 0) {
              return;
            }
            this.props.onDislike(v);
          }}
          styles={{
            buttonText: {
              fontFamily: 'Poppins-Regular',
              fontSize: 16,
            },
            cancelButtonBox: {
              height: 50,
              marginBottom: Utils.getInsetBottom(),
            },
            // body: {paddingBottom: 20},
          }}
        />
      </View>
    );
  }
}
