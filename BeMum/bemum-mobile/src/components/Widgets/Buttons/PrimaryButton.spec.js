import React from 'react';
import PrimaryButton from './PrimaryButton';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<PrimaryButton />', () => {
  it('should fire onPress events', () => {
    const onPress = jest.fn();
    const rendered = render(<PrimaryButton onPress={onPress} />);
    const buttonComponent = rendered.getByTestId('button');

    fireEvent(buttonComponent, 'press');

    expect(onPress).toHaveBeenCalled();
  });
});
