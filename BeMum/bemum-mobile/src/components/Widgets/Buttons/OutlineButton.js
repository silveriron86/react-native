import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import commonStyles from '../../Layouts/Styles';
import styles from '../_styles';

export default class OutlineButton extends React.Component {
  render() {
    const {onPress, title, style, children} = this.props;
    return (
      <TouchableOpacity
        testID="button"
        onPress={onPress}
        style={[styles.button, styles.outline, style]}>
        {children ? (
          children
        ) : (
          <Text style={commonStyles.btnText}>{title}</Text>
        )}
      </TouchableOpacity>
    );
  }
}
