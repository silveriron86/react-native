module.exports = [
  {
    question_id: 'INTRO_WEIGHT_SURVEY',
    required: false,
    ui: '',
    label_question:
      "Vos mesures corporelles et vos habitudes de vie impactent directement votre besoin nutritionnel. C'est pour cela qu'il est important de les renseigner régulièrement.",
    frequency: 7,
  },
  {
    question_id: 'WEIGHT',
    required: true,
    ui: 'stepper',
    label_question: 'Quel est votre poids actuel ?',
    tooltip: 'En kg',
    ui_config: {
      min: 40,
      max: 250,
      precision: 0.1,
    },
    value: 65,
    unit: {
      short_name: 'kg',
    },
    frequency: 7,
  },
  {
    question_id: 'WAIST',
    required: true,
    ui: 'stepper',
    label_question: 'Quel est votre tour de taille ?',
    tooltip:
      "Votre tour de taille et votre tour de hanches participent à établir votre profil de coaching, et donc à vous fournir des recettes et un accompagnement adapté. Il est très important que vous rentriez des valeurs correctes. Pour ces mesures, tenez-vous debout, pieds écartés à la largeur des épaules, l'abdomen détendu et libéré de tout vêtement. À l'aide d'une main, repérez l'extrémité supérieure de vos hanches. Positionnez le ruban à mesurer juste au-dessus de cet endroit et, en respirant normalement, resserrez le ruban autour de votre taille tout en évitant qu'il ne s'enfonce dans la peau. Le tour de taille doit être mesuré à la fin d'une expiration normale, il est important de ne pas rentrer le ventre mais de le relâcher normalement.",
    ui_config: {
      min: 50,
      max: 150,
      precision: 0.5,
    },
    unit: {
      short_name: 'cm',
    },
    frequency: 14,
  },
  {
    question_id: 'HIPS',
    required: false,
    ui: 'stepper',
    label_question: 'Quel est votre tour de hanches ?',
    tooltip:
      'Le tour de hanches doit être mesuré sur la partie la plus large des fesses. Prenez garde à ce que le ruban soit bien horizontal.',
    ui_config: {
      min: 50,
      max: 150,
      precision: 0.5,
    },
    unit: {
      short_name: 'cm',
    },
    frequency: 14,
  },
  {
    question_id: 'SMOKER_DAILY_QTY',
    required: true,
    ui: 'stepper',
    label_question:
      'Comment a évolué votre consommation de tabac : combien de cigarettes par jour fumez-vous actuellement ?',
    ui_config: {
      min: 1,
      max: 80,
      precision: 1,
    },
    conditions: [
      {
        parentQuestionId: 'SMOKER',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 7,
  },
  {
    question_id: 'SMOKER_CANNABIS_DAILY_QTY',
    required: true,
    ui: 'stepper',
    label_question:
      'Comment a évolué votre consommation de cannabis : combien de doses de cannabis par semaine fumez-vous actuellement ?',
    ui_config: {
      min: 1,
      max: 80,
      precision: 1,
    },
    conditions: [
      {
        parentQuestionId: 'SMOKER_CANNABIS',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 7,
  },
  {
    question_id: 'ALCOHOL_WEEKLY_QTY',
    required: true,
    ui: 'stepper',
    label_question:
      "Comment a évolué votre consommation d'alcool : combien de verre d'alcool buvez-vous par semaine en moyenne actuellement ?",
    ui_config: {
      min: 1,
      max: 100,
      precision: 1,
    },
    conditions: [
      {
        parentQuestionId: 'ALCOHOL',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 7,
  },
  {
    question_id: 'CONSUMPTION_TEA_DAILY_QTY',
    required: true,
    ui: 'stepper',
    label_question:
      'Comment a évolué votre consommation de thé : en moyenne combien de tasses de thé buvez-vous actuellement ?',
    ui_config: {
      min: 1,
      max: 50,
      precision: 1,
    },
    conditions: [
      {
        parentQuestionId: 'CONSUMPTION_TEA',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 14,
  },
];
