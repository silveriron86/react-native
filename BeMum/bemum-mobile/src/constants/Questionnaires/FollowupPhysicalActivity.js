module.exports = [
  {
    question_id: 'PAL_CHANGE',
    required: false,
    ui: 'switch',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    label_question:
      'Avez-vous modifié votre niveau d’activité physique au cours de la dernière semaine ?',
    frequency: 7,
  },
  {
    question_id: 'PAL_CHANGE_LASTING',
    required: false,
    ui: 'switch',

    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    label_question: "Comptez-vous maintenir ce nouveau niveau d'activité ?",
    frequency: 7,
    conditions: [
      {
        parentQuestionId: 'PAL_CHANGE',
        operator: '$neq',
        value: false,
      },
    ],
  },
  {
    question_id: 'PHYSICAL_ACTIVITY_INTRO',
    required: false,
    ui: '',
    label_question:
      "Le calcul de votre activité physique s'effectue sur une journée complète. Afin que votre besoin calorique calculé soit correct, il est important que le cumul de vos réponses approche 24h. Attention, les heures passées à faire du sport sont déclarées à la semaine.",
    frequency: 7,
  },
  {
    question_id: 'PAL_TIME_LAYING',
    required: true,
    ui: 'wheel',
    label_question:
      'Sur 24h, combien de temps en moyenne passez-vous en position couchée (sommeil, sieste, repos...) ?',
    ui_config: {
      min: 1,
      max: 18,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
    frequency: 7,
    conditions: [
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
  },
  {
    question_id: 'PAL_TIME_SITTING',
    required: true,
    ui: 'wheel',
    label_question:
      'Combien de temps passez-vous en position assise (ordinateur, bureau, transport, repas, lecture, télévision...) ?',
    ui_config: {
      min: 0,
      max: 18,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
    frequency: 7,
    conditions: [
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
  },
  {
    question_id: 'PAL_TIME_STANDING',
    required: true,
    ui: 'wheel',
    label_question:
      'Combien de temps par jour passez-vous en position debout ou à effectuer des travaux légers (faire votre toilette, petits déplacements dans la maison, activité professionnelle, cuisine, travaux ménagers, achats...) ?',
    ui_config: {
      min: 0,
      max: 18,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
    frequency: 7,
    conditions: [
      // {
      //   parentQuestionId: 'PROFESSIONAL_ACTIVITY',
      //   operator: '$eq',
      //   value: true,
      // },
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    conditions_assoc: 'AND',
  },
  {
    question_id: 'PAL_WALKING_OR_CYCLING',
    required: true,
    ui: 'wheel',
    label_question:
      'Combien de temps par jour consacrez-vous à vous déplacer à pied ou à vélo (pour vous rendre sur votre lieu de travail, effectuer des achats...) ?',
    ui_config: {
      min: 0,
      max: 18,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
    frequency: 7,
    conditions: [
      // {
      //   parentQuestionId: 'PROFESSIONAL_ACTIVITY',
      //   operator: '$eq',
      //   value: true,
      // },
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    conditions_assoc: 'AND',
  },
  {
    question_id: 'PAL_PROFESSIONAL_MEDIUM_INTENSITY',
    required: true,
    ui: 'switch',
    label_question:
      "Vos activités professionnelles impliquent-elles des activités physiques d'intensité moyenne (industrie chimique, machines-outils, menuiserie, porter des charges...), hors activité sportive professionnelle ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    frequency: 7,
    conditions: [
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
  },
  {
    question_id: 'PAL_PROFESSIONAL_MEDIUM_INTENSITY_DURATION',
    required: true,
    ui: 'wheel',
    label_question:
      "Habituellement, combien de temps par jour passez-vous à effectuer ce genre d'activités dans le cadre de votre travail ?",
    ui_config: {
      min: 0.25,
      max: 18,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
    conditions: [
      {
        parentQuestionId: 'PAL_PROFESSIONAL_MEDIUM_INTENSITY',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    conditions_assoc: 'AND',
    frequency: 7,
  },
  {
    question_id: 'PAL_HOUSEWORK',
    required: true,
    ui: 'switch',
    label_question:
      'Lors d’une journée habituelle en dehors de votre activité professionnelle, effectuez-vous des activités physiques d’intensité modérée (soulever des charges légères, tondre la pelouse, bricoler, jardiner…) ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    frequency: 7,
    conditions: [
      //   {
      //     parentQuestionId: 'PROFESSIONAL_ACTIVITY',
      //     operator: '$eq',
      //     value: true,
      //   },
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    // conditions_assoc: 'OR',
  },
  {
    question_id: 'PAL_HOUSEWORK_DURATION',
    required: true,
    ui: 'wheel',
    label_question:
      'Combien de temps par jour consacrez-vous à ces activités ?',
    ui_config: {
      min: 0.25,
      max: 18,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
    conditions: [
      {
        parentQuestionId: 'PAL_HOUSEWORK',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    conditions_assoc: 'AND',
    frequency: 7,
  },
  {
    question_id: 'PAL_SPORT_INTENSITY_LOW',
    required: true,
    ui: 'switch',
    label_question:
      "Pratiquez vous régulièrement une activité sportive d'intensité légère telle que celles citées dans la liste ci-dessous ou approchant ?",
    tooltip:
      '• Yoga, Taï-chi, Pilates, Gym légère\n• Danse (tango, mambo, salsa, flamenco, swing…)\n• Vélo (promenade)\n• Canoë, Aviron (amateur)\n• Volley-ball, Football (amateur)\n• Golf\n• Skateboarding\n• Équitation\n• Ping-pong',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    frequency: 7,
    conditions: [
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
  },
  {
    question_id: 'PAL_SPORT_INTENSITY_LOW_DURATION_WEEKLY',
    required: true,
    ui: 'wheel',
    label_question:
      'Combien de temps consacrez-vous en moyenne par semaine à cette activité sportive ?',
    ui_config: {
      min: 1,
      max: 35,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
    conditions: [
      {
        parentQuestionId: 'PAL_SPORT_INTENSITY_LOW',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    conditions_assoc: 'AND',
    frequency: 7,
  },
  {
    question_id: 'PAL_SPORT_INTENSITY_MODERATE',
    required: true,
    ui: 'switch',
    label_question:
      "Pratiquez vous régulièrement une activité sportive d'intensité modérée telle que celles citées dans la liste ci-dessous ou approchant ?",
    tooltip:
      '• Course à pied\n• Marche nordique ou marche rapide ou en montée\n• Vélo à 15 km/h, RPM\n• Escalade\n• Zumba, Sh’Bam, Bodyjam…\n• Aquagym\n• Rameur 100 watts\n• Escrime\n• Bodypump, Bodystep, CrossFit, Circuit training, Pompes, Gainage, Haltérophilie...\n• Basketball\n• Natation (brasse, crawl lent…)\n• Badminton, Tennis\n• Football (professionnel)',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    frequency: 7,
  },
  {
    question_id: 'PAL_SPORT_INTENSITY_MODERATE_DURATION_WEEKLY',
    required: true,
    ui: 'wheel',
    label_question:
      'Combien de temps consacrez-vous en moyenne par semaine à cette activité sportive ?',
    ui_config: {
      min: 1,
      max: 35,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
    conditions: [
      {
        parentQuestionId: 'PAL_SPORT_INTENSITY_MODERATE',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    conditions_assoc: 'AND',
    frequency: 7,
  },
  {
    question_id: 'PAL_SPORT_INTENSITY_HIGH',
    required: true,
    ui: 'switch',
    label_question:
      'Pratiquez vous régulièrement une activité sportive intense telle que celles citées dans la liste ci-dessous ou approchant ?',
    tooltip:
      '• Natation (papillon, crawl rapide…)\n• Squash\n• Canoë ou Aviron (professionnel), Rameur 200 watts\n• Vélo, plus de 30 km/h\n• Marathon, Course à pied à plus de 17,5 km/h ou en montant des marches\n• Rugby, Handball, Water-polo\n• Judo, Karaté, Kick-boxing\n• Saut à la corde',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    frequency: 7,
  },
  {
    question_id: 'PAL_SPORT_INTENSITY_HIGH_DURATION_WEEKLY',
    required: true,
    ui: 'wheel',
    label_question:
      'Combien de temps consacrez-vous en moyenne par semaine à cette activité sportive ?',
    ui_config: {
      min: 1,
      max: 35,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
    conditions: [
      {
        parentQuestionId: 'PAL_SPORT_INTENSITY_HIGH',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'PAL_CHANGE_LASTING',
        operator: '$neq',
        value: false,
      },
    ],
    conditions_assoc: 'AND',
    frequency: 7,
  },
];
