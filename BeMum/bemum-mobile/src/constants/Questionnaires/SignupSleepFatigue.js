module.exports = [
  {
    question_id: 'SLEEP_DURATION_DAILY',
    required: true,
    ui: 'wheel',
    label_question: "Combien d'heures dormez-vous en moyenne par nuit ?",
    ui_config: {
      min: 1,
      max: 15,
      precision: 0.25,
    },
    unit: {
      short_name: 'h',
    },
  },
  {
    question_id: 'FALL_ASLEEP_DIFFICULTIES_WORRIED',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous des préoccupations qui vous empêchent de vous endormir ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MIDDLE_OF_NIGHT_AWAKENING',
    required: true,
    ui: 'switch',
    label_question: 'Vous réveillez-vous souvent la nuit ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'EARLY_SPONTANEOUS_AWAKENING',
    required: true,
    ui: 'switch',
    label_question:
      'Finissez-vous spontanément votre nuit vers 4/5h du matin ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'VERY_ACTIVE_LIFESTYLE',
    required: true,
    ui: 'switch',
    label_question: "Avez-vous l'impression d'avoir un rythme de vie effréné ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'SLEEP_DEPRIVATION',
    required: true,
    ui: 'switch',
    label_question: 'Ressentez-vous le besoin de dormir davantage ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'SLEEP_DEPRIVATION_LACK_TIME',
    required: true,
    ui: 'switch',
    label_question: 'Votre manque de sommeil est-il lié à un manque de temps ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'SLEEP_DEPRIVATION',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'NEED_COFFEE_STIMULANT',
    required: true,
    ui: 'switch',
    label_question:
      'Ressentez-vous le besoin de consommer du café ou des stimulants ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'PHYSICAL_FATIGUE',
    required: true,
    ui: 'switch',
    label_question: 'Vous sentez-vous physiquement fatiguée ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'PHYSICAL_FATIGUE_ON_WAKING',
    required: true,
    ui: 'switch',
    label_question: 'Êtes-vous fatiguée physiquement dès le réveil ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'PHYSICAL_FATIGUE',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'FATIGUE_PHYSICAL_EFFORT',
    required: true,
    ui: 'switch',
    label_question:
      "Avez-vous le sentiment d'être anormalement fatiguée après un effort physique ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
];
