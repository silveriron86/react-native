module.exports = [
  {
    question_id: 'STRESSED',
    required: true,
    ui: 'switch',
    label_question: 'Vous sentez-vous stressée en ce moment ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'ANXIOUS',
    required: true,
    ui: 'switch',
    label_question: 'Vous sentez-vous anxieuse ou angoissée ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },

  {
    question_id: 'STRESSED_ON_WAKING',
    required: true,
    ui: 'switch',
    label_question:
      "Vous réveillez-vous avec un sentiment d'oppression ou de boule dans le ventre ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'STRESS_PERSONNAL_OR_JOB',
    required: true,
    ui: 'switch',
    label_question:
      'Êtes-vous préoccupée actuellement, au niveau professionnel ou personnel ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'STRESSFUL_EVENT',
    required: false,
    ui: '',
    label_question:
      'Quels évènements parmi ceux-ci avez vous vécu ces douze derniers mois ?',
  },
  {
    question_id: 'STRESSFUL_EVENT_PROFESSIONAL',
    required: true,
    ui: 'checkbox',
    label_question: 'Changement professionnel',
  },
  {
    question_id: 'STRESSFUL_EVENT_MARRIAGE',
    required: true,
    ui: 'checkbox',
    label_question: 'Mariage',
  },
  {
    question_id: 'STRESSFUL_EVENT_DIVORCE',
    required: true,
    ui: 'checkbox',
    label_question: 'Divorce',
  },
  {
    question_id: 'STRESSFUL_EVENT_BEREAVEMENT',
    required: true,
    ui: 'checkbox',
    label_question: 'Deuil',
  },
  {
    question_id: 'STRESSFUL_EVENT_HARASSMENT',
    required: true,
    ui: 'checkbox',
    label_question: 'Harcèlement',
  },
  {
    question_id: 'STRESSFUL_EVENT_MOVE',
    required: true,
    ui: 'checkbox',
    label_question: 'Déménagement',
  },
  {
    question_id: 'STRESSFUL_EVENT_FINANCIAL',
    required: true,
    ui: 'checkbox',
    label_question: 'Problèmes financiers',
  },
  {
    question_id: 'STRESSFUL_EVENT_SURGERY',
    required: true,
    ui: 'checkbox',
    label_question: 'Chirurgie',
  },
  {
    question_id: 'STRESSFUL_EVENT_ACCIDENT',
    required: true,
    ui: 'checkbox',
    label_question: 'Accident',
  },
  {
    question_id: 'DESIRE_TO_EAT',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous souvent envie ou besoin de manger au cours de la journée sans sensation de faim ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'DESIRE_TO_EAT_MOMENT',
    required: true,
    label_question: 'À quel moment de la journée ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: "Essentiellement en fin d'après midi ou en soirée",
          value: 0,
        },
        {
          label: "Cela peut être n'importe quand dans la journée",
          value: 1,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'DESIRE_TO_EAT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'SNACKING',
    required: true,
    ui: 'switch',
    label_question:
      'Vous arrive-t-il régulièrement de répondre à cette envie et de grignoter sans sensation de faim ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'DESIRE_TO_EAT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'ALCOHOL_RELAXED',
    required: true,
    ui: 'switch',
    label_question: "Vous sentez-vous détendue après avoir bu de l'alcool ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'ALCOHOL',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'ALCOHOL_WEEKLY_QTY',
        operator: '$gte',
        value: 14,
      },
    ],
    conditions_assoc: 'AND',
  },
  {
    question_id: 'MED_SPASMOPHILIA',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous des épisodes de tétanie ou de spasmophilie ?',
    tooltip: 'Contractions importantes et prolongées des muscles.',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MED_TINNITUS',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous des acouphènes ?',
    tooltip:
      "Sensation auditive de bourdonnement ou tintement qui n'est pas causée par un son extérieur.",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MED_PALPITATIONS',
    required: true,
    ui: 'switch',
    label_question: 'Faites-vous régulièrement de la tachycardie ?',
    tooltip:
      'Trouble du rythme cardiaque qui consiste en un accélération des battements du coeur (plus de 100 battements par minute).',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MED_HAIR_LOSS',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous constaté une perte anormale de cheveux ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MED_CRAMPS',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous fréquemment des crampes ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
];
