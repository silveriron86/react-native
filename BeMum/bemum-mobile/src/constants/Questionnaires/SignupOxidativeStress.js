module.exports = [
  {
    question_id: 'RESIDENCE_ENVIRONMENT',
    required: true,
    label_question: 'Vous vivez :',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'en ville (plus de 20 000 habitants)',
          value: 'urban',
        },
        {
          label: 'à la campagne',
          value: 'rural',
        },
      ],
    },
    value: 'rural',
  },
  {
    question_id: 'ENVIRONMENT_POLLUTED',
    required: true,
    ui: 'switch',
    label_question: 'Vivez-vous dans un environnement pollué ?',
    tooltip:
      "Grande ville, proche d'une usine chimique, d'une zone d'utilisation de pesticides, trafic routier important etc.",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'HIGH_SUN_EXPOSURE',
    required: true,
    ui: 'switch',
    label_question:
      'Dans le cadre de votre activité quotidienne, êtes-vous fortement exposée au soleil (travail en extérieur par exemple) ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'SUFFICIENT_SUN_EXPOSURE',
    required: true,
    ui: 'switch',
    label_question:
      'En moyenne, êtes-vous exposée au soleil plus de 30 min par jour ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'SPOTTED_SKIN',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous des tâches de soleil sur la peau ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'CICATRISING_DIFFICULTIES',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous des difficultés à cicatriser ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'REPEATED_INFECTIONS',
    required: true,
    ui: 'switch',
    label_question:
      "Souffrez-vous d'infections à répétition ou avez-vous été plus souvent malade ces derniers temps ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MUSCLE_OR_JOINT_PAIN',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous régulièrement des douleurs articulaires ou musculaires ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'BLOOD_CIRCULATION_ISSUES',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous des problèmes de circulation sanguine ?',
    tooltip: 'Problèmes de coagulation, jambes lourdes, varices...',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'HAIR_DULL_BRITTLE',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous les cheveux ternes ou cassants ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
];
