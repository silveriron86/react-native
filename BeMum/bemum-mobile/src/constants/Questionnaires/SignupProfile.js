module.exports = [
  {
    question_id: 'BIRTHDATE',
    required: true,
    ui: 'date',
    label_question: 'Quelle est votre date de naissance ?',
  },
  {
    question_id: 'DEPARTEMENT_OF_RESIDENCE',
    required: true,
    ui: 'stepper',
    ui_config: {
      min: 1,
      max: 101,
      precision: 1,
    },
    value: 1,
    label_question: 'Dans quel département résidez-vous ?',
    tooltip: 'Indiquez le numéro : 02, 10, 75...',
  },
  {
    question_id: 'PROFESSIONAL_ACTIVITY',
    required: true,
    ui: 'switch',
    label_question: 'Exercez-vous une activité professionnelle ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'PROFESSIONAL_ACTIVITY_INFO',
    required: false,
    ui: '',
    value: '',
    label_question: 'Quelle(s) propositions vous concernent ?',
    conditions: [
      {
        parentQuestionId: 'PROFESSIONAL_ACTIVITY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'PROFESSIONAL_ACTIVITY_HOME_OFFICE',
    required: false,
    ui: 'checkbox',
    label_question: 'Vous travaillez depuis votre domicile',
    conditions: [
      {
        parentQuestionId: 'PROFESSIONAL_ACTIVITY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'PROFESSIONAL_ACTIVITY_BUSINESS_TRIPS',
    required: false,
    ui: 'checkbox',
    label_question:
      'Votre profession demande beaucoup de déplacements sur plusieurs jours consécutifs',
    conditions: [
      {
        parentQuestionId: 'PROFESSIONAL_ACTIVITY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'PROFESSIONAL_ACTIVITY_STAGGERED_HOURS',
    required: false,
    ui: 'checkbox',
    label_question: 'Vous travaillez en horaires décalés',
    conditions: [
      {
        parentQuestionId: 'PROFESSIONAL_ACTIVITY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'PROFESSIONAL_ACTIVITY_SEDENTARY',
    required: false,
    ui: 'checkbox',
    label_question: 'Votre profession est sédentaire (bureau, voiture)',
    conditions: [
      {
        parentQuestionId: 'PROFESSIONAL_ACTIVITY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'PROFESSIONAL_ACTIVITY_ON_FOOT',
    required: false,
    ui: 'checkbox',
    label_question:
      'Votre profession demande beaucoup de déplacements à pieds ou de rester debout longtemps',
    conditions: [
      {
        parentQuestionId: 'PROFESSIONAL_ACTIVITY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'PROFESSIONAL_ACTIVITY_PHYSICAL',
    required: false,
    ui: 'checkbox',
    label_question: 'Votre profession demande des efforts physiques',
    conditions: [
      {
        parentQuestionId: 'PROFESSIONAL_ACTIVITY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'FOOD_SUPPLEMENT',
    required: true,
    ui: 'switch',
    label_question:
      'Consommez-vous des compléments alimentaires (hors BeMum) ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'FOOD_SUPPLEMENT_LABEL',
    require: true,
    ui: 'text',
    label_question: "Merci de préciser de quel type de compléments il s'agit :",
    ui_config: {
      max: 150,
    },
    conditions: [
      {
        parentQuestionId: 'FOOD_SUPPLEMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'COACHING_GOALS',
    required: false,
    ui: '',
    label_question:
      "Au cours de l'expérience BeMum, en plus de l'accompagnement au projet de grossesse, est-il important pour vous de :",
  },
  {
    question_id: 'COACHING_GOALS_WEIGHT_LOSS',
    required: false,
    ui: 'checkbox',
    label_question: 'Perdre du poids',
  },
  {
    question_id: 'COACHING_GOALS_IMPROVE_DIET',
    required: false,
    ui: 'checkbox',
    label_question: 'Améliorer vos habitudes alimentaires',
  },
  {
    question_id: 'COACHING_GOALS_BLOOD_TEST',
    required: false,
    ui: 'checkbox',
    label_question: 'Améliorer votre bilan biologique',
  },
  {
    question_id: 'COACHING_GOALS_LIFESTYLE',
    required: false,
    ui: 'checkbox',
    label_question: 'Améliorer vos habitudes de vie',
  },
  {
    question_id: 'COACHING_GOALS_ELSE',
    required: false,
    ui: 'checkbox',
    label_question: 'Autre',
    tooltip: "Votre nutritionniste l'abordera avec vous en détail.",
  },
  {
    question_id: 'INTRO_WEIGHT_SURVEY',
    required: false,
    ui: '',
    label_question:
      'Ces quelques questions sur votre historique de poids nous permettront de mieux orienter votre accompagnement :',
  },
  {
    question_id: 'HEIGHT',
    required: true,
    ui: 'stepper',
    label_question: 'Quelle est votre taille ?',
    tooltip: 'En cm (1,62 m = 162 cm)',
    ui_config: {
      min: 100,
      max: 250,
      precision: 1,
    },
    value: 162,
    unit: {
      short_name: 'cm',
    },
  },
  {
    question_id: 'WEIGHT',
    required: true,
    ui: 'stepper',
    label_question: 'Quel est votre poids actuel ?',
    tooltip: 'En kg',
    ui_config: {
      min: 40,
      max: 250,
      precision: 0.1,
    },
    value: 65,
    unit: {
      short_name: 'kg',
    },
  },
  {
    question_id: 'WAIST',
    required: true,
    ui: 'stepper',
    label_question: 'Quel est votre tour de taille ?',
    tooltip:
      "Votre tour de taille et votre tour de hanches participent à établir votre profil de coaching, et donc à vous fournir des recettes et un accompagnement adapté. Il est très important que vous rentriez des valeurs correctes. Pour ces mesures, tenez-vous debout, pieds écartés à la largeur des épaules, l'abdomen détendu et libéré de tout vêtement. À l'aide d'une main, repérez l'extrémité supérieure de vos hanches. Positionnez le ruban à mesurer juste au-dessus de cet endroit et, en respirant normalement, resserrez le ruban autour de votre taille tout en évitant qu'il ne s'enfonce dans la peau. Le tour de taille doit être mesuré à la fin d'une expiration normale, il est important de ne pas rentrer le ventre mais de le relâcher normalement.",
    ui_config: {
      min: 50,
      max: 150,
      precision: 0.5,
    },
    unit: {
      short_name: 'cm',
    },
  },
  {
    question_id: 'HIPS',
    required: false,
    ui: 'stepper',
    label_question: 'Quel est votre tour de hanches ?',
    tooltip:
      'Le tour de hanches doit être mesuré sur la partie la plus large des fesses. Prenez garde à ce que le ruban soit bien horizontal.',
    ui_config: {
      min: 50,
      max: 150,
      precision: 0.5,
    },
    unit: {
      short_name: 'cm',
    },
  },
  {
    question_id: 'CHILDHOOD_OVERWEIGHT',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous été en surpoids au cours de votre enfance ou de votre adolescence ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'WEIGHT_MAXIMUM',
    required: true,
    ui: 'stepper',
    label_question:
      "Quel est le poids le plus haut que vous ayez atteint à l'âge adulte ?",
    tooltip: 'En kg',
    ui_config: {
      min: 40,
      max: 250,
      precision: 0.1,
    },
    unit: {
      short_name: 'kg',
    },
  },
  {
    question_id: 'WEIGHT_MINIMUM',
    required: true,
    ui: 'stepper',
    label_question:
      'Quel est le poids le plus bas que vous ayez réussi à maintenir sans trop d’effort à l’âge adulte ?',
    tooltip: 'En kg',
    ui_config: {
      min: 40,
      max: 250,
      precision: 0.1,
    },
    unit: {
      short_name: 'kg',
    },
  },
  {
    question_id: 'INTRO_RISK_HABITS',
    required: false,
    ui: '',
    label_question:
      'Merci de répondre à ces questions concernant vos habitudes de vie :',
  },
  {
    question_id: 'SMOKER',
    required: true,
    ui: 'switch',
    label_question: 'Fumez-vous ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'SMOKER_DURATION',
    required: true,
    ui: 'stepper',
    label_question: "Depuis combien d'années fumez-vous ?",
    ui_config: {
      min: 1,
      max: 70,
      precision: 1,
    },
    conditions: [
      {
        parentQuestionId: 'SMOKER',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'SMOKER_DAILY_QTY',
    required: true,
    ui: 'stepper',
    label_question: 'Combien de cigarettes par jour fumez-vous actuellement ?',
    ui_config: {
      min: 1,
      max: 80,
      precision: 1,
    },
    conditions: [
      {
        parentQuestionId: 'SMOKER',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'SMOKER_CANNABIS',
    required: true,
    ui: 'switch',
    label_question: 'Consommez-vous du cannabis ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'SMOKER_CANNABIS_DAILY_QTY',
    required: true,
    ui: 'stepper',
    label_question:
      'Combien de doses de cannabis par semaine fumez-vous actuellement ?',
    ui_config: {
      min: 1,
      max: 80,
      precision: 1,
    },
    conditions: [
      {
        parentQuestionId: 'SMOKER_CANNABIS',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'ALCOHOL',
    required: true,
    ui: 'switch',
    label_question: "Consommez-vous de l'alcool ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'ALCOHOL_WEEKLY_QTY',
    required: true,
    ui: 'stepper',
    label_question:
      "Combien de verres d'alcool buvez-vous par semaine en moyenne ?",
    ui_config: {
      min: 1,
      max: 100,
      precision: 1,
    },
    conditions: [
      {
        parentQuestionId: 'ALCOHOL',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'CONSUMPTION_TEA',
    required: true,
    ui: 'switch',
    label_question: 'Consommez-vous du thé ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'CONSUMPTION_TEA_DAILY_QTY',
    required: true,
    ui: 'stepper',
    label_question: 'Combien de tasses de thé consommez-vous par jour ?',
    ui_config: {
      min: 1,
      max: 50,
      precision: 1,
    },
    conditions: [
      {
        parentQuestionId: 'CONSUMPTION_TEA',
        operator: '$eq',
        value: true,
      },
    ],
  },
];
