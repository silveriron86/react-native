module.exports = [
  {
    question_id: 'MED_HISTORY_INFERTILITY_DIAGNOSED',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous une infertilité diagnostiquée ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MED_HISTORY_INFERTILITY_DISEASES',
    required: false,
    ui: '',
    label_question:
      "Souffrez-vous de l'une des pathologies suivantes, associées à la fertilité ?",
  },
  {
    question_id: 'MED_HISTORY_ENDOMETRIOSIS',
    required: false,
    ui: 'checkbox',
    label_question: 'Endométriose',
  },
  {
    question_id: 'MED_HISTORY_PCOS',
    required: false,
    ui: 'checkbox',
    label_question: 'Syndrome des ovaires polykystiques',
  },
  {
    question_id: 'MED_HISTORY_OVARIAN_INSUFFICIENCY',
    required: false,
    ui: 'checkbox',
    label_question: 'Insuffisance ovarienne',
  },
  {
    question_id: 'MED_HISTORY_OVULATION_ISSUES',
    required: false,
    ui: 'checkbox',
    label_question: "Problèmes d'ovulation",
  },
  {
    question_id: 'MED_HISTORY_HORMONAL_ISSUES',
    required: false,
    ui: 'checkbox',
    label_question: 'Problèmes hormonaux',
  },
  {
    question_id: 'GENETICS_MTHFR',
    required: true,
    ui: 'switch',
    label_question: 'Vous a-t-on diagnostiqué une mutation sur le gène MTHFR ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MED_TREATMENT_INFERTILITY',
    required: true,
    ui: 'switch',
    label_question:
      'Votre médecin vous a-t-il prescrit un traitement pour votre infertilité ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'MED_HISTORY_INFERTILITY_DIAGNOSED',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_INFERTILITY_LABEL',
    require: true,
    ui: 'text',
    label_question:
      "Pouvez-vous nous indiquer de quel type de traitement il s'agit ?",
    ui_config: {
      max: 150,
    },
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT_INFERTILITY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_HORMONES',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous un bilan hormonal dont vous pourriez reporter les résultats ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'BLOOD_HORMONES_INTRO',
    required: false,
    ui: '',
    label_question:
      "Merci de rentrer avec soin la valeur correspondant à l'unité de référence.",
    conditions: [
      {
        parentQuestionId: 'BLOOD_HORMONES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_TESTOSTERONE_TOTAL',
    required: false,
    ui: 'stepper',
    label_question: 'Testostérone totale',
    ui_config: {
      min: 2000,
      max: 10000,
      precision: 1,
    },
    unit: {
      short_name: 'pg/mL',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_HORMONES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_TESTOSTERONE_FREE',
    required: false,
    ui: 'stepper',
    label_question: 'Testostérone libre',
    ui_config: {
      min: 2000,
      max: 10000,
      precision: 1,
    },
    unit: {
      short_name: 'pg/mL',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_HORMONES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_LH',
    required: false,
    ui: 'stepper',
    label_question: 'Hormone lutéinisante : LH',
    ui_config: {
      min: 0,
      max: 100,
      precision: 0.01,
    },
    unit: {
      short_name: 'mUI/mL',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_HORMONES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_ESTRADIOL',
    required: false,
    ui: 'stepper',
    label_question: 'Œstradiol',
    ui_config: {
      min: 2000,
      max: 10000,
      precision: 0.1,
    },
    unit: {
      short_name: 'pg/mL',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_HORMONES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_PROGESTERONE',
    required: false,
    ui: 'stepper',
    label_question: 'Progestérone',
    ui_config: {
      min: 0,
      max: 100,
      precision: 0.1,
    },
    unit: {
      short_name: 'ng/mL',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_HORMONES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_FSH',
    required: false,
    ui: 'stepper',
    label_question: 'Hormone folliculo-stimulante : FSH',
    ui_config: {
      min: 0,
      max: 20,
      precision: 0.01,
    },
    unit: {
      short_name: 'mUI/mL',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_HORMONES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_TSH',
    required: false,
    ui: 'stepper',
    label_question: 'Thyréostimuline : TSH',
    ui_config: {
      min: 0,
      max: 20,
      precision: 0.01,
    },
    unit: {
      short_name: 'mUI/mL',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_HORMONES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_AMH',
    required: false,
    ui: 'stepper',
    label_question: 'Hormone anti-mullërienne : AMH',
    ui_config: {
      min: 0,
      max: 20,
      precision: 0.01,
    },
    unit: {
      short_name: 'ng/mL',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_HORMONES',
        operator: '$eq',
        value: true,
      },
    ],
  },
];
