import {
  SignupProfileQuestions,
  SignupHealthQuestions,
  SignupReproductiveHealthQuestions,
  SignupPhysicalActivityQuestions,
  SignupSleepFatigueQuestions,
  SignupStressQuestions,
  SignupMoraleWellbeingQuestions,
  SignupMemoryConcentrationQuestions,
  SignupOxidativeStressQuestions,
} from './Questionnaires';

import {
  FollowupProfileQuestions,
  FollowupHealthQuestions,
  FollowupReproductiveHealthQuestions,
  FollowupPhysicalActivityQuestions,
  FollowupSleepFatigueQuestions,
  FollowupStressQuestions,
  FollowupMoraleWellbeingQuestions,
  FollowupMemoryConcentrationQuestions,
} from './Questionnaires';

module.exports = {
  CATEGORIES: [
    {
      title: 'Profil',
      form_id: 'profile',
      survey_id: 'profile',
      questions: SignupProfileQuestions,
    },
    {
      title: 'Santé',
      form_id: 'health',
      survey_id: 'health',
      questions: SignupHealthQuestions,
    },
    {
      title: 'Fertilité',
      form_id: 'reproductive_health',
      survey_id: 'reproductive',
      questions: SignupReproductiveHealthQuestions,
    },
    {
      title: 'Activité physique',
      form_id: 'physical_activity',
      survey_id: 'physical-activity',
      questions: SignupPhysicalActivityQuestions,
    },
    {
      title: 'Sommeil & fatigue',
      form_id: 'sleep_fatigue',
      survey_id: 'sleep',
      questions: SignupSleepFatigueQuestions,
    },
    {
      title: 'Stress',
      form_id: 'stress',
      survey_id: 'stress',
      questions: SignupStressQuestions,
    },
    {
      title: 'Moral & Bien-être Psychologique',
      form_id: 'moral_wellbeing',
      survey_id: 'mood',
      questions: SignupMoraleWellbeingQuestions,
    },
    {
      title: 'Mémoire & Concentration',
      form_id: 'memory_concentration',
      survey_id: 'memory',
      questions: SignupMemoryConcentrationQuestions,
    },
    {
      title: 'Stress Oxydant',
      form_id: 'oxydative_stress',
      survey_id: 'oxidative',
      questions: SignupOxidativeStressQuestions,
    },
  ],
  FOLLOWUP_CATEGORIES: [
    {
      title: 'Profil',
      form_id: 'profile',
      survey_id: 'profile',
      questions: FollowupProfileQuestions,
    },
    {
      title: 'Santé',
      form_id: 'health',
      survey_id: 'health',
      questions: FollowupHealthQuestions,
    },
    {
      title: 'Fertilité',
      form_id: 'reproductive_health',
      survey_id: 'reproductive',
      questions: FollowupReproductiveHealthQuestions,
    },
    {
      title: 'Activité physique',
      form_id: 'physical_activity',
      survey_id: 'physical-activity',
      questions: FollowupPhysicalActivityQuestions,
    },
    {
      title: 'Sommeil & fatigue',
      form_id: 'sleep_fatigue',
      survey_id: 'sleep',
      questions: FollowupSleepFatigueQuestions,
    },
    {
      title: 'Stress',
      form_id: 'stress',
      survey_id: 'stress',
      questions: FollowupStressQuestions,
    },
    {
      title: 'Moral & Bien-être Psychologique',
      form_id: 'moral_wellbeing',
      survey_id: 'mood',
      questions: FollowupMoraleWellbeingQuestions,
    },
    {
      title: 'Mémoire & Concentration',
      form_id: 'memory_concentration',
      survey_id: 'memory',
      questions: FollowupMemoryConcentrationQuestions,
    },
  ],
  COMPLETED_FOLLOWUP_SURVEY: 'COMPLETED_FOLLOWUP_SURVEY',
  COMPLETED_SURVEY: 'COMPLETED_SURVEY',
  LOADED_SURVEY: 'LOADED_SURVEY',
  GET_FORM_SUCCESS: 'SURVEY_GET_FORM_SUCCESS',
  GET_FORM_ERROR: 'SURVEY_GET_FORM_ERROR',
  POST_FORM_SUCCESS: 'SURVEY_POST_FORM_SUCCESS',
  POST_FORM_ERROR: 'SURVEY_POST_FORM_ERROR',
  UPDATE_FORM_DATA: 'UPDATE_FORM_DATA',
  GET_SEARCH_DATA_SUCCESS: 'GET_SEARCH_DATA_SUCCESS',
  GET_SEARCH_DATA_ERROR: 'GET_SEARCH_DATA_ERROR',
  GET_SURVEY_SUCCESS: 'GET_SURVEY_SUCCESS',
  GET_SURVEY_ERROR: 'GET_SURVEY_ERROR',
};
