import axios from 'axios';
import AccountConstants from '../constants/AccountConstants';
import ApiConstants from '../constants/ApiConstants';
import Utils from '../utils';

let AccountActions = {
  loginError: function(error) {
    return {
      error,
      type: AccountConstants.LOGIN_ERROR,
    };
  },

  loginSuccess: function(response) {
    return {
      response,
      type: AccountConstants.LOGIN_SUCCESS,
    };
  },

  login: function(args, cb) {
    const {email, password} = args;
    const data = JSON.stringify({
      email,
      password,
    });

    return dispatch => {
      return axios({
        method: 'POST',
        url: `${ApiConstants.getApiPath()}authentication/patient/jwt`,
        headers: Utils.getHeader(data),
        data,
      })
        .then(response => {
          if (cb) {
            cb({success: true, data: response.data});
          }
          dispatch(this.loginSuccess(response.data));
        })
        .catch(error => {
          if (cb) {
            cb({success: false, error});
          }
          dispatch(this.loginError(error));
        });
    };
  },

  forgotError: function(error) {
    return {
      error,
      type: AccountConstants.FORGOT_ERROR,
    };
  },

  forgotSuccess: function(response) {
    return {
      response,
      type: AccountConstants.FORGOT_SUCCESS,
    };
  },

  forgot: function(email, cb) {
    let data = JSON.stringify({
      email,
    });
    return dispatch => {
      return axios({
        method: 'POST',
        url: `${ApiConstants.getApiPath()}authentication/patient/forgot-password`,
        headers: Utils.getHeader(data),
        data,
      })
        .then(response => {
          if (cb) {
            cb({success: true, response});
          }
          dispatch(this.forgotSuccess(response.data));
        })
        .catch(error => {
          if (cb) {
            cb({success: false, error});
          }
          dispatch(this.forgotError(error));
        });
    };
  },

  updateError: function(error) {
    return {
      error,
      type: AccountConstants.UPDATE_ERROR,
    };
  },

  updateSuccess: function(response) {
    return {
      response,
      type: AccountConstants.UPDATE_SUCCESS,
    };
  },

  update: function(id, data, cb) {
    return dispatch => {
      return axios({
        method: 'PATCH',
        url: `${ApiConstants.getApiPath()}patients/${id}`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
        data,
      })
        .then(response => {
          if (cb) {
            cb(response.data);
          }
          dispatch(this.updateSuccess(response.data));
        })
        .catch(error => {
          if (cb) {
            cb(error);
          }
          dispatch(this.updateError(error));
        });
    };
  },

  getError: function(error) {
    return {
      error,
      type: AccountConstants.GET_ERROR,
    };
  },
  getSuccess: function(response) {
    return {
      response,
      type: AccountConstants.GET_SUCCESS,
    };
  },
  get: function(cb) {
    return dispatch => {
      return axios({
        method: 'GET',
        url: `${ApiConstants.getApiPath()}patients/${global.userID}`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          if (cb) {
            cb(response.data);
          }
          dispatch(this.getSuccess(response.data));
        })
        .catch(error => {
          if (cb) {
            cb(error);
          }
          dispatch(this.getError(error));
        });
    };
  },

  getMetricsError: function(error) {
    return {
      error,
      type: AccountConstants.GET_METRICS_ERROR,
    };
  },

  getMetricsSuccess: function(response) {
    return {
      response,
      type: AccountConstants.GET_METRICS_SUCCESS,
    };
  },

  getMetrics: function(cb) {
    return dispatch => {
      return axios({
        method: 'GET',
        url: `${ApiConstants.getApiPath()}patients/${
          global.userID
        }/metrics?limit=10&order_by_date=ASC&include=WEIGHT,BMI,PAL,HIPS,WAIST`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          cb(response.data);
          dispatch(this.getMetricsSuccess(response.data));
        })
        .catch(error => {
          cb(error);
          dispatch(this.getMetricsError(error));
        });
    };
  },

  getNutrinomesError: function(error) {
    return {
      error,
      type: AccountConstants.GET_NUTRINOMES_ERROR,
    };
  },

  getNutrinomesSuccess: function(response) {
    return {
      response,
      type: AccountConstants.GET_NUTRINOMES_SUCCESS,
    };
  },

  getNutrinomes: function(cb) {
    return dispatch => {
      return axios({
        method: 'GET',
        url: `${ApiConstants.getApiPath()}patients/${global.userID}/nutrinomes`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          dispatch(this.getNutrinomesSuccess(response.data));
          cb(response.data);
        })
        .catch(error => {
          dispatch(this.getNutrinomesError(error));
          cb(error);
        });
    };
  },

  updateIngredientsToAvoidError: function(error) {
    return {
      error,
      type: AccountConstants.UPDATE_INGREDIENTS_TO_AVOID_ERROR,
    };
  },

  updateIngredientsToAvoidSuccess: function(response) {
    return {
      response,
      type: AccountConstants.UPDATE_INGREDIENTS_TO_AVOID_SUCCESS,
    };
  },

  updateIngredientsToAvoid: function(data, cb) {
    return dispatch => {
      return axios({
        method: 'PUT',
        url: `${ApiConstants.getApiPath()}patients/${
          global.userID
        }/ingredients-to-avoid`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
        data,
      })
        .then(response => {
          if (cb) {
            cb(response.data);
          }
          dispatch(this.updateIngredientsToAvoidSuccess(response.data));
        })
        .catch(error => {
          if (cb) {
            cb(error);
          }
          dispatch(this.updateIngredientsToAvoidError(error));
        });
    };
  },
};

export default AccountActions;
