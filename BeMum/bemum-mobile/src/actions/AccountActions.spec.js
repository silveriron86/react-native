// import configureMockStore from 'redux-mock-store';
import createMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import AccountActions from './AccountActions';
import AccountConstants from '../constants/AccountConstants';
import ApiConstants from '../constants/ApiConstants';
// import rootReducer from '../reducers/rootReducer';

const mws = [thunk];
const mockStore = createMockStore(mws);
const store = mockStore({});
const mock = new MockAdapter(axios);

describe('POST Login actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'authentication/patient/jwt';
  const data = {
    email: 'tester@email.com',
    password: 'test',
  };
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: AccountConstants.LOGIN_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: AccountConstants.LOGIN_ERROR,
    error: {},
  };

  it('Login Error', () => {
    expect(AccountActions.loginError({})).toEqual(expectedErrorActions);
  });

  it('Login Success', () => {
    expect(AccountActions.loginSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches LOGIN_SUCCESS after a successful API requests', () => {
    mock.onPost(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(AccountActions.login(data)).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches LOGIN_ERROR after a FAILED API requests', () => {
    mock.onPost(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(AccountActions.login(data)).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});

describe('POST Forgot actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url =
    ApiConstants.getApiPath() + 'authentication/patient/forgot-password';
  const data = {
    email: 'silver1@email.com',
  };

  it('Dispatches FORGOT_SUCCESS after a successful API requests', () => {
    mock.onPost(url).reply(201, {response: {item: 'item1'}});

    const expectedActions = {
      type: AccountConstants.FORGOT_SUCCESS,
      response: {response: {item: 'item1'}},
    };

    store.dispatch(AccountActions.forgot(data)).then(() => {
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });

  it('dispatches FORGOT_ERROR after a FAILED API requests', () => {
    mock.onPost(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(AccountActions.forgot(data)).then(() => {
      let expectedActions = {
        type: AccountConstants.FORGOT_ERROR,
        payload: {error: {message: 'error message'}},
      };
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });
});

describe('PATCH update patient actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const id = 1;
  const url = `${ApiConstants.getApiPath()}patients/${id}`;
  const data = {
    firebaseTokens: 'test1234',
  };

  it('Dispatches UPDATE_SUCCESS after a successful API requests', () => {
    mock.onPatch(url).reply(201, {response: {item: 'item1'}});

    const expectedActions = {
      type: AccountConstants.UPDATE_SUCCESS,
      response: {response: {item: 'item1'}},
    };

    store.dispatch(AccountActions.update(id, data)).then(() => {
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });

  it('dispatches UPDATE_ERROR after a FAILED API requests', () => {
    mock.onPatch(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(AccountActions.update(id, data)).then(() => {
      let expectedActions = {
        type: AccountConstants.UPDATE_ERROR,
        payload: {error: {message: 'error message'}},
      };
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });
});

describe('GET get patient actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const id = 1;
  const url = `${ApiConstants.getApiPath()}patients/${id}`;

  it('Dispatches GET_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});

    store.dispatch(AccountActions.get(id)).then(() => {
      expect(store.getActions()).toContainEqual({
        type: AccountConstants.GET_SUCCESS,
        response: {response: {item: 'item1'}},
      });
    });
  });

  it('dispatches GET_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(AccountActions.get(id)).then(() => {
      expect(store.getActions()).toContainEqual({
        type: AccountConstants.GET_ERROR,
        payload: {error: {message: 'error message'}},
      });
    });
  });
});

describe('GET get metrics actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const id = 1;
  const url = `${ApiConstants.getApiPath()}patients/${id}/metrics`;

  it('Dispatches GET_METRICS_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});

    store.dispatch(AccountActions.getMetrics()).then(() => {
      expect(store.getActions()).toContainEqual({
        type: AccountConstants.GET_METRICS_SUCCESS,
        response: {response: {item: 'item1'}},
      });
    });
  });

  it('dispatches GET_METRICS_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    const expectedActions = {
      type: AccountConstants.GET_METRICS_ERROR,
      payload: {error: {message: 'error message'}},
    };
    store.dispatch(AccountActions.getMetrics()).then(() => {
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });
});

describe('GET get nutrinomes actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const id = 1;
  const url = `${ApiConstants.getApiPath()}patients/${id}/nutrinomes`;

  it('Dispatches GET_NUTRINOMES_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});

    const expectedActions = {
      type: AccountConstants.GET_NUTRINOMES_SUCCESS,
      response: {response: {item: 'item1'}},
    };

    store.dispatch(AccountActions.getNutrinomes()).then(() => {
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });

  it('dispatches GET_NUTRINOMES_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    const expectedActions = {
      type: AccountConstants.GET_NUTRINOMES_ERROR,
      payload: {error: {message: 'error message'}},
    };
    store.dispatch(AccountActions.getNutrinomes()).then(() => {
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });
});

describe('PUT update ingredients to avoid actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const id = 1;
  const url = `${ApiConstants.getApiPath()}patients/${id}/ingredients-to-avoid`;
  const data = {test: 'test'};

  it('Dispatches UPDATE_INGREDIENTS_TO_AVOID_SUCCESS after a successful API requests', () => {
    mock.onPut(url).reply(201, {response: {item: 'item1'}});

    store.dispatch(AccountActions.updateIngredientsToAvoid(data)).then(() => {
      expect(store.getActions()).toContainEqual({
        type: AccountConstants.UPDATE_INGREDIENTS_TO_AVOID_SUCCESS,
        response: {response: {item: 'item1'}},
      });
    });
  });

  it('dispatches UPDATE_INGREDIENTS_TO_AVOID_ERROR after a FAILED API requests', () => {
    mock.onPut(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(AccountActions.updateIngredientsToAvoid(data)).then(() => {
      expect(store.getActions()).toContainEqual({
        type: AccountConstants.UPDATE_INGREDIENTS_TO_AVOID_ERROR,
        payload: {error: {message: 'error message'}},
      });
    });
  });
});
