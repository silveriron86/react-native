import axios from 'axios';
import ApiConstants from '../constants/ApiConstants';
import {RecipeConstants} from '../constants';

const RecipeActions = {
  getError: function(error) {
    return {
      error,
      type: RecipeConstants.GET_RECIPE_ERROR,
    };
  },

  getSuccess: function(response) {
    return {
      response,
      type: RecipeConstants.GET_RECIPE_SUCCESS,
    };
  },

  get: function(recipeId, cb) {
    return dispatch => {
      return axios({
        method: 'GET',
        url: `${ApiConstants.getApiPath()}recipes/${recipeId}`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          dispatch(this.getSuccess(response.data));
          cb(response.data);
        })
        .catch(error => {
          dispatch(this.getError(error));
          cb({success: false, error});
        });
    };
  },

  dislikeError: function(error) {
    return {
      error,
      type: RecipeConstants.DISLIKE_ERROR,
    };
  },

  dislikeSuccess: function(response) {
    return {
      response,
      type: RecipeConstants.DISLIKE_SUCCESS,
    };
  },

  dislike: function(recipeId, data, cb) {
    return dispatch => {
      return axios({
        method: 'POST',
        url: `${ApiConstants.getApiPath()}recipes/${recipeId}/dislike?patientId=${
          global.userID
        }`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
        data,
      })
        .then(response => {
          dispatch(this.dislikeSuccess(response.data));
          cb({success: true, data: response.data});
        })
        .catch(error => {
          dispatch(this.dislikeError(error));
          cb({success: false, error});
        });
    };
  },

  unDislikeError: function(error) {
    return {
      error,
      type: RecipeConstants.UN_DISLIKE_ERROR,
    };
  },

  unDislikeSuccess: function(response) {
    return {
      response,
      type: RecipeConstants.UN_DISLIKE_SUCCESS,
    };
  },

  unDislike: function(recipeId, cb) {
    return dispatch => {
      return axios({
        method: 'DELETE',
        url: `${ApiConstants.getApiPath()}recipes/${recipeId}/dislike?patientId=${
          global.userID
        }`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          dispatch(this.unDislikeSuccess(response.data));
          cb({success: true, result: response.data});
        })
        .catch(error => {
          dispatch(this.unDislikeError(error));
          cb({success: false, error});
        });
    };
  },
};

export default RecipeActions;
