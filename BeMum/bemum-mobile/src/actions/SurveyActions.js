import axios from 'axios';
import SurveyConstants from '../constants/SurveyConstants';
import ApiConstants from '../constants/ApiConstants';

let SurveyActions = {
  completedSurvey: function(index) {
    return {
      type: SurveyConstants.COMPLETED_SURVEY,
      response: index,
    };
  },

  completedFollowupSurvey: function(index) {
    return {
      type: SurveyConstants.COMPLETED_FOLLOWUP_SURVEY,
      response: index,
    };
  },

  setLoadedSurveys: function(val) {
    return {
      type: SurveyConstants.LOADED_SURVEY,
      response: val,
    };
  },

  updateFormData: function(data, cb) {
    return dispatch => {
      dispatch({
        type: SurveyConstants.UPDATE_FORM_DATA,
        response: data,
      });
      cb(data);
    };
  },

  getFormError: function(error) {
    return {
      error,
      type: SurveyConstants.GET_FORM_ERROR,
    };
  },

  getFormSuccess: function(response) {
    return {
      response,
      type: SurveyConstants.GET_FORM_SUCCESS,
    };
  },

  getForm: function(type, survey_id, form_id, cb) {
    let url = `${ApiConstants.getApiPath()}survey/${
      type === 'signup' ? 'sign-up' : 'follow-up'
    }/${survey_id}`;

    if (form_id === 'preference_food') {
      url = `${ApiConstants.getApiPath()}survey/food-preference`;
    }

    return dispatch => {
      return axios({
        method: 'GET',
        url: `${url}?patientId=${global.userID}`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          dispatch(
            this.getFormSuccess({
              type: type,
              id: form_id,
              data: response.data,
            }),
          );
          cb(response.data);
        })
        .catch(error => {
          dispatch(this.getFormError(error.response));
          cb(error.response);
        });
    };
  },

  postFormError: function(error) {
    return {
      error,
      type: SurveyConstants.POST_FORM_ERROR,
    };
  },

  postFormSuccess: function(response) {
    return {
      response,
      type: SurveyConstants.POST_FORM_SUCCESS,
    };
  },

  postForm: function(data, cb) {
    let url = `${ApiConstants.getApiPath()}survey/${
      data.type === 'signup' ? 'sign-up' : 'follow-up'
    }/${data.surveyId}`;

    if (data.id === 'preference_food') {
      //if BREAKFAST is 'week' or 'always' ==> BREAKFAST_WEEK_CHANGE must be null
      //if BREAKFAST is 'weekend' or 'no' ==> BREAKFAST_WEEK_CHANGE must be true or false
      if (data.form.BREAKFAST === 'week' || data.form.BREAKFAST === 'always') {
        data.form.BREAKFAST_WEEK_CHANGE = null;
      }
      if (data.form.BREAKFAST === 'weekend' || data.form.BREAKFAST === 'no') {
        if (
          typeof data.form.BREAKFAST_WEEK_CHANGE === 'undefined' ||
          data.form.BREAKFAST_WEEK_CHANGE === null
        ) {
          data.form.BREAKFAST_WEEK_CHANGE = false;
        }
      }
      url = `${ApiConstants.getApiPath()}survey/food-preference`;
      // console.log(data);
    } else if (data.type === 'signup' && data.surveyId === 'profile') {
      // when alcohol is false, then alcohol weekly qty must be null
      // when alcohol is true, alcohol weekly qty can be different than null
      if (data.form.ALCOHOL !== true) {
        data.form.ALCOHOL_WEEKLY_QTY = null;
      }
    } else if (data.type === 'signup' && data.surveyId === 'reproductive') {
      if (data.form.MED_HISTORY_INFERTILITY_DIAGNOSED === true) {
        if (
          typeof data.form.MED_TREATMENT_INFERTILITY_LABEL === 'undefined' ||
          data.form.MED_TREATMENT_INFERTILITY_LABEL === null
        ) {
          data.form.MED_TREATMENT_INFERTILITY_LABEL = '';
        }
      } else {
        data.form.MED_TREATMENT_INFERTILITY_LABEL = null;
      }
    }

    let body = JSON.parse(JSON.stringify(data.form));
    // eslint-disable-next-line no-unused-vars
    for (const [key, value] of Object.entries(data.form)) {
      if (value === null) {
        delete body[key];
      }
    }

    // console.log(url);
    // console.log(body);

    return dispatch => {
      const token = global.accessToken;
      const user_id = global.userID;
      if (
        data.type === 'signup' &&
        data.id !== 'preference_food' &&
        data.method === 'PUT'
      ) {
        url = `${ApiConstants.getApiPath()}survey/sign-up/${
          data.surveyId
        }?patientId=${user_id}`;
      }
      const surveyData = {
        ...body,
        ...{
          patient: user_id,
        },
      };
      for (var propName in surveyData) {
        if (
          surveyData[propName] === null ||
          surveyData[propName] === undefined
        ) {
          delete surveyData[propName];
        }
      }
      return axios({
        url,
        method:
          data.type === 'signup' && data.id !== 'preference_food'
            ? data.method
            : 'POST',
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
        data: surveyData,
      })
        .then(response => {
          dispatch(this.postFormSuccess(response.data));
          cb(response.data);
        })
        .catch(error => {
          dispatch(this.postFormError(error.response));
          cb(error.response);
        });
    };
  },
};

export default SurveyActions;
