import axios from 'axios';
import ApiConstants from '../constants/ApiConstants';
import {ShoppingConstants} from '../constants';

const ShoppingActions = {
  getListError: function(error) {
    return {
      error,
      type: ShoppingConstants.GET_SHOPPING_LIST_ERROR,
    };
  },

  getListSuccess: function(response) {
    return {
      response,
      type: ShoppingConstants.GET_SHOPPING_LIST_SUCCESS,
    };
  },

  getList: function(dates, cb) {
    return dispatch => {
      return axios({
        method: 'GET',
        url: `${ApiConstants.getApiPath()}shopping-list?dates=${dates}&patientId=${
          global.userID
        }`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          dispatch(this.getListSuccess(response.data));
          cb({success: true, data: response.data});
        })
        .catch(error => {
          dispatch(this.getListError(error));
          cb({success: false, error});
        });
    };
  },
};

export default ShoppingActions;
