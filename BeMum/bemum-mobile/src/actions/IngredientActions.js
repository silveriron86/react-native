import axios from 'axios';
import ApiConstants from '../constants/ApiConstants';
import {IngredientConstants} from '../constants';

const IngredientActions = {
  getIngredientsError: function(error) {
    return {
      error,
      type: IngredientConstants.GET_INGREDIENTS_ERROR,
    };
  },

  getIngredientsSuccess: function(response) {
    return {
      response,
      type: IngredientConstants.GET_INGREDIENTS_SUCCESS,
    };
  },

  getIngredients: function(cb) {
    return dispatch => {
      return axios({
        method: 'GET',
        url: `${ApiConstants.getApiPath()}ingredients`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          dispatch(this.getIngredientsSuccess(response.data));
          cb(response.data);
        })
        .catch(error => {
          dispatch(this.getIngredientsError(error));
          cb(error);
        });
    };
  },
};

export default IngredientActions;
