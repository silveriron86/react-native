import createMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import ShoppingActions from './ShoppingActions';
import {ShoppingConstants} from '../constants';

const mws = [thunk];
const mockStore = createMockStore(mws);
const store = mockStore({});
const mock = new MockAdapter(axios);

describe('GET Shopping List Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'shopping-list';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: ShoppingConstants.GET_SHOPPING_LIST_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: ShoppingConstants.GET_SHOPPING_LIST_ERROR,
    error: {},
  };

  it('Get Shopping List Error', () => {
    expect(ShoppingActions.getListError({})).toEqual(expectedErrorActions);
  });

  it('Get Shopping List Success', () => {
    expect(ShoppingActions.getListSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches GET_SHOPPING_LIST_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(ShoppingActions.getList()).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches GET_SHOPPING_LIST_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(ShoppingActions.getList()).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});
