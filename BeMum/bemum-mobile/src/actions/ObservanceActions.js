import axios from 'axios';
import ApiConstants from '../constants/ApiConstants';
import {ObservanceConstants} from '../constants';

const ObservanceActions = {
  // List observance data
  getListError: function(error) {
    return {
      error,
      type: ObservanceConstants.GET_OBSERVANCE_LIST_ERROR,
    };
  },
  getListSuccess: function(response) {
    return {
      response,
      type: ObservanceConstants.GET_OBSERVANCE_LIST_SUCCESS,
    };
  },
  getList: function(cb) {
    return dispatch => {
      return axios({
        method: 'GET',
        url: `${ApiConstants.getApiPath()}observance?patientId=${
          global.userID
        }`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          dispatch(this.getListSuccess(response.data));
          cb({success: true, data: response.data});
        })
        .catch(error => {
          dispatch(this.getListError(error));
          cb({success: false, error});
        });
    };
  },

  // Describe observance data
  getError: function(error) {
    return {
      error,
      type: ObservanceConstants.GET_OBSERVANCE_ERROR,
    };
  },
  getSuccess: function(response) {
    return {
      response,
      type: ObservanceConstants.GET_OBSERVANCE_SUCCESS,
    };
  },
  get: function(date, cb) {
    return dispatch => {
      return axios({
        method: 'GET',
        url: `${ApiConstants.getApiPath()}observance/${date}?patientId=${
          global.userID
        }`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          dispatch(this.getSuccess(response.data));
          cb({success: true, data: response.data});
        })
        .catch(error => {
          dispatch(this.getError(error));
          cb({success: false, error});
        });
    };
  },

  // Record observance data
  postError: function(error) {
    return {
      error,
      type: ObservanceConstants.POST_OBSERVANCE_ERROR,
    };
  },
  postSuccess: function(response) {
    return {
      response,
      type: ObservanceConstants.POST_OBSERVANCE_SUCCESS,
    };
  },
  post: function(data, cb) {
    return dispatch => {
      return axios({
        method: 'POST',
        url: `${ApiConstants.getApiPath()}observance?patientId=${
          global.userID
        }`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
        data,
      })
        .then(response => {
          dispatch(this.postSuccess(response.data));
          cb({success: true, data: response.data});
        })
        .catch(error => {
          dispatch(this.postError(error));
          cb({success: false, error});
        });
    };
  },

  // Update observance data
  updateError: function(error) {
    return {
      error,
      type: ObservanceConstants.UPDATE_OBSERVANCE_ERROR,
    };
  },
  updateSuccess: function(response) {
    return {
      response,
      type: ObservanceConstants.UPDATE_OBSERVANCE_SUCCESS,
    };
  },
  update: function(data, cb) {
    return dispatch => {
      return axios({
        method: 'PATCH',
        url: `${ApiConstants.getApiPath()}observance?patientId=${
          global.userID
        }`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
        data,
      })
        .then(response => {
          dispatch(this.postSuccess(response.data));
          cb({success: true, data: response.data});
        })
        .catch(error => {
          dispatch(this.postError(error));
          cb({success: false, error});
        });
    };
  },
};

export default ObservanceActions;
