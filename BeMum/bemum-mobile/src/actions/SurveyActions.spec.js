import createMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import SurveyActions from './SurveyActions';
import {SurveyConstants} from '../constants';

const mws = [thunk];
const mockStore = createMockStore(mws);
const store = mockStore({});
const mock = new MockAdapter(axios);

describe('GET Form Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'survey/food-preference';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: SurveyConstants.GET_FORM_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: SurveyConstants.GET_FORM_ERROR,
    error: {},
  };

  it('Get Form Error', () => {
    expect(SurveyActions.getFormError({})).toEqual(expectedErrorActions);
  });

  it('Get Form Success', () => {
    expect(SurveyActions.getFormSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches GET_FORM_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(SurveyActions.getForm()).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches GET_FORM_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(SurveyActions.getForm()).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});

describe('POST Form Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'survey/food-preference';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: SurveyConstants.POST_FORM_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: SurveyConstants.POST_FORM_ERROR,
    error: {},
  };

  it('Post Form Error', () => {
    expect(SurveyActions.postFormError({})).toEqual(expectedErrorActions);
  });

  it('Post Form Success', () => {
    expect(SurveyActions.postFormSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  const data = {
    form: 'test',
    surveyId: 1,
    method: 'POST',
  };
  it('Dispatches POST_FORM_SUCCESS after a successful API requests', () => {
    mock.onPost(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(SurveyActions.postForm(data)).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches POST_FORM_ERROR after a FAILED API requests', () => {
    mock.onPost(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(SurveyActions.postForm(data)).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});
