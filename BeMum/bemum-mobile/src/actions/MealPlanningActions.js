import axios from 'axios';
import ApiConstants from '../constants/ApiConstants';
import {MealPlanningConstants} from '../constants';

const MealPlanningActions = {
  getError: function(error) {
    return {
      error,
      type: MealPlanningConstants.GET_MEAL_PLANNING_ERROR,
    };
  },

  getSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.GET_MEAL_PLANNING_SUCCESS,
    };
  },

  get: function(date, cb) {
    return dispatch => {
      return axios({
        method: 'GET',
        url: `${ApiConstants.getApiPath()}meal-planner?date=${date}&patientId=${
          global.userID
        }`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(response => {
          dispatch(this.getSuccess(response.data));
          cb({success: true, data: response.data});
        })
        .catch(error => {
          dispatch(this.getError(error));
          cb({success: false, error});
        });
    };
  },

  updateError: function(error) {
    return {
      error,
      type: MealPlanningConstants.UPDATE_MEAL_PLANNING_ERROR,
    };
  },

  updateSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.UPDATE_MEAL_PLANNING_SUCCESS,
    };
  },

  update: function(mealPlanningId, data, cb) {
    return dispatch => {
      return axios({
        method: 'PATCH',
        url: `${ApiConstants.getApiPath()}meal-planner/${mealPlanningId}`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
        data,
      })
        .then(response => {
          dispatch(this.updateSuccess(response.data));
          cb({success: true, data: response.data});
        })
        .catch(error => {
          dispatch(this.updateError(error));
          cb({success: false, error});
        });
    };
  },
};

export default MealPlanningActions;
