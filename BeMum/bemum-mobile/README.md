# BeMum Mobile

BeMum Mobile is a react native project built on RN 0.62.2

# Minimum OS compatibility

- iOS: 9.0
- Android: 4.1

## Installation

npm install\
cd ios\
pod install\
cd ..

## Usage

- How to build and test on iOS\

  - Install package and pod

    ```sh
    npm install
    ```

    ```sh
    cd ios && pod install
    ```

  - Archive & Upload\
    ![my-image-alt-text](./docs/archive1.png)
    ![my-image-alt-text](./docs/3.png)
    ![my-image-alt-text](./docs/4.png)
    ![my-image-alt-text](./docs/5.png)
    ![my-image-alt-text](./docs/6.png)
    ![my-image-alt-text](./docs/7.png)
    ![my-image-alt-text](./docs/8.png)
  - Approve the uploaded build\
    Visit https://appstoreconnect.apple.com/apps/1521451081/testflight/ios\
    ![my-image-alt-text](./docs/11.png)
    ![my-image-alt-text](./docs/12.png)
    ![my-image-alt-text](./docs/13.png)

- How to build and test on Android\

  - Refresh the updated resources

    ```sh
    react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/
    ```

  - Open Android Studio
  - Open an existing Android Studio project and select /bemum-mobile/android\
    ![my-image-alt-text](./docs/android2.png)
    ![my-image-alt-text](./docs/android3.png)
  - Generate APK file\
    ![my-image-alt-text](./docs/android4.png)
    ![my-image-alt-text](./docs/android5.png)
  - Pass the keystore file and password correctly\
    ![my-image-alt-text](./docs/android6.png)
  - Finish\
    ![my-image-alt-text](./docs/android7.png)
  - Generated result will be this:

    ```sh
    /build-mobile/android/app/release/apk-release.apk
    ```

## Caveats
- Android keystore and password are stored in notion
