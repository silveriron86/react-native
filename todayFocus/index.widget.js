import {AppRegistry, Platform, Text, View} from 'react-native';
const TodayWidget = () => (
  <View>
    <Text>Hello Today!</Text>
  </View>
);
if (Platform.OS === 'ios') {
  AppRegistry.registerComponent('FocusTodayWidget', () => TodayWidget);
}
