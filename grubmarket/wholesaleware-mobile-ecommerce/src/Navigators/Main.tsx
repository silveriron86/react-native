/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator, HeaderBackButton } from '@react-navigation/stack'
import { Image, View, Text, Platform } from 'react-native'
import { getFontSize, getViewSize } from '@/Containers/_styles'
import OrdersListContainer from '@/Containers/Main/Orders'
import OrderDetailContainer from '@/Containers/Main/Orders/details'
import CartContainer from '@/Containers/Main/Cart'
import ContactContainer from '@/Containers/Main/Contact'
import PriceSheetsDetailContainer from '@/Containers/Main/PriceSheets/detail'
import PriceSheetsListContainer from '@/Containers/Main/PriceSheets'
import ProfileContainer from '@/Containers/Main/Profile'
import PriceSheetsCartContainer from '@/Containers/Main/PriceSheets/cart'
import DeliveryAddressesContainer from '@/Containers/Main/Cart/details'
import styles from '@/Containers/_styles'
import CartNotesContainer from '@/Containers/Main/Cart/notes'
import ItemsContainer from '@/Containers/Main/Cart/items'
import OrderConfirmation from '@/Containers/Main/Cart/confirmation'
import AsyncStorage from '@react-native-async-storage/async-storage'
import PriceSheetsLoadingContainer from '@/Containers/Main/PriceSheets/loading'

const Stack = createStackNavigator()
const BottomTab = createBottomTabNavigator()
const isAndroid = Platform.OS === 'android'

const tabIcons = [
  require('../Assets/Images/bottom_icon1.png'),
  require('../Assets/Images/bottom_icon2.png'),
  require('../Assets/Images/bottom_icon3.png'),
  require('../Assets/Images/bottom_icon4.png'),
  require('../Assets/Images/bottom_icon5.png'),
]

const screenOptions: any = {
  headerBackTitleVisible: false,
  headerTitleStyle: {
    color: '#1C6E31',
    fontSize: getFontSize(20),
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
  },
  headerStyle: {
    height: 65,
    shadowRadius: 0,
    shadowOffset: {
      height: 0,
    },
  },
  headerBackTitleStyle: {
    color: '#0E161C',
  },
  headerTintColor: '#0E161C',
}

// @refresh reset
const MainNavigator = () => {
  const [badgeCount, setBadgeCount] = useState(0)
  useEffect(() => {
    updateBadge()
  })

  useEffect(() => {
    global.eventEmitter.addListener('CART_CHANGED', () => {
      console.log('***** CART_CHANGED')
      updateBadge()
    })
  }, [])

  const updateBadge = () => {
    AsyncStorage.getItem('CART', (_err, items) => {
      if (items) {
        let amount = 0
        JSON.parse(items).forEach((item: any) => {
          amount += item.count
        })
        setBadgeCount(amount)
      } else {
        setBadgeCount(0)
      }
    })
  }

  return (
    <BottomTab.Navigator
      initialRouteName="Products"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          const routes = ['Orders', 'Products', 'Cart', 'Contact', 'Profile']
          const index = routes.indexOf(route.name)
          return (
            <>
              <View
                style={{
                  width: getViewSize(20),
                  height: getViewSize(20),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Image
                  style={{
                    resizeMode: 'contain',
                    tintColor: color,
                    width: '100%',
                    height: '100%',
                  }}
                  source={tabIcons[index]}
                />
              </View>
              {index === 2 && badgeCount > 0 && (
                <View style={styles.badge}>
                  <Text style={styles.badgeText}>{badgeCount}</Text>
                </View>
              )}
            </>
          )
        },
      })}
      tabBarOptions={{
        activeTintColor: 'white',
        inactiveTintColor: '#9EB7A4',
        tabStyle: {
          backgroundColor: '#0D4B1C',
        },
        style: {
          height: 65,
          // height: 70,
          // borderWidth: 1,
        },
        labelStyle: {
          fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
          fontSize: getFontSize(9),
          fontWeight: isAndroid ? '500' : '700',
          marginBottom: getViewSize(6),
          // marginTop: getViewSize(20),
        },
      }}
    >
      <BottomTab.Screen name="Orders" component={OrdersStack} />
      <BottomTab.Screen name="Products" component={PriceSheetsStack} />
      <BottomTab.Screen name="Cart" component={CartStack} />
      <BottomTab.Screen name="Contact" component={ContactStack} />
      <BottomTab.Screen name="Profile" component={ProfileStack} />
    </BottomTab.Navigator>
  )
}

function OrdersStack() {
  const [title, setTitle] = useState('')
  const updateTitle = () => {
    AsyncStorage.getItem('COMPANY_NAME', (_err, cName) => {
      if (cName) {
        setTitle(cName)
      }
    })
  }
  useEffect(() => {
    updateTitle()
    global.eventEmitter.addListener('CHOSEN_COMPANY', () => {
      updateTitle()
    })
  }, [])

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen
        name="OrdersList"
        component={OrdersListContainer}
        options={({ route }) => ({ title })}
      />
      <Stack.Screen
        name="OrderDetail"
        component={OrderDetailContainer}
        options={({ route }) => ({ title })}
      />
    </Stack.Navigator>
  )
}

function PriceSheetsStack() {
  const [title, setTitle] = useState('')
  const updateTitle = () => {
    AsyncStorage.getItem('COMPANY_NAME', (_err, cName) => {
      if (cName) {
        setTitle(cName)
      }
    })
  }
  useEffect(() => {
    updateTitle()
    global.eventEmitter.addListener('CHOSEN_COMPANY', () => {
      updateTitle()
    })
  }, [])

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen
        name="PriceSheetsLoading"
        component={PriceSheetsLoadingContainer}
        options={{
          animationEnabled: false,
        }}
      />
      <Stack.Screen
        name="PriceSheetsList"
        component={PriceSheetsListContainer}
        options={({ route }) => ({ title })}
      />
      <Stack.Screen
        name="PriceSheetsDetail"
        component={PriceSheetsDetailContainer}
        options={({ route }) => ({ title })}
      />
      <Stack.Screen
        name="PriceSheetsCart"
        component={PriceSheetsCartContainer}
        options={({ route }) => ({ title })}
      />
    </Stack.Navigator>
  )
}

function CartStack() {
  const [title, setTitle] = useState('')
  const updateTitle = () => {
    AsyncStorage.getItem('COMPANY_NAME', (_err, cName) => {
      if (cName) {
        setTitle(cName)
      }
    })
  }
  useEffect(() => {
    updateTitle()
    global.eventEmitter.addListener('CHOSEN_COMPANY', () => {
      updateTitle()
    })
  }, [])

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen
        name="CartList"
        component={CartContainer}
        options={({ route }) => ({ title, animationEnabled: false })}
      />
      <Stack.Screen
        name="DeliveryAddresses"
        component={DeliveryAddressesContainer}
        options={({ route }) => ({ title })}
      />
      <Stack.Screen
        name="CartNotes"
        component={CartNotesContainer}
        options={({ route }) => ({ title })}
      />
      <Stack.Screen
        name="RequestItems"
        component={ItemsContainer}
        options={({ route }) => ({ title })}
      />
      <Stack.Screen
        name="PriceSheetsCart"
        component={PriceSheetsCartContainer}
        options={({ route }) => ({ title })}
      />
      <Stack.Screen
        name="OrderConfirmation"
        component={OrderConfirmation}
        options={({ route }) => ({ title })}
      />
      <Stack.Screen
        name="ViewOrder"
        component={OrderDetailContainer}
        options={({ route, navigation }) => ({
          title,
          headerLeft: props => (
            <HeaderBackButton
              {...props}
              onPress={() => {
                navigation.navigate('CartList')
              }}
            />
          ),
        })}
      />
    </Stack.Navigator>
  )
}

function ContactStack() {
  const [title, setTitle] = useState('')
  const updateTitle = () => {
    AsyncStorage.getItem('COMPANY_NAME', (_err, cName) => {
      if (cName) {
        setTitle(cName)
      }
    })
  }
  useEffect(() => {
    updateTitle()
    global.eventEmitter.addListener('CHOSEN_COMPANY', () => {
      updateTitle()
    })
  }, [])

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen
        name="Contact"
        component={ContactContainer}
        options={({ route }) => ({ title })}
      />
    </Stack.Navigator>
  )
}

function ProfileStack() {
  const [title, setTitle] = useState('')
  const updateTitle = () => {
    AsyncStorage.getItem('COMPANY_NAME', (_err, cName) => {
      if (cName) {
        setTitle(cName)
      }
    })
  }
  useEffect(() => {
    updateTitle()
    global.eventEmitter.addListener('CHOSEN_COMPANY', () => {
      updateTitle()
    })
  }, [])

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen
        name="Profile"
        component={ProfileContainer}
        options={({ route }) => ({ title })}
      />
    </Stack.Navigator>
  )
}

export default MainNavigator
