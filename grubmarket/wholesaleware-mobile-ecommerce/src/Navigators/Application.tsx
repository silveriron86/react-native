/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState, FunctionComponent } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { IndexStartupContainer } from '@/Containers'
import { useSelector } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'
import { navigationRef } from '@/Navigators/Root'
import { SafeAreaView, StatusBar } from 'react-native'
import { useTheme } from '@/Theme'
import { StartupState } from '@/Store/Startup'
import LoginContainer from '@/Containers/Auth/Login'
import ForgotContainer from '@/Containers/Auth/Forgot'
import CheckEmailContainer from '@/Containers/Auth/Forgot/check'
// import AsyncStorage from '@react-native-async-storage/async-storage'
import { AuthState } from '@/Store/Auth'
import _ from 'lodash'
import LoadingContainer from '@/Containers/Auth/Login/Loading'
import { SettingState } from '@/Store/Setting'

const Stack = createStackNavigator()

let MainNavigator: FunctionComponent | null

// @refresh reset
const ApplicationNavigator = () => {
  const { Layout, darkMode, NavigationTheme } = useTheme()
  // const { colors } = NavigationTheme
  const [isApplicationLoaded, setIsApplicationLoaded] = useState(false)
  const applicationIsLoading = useSelector(
    (state: { startup: StartupState }) => state.startup.loading,
  )

  const auth = useSelector((state: { auth: AuthState }) => state.auth.item)
  const { setting } = useSelector((state: { setting: SettingState }) => state)

  useEffect(() => {
    if (MainNavigator == null && !applicationIsLoading) {
      MainNavigator = require('@/Navigators/Main').default
      setIsApplicationLoaded(true)
    }
  }, [applicationIsLoading])

  // on destroy needed to be able to reset when app close in background (Android)
  useEffect(
    () => () => {
      setIsApplicationLoaded(false)
      MainNavigator = null
    },
    [],
  )

  const isLoggedIn = !_.isEmpty(auth)
  return (
    <SafeAreaView
      style={[
        Layout.fill,
        {
          backgroundColor:
            isLoggedIn && setting.viewedLogo ? '#0D4B1C' : '#1C6E31',
        },
      ]}
    >
      <NavigationContainer
        theme={{ ...NavigationTheme, colors: { background: 'white' } }}
        ref={navigationRef}
      >
        {/* <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'} /> */}
        <StatusBar barStyle="light-content" />
        <Stack.Navigator headerMode={'none'}>
          <Stack.Screen name="Startup" component={IndexStartupContainer} />
          {isApplicationLoaded && MainNavigator != null && (
            <>
              <Stack.Screen
                name="Login"
                component={LoginContainer}
                options={{
                  animationEnabled: false,
                }}
              />
              <Stack.Screen name="Forgot" component={ForgotContainer} />
              <Stack.Screen name="CheckEmail" component={CheckEmailContainer} />
              <Stack.Screen name="Loading" component={LoadingContainer} />
              <Stack.Screen
                name="Main"
                component={MainNavigator}
                options={
                  {
                    // animationEnabled: false,
                  }
                }
              />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  )
}

export default ApplicationNavigator
