import { StyleSheet, Platform } from 'react-native'
import { COLOR_GREEN, getFontSize, getViewSize } from '@/Containers/_styles'

const isAndroid = Platform.OS === 'android'
export default StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    width: '100%', //'Dimensions.get('window').width',
    height: '100%', //Dimensions.get('window').height,
    zIndex: 99999,
  },
  primaryButton: {
    backgroundColor: COLOR_GREEN,
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: getViewSize(2.5),
  },
  disabledButton: {
    backgroundColor: `${COLOR_GREEN}90`,
  },
  primaryButtonText: {
    color: 'white',
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    fontSize: getFontSize(14),
  },
  searcher: {
    position: 'relative',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  searchInputWrapper: {
    position: 'relative',
    flex: 1,
    height: 32,
  },
  searchInput: {
    flex: 1,
    backgroundColor: '#EDF2F5',
    color: '#98ABB6',
    height: 32,
    lineHeight: 18,
    borderRadius: 5,
    paddingHorizontal: 36,
    paddingVertical: 0,
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontWeight: '500',
    fontSize: getFontSize(16),
  },
  searchIcon: {
    position: 'absolute',
    left: 10,
    top: 7,
    width: getViewSize(16),
    height: getViewSize(16),
  },
  closeIconBtn: {
    position: 'absolute',
    right: 0,
    top: 0,
    height: 32,
    width: 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeIcon: {
    width: getViewSize(14),
    height: getViewSize(14),
    tintColor: '#98ABB6',
  },
  searchCancelBtn: {
    paddingLeft: 10,
  },
  searchCancelText: {
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontWeight: '500',
    fontSize: getFontSize(16),
    color: '#98ABB6',
  },
  filterBtn: {
    marginLeft: getViewSize(13),
  },
  filterIcon: {
    width: getViewSize(24),
    height: getViewSize(24),
  },
})
