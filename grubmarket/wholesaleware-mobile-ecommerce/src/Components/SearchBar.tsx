/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect, useRef } from 'react'
import {
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Text,
  Keyboard,
} from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
// import ActionSheet from 'react-native-actionsheet'
import styles from './_styles'
import { COLOR_GREEN, COLOR_LIGHT_GREEN } from '@/Containers/_styles'

const searchIcon = require('@/Assets/Images/search.png')
const filterIcon = require('@/Assets/Images/filter.png')
const closeIcon = require('@/Assets/Images/close.png')

interface Props {
  placeholder?: string
  onChange: any
  categories: string[]
  autoFocus: boolean
}

const SearchBar = ({ placeholder, categories, autoFocus, onChange }: Props) => {
  // const actionSheetRef = useRef(null)
  const filterRef = useRef(null)
  const [search, setSearch] = useState('')
  const [category, setCategory] = useState('')

  useEffect(() => {
    onChange(search, category)
  }, [onChange, search, category])

  const onChangeText = (text: string) => {
    setSearch(text)
  }

  const onClear = () => {
    setSearch('')
    Keyboard.dismiss()
  }

  // const showActionSheet = () => {
  //   if (actionSheetRef && actionSheetRef) {
  //     actionSheetRef.current!.show()
  //   }
  // }

  const onShowFilter = () => {
    if (filterRef) {
      filterRef.current!.togglePicker(true)
    }
  }

  let items = [] //[{ label: 'All categories', value: '' }]
  if (categories) {
    Object.keys(categories).forEach((cat: string) => {
      items.push({ label: cat, value: cat })
    })
  }

  return (
    <View style={{ padding: 16, width: '100%' }}>
      <View style={styles.searcher}>
        <View style={styles.searchInputWrapper}>
          <TextInput
            autoFocus={autoFocus}
            autoCapitalize="none"
            style={styles.searchInput}
            value={search}
            placeholder={placeholder}
            onChangeText={onChangeText}
            returnKeyType="search"
          />
          <Image source={searchIcon} style={styles.searchIcon} />
          {search !== '' && (
            <TouchableOpacity style={styles.closeIconBtn} onPress={onClear}>
              <Image source={closeIcon} style={styles.closeIcon} />
            </TouchableOpacity>
          )}
        </View>
        {search === '' && (
          <>
            {items.length > 0 && (
              <TouchableOpacity style={styles.filterBtn} onPress={onShowFilter}>
                <Image
                  source={filterIcon}
                  style={[
                    styles.filterIcon,
                    category !== '' && {
                      tintColor: 'white',
                      backgroundColor: COLOR_LIGHT_GREEN,
                    },
                  ]}
                />
              </TouchableOpacity>
            )}
          </>
        )}
      </View>
      {/*
      <ActionSheet
        ref={actionSheetRef}
        title={'Filter by category'}
        options={['All Categories', 'Fruits', 'Vegetables']}
        // cancelButtonIndex={2}
        // destructiveButtonIndex={1}
        onPress={index => {
          console.log('pressed action = ', index)
        }}
      />
      */}

      <RNPickerSelect
        value={category}
        placeholder={{ label: 'All categories', value: '' }}
        useNativeAndroidPickerStyle={true}
        style={{ viewContainer: { opacity: 0, height: 0 } }}
        ref={filterRef}
        onValueChange={setCategory}
        items={items}
        doneText="Save"
      />
    </View>
  )
}

export default SearchBar
