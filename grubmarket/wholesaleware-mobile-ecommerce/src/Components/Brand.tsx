/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { View, Image } from 'react-native'
import { useTheme } from '@/Theme'
import { getViewSize } from '@/Containers/_styles'

interface Props {
  height?: number | string
  width?: number | string
  mode?: 'contain' | 'cover' | 'stretch' | 'repeat' | 'center'
}

const logoImg = require('../Assets/Images/logo.png')

const Brand = () => {
  return (
    <View
      style={{
        marginTop: getViewSize(30),
        width: '100%',
        alignItems: 'center',
      }}
    >
      <Image
        style={{ height: 48, width: 241 }}
        source={logoImg}
        resizeMode="contain"
      />
    </View>
  )
}

export default Brand
