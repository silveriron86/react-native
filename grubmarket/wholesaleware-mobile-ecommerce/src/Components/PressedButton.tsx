/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { TouchableHighlight, Text } from 'react-native'
import authStyles from '@/Containers/Auth/_styles'
import { COLOR_GREEN, COLOR_GREEN_DARK } from '@/Containers/_styles'

export default function PressedButton({ label, onPress }: any) {
  var [isPress, setIsPress] = React.useState(false)

  var touchProps = {
    activeOpacity: 1,
    underlayColor: COLOR_GREEN_DARK,
    style: authStyles.button,
    onHideUnderlay: () => setIsPress(false),
    onShowUnderlay: () => setIsPress(true),
    onPress,
  }

  return (
    <TouchableHighlight {...touchProps}>
      <Text
        style={[authStyles.btnText, { color: isPress ? 'white' : COLOR_GREEN }]}
      >
        {label}
      </Text>
    </TouchableHighlight>
  )
}
