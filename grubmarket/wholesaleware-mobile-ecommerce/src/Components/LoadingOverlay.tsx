/* eslint-disable react/require-default-props */
import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import styles from './_styles'

interface Props {
  loading: boolean
  containerStyle?: any
}

const LoadingOverlay = ({ loading, containerStyle }: Props) => {
  return loading ? (
    <View style={[styles.loading, containerStyle]}>
      <ActivityIndicator size={'large'} color="white" />
    </View>
  ) : null
}

export default LoadingOverlay
