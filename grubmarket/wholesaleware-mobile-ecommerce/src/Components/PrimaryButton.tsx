/* eslint-disable react/require-default-props */
import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import styles from './_styles'

interface Props {
  text: string
  onPress: any
  disabled?: boolean
}

const PrimaryButton = ({ text, onPress, disabled }: Props) => {
  return (
    <TouchableOpacity
      style={[styles.primaryButton, disabled === true && styles.disabledButton]}
      onPress={onPress}
      disabled={disabled}
    >
      <Text style={styles.primaryButtonText}>{text}</Text>
    </TouchableOpacity>
  )
}

export default PrimaryButton
