import AsyncStorage from '@react-native-async-storage/async-storage'
import { buildSlice } from '@thecodingmachine/redux-toolkit-wrapper'
import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper'
import { cloneDeep } from 'lodash'
import { createOrder } from '@/Services/Order'

let sliceInitialState = {
  cartItems: [],
  getCartItems: {
    loading: false,
    error: null,
  },
  addCartItem: {
    loading: true,
    error: null,
  },
  removeCartItem: {
    loading: true,
    error: null,
  },
  fillCartItems: {
    loading: true,
    error: null,
  },
  createOrder: {
    loading: false,
    error: null,
  },
  newOrder: null,
}

export const getCartItemsModule = {
  initialState: buildAsyncState('getCartItems'),
  action: buildAsyncActions('getCartItems', async (_args, { dispatch }) => {
    const items = await AsyncStorage.getItem('CART')
    if (items) {
      return JSON.parse(items)
    }
    return []
  }),
  reducers: buildAsyncReducers({
    itemKey: 'cartItems',
    errorKey: 'getCartItems.error',
    loadingKey: 'getCartItems.loading',
  }),
}

export const createOrderModule = {
  initialState: buildAsyncState('createOrder'),
  action: buildAsyncActions('createOrder', createOrder),

  reducers: buildAsyncReducers({
    itemKey: 'newOrder',
    errorKey: 'createOrder.error',
    loadingKey: 'createOrder.loading',
  }),
}

export const resetNewOrder = {
  initialState: buildAsyncState('resetNewOrder'),
  action: buildAsyncActions('resetNewOrder', async () => {
    return null
  }),
  reducers: buildAsyncReducers({
    itemKey: 'newOrder',
  }),
}

export const resetCartModule = {
  initialState: buildAsyncState('resetCart'),
  action: buildAsyncActions('resetCart', async () => {
    AsyncStorage.removeItem('CART')
    return []
  }),
  reducers: buildAsyncReducers({
    itemKey: 'cartItems',
    errorKey: 'resetCart.error',
    loadingKey: 'resetCart.loading',
  }),
}

export const addCartItemModule = {
  initialState: buildAsyncState('addCartItem'),
  action: buildAsyncActions('addCartItem', async (cartItem: any) => {
    const items = await AsyncStorage.getItem('CART')
    let cartItems = items ? JSON.parse(items) : []
    if (typeof cartItem.index !== 'undefined') {
      // change qty
      cartItems[cartItem.index] = {
        ...cartItems[cartItem.index],
        ...cartItem,
      }
      delete cartItem.index
    } else {
      console.log('cartItem.wholesaleItemId = ', cartItem)
      const found = cartItems.findIndex((c: any) => {
        if (c.priceSheetItem && cartItem.priceSheetItem) {
          return (
            c.priceSheetItem.priceSheetItemId ===
            cartItem.priceSheetItem.priceSheetItemId
          )
        } else {
          if (cartItem.wholesaleItemId === null) {
            return c.variety === cartItem.variety
          }
          return c.wholesaleItemId === cartItem.wholesaleItemId
        }
      })
      if (found >= 0) {
        cartItems[found] = cloneDeep(cartItem)
      } else {
        cartItems.push(cartItem)
      }
    }
    AsyncStorage.setItem('CART', JSON.stringify(cartItems))
    return cartItems
  }),
  reducers: buildAsyncReducers({
    itemKey: 'cartItems',
    errorKey: 'addCartItem.error',
    loadingKey: 'addCartItem.loading',
  }),
}

export const fillCartItemsModule = {
  initialState: buildAsyncState('fillCartItems'),
  action: buildAsyncActions('fillCartItems', async (cartItems: any) => {
    AsyncStorage.setItem('CART', JSON.stringify(cartItems))
    global.eventEmitter.emit('CART_CHANGED')
    return cartItems
  }),
  reducers: buildAsyncReducers({
    itemKey: 'cartItems',
    errorKey: 'fillCartItems.error',
    loadingKey: 'fillCartItems.loading',
  }),
}

export const removeCartItemModule = {
  initialState: buildAsyncState('removeCartItem'),
  action: buildAsyncActions('removeCartItem', async (index: any) => {
    const items = await AsyncStorage.getItem('CART')
    let cartItems = items ? JSON.parse(items) : []
    cartItems.splice(index, 1)
    AsyncStorage.setItem('CART', JSON.stringify(cartItems))
    global.eventEmitter.emit('CART_CHANGED')
    return cartItems
  }),
  reducers: buildAsyncReducers({
    itemKey: 'cartItems',
    errorKey: 'removeCartItem.error',
    loadingKey: 'removeCartItem.loading',
  }),
}

export default buildSlice(
  'cart',
  [
    getCartItemsModule,
    addCartItemModule,
    removeCartItemModule,
    fillCartItemsModule,
    createOrderModule,
    resetNewOrder,
    resetCartModule,
  ],
  sliceInitialState,
).reducer

export interface CartState {
  newOrder: object
  cartItems: object[]
  getCartItems: {
    loading: boolean
    error: any
  }
  addCartItem: {
    loading: boolean
    error: any
  }
  removeCartItem: {
    loading: boolean
    error: any
  }
  fillCartItems: {
    loading: boolean
    error: any
  }
  createOrder: {
    loading: boolean
    error: any
  }
}
