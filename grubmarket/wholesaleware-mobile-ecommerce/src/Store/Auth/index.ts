import { buildSlice } from '@thecodingmachine/redux-toolkit-wrapper'
import Login from './Login'
import Logout from './Logout'
import Init from './Init'
import Forgot, { InitReset } from './Forgot'

// This state is common to all the "auth" module, and can be modified by any "auth" reducers
let sliceInitialState = {
  item: {},
}

export default buildSlice('auth', [Login, Logout, Init], sliceInitialState)
  .reducer

export interface AuthState {
  item: {
    name: string
  }
  login: {
    loading: boolean
    error: any
  }
}

export const ForgotReducer = buildSlice(
  'forgot',
  [Forgot, InitReset],
  sliceInitialState,
).reducer

export interface ForgotState {
  item: {
    name: string
  }
  forgot: {
    loading: boolean
    error: any
  }
}
