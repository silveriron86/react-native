import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper'
import { resetPasswordService } from '@/Services/Auth'

export default {
  initialState: buildAsyncState('forgot'),
  action: buildAsyncActions('forgot', resetPasswordService),
  reducers: buildAsyncReducers({
    errorKey: 'forgot.error',
    loadingKey: 'forgot.loading',
  }),
}

export const InitReset = {
  initialState: buildAsyncState('init'),
  action: buildAsyncActions('init', async (_args, { dispatch }) => {
    return {}
  }),
  reducers: buildAsyncReducers({
    errorKey: 'init.error',
    loadingKey: 'init.loading',
  }),
}
