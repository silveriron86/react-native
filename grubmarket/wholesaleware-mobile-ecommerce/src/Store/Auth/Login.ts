import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper'
import { loginByTokenService } from '@/Services/Auth'

export default {
  initialState: buildAsyncState('login'),
  action: buildAsyncActions('login', loginByTokenService),
  reducers: buildAsyncReducers({
    errorKey: 'login.error',
    loadingKey: 'login.loading',
  }),
}
