import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper'
import { logOutService } from '@/Services/Auth'

export default {
  initialState: buildAsyncState('logout'),
  action: buildAsyncActions('logout', logOutService),
  reducers: buildAsyncReducers({
    errorKey: 'logout.error',
    loadingKey: 'logout.loading',
  }),
}
