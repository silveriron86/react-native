import {
  buildAsyncState,
  buildAsyncActions,
  buildAsyncReducers,
} from '@thecodingmachine/redux-toolkit-wrapper'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default {
  initialState: buildAsyncState('init'),
  action: buildAsyncActions('init', async (_args, { dispatch }) => {
    // await new Promise(resolve => setTimeout(resolve, 1000))
    const auth = await AsyncStorage.getItem('AUTH')
    return auth ? JSON.parse(auth) : {}
  }),
  reducers: buildAsyncReducers({
    errorKey: 'init.error',
    loadingKey: 'init.loading',
  }),
}
