import { buildSlice } from '@thecodingmachine/redux-toolkit-wrapper'
import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper'
import { getLogo, getCompanyAddress } from '@/Services/Setting'
import { getUser } from '@/Services/Account'

export const getLogoModule = {
  initialState: buildAsyncState('logo'),
  action: buildAsyncActions('logo', getLogo),
  reducers: buildAsyncReducers({
    itemKey: 'logoImage',
    errorKey: 'logo.error',
    loadingKey: 'logo.loading',
  }),
}

export const viewLogoModule = {
  initialState: buildAsyncState('viewLogo'),
  action: buildAsyncActions('viewLogo', async (arg: boolean) => {
    return arg
  }),
  reducers: buildAsyncReducers({
    itemKey: 'viewedLogo',
    errorKey: 'logo.error',
    loadingKey: 'logo.loading',
  }),
}

export const getCompanyAddressModule = {
  initialState: buildAsyncState('getCompanyAddress'),
  action: buildAsyncActions('getCompanyAddress', getCompanyAddress),
  reducers: buildAsyncReducers({
    itemKey: 'companyAddress',
    errorKey: 'getCompanyAddress.error',
    loadingKey: 'getCompanyAddress.loading',
  }),
}

export const getUserModule = {
  initialState: buildAsyncState('getUser'),
  action: buildAsyncActions('getUser', getUser),
  reducers: buildAsyncReducers({
    itemKey: 'user',
    errorKey: 'getUser.error',
    loadingKey: 'getUser.loading',
  }),
}

// This state is common to all the "auth" module, and can be modified by any "auth" reducers
let sliceInitialState = {
  viewedLogo: false,
  logoImage: null,
  logo: {
    loading: false,
    error: null,
  },
  companyAddress: [],
  getCompanyAddress: {
    loading: false,
    error: null,
  },
  user: null,
  getUser: {
    loading: false,
    error: null,
  },
}

export default buildSlice(
  'setting',
  [getLogoModule, viewLogoModule, getCompanyAddressModule, getUserModule],
  sliceInitialState,
).reducer

export interface SettingState {
  viewedLogo: boolean
  logoImage: object
  logo: {
    loading: boolean
    error: any
  }
  companyAddress: any[]
  getCompanyAddress: {
    loading: boolean
    error: any
  }
  user: any
  getUser: {
    loading: boolean
    error: any
  }
}
