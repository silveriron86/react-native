import { buildSlice } from '@thecodingmachine/redux-toolkit-wrapper'
import AsyncStorage from '@react-native-async-storage/async-storage'
import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper'
import {
  getPriceSheetItems,
  getCompanyPriceSheets,
  getCustomersPricesheets,
} from '@/Services/Pricing'

let sliceInitialState = {
  saleItems: [],
  getSaleItems: {
    loading: true,
    error: null,
  },
  priceSheetItems: null,
  getPriceSheetItems: {
    loading: true,
    error: null,
  },
  getCustomersPriceSheets: {
    loading: true,
    error: null,
  },
  customerPriceSheets: [],
  selectedPriceSheet: null,
  selectPriceSheet: {
    loading: true,
    error: null,
  },
}

export const selectPriceSheetModule = {
  initialState: buildAsyncState('selectPriceSheet'),
  action: buildAsyncActions('selectPriceSheet', async (ps: any) => {
    const json = JSON.stringify(ps)
    AsyncStorage.setItem('CURRENT_PRICESHEET', json)
    return JSON.parse(json)
  }),
  reducers: buildAsyncReducers({
    itemKey: 'selectedPriceSheet',
    errorKey: 'selectPriceSheet.error',
    loadingKey: 'selectPriceSheet.loading',
  }),
}

export const getSaleItemsModule = {
  initialState: buildAsyncState('getSaleItems'),
  action: buildAsyncActions('getSaleItems', getCompanyPriceSheets),

  reducers: buildAsyncReducers({
    itemKey: 'saleItems',
    errorKey: 'getSaleItems.error',
    loadingKey: 'getSaleItems.loading',
  }),
}

export const getCustomersPriceSheetsModule = {
  initialState: buildAsyncState('getCustomersPriceSheets'),
  action: buildAsyncActions('getCustomersPriceSheets', getCustomersPricesheets),

  reducers: buildAsyncReducers({
    itemKey: 'customerPriceSheets',
    errorKey: 'getCustomersPriceSheets.error',
    loadingKey: 'getCustomersPriceSheets.loading',
  }),
}

export const getPriceSheetItemsModule = {
  initialState: buildAsyncState('getPriceSheetItems'),
  action: buildAsyncActions('getPriceSheetItems', getPriceSheetItems),

  reducers: buildAsyncReducers({
    itemKey: 'priceSheetItems',
    errorKey: 'getPriceSheetItems.error',
    loadingKey: 'getPriceSheetItems.loading',
  }),
}

export default buildSlice(
  'pricing',
  [
    getSaleItemsModule,
    getPriceSheetItemsModule,
    getCustomersPriceSheetsModule,
    selectPriceSheetModule,
  ],
  sliceInitialState,
).reducer

export interface PricingState {
  saleItems: object[]
  getSaleItems: {
    loading: boolean
    error: any
  }
  getPriceSheetItems: {
    loading: boolean
    error: any
  }
  priceSheetItems: object
  getCustomersPriceSheets: {
    loading: boolean
    error: any
  }
  customerPriceSheets: object[]
  selectedPriceSheet: object
  selectPriceSheet: {
    loading: boolean
    error: any
  }
}
