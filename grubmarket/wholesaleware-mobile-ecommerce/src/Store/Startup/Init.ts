import {
  buildAsyncState,
  buildAsyncActions,
  buildAsyncReducers,
} from '@thecodingmachine/redux-toolkit-wrapper'
import { navigateAndSimpleReset } from '@/Navigators/Root'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default {
  initialState: buildAsyncState(),
  action: buildAsyncActions('startup/init', async (_args, { dispatch }) => {
    AsyncStorage.getItem('AUTH', (_err, val) => {
      navigateAndSimpleReset(val ? 'Loading' : 'Login')
    })
  }),
  reducers: buildAsyncReducers({ itemKey: null }),
}
