import { buildSlice } from '@thecodingmachine/redux-toolkit-wrapper'
import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper'
import {
  getOrdersForCustomerUserId,
  getWarehouseSalesOrdersByDate,
  getOrderItemsById,
  getAddresses,
  duplicateOrder,
  getOrderOneOffItemsById,
  getCustomerItems,
  getOrderByToken,
  getOrderItemsByToken,
} from '@/Services/Order'
import { getUserSetting } from '@/Services/Setting'

let sliceInitialState = {
  newOrderId: -1,
  orders: null,
  sellerSetting: null,
  getOrders: {
    loading: false,
    error: null,
  },
  orderItems: [],
  getOrderItemsByToken: {
    loading: true,
    error: null,
  },
  addresses: [],
  getAddresses: {
    loading: false,
    error: null,
  },
  duplicateOrder: {
    loading: false,
    error: null,
  },
  getSellerSetting: {
    loading: false,
    error: null,
  },
  orderOneOffItems: [],
  getOrderOneOffItems: {
    loading: false,
    error: null,
  },
  customerItems: [],
  getCustomerItems: {
    loading: false,
    error: null,
  },
  order: null,
  getOrderByToken: {
    loading: false,
    error: null,
  },
}

export const getOrdersForCustomerUserIdModule = {
  initialState: buildAsyncState('getOrders'),
  action: buildAsyncActions('getOrdersForCustomer', getOrdersForCustomerUserId),
  reducers: buildAsyncReducers({
    itemKey: 'orders',
    errorKey: 'getOrders.error',
    loadingKey: 'getOrders.loading',
  }),
}

export const getWarehouseSalesOrdersByDateModule = {
  initialState: buildAsyncState('getOrders'),
  action: buildAsyncActions(
    'getWarehouseSalesOrders',
    getWarehouseSalesOrdersByDate,
  ),

  reducers: buildAsyncReducers({
    itemKey: 'orders',
    errorKey: 'getOrders.error',
    loadingKey: 'getOrders.loading',
  }),
}

export const getOrderItemsByIdModule = {
  initialState: buildAsyncState('getOrderItems'),
  action: buildAsyncActions('getOrderItemsById', getOrderItemsById),

  reducers: buildAsyncReducers({
    itemKey: 'orderItems',
    errorKey: 'getOrderItems.error',
    loadingKey: 'getOrderItems.loading',
  }),
}

export const getOrderOneOffItemsByIdModule = {
  initialState: buildAsyncState('getOrderOneOffItems'),
  action: buildAsyncActions('getOrderOneOffItemsById', getOrderOneOffItemsById),

  reducers: buildAsyncReducers({
    itemKey: 'orderOneOffItems',
    errorKey: 'getOrderOneOffItems.error',
    loadingKey: 'getOrderOneOffItems.loading',
  }),
}

export const getCustomerItemsModule = {
  initialState: buildAsyncState('getCustomerItems'),
  action: buildAsyncActions('getCustomerItems', getCustomerItems),

  reducers: buildAsyncReducers({
    itemKey: 'customerItems',
    errorKey: 'getCustomerItems.error',
    loadingKey: 'getCustomerItems.loading',
  }),
}

export const initOrderItems = {
  initialState: buildAsyncState('initOrderItems'),
  action: buildAsyncActions('initOrderItems', async (_args, { dispatch }) => {
    return _args
  }),
  reducers: buildAsyncReducers({
    itemKey: 'orderItems',
  }),
}

export const initOrderOneOffItems = {
  initialState: buildAsyncState('initOrderOneOffItems'),
  action: buildAsyncActions(
    'initOrderOneOffItems',
    async (_args, { dispatch }) => {
      return _args
    },
  ),
  reducers: buildAsyncReducers({
    itemKey: 'orderOneOffItems',
  }),
}

export const getAddressesModule = {
  initialState: buildAsyncState('getAddresses'),
  action: buildAsyncActions('getAddresses', getAddresses),

  reducers: buildAsyncReducers({
    itemKey: 'addresses',
    errorKey: 'getAddresses.error',
    loadingKey: 'getAddresses.loading',
  }),
}

export const getSellerSettingModule = {
  initialState: buildAsyncState('getSellerSetting'),
  action: buildAsyncActions('getSellerSetting', getUserSetting),

  reducers: buildAsyncReducers({
    itemKey: 'sellerSetting',
    errorKey: 'getSellerSetting.error',
    loadingKey: 'getSellerSetting.loading',
  }),
}

export const duplicateOrderModule = {
  initialState: buildAsyncState('duplicateOrder'),
  action: buildAsyncActions('duplicateOrder', duplicateOrder),

  reducers: buildAsyncReducers({
    itemKey: 'newOrderId',
    errorKey: 'duplicateOrder.error',
    loadingKey: 'duplicateOrder.loading',
  }),
}

export const resetNewOrderId = {
  initialState: buildAsyncState('resetNewOrderId'),
  action: buildAsyncActions('resetNewOrderId', async () => {
    return -1
  }),
  reducers: buildAsyncReducers({
    itemKey: 'newOrderId',
  }),
}

export const getOrderByTokenModule = {
  initialState: buildAsyncState('getOrderByToken'),
  action: buildAsyncActions('getOrderByToken', getOrderByToken),

  reducers: buildAsyncReducers({
    itemKey: 'order',
    errorKey: 'getOrderByToken.error',
    loadingKey: 'getOrderByToken.loading',
  }),
}

export const getOrderItemsByTokenModule = {
  initialState: buildAsyncState('getOrderItemsByToken'),
  action: buildAsyncActions('getOrderItemsByToken', getOrderItemsByToken),

  reducers: buildAsyncReducers({
    itemKey: 'orderItems',
    errorKey: 'getOrderItemsByToken.error',
    loadingKey: 'getOrderItemsByToken.loading',
  }),
}

export default buildSlice(
  'order',
  [
    getOrdersForCustomerUserIdModule,
    getWarehouseSalesOrdersByDateModule,
    getOrderItemsByIdModule,
    getOrderOneOffItemsByIdModule,
    getAddressesModule,
    duplicateOrderModule,
    getSellerSettingModule,
    initOrderItems,
    resetNewOrderId,
    getCustomerItemsModule,
    getOrderByTokenModule,
    getOrderItemsByTokenModule,
  ],
  sliceInitialState,
).reducer

export interface OrderState {
  newOrderId: number
  orders: object
  sellerSetting: object
  getOrders: {
    loading: boolean
    error: any
  }
  orderItems: object[]
  getOrderItemsByToken: {
    loading: boolean
    error: any
  }
  addresses: object[]
  getAddresses: {
    loading: boolean
    error: any
  }
  duplicateOrder: {
    loading: boolean
    error: any
  }
  getSellerSetting: {
    loading: boolean
    error: any
  }
  orderOneOffItems: any[]
  getOrderOneOffItems: {
    loading: boolean
    error: any
  }
  customerItems: any[]
  getCustomerItems: {
    loading: boolean
    error: any
  }
  order: any
  getOrderByToken: {
    loading: boolean
    error: any
  }
}
