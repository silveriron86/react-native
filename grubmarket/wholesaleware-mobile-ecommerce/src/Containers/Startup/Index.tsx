/* eslint-disable react/react-in-jsx-scope */
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { View } from 'react-native'
import InitStartup from '@/Store/Startup/Init'
import Init from '@/Store/Auth/Init'
import CommonStyles from '../Auth/_styles'

const IndexStartupContainer = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(Init.action())
    dispatch(InitStartup.action())
  }, [dispatch])

  return <View style={CommonStyles.screen} />
}

export default IndexStartupContainer
