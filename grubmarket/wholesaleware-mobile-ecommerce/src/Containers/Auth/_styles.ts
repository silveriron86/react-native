import { StyleSheet, Platform } from 'react-native'
import { COLOR_GREEN, COLOR_GREEN_DARK, getFontSize, getViewSize } from '../_styles'

const isAndroid = Platform.OS === 'android'

export default StyleSheet.create({
  screen: {
    backgroundColor: COLOR_GREEN,
    width: '100%',
    height: '100%',
    paddingHorizontal: getViewSize(23),
    paddingBottom: getViewSize(28),
  },
  fullScreen: {
    width: '100%',
    height: '100%',
  },
  noPadding: {
    paddingHorizontal: 0,
    paddingBottom: 0,
  },
  logoTitle: {
    position: 'absolute',
    top: getViewSize(52),
    alignSelf: 'center',
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    fontSize: getFontSize(30),
    color: 'white',
    // height: 68,
    textAlign: 'center',
  },
  screenTitle: {
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    // fontStyle: 700,
    fontSize: getFontSize(32),
    color: 'white',
    marginBottom: getViewSize(23),
  },
  description: {
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontWeight: 'normal',
    fontSize: getFontSize(14),
    color: 'white',
    textAlign: 'center',
    width: 280,
  },
  mainArea: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  padBottom: {
    height: '20%',
  },
  input: {
    height: getViewSize(40),
    width: '100%',
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontSize: getFontSize(14),
    backgroundColor: 'white',
    marginBottom: getViewSize(8),
    paddingHorizontal: getViewSize(8.65),
    borderRadius: getViewSize(3),
  },
  button: {
    backgroundColor: 'white',
    borderRadius: getViewSize(3),
    height: getViewSize(36),
    justifyContent: 'center',
    width: '100%',
  },
  btnText: {
    color: COLOR_GREEN,
    fontSize: getFontSize(14),
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    textAlign: 'center',
  },
  link: {
    alignSelf: 'center',
    marginTop: getViewSize(14),
  },
  linkText: {
    color: 'white',
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontSize: getFontSize(14),
  },
  msgWrapper: {
    marginVertical: getViewSize(10),
  },
  msg: {
    color: '#FC706F',
    fontSize: getFontSize(14),
    lineHeight: getFontSize(15),
  },
  loadingDots: {
    width: getViewSize(32 * 1.2),
    height: getViewSize(8 * 1.2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    resizeMode: 'contain',
  },
  dot: {
    width: getViewSize(6),
    height: getViewSize(6),
    borderRadius: getViewSize(3),
    backgroundColor: 'white',
  },
  currentDot: {
    width: getViewSize(8),
    height: getViewSize(8),
    borderRadius: getViewSize(4),
  },
  rememberMe: {
    marginVertical: 16,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkedBtn: {
    width: 22,
    height: 22,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 3,
  },
  checkedIcon: {
    width: 15,
    height: 15,
    tintColor: COLOR_GREEN,
  },
})
