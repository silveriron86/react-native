/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react'
import {
  View,
  // StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Image,
} from 'react-native'
import styles from '@/Containers/Auth/_styles'
import { COLOR_GREEN_DARK, COLOR_PLACEHOLDER, getViewSize } from '../../_styles'
import { navigateAndSimpleReset } from '@/Navigators/Root'
import { useSelector, useDispatch } from 'react-redux'
import { AuthState } from '@/Store/Auth'
import Login from '@/Store/Auth/Login'
import _ from 'lodash'
import LoadingOverlay from '@/Components/LoadingOverlay'
import { Brand } from '@/Components'
import PressedButton from '@/Components/PressedButton'
import AsyncStorage from '@react-native-async-storage/async-storage'

const checkedIcon = require('../../../Assets/Images/checked.png')

const LoginContainer = (props: any) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [message, setMessage] = useState('')
  const [toggleCheckBox, setToggleCheckBox] = useState(false)

  const dispatch = useDispatch()

  const auth = useSelector((state: { auth: AuthState }) => state.auth.item)
  const loginLoading = useSelector(
    (state: { auth: AuthState }) => state.auth.login.loading,
  )
  const loginError = useSelector(
    (state: { auth: AuthState }) => state.auth.login.error,
  )

  useEffect(() => {
    if (!_.isEmpty(auth)) {
      navigateAndSimpleReset('Loading')
    }
  }, [auth])

  useEffect(() => {
    if (loginError) {
      setMessage('Incorrect email or password')
    }
  }, [loginError])

  useEffect(() => {
    AsyncStorage.getItem('REMEMBERED_EMAIL', (_err, value) => {
      if (value) {
        setToggleCheckBox(true)
        setEmail(value)
      }
    })
  }, [])

  const goForgot = () => {
    props.navigation.navigate('Forgot')
  }

  const handelLogin = () => {
    if (email && password) {
      if (toggleCheckBox) {
        AsyncStorage.setItem('REMEMBERED_EMAIL', email)
      }
      setMessage('')

      dispatch(
        Login.action({
          email,
          password,
        }),
      )
    }

    if (!email) {
      setMessage('Email is required!')
    } else if (!password) {
      setMessage('Password is required!')
    }
  }

  return (
    <KeyboardAvoidingView style={[styles.screen, styles.noPadding]}>
      <View style={styles.screen}>
        <Brand />
        <View style={styles.mainArea}>
          <Text style={[styles.screenTitle, { marginBottom: getViewSize(57) }]}>
            Login
          </Text>
          <TextInput
            value={email}
            style={[styles.input]}
            placeholder="Email"
            textContentType="username"
            placeholderTextColor={COLOR_PLACEHOLDER}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            onChangeText={setEmail}
          />
          <TextInput
            value={password}
            style={[styles.input, { fontFamily: 'Arial' }]}
            placeholder="Password"
            textContentType="password"
            placeholderTextColor={COLOR_PLACEHOLDER}
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            onChangeText={setPassword}
          />
          <PressedButton label="Log in" onPress={handelLogin} />
          {message !== '' && (
            <View style={styles.msgWrapper}>
              <Text style={styles.msg}>{message}</Text>
            </View>
          )}
          <TouchableOpacity
            style={styles.rememberMe}
            onPress={() => setToggleCheckBox(!toggleCheckBox)}
          >
            {/* <CheckBox
              tintColor="white"
              boxType="square"
              onFillColor="white"
              onTintColor="white"
              onCheckColor={COLOR_GREEN_DARK}
              value={toggleCheckBox}
              lineWidth={3}
              onValueChange={newValue => setToggleCheckBox(newValue)}
            /> */}
            <View style={styles.checkedBtn}>
              {toggleCheckBox && (
                <Image source={checkedIcon} style={styles.checkedIcon} />
              )}
            </View>
            <Text style={[styles.linkText, { marginLeft: 8 }]}>
              Remember me
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.link} onPress={goForgot}>
            <Text style={styles.linkText}>Forgot your password?</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.padBottom} />
      </View>
      <LoadingOverlay loading={loginLoading} />
    </KeyboardAvoidingView>
  )
}

export default LoginContainer
