/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { Image, Platform } from 'react-native'
import {
  getViewSize,
  getFontSize,
  COLOR_BORDER,
  COLOR_DARK,
} from '../../_styles'
import RNPickerSelect from 'react-native-picker-select'

const arrowDown = require('../../../Assets/Images/arrow_down.png')
const isAndroid = Platform.OS === 'android'

const NumberPicker = (props: any) => {
  const { defaultValue, onChange } = props

  let items = [
    { label: '0', value: 0 },
  ]
  for (let i = 1; i <= 999; i++) {
    items.push({ label: i.toString(), value: i })
  }

  return (
    <RNPickerSelect
      placeholder={{}}
      value={defaultValue}
      style={{
        viewContainer: {
          width: '100%',
          height: getViewSize(35),
          justifyContent: 'center',
          paddingHorizontal: 0,
        },
        placeholder: {
          fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
          fontWeight: '500',
          fontSize: getFontSize(14),
          color: COLOR_BORDER,
        },
        inputIOS: {
          fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
          fontWeight: '500',
          fontSize: getFontSize(14),
          color: COLOR_DARK,
        },
        inputAndroid: {
          fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
          fontWeight: '500',
          fontSize: getFontSize(14),
          color: COLOR_DARK,
        },
      }}
      // textInputProps={{}}
      onValueChange={onChange}
      items={items}
      Icon={() => {
        return (
          <Image
            source={arrowDown}
            style={{
              width: 12.5,
              height: 8,
              marginTop: 5,
              resizeMode: 'stretch',
            }}
          />
        )
      }}
    />
  )
}

export default NumberPicker
