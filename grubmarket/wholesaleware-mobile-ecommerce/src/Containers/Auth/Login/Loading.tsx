/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react'
import { SafeAreaView, View, Text, Image } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import ImageProgress from 'react-native-image-progress'
import * as Progress from 'react-native-progress'
import _ from 'lodash'
import styles from '../_styles'
import { navigateAndSimpleReset } from '@/Navigators/Root'
import { getLogoModule, SettingState, viewLogoModule } from '@/Store/Setting'
import { AuthState } from '@/Store/Auth'
import { COLOR_GREEN } from '@/Containers/_styles'
import Logout from '@/Store/Auth/Logout'
import { CommonActions } from '@react-navigation/native'
import Layout from '@/Theme/Layout'
import {
  getCustomersPriceSheetsModule,
  getPriceSheetItemsModule,
  PricingState,
} from '@/Store/Pricing'
import Config from '@/Config'
import AsyncStorage from '@react-native-async-storage/async-storage'

// const productLogo = require('../../../Assets/Images/product_logo.png')
const loadingGif = require('../../../Assets/Images/loading.gif')

const LoadingContainer = (props: any) => {
  const dispatch = useDispatch()
  const [priceSheet, setPriceSheet] = useState(null)
  const { auth, setting, pricing } = useSelector(
    (state: {
      auth: AuthState
      setting: SettingState
      pricing: PricingState
    }) => state,
  )
  useEffect(() => {
    dispatch(getCustomersPriceSheetsModule.action(auth.item.userId))
    // dispatch(getLogoModule.action())

    global.eventEmitter.addListener('401_ERROR', () => {
      dispatch(Logout.action())
    })
  }, [])

  useEffect(() => {
    if (!pricing.getPriceSheetItems.loading && pricing.priceSheetItems) {
      setTimeout(() => {
        if (!_.isEmpty(auth.item)) {
          console.log(pricing.priceSheetItems)
          const { company } = pricing.priceSheetItems.sellerSetting
          // AsyncStorage.setItem('COMPANY_NAME', company.companyName)
          dispatch(viewLogoModule.action(true))
        }
      }, 5000)
    }
  }, [pricing.getPriceSheetItems])

  useEffect(() => {
    if (pricing.customerPriceSheets.length > 0) {
      let firstItem = pricing.customerPriceSheets[0]
      setPriceSheet(firstItem)
    }
  }, [pricing.customerPriceSheets])

  useEffect(() => {
    if (priceSheet) {
      dispatch(getPriceSheetItemsModule.action(priceSheet.linkToken))
    }
  }, [priceSheet])

  useEffect(() => {
    if (!pricing.getCustomersPriceSheets.loading) {
      if (setting.viewedLogo) {
        AsyncStorage.setItem(
          'PRICESHEETS_LEN',
          pricing.customerPriceSheets.length.toString(),
        )
        navigateAndSimpleReset('Main')
      }
    }
  }, [
    setting.viewedLogo,
    pricing.getCustomersPriceSheets.loading,
    pricing.customerPriceSheets,
  ])

  useEffect(() => {
    if (_.isEmpty(auth.item)) {
      props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        }),
      )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth])

  // const { logoImage } = setting
  let logoImage = null
  let companyName = ''
  const { customerPriceSheets } = pricing
  if (customerPriceSheets) {
    let all: any[] = []
    customerPriceSheets.forEach((cp: any) => {
      const result = _.groupBy(cp.assignedClients, (client: any) => {
        return client.wholesaleClient.wholesaleCompany.companyId
      })
      Object.keys(result).forEach(k => {
        all.push(result[k][0])
      })
    })
    console.log('all = ', all)
    const result = _.groupBy(all, (client: any) => {
      return client.wholesaleClient.wholesaleCompany.companyId
    })

    if (Object.keys(result).length === 1) {
      console.log(all[0])
      logoImage = all[0].companyImagePath.replace(
        'http://staging.grubmarket.com',
        Config.getBaseURL(),
      )
      companyName = all[0].wholesaleClient.wholesaleCompany.companyName
    }
  }

  return (
    <View style={styles.screen}>
      <View style={[styles.mainArea]}>
        {companyName !== '' && (
          <Text
            style={[
              styles.screenTitle,
              { textAlign: 'center', marginBottom: 0 },
            ]}
          >
            Welcome to {'\n'} {companyName}
          </Text>
        )}
        <View style={{ marginVertical: 30, width: '100%' }}>
          {logoImage ? (
            <ImageProgress
              source={{ uri: logoImage }}
              indicator={Progress.Circle}
              indicatorProps={{
                size: 50,
                borderWidth: 2,
                color: `${COLOR_GREEN}80`,
                unfilledColor: `${COLOR_GREEN}60`,
              }}
              style={{ width: '100%', height: 300 }}
              resizeMode="contain"
            />
          ) : (
            <View style={{ height: 300 }} />
          )}
        </View>
        <Image source={loadingGif} style={styles.loadingDots} />
      </View>
    </View>
  )
}

export default LoadingContainer
