/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react'
import {
  View,
  // StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native'
import styles from '../_styles'
import { COLOR_PLACEHOLDER, getFontSize, getViewSize } from '../../_styles'
// import LoadingOverlay from '@/Components/LoadingOverlay'
import { useSelector, useDispatch } from 'react-redux'
import { ForgotState } from '@/Store/Auth'
import Forgot, { InitReset } from '@/Store/Auth/Forgot'
import PressedButton from '@/Components/PressedButton'
import _ from 'lodash'

const loadingGif = require('@/Assets/Images/loading.gif')

const ForgotContainer = (props: any) => {
  const dispatch = useDispatch()
  const [email, setEmail] = useState('joe.tan086@gmail.com')
  const forgotLoading = useSelector(
    (state: { reset: ForgotState }) => state.reset.forgot.loading,
  )
  const forgotError = useSelector(
    (state: { reset: ForgotState }) => state.reset.forgot.error,
  )

  const resetResult = useSelector(
    (state: { reset: ForgotState }) => state.reset.item,
  )

  const goLogin = () => {
    props.navigation.goBack()
  }

  useEffect(() => {
    if (!_.isEmpty(resetResult)) {
      dispatch(InitReset.action())
    }
  })

  useEffect(() => {
    if (!_.isEmpty(resetResult)) {
      props.navigation.navigate('CheckEmail')
    }
  }, [resetResult])

  const handleSubmit = () => {
    dispatch(Forgot.action(email))
  }

  return (
    <View style={styles.screen}>
      <Text style={styles.logoTitle}>WholesaleWare</Text>
      <View style={styles.mainArea}>
        <Text style={[styles.screenTitle, { marginBottom: 10 }]}>
          Reset Your Password
        </Text>
        <View style={{ width: '100%', height: 254 }}>
          {forgotLoading ? (
            <View
              style={{
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Image source={loadingGif} style={styles.loadingDots} />
            </View>
          ) : (
            <View style={{ width: '100%', alignItems: 'center' }}>
              <Text style={styles.description}>
                Enter your email, and we will send you a link to reset your
                password
              </Text>
              <View style={{ height: 240, width: '100%' }}>
                <TextInput
                  value={email}
                  style={[styles.input, { marginTop: getViewSize(20) }]}
                  placeholder="Email"
                  textContentType="username"
                  placeholderTextColor={COLOR_PLACEHOLDER}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  onChangeText={setEmail}
                />
                <PressedButton label="Submit" onPress={handleSubmit} />
                {forgotError !== null && (
                  <View style={[styles.msgWrapper]}>
                    <Text style={styles.msg}>
                      Reset password failed. Please check your email address
                    </Text>
                  </View>
                )}
                <TouchableOpacity
                  style={[styles.link, { marginTop: getViewSize(27) }]}
                  onPress={goLogin}
                >
                  <Text style={styles.linkText}>Return to Log in</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
      </View>
      {/* <LoadingOverlay loading={forgotLoading} /> */}
    </View>
  )
}

export default ForgotContainer
