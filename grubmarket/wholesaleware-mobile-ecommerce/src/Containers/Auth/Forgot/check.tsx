/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { View, Text } from 'react-native'
import styles from '../_styles'
import { getViewSize } from '../../_styles'
import PressedButton from '@/Components/PressedButton'

const CheckEmailContainer = (props: any) => {
  const goLogin = () => {
    props.navigation.navigate('Login')
  }

  return (
    <View style={styles.screen}>
      <Text style={styles.logoTitle}>WholesaleWare</Text>
      <View style={styles.mainArea}>
        <Text style={[styles.screenTitle, { marginBottom: 10 }]}>
          Check Your Email
        </Text>
        <View style={{ width: '100%', height: 254, alignItems: 'center' }}>
          <Text style={styles.description}>
            We sent you an email with a link to reset upir password.
          </Text>
          <View style={{ height: 240, width: '100%' }}>
            <View style={{ marginTop: getViewSize(42) }}>
              <PressedButton label="Return to Login" onPress={goLogin} />
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}

export default CheckEmailContainer
