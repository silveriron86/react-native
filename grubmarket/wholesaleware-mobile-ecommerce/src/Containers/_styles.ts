import { StyleSheet, Platform } from 'react-native'

export const COLOR_LIGHT_GREEN = '#26AC5F'
export const COLOR_GREEN = '#1C6E31'
export const COLOR_GREEN_DARK = '#175d29'
export const COLOR_DARK = '#0E161C'
export const COLOR_PLACEHOLDER = '#82898A'
export const LIGHT_COLOR_PLACEHOLDER = '#979797'
export const COLOR_COMMENT = '#98ABB6'
export const COLOR_BORDER = '#D8DBDB'
export const COLOR_BG = '#FFFFFF'
export const COLOR_TEXT_PLACEHOLDER = '#A2A2A2'
export const COLOR_LIGHT_BLACK = '#22282A'

export const FONT_SCALE = 1
export const PX_SCALE = 1

export const getFontSize = (val: number) => {
  return val * FONT_SCALE
}
export const getViewSize = (val: number) => {
  return val * PX_SCALE
}

const isAndroid = Platform.OS === 'android'

export default StyleSheet.create({
  screen: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  bgWhite: {
    backgroundColor: 'white',
  },
  scrollView: {
    justifyContent: 'flex-start',
    height: 'auto',
  },
  ph16: {
    paddingHorizontal: 16,
  },
  subHeader: {
    alignSelf: 'flex-start',
    paddingHorizontal: 16,
    marginBottom: 6,
  },
  mt0: {
    marginTop: 0,
  },
  subTitle: {
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    fontSize: getFontSize(14),
    marginVertical: getViewSize(7),
    color: '#22282A',
    textAlign: 'center',
  },
  smTitle: {
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontWeight: '500',
    fontSize: getFontSize(12),
    marginVertical: 0,
  },
  tempBtnText: {
    fontSize: getFontSize(20),
    color: COLOR_GREEN,
  },
  textLeft: {
    textAlign: 'left',
  },
  textRight: {
    textAlign: 'right',
  },
  textCenter: {
    textAlign: 'center',
  },
  fullSize: {
    width: '100%',
    height: '100%',
  },
  fullWidth: {
    width: '100%',
  },
  flex1: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
  },
  vCenter: {
    alignItems: 'center',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  spaceBetween: {
    width: '100%',
    justifyContent: 'space-between',
  },
  alignRight: {
    textAlign: 'right',
  },
  label: {
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontWeight: '500',
    fontSize: getFontSize(14),
    color: '#4A5355',
    marginVertical: 4,
    lineHeight: 22,
  },
  mv0: {
    marginVertical: 0,
  },
  textArea: {
    backgroundColor: 'white',
    width: '100%',
    height: 96,
    borderWidth: 1,
    borderColor: COLOR_BORDER,
    borderRadius: 2,
    paddingHorizontal: 10,
    paddingVertical: 6,
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontWeight: '500',
    fontSize: getFontSize(14),
  },
  text16: {
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    fontSize: getFontSize(16),
    lineHeight: 24,
    color: '#22282A',
  },
  text18: {
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    fontSize: getFontSize(18),
    lineHeight: 25,
    color: '#22282A',
  },
  text14: {
    fontFamily: isAndroid ? 'MuseoSansRounded-300' : 'Museo Sans Rounded',
    fontWeight: '300',
    fontSize: getFontSize(14),
    lineHeight: 24,
    color: '#0E161C',
  },
  greyColor: {
    color: COLOR_PLACEHOLDER,
  },
  lightGreyColor: {
    color: LIGHT_COLOR_PLACEHOLDER,
  },
  lightBlackColor: {
    color: COLOR_LIGHT_BLACK,
  },
  greenColor: {
    color: COLOR_GREEN,
  },
  lightGreenColor: {
    color: COLOR_LIGHT_GREEN,
  },
  bold: {
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
  },
  badge: {
    position: 'absolute',
    right: 20,
    backgroundColor: 'red',
    // width: 11,
    height: 11,
    borderRadius: 5.5,
    paddingHorizontal: 3,
  },
  badgeText: {
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    fontSize: getFontSize(8.5),
    color: 'white',
    textAlign: 'center',
    width: '100%',
  },
  commentText: {
    fontFamily: isAndroid ? 'MuseoSansRounded-300' : 'Museo Sans Rounded',
    fontWeight: '300',
    fontSize: getFontSize(9.5),
    color: COLOR_COMMENT,
  },
  inCartView: {
    width: '100%',
    height: getViewSize(40),
    borderWidth: 1,
    borderColor: COLOR_BORDER,
    justifyContent: 'center',
    // paddingHorizontal: getViewSize(13),
    backgroundColor: 'white',
    borderRadius: 2.5,
    marginBottom: getViewSize(15),
  },
  pickerInput: {
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    fontSize: getFontSize(14),
    color: COLOR_GREEN,
    textAlign: 'center',
  },
  pickerArrow: {
    borderLeftColor: COLOR_BORDER,
    borderLeftWidth: 1,
    height: '100%',
    width: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contactIcon: {
    width: 24,
    height: 24,
    marginRight: 13,
  },
})
