import { StyleSheet, Platform } from 'react-native'
import {
  getFontSize,
  getViewSize,
  COLOR_GREEN,
  COLOR_DARK,
  COLOR_BORDER,
} from '@/Containers/_styles'

const isAndroid = Platform.OS === 'android'

export default StyleSheet.create({
  list: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
  },
  th: {
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontSize: getFontSize(16),
    fontWeight: isAndroid ? '500' : '700',
    color: COLOR_DARK,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FCFCFC',
    borderTopWidth: 1,
    borderTopColor: COLOR_BORDER,
    minHeight: getViewSize(68),
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  productIcon: {
    width: getViewSize(30),
    height: getViewSize(30),
    marginRight: getViewSize(12),
  },
  title: {
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontSize: getFontSize(14),
    color: COLOR_GREEN,
  },
  firstCol: {
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 16,
  },
  text: {
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontWeight: '500',
    fontSize: getFontSize(14),
    color: '#22282A',
    textAlign: 'right',
  },
  greyFont: {
    color: '#A2A2A2',
  },
  psBlockWrapper: {
    width: 167,
    margin: 4,
    marginBottom: 25,
  },
  psBlock: {
    width: '100%',
    height: 167,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR_GREEN,
  },
  psBlockName: {
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '900',
    fontSize: getFontSize(40),
    color: 'white',
  },
  psBlockTitle: {
    marginTop: 7,
    marginBottom: 5,
    fontFamily: isAndroid ? 'MuseoSansRounded-700' : 'Museo Sans Rounded',
    fontWeight: isAndroid ? '500' : '700',
    fontSize: getFontSize(14),
    color: '#22282A',
  },
  psBlockItems: {
    fontFamily: isAndroid ? 'MuseoSansRounded-500' : 'Museo Sans Rounded',
    fontWeight: '500',
    fontSize: getFontSize(10),
    color: '#82898A',
  },
  rightArrow: {
    width: 9,
    height: 16,
  },
})
