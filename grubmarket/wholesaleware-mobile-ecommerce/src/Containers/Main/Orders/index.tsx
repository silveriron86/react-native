/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Platform,
} from 'react-native'
import moment from 'moment'
import { useDispatch, useSelector } from 'react-redux'
import _, { cloneDeep } from 'lodash'
import styles, { getViewSize, getFontSize, COLOR_BORDER } from '../../_styles'
import RNPickerSelect from 'react-native-picker-select'
import orderStyles from '../_styles'
import {
  getWarehouseSalesOrdersByDateModule,
  OrderState,
  initOrderItems,
  getSellerSettingModule,
  initOrderOneOffItems,
  getOrdersForCustomerUserIdModule,
} from '@/Store/Order'
import { AuthState } from '@/Store/Auth'
import { CommonActions } from '@react-navigation/native'
import Logout from '@/Store/Auth/Logout'

const arrowDown = require('../../../Assets/Images/arrow_down.png')
const isAndroid = Platform.OS === 'android'

const presets = [
  {
    value: 0,
    label: 'Past 7 Days',
    from: moment().subtract(7, 'days'),
    to: moment(),
  },
  {
    value: 1,
    label: 'Past 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment(),
  },
  {
    value: 9,
    label: 'Yesterday',
    from: moment().subtract(1, 'days'),
    to: moment().subtract(1, 'days'),
  },
  {
    value: 2,
    label: 'Today',
    from: moment(),
    to: moment(),
  },
  {
    value: 3,
    label: 'Tomorrow',
    from: moment().add(1, 'days'),
    to: moment().add(1, 'days'),
  },
  {
    value: 4,
    label: 'Today and Tomorrow',
    from: moment(),
    to: moment().add(1, 'days'),
  },
  {
    value: 5,
    label: 'Next 7 Days',
    from: moment(),
    to: moment().add(7, 'days'),
  },
  {
    value: 6,
    label: 'Next 30 Days',
    from: moment(),
    to: moment().add(30, 'days'),
  },
  {
    value: 7,
    label: 'Past 30 Days and Next 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment().add(30, 'days'),
  },
  // {
  //   value: 8,
  //   label: 'Custom Date Range',
  // },
]

const OrdersListContainer = (props: any) => {
  const { order, auth } = useSelector(
    (state: { order: OrderState; auth: AuthState }) => state,
  )
  const { orders, getOrders } = order
  const [filter, setFilter] = useState({
    curPreset: 7,
    page: 0,
    pageSize: 1000,
    searchStr: '',
    orderByFilter: '',
    fromDate: moment(),
    toDate: moment(),
  })
  const dispatch = useDispatch()

  useEffect(() => {
    if (_.isEmpty(auth.item)) {
      props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        }),
      )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth])

  React.useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      dispatch(initOrderItems.action([]))
      // dispatch(initOrderOneOffItems.action([]))
    })
    return unsubscribe
  }, [props.navigation])

  useEffect(() => {
    global.eventEmitter.addListener('401_ERROR', () => {
      dispatch(Logout.action())
    })
    global.eventEmitter.addListener('REFRESH_ORDERS_LIST', () => {
      onRefresh()
    })
    dispatch(getSellerSettingModule.action(auth.item.userId))
  }, [])

  useEffect(() => {
    const selected = presets.find((p: any) => p.value === filter.curPreset)
    if (selected && selected.from && selected.to) {
      getAllOrders(selected.from, selected.to)
    }
  }, [filter])

  const getAllOrders = (fromDate: any, toDate: any) => {
    const searchObj = {
      ...filter,
      fromDate: fromDate.format('MM/DD/YYYY'),
      toDate: toDate.format('MM/DD/YYYY'),
    }
    dispatch(getOrdersForCustomerUserIdModule.action(searchObj))
    // dispatch(getWarehouseSalesOrdersByDateModule.action(searchObj))
  }

  const onPresetChange = (val: number) => {
    setFilterValue('curPreset', val)
  }

  const setFilterValue = (field: string, val: any) => {
    let curFilter = cloneDeep(filter)
    curFilter[field] = val
    setFilter(curFilter)
  }

  const onRefresh = () => {
    setFilterValue('page', 0)
  }

  // const onEndReached = () => {
  //   const pages = Math.ceil(orders.total / filter.pageSize)
  //   console.log('*onEndReached', filter.page, pages)
  //   if (filter.page < pages) {
  //     setFilterValue('page', filter.page + 1)
  //   }
  // }

  const goDetail = (data: any) => {
    props.navigation.navigate('OrderDetail', {
      data,
    })
  }

  const Item = (record: any) => {
    // let items = record.orderItems.filter(
    //   (item: any) => item.wholesaleItem !== null,
    // )
    // const items = 0
    // let items = record.orderItems
    // items = `${items.length} ${items.length <= 1 ? 'item' : 'items'}`
    const { deliveryDate, status, totalPrice } = record
    return (
      <TouchableOpacity
        style={orderStyles.row}
        onPress={() => goDetail(record)}
      >
        <View style={styles.flex1}>
          <Text style={orderStyles.title}>
            {moment(deliveryDate).format('MM/DD/YYYY')}
            {/* ({items}) */}
          </Text>
        </View>

        <View style={{ width: 80 }}>
          <Text style={orderStyles.text}>{status}</Text>
        </View>
        <View style={{ width: 100 }}>
          <Text style={orderStyles.text}>${totalPrice.toFixed(2)}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  const renderItem = ({ item }: any) => <Item {...item} />

  return (
    <View style={[styles.screen]}>
      <Text style={[styles.subTitle, styles.mt0]}>Order history</Text>
      <View style={{ padding: 16, width: '100%' }}>
        <RNPickerSelect
          value={filter.curPreset}
          style={{
            viewContainer: {
              width: '100%',
              height: getViewSize(35),
              borderWidth: 1,
              borderColor: COLOR_BORDER,
              justifyContent: 'center',
              borderRadius: 8,
              paddingHorizontal: getViewSize(13),
            },
            placeholder: {
              fontFamily: isAndroid
                ? 'MuseoSansRounded-500'
                : 'Museo Sans Rounded',
              fontWeight: '500',
              fontSize: getFontSize(14),
              color: 'COLOR_BORDER',
            },
            // inputIOS: {
            //   color: COLOR_BORDER,
            // },
            inputAndroid: {
              fontFamily: isAndroid
                ? 'MuseoSansRounded-500'
                : 'Museo Sans Rounded',
              fontWeight: '500',
              color: 'black',
              borderWidth: 1,
              borderColor: '#D8D8D8',
              borderRadius: 8,
              paddingHorizontal: 13,
            },
          }}
          useNativeAndroidPickerStyle={false}
          onValueChange={onPresetChange}
          items={presets}
          Icon={() => {
            return (
              <Image
                source={arrowDown}
                style={{
                  width: 16,
                  height: 8,
                  marginTop: isAndroid ? 20 : 5,
                  marginRight: isAndroid ? 15 : 0,
                }}
              />
            )
          }}
        />
      </View>
      <View style={orderStyles.list}>
        <FlatList
          data={orders ? orders.dataList : []}
          renderItem={renderItem}
          keyExtractor={item => item.wholesaleOrderId}
          refreshing={getOrders.loading}
          onRefresh={onRefresh}
          // onEndReached={onEndReached}
          // onEndReachedThreshold={0}
        />
      </View>
      {/* <LoadingOverlay loading={getOrders.loading} /> */}
    </View>
  )
}

export default OrdersListContainer
