/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react'
import { View, Text, FlatList } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import styles, { getViewSize, COLOR_DARK } from '../../_styles'
import orderStyles from '../_styles'
// import PrimaryButton from '@/Components/PrimaryButton'
import { AuthState } from '@/Store/Auth'
import {
  OrderState,
  getSellerSettingModule,
  resetNewOrderId,
  getOrderByTokenModule,
  getOrderItemsByTokenModule,
} from '@/Store/Order'
import {
  formatAddress,
  getFulfillmentDate,
  getOrderPrefix,
} from '@/Services/utils'
import LoadingOverlay from '@/Components/LoadingOverlay'
import { TouchableOpacity } from 'react-native-gesture-handler'
import PrimaryButton from '@/Components/PrimaryButton'
import { CartState, fillCartItemsModule, addCartItemModule } from '@/Store/Cart'
import { PricingState } from '@/Store/Pricing'

const OrderDetailContainer = (props: any) => {
  const currentOrder = props.route.params.data
  const { auth, order, cart, pricing } = useSelector(
    (state: {
      auth: AuthState
      order: OrderState
      cart: CartState
      pricing: PricingState
    }) => state,
  )

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getSellerSettingModule.action(auth.item.userId))
    onRefresh()
  }, [])

  useEffect(() => {
    if (order.newOrderId !== -1) {
      dispatch(resetNewOrderId.action())
      global.eventEmitter.emit('REFRESH_ORDERS_LIST')
      goBack()
    }
  }, [order.newOrderId])

  const goBack = () => {
    props.navigation.goBack()
  }

  const onRefresh = () => {
    dispatch(getOrderByTokenModule.action(currentOrder.linkToken))
    dispatch(getOrderItemsByTokenModule.action(currentOrder.linkToken))
    // dispatch(
    //   getAddressesModule.action(
    //     currentOrder.wholesaleClient.clientId.toString(),
    //   ),
    // )
    // dispatch(getOrderItemsByIdModule.action(wholesaleOrderId))
    // dispatch(getOrderOneOffItemsByIdModule.action(wholesaleOrderId))
  }

  const onDuplicateOrder = () => {
    const fulfillmentDate = getFulfillmentDate(order.sellerSetting)
    let newOrderItems = order.orderItems //.concat(oneOffItems)
    let tempOrderItems = newOrderItems.map((obj: any) => {
      return {
        ...obj,
        wholesaleItemId: obj.itemId,
      }
    })

    const data = {
      userId: auth.item.userId,
      deliveryDate: fulfillmentDate,
      orderDate: moment().format('MM/DD/YYYY'),
      wholesaleCustomerClientId:
        order.order.currentOrder!.wholesaleClient.clientId,
      itemList: tempOrderItems,
      wholesaleOrderId: order.order.currentOrder!.wholesaleOrderId,
      // cashSale: true,
    }

    let cartItems: any[] = []
    const { itemList } = data
    itemList.forEach((item, index) => {
      const cartItem = _buildCartItem(item, index)
      cartItems.push(cartItem)
    })

    dispatch(fillCartItemsModule.action(cartItems))
    props.navigation.navigate('CartList')
    setTimeout(() => {
      const { wholesaleAddressId, address } =
        order.order.currentOrder.shippingAddress
      global.eventEmitter.emit('SET_DELIVERY', {
        addressId: wholesaleAddressId,
        address,
      })
      global.eventEmitter.emit(
        'SET_CART_NOTES',
        order.order.currentOrder.pickerNote,
      )
    }, 100)
    // dispatch(duplicateOrderModule.action(data))
  }

  const _buildCartItem = (item: any, index: number | null) => {
    const priceSheetItem = pricing.priceSheetItems.priceSheetItemList.find(
      (p_item: any) => p_item.wholesaleItem.wholesaleItemId === item.itemId,
    )

    let variety = item.variety
    // if (item.price === 0) {
    if (!priceSheetItem) {
      if (!variety) {
        variety = item.chargeDesc
      }
    }

    let cartItem = {
      count: item.quantity,
      index,
      variety,
      instruction: item.note ?? '',
      priceSheetItem: priceSheetItem ?? null,
    }
    return cartItem
  }

  const onAdd = (item: any) => {
    const filtered = cart.cartItems.filter(ci => {
      return ci.variety === item.variety || ci.variety === item.chargeDesc
    })
    if (filtered.length > 0) {
      // already added
      return
    }
    const index = cart.cartItems.length
    const cartItem = _buildCartItem(item, index)
    dispatch(addCartItemModule.action(cartItem))
    setTimeout(() => {
      global.eventEmitter.emit('CART_CHANGED')
    }, 100)
  }

  const goDetail = (item: any, note: string) => {
    if (pricing.selectedPriceSheet) {
      const priceSheetItem = pricing.priceSheetItems.priceSheetItemList.find(
        (p_item: any) => p_item.wholesaleItem.wholesaleItemId === item.itemId,
      )

      props.navigation.navigate('PriceSheetsCart', {
        priceSheetItem,
        priceSheetName: pricing.selectedPriceSheet.name,
        note,
      })
    }
  }

  const isTouchable = (item: any) => {
    const priceSheetItem = pricing.priceSheetItems.priceSheetItemList.find(
      (p_item: any) => p_item.wholesaleItem.wholesaleItemId === item.itemId,
    )
    return priceSheetItem ? true : false
  }

  const Item = (record: any) => {
    const { quantity, variety, price, chargeDesc, itemId } = record
    // let total = 0

    // let constantRatio = judgeConstantRatio(record)
    // let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
    // let recordPrice = record.price

    // let ratioPrice = basePriceToRatioPrice(pricingUOM, recordPrice, record, 12)

    // if (record.oldCatchWeight) {
    //   total = numberMultipy(recordPrice, record.catchWeightQty)
    // } else {
    //   if (!constantRatio) {
    //     total = numberMultipy(
    //       ratioPrice,
    //       inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record, 12),
    //     )
    //   } else {
    //     total = numberMultipy(recordPrice, record.catchWeightQty)
    //   }
    // }
    const note = variety ? variety : chargeDesc

    return (
      <View style={[orderStyles.row, { paddingLeft: 6 }]}>
        <View style={[orderStyles.firstCol, { width: 50, paddingRight: 0 }]}>
          <Text style={orderStyles.text}>{quantity}</Text>
        </View>
        <View style={styles.flex1}>
          <TouchableOpacity
            disabled={!itemId || !isTouchable(record)}
            onPress={() => goDetail(record, '')}
          >
            <Text style={[orderStyles.title, { color: COLOR_DARK }]}>
              {note}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ minWidth: 80 }}>
          <Text style={orderStyles.text}>${price.toFixed(2)}</Text>
        </View>
        <View style={{ paddingLeft: 15, paddingRight: 10 }}>
          <TouchableOpacity onPress={() => onAdd(record)}>
            <Text style={[styles.label, styles.lightGreenColor]}>Add</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  let totalPrice = 0
  if (order.orderItems && order.orderItems.length > 0) {
    order.orderItems.forEach((item: any) => {
      totalPrice += item.price * item.quantity
    })
  }

  const renderItem = ({ item }: any) => <Item {...item} />
  return (
    <View style={styles.screen}>
      {/* <Text style={styles.subTitle}>{currentOrder.wholesaleOrderStatus}</Text> */}
      <View style={[styles.subHeader, { marginTop: getViewSize(7) }]}>
        <View style={[styles.flexRow, styles.spaceBetween]}>
          <View style={styles.flex1}>
            <Text style={[styles.subTitle, styles.textLeft, styles.greyColor]}>
              {moment(currentOrder.createdDate).format('MMMM D, YYYY')}
            </Text>
            <Text
              style={[
                styles.subTitle,
                styles.smTitle,
                styles.textLeft,
                styles.greyColor,
              ]}
            >
              Ordered {moment(currentOrder.deliveryDate).format('MMMM D, YYYY')}
            </Text>
          </View>
          <View style={styles.flex1}>
            <Text style={[styles.subTitle, styles.textRight, styles.greyColor]}>
              Order # {getOrderPrefix(order.sellerSetting, 'sales')}
              {currentOrder.wholesaleOrderId}
            </Text>
            <Text
              style={[
                styles.subTitle,
                styles.smTitle,
                styles.textRight,
                styles.greyColor,
              ]}
            >
              Total ${totalPrice.toFixed(2)}
            </Text>
          </View>
        </View>
        {order.order && order.order.currentOrder.shippingAddress !== '' && (
          <View
            style={{ marginTop: getViewSize(8), marginBottom: getViewSize(10) }}
          >
            <Text style={[styles.label, styles.greyColor]}>
              {auth.item.fullName}
              {'\n'}
              {formatAddress(
                order.order.currentOrder.shippingAddress.address,
                true,
              )}
            </Text>
          </View>
        )}
        {order.order && order.order.currentOrder.pickerNote !== null && (
          <Text style={[styles.label, styles.greyColor]}>
            Notes: {order.order.currentOrder.pickerNote}
          </Text>
        )}
      </View>
      <View style={orderStyles.list}>
        {order.orderItems && (
          <FlatList
            data={order.orderItems}
            renderItem={renderItem}
            keyExtractor={item => item.wholesaleOrderItemId}
            refreshing={
              order.getOrderByToken.loading ||
              order.getOrderItemsByToken.loading
            }
            onRefresh={onRefresh}
          />
        )}
      </View>
      <View
        style={{ paddingHorizontal: 16, paddingVertical: 32, width: '100%' }}
      >
        <PrimaryButton
          onPress={onDuplicateOrder}
          text="Duplicate Order"
          disabled={cart.cartItems.length > 0}
        />
      </View>
      <LoadingOverlay loading={order.duplicateOrder.loading} />
    </View>
  )
}

export default OrderDetailContainer
