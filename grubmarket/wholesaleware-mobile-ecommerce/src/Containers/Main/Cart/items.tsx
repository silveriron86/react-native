/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react'
import { View, Text, FlatList, TouchableOpacity, Image } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import styles from '../../_styles'
import { AuthState } from '@/Store/Auth'
import SearchBar from '@/Components/SearchBar'
import orderStyles from '../_styles'
import { getCustomerItemsModule, OrderState } from '@/Store/Order'
import { PricingState } from '@/Store/Pricing'
import { addCartItemModule } from '@/Store/Cart'
const productIcon = require('@/Assets/Images/default_product.png')

const ItemsContainer = (props: any) => {
  const { priceSheet } = props.route.params
  const [search, setSearch] = useState('')
  const { auth, order, pricing } = useSelector(
    (state: { auth: AuthState; order: OrderState; pricing: PricingState }) =>
      state,
  )

  const dispatch = useDispatch()

  useEffect(() => {
    onRefresh()
  }, [])

  const onSearch = (searchText: string, categoryText: string) => {
    setSearch(searchText)
  }

  const onRefresh = () => {
    dispatch(
      getCustomerItemsModule.action(
        pricing.priceSheetItems.sellerSetting.userId,
      ),
    )
  }

  const onRequest = (item: any) => {
    dispatch(
      addCartItemModule.action({
        ...item,
        priceSheetItem: null,
        instruction: '',
        count: 1,
      }),
    )
    setTimeout(() => {
      props.navigation.goBack()
    }, 100)
  }

  const Item = (item: any) => {
    const { variety } = item

    return (
      <TouchableOpacity style={[orderStyles.row]}>
        <Image
          source={productIcon}
          style={[
            orderStyles.productIcon,
            {
              width: 25,
              height: 25,
              tintColor: '#E0E0E0',
            },
          ]}
        />

        <View style={[styles.flexRow, styles.flex1]}>
          <Text style={orderStyles.title}>{variety}</Text>
        </View>
        <TouchableOpacity onPress={() => onRequest(item)}>
          <Text style={[styles.label, styles.lightGreenColor]}>Request</Text>
        </TouchableOpacity>
      </TouchableOpacity>
    )
  }

  const renderItem = ({ item }: any) => <Item {...item} />
  const lastItem = {
    wholesaleItemId: null,
    variety: search,
  }
  let items = order.customerItems
  if (search) {
    items = order.customerItems.filter(
      item => item.variety.toLowerCase().indexOf(search.toLowerCase()) >= 0,
    )
  }

  let isExactMatch =
    search !== '' && items.filter(item => item.variety === search).length === 0
  if (items) {
    if (items.length > 0) {
      items = items.filter((c_item: any) => {
        const duplicated = pricing.priceSheetItems.priceSheetItemList.filter(
          (p_item: any) =>
            p_item.wholesaleItem.wholesaleItemId === c_item.wholesaleItemId,
        )
        return duplicated.length === 0
      })
      items = [...items].sort((a: any, b: any) =>
        a.variety.localeCompare(b.variety),
      )
    }
  }

  return (
    <View style={styles.screen}>
      <Text style={[styles.subTitle, styles.mt0]}>{priceSheet.name}</Text>
      <SearchBar
        placeholder="Request an item"
        autoFocus={true}
        categories={[]}
        onChange={onSearch}
      />
      <View style={orderStyles.list}>
        {search !== '' && (
          <View style={{ margin: 17 }}>
            <Text style={[orderStyles.psBlockItems, { fontSize: 14 }]}>
              Search results
            </Text>
          </View>
        )}

        {items.length > 0 ? (
          <FlatList
            data={[...items, isExactMatch ? lastItem : null]}
            renderItem={renderItem}
            keyExtractor={item => item.wholesaleItemId}
            refreshing={order.getCustomerItems.loading}
            onRefresh={onRefresh}
            keyboardShouldPersistTaps="always"
          />
        ) : (
          <>
            {search !== '' && (
              <FlatList
                data={[lastItem]}
                renderItem={renderItem}
                keyboardShouldPersistTaps="always"
              />
            )}
          </>
        )}
      </View>
    </View>
  )
}

export default ItemsContainer
