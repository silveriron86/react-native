/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react'
import { View, Text, TextInput, Keyboard } from 'react-native'
import styles, {
  COLOR_BG,
  COLOR_BORDER,
  COLOR_TEXT_PLACEHOLDER,
} from '../../_styles'
import orderStyles from '../_styles'
import PrimaryButton from '@/Components/PrimaryButton'

const CartNotesContainer = (props: any) => {
  const [notes, setNotes] = useState(props.route.params.notes)

  const goBack = () => {
    global.eventEmitter.emit('SET_CART_NOTES', notes)
    props.navigation.goBack()
  }

  return (
    <View style={styles.screen}>
      <Text style={[styles.subTitle, styles.mt0]}>
        {props.route.params.name}
      </Text>
      <View style={styles.subHeader}>
        <Text
          style={[styles.label, styles.lightGreyColor, { marginVertical: 14 }]}
        >
          Leave a note
        </Text>
      </View>
      <View
        style={[
          orderStyles.list,
          styles.ph16,
          {
            backgroundColor: COLOR_BG,
            borderTopWidth: 1,
            borderTopColor: COLOR_BORDER,
            paddingTop: 12,
          },
        ]}
      >
        <TextInput
          value={notes}
          onChangeText={setNotes}
          style={[styles.label, styles.mv0]}
          keyboardType="default"
          returnKeyType="done"
          multiline={true}
          blurOnSubmit={true}
          onSubmitEditing={() => {
            Keyboard.dismiss()
          }}
          placeholderTextColor={COLOR_TEXT_PLACEHOLDER}
          placeholder="Let us know if you have any requests relating to your order or pertaining to your delivery."
        />
      </View>
      <View
        style={{ paddingHorizontal: 16, paddingVertical: 12, width: '100%' }}
      >
        <PrimaryButton onPress={goBack} text="Save" />
      </View>
    </View>
  )
}

export default CartNotesContainer
