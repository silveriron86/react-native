/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react'
import { View, Text, Image } from 'react-native'
import { useSelector } from 'react-redux'
import styles from '../../_styles'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import orderStyles from '../_styles'
import { AuthState } from '@/Store/Auth'
import { formatAddress } from '@/Services/utils'

const rightArrowIcon = require('../../../Assets/Images/arrow_right.png')

const DeliveryAddressesContainer = (props: any) => {
  const { auth } = useSelector((state: { auth: AuthState }) => state)

  const onPick = (wholesaleAddressId: string, address: any) => {
    global.eventEmitter.emit('SET_DELIVERY', {
      addressId: wholesaleAddressId,
      address,
    })
    props.navigation.goBack()
  }

  const { name, addresses } = props.route.params

  const Item = ({ wholesaleAddressId, address }: any) => {
    return (
      <TouchableOpacity
        style={orderStyles.row}
        onPress={() => onPick(wholesaleAddressId, address)}
      >
        <View style={styles.flex1}>
          <Text style={[styles.label, styles.mv0]}>
            {auth.item.fullName}
            {'\n'}
            {formatAddress(address, true)}
          </Text>
          {/* <Text style={[styles.label, styles.mv0]}>{name}</Text>
          <Text style={[(styles.label, styles.mv0)]}>{address1}</Text>
          <Text style={[styles.label, styles.mv0]}>{address2}</Text> */}
        </View>
        <View>
          <Image source={rightArrowIcon} style={orderStyles.rightArrow} />
        </View>
      </TouchableOpacity>
    )
  }

  const renderItem = ({ item }: any) => <Item {...item} />

  return (
    <View style={styles.screen}>
      <Text style={[styles.subTitle, styles.mt0]}>{name}</Text>
      <View style={styles.subHeader}>
        <Text
          style={[styles.label, styles.lightGreyColor, { marginVertical: 14 }]}
        >
          Please select a delivery address
        </Text>
      </View>
      <View style={orderStyles.list}>
        <FlatList
          data={addresses}
          renderItem={renderItem}
          keyExtractor={item => item.wholesaleAddressId}
        />
      </View>
    </View>
  )
}

export default DeliveryAddressesContainer
