/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react'
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import styles, { COLOR_BORDER, COLOR_GREEN } from '../../_styles'
import orderStyles from '../_styles'
import PrimaryButton from '@/Components/PrimaryButton'
import NumberPicker from '@/Containers/Auth/Login/NumberPicker'
import { AuthState } from '@/Store/Auth'
import { getSaleItemsModule } from '@/Store/Pricing'
import {
  CartState,
  getCartItemsModule,
  addCartItemModule,
  createOrderModule,
  removeCartItemModule,
} from '@/Store/Cart'
import AsyncStorage from '@react-native-async-storage/async-storage'
import DateTimePicker from '@react-native-community/datetimepicker'
import LoadingOverlay from '@/Components/LoadingOverlay'
// import DateTimePickerModal from 'react-native-modal-datetime-picker'

const CartContainer = (props: any) => {
  const [show, setShow] = useState(false)
  const [notes, setNotes] = useState('')
  const [shippingAddressId, setShippingAddressId] = useState(null)
  const [shippingAddress, setShippingAddress] = useState(null)
  const [requestedDate, setRequestedDate] = useState(new Date())
  const [priceSheet, setPriceSheet] = useState(null)
  const { auth, pricing, cart } = useSelector(
    (state: { auth: AuthState; pricing: PricingState; cart: CartState }) =>
      state,
  )

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getSaleItemsModule.action(auth.item.userId))
    dispatch(getCartItemsModule.action())

    global.eventEmitter.addListener('SET_CART_NOTES', (value: string) => {
      saveNotes(value)
    })

    global.eventEmitter.addListener(
      'SET_DELIVERY',
      ({ addressId, address }: any) => {
        setShippingAddressId(addressId)
        setShippingAddress(address)
      },
    )

    AsyncStorage.getItem('CART_NOTES', (_err, value) => {
      if (value) {
        saveNotes(value)
      }
    })

    const addresses =
      pricing.priceSheetItems.wholesaleCustomerClient.wholesaleAddressList.filter(
        (addr: any) => {
          return addr.addressType === 'SHIPPING'
        },
      )
    if (addresses.length > 0) {
      let defaultAddress = addresses.find((addr: any) => addr.isMain === true)
      if (!defaultAddress) {
        defaultAddress = addresses[0]
      }
      setShippingAddressId(defaultAddress.wholesaleAddressId)
      setShippingAddress(defaultAddress.address)
    }
  }, [])

  useEffect(() => {
    if (pricing.selectedPriceSheet) {
      setPriceSheet(pricing.selectedPriceSheet)
    }
  }, [pricing.selectedPriceSheet])

  // useEffect(() => {
  //   const unsubscribe = props.navigation.addListener('focus', () => {
  //     dispatch(getCartItemsModule.action())
  //   })
  //   return unsubscribe
  // }, [props.navigation])

  useEffect(() => {
    if (!cart.createOrder.loading && cart.newOrder !== null) {
      props.navigation.navigate('OrderConfirmation', {
        newOrder: cart.newOrder,
        name: priceSheet.name,
        shippingAddress,
        requestedDate,
      })
    }
  }, [cart.createOrder.loading, cart.newOrder])

  const saveNotes = (value: string) => {
    AsyncStorage.setItem('CART_NOTES', value)
    setNotes(value)
  }

  const goRequestItem = () => {
    if (!priceSheet) {
      return
    }
    props.navigation.navigate('RequestItems', {
      priceSheet,
    })
  }

  const pickDeliveryAddress = () => {
    if (!priceSheet) {
      return
    }

    const addresses =
      pricing.priceSheetItems.wholesaleCustomerClient.wholesaleAddressList.filter(
        (addr: any) => {
          return addr.addressType === 'SHIPPING'
        },
      )
    props.navigation.navigate('DeliveryAddresses', {
      name: priceSheet.name,
      addresses,
    })
  }

  const addNotes = () => {
    if (!priceSheet) {
      return
    }

    props.navigation.navigate('CartNotes', {
      name: priceSheet.name,
      notes,
    })
  }

  const onChangeQty = (qty: number, index: number) => {
    const { priceSheetItem, instruction } = cart.cartItems[index]
    if (qty === 0) {
      return dispatch(removeCartItemModule.action(index))
    }
    dispatch(
      addCartItemModule.action({
        priceSheetItem: priceSheetItem,
        instruction,
        count: qty,
        index,
      }),
    )
  }

  const goDetail = (priceSheetItem: any, note: string) => {
    props.navigation.navigate('PriceSheetsCart', {
      priceSheetItem,
      priceSheetName: priceSheet.name,
      note,
    })
  }

  const handlePlaceOrder = () => {
    const { wholesaleCustomerClient } = pricing.priceSheetItems
    let itemList: any[] = []
    cart.cartItems.forEach((ci: any) => {
      const { priceSheetItem, count, instruction, isRequest, variety } = ci
      let price = 0
      let cost = 0
      let wholesaleItemId = 0
      let modifier = null
      let defaultLogic = null
      let defaultGroup = null
      if (priceSheetItem) {
        const { wholesaleItem, salePrice } = priceSheetItem
        price = salePrice
        cost = wholesaleItem.cost
        wholesaleItemId = wholesaleItem.wholesaleItemId

        modifier = priceSheetItem.modifier
        defaultLogic = priceSheetItem.defaultLogic
        defaultGroup = priceSheetItem.defaultGroup
      } else {
        wholesaleItemId = ci.wholesaleItemId
      }

      itemList.push({
        price,
        wholesaleItemId,
        quantity: count,
        cost,
        margin: 0,
        freight: 0,
        modifier,
        defaultLogic,
        defaultGroup,
        note: instruction,
        uom: !wholesaleItemId ? 'each' : undefined,
        itemDesc: !wholesaleItemId ? variety : undefined,
      })
    })

    const data = {
      deliveryDate: moment(requestedDate).format('MM/DD/YYYY'),
      priceSheetId: priceSheet.priceSheetId,
      itemList,
      totalPrice: getTotalPrice(),
      userId: wholesaleCustomerClient.seller.userId,
      wholesaleClientId: wholesaleCustomerClient.clientId,
      token: priceSheet.linkToken,
      orderDate: moment(new Date()).format('MM/DD/YYYY'),
      pickerNote: notes,
      shippingAddressId,
    }

    dispatch(
      createOrderModule.action({
        data,
        token: priceSheet.linkToken,
      }),
    )
    // saveNotes('')
  }

  const Item = (item: any) => {
    const { displayPriceSheetPrice } =
      pricing.priceSheetItems.wholesaleCustomerClient
    const { priceSheetItem, count, instruction, index, wholesaleItemId } = item
    let variety = ''
    let salePrice = 0
    if (priceSheetItem) {
      variety = priceSheetItem.wholesaleItem.variety
      salePrice = priceSheetItem.salePrice
    } else {
      variety = item.variety
    }

    const price = displayPriceSheetPrice
      ? `$${salePrice.toFixed(2)}`
      : 'Available'

    return (
      <View style={[orderStyles.row]}>
        <View style={[orderStyles.firstCol, { width: 60, paddingLeft: 8 }]}>
          {/* <Text style={[orderStyles.text, styles.textLeft, styles.fullWidth]}>
            {qty}
          </Text> */}
          <NumberPicker
            defaultValue={count}
            onChange={(v: number) => onChangeQty(v, index)}
          />
        </View>
        <View style={styles.flex1}>
          <TouchableOpacity
            onPress={() => goDetail(priceSheetItem, instruction)}
            disabled={!priceSheetItem}
          >
            <Text style={orderStyles.title}>{variety}</Text>
          </TouchableOpacity>
          {priceSheetItem && instruction !== '' ? (
            <Text style={[styles.smTitle, styles.greyColor, { marginTop: 3 }]}>
              {instruction}
            </Text>
          ) : (
            <>
              {(!priceSheetItem || instruction !== '') && (
                <Text
                  style={[styles.smTitle, styles.greyColor, { marginTop: 3 }]}
                >
                  {instruction !== ''
                    ? instruction
                    : 'Add details in order notes'}
                </Text>
              )}
            </>
          )}
        </View>
        <View style={{ width: 80 }}>
          <Text style={orderStyles.text}>
            {priceSheetItem
              ? `${price}${
                  isVariableUOM(priceSheetItem.wholesaleItem) ? '*' : ''
                }`
              : 'REQUEST'}
          </Text>
        </View>
      </View>
    )
  }

  const renderItem = ({ item, index }: any) => {
    return <Item {...item} index={index} />
  }

  const getTotalPrice = () => {
    let totalPrice = 0
    cart.cartItems.forEach((c: any) => {
      if (c.priceSheetItem) {
        totalPrice += c.priceSheetItem.salePrice * c.count
      }
    })
    return totalPrice
  }

  const isVariableUOM = ({ wholesaleProductUomList }: any) => {
    let ret = false
    if (wholesaleProductUomList) {
      wholesaleProductUomList.forEach((uom: any) => {
        if (uom.constantRatio === false) {
          ret = true
        }
      })
    }
    return ret
  }

  let hasVariableUOM = false
  cart.cartItems.forEach((item: any) => {
    const { priceSheetItem } = item
    if (
      priceSheetItem &&
      priceSheetItem.wholesaleItem.wholesaleProductUomList
    ) {
      priceSheetItem.wholesaleItem.wholesaleProductUomList.forEach(
        (uom: any) => {
          if (uom.constantRatio === false) {
            hasVariableUOM = true
          }
        },
      )
    }
  })

  return (
    <ScrollView contentContainerStyle={[styles.screen, styles.scrollView]}>
      {/* <View style={styles.screen}> */}
      <View style={[orderStyles.list, { flex: 0 }]}>
        <FlatList
          data={cart.cartItems}
          renderItem={renderItem}
          keyExtractor={(item: any, index: number) => index}
          ListHeaderComponent={
            <>
              {typeof priceSheet !== 'undefined' && priceSheet !== null && (
                <Text style={[styles.subTitle, styles.mt0, styles.textCenter]}>
                  {priceSheet.name}
                </Text>
              )}
              <View style={styles.subHeader}>
                <View style={styles.flexRow}>
                  <Text style={[styles.label, styles.greyColor]}>
                    Deliver to:{' '}
                  </Text>
                  <TouchableOpacity onPress={pickDeliveryAddress}>
                    {shippingAddress !== null ? (
                      <Text
                        style={[styles.label, styles.bold, styles.greenColor]}
                      >
                        {`${
                          shippingAddress.department
                            ? shippingAddress.department + ', '
                            : ''
                        }${shippingAddress.street1} ${shippingAddress.city}`}
                      </Text>
                    ) : (
                      <Text
                        style={[
                          styles.bold,
                          styles.lightGreenColor,
                          { marginTop: 7, marginLeft: 6 },
                        ]}
                      >
                        Edit
                      </Text>
                    )}
                  </TouchableOpacity>
                </View>

                <View style={styles.flexRow}>
                  <Text style={[styles.label, styles.greyColor]}>
                    Requested Date:{' '}
                  </Text>
                  <View style={{ position: 'relative', width: 300 }}>
                    <TouchableOpacity onPress={() => setShow(true)}>
                      <Text
                        style={[styles.label, styles.bold, styles.greenColor]}
                      >
                        {moment(requestedDate).format('dddd, MMMM D')}
                      </Text>
                    </TouchableOpacity>
                    {show && (
                      <DateTimePicker
                        minimumDate={new Date()}
                        value={requestedDate}
                        mode="date"
                        display="default"
                        onChange={(_event, selectedDate) => {
                          if (selectedDate) {
                            setRequestedDate(selectedDate)
                          }
                          setShow(false)
                        }}
                        style={{
                          position: 'absolute',
                          left: 0,
                          width: '100%',
                          backgroundColor: 'transparent',
                          opacity: 0.05,
                        }}
                      />
                    )}
                  </View>
                </View>

                {notes !== '' ? (
                  <Text
                    style={[
                      styles.label,
                      styles.greyColor,
                      {
                        paddingRight: 35,
                      },
                    ]}
                  >
                    Notes: {notes}
                    {'    '}
                    <TouchableOpacity
                      style={{ marginTop: -1 }}
                      onPress={addNotes}
                    >
                      <Text style={[styles.bold, styles.lightGreenColor]}>
                        Edit
                      </Text>
                    </TouchableOpacity>
                  </Text>
                ) : (
                  // </View>
                  <TouchableOpacity
                    style={{ marginTop: 2, marginBottom: 6 }}
                    onPress={addNotes}
                  >
                    <Text
                      style={[styles.label, styles.bold, styles.greenColor]}
                    >
                      Add notes
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
              <View style={orderStyles.row}>
                <View style={[orderStyles.firstCol, { width: 68 }]}>
                  <Text style={[orderStyles.th, styles.fullWidth]}>Qty</Text>
                </View>
                <View style={styles.flex1}>
                  <Text style={orderStyles.th}>Item</Text>
                </View>
                <View style={{ width: 80 }}>
                  <Text style={[orderStyles.text, orderStyles.th]}>Price</Text>
                </View>
              </View>
            </>
          }
          ListFooterComponent={
            <>
              <TouchableOpacity
                style={[
                  { paddingVertical: 5, paddingHorizontal: 24 },
                  {
                    borderTopWidth: 1,
                    borderBottomWidth: 1,
                    borderColor: COLOR_BORDER,
                  },
                  styles.fullWidth,
                ]}
                onPress={goRequestItem}
              >
                <Text style={[styles.label, styles.greenColor]}>
                  Request item
                </Text>
              </TouchableOpacity>
              <View
                style={[
                  styles.ph16,
                  styles.flexRow,
                  styles.spaceBetween,
                  { marginTop: 27, marginBottom: 13 },
                ]}
              >
                <Text style={styles.text18}>Total</Text>
                <Text style={styles.text18}>
                  {pricing.priceSheetItems.wholesaleCustomerClient
                    .displayPriceSheetPrice
                    ? `$${getTotalPrice().toFixed(2)}`
                    : 'Available'}
                  {hasVariableUOM ? '*' : ''}
                </Text>
              </View>

              <View style={styles.ph16}>
                {hasVariableUOM && (
                  <Text style={[styles.commentText, styles.greyColor]}>
                    * This order contains catchweight items. Final pricing will
                    be dependent on final weight.
                  </Text>
                )}
                <View style={{ marginTop: 9 }}>
                  <Text style={[styles.commentText, styles.greyColor]}>
                    {
                      pricing.priceSheetItems.sellerSetting.company
                        .purchaseTerms
                    }
                  </Text>
                </View>
              </View>
              {cart.cartItems.length > 0 && (
                <View
                  style={{
                    paddingHorizontal: 16,
                    paddingVertical: 12,
                    width: '100%',
                  }}
                >
                  <PrimaryButton
                    onPress={handlePlaceOrder}
                    text="Place Order"
                  />
                </View>
              )}
            </>
          }
        />
      </View>
      <LoadingOverlay loading={cart.createOrder.loading} />
    </ScrollView>
  )
}

export default CartContainer
