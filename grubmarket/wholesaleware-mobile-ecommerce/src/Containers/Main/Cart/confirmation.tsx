/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react'
import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import styles, { COLOR_BORDER } from '../../_styles'
import orderStyles from '../_styles'
import { AuthState } from '@/Store/Auth'
import { CartState, resetNewOrder, resetCartModule } from '@/Store/Cart'
import { getOrderPrefix, formatAddress } from '@/Services/utils'
import { OrderState, getSellerSettingModule } from '@/Store/Order'
import { PricingState } from '@/Store/Pricing'
// import DateTimePickerModal from 'react-native-modal-datetime-picker'

const OrderConfirmation = (props: any) => {
  const { auth, order, pricing } = useSelector(
    (state: {
      auth: AuthState
      pricing: PricingState
      cart: CartState
      order: OrderState
    }) => state,
  )

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getSellerSettingModule.action(auth.item.userId))
    dispatch(resetNewOrder.action())
    dispatch(resetCartModule.action())
  }, [])

  const viewOrder = () => {
    const { newOrder } = props.route.params
    props.navigation.navigate('ViewOrder', {
      data: newOrder,
    })
  }

  const { name, requestedDate, newOrder, shippingAddress } = props.route.params

  return (
    <ScrollView contentContainerStyle={[styles.screen, styles.scrollView]}>
      {/* <View style={styles.screen}> */}
      <View style={[orderStyles.list, { flex: 0 }]}>
        <Text style={[styles.subTitle, styles.mt0, { paddingBottom: 7 }]}>
          {name}
        </Text>
        <View
          style={[
            styles.subHeader,
            styles.fullWidth,
            {
              paddingTop: 16,
              borderTopWidth: 1,
              borderTopColor: COLOR_BORDER,
            },
          ]}
        >
          <View style={styles.flexRow}>
            <Text style={styles.label}>Order successfully placed!</Text>
          </View>

          <Text style={[styles.text16, styles.greenColor, { marginTop: 25 }]}>
            Order Number: {getOrderPrefix(order.sellerSetting, 'sales')}
            {newOrder.wholesaleOrderId}
          </Text>

          <View style={{ marginTop: 18, marginBottom: 12 }}>
            <Text style={styles.label}>
              {auth.item.fullName}
              {'\n'}
              {formatAddress(shippingAddress, true)}
            </Text>
          </View>

          <Text style={styles.label}>
            Requested Date: {moment(requestedDate).format('dddd, MMMM D')}
          </Text>

          <TouchableOpacity
            style={{ marginTop: 17, marginBottom: 52 }}
            onPress={viewOrder}
          >
            <Text style={[styles.label, styles.greenColor]}>
              View Order Details
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.ph16}>
          <Text style={styles.commentText}>
            {pricing.priceSheetItems.sellerSetting.company.picksheetTerms}
          </Text>
        </View>
      </View>
    </ScrollView>
  )
}

export default OrderConfirmation
