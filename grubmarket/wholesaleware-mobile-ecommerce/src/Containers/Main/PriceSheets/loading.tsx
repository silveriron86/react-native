/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { PricingState } from '@/Store/Pricing'
import { CommonActions } from '@react-navigation/routers'

const PriceSheetsLoadingContainer = (props: any) => {
  const { pricing } = useSelector((state: { pricing: PricingState }) => state)

  useEffect(() => {
    const { navigation } = props
    console.log(pricing.customerPriceSheets)
    const name =
      pricing.customerPriceSheets.length > 1
        ? 'PriceSheetsList'
        : 'PriceSheetsDetail'

    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{ name }],
      }),
    )
  }, [])
  return null
}

export default PriceSheetsLoadingContainer
