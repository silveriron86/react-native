/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Platform,
} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import ImageProgress from 'react-native-image-progress'
import * as Progress from 'react-native-progress'
import _ from 'lodash'
import styles from '../../_styles'
import { AuthState } from '@/Store/Auth'
import orderStyles from '../_styles'
import SearchBar from '@/Components/SearchBar'
import {
  getPriceSheetItemsModule,
  PricingState,
  getCustomersPriceSheetsModule,
  selectPriceSheetModule,
} from '@/Store/Pricing'
import { getFileUrl } from '@/Services/utils'
import Logout from '@/Store/Auth/Logout'
import { DEFAULT_PRICESHEET } from 'react-native-dotenv'
import { addCartItemModule } from '@/Store/Cart'
import AsyncStorage from '@react-native-async-storage/async-storage'

const productIcon = require('@/Assets/Images/default_product.png')

const PriceSheetsDetailContainer = (props: any) => {
  const [search, setSearch] = useState('')
  const [category, setCategory] = useState('')
  const [priceSheet, setPriceSheet] = useState(
    props.route.params ? props.route.params.ps : null,
  )
  const { pricing, auth } = useSelector(
    (state: { auth: AuthState; pricing: PricingState }) => state,
  )
  const dispatch = useDispatch()

  useEffect(() => {
    global.eventEmitter.addListener('401_ERROR', () => {
      dispatch(Logout.action())
    })
    // dispatch(getSaleItemsModule.action(auth.item.userId))
    // onRefresh()

    if (!priceSheet) {
      dispatch(getCustomersPriceSheetsModule.action(auth.item.userId))
    }
  }, [])

  useEffect(() => {
    if (!priceSheet && pricing.customerPriceSheets.length > 0) {
      let firstItem = pricing.customerPriceSheets[0]
      if (DEFAULT_PRICESHEET) {
        firstItem = pricing.customerPriceSheets.find(
          (item: any) => item.priceSheetId === parseInt(DEFAULT_PRICESHEET, 10),
        )
      }
      // props.navigation.navigate('PriceSheetsDetail', {
      //   priceSheet: firstItem,
      // })
      dispatch(selectPriceSheetModule.action(firstItem))
      setPriceSheet(firstItem)
    }
  }, [pricing.customerPriceSheets])

  useEffect(() => {
    if (priceSheet) {
      onRefresh()
    }
  }, [priceSheet])

  useEffect(() => {
    if (priceSheet) {
      if (!pricing.getPriceSheetItems.loading && pricing.priceSheetItems) {
        const { assignedClients } = priceSheet
        // console.log(assignedClients.wholesaleCompany.companyId)
        const companies = Object.keys(
          _.groupBy(assignedClients, 'wholesaleCompany.companyId'),
        ).length

        if (companies.length > 1) {
          AsyncStorage.setItem('COMPANY_NAME', '')
        } else {
          const { company } = pricing.priceSheetItems.sellerSetting
          AsyncStorage.setItem('COMPANY_NAME', company.companyName)
        }
        global.eventEmitter.emit('CHOSEN_COMPANY')
      }
    }
  }, [pricing.getPriceSheetItems, priceSheet])

  const onRefresh = () => {
    dispatch(getPriceSheetItemsModule.action(priceSheet.linkToken))
  }

  const goDetail = (priceSheetItem: any) => {
    props.navigation.navigate('PriceSheetsCart', {
      priceSheetItem,
      priceSheetName: priceSheet.name,
    })
  }

  const onRequest = (item: any) => {
    dispatch(
      addCartItemModule.action({
        ...item,
        priceSheetItem: null,
        instruction: '',
        count: 1,
      }),
    )
    setTimeout(() => {
      props.navigation.goBack()
    }, 100)
  }

  const onSearch = (searchText: string, categoryText: string) => {
    setSearch(searchText)
    setCategory(categoryText)
  }

  const formatNumber = (num: number, len: number) => {
    if (!num) {
      return '0'
    }

    return (typeof num === 'string' ? parseFloat(num) : num)
      .toFixed(len)
      .toString()
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  const Item = (item: any) => {
    if (!item) {
      return null
    }

    const { priceSheetItemId, variety, wholesaleItem, salePrice } = item
    const { displayPriceSheetPrice } =
      pricing.priceSheetItems.wholesaleCustomerClient

    let newSalePrice: any = 0
    const cost = item.wholesaleItem ? item.wholesaleItem.cost : 0
    if (item.defaultLogic === 'FOLLOW_GROUP') {
      if (item.wholesaleItem && item.wholesaleItem.priceGroupType === 1) {
        newSalePrice = formatNumber(salePrice, 2)
      } else {
        newSalePrice = formatNumber(
          cost / (1.0 - item.margin / 100.0) + item.markup,
          2,
        )
      }
    } else {
      newSalePrice = formatNumber(
        cost / (1.0 - item.margin / 100.0) + item.markup,
        2,
      )
    }

    if (priceSheetItemId === 'none') {
      if (search === '') {
        return null
      }
      return (
        <TouchableOpacity style={[orderStyles.row]}>
          <Image
            source={productIcon}
            style={[
              orderStyles.productIcon,
              {
                width: 25,
                height: 25,
                tintColor: '#E0E0E0',
              },
            ]}
          />
          <View style={[styles.flexRow, styles.flex1]}>
            <Text style={orderStyles.title}>{variety}</Text>
          </View>
          <TouchableOpacity onPress={() => onRequest(item)}>
            <Text style={[styles.label, styles.lightGreenColor]}>Request</Text>
          </TouchableOpacity>
        </TouchableOpacity>
      )
    }

    return (
      <TouchableOpacity
        style={[orderStyles.row]}
        onPress={() => goDetail(item)}
      >
        {wholesaleItem.cover ? (
          <ImageProgress
            source={{ uri: getFileUrl(wholesaleItem.cover, true) }}
            indicator={Progress.Circle}
            indicatorProps={{
              size: 30,
              borderWidth: 2,
              color: '#3079c1',
              unfilledColor: 'rgba(200, 200, 200, 0.2)',
            }}
            style={[orderStyles.productIcon, { resizeMode: 'contain' }]}
          />
        ) : (
          <Image
            source={productIcon}
            style={[
              orderStyles.productIcon,
              {
                width: 25,
                height: 25,
                tintColor: '#E0E0E0',
              },
            ]}
          />
        )}
        <View style={[styles.flexRow, styles.flex1]}>
          <Text style={orderStyles.title}>{wholesaleItem.variety}</Text>
        </View>
        {displayPriceSheetPrice === 1 && (
          <View style={[styles.flexRow, { width: 115 }]}>
            <Text style={[{ width: '100%' }, orderStyles.text]}>
              ${newSalePrice}/{wholesaleItem.inventoryUOM}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    )
  }

  const renderItem = ({ item }: any) => <Item {...item} />

  const { priceSheetItems, getPriceSheetItems, getSaleItems } = pricing
  let categories = []
  let items = []
  let isExactMatch = false
  if (priceSheetItems) {
    categories = _.groupBy(priceSheetItems.priceSheetItemList, (item: any) => {
      return item.wholesaleItem.wholesaleCategory.wholesaleSection.name
    })
    items = priceSheetItems.priceSheetItemList
    isExactMatch =
      priceSheetItems.priceSheetItemList.filter(item => item.variety === search)
        .length === 0
  }

  if (priceSheetItems && (search || category)) {
    items = priceSheetItems.priceSheetItemList.filter(
      (item: any) =>
        item.wholesaleItem.variety
          .toLowerCase()
          .indexOf(search.toLowerCase()) >= 0 &&
        (!category ||
          item.wholesaleItem.wholesaleCategory.wholesaleSection.name ===
            category),
    )
  }

  if (items.length > 0) {
    items = [...items].sort((a: any, b: any) =>
      a.wholesaleItem.variety.localeCompare(b.wholesaleItem.variety),
    )
  }

  const lastItem = {
    priceSheetItemId: 'none',
    wholesaleItemId: null,
    variety: search,
  }

  return (
    <View style={[styles.screen]}>
      {typeof priceSheet !== 'undefined' && priceSheet !== null && (
        <Text style={[styles.subTitle, styles.mt0]}>{priceSheet.name}</Text>
      )}
      <SearchBar
        autoFocus={false}
        categories={categories}
        onChange={onSearch}
      />
      <View style={orderStyles.list}>
        {(search !== '' || category !== '') && (
          <View style={{ margin: 17 }}>
            <Text style={[orderStyles.psBlockItems, { fontSize: 14 }]}>
              Search results
            </Text>
          </View>
        )}
        <FlatList
          data={[...items, isExactMatch ? lastItem : null]}
          renderItem={renderItem}
          keyExtractor={item => (item ? item.priceSheetItemId : 'none')}
          refreshing={getPriceSheetItems.loading || getSaleItems.loading}
          onRefresh={onRefresh}
          keyboardShouldPersistTaps="always"
        />
      </View>
    </View>
  )
}

export default PriceSheetsDetailContainer
