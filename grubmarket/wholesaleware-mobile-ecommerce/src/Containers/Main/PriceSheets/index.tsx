/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import styles, { COLOR_GREEN, COLOR_BORDER } from '../../_styles'
import orderStyles from '../_styles'
import {
  getCustomersPriceSheetsModule,
  PricingState,
  selectPriceSheetModule,
} from '@/Store/Pricing'
import { AuthState } from '@/Store/Auth'
import LoadingOverlay from '@/Components/LoadingOverlay'
import Logout from '@/Store/Auth/Logout'

const PriceSheetsListContainer = (props: any) => {
  const { pricing, auth } = useSelector(
    (state: { pricing: PricingState; auth: AuthState }) => state,
  )
  const dispatch = useDispatch()

  useEffect(() => {
    global.eventEmitter.addListener('401_ERROR', () => {
      dispatch(Logout.action())
    })
    dispatch(getCustomersPriceSheetsModule.action(auth.item.userId))
  }, [])

  const goDetail = (priceSheet: object) => {
    dispatch(selectPriceSheetModule.action(priceSheet))
    props.navigation.navigate('PriceSheetsDetail', {
      ps: priceSheet,
    })
  }

  const colors = [COLOR_GREEN, '#72BCDB', '#854ABF', '#82898A']

  return (
    <ScrollView contentContainerStyle={[styles.screen, styles.scrollView]}>
      <Text style={[styles.subTitle, styles.mt0]}>Price Sheets</Text>
      {/* <View style={styles.subHeader}>
        <Text style={styles.subTitle} />
        <Text style={[styles.subTitle, styles.smTitle, styles.textLeft]} />
      </View> */}
      {
        <View
          style={[
            orderStyles.list,
            {
              marginVertical: 18,
              paddingHorizontal: 16,
              flexDirection: 'row',
              flexWrap: 'wrap',
              alignItems: 'center',
              justifyContent: 'space-between',
              borderTopWidth: 1,
              borderTopColor: COLOR_BORDER,
              paddingTop: 25,
            },
          ]}
        >
          {pricing.customerPriceSheets.map((ps, index) => {
            const matches = ps.name.match(/\b(\w)/g)
            const acronym = matches.join('')

            return (
              <TouchableOpacity
                key={`block-${index}`}
                style={orderStyles.psBlockWrapper}
                onPress={() => goDetail(ps)}
              >
                <View
                  style={[
                    orderStyles.psBlock,
                    { backgroundColor: colors[index % 4] },
                  ]}
                >
                  <Text style={orderStyles.psBlockName}>{acronym}</Text>
                </View>
                <Text style={orderStyles.psBlockTitle}>{ps.name}</Text>
                <Text style={orderStyles.psBlockItems}>
                  {ps.itemCount} items
                </Text>
              </TouchableOpacity>
            )
          })}
        </View>
      }
      <LoadingOverlay loading={pricing.getSaleItems.loading} />
    </ScrollView>
  )
}

export default PriceSheetsListContainer
