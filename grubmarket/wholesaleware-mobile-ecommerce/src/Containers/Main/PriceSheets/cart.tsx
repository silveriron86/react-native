/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect, useRef } from 'react'
import {
  View,
  Text,
  Image,
  Keyboard,
  TextInput,
  TouchableOpacity,
} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import styles, { COLOR_GREEN } from '../../_styles'
import PrimaryButton from '@/Components/PrimaryButton'
import RNPickerSelect from 'react-native-picker-select'
import ImageView from 'react-native-image-view'
import ImageProgress from 'react-native-image-progress'
import * as Progress from 'react-native-progress'
import { KeyboardAvoidingScrollView } from 'react-native-keyboard-avoiding-scroll-view'
import { getFileUrl, hasValue } from '@/Services/utils'
import { AuthState } from '@/Store/Auth'
import { PricingState } from '@/Store/Pricing'
import { addCartItemModule, CartState, getCartItemsModule } from '@/Store/Cart'

const productIcon = require('@/Assets/Images/default_product.png')
const arrowDown = require('@/Assets/Images/arrow_down.png')

const PriceSheetsCartContainer = (props: any) => {
  const { priceSheetItem, priceSheetName, note } = props.route.params
  const pickerRef = useRef(null)
  const [added, setAdded] = useState(0)
  const [imageVisible, setImageVisible] = useState(false)
  const [instruction, setInstruction] = useState(note)
  const { pricing, auth, cart } = useSelector(
    (state: { auth: AuthState; pricing: PricingState; cart: CartState }) =>
      state,
  )
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getCartItemsModule.action())
  }, [])

  const goBack = () => {
    props.navigation.goBack()
    setAdded(0)
  }

  useEffect(() => {
    if (cart.cartItems.length > 0) {
      const found = cart.cartItems.find((c: any) => {
        if (c.priceSheetItem) {
          return (
            c.priceSheetItem.priceSheetItemId ===
            priceSheetItem.priceSheetItemId
          )
        }
        return false
      })
      if (found) {
        // If item is already in the cart, then user is able to adjust the quantity
        setAdded(found.count)
      }
    }
  }, [cart.cartItems])

  const addCart = () => {
    dispatch(
      addCartItemModule.action({
        priceSheetItem: priceSheetItem,
        instruction,
        count: added > 0 ? added : 1,
      }),
    )
    setTimeout(() => {
      global.eventEmitter.emit('CART_CHANGED')
    }, 100)
    // props.navigation.navigate('PriceSheetsDetail')
    // setTimeout(() => {
    //   props.navigation.navigate('CartList')
    // }, 10)
  }

  const openPicker = () => {
    if (pickerRef) {
      pickerRef.current!.togglePicker(true)
    }
  }

  const formatNumber = (num: number, len: number) => {
    if (!num) {
      return '0'
    }

    return (typeof num === 'string' ? parseFloat(num) : num)
      .toFixed(len)
      .toString()
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  let items = [{ label: '0', value: 0 }]
  for (let i = 1; i <= 999; i++) {
    items.push({ label: i.toString(), value: i })
  }

  if (!priceSheetItem) {
    return null
  }

  const { wholesaleItem, salePrice, defaultLogic, margin, markup } =
    priceSheetItem

  let newSalePrice: any = 0
  const cost = wholesaleItem ? wholesaleItem.cost : 0
  if (defaultLogic === 'FOLLOW_GROUP') {
    if (wholesaleItem && wholesaleItem.priceGroupType === 1) {
      newSalePrice = formatNumber(salePrice, 2)
    } else {
      newSalePrice = formatNumber(cost / (1.0 - margin / 100.0) + markup, 2)
    }
  } else {
    newSalePrice = formatNumber(cost / (1.0 - margin / 100.0) + markup, 2)
  }

  const imageURI = wholesaleItem.cover
    ? getFileUrl(wholesaleItem.cover, true)
    : ''

  const { displayPriceSheetPrice } =
    pricing.priceSheetItems.wholesaleCustomerClient

  return (
    <KeyboardAvoidingScrollView
      contentContainerStyle={[styles.screen, styles.scrollView]}
    >
      <Text style={[styles.subTitle, styles.mt0]}>{priceSheetName}</Text>
      {/* <SearchBar autoFocus={false} categories={[]} onChange={() => {}} /> */}
      <View style={{ width: '100%', padding: 16 }}>
        <View
          style={{
            width: '100%',
            height: 137,
            backgroundColor: 'white',
            marginBottom: 24,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {imageURI ? (
            <TouchableOpacity
              onPress={() => setImageVisible(true)}
              style={{ width: '100%' }}
            >
              <ImageProgress
                source={{ uri: imageURI }}
                indicator={Progress.Circle}
                indicatorProps={{
                  size: 100,
                  borderWidth: 5,
                  color: `${COLOR_GREEN}80`,
                  unfilledColor: `${COLOR_GREEN}60`,
                }}
                style={styles.fullSize}
                resizeMode="contain"
              />
            </TouchableOpacity>
          ) : (
            <Image
              source={productIcon}
              style={{
                width: 94,
                height: 94,
                resizeMode: 'contain',
                tintColor: '#E0E0E0',
              }}
            />
          )}
        </View>

        <ImageView
          images={[
            {
              source: { uri: imageURI },
            },
          ]}
          isVisible={imageVisible}
          onClose={() => setImageVisible(false)}
          imageIndex={0}
        />
        <View style={[styles.flexRow, { alignItems: 'flex-end' }]}>
          <View style={styles.flex1}>
            <Text style={[styles.text16, { lineHeight: 16 }]}>
              {wholesaleItem.variety}
            </Text>
          </View>
          {displayPriceSheetPrice === 1 && (
            <View style={[styles.flex1, { paddingBottom: 1 }]}>
              <Text style={styles.alignRight}>
                ${newSalePrice}/{wholesaleItem.inventoryUOM}
              </Text>
            </View>
          )}
        </View>
        {hasValue(wholesaleItem.origin) && (
          <View>
            <Text style={styles.text14}>Origin: {wholesaleItem.origin}</Text>
          </View>
        )}
        {hasValue(wholesaleItem.suppliers) && (
          <View>
            <Text style={styles.text14}>Brand: {wholesaleItem.suppliers}</Text>
          </View>
        )}
        {hasValue(wholesaleItem.packing) && (
          <View style={{ marginBottom: 30 }}>
            <Text style={styles.text14}>Packing: {wholesaleItem.packing}</Text>
          </View>
        )}

        <Text style={styles.label}>Special instructions</Text>
        <TextInput
          value={instruction}
          onChangeText={setInstruction}
          keyboardType="default"
          returnKeyType="done"
          multiline={true}
          blurOnSubmit={true}
          onSubmitEditing={() => {
            Keyboard.dismiss()
          }}
          placeholder="Share any special request(s) regarding this item"
          style={styles.textArea}
        />
      </View>

      <View
        style={{ paddingHorizontal: 16, paddingVertical: 32, width: '100%' }}
      >
        {added > 0 ? (
          <>
            <TouchableOpacity
              onPress={openPicker}
              style={[styles.flexRow, styles.vCenter, styles.inCartView]}
            >
              <View style={styles.flex1}>
                <Text style={styles.pickerInput}>{added} in cart</Text>
              </View>
              <View style={styles.pickerArrow}>
                <Image
                  source={arrowDown}
                  style={{ width: 8.33, height: 5, resizeMode: 'stretch' }}
                />
              </View>
            </TouchableOpacity>
            <RNPickerSelect
              ref={pickerRef}
              style={{ viewContainer: { opacity: 0, height: 0 } }}
              value={added}
              placeholder={{}}
              // textInputProps={{}}
              onValueChange={setAdded}
              items={items}
            />
            <PrimaryButton onPress={addCart} text="Save to Cart" />
          </>
        ) : (
          <PrimaryButton onPress={addCart} text="Add to Cart" />
        )}
      </View>
    </KeyboardAvoidingScrollView>
  )
}

export default PriceSheetsCartContainer
