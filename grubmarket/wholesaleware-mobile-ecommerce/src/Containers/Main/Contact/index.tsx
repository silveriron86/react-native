/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { ScrollView, View, Text, Image, Linking, Alert } from 'react-native'
import { useSelector } from 'react-redux'
import _ from 'lodash'
import styles, { COLOR_BG, COLOR_BORDER } from '../../_styles'
import orderStyles from '../_styles'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { OrderState } from '@/Store/Order'
import { hasValue, formatAddress } from '@/Services/utils'
import { PricingState } from '@/Store/Pricing'
import Config from '@/Config'

const emailIcon = require('@/Assets/Images/contact_mail.png')
const messageIcon = require('@/Assets/Images/contact_message.png')
const phoneIcon = require('@/Assets/Images/contact_phone.png')

const ContactContainer = () => {
  const { pricing } = useSelector(
    (state: { order: OrderState; pricing: PricingState }) => state,
  )

  const { customerPriceSheets } = pricing

  const openLinking = async (url: string) => {
    const supported = await Linking.canOpenURL(url)

    if (supported) {
      // Opening the link with some app, if the URL scheme is "http" the web link should be opened
      // by some browser in the mobile
      await Linking.openURL(url)
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`)
    }
  }

  const renderContent = () => {
    let all: any[] = []
    let rows: any[] = []
    if (customerPriceSheets) {
      customerPriceSheets.forEach((cp: any) => {
        const result = _.groupBy(cp.assignedClients, (client: any) => {
          return client.wholesaleClient.wholesaleCompany.companyId
        })
        Object.keys(result).forEach(k => {
          all.push(result[k][0])
        })
      })
      const result = _.groupBy(all, (client: any) => {
        return client.wholesaleClient.wholesaleCompany.companyId
      })
      Object.keys(result).forEach(k => {
        rows.push(result[k][0])
      })
    }

    console.log('rows = ', rows)
    return rows.map((row: any) => {
      const company = row.wholesaleClient.wholesaleCompany
      const { phone, mobileNumber, email, mainBillingAddress } = company
      const imagePath = row.companyImagePath

      let companyName = ''
      if (company) {
        companyName = company.companyName
      }

      return (
        <View
          style={[
            orderStyles.list,
            styles.ph16,
            {
              backgroundColor: COLOR_BG,
              borderTopWidth: 1,
              borderTopColor: COLOR_BORDER,
              paddingTop: 17,
            },
          ]}
        >
          {mainBillingAddress !== null && (
            <View>
              <Text style={[styles.subTitle, styles.textLeft]}>
                {companyName}
                {'\n'}
                {formatAddress(mainBillingAddress.address, true)}
              </Text>
            </View>
          )}
          <View style={{ marginTop: 5, marginBottom: 14 }}>
            <Text style={[styles.subTitle, styles.textLeft]}>
              Monday—Saturday: 8pm—8am{'\n'}Sunday: 8pm—2am
            </Text>
          </View>
          {hasValue(phone) && (
            <TouchableOpacity
              style={[styles.flexRow, styles.vCenter, { marginVertical: 21 }]}
              onPress={() => openLinking(`tel:${phone}`)}
            >
              <Image source={phoneIcon} style={styles.contactIcon} />
              <Text style={[styles.text16, styles.greenColor]}>{phone}</Text>
            </TouchableOpacity>
          )}

          {hasValue(mobileNumber) && (
            <TouchableOpacity
              style={[styles.flexRow, styles.vCenter, { marginVertical: 21 }]}
              onPress={() => openLinking(`sms:${mobileNumber}`)}
            >
              <Image source={messageIcon} style={styles.contactIcon} />
              <Text style={[styles.text16, styles.greenColor]}>
                {mobileNumber}
              </Text>
            </TouchableOpacity>
          )}

          {hasValue(email) && (
            <TouchableOpacity
              style={[styles.flexRow, styles.vCenter, { marginVertical: 21 }]}
              onPress={() => openLinking(`mailto:${email}`)}
            >
              <Image source={emailIcon} style={styles.contactIcon} />
              <Text style={[styles.text16, styles.greenColor]}>{email}</Text>
            </TouchableOpacity>
          )}

          {hasValue(imagePath) && (
            <View
              style={[
                styles.fullWidth,
                styles.center,
                { marginVertical: 45, height: 160 },
              ]}
            >
              <Image
                source={{
                  uri: imagePath.replace(
                    'http://staging.grubmarket.com',
                    Config.getBaseURL(),
                  ),
                }}
                style={[{ resizeMode: 'contain' }, styles.screen]}
              />
            </View>
          )}
        </View>
      )
    })
  }

  return (
    <ScrollView contentContainerStyle={[styles.screen, styles.scrollView]}>
      <View style={[styles.subHeader, styles.fullWidth]}>
        <Text style={[styles.subTitle, styles.mt0]}>Contact Us</Text>
      </View>
      {renderContent()}
    </ScrollView>
  )
}

export default ContactContainer
