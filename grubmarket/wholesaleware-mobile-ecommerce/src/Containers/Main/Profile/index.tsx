/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import styles, { COLOR_BG, COLOR_BORDER } from '../../_styles'
import Logout from '@/Store/Auth/Logout'
import PrimaryButton from '@/Components/PrimaryButton'
import { AuthState } from '@/Store/Auth'
import LoadingOverlay from '@/Components/LoadingOverlay'
import { navigateAndSimpleReset } from '@/Navigators/Root'
import { getUserModule, SettingState, viewLogoModule } from '@/Store/Setting'

const ProfileContainer = (props: any) => {
  const { auth, setting } = useSelector(
    (state: { auth: AuthState; setting: SettingState }) => state,
  )
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getUserModule.action(auth.item.userId))
  }, [])

  const logOut = () => {
    // dispatch(Init.action())
    dispatch(Logout.action())
    dispatch(viewLogoModule.action(false))
    setTimeout(() => {
      navigateAndSimpleReset('Login')
    }, 100)
  }

  return (
    <View style={styles.screen}>
      {/* <Text style={styles.subTitle}>Happy Dog Pizza</Text> */}
      <View style={[styles.subHeader, styles.fullWidth]}>
        <Text style={[styles.subTitle, styles.mt0]}>Profile Information</Text>
      </View>
      <View
        style={[
          styles.fullWidth,
          styles.ph16,
          {
            backgroundColor: COLOR_BG,
            borderTopWidth: 1,
            borderTopColor: COLOR_BORDER,
            paddingTop: 20,
            flex: 1,
          },
        ]}
      >
        {setting.user !== null && (
          <View style={{ height: 100 }}>
            <Text style={styles.text16}>
              {setting.user.firstName} {setting.user.lastName}
            </Text>
            <View>
              <Text style={styles.text14}>{setting.user.emailAddress}</Text>
            </View>
          </View>
        )}
        <View style={{ paddingVertical: 32, width: '100%' }}>
          <PrimaryButton onPress={logOut} text="Log Out" />
        </View>
      </View>
      <LoadingOverlay loading={setting.getUser.loading} />
    </View>
  )
}

export default ProfileContainer
