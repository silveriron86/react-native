import { apiInstance as api } from '@/Services'

export const getCompanyPriceSheets = async (userId: string) => {
  const url = `/pricesheet/userCompany/${userId}`
  const response = await api.get(url)
  const result = response.data.body.data
  return result
}

export const getCustomersPricesheets = async (userId: string) => {
  const url = `/account/user/${userId}/customer/pricesheets/list`
  const response = await api.get(url)
  const result = response.data.body.data
  console.log(url, result)
  return result
}

export const getPriceSheetItems = async (linkToken: string) => {
  let url = `pricesheet/getPriceSheetFromToken/${linkToken}`
  const response = await api.get(url)
  const result = response.data.body.data
  // console.log(url, result)
  return result
}

export const getAllItems = async (companyName: string) => {
  const url = `/inventory/${companyName}/items/list?type=PRICESHEET`
  const response = await api.get(url)
  const result = response.data.body.data
  return result
}
