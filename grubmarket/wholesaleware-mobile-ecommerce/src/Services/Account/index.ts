import { apiInstance as api } from '@/Services'

export const getUser = async (userId: string) => {
  const url = `/account/user/${userId}`
  const response = await api.get(url)
  const result = response.data.body.data
  return result
}
