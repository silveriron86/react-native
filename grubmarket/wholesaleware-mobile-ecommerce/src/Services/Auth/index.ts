import qs from 'qs'
import {
  authInstance as auth,
  apiInstance as api,
  setApiToken,
} from '@/Services'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { b64EncodeUnicode } from '../utils'
import Config from '@/Config'

export const loginByTokenService = async (data: any) => {
  const requestData: any = {
    username: data.email,
    password: data.password,
    grant_type: 'password',
    scope: 'all',
  }
  const credentials = 'wsw:oauth2'
  const response = await auth.post(
    'wsw-auth/oauth/token',
    qs.stringify(requestData),
    {
      headers: {
        Authorization: `Basic ${b64EncodeUnicode(credentials)}`,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
  )
  const token = response.data.access_token
  AsyncStorage.setItem('accessToken', token)
  await setApiToken(token)
  const userInfo = await getUserInfoService(token)
  return userInfo
}

export const getUserInfoService = async (token: string) => {
  const response = await api.post('session/userInfo', null, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
  const result = response.data.body.data
  AsyncStorage.setItem('AUTH', JSON.stringify(result))
  return result
}

export const logOutService = async () => {
  AsyncStorage.getItem('REMEMBERED_EMAIL', (_err, value) => {
    AsyncStorage.clear()
    if (value) {
      AsyncStorage.setItem('REMEMBERED_EMAIL', value)
    }
  })
  return {}
}

export const resetPasswordService = async (email: string) => {
  const response = await auth.post(
    `wsw-hr/api/auth/reset-password?account=${email}`,
    null,
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
  )
  return response.data
}

export const judgeConstantRatio = (orderItem: any) => {
  let pricingUOM = orderItem.pricingUOM ? orderItem.pricingUOM : orderItem.UOM
  let unitUOM = orderItem.overrideUOM ? orderItem.overrideUOM : orderItem.UOM

  let unitProductUOM =
    orderItem != null && orderItem.wholesaleProductUomList != null
      ? orderItem.wholesaleProductUomList.filter((uom: any) => {
          return uom.name === unitUOM
        })
      : []
  let pricingProductUOM =
    orderItem != null && orderItem.wholesaleProductUomList != null
      ? orderItem.wholesaleProductUomList.filter((uom: any) => {
          return uom.name === pricingUOM
        })
      : []
  if (
    (unitUOM === orderItem.inventoryUOM ||
      (unitProductUOM.length > 0 && unitProductUOM[0].constantRatio)) &&
    pricingProductUOM.length > 0 &&
    !pricingProductUOM[0].constantRatio
  ) {
    return false
  } else {
    return true
  }
}
