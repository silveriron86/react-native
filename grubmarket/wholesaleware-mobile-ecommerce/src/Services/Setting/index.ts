import { apiInstance as api } from '@/Services'

export const getUserSetting = async (sellerId: string) => {
  const url = `/session/userSettings?sellerId=${sellerId}`
  const response = await api.get(url)
  const result = response.data.userSetting
  return result
}

export const getTheme = async () => {
  const response = await api.get('session/getTheme')
  const result = response.data
  return result
}

export const getLogo = async () => {
  const response = await api.get('session/getLogo')
  const result = response.data
  return result
}

export const getCompanyAddress = async () => {
  const response = await api.get('session/company-address')
  const result = response.data.body.data
  return result
}
