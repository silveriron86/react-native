import { apiInstance as api } from '@/Services'

export const getOrdersForCustomerUserId = async (data: any) => {
  let url = `/account/customer/orders?page=${data.page}&pageSize=${data.pageSize}`
  if (data.fromDate.length > 0) {
    url += '&from=' + data.fromDate
  }
  if (data.toDate.length > 0) {
    url += '&to=' + data.toDate
  }
  const response = await api.get(url)
  const result = response.data.body.data
  return result
}

export const getWarehouseSalesOrdersByDate = async (data: any) => {
  let url = `/inventory/warehouse/wholesaleSaleOrders?page=${data.page}&pageSize=${data.pageSize}`
  if (data.fromDate.length > 0) {
    url += '&from=' + data.fromDate
  }
  if (data.toDate.length > 0) {
    url += '&to=' + data.toDate
  }
  if (data.searchStr !== '') {
    url += `&search=${data.searchStr}`
  }
  if (data.orderByFilter !== '') {
    url += `&orderBy=${data.orderByFilter}&direction=${data.direction}`
  }
  const response = await api.get(url)
  const result = response.data.body.data
  return result
}

export const getOrderItemsById = async (id: string) => {
  const response = await api.get(
    `/account/order/${id}/items/list?timestamp=${new Date().getTime()}`,
  )
  const result = response.data.body.data
  return result
}

export const getOrderOneOffItemsById = async (id: string) => {
  const response = await api.get(`/account/order/${id}/one-off-item/list`)
  const result = response.data.body.data
  return result
}

export const getAddresses = async (customerId: string) => {
  const url = `/account/user/getAddress/${customerId}`
  const response = await api.get(url)
  const result = response.data.body.data
  return result
}

export const duplicateOrder = async (data: any) => {
  const response = await api.post('/inventory/duplicate/orders', data)
  const result = response.data.body.data.wholesaleOrderId
  return result
}

export const createOrder = async (arg: any) => {
  const { data, token } = arg
  const response = await api.post(`/inventory/create/order/${token}`, data)
  const result = response.data.body.data
  return result
}

export const getOrderByToken = async (token: string) => {
  const url = `/inventory/order/token/${token}`
  console.log(url)
  const response = await api.get(url)
  const result = response.data.body.data
  console.log(result)
  return result
}

export const getOrderItemsByToken = async (token: string) => {
  const url = `/account/order/token/${token}/items/list`
  console.log(url)
  const response = await api.get(url)
  const result = response.data.body.data
  console.log('items = ', response)
  return result
}

export const getCustomerItems = async (sellerId: string) => {
  const response = await api.get(
    `/inventory/customer-items?sellerId=${sellerId}`,
  )
  const result = response.data.body.data
  return result
}
