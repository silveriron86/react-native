/* eslint-disable no-bitwise */
import moment from 'moment'
import Config from '@/Config'

// import btoa from 'btoa'
const chars =
  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
const Base64 = {
  btoa: (input: string = '') => {
    let str = input
    let output = ''

    for (
      let block = 0, charCode, i = 0, map = chars;
      str.charAt(i | 0) || ((map = '='), i % 1);
      output += map.charAt(63 & (block >> (8 - (i % 1) * 8)))
    ) {
      charCode = str.charCodeAt((i += 3 / 4))

      if (charCode > 0xff) {
        throw new Error(
          "'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.",
        )
      }

      block = (block << 8) | charCode
    }

    return output
  },

  atob: (input: string = '') => {
    let str = input.replace(/=+$/, '')
    let output = ''

    if (str.length % 4 === 1) {
      throw new Error(
        "'atob' failed: The string to be decoded is not correctly encoded.",
      )
    }
    for (
      let bc = 0, bs = 0, buffer, i = 0;
      (buffer = str.charAt(i++));
      ~buffer && ((bs = bc % 4 ? bs * 64 + buffer : buffer), bc++ % 4)
        ? (output += String.fromCharCode(255 & (bs >> ((-2 * bc) & 6))))
        : 0
    ) {
      buffer = chars.indexOf(buffer)
    }

    return output
  },
}

export const b64EncodeUnicode = (str: any) => {
  return Base64.btoa(
    encodeURIComponent(str).replace(
      /%([0-9A-F]{2})/g,
      function (match, p1: String) {
        return String.fromCharCode(`0x${p1}`)
      },
    ),
  )
}

export const numberMultipy = (arg1: number, arg2: number) => {
  var m = 0,
    s1 = arg1 != null ? arg1.toString() : '0',
    s2 = arg2 != null ? arg2.toString() : '0'
  try {
    m += s1.split('.')[1].length
  } catch (e) {}
  try {
    m += s2.split('.')[1].length
  } catch (e) {}
  return (
    (Number(s1.replace('.', '')) * Number(s2.replace('.', ''))) /
    Math.pow(10, m)
  )
}

export const judgeConstantRatio = (orderItem: any) => {
  let pricingUOM = orderItem.pricingUOM ? orderItem.pricingUOM : orderItem.UOM
  let unitUOM = orderItem.overrideUOM ? orderItem.overrideUOM : orderItem.UOM

  let unitProductUOM =
    orderItem != null && orderItem.wholesaleProductUomList != null
      ? orderItem.wholesaleProductUomList.filter((uom: any) => {
          return uom.name === unitUOM
        })
      : []
  let pricingProductUOM =
    orderItem != null && orderItem.wholesaleProductUomList != null
      ? orderItem.wholesaleProductUomList.filter((uom: any) => {
          return uom.name === pricingUOM
        })
      : []
  if (
    (unitUOM === orderItem.inventoryUOM ||
      (unitProductUOM.length > 0 && unitProductUOM[0].constantRatio)) &&
    pricingProductUOM.length > 0 &&
    !pricingProductUOM[0].constantRatio
  ) {
    return false
  } else {
    return true
  }
}

export const basePriceToRatioPrice = (
  currentUom: string,
  price: number,
  item: any,
  digit?: number,
  useFactor? = false,
) => {
  if (currentUom === item.inventoryUOM) {
    price = price
  } else {
    if (
      item.wholesaleProductUomList != null &&
      item.wholesaleProductUomList.length > 0
    ) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom === uom.name) {
          if (useFactor) {
            price = (price / uom.ratio) * (1 + uom.priceFactor / 100)
          } else {
            price = price / uom.ratio
          }

          break
        }
      }
    }
  }
  price = typeof price === 'string' ? parseFloat(price) : price
  if (digit) {
    return mathRoundFun(price, digit)
  }
  return mathRoundFun(price, 2)
}

export const ratioPriceToBasePrice = (
  currentUom: string,
  price: number,
  item: any,
  digit?: number,
) => {
  if (currentUom === item.inventoryUOM) {
    price = price
  } else {
    if (
      item.wholesaleProductUomList != null &&
      item.wholesaleProductUomList.length > 0
    ) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom === uom.name) {
          price = price * uom.ratio
          break
        }
      }
    }
  }
  price = typeof price === 'string' ? parseFloat(price) : price
  if (digit) {
    return mathRoundFun(price, digit)
  }
  return mathRoundFun(price, 2)
}

export const baseQtyToRatioQty = (
  currentUom: string,
  qty: number,
  item: any,
  digit?: number,
) => {
  if (currentUom === item.inventoryUOM) {
    qty = qty
  } else {
    if (
      item.wholesaleProductUomList != null &&
      item.wholesaleProductUomList.length > 0
    ) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom === uom.name) {
          qty = qty * uom.ratio
          break
        }
      }
    }
  }
  qty = typeof qty === 'string' ? parseFloat(qty) : qty
  if (digit) {
    return mathRoundFun(qty, digit)
  }
  return mathRoundFun(qty, 2)
}

export const ratioQtyToBaseQty = (
  currentUom: string,
  qty: number,
  item: any,
  digit?: number,
) => {
  if (currentUom === item.inventoryUOM) {
    qty = qty
  } else {
    if (
      item.wholesaleProductUomList != null &&
      item.wholesaleProductUomList.length > 0
    ) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom === uom.name) {
          qty = qty / uom.ratio
          break
        }
      }
    }
  }
  qty = typeof qty === 'string' ? parseFloat(qty) : qty
  if (digit) {
    return mathRoundFun(qty, digit)
  }
  return mathRoundFun(qty, 2)
}

export const inventoryQtyToRatioQty = (
  currentUom: string,
  qty: number,
  item: any,
  digit?: number,
) => {
  if (currentUom === item.inventoryUOM) {
    qty = qty
  } else {
    if (
      item.wholesaleProductUomList != null &&
      item.wholesaleProductUomList.length > 0
    ) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom === uom.name) {
          qty = qty * uom.ratio
          break
        }
      }
    }
  }
  qty = typeof qty === 'string' ? parseFloat(qty) : qty
  if (digit) {
    return mathRoundFun(qty, digit)
  }
  return mathRoundFun(qty, 2)
}

export const ratioQtyToInventoryQty = (
  currentUom: string,
  qty: number,
  item: any,
  digit?: number,
) => {
  if (currentUom === item.inventoryUOM) {
    qty = qty
  } else {
    if (
      item.wholesaleProductUomList != null &&
      item.wholesaleProductUomList.length > 0
    ) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom === uom.name) {
          qty = qty / uom.ratio
          break
        }
      }
    }
  }
  qty = typeof qty === 'string' ? parseFloat(qty) : qty
  if (digit) {
    return mathRoundFun(qty, digit)
  }
  return mathRoundFun(qty, 2)
}

export const inventoryPriceToRatioPrice = (
  currentUom: string,
  price: number,
  item: any,
  digit?: number,
) => {
  if (currentUom === item.inventoryUOM) {
    price = price
  } else {
    if (
      item.wholesaleProductUomList != null &&
      item.wholesaleProductUomList.length > 0
    ) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom === uom.name) {
          price = price / uom.ratio
          break
        }
      }
    }
  }
  price = typeof price === 'string' ? parseFloat(price) : price
  if (digit) {
    return mathRoundFun(price, digit)
  }
  return mathRoundFun(price, 2)
}

export const mathRoundFun = (value: number, decimal: number) => {
  return Math.round(value * Math.pow(10, decimal)) / Math.pow(10, decimal)
}

export const formatOrderItem = (item: any) => {
  return {
    wholesaleItemId: item.itemId,
    wholesaleOrderItemId: item.wholesaleOrderItemId,
    quantity: item.quantity ? item.quantity : 0,
    picked: item.picked,
    cost: item.cost,
    price: item.price,
    margin: item.margin,
    freight: item.freight,
    status: item.status,
    UOM: item.UOM,
  }
}

export function formatAddress(item: any, multiLines: boolean = false) {
  if (!item) {
    return ''
  }
  return `${
    item.department && item.department !== 'null' ? item.department + ' ' : ''
  }${item.street1}${multiLines === true ? '\n' : ' '}${item.city} ${
    item.state
  }, ${item.zipcode}`
}

export const getFulfillmentDate = (sellerSetting: any) => {
  let fulfillmentDate = moment().format('MM/DD/YYYY')
  if (sellerSetting && sellerSetting.company) {
    if (sellerSetting.company.targetFulfillmentDate === 'Tomorrow') {
      fulfillmentDate = moment().add(1, 'days').format('MM/DD/YYYY')
    } else if (
      sellerSetting.company.targetFulfillmentDate ===
        'Today with Cut-off Time' &&
      sellerSetting.company.targetFulfillmentDate !== null
    ) {
      const isPassed = moment().isAfter(
        moment(sellerSetting.company.cutOffTime, 'hh:mm:a'),
      )
      if (isPassed) {
        fulfillmentDate = moment().add(1, 'days').format('MM/DD/YYYY')
      } else {
        fulfillmentDate = moment().format('MM/DD/YYYY')
      }
    } else {
      fulfillmentDate = moment().format('MM/DD/YYYY')
    }
  }

  return fulfillmentDate
}

export const getOrderPrefix = (setting: any, type: string) => {
  if (setting && setting.company) {
    if (type === 'sales') {
      return setting.company.salesPrefix ? setting.company.salesPrefix : ''
    } else {
      return setting.company.purchasePrefix
        ? setting.company.purchasePrefix
        : ''
    }
  } else {
    return ''
  }
}

export const getFileUrl = (url: string, isPublic: boolean) => {
  if (url == null) {
    return ''
  }

  if (url.indexOf('http') > -1) {
    return url
  } else {
    if (isPublic) {
      return `${Config.getAwsPublic()}/${url}`
    } else {
      return `${Config.getAwsPrivate()}/${b64EncodeUnicode(
        global.accessToken,
      )}/${url}`
    }
  }
}

export const hasValue = (v: any) => {
  return v !== null && v !== ''
}
