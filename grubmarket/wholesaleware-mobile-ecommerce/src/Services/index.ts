import axios from 'axios'
import handleError from '@/Services/utils/handleError'
import Config from '@/Config'
import AsyncStorage from '@react-native-async-storage/async-storage'
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter'

const eventEmitter = new EventEmitter()
global.eventEmitter = eventEmitter

export const authInstance = axios.create({
  baseURL: Config.getAuthPath(),
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
  },
  timeout: 3000,
})

authInstance.interceptors.response.use(
  response => {
    return response
  },
  res => {
    const { message, response } = res
    console.log('message = ', message)
    console.log(res)
    const { data, status } = response
    console.log(message)
    return handleError({ message, data, status })
  },
)

export const apiInstance = axios.create({
  baseURL: Config.getApiPath(),
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 10000,
})

export const setApiToken = async (token?: string) => {
  let accessToken = ''
  if (token) {
    AsyncStorage.setItem('accessToken', token)
    accessToken = token
  }
  global.accessToken = accessToken
  apiInstance.interceptors.request.use(function (config) {
    config.headers.Authorization = `Bearer ${global.accessToken}`
    return config
  })
}

AsyncStorage.getItem('accessToken', (_err, token) => {
  setApiToken(token)
})

apiInstance.interceptors.response.use(
  response => {
    // console.log('api response = ', response)
    return response
  },
  res => {
    const { message, response } = res
    console.log(message)
    console.log(res)
    const { data, status } = response
    console.log('error = ', { message, data, status })
    if (status === 401) {
      global.eventEmitter.emit('401_ERROR')
    }
    return handleError({ message, data, status })
  },
)
