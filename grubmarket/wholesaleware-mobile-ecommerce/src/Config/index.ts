import { ENVIRONMENT } from 'react-native-dotenv'
const _env: any = {
  TEST: {
    BASE: 'http://wholesaleware.grubmarket.com',
    API: 'http://wholesaleware.grubmarket.com/api/v1/wholesale/',
    AUTH: 'http://wsw-dev-gateway.us-east-2.elasticbeanstalk.com/',
    AWS_PRIVATE: 'http://d1i5d4tau1jjns.cloudfront.net',
    AWS_PUBLIC: 'http://dh99yd9ydsyt1.cloudfront.net',
  },
  STAGING: {
    BASE: 'http://wholesaleware.grubmarket.com',
    API: 'http://wholesaleware.grubmarket.com/api/v1/wholesale/',
    AUTH: 'http://wholesaleware-dev-gateway.us-east-2.elasticbeanstalk.com/',
    AWS_PRIVATE: 'http://d1i5d4tau1jjns.cloudfront.net',
    AWS_PUBLIC: 'http://dh99yd9ydsyt1.cloudfront.net',
  },
  PRODUCTION: {
    BASE: 'https://wholesaleware.com',
    API: 'https://wholesaleware.com/api/v1/wholesale',
    AUTH: 'https://aut.wholesaleware.com/',
    AWS_PRIVATE: 'https://d1wkky6g2l0f4m.cloudfront.net',
    AWS_PUBLIC: 'https://d2pa4uyr6dz0nu.cloudfront.net',
  },
  ENVIRONMENT,
}

const Config = {
  getBaseURL: () => {
    return _env[_env.ENVIRONMENT].BASE
  },

  getApiPath: () => {
    return _env[_env.ENVIRONMENT].API
  },

  getAuthPath: () => {
    return _env[_env.ENVIRONMENT].AUTH
  },

  getAwsPrivate: () => {
    return _env[_env.ENVIRONMENT].AWS_PRIVATE
  },

  getAwsPublic: () => {
    return _env[_env.ENVIRONMENT].AWS_PUBLIC
  },
}

export default Config
