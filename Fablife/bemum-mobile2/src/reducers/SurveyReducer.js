import {SurveyConstants} from '../constants';
import FoodPreferencesQuestions from '../constants/Questionnaires/FoodPreferences';
import DietarySurveyQuestions from '../constants/Questionnaires/DietarySurvey';

const cloneFormData = data => {
  let ret = {...data};
  ret.questions.map(question => {
    if (typeof question.value === 'undefined') {
      question.value = null;

      if (question.ui === 'switch') {
        question.value = false;
      }

      if (/*question.required === true && */ question.ui === 'checkbox') {
        question.value = false;
      }
    }

    if (question.ui === 'switch' && question.value === null) {
      question.value = false;
    }
  });
  ret.data = {...ret.questions};
  return ret;
};

const initialState = {
  loadedSurveys: 0,
  InputtingSurveyIndex: 0,
  InputtingFollowupSurveyIndex: 0,

  signup_profileFormData: cloneFormData(SurveyConstants.CATEGORIES[0]),
  signup_healthFormData: cloneFormData(SurveyConstants.CATEGORIES[1]),
  signup_reproductive_healthFormData: cloneFormData(
    SurveyConstants.CATEGORIES[2],
  ),
  signup_physical_activityFormData: cloneFormData(
    SurveyConstants.CATEGORIES[3],
  ),
  signup_sleep_fatigueFormData: cloneFormData(SurveyConstants.CATEGORIES[4]),
  signup_stressFormData: cloneFormData(SurveyConstants.CATEGORIES[5]),
  signup_moral_wellbeingFormData: cloneFormData(SurveyConstants.CATEGORIES[6]),
  signup_memory_concentrationFormData: cloneFormData(
    SurveyConstants.CATEGORIES[7],
  ),
  signup_oxydative_stressFormData: cloneFormData(SurveyConstants.CATEGORIES[8]),

  followup_profileFormData: cloneFormData(
    SurveyConstants.FOLLOWUP_CATEGORIES[0],
  ),
  followup_healthFormData: cloneFormData(
    SurveyConstants.FOLLOWUP_CATEGORIES[1],
  ),
  followup_reproductive_healthFormData: cloneFormData(
    SurveyConstants.FOLLOWUP_CATEGORIES[2],
  ),
  followup_physical_activityFormData: cloneFormData(
    SurveyConstants.FOLLOWUP_CATEGORIES[3],
  ),
  followup_sleep_fatigueFormData: cloneFormData(
    SurveyConstants.FOLLOWUP_CATEGORIES[4],
  ),
  followup_stressFormData: cloneFormData(
    SurveyConstants.FOLLOWUP_CATEGORIES[5],
  ),
  followup_moral_wellbeingFormData: cloneFormData(
    SurveyConstants.FOLLOWUP_CATEGORIES[6],
  ),
  followup_memory_concentrationFormData: cloneFormData(
    SurveyConstants.FOLLOWUP_CATEGORIES[7],
  ),

  preference_foodFormData: cloneFormData({
    form_id: 'preference_food',
    questions: FoodPreferencesQuestions,
  }),

  dietary_surveyFormData: cloneFormData({
    form_id: 'dietary_survey',
    questions: DietarySurveyQuestions,
  }),

  'genetic-user_genetic-userFormData': {},
  postResult: {},
  signupSurveyData: {},
  preferenceSurveyData: {},
  followupSurveyData: {},
  noticeSurveyData: {},
  geneticSurveyData: {},
  searchData: {},
  error: null,
};

function SurveyReducer(state = initialState, action) {
  switch (action.type) {
    case SurveyConstants.COMPLETED_SURVEY:
      return Object.assign({}, state, {
        InputtingSurveyIndex: action.response,
        error: null,
      });

    case SurveyConstants.COMPLETED_FOLLOWUP_SURVEY:
      return Object.assign({}, state, {
        InputtingFollowupSurveyIndex: action.response,
        error: null,
      });

    case SurveyConstants.LOADED_SURVEY:
      return Object.assign({}, state, {
        loadedSurveys: action.response,
        error: null,
      });

    case SurveyConstants.UPDATE_FORM_DATA:
      let res = {
        error: null,
      };

      const {type, form_id} = action.response;
      let formData =
        type === 'signup' && form_id === 'preference_food'
          ? state[`${form_id}FormData`]
          : state[`${type}_${form_id}FormData`];

      formData.questions.forEach((question, idx) => {
        if (question.question_id === action.response.question_id) {
          question.value = action.response.value;
        }
      });
      res[`${type}_${form_id}FormData`] = formData;
      return Object.assign({}, state, res);

    case SurveyConstants.GET_FORM_SUCCESS:
      let ret = {
        error: null,
      };

      // console.log('***', action.response);
      const {data, id} = action.response;
      const formType = action.response.type;
      if (data) {
        const formName =
          formType === 'signup' && id === 'preference_food'
            ? `${id}FormData`
            : `${formType}_${id}FormData`;
        // console.log(formName);
        // console.log(state[formName]);

        const entries = Object.entries(data);
        entries.forEach(e => {
          const key = e[0];
          const value = e[1];
          const foundIndex = state[formName].questions.findIndex(
            q => q.question_id === key,
          );
          if (foundIndex >= 0) {
            const qType = state[formName].questions[foundIndex].ui;
            state[formName].questions[foundIndex].value =
              qType === 'stepper' && value !== null ? parseFloat(value) : value;

            if (qType === 'date') {
              if (value && value.length === 10) {
                state[formName].questions[
                  foundIndex
                ].value = `${value}T00:00:00`;
              }
            }
          }
        });
        ret[formName] = state[formName];
      }

      let r = Object.assign({}, state, ret);
      return r;

    case SurveyConstants.GET_FORM_ERROR:
      return Object.assign({}, state, {
        formData: {},
        error: action.error,
      });

    case SurveyConstants.POST_FORM_SUCCESS:
      return Object.assign({}, state, {
        postResult: action.response,
        error: null,
      });

    case SurveyConstants.POST_FORM_ERROR:
      return Object.assign({}, state, {
        postResult: {},
        error: action.error,
      });

    case SurveyConstants.GET_SEARCH_DATA_SUCCESS:
      return Object.assign({}, state, {
        searchData: action.response,
        error: null,
      });

    case SurveyConstants.GET_SEARCH_DATA_ERROR:
      return Object.assign({}, state, {
        searchData: {},
        error: action.error,
      });

    case SurveyConstants.GET_SURVEY_SUCCESS:
      let surveyRet = {
        error: null,
      };
      surveyRet[`${action.response.type}SurveyData`] = action.response.data;
      return Object.assign({}, state, surveyRet);

    case SurveyConstants.GET_SURVEY_ERROR:
      return Object.assign({}, state, {
        surveyResult: {},
        error: action.error,
      });

    default:
      return state;
  }
}

export default SurveyReducer;
