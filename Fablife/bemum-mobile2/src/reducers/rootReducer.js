import {combineReducers} from 'redux';
import AccountReducer from './AccountReducer';
import SurveyReducer from './SurveyReducer';
import IngredientReducer from './IngredientReducer';
import MealPlanningReducer from './MealPlanningReducer';
import ShoppingReducer from './ShoppingReducer';
import RecipeReducer from './RecipeReducer';
import ObservanceReducer from './ObservanceReducer';

export default combineReducers({
  AccountReducer,
  SurveyReducer,
  IngredientReducer,
  MealPlanningReducer,
  ShoppingReducer,
  RecipeReducer,
  ObservanceReducer,
});
