import AsyncStorage from '@react-native-community/async-storage';
import AccountConstants from '../constants/AccountConstants';
// import Immutable from 'immutable';

const initialState = {
  accessToken: '',
  userData: {},
  patient: null,
  error: null,
  metrics: null,
};

function AccountReducer(state = initialState, action) {
  switch (action.type) {
    case AccountConstants.LOGIN_SUCCESS:
      return Object.assign({}, state, {
        userData: action.response,
        accessToken: action.response.accessToken,
        error: null,
      });

    case AccountConstants.LOGIN_ERROR:
      return Object.assign({}, state, {
        userData: {},
        error: action.error,
      });

    case AccountConstants.GET_SUCCESS:
    case AccountConstants.UPDATE_SUCCESS:
      AsyncStorage.setItem('PATIENT', JSON.stringify(action.response));
      return Object.assign({}, state, {
        patient: action.response,
        error: null,
      });

    case AccountConstants.GET_ERROR:
    case AccountConstants.UPDATE_ERROR:
      return Object.assign({}, state, {
        patient: null,
        error: action.error,
      });

    case AccountConstants.GET_METRICS_SUCCESS:
      return Object.assign({}, state, {
        metrics: action.response,
        error: null,
      });

    case AccountConstants.GET_METRICS_ERROR:
      return Object.assign({}, state, {
        metrics: [],
        error: action.error,
      });

    case AccountConstants.GET_NUTRINOMES_SUCCESS:
      return Object.assign({}, state, {
        nutrinomes: action.response,
        error: null,
      });

    case AccountConstants.GET_NUTRINOMES_ERROR:
      return Object.assign({}, state, {
        nutrinomes: [],
        error: action.error,
      });

    default:
      return state;
  }
}

export default AccountReducer;
