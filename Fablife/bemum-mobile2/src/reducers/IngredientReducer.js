import {IngredientConstants} from '../constants';

const initialState = {
  ingredients: [],
  error: null,
};

function IngredientReducer(state = initialState, action) {
  switch (action.type) {
    case IngredientConstants.GET_INGREDIENTS_SUCCESS:
      return Object.assign({}, state, {
        ingredients: action.response,
        error: null,
      });

    case IngredientConstants.GET_INGREDIENTS_ERROR:
      return Object.assign({}, state, {
        ingredients: [],
        error: action.error,
      });

    default:
      return state;
  }
}

export default IngredientReducer;
