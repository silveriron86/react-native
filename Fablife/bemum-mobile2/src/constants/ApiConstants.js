import { ENVIRONMENT } from '@env';

let _inv = {
  DEV: {
    url: "https://api-preview.bemum.co/",
    host: "api-preview.bemum.co",
    sentry:
      "https://59d7cff074ad47d9b682e2ee6e993eca@o333392.ingest.sentry.io/5449136",
  },
  PRODUCTION: {
    url: "https://api.bemum.co/",
    host: "api.bemum.co",
    sentry:
      "https://59d7cff074ad47d9b682e2ee6e993eca@o333392.ingest.sentry.io/5449136",
  },
  ENVIRONMENT: ENVIRONMENT,
};

module.exports = {
  getApiPath: () => {
    return _inv[_inv.ENVIRONMENT].url;
  },

  getApiHost: () => {
    return _inv[_inv.ENVIRONMENT].host;
  },

  getSentryDNS: () => {
    return _inv[_inv.ENVIRONMENT].sentry;
  },
};
