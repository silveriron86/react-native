module.exports = [
  {
    question_id: 'MORALE_LOW',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous ressenti une baisse de moral ces derniers mois ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MORALE_FATIGUE',
    required: true,
    ui: 'switch',
    label_question: 'Vous sentez-vous moralement fatiguée ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MORALE_FATIGUE_ON_WAKING',
    required: true,
    ui: 'switch',
    label_question: 'Êtes-vous fatiguée moralement dès le réveil ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'MORALE_FATIGUE',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DEPRESSED',
    required: true,
    ui: 'switch',
    label_question: 'Vous sentez-vous déprimée ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MORALE_SUFFERING',
    required: true,
    ui: 'switch',
    label_question: 'Ressentez-vous une souffrance morale ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'SLOWED_DOWN_LIFE_RYTHM',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous l\'impression de fonctionner "au ralenti" ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'LACK_MOTIVATION_PROJECTS',
    required: true,
    ui: 'switch',
    label_question:
      'Globalement, êtes-vous moins motivée pour créer ou gérer des projets ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'LACK_SELF_CONFIDENCE',
    required: true,
    ui: 'switch',
    label_question:
      'Doutez-vous du fait que vous puissiez atteindre vos objectifs ou avez-vous tendance à vous dévaloriser ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'LACK_INTEREST_HOBBIES_LIBIDO',
    required: true,
    ui: 'switch',
    label_question:
      "Ressentez-vous moins d'intérêt pour vos occupations, vos loisirs ou votre libido ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'IRRITABLE_AGRESSIVE',
    required: true,
    ui: 'switch',
    label_question: 'Vous sentez-vous irritable ou agressive ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'CHANGEABLE_MOOD',
    required: true,
    ui: 'switch',
    label_question: "Êtes-vous d'humeur changeante ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
];
