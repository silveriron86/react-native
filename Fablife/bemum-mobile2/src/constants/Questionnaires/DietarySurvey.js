module.exports = [
  {
    question_id: 'INTRO_FOOD_SURVEY_1',
    required: false,
    label_question:
      "Pour mieux orienter votre coaching, nous avons besoin d'en savoir un peu plus sur votre alimentation actuelle. Merci de répondre aux questions suivantes.",
    ui: '',
  },
  {
    question_id: 'INTRO_FOOD_FREQUENCY_1',
    required: false,
    label_question: 'Commençons par évaluer votre diversité alimentaire.',
    ui: '',
  },
  {
    question_id: 'INTRO_FOOD_FREQUENCY_SPECIFIC_DIET_1',
    required: false,
    label_question:
      "Une alimentation variée est souhaitable et l'élimination d'un aliment doit être prise en compte peu importe la raison pour laquelle il est écarté. C'est pour cette raison que même si vous suivez un régime alimentaire spécifique nous vous demandons d'indiquer votre fréquence de consommation pour tous les types d'aliments.",
    ui: '',
    conditions: [
      {
        parentQuestionId: 'SPECIFIC_DIET',
        operator: '$neq',
        value: 'no',
      },
    ],
  },
  {
    question_id: 'FREQUENCY_DAIRY_PRODUCTS_1',
    required: true,
    label_question: 'Consommez-vous du lait ou des produits laitiers ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'Once-or-more-per-week',
          scoring: 1,
        },
        {
          label: 'Une fois par jour',
          value: 'Once-per-day',
          scoring: 2,
        },
        {
          label: 'Deux fois par jour',
          value: 'twice-per-day',
          scoring: 3,
        },
        {
          label: 'Plus de deux fois par jour',
          value: 'more-than-twice-per-day',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_CHEESE_1',
    required: true,
    label_question: 'Dont du fromage : ',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: "Moins d'une fois par jour",
          value: 'less-than-once-per-day',
          scoring: 0,
        },
        {
          label: 'Une fois par jour',
          value: 'once-per-day',
          scoring: 2,
        },
        {
          label: 'Deux fois par jour et plus',
          value: 'twice-or-more-per-day',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_MEAT_1',
    required: true,
    label_question: 'Consommez-vous de la viande rouge ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 2,
        },
        {
          label: 'Une à deux fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 2,
        },
        {
          label: 'Plus de deux fois par semaine',
          value: 'more-than-twice-per-week',
          scoring: 0,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_POULTRY_1',
    required: true,
    label_question: 'Consommez-vous de la viande rouge ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 1,
        },
        {
          label: 'Une à deux fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 2,
        },
        {
          label: 'Plus de deux fois par semaine',
          value: 'more-than-twice-per-week',
          scoring: 2,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 1,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_OFFAL_1',
    required: true,
    label_question: 'Consommez-vous de la viande rouge ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 1,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 1,
        },
        {
          label: 'Une à deux fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 0.5,
        },
        {
          label: 'Plus de deux fois par semaine',
          value: 'more-than-twice-per-week',
          scoring: 0,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_COLD_MEATS_1',
    required: true,
    label_question: 'Consommez-vous de la viande rouge ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 1,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 1,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 1,
        },
        {
          label: 'Une à deux fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 0,
        },
        {
          label: 'Plus de deux fois par semaine',
          value: 'more-than-twice-per-week',
          scoring: 0,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_FISH_1',
    required: true,
    label_question: 'Consommez-vous de la viande rouge ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 1,
        },
        {
          label: 'Une à deux fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 2,
        },
        {
          label: 'Plus de deux fois par semaine',
          value: 'more-than-twice-per-week',
          scoring: 2,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 0.5,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_OILY_FISH_1',
    required: true,
    label_question: 'Dont des petits poissons gras :',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: "Moins d'une fois par semaine",
          value: 'less-than-once-per-week',
          scoring: 0,
        },
        {
          label: 'Une fois par semaine',
          value: 'once-per-week',
          scoring: 1,
        },
        {
          label: 'Deux fois par semaine',
          value: 'twice-per-week',
          scoring: 1,
        },
        {
          label: 'Plus de deux fois par semaine',
          value: 'more-than-twice-a-week',
          scoring: 1,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_SEA_PRODUCTS_1',
    required: true,
    label_question: 'Consommez-vous des fruits de mer ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 1,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 1,
        },
        {
          label: 'Une à deux fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 1,
        },
        {
          label: 'Plus de deux fois par semaine',
          value: 'more-than-twice-per-week',
          scoring: 1,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_EGGS_1',
    required: true,
    label_question: 'Consommez-vous des oeufs ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 0.5,
        },
        {
          label: 'Une à deux fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 1,
        },
        {
          label: 'Plus de deux fois par semaine',
          value: 'more-than-twice-per-week',
          scoring: 1,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 0.5,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_CEREAL_PRODUCTS_1',
    required: true,
    label_question: 'Consommez-vous des céréales ou produits céréaliers ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 1,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 3,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: 4,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_LEGUMES_1',
    required: true,
    label_question: 'Consommez-vous des légumes secs  ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 1,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 5,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 4,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: 2,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_RAW_VEGETABLES_1',
    required: true,
    label_question: 'Consommez-vous des légumes crus, des crudités ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 1,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 4,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: 7,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_COOKED_VEGETABLES_1',
    required: true,
    label_question: 'Consommez-vous des légumes cuits ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 1,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 4,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: 3,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_FRESH_FRUITS_1',
    required: true,
    label_question: 'Consommez-vous des fruits frais ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 1,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 4,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: 2,
        },
      ],
    },
  },
  {
    question_id: 'INTRO_B9_1',
    required: false,
    label_question:
      "La vitamine B9 jouant un rôle important durant la période périconceptionnelle, il est important pour nous d'en savoir un peu plus sur vos apports en cette vitamine.",
    ui: '',
  },
  {
    question_id: 'FREQUENCY_VEGETABLES_B9_1',
    required: true,
    label_question:
      'Consommez-vous ces légumes, riches en B9 : épinards, salades vertes, brocolis, choux, asperges, betteraves ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-twice-per-week',
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_FRESH_FRUITS_B9_1',
    required: true,
    label_question:
      'Consommez-vous ces fruits riches en B9 : mangue, fraises ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-twice-per-week',
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_HERBS_1',
    required: true,
    label_question: 'Consommez-vous des fruits frais ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 1,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 1,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 1,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: 1,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_OLEAGINOUS_1',
    required: true,
    label_question: 'Consommez-vous des graines ou fruits secs oléagineux ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 0,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 0,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 1,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-twice-per-week',
          scoring: 2,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 4,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: 2,
        },
      ],
    },
  },
  {
    question_id: 'INTRO_FOOD_QUALITY_1',
    label_question:
      "Évaluons maintenant l'aspect qualitatif de votre alimentation.",
    ui: '',
  },
  {
    question_id: 'COOKING_WITH_FAT_1',
    required: false,
    label_question:
      'Si vous utilisez régulièrement une ou plusieurs matière(s) grasse(s) pour la cuisson, quelles sont-elles ?',
    ui: '',
  },
  {
    question_id: 'COOKING_WITH_FAT_OLIVE_1',
    required: true,
    ui: 'checkbox',
    label_question: "Huile d'olive",
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_WITH_FAT_CANOLA_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Huile de colza',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: -2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_WITH_FAT_SUNFLOWER_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Huile de tournesol',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_WITH_FAT_COCO_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Huile de noix de coco',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0.5,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_WITH_FAT_BUTTER_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Beurre',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: -2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_WITH_FAT_MARGARINE_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Margarine',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: -2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_WITH_FAT_CREAM_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Crème fraîche',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: -2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_WITH_FAT_ELSE_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Autre(s)',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: -2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_WITH_FAT_NONE_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Aucune',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },

  {
    question_id: 'SEASONING_WITH_FAT_1',
    required: false,
    label_question:
      "Si vous utilisez régulièrement une ou plusieurs matière(s) grasse(s) pour l'assaisonnement, quelles sont-elles ?",
    ui: '',
  },
  {
    question_id: 'SEASONING_WITH_FAT_OLIVE_1',
    required: true,
    ui: 'checkbox',
    label_question: "Huile d'olive",
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'SEASONING_WITH_FAT_CANOLA_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Huile de colza',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'SEASONING_WITH_FAT_SUNFLOWER_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Huile de tournesol',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0.5,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'SEASONING_WITH_FAT_BUTTER_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Beurre',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0.5,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'SEASONING_WITH_FAT_MARGARINE_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Margarine',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'SEASONING_WITH_FAT_CREAM_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Crème fraîche',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'SEASONING_WITH_FAT_ELSE_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Autres (huiles végétales)',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'SEASONING_WITH_FAT_NONE_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Aucune',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_METHOD_1',
    required: false,
    label_question: 'Quelle(s) méthode(s) de cuisson privilégiez-vous ?',
    ui: '',
  },
  {
    question_id: 'COOKING_METHOD_STEAM_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Vapeur',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_METHOD_BOILING_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Eau bouillante',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_METHOD_MICROWAVE_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Micro-ondes',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: -2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_METHOD_HOVEN_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Four',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_METHOD_PAN_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Cuisson à la casserole (sans eau)',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_METHOD_SAUTE_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Cuisson à la poêle',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: 0,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_METHOD_FRYING_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Friture',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: -2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'COOKING_METHOD_ELSE_1',
    required: true,
    ui: 'checkbox',
    label_question: 'Autre(s)',
    ui_config: {
      choices: [
        {
          value: true,
          scoring: -2,
        },
        {
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_STILL_WATER_1',
    required: true,
    label_question:
      "Quelle quantité d'eau plate (minérale, de source ou du robinet, infusion) buvez-vous par jour ?",
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Moins de 1,5L',
          value: 'less-1500',
          scoring: 0,
        },
        {
          label: 'Entre 1,5 et 2,5L',
          value: '1500-to-2500',
          scoring: 3,
        },
        {
          label: 'Plus de 2,5L',
          value: 'more-2500',
          scoring: 1,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_SPARKLING_WATER_1',
    required: true,
    label_question: 'Consommez-vous des eaux gazeuses ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 1,
        },
        {
          label: 'De temps en temps',
          value: 'sometimes',
          scoring: 1,
        },
        {
          label: "Moins d'1L par jour",
          value: 'less-1000',
          scoring: 0,
        },
        {
          label: "Plus d'1L par jour",
          value: 'more-1000',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_SODAS_1',
    required: true,
    label_question:
      'Buvez-vous des jus de fruits, sodas, boissons énergisantes et autres boissons sucrées ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 3,
        },
        {
          label: 'De temps en temps',
          value: 'sometimes',
          scoring: 0.5,
        },
        {
          label: 'Un à deux verres par jour',
          value: 'one-or-two-glasses-per-day',
          scoring: 0,
        },
        {
          label: 'Plus de deux verres par jour',
          value: 'more-than-two-glasses-per-day',
          scoring: -1,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_COFFEE_1',
    required: true,
    label_question: 'Combien de thé ou de café buvez vous par jour ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Aucun',
          value: 'none',
          scoring: 1,
        },
        {
          label: 'Moins de trois',
          value: 'less-than-three',
          scoring: 1,
        },
        {
          label: 'Trois ou plus',
          value: 'three-or-more',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'SUGARY_HOT_DRINKS_1',
    required: true,
    label_question:
      'Combien de boissons chaudes sucrées buvez-vous par jour ? ',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Aucun',
          value: 'none',
          scoring: 2,
        },
        {
          label: 'Moins de trois',
          value: 'less-than-three',
          scoring: 0,
        },
        {
          label: 'Trois ou plus',
          value: 'three-or-more',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_SUGARY_FOODS_1',
    required: true,
    label_question: 'Consommez-vous des produits sucrés ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 3,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 3,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 2,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-more-per-week',
          scoring: 0,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: -1,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: -1,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_FAST_FOOD_1',
    required: true,
    label_question:
      'À quelle fréquence consommez-vous du fast-food et des produits frits ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 3,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 3,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 2,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-more-per-week',
          scoring: 0,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: -1,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: -1,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_RESTAURANT_1',
    required: true,
    label_question:
      'À quelle fréquence êtes-vous amené.e à manger au restaurant ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 2,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 2,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 2,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-more-per-week',
          scoring: 1,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 0,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FREQUENCY_DELIVERY_FOOD_1',
    required: true,
    label_question: 'À quelle fréquence vous faites-vous livrer des plats ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Jamais',
          value: 'never',
          scoring: 2,
        },
        {
          label: 'De manière occasionnelle',
          value: 'occasionally',
          scoring: 2,
        },
        {
          label: 'Une à plusieurs fois par mois',
          value: 'once-or-more-per-month',
          scoring: 2,
        },
        {
          label: 'Une à plusieurs fois par semaine',
          value: 'once-or-more-per-week',
          scoring: 1,
        },
        {
          label: 'Chaque jour',
          value: 'each-day',
          scoring: 0,
        },
        {
          label: 'À chaque repas',
          value: 'each-meal',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'SALTING_MEALS_1',
    required: true,
    ui: 'switch',
    label_question:
      "Avez-vous l'habitude de saler votre plat à la cuisson puis dans votre assiette?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 0,
        },
        {
          label: 'Non',
          value: false,
          scoring: 3,
        },
      ],
    },
  },
  {
    question_id: 'CEREAL_PRODUCTS_WHOLE_GRAIN_1',
    required: true,
    ui: 'switch',
    label_question: 'Privilégiez-vous les produits complets ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 3,
        },
        {
          label: 'Non',
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'ORGANIC_FOOD_1',
    required: true,
    ui: 'switch',
    label_question:
      "Privilégiez-vous les produits issus de l'agriculture biologique ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 1,
        },
        {
          label: 'Non',
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'ORGANIC_FOOD_1',
    required: true,
    ui: 'switch',
    label_question:
      "Privilégiez-vous les produits issus de l'agriculture biologique ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 1,
        },
        {
          label: 'Non',
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'HOME_MADE_FOOD_1',
    required: true,
    ui: 'switch',
    label_question: 'Privilégiez-vous le fait maison ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 2,
        },
        {
          label: 'Non',
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'INTRO_FOOD_BEHAVIOR_1',
    required: false,
    label_question:
      "Pour finir, dites-nous en plus sur vos habitudes de vies liées à l'alimentation.",
    ui: '',
  },
  {
    question_id: 'ALCOHOL_QUANTITY_1',
    required: true,
    label_question: 'Combien de doses d’alcool consommez-vous par semaine ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Aucune',
          value: 'none',
          scoring: 4,
        },
        {
          label: 'Une à quatre',
          value: '1-to-4',
          scoring: 3,
        },
        {
          label: 'Cinq à dix',
          value: '5-to-10',
        },
        {
          label: 'Plus de dix',
          value: 'more-than-10',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'ALCOHOL_DISTRIBUTION_1',
    required: false,
    label_question:
      "Si vous consommez de l'alcool, comment en répartissez-vous votre consommation ?",
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Non concerné(e)',
          value: 'not-relevant',
        },
        {
          label: 'Plutôt chaque jour',
          value: 'each-day',
        },
        {
          label: 'Plutôt sur un ou deux jours',
          value: 'over-1-or-2-days',
        },
      ],
    },
  },
  {
    question_id: 'ALCOHOL_COCKTAIL_1',
    required: false,
    ui: 'switch',
    label_question: 'Consommez-vous des cocktails ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'DIFFERENCE_HUNGER_CRAVING_1',
    required: true,
    ui: 'switch',
    label_question: 'Faites-vous la différence entre faim et envie de manger ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 1,
        },
        {
          label: 'Non',
          value: false,
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'FOOD_CRAVING_1',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous souvent envie ou besoin de manger au cours de la journée sans sensation de faim ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'FOOD_CRAVING_AND_SNACKING_1',
    required: false,
    ui: 'switch',
    label_question:
      'Si oui, vous arrive-t-il régulièrement de répondre à cette envie et de grignoter sans faim ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 0,
        },
        {
          label: 'Non',
          value: false,
          scoring: 3,
        },
        {
          label: 'Non concerné(e)',
          value: 'not-relevant',
          scoring: 3,
        },
      ],
    },
  },
  {
    question_id: 'KEEPS_EATING_WITHOUT_HUNGER_1',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous tendance à continuer votre repas alors que vous n’avez plus faim ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 0,
        },
        {
          label: 'Non',
          value: false,
          scoring: 3,
        },
      ],
    },
  },
  {
    question_id: 'SECOND_HELPING_1',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous tendance à vous resservir ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 0,
        },
        {
          label: 'Non',
          value: false,
          scoring: 2,
        },
      ],
    },
  },
  {
    question_id: 'EAT_SAME_QUANTITY_AS_PARTNER_1',
    required: true,
    ui: 'switch',
    label_question:
      'Pour les femmes en couple avec un homme, mangez-vous la même quantité que votre compagnon ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: -1,
        },
        {
          label: 'Non',
          value: false,
          scoring: 0,
        },
        {
          label: 'Non concerné(e)',
          value: 'not-relevant',
          scoring: 0,
        },
      ],
    },
  },
  {
    question_id: 'EATING_LATE_1',
    required: true,
    ui: 'switch',
    label_question:
      'Lorsque vous avez le choix, mangez-vous régulièrement après 21h  ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 0,
        },
        {
          label: 'Non',
          value: false,
          scoring: 2,
        },
      ],
    },
  },
  {
    question_id: 'EATING_FAST_1',
    required: true,
    ui: 'switch',
    label_question: 'Mangez-vous rapidement?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 0,
        },
        {
          label: 'Non',
          value: false,
          scoring: 2,
        },
      ],
    },
  },
  {
    question_id: 'EATING_SCREEN_1',
    required: true,
    ui: 'switch',
    label_question: "Avez-vous l'habitude de manger devant un écran ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
          scoring: 0,
        },
        {
          label: 'Non',
          value: false,
          scoring: 3,
        },
      ],
    },
  },
];
