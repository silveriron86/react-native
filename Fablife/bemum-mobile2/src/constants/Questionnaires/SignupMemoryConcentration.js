module.exports = [
  {
    question_id: 'MEMORY_LESS_EFFICIENT',
    required: true,
    ui: 'switch',
    label_question:
      'En ce moment, estimez-vous que votre mémoire soit moins performante ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MEMORISING_DIFFICULTIES',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous davantage de difficulté à mémoriser ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'MEMORY_LESS_EFFICIENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MEMORISING_DIFFICULTIES_EVENTS',
    required: true,
    ui: 'radio',
    label_question:
      'Éprouvez-vous des difficultés à vous souvenir des événements :',
    ui_config: {
      choices: [
        {
          label: 'récents',
          value: 'recent',
        },
        {
          label: 'anciens',
          value: 'old',
        },
        {
          label: 'les deux',
          value: 'all',
        },
        {
          label: 'aucun',
          value: 'NULL',
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'MEMORY_LESS_EFFICIENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MEMORY_DATE',
    required: true,
    ui: 'switch',
    label_question:
      "Éprouvez-vous des difficultés à vous souvenir de la date d'aujourd'hui ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'MEMORY_LESS_EFFICIENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MEMORY_SHORT_TERM_DIFFICULTIES',
    required: true,
    ui: 'switch',
    label_question:
      'Éprouvez-vous des difficultés à assimilier les informations à très court terme ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'MEMORY_LESS_EFFICIENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECISION_DIFFICULTIES',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous du mal à prendre des décisions ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'CONCENTRATION_DIFFICULTIES',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous du mal à vous concentrer ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'INTELLECTUAL_PERFORMANCE_DECREASED',
    required: true,
    ui: 'switch',
    label_question:
      'Estimez-vous que vos performances intellectuelles soient amoindries ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
];
