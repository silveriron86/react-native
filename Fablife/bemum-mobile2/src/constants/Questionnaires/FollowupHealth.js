module.exports = [
  {
    question_id: 'BLOOD_TEST',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous un bilan sanguin récent dont vous pourriez reporter les résultats ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    frequency: 28,
  },
  {
    question_id: 'BLOOD_TEST_INTRO',
    required: false,
    ui: '',
    label_question:
      "Merci de rentrer avec soin la valeur correspondant à l'unité de référence.",
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'BLOOD_TEST_CHOL',
    required: true,
    ui: 'stepper',
    label_question: 'Cholestérol, total',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'BLOOD_TEST_HDL_CHOL',
    required: true,
    ui: 'stepper',
    label_question: 'Cholestérol, HDL',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'BLOOD_TEST_LDL_CHOL',
    required: true,
    ui: 'stepper',
    label_question: 'Cholestérol, LDL',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'BLOOD_TEST_TRIGLYCERIDES',
    required: true,
    ui: 'stepper',
    label_question: 'Triglycérides',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'BLOOD_TEST_GLUCOSE',
    required: true,
    ui: 'stepper',
    label_question: 'Glycémie à jeun',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'BLOOD_TEST_CORTISOL',
    required: true,
    ui: 'stepper',
    label_question: 'Cortisol',
    ui_config: {
      min: 0,
      max: 25,
      precision: 0.01,
    },
    unit: {
      short_name: 'nmol/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT',
    required: true,
    ui: 'switch',
    label_question: 'Prenez-vous un traitement médicamenteux actuellement ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_TYPE',
    required: false,
    ui: '',
    label_question: 'Fait-il partie des traitements suivants ?',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_ANXIOLYTICS',
    required: false,
    ui: 'checkbox',
    label_question: 'Anxiolytiques',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_ANTIDEPRESSANTS',
    required: false,
    ui: 'checkbox',
    label_question: 'Antidépresseurs',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_NEUROLEPTICS',
    required: false,
    ui: 'checkbox',
    label_question: 'Neuroleptiques',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_CORTICOSTEROIDS',
    required: false,
    ui: 'checkbox',
    label_question: 'Corticoïdes',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_SLEEPING_PILL',
    required: false,
    ui: 'checkbox',
    label_question: 'Somnifères',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_BETA_BLOCKERS',
    required: false,
    ui: 'checkbox',
    label_question: 'Beta-bloquants',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_LAXATIVES',
    required: false,
    ui: 'checkbox',
    label_question: 'Laxatifs',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_PPI',
    required: false,
    ui: 'checkbox',
    label_question: 'Antiacides (inhibiteurs de la pompe à protons)',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'MED_TREATMENT_OTHER',
    require: true,
    ui: 'text',
    label_question:
      'Si vous prenez un autre traitement médicamenteux, veuillez nous préciser lequel.',
    ui_config: {
      max: 150,
    },
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'DIGESTIVES_ISSUES',
    required: true,
    ui: 'switch',
    label_question: "Avez-vous des signes d'inconfort digestif ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    frequency: 28,
  },
  {
    question_id: 'DIGESTIVES_ISSUES_TYPE',
    required: false,
    ui: '',
    label_question: 'Pouvez-vous nous indiquer lesquels ?',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'DIGESTIVES_ISSUES_DIARRHEA_CONSTIPATION',
    required: true,
    ui: 'checkbox',
    label_question:
      'Troubles du transit (diarrhées, constipation, alternance des deux)',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'DIGESTIVES_ISSUES_BLOATING',
    required: true,
    ui: 'checkbox',
    label_question: 'Ballonnements',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'DIGESTIVES_ISSUES_PAIN_SPASMS',
    required: true,
    ui: 'checkbox',
    label_question: 'Douleurs ou spasmes intestinaux',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'DIGESTIVES_ISSUES_HEARTBURN_GASTRIC_REFLUX',
    required: true,
    ui: 'checkbox',
    label_question: "Brûlures d'estomac et/ou reflux acides",
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'DIGESTIVES_ISSUES_ELSE',
    required: true,
    ui: 'checkbox',
    label_question: 'Autres',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: 'DIGESTIVE_ISSUE_DIET',
    required: true,
    ui: 'switch',
    label_question:
      'Pensez-vous que votre alimentation puisse justifier ces inconforts digestifs ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: false,
      },
    ],
    conditions_assoc: 'AND',
    frequency: 28,
  },
  {
    question_id: 'DIGESTIVE_ISSUE_MED_TREATMENT_OR_DIET',
    required: true,
    ui: 'switch',
    label_question:
      'Pensez-vous que votre traitement médicamenteux ou votre alimentation puisse justifier ces inconforts digestifs ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    conditions_assoc: 'AND',
    frequency: 28,
  },
];
