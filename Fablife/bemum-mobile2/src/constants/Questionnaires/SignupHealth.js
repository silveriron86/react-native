module.exports = [
  {
    question_id: 'BLOOD_TEST',
    required: true,
    ui: 'switch',
    label_question:
      'Avez-vous un bilan sanguin de moins de 6 mois dont vous pourriez reporter les résultats ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'BLOOD_TEST_INTRO',
    required: false,
    ui: '',
    label_question:
      "Merci de rentrer avec soin la valeur correspondant à l'unité de référence.",
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_TEST_CHOL',
    required: true,
    ui: 'stepper',
    label_question: 'Cholestérol, total',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_TEST_HDL_CHOL',
    required: true,
    ui: 'stepper',
    label_question: 'Cholestérol, HDL',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_TEST_LDL_CHOL',
    required: true,
    ui: 'stepper',
    label_question: 'Cholestérol, LDL',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_TEST_TRIGLYCERIDES',
    required: true,
    ui: 'stepper',
    label_question: 'Triglycérides',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_TEST_GLUCOSE',
    required: true,
    ui: 'stepper',
    label_question: 'Glycémie à jeun',
    ui_config: {
      min: 0,
      max: 10,
      precision: 0.01,
    },
    unit: {
      short_name: 'g/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'BLOOD_TEST_CORTISOL',
    required: true,
    ui: 'stepper',
    label_question: 'Cortisol',
    ui_config: {
      min: 0,
      max: 25,
      precision: 0.01,
    },
    unit: {
      short_name: 'nmol/L',
    },
    conditions: [
      {
        parentQuestionId: 'BLOOD_TEST',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_HISTORY_DIAGNOSED_DISEASE',
    required: true,
    ui: 'switch',
    label_question:
      "Souffrez-vous d'une maladie diagnostiquée par votre médecin ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MED_HISTORY_DIAGNOSED_DISEASE_TYPE',
    required: false,
    ui: '',
    label_question: 'Fait-elle partie des maladies suivantes ?',
    conditions: [
      {
        parentQuestionId: 'MED_HISTORY_DIAGNOSED_DISEASE',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_HISTORY_CARDIOVASCULAR',
    required: false,
    ui: 'checkbox',
    label_question: 'Maladie cardiovasculaire',
    conditions: [
      {
        parentQuestionId: 'MED_HISTORY_DIAGNOSED_DISEASE',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_HISTORY_HIGH_BLOOD_PRESSURE',
    required: false,
    ui: 'checkbox',
    label_question: 'Hypertension artérielle',
    conditions: [
      {
        parentQuestionId: 'MED_HISTORY_DIAGNOSED_DISEASE',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_HISTORY_CHRONIC_INFLAMMATORY_DISEASE',
    required: false,
    ui: 'checkbox',
    label_question: 'Maladie inflammatoire chronique',
    conditions: [
      {
        parentQuestionId: 'MED_HISTORY_DIAGNOSED_DISEASE',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_HISTORY_HYPOTHYROIDISM',
    required: false,
    ui: 'checkbox',
    label_question: 'Hypothyroïdie',
    conditions: [
      {
        parentQuestionId: 'MED_HISTORY_DIAGNOSED_DISEASE',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_HISTORY_HYPERTHYROIDISM',
    required: false,
    ui: 'checkbox',
    label_question: 'Hyperthyroïdie',
    conditions: [
      {
        parentQuestionId: 'MED_HISTORY_DIAGNOSED_DISEASE',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_HISTORY_DIAGNOSED_DISEASE_LABEL',
    require: true,
    ui: 'text',
    label_question:
      "Si vous souffrez d'une autre maladie, veuillez nous préciser laquelle.",
    ui_config: {
      max: 150,
    },
    conditions: [
      {
        parentQuestionId: 'MED_HISTORY_DIAGNOSED_DISEASE',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_FAMILY_HISTORY_DISEASE',
    required: false,
    ui: '',
    label_question:
      'Y a-t-il des antécédents des dérèglements et pathologies suivantes dans votre famille ?',
    // conditions: [
    //   {
    //     parentQuestionId: 'MED_HISTORY_DIAGNOSED_DISEASE',
    //     operator: '$eq',
    //     value: true,
    //   },
    // ],
  },
  {
    question_id: 'MED_FAMILY_HISTORY_OVERWEIGHT',
    required: false,
    ui: 'checkbox',
    label_question: 'Surpoids ou obésité',
  },
  {
    question_id: 'MED_FAMILY_HISTORY_DIABETES',
    required: false,
    ui: 'checkbox',
    label_question: 'Diabète de type 2',
  },
  {
    question_id: 'MED_FAMILY_HISTORY_HYPERCHOLESTEROLEMIA',
    required: false,
    ui: 'checkbox',
    label_question: 'Cholestérol élevé',
  },
  {
    question_id: 'MED_FAMILY_HISTORY_HYPERTRIGLYCERIDEMIA',
    required: false,
    ui: 'checkbox',
    label_question: 'Triglycérides élevés',
  },
  {
    question_id: 'MED_FAMILY_HISTORY_CARDIOVASCULAR',
    required: false,
    ui: 'checkbox',
    label_question: 'Maladies cardiovasculaires',
  },
  {
    question_id: 'MED_TREATMENT',
    required: true,
    ui: 'switch',
    label_question: 'Prenez-vous un traitement médicamenteux ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'MED_TREATMENT_TYPE',
    required: false,
    ui: '',
    label_question: 'Fait-il partie des traitements suivants ?',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_ANXIOLYTICS',
    required: false,
    ui: 'checkbox',
    label_question: 'Anxiolytiques',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_ANTIDEPRESSANTS',
    required: false,
    ui: 'checkbox',
    label_question: 'Antidépresseurs',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_NEUROLEPTICS',
    required: false,
    ui: 'checkbox',
    label_question: 'Neuroleptiques',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_CORTICOSTEROIDS',
    required: false,
    ui: 'checkbox',
    label_question: 'Corticoïdes',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_SLEEPING_PILL',
    required: false,
    ui: 'checkbox',
    label_question: 'Somnifères',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_BETA_BLOCKERS',
    required: false,
    ui: 'checkbox',
    label_question: 'Beta-bloquants',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_LAXATIVES',
    required: false,
    ui: 'checkbox',
    label_question: 'Laxatifs',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_PPI',
    required: false,
    ui: 'checkbox',
    label_question: 'Antiacides (inhibiteurs de la pompe à protons)',
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'MED_TREATMENT_OTHER',
    require: true,
    ui: 'text',
    label_question:
      'Si vous prenez un autre traitement médicamenteux, veuillez nous préciser lequel.',
    ui_config: {
      max: 150,
    },
    conditions: [
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY',
    required: true,
    ui: 'switch',
    label_question:
      "Souffrez-vous d'une carence alimentaire diagnostiquée par votre médecin ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_LIST',
    required: false,
    ui: '',
    label_question: 'De quel(s) nutriment(s) êtes-vous carencée ?',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_VITAMINE_B2',
    required: false,
    ui: 'checkbox',
    label_question: 'Vitamine B2',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_VITAMINE_B3',
    required: false,
    ui: 'checkbox',
    label_question: 'Vitamine B3',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_VITAMINE_B9',
    required: false,
    ui: 'checkbox',
    label_question: 'Vitamine B9',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_VITAMINE_B12',
    required: false,
    ui: 'checkbox',
    label_question: 'Vitamine B12',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_VITAMINE_D',
    required: false,
    ui: 'checkbox',
    label_question: 'Vitamine D',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_CALCIUM',
    required: false,
    ui: 'checkbox',
    label_question: 'Calcium',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_IRON',
    required: false,
    ui: 'checkbox',
    label_question: 'Fer',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_MAGNESIUM',
    required: false,
    ui: 'checkbox',
    label_question: 'Magnésium',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_ZINC',
    required: false,
    ui: 'checkbox',
    label_question: 'Zinc',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DECLARATIVE_DEFICIENCY_OTHER',
    required: false,
    ui: 'checkbox',
    label_question: 'Autre',
    conditions: [
      {
        parentQuestionId: 'DECLARATIVE_DEFICIENCY',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DIGESTIVES_ISSUES',
    required: true,
    ui: 'switch',
    label_question: "Avez-vous des signes d'inconfort digestif ?",
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
  {
    question_id: 'DIGESTIVES_ISSUES_TYPE',
    required: false,
    ui: '',
    label_question: 'Pouvez-vous nous indiquer lesquels ?',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DIGESTIVES_ISSUES_DIARRHEA_CONSTIPATION',
    required: true,
    ui: 'checkbox',
    label_question:
      'Troubles du transit (diarrhées, constipation, alternance des deux)',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DIGESTIVES_ISSUES_BLOATING',
    required: true,
    ui: 'checkbox',
    label_question: 'Ballonnements',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DIGESTIVES_ISSUES_PAIN_SPASMS',
    required: true,
    ui: 'checkbox',
    label_question: 'Douleurs ou spasmes intestinaux',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DIGESTIVES_ISSUES_HEARTBURN_GASTRIC_REFLUX',
    required: true,
    ui: 'checkbox',
    label_question: "Brûlures d'estomac et/ou reflux acides",
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DIGESTIVES_ISSUES_ELSE',
    required: true,
    ui: 'checkbox',
    label_question: 'Autres',
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
    ],
  },
  {
    question_id: 'DIGESTIVE_ISSUE_DIET',
    required: true,
    ui: 'switch',
    label_question:
      'Pensez-vous que votre alimentation puisse justifier ces inconforts digestifs ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: false,
      },
    ],
    conditions_assoc: 'AND',
  },
  {
    question_id: 'DIGESTIVE_ISSUE_MED_TREATMENT_OR_DIET',
    required: true,
    ui: 'switch',
    label_question:
      'Pensez-vous que votre traitement médicamenteux ou votre alimentation puisse justifier ces inconforts digestifs ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'DIGESTIVES_ISSUES',
        operator: '$eq',
        value: true,
      },
      {
        parentQuestionId: 'MED_TREATMENT',
        operator: '$eq',
        value: true,
      },
    ],
    conditions_assoc: 'AND',
  },
  {
    question_id: 'ABUNDANT_MENSTRUATIONS',
    required: true,
    ui: 'switch',
    label_question: 'Avez-vous des menstruations abondantes ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
  },
];
