module.exports = [
  {
    question_id: "BLOOD_HORMONES",
    required: true,
    ui: "switch",
    label_question:
      "Avez-vous un bilan hormonal récent dont vous pourriez reporter les résultats ?",
    ui_config: {
      choices: [
        {
          label: "Oui",
          value: true,
        },
        {
          label: "Non",
          value: false,
        },
      ],
    },
    frequency: 28,
  },
  {
    question_id: "BLOOD_HORMONES_INTRO",
    required: false,
    ui: "",
    label_question:
      "Merci de rentrer avec soin la valeur correspondant à l'unité de référence.",
    conditions: [
      {
        parentQuestionId: "BLOOD_HORMONES",
        operator: "$eq",
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: "BLOOD_TESTOSTERONE_TOTAL",
    required: false,
    ui: "stepper",
    label_question: "Testostérone totale",
    ui_config: {
      min: 2000,
      max: 10000,
      precision: 1,
    },
    unit: {
      short_name: "pg/mL",
    },
    conditions: [
      {
        parentQuestionId: "BLOOD_HORMONES",
        operator: "$eq",
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: "BLOOD_TESTOSTERONE_FREE",
    required: false,
    ui: "stepper",
    label_question: "Testostérone libre",
    ui_config: {
      min: 2000,
      max: 10000,
      precision: 1,
    },
    unit: {
      short_name: "pg/mL",
    },
    conditions: [
      {
        parentQuestionId: "BLOOD_HORMONES",
        operator: "$eq",
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: "BLOOD_LH",
    required: false,
    ui: "stepper",
    label_question: "Hormone lutéinisante : LH",
    ui_config: {
      min: 0,
      max: 100,
      precision: 0.01,
    },
    unit: {
      short_name: "mUI/mL",
    },
    conditions: [
      {
        parentQuestionId: "BLOOD_HORMONES",
        operator: "$eq",
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: "BLOOD_ESTRADIOL",
    required: false,
    ui: "stepper",
    label_question: "Œstradiol",
    ui_config: {
      min: 0,
      max: 10000,
      precision: 0.1,
    },
    unit: {
      short_name: "pg/mL",
    },
    conditions: [
      {
        parentQuestionId: "BLOOD_HORMONES",
        operator: "$eq",
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: "BLOOD_PROGESTERONE",
    required: false,
    ui: "stepper",
    label_question: "Progestérone",
    ui_config: {
      min: 0,
      max: 100,
      precision: 0.1,
    },
    unit: {
      short_name: "ng/mL",
    },
    conditions: [
      {
        parentQuestionId: "BLOOD_HORMONES",
        operator: "$eq",
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: "BLOOD_FSH",
    required: false,
    ui: "stepper",
    label_question: "Hormone folliculo-stimulante : FSH",
    ui_config: {
      min: 0,
      max: 20,
      precision: 0.01,
    },
    unit: {
      short_name: "mUI/mL",
    },
    conditions: [
      {
        parentQuestionId: "BLOOD_HORMONES",
        operator: "$eq",
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: "BLOOD_TSH",
    required: false,
    ui: "stepper",
    label_question: "Thyréostimuline : TSH",
    ui_config: {
      min: 0,
      max: 20,
      precision: 0.01,
    },
    unit: {
      short_name: "mUI/mL",
    },
    conditions: [
      {
        parentQuestionId: "BLOOD_HORMONES",
        operator: "$eq",
        value: true,
      },
    ],
    frequency: 28,
  },
  {
    question_id: "BLOOD_AMH",
    required: false,
    ui: "stepper",
    label_question: "Hormone anti-mullërienne : AMH",
    ui_config: {
      min: 0,
      max: 20,
      precision: 0.01,
    },
    unit: {
      short_name: "ng/mL",
    },
    conditions: [
      {
        parentQuestionId: "BLOOD_HORMONES",
        operator: "$eq",
        value: true,
      },
    ],
    frequency: 28,
  },
];
