import createMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import RecipeActions from './RecipeActions';
import {RecipeConstants} from '../constants';

const mws = [thunk];
const mockStore = createMockStore(mws);
const store = mockStore({});
const mock = new MockAdapter(axios);

describe('GET Recipe Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'recipes';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: RecipeConstants.GET_RECIPE_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: RecipeConstants.GET_RECIPE_ERROR,
    error: {},
  };

  it('Get Recipe Error', () => {
    expect(RecipeActions.getError({})).toEqual(expectedErrorActions);
  });

  it('Get Recipe Success', () => {
    expect(RecipeActions.getSuccess(response)).toEqual(expectedSuccessActions);
  });

  it('Dispatches GET_RECIPE_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(RecipeActions.get()).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches GET_RECIPE_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(RecipeActions.get()).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});

describe('POST Dislike Recipe Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'recipes';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: RecipeConstants.DISLIKE_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: RecipeConstants.DISLIKE_ERROR,
    error: {},
  };

  it('Dislike Recipe Error', () => {
    expect(RecipeActions.dislikeError({})).toEqual(expectedErrorActions);
  });

  it('Dislike Recipe Success', () => {
    expect(RecipeActions.dislikeSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches DISLIKE_SUCCESS after a successful API requests', () => {
    mock.onPost(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(RecipeActions.dislike()).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches DISLIKE_ERROR after a FAILED API requests', () => {
    mock.onPost(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(RecipeActions.dislike()).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});

describe('DELETE UnDislike Recipe Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'recipes';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: RecipeConstants.UN_DISLIKE_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: RecipeConstants.UN_DISLIKE_ERROR,
    error: {},
  };

  it('UnDislike Recipe Error', () => {
    expect(RecipeActions.unDislikeError({})).toEqual(expectedErrorActions);
  });

  it('UnDislike Recipe Success', () => {
    expect(RecipeActions.unDislikeSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches UN_DISLIKE_SUCCESS after a successful API requests', () => {
    mock.onDelete(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(RecipeActions.unDislike()).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches UN_DISLIKE_ERROR after a FAILED API requests', () => {
    mock.onDelete(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(RecipeActions.unDislike()).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});
