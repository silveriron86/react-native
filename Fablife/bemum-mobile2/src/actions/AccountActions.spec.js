// import configureMockStore from 'redux-mock-store';
import createMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import AccountActions from './AccountActions';
import AccountConstants from '../constants/AccountConstants';
import ApiConstants from '../constants/ApiConstants';
// import rootReducer from '../reducers/rootReducer';

const mws = [thunk];
const mockStore = createMockStore(mws);
const store = mockStore({});
const mock = new MockAdapter(axios);

describe('GET get patient actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const id = 1;
  const url = `${ApiConstants.getApiPath()}patients/${id}`;

  it('Dispatches GET_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});

    store.dispatch(AccountActions.get(id)).then(() => {
      expect(store.getActions()).toContainEqual({
        type: AccountConstants.GET_SUCCESS,
        response: {response: {item: 'item1'}},
      });
    });
  });

  it('dispatches GET_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(AccountActions.get(id)).then(() => {
      expect(store.getActions()).toContainEqual({
        type: AccountConstants.GET_ERROR,
        payload: {error: {message: 'error message'}},
      });
    });
  });
});

describe('GET get metrics actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const id = 1;
  const url = `${ApiConstants.getApiPath()}patients/${id}/metrics`;

  it('Dispatches GET_METRICS_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});

    store.dispatch(AccountActions.getMetrics()).then(() => {
      expect(store.getActions()).toContainEqual({
        type: AccountConstants.GET_METRICS_SUCCESS,
        response: {response: {item: 'item1'}},
      });
    });
  });

  it('dispatches GET_METRICS_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    const expectedActions = {
      type: AccountConstants.GET_METRICS_ERROR,
      payload: {error: {message: 'error message'}},
    };
    store.dispatch(AccountActions.getMetrics()).then(() => {
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });
});

describe('GET get nutrinomes actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const id = 1;
  const url = `${ApiConstants.getApiPath()}patients/${id}/nutrinomes`;

  it('Dispatches GET_NUTRINOMES_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});

    const expectedActions = {
      type: AccountConstants.GET_NUTRINOMES_SUCCESS,
      response: {response: {item: 'item1'}},
    };

    store.dispatch(AccountActions.getNutrinomes()).then(() => {
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });

  it('dispatches GET_NUTRINOMES_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    const expectedActions = {
      type: AccountConstants.GET_NUTRINOMES_ERROR,
      payload: {error: {message: 'error message'}},
    };
    store.dispatch(AccountActions.getNutrinomes()).then(() => {
      expect(store.getActions()).toContainEqual(expectedActions);
    });
  });
});
