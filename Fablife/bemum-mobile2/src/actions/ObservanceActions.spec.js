import createMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import ObservanceActions from './ObservanceActions';
import {ObservanceConstants} from '../constants';

const mws = [thunk];
const mockStore = createMockStore(mws);
const store = mockStore({});
const mock = new MockAdapter(axios);

describe('GET Observance List Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'observance';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: ObservanceConstants.GET_OBSERVANCE_LIST_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: ObservanceConstants.GET_OBSERVANCE_LIST_ERROR,
    error: {},
  };

  it('Get Observance List Error', () => {
    expect(ObservanceActions.getListError({})).toEqual(expectedErrorActions);
  });

  it('Get Observance List Success', () => {
    expect(ObservanceActions.getListSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches GET_OBSERVANCE_LIST_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(ObservanceActions.getList()).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches GET_OBSERVANCE_LIST_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(ObservanceActions.getList()).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});

describe('GET Observance Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'observance';
  const date = 'YYYY-MM-DD';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: ObservanceConstants.GET_OBSERVANCE_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: ObservanceConstants.GET_OBSERVANCE_ERROR,
    error: {},
  };

  it('Get Observance Error', () => {
    expect(ObservanceActions.getError({})).toEqual(expectedErrorActions);
  });

  it('Get Observance Success', () => {
    expect(ObservanceActions.getSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches GET_OBSERVANCE_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(ObservanceActions.get(date)).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches GET_OBSERVANCE_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(ObservanceActions.get(date)).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});
