// import axios from 'axios';
import "text-encoding";
import "web-streams-polyfill";
// import {fetch} from 'react-native-fetch-api';
import ApiConstants from "../constants/ApiConstants";
import { RecipeConstants } from "../constants";

const RecipeActions = {
  getError: function (error) {
    return {
      error,
      type: RecipeConstants.GET_RECIPE_ERROR,
    };
  },

  getSuccess: function (response) {
    return {
      response,
      type: RecipeConstants.GET_RECIPE_SUCCESS,
    };
  },

  get: function (recipeId, cb) {
    return (dispatch) => {
      return fetch(`${ApiConstants.getApiPath()}recipes/${recipeId}`, {
        method: "GET",
        headers: {
          accept: "application/json",
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then((res) => res.json())
        .then((response) => {
          if (cb) {
            cb(response);
          }
          dispatch(this.getSuccess(response));
        })
        .catch((error) => {
          if (cb) {
            cb({ success: false, error });
          }
          dispatch(this.getError(error));
        });
    };
  },

  dislikeError: function (error) {
    return {
      error,
      type: RecipeConstants.DISLIKE_ERROR,
    };
  },

  dislikeSuccess: function (response) {
    return {
      response,
      type: RecipeConstants.DISLIKE_SUCCESS,
    };
  },

  dislike: function (recipeId, data, cb) {
    return (dispatch) => {
      return fetch(
        `${ApiConstants.getApiPath()}recipes/${recipeId}/dislike?patientId=${
          global.userID
        }`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            accept: "application/json",
            Authorization: `Bearer ${global.accessToken}`,
          },
          body: JSON.stringify(data),
        }
      )
        .then((response) => {
          if (cb) {
            cb({ success: true, data: response });
          }
          dispatch(this.dislikeSuccess(response));
        })
        .catch((error) => {
          if (cb) {
            cb({ success: false, error });
          }
          dispatch(this.dislikeError(error));
        });
    };
  },

  unDislikeError: function (error) {
    return {
      error,
      type: RecipeConstants.UN_DISLIKE_ERROR,
    };
  },

  unDislikeSuccess: function (response) {
    return {
      response,
      type: RecipeConstants.UN_DISLIKE_SUCCESS,
    };
  },

  unDislike: function (recipeId, cb) {
    return (dispatch) => {
      return fetch(
        `${ApiConstants.getApiPath()}recipes/${recipeId}/dislike?patientId=${
          global.userID
        }`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            accept: "application/json",
            Authorization: `Bearer ${global.accessToken}`,
          },
        }
      )
        .then((response) => {
          if (cb) {
            cb({ success: true, data: response });
          }
          dispatch(this.unDislikeSuccess(response));
        })
        .catch((error) => {
          if (cb) {
            cb({ success: false, error });
          }
          dispatch(this.unDislikeError(error));
        });
    };
  },
};

export default RecipeActions;
