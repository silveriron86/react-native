// import axios from 'axios';
import "text-encoding";
import "web-streams-polyfill";
// import {fetch} from 'react-native-fetch-api';
import ApiConstants from "../constants/ApiConstants";
import { ShoppingConstants } from "../constants";

const ShoppingActions = {
  getListError: function (error) {
    return {
      error,
      type: ShoppingConstants.GET_SHOPPING_LIST_ERROR,
    };
  },

  getListSuccess: function (response) {
    return {
      response,
      type: ShoppingConstants.GET_SHOPPING_LIST_SUCCESS,
    };
  },

  getList: function (dates, cb) {
    return (dispatch) => {
      return fetch(
        `${ApiConstants.getApiPath()}shopping-list?dates=${dates}&patientId=${
          global.userID
        }`,
        {
          method: "GET",
          headers: {
            accept: "application/json",
            Authorization: `Bearer ${global.accessToken}`,
          },
        }
      )
        .then((res) => {
          if (res.ok) {
            res.json().then((response) => {
              if (cb) {
                cb({ success: true, data: response });
              }
              dispatch(this.getListSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb({ success: false, _error });
            }
            dispatch(this.getListError(_error));
          }
        })
        .catch((error) => {
          if (cb) {
            cb({ success: false, error });
          }
          dispatch(this.getListError(error));
        });
    };
  },
};

export default ShoppingActions;
