// import axios from 'axios';
// import "text-encoding";
// import "web-streams-polyfill";
// import { fetch } from "react-native-fetch-api";
import ApiConstants from "../constants/ApiConstants";
import { ObservanceConstants } from "../constants";

const ObservanceActions = {
  // List observance data
  getListError: function (error) {
    return {
      error,
      type: ObservanceConstants.GET_OBSERVANCE_LIST_ERROR,
    };
  },
  getListSuccess: function (response) {
    return {
      response,
      type: ObservanceConstants.GET_OBSERVANCE_LIST_SUCCESS,
    };
  },
  getList: function (cb) {
    return (dispatch) => {
      return fetch(
        `${ApiConstants.getApiPath()}observance?patientId=${global.userID}`,
        {
          method: "GET",
          headers: {
            accept: "application/json",
            Authorization: `Bearer ${global.accessToken}`,
          },
        }
      )
        .then((res) => res.json())
        .then((response) => {
          if (cb) {
            cb({ success: true, data: response });
          }
          dispatch(this.getListSuccess(response));
        })
        .catch((error) => {
          if (cb) {
            cb({ success: false, error });
          }
          dispatch(this.getListError(error));
        });
    };
  },

  // Describe observance data
  getError: function (error) {
    return {
      error,
      type: ObservanceConstants.GET_OBSERVANCE_ERROR,
    };
  },
  getSuccess: function (response) {
    return {
      response,
      type: ObservanceConstants.GET_OBSERVANCE_SUCCESS,
    };
  },
  get: function (date, cb) {
    return (dispatch) => {
      return fetch(
        `${ApiConstants.getApiPath()}observance/${date}?patientId=${
          global.userID
        }`,
        {
          method: "GET",
          headers: {
            accept: "application/json",
            Authorization: `Bearer ${global.accessToken}`,
          },
        }
      )
        .then((res) => res.json())
        .then((response) => {
          if (cb) {
            cb({ success: true, data: response });
          }
          dispatch(this.getSuccess(response));
        })
        .catch((error) => {
          if (cb) {
            cb({ success: false, error });
          }
          dispatch(this.getError(error));
        });
    };
  },

  // Record observance data
  postError: function (error) {
    return {
      error,
      type: ObservanceConstants.POST_OBSERVANCE_ERROR,
    };
  },
  postSuccess: function (response) {
    return {
      response,
      type: ObservanceConstants.POST_OBSERVANCE_SUCCESS,
    };
  },
  post: function (data, cb) {
    return (dispatch) => {
      fetch(
        `${ApiConstants.getApiPath()}observance?patientId=${global.userID}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${global.accessToken}`,
          },
          body: JSON.stringify(data),
        }
      )
        .then((response) => {
          if (cb) {
            cb({ success: true, data: response });
          }
          dispatch(this.postSuccess(response));
        })
        .catch((error) => {
          if (cb) {
            cb({ success: false, error });
          }
          dispatch(this.postError(error));
        });
    };
  },

  // Update observance data
  updateError: function (error) {
    return {
      error,
      type: ObservanceConstants.UPDATE_OBSERVANCE_ERROR,
    };
  },
  updateSuccess: function (response) {
    return {
      response,
      type: ObservanceConstants.UPDATE_OBSERVANCE_SUCCESS,
    };
  },
  update: function (data, cb) {
    return (dispatch) => {
      return fetch(
        `${ApiConstants.getApiPath()}observance?patientId=${global.userID}`,
        {
          method: "PATCH",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${global.accessToken}`,
          },
          body: JSON.stringify(data),
        }
      )
        .then((response) => {
          console.log("response =", response);
          if (cb) {
            cb({ success: true, data: response });
          }
          dispatch(this.updateSuccess(response));
        })
        .catch((error) => {
          console.log("error =", error);
          if (cb) {
            cb({ success: false, error });
          }
          dispatch(this.updateError(error));
        });
    };
  },
};

export default ObservanceActions;
