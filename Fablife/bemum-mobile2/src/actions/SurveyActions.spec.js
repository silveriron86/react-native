import createMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import SurveyActions from './SurveyActions';
import {SurveyConstants} from '../constants';

const mws = [thunk];
const mockStore = createMockStore(mws);
const store = mockStore({});
const mock = new MockAdapter(axios);

describe('GET Form Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'survey/food-preference';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: SurveyConstants.GET_FORM_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: SurveyConstants.GET_FORM_ERROR,
    error: {},
  };

  it('Get Form Error', () => {
    expect(SurveyActions.getFormError({})).toEqual(expectedErrorActions);
  });

  it('Get Form Success', () => {
    expect(SurveyActions.getFormSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches GET_FORM_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(SurveyActions.getForm()).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches GET_FORM_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(SurveyActions.getForm()).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});
