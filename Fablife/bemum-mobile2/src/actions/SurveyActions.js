import "text-encoding";
import "web-streams-polyfill";
import ApiConstants from "../constants/ApiConstants";
import SurveyConstants from "../constants/SurveyConstants";

let SurveyActions = {
  completedSurvey: function (index) {
    return {
      type: SurveyConstants.COMPLETED_SURVEY,
      response: index,
    };
  },

  completedFollowupSurvey: function (index) {
    return {
      type: SurveyConstants.COMPLETED_FOLLOWUP_SURVEY,
      response: index,
    };
  },

  setLoadedSurveys: function (val) {
    return {
      type: SurveyConstants.LOADED_SURVEY,
      response: val,
    };
  },

  updateFormData: function (data, cb) {
    return (dispatch) => {
      dispatch({
        type: SurveyConstants.UPDATE_FORM_DATA,
        response: data,
      });
      cb(data);
    };
  },

  getFormError: function (error) {
    return {
      error,
      type: SurveyConstants.GET_FORM_ERROR,
    };
  },

  getFormSuccess: function (response) {
    return {
      response,
      type: SurveyConstants.GET_FORM_SUCCESS,
    };
  },

  getForm: function (type, survey_id, form_id, cb) {
    let url = `${ApiConstants.getApiPath()}survey/${type === "signup" ? "sign-up" : "follow-up"
      }/${survey_id}`;

    if (form_id === "preference_food") {
      url = `${ApiConstants.getApiPath()}survey/food-preference`;
    }

    return async (dispatch) => {
      try {
        const res = await fetch(`${url}?patientId=${global.userID}`, {
          method: "GET",
          headers: {
            accept: "application/json",
            Authorization: `Bearer ${global.accessToken}`,
          },
        })

        if (!res.ok) {
          let _error = {
            status: res.status,
            error: res.statusText,
          };
          if (cb) {
            cb(_error);
          }
          dispatch(this.getFormError(_error));
          return;
        }

        const body = await res.json();
        if (cb) {
          cb(body);
        }

        dispatch(
          this.getFormSuccess({
            type: type,
            id: form_id,
            data: body,
          })
        );
      } catch (error) {
        if (cb) {
          cb(error);
        }
        dispatch(this.getFormError(error));
      };
    };
  },

  postFormError: function (error) {
    return {
      error,
      type: SurveyConstants.POST_FORM_ERROR,
    };
  },

  postFormSuccess: function (response) {
    return {
      response,
      type: SurveyConstants.POST_FORM_SUCCESS,
    };
  },

  postForm: function (data, cb) {
    let url = `${ApiConstants.getApiPath()}survey/${data.type === "signup" ? "sign-up" : "follow-up"
      }/${data.surveyId}`;

    if (data.id === "preference_food") {
      //if BREAKFAST is 'week' or 'always' ==> BREAKFAST_WEEK_CHANGE must be null
      //if BREAKFAST is 'weekend' or 'no' ==> BREAKFAST_WEEK_CHANGE must be true or false
      if (data.form.BREAKFAST === "week" || data.form.BREAKFAST === "always") {
        data.form.BREAKFAST_WEEK_CHANGE = null;
      }
      if (data.form.BREAKFAST === "weekend" || data.form.BREAKFAST === "no") {
        if (
          typeof data.form.BREAKFAST_WEEK_CHANGE === "undefined" ||
          data.form.BREAKFAST_WEEK_CHANGE === null
        ) {
          data.form.BREAKFAST_WEEK_CHANGE = false;
        }
      }
      url = `${ApiConstants.getApiPath()}survey/food-preference`;
    } else if (data.type === "signup" && data.surveyId === "profile") {
      // when alcohol is false, then alcohol weekly qty must be null
      // when alcohol is true, alcohol weekly qty can be different than null
      if (data.form.ALCOHOL !== true) {
        data.form.ALCOHOL_WEEKLY_QTY = null;
      }
    } else if (data.type === "signup" && data.surveyId === "reproductive") {
      if (data.form.MED_HISTORY_INFERTILITY_DIAGNOSED === true) {
        if (
          typeof data.form.MED_TREATMENT_INFERTILITY_LABEL === "undefined" ||
          data.form.MED_TREATMENT_INFERTILITY_LABEL === null
        ) {
          data.form.MED_TREATMENT_INFERTILITY_LABEL = "";
        }
      } else {
        data.form.MED_TREATMENT_INFERTILITY_LABEL = null;
      }
    }

    let body = JSON.parse(JSON.stringify(data.form));
    // eslint-disable-next-line no-unused-vars
    for (const [key, value] of Object.entries(data.form)) {
      if (value === null) {
        delete body[key];
      }
    }

    return async (dispatch) => {
      const user_id = global.userID;
      if (
        data.type === "signup" &&
        data.id !== "preference_food" &&
        data.method === "PUT"
      ) {
        url = `${ApiConstants.getApiPath()}survey/sign-up/${data.surveyId
          }?patientId=${user_id}`;
      }
      const surveyData = {
        ...body,
        ...{
          patient: user_id,
        },
      };
      for (var propName in surveyData) {
        if (
          surveyData[propName] === null ||
          surveyData[propName] === undefined
        ) {
          delete surveyData[propName];
        }
      }

      try {
        const response = await fetch(url, {
          method:
            data.type === "signup" && data.id !== "preference_food"
              ? data.method
              : "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${global.accessToken}`,
          },
          body: JSON.stringify(surveyData),
        });

        const body = await response.json();

        if (!response.ok) {
          const error = {
            status: response.status,
            body
          }
          if (cb) {
            cb({ success: false, error })
          }
          dispatch(this.postFormError(error));
          return;
        }

        cb({ success: true, body })
        dispatch(this.postFormSuccess(body));

      } catch (error) {
        if (cb) {
          cb(error);
        }
        dispatch(this.postFormError(error));
      }
    };
  },
};

export default SurveyActions;
