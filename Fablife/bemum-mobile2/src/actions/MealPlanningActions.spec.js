import createMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import MealPlanningActions from './MealPlanningActions';
import {MealPlanningConstants} from '../constants';

const mws = [thunk];
const mockStore = createMockStore(mws);
const store = mockStore({});
const mock = new MockAdapter(axios);

describe('GET Meal-Planner Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'getIngredients';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: MealPlanningConstants.GET_MEAL_PLANNING_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: MealPlanningConstants.GET_MEAL_PLANNING_ERROR,
    error: {},
  };

  it('Get Meal-Planner Error', () => {
    expect(MealPlanningActions.getError({})).toEqual(expectedErrorActions);
  });

  it('Get Meal-Planner Success', () => {
    expect(MealPlanningActions.getSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches GET_MEAL_PLANNING_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(MealPlanningActions.get()).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches GET_MEAL_PLANNING_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(MealPlanningActions.get()).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});
