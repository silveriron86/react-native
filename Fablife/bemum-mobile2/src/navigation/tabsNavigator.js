/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import {Image, TouchableOpacity} from 'react-native';
import Animated from 'react-native-reanimated';
import {
  CoursesScreen,
  AddProductModal,
  ProfileScreen,
  SignupQuestsViewScreen,
  InformationSheetScreen,
  NotificationSettingsScreen,
  TermsScreen,
  GeneralUseConditionScreen,
  AlimentationScreen,
  AlimentationDetailsScreen,
  AlimentationMenuScreen,
  AlimentationMenuDetailsScreen,
  AlimentationOutsideScreen,
  ProfilingCategoryScreen,
  ProfilingDetailScreen,
} from '../components/Layouts/Tabs';
import MonitoringScreen from '../components/Layouts/Tabs/Progression/Monitoring';
import ObjectivesScreen from '../components/Layouts/Tabs/Progression/Objectives';
import ResourcesScreen from '../components/Layouts/Tabs/Progression/Resources';
import CategoryQuestionScreen from '../components/Layouts/Questionnaires/CategoryQuestion';
import FoodPreferencesScreen from '../components/Layouts/Questionnaires/FoodPreferences';
import {Header, BackButton} from '../components/Widgets';
import commonStyles from '../components/Layouts/Styles';
import Utils from '../utils';
import Colors from '../constants/Colors';
import FoodExclusion from '../components/Layouts/Questionnaires/FoodExclusion';

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();
const TopTab = createMaterialTopTabNavigator();

const tabIcons = [
  require('../../assets/icons/tab1.png'),
  require('../../assets/icons/tab2.png'),
  require('../../assets/icons/tab3.png'),
  require('../../assets/icons/tab4.png'),
];

function getTabBarVisibility(route) {
  const routeName = route.state
    ? route.state.routes[route.state.index].name
    : '';

  if (
    routeName === 'InformationSheet' ||
    routeName === 'NotificationSettings' ||
    routeName === 'AddProduct' ||
    routeName === 'SignupQuestsView' ||
    routeName === 'ProfileCategoryQuestion'
  ) {
    return false;
  }

  return true;
}

function TabsNavigator() {
  return (
    <BottomTab.Navigator
      lazy={true}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          const routes = [
            'ProgressionStack',
            'AlimentationStack',
            'CoursesStack',
            'ProfileStack',
          ];
          const index = routes.indexOf(route.name);
          // console.log(route.name);
          if (route.name === 'ProgressionStack') {
            //
          } else {
            //
          }

          // You can return any component that you like here!
          return (
            <Image
              style={{
                width: 27,
                height: 27,
                resizeMode: 'contain',
                tintColor: color,
              }}
              source={tabIcons[index]}
            />
          );
        },
      })}
      tabBarOptions={{
        activeTintColor: Colors.theme.DARK,
        inactiveTintColor: Colors.theme.BLACK,
        tabStyle: {
          backgroundColor: '#FDF6F5',
          paddingVertical: 12,
        },
        style: {
          height: Utils.getInsetBottom() + 70,
        },
        labelStyle: {
          fontFamily: 'Poppins-Medium',
          fontSize: 10,
        },
      }}>
      <BottomTab.Screen
        name="ProgressionStack"
        component={ProgressionStack}
        options={{
          title: 'Progression',
        }}
        listeners={({navigation}) => ({
          tabPress: e => {
            global.eventEmitter.emit('VISITED_PROGRESSION');
            Utils.logEvent({
              screenName: 'Progression',
              className: 'MonitoringScreen',
            });
          },
        })}
      />
      <BottomTab.Screen
        name="AlimentationStack"
        component={AlimentationStack}
        options={{title: 'Alimentation'}}
        listeners={({navigation}) => ({
          tabPress: e => {
            Utils.logEvent({
              screenName: 'Alimentation',
              className: 'AlimentationScreen',
            });
          },
        })}
      />
      <BottomTab.Screen
        name="CoursesStack"
        component={CoursesStack}
        options={({navigation, route}) => {
          return {
            title: 'Courses',
            tabBarVisible: getTabBarVisibility(route),
          };
        }}
        listeners={({navigation}) => ({
          tabPress: e => {
            Utils.logEvent({
              screenName: 'Courses',
              className: 'CoursesScreen',
            });
          },
        })}
      />
      <BottomTab.Screen
        name="ProfileStack"
        component={ProfileStack}
        options={({navigation, route}) => {
          return {
            title: 'Profil',
            tabBarVisible: getTabBarVisibility(route),
          };
        }}
        listeners={({navigation}) => ({
          tabPress: e => {
            global.eventEmitter.emit('VISITED_PROFILE');
            Utils.logEvent({
              screenName: 'Profil',
              className: 'ProfileScreen',
            });
          },
        })}
      />
    </BottomTab.Navigator>
  );
}

function AlimentationStack(props) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Header />,
        headerBackTitleVisible: false,
        headerTitleStyle: commonStyles.H1,
        headerStyle: commonStyles.headerStyle,
        headerTitleAlign: 'left',
      }}>
      <Stack.Screen
        name="Alimentation"
        component={AlimentationScreen}
        options={{title: '   Alimentation', headerLeft: null}}
      />
      <Stack.Screen
        name="AlimentationDetails"
        component={AlimentationDetailsScreen}
        // options={({navigation, route}) => {
        //   return {
        //     header: null,
        //   };
        // }}
        options={{headerShown: false}}
      />
      {/* <Stack.Screen name="AlimentationDetails" options={{headerShown: false}}>
        {() => <AlimentationDetailsScreen {...props} />}
      </Stack.Screen> */}
      <Stack.Screen
        name="AlimentationMenu"
        component={AlimentationMenuScreen}
        options={({navigation, route}) => {
          return {
            title: 'Menu du déjeuner',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="AlimentationMenuDetails"
        component={MenuDetailsTabStack}
        options={({navigation, route}) => {
          return {
            headerBackground: () => <Header isMenuTab={true} />,
            title: 'Menu du déjeuner',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="AlimentationOutside"
        component={AlimentationOutsideScreen}
        options={({navigation, route}) => {
          return {
            title: 'Je mange dehors',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="FoodExclusion"
        component={FoodExclusion}
        options={({navigation, route}) => {
          return {
            title: 'Aversions',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
    </Stack.Navigator>
  );
}

function MenuTabBar({state, descriptors, navigation, position}) {
  return (
    <Header
      isMenuTab={true}
      style={{
        flexDirection: 'row',
        backgroundColor: 'transparent',
        paddingHorizontal: 20,
        paddingTop: 0,
        height: 40,
        justifyContent: 'flex-start',
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={`tab-${index}`}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={[
              {
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
                paddingHorizontal: 20,
              },
              isFocused && {backgroundColor: 'white'},
            ]}>
            <Animated.Text
              style={{fontSize: 18, fontFamily: 'Poppins-Regular'}}>
              {label}
            </Animated.Text>
          </TouchableOpacity>
        );
      })}
    </Header>
  );
}

function MenuDetailsTabStack(props) {
  return (
    <TopTab.Navigator
      initialRouteName={props.route.params.routeName}
      tabBar={_props => <MenuTabBar {..._props} lazy={true} />}>
      {props.route.params.recipes[0] !== '' &&
        props.route.params.recipes[0] !== null && (
          <TopTab.Screen name="EntreeMenuStack" options={{title: 'Entrée'}}>
            {() => (
              <AlimentationMenuDetailsScreen
                {...props}
                selected={props.route.params.recipes[0]}
              />
            )}
          </TopTab.Screen>
        )}
      {props.route.params.recipes[1] !== '' &&
        props.route.params.recipes[1] !== null && (
          <TopTab.Screen name="PlatMenuStack" options={{title: 'Plat'}}>
            {() => (
              <AlimentationMenuDetailsScreen
                {...props}
                selected={props.route.params.recipes[1]}
              />
            )}
          </TopTab.Screen>
        )}
      {props.route.params.recipes[2] !== '' &&
        props.route.params.recipes[2] !== null && (
          <TopTab.Screen name="DessertMenuStack" options={{title: 'Dessert'}}>
            {() => (
              <AlimentationMenuDetailsScreen
                {...props}
                selected={props.route.params.recipes[2]}
              />
            )}
          </TopTab.Screen>
        )}
    </TopTab.Navigator>
  );
}

function CoursesStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Header />,
        headerBackTitleVisible: false,
        headerTitleStyle: commonStyles.H1,
        headerStyle: commonStyles.headerStyle,
        headerTitleAlign: 'left',
      }}>
      <Stack.Screen
        name="Courses"
        component={CoursesScreen}
        options={{title: '   Liste de courses', headerLeft: null}}
      />
      <Stack.Screen
        name="AddProduct"
        component={AddProductModal}
        options={({navigation, route}) => {
          return {
            title: 'Ajouter un produit',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
    </Stack.Navigator>
  );
}

function ProgressionStack() {
  return (
    <Stack.Navigator
      screenOptions={({navigation}) => ({
        headerBackground: () => <Header />,
        headerBackTitleVisible: false,
        headerTitleStyle: commonStyles.H1,
        headerStyle: commonStyles.headerStyle,
        headerTitleAlign: 'left',
      })}>
      {/* <Stack.Screen
        name="Progression"
        component={MonitoringStack}
        options={{title: 'Progression'}}
      /> */}
      <Stack.Screen
        name="Monitoring"
        component={MonitoringScreen}
        options={{
          title: '   Progression',
          headerShown: true,
          headerLeft: null,
        }}
      />
      <Stack.Screen
        name="ProfilingCategory"
        component={InformationSheetScreen}
        options={({navigation, route}) => {
          return {
            title: 'Mettre à jour',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="ProfilingDetail"
        component={ProfilingDetailScreen}
        options={({navigation, route}) => {
          return {
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
    </Stack.Navigator>
  );
}

// This will be used later
// eslint-disable-next-line no-unused-vars
function ProgressionTabStack() {
  return (
    <TopTab.Navigator>
      <TopTab.Screen
        name="MonitoringStack"
        component={MonitoringStack}
        options={{title: 'Suivi'}}
      />
      <TopTab.Screen
        name="ObjectivesStack"
        component={ObjectivesScreen}
        options={{title: 'Objectifs'}}
      />
      <TopTab.Screen
        name="ResourcesStack"
        component={ResourcesScreen}
        options={{title: 'Ressources'}}
      />
    </TopTab.Navigator>
  );
}

function MonitoringStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Header />,
        headerBackTitleVisible: false,
        headerTitleStyle: commonStyles.H1,
        headerStyle: commonStyles.headerStyle,
      }}>
      <Stack.Screen
        name="Monitoring"
        component={MonitoringScreen}
        options={{headerShown: true, headerLeft: null}}
      />
      <Stack.Screen
        name="ProfilingCategory"
        component={ProfilingCategoryScreen}
      />
      <Stack.Screen name="ProfilingDetail" component={ProfilingDetailScreen} />
    </Stack.Navigator>
  );
}

function ProfileStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Header />,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          ...commonStyles.H1,
          ...{marginLeft: 0},
        },
        headerStyle: commonStyles.headerStyle,
        headerTitleAlign: 'left',
      }}>
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          title: '   Profil',
          headerLeft: null,
        }}
      />
      <Stack.Screen
        name="SignupQuestsView"
        component={SignupQuestsViewScreen}
        options={({navigation, route}) => {
          return {
            title: 'Consulter',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="ProfileCategoryQuestion"
        component={CategoryQuestionScreen}
        options={({navigation, route}) => {
          return {
            title: 'Catégorie de question',
            animationEnabled: true,
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="InformationSheet"
        component={InformationSheetScreen}
        options={({navigation, route}) => {
          return {
            title: 'Mettre à jour',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="FoodPreferencesProfile"
        component={FoodPreferencesScreen}
        options={({navigation, route}) => {
          return {
            title: 'Préférences',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="NotificationSettings"
        component={NotificationSettingsScreen}
        options={({navigation, route}) => {
          return {
            title: 'Notifications',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="Terms"
        component={TermsScreen}
        options={({navigation, route}) => {
          return {
            title: 'Consulter',
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
      <Stack.Screen
        name="GeneralUseCondition"
        component={GeneralUseConditionScreen}
        options={({navigation, route}) => {
          return {
            title: "Condition générale d'utilisation",
            headerLeft: () => <BackButton navigation={navigation} />,
          };
        }}
      />
    </Stack.Navigator>
  );
}

export default TabsNavigator;
