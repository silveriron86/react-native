import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import InitialScreen from '../components/Layouts/Initial';
import LoginScreen from '../components/Layouts/Login/Login';
import ForgotScreen from '../components/Layouts/Login/Forgot';
import WelcomeScreen from '../components/Layouts/Welcome';
import QuestionnairesScreen from '../components/Layouts/Questionnaires/';
import SecondQuestionnairesScreen from '../components/Layouts/Questionnaires/SecondQuestionnaires';
import CategoryQuestionScreen from '../components/Layouts/Questionnaires/CategoryQuestion';
import FoodPreferencesScreen from '../components/Layouts/Questionnaires/FoodPreferences';
import ResumeScreen from '../components/Layouts/Questionnaires/Resume';
import FollowUpQuestionnaireModal from '../components/Layouts/Tabs/Progression/Monitoring/FollowUpQuestionnaire';
import {AddProductModal} from '../components/Layouts/Tabs';
import TabsNavigator from './tabsNavigator';
import {Header, BackButton} from '../components/Widgets';
import commonStyles from '../components/Layouts/Styles';

const Stack = createStackNavigator();
export default () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Initial"
        screenOptions={{
          headerBackground: () => <Header />,
          headerBackTitleVisible: false,
          headerTitleStyle: {
            ...commonStyles.H1,
            ...{marginLeft: 0},
          },
          headerStyle: commonStyles.headerStyle,
          headerTitleAlign: 'left',
        }}>
        <Stack.Screen
          name="Initial"
          component={InitialScreen}
          options={{headerShown: false, animationEnabled: false}}
        />
        <Stack.Screen
          name="Welcome"
          component={WelcomeScreen}
          options={{headerShown: false, animationEnabled: false}}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false, animationEnabled: false}}
        />
        <Stack.Screen
          name="FirstQuestionnaires"
          component={QuestionnairesScreen}
          options={({navigation, route}) => {
            return {
              title: '   Questionnaire',
              animationEnabled: true,
            };
          }}
        />
        <Stack.Screen
          name="SecondQuestionnaires"
          component={SecondQuestionnairesScreen}
          options={({navigation, route}) => {
            return {
              title: 'Questionnaire 2/2',
              animationEnabled: true,
              headerLeft: () => <BackButton navigation={navigation} />,
            };
          }}
        />
        <Stack.Screen
          name="CategoryQuestion"
          component={CategoryQuestionScreen}
          options={({navigation, route}) => {
            return {
              title: 'Catégorie de question',
              animationEnabled: true,
              headerLeft: () => <BackButton navigation={navigation} />,
            };
          }}
        />
        <Stack.Screen
          name="FoodPreferences"
          component={FoodPreferencesScreen}
          options={({navigation, route}) => {
            return {
              title: 'Préférences',
              headerLeft: () => <BackButton navigation={navigation} />,
            };
          }}
        />
        <Stack.Screen
          name="AddProductModal"
          component={AddProductModal}
          options={{title: 'Ajouter un produit', headerShown: false}}
        />
        <Stack.Screen
          name="Resume"
          component={ResumeScreen}
          options={({navigation, route}) => {
            return {
              title: 'Résumé',
              animationEnabled: true,
              headerLeft: () => <BackButton navigation={navigation} />,
            };
          }}
        />
        <Stack.Screen
          name="TabsStack"
          component={TabsNavigator}
          options={{
            headerShown: false,
            animationEnabled: false,
          }}
        />
        <Stack.Screen
          name="Forgot"
          component={ForgotScreen}
          options={({navigation, route}) => {
            return {
              title: 'Mot de passe oublié ?',
              animationEnabled: true,
              headerLeft: () => <BackButton navigation={navigation} />,
            };
          }}
        />
        <Stack.Screen
          name="FollowUpQuestionnaire"
          component={FollowUpQuestionnaireModal}
          options={({navigation, route}) => {
            return {
              title: 'Questionnaire de suivi',
              animationEnabled: true,
              headerLeft: () => <BackButton navigation={navigation} />,
            };
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
