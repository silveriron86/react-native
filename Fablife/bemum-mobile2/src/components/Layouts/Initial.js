import AsyncStorage from '@react-native-community/async-storage';
import { CommonActions } from '@react-navigation/native';
import React from 'react';

export default class InitialScreen extends React.Component {
  async UNSAFE_componentWillMount() {
    try {
      // Redirect to Login screen if no API access token is available
      const accessToken = await AsyncStorage.getItem('ACCESS_TOKEN');
      if (!accessToken) {
        global.eventEmitter.emit("CRISP_URL", {
          crispUrl: "",
        });
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          }),
        );
        return;
      }

      const userId = await AsyncStorage.getItem('USER_ID');
      global.accessToken = accessToken;
      global.userID = userId;



      // Index of the last signup survey that has been completed (9 in total)
      const signupSurveyIndex = parseInt(await AsyncStorage.getItem('completedSignupSurveys'), 10) || 0; // integer
      const isFoodSurveyComplete = !!(await AsyncStorage.getItem('completedFoodPreferences'));           // boolean

      // Signup surveys are not complete ==> redirect to the Signup Survey screens
      if (signupSurveyIndex < 9) {
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: 'FirstQuestionnaires' }],
          }),
        );
        return;
      }

      // Food preference survey is not complete ==> redirect to the FoodPreferences screen
      if (!isFoodSurveyComplete) {
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: 'FoodPreferences' }],
          }),
        );
        return;
      }

      // All surveys are complete ==> redirect to the main app
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: 'TabsStack' }],
        }),
      );
    } catch (error) {
      alert('Une erreur inconnue est survenue');
    }
  }

  render() {
    return <></>;
  }
}