/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {ScrollView, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import CommonStyles from '../Styles';
import {CrispLoader, LoadingOverlay, WelcomeFirst} from '../../Widgets';
import {QuestionnairesList} from '../../Widgets';
import {SurveyConstants} from '../../../constants';
import {SurveyActions} from '../../../actions';
import Utils from '../../../utils';

class QuestionnairesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: 0,
      completed: 0,
    };
  }

  componentDidMount() {
    Utils.logEvent({
      screenName: 'Questionnaires',
      className: 'QuestionnairesScreen',
    });

    const {navigation} = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      // The screen is focused
    });

    AsyncStorage.getItem('completedSignupSurveys', (err, v) => {
      if (!err && v) {
        this.props.completedSurvey(v);
      }
    });

    AsyncStorage.getItem('LOGGED-IN', (_err, v) => {
      if (v === 'true') {
        this.setState({
          loading: SurveyConstants.CATEGORIES.length,
        });
        AsyncStorage.removeItem('LOGGED-IN');
      } else {
        SurveyConstants.CATEGORIES.forEach(category => {
          // console.log(index);
          this.props.getForm({
            type: 'signup',
            survey_id: category.survey_id,
            form_id: category.form_id,
            cb: res => {
              this.setState(
                {
                  loading: this.state.loading + 1,
                },
                () => {
                  console.log('status = ', res.status);
                  if (typeof res.status === 'undefined') {
                    const completed = this.state.completed + 1;
                    this.setState(
                      {
                        completed,
                      },
                      () => {
                        console.log('*** ', completed);
                        this.props.completedSurvey(completed);
                        AsyncStorage.setItem(
                          'completedSignupSurveys',
                          completed.toString(),
                        );
                      },
                    );
                  }
                },
              );
            },
          });
        });
      }
    });
  }

  goNext = () => {
    const {navigation} = this.props;
    navigation.navigate('TabsStack');
  };

  goDetail = (category, readOnly) => {
    const {navigation} = this.props;
    navigation.navigate('CategoryQuestion', {
      type: 'signup',
      category,
      readOnly: readOnly === true ? true : false,
    });
  };

  render() {
    const {loading} = this.state;
    const {InputtingSurveyIndex} = this.props;
    // console.log('InputtingSurveyIndex = ', InputtingSurveyIndex);
    // console.log(this.props);
    return (
      <SafeAreaView style={CommonStyles.container}>
        <ScrollView contentContainerStyle={CommonStyles.wrapper}>
          {/* <Text style={styles.description}>
            Pnam in culpa idiota aliis pravitatis. Principium ponere culpam in
            se justum praeceptium. Neque improperes et aliis qui non perfecle
          </Text> */}
          <QuestionnairesList
            InputtingSurveyIndex={InputtingSurveyIndex}
            categories={SurveyConstants.CATEGORIES}
            onPress={this.goDetail}
            containerStyle={{paddingTop: 0}}
          />
          {/* <PrimaryButton onPress={this.goNext} title="Valider" /> */}
        </ScrollView>
        <LoadingOverlay loading={loading < SurveyConstants.CATEGORIES.length} />
        <WelcomeFirst />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    loadedSurveys: state.SurveyReducer.loadedSurveys,
    InputtingSurveyIndex: state.SurveyReducer.InputtingSurveyIndex,
    profileFormData: state.SurveyReducer.signup_profileFormData,
    healthFormData: state.SurveyReducer.signup_healthFormData,
    reproductive_healthFormData:
      state.SurveyReducer.signup_reproductive_healthFormData,
    physical_activityFormData:
      state.SurveyReducer.signup_physical_activityFormData,
    sleep_fatigueFormData: state.SurveyReducer.signup_sleep_fatigueFormData,
    stressFormData: state.SurveyReducer.signup_stressFormData,
    moral_wellbeingFormData: state.SurveyReducer.signup_moral_wellbeingFormData,
    memory_concentrationFormData:
      state.SurveyReducer.signup_memory_concentrationFormData,
    oxydative_stressFormData:
      state.SurveyReducer.signup_oxydative_stressFormData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    completedSurvey: index => dispatch(SurveyActions.completedSurvey(index)),
    getForm: request =>
      dispatch(
        SurveyActions.getForm(
          request.type,
          request.survey_id,
          request.form_id,
          request.cb,
        ),
      ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QuestionnairesScreen);
