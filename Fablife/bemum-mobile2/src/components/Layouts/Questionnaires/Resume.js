import React from 'react';
import {connect} from 'react-redux';
import {SafeAreaView, ScrollView} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import CommonStyles from '../Styles';
import {PrimaryButton} from '../../Widgets';

class ResumeScreen extends React.Component {
  goNext = () => {
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'TabsStack'}],
      }),
    );
  };

  render() {
    return (
      <SafeAreaView style={CommonStyles.container}>
        <ScrollView contentContainerStyle={CommonStyles.wrapper}>
          <PrimaryButton onPress={this.goNext} title="Continuer" />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResumeScreen);
