import React from 'react';
import {connect} from 'react-redux';
import {SafeAreaView} from 'react-native';
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';
import CommonStyles from '../Styles';
import {CrispLoader} from '../../Widgets';
import * as rootNavigation from '../../../navigation/rootNavigation';
import Form from './Form';
import {IngredientActions} from '../../../actions';
import Utils from '../../../utils';

// Préférences alimentaires
class FoodPreferencesScreen extends React.Component {
  changeFormData = () => {
    // console.log('* changeFormData', question_id, value);
  };

  componentDidMount() {
    Utils.logEvent({
      screenName: 'FoodPreferences',
      className: 'FoodPreferencesScreen',
    });

    this.props.getIngredients({
      cb: () => {},
    });
  }

  goNext = () => {
    const {navigation, route} = this.props;
    const from = route.params?.from ?? '';
    if (from === 'Profile') {
      // from Profile
      rootNavigation.goBack();
    } else {
      // from first connection
      navigation.navigate('TabsStack');
    }
  };

  render() {
    const {navigation} = this.props;

    return (
      <SafeAreaView style={CommonStyles.container}>
        <KeyboardAvoidingScrollView
          contentContainerStyle={CommonStyles.wrapper}>
          {/* <QuestionsListView
            questions={FoodPreferencesQuestions}
            navigation={navigation}
            changeFormData={this.changeFormData}
          /> */}
          <Form
            id={'preference_food'}
            surveyId={'food-preference'}
            type={'signup'}
            navigation={navigation}
            readOnly={false}
          />
          {/* <View style={{marginTop: 20}}>
            <PrimaryButton onPress={this.goNext} title="Valider" />
          </View> */}
        </KeyboardAvoidingScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    preference_foodFormData: state.SurveyReducer.preference_foodFormData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getIngredients: req => dispatch(IngredientActions.getIngredients(req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FoodPreferencesScreen);
