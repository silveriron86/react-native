import React from 'react';
import { SafeAreaView } from 'react-native';
import { KeyboardAvoidingScrollView } from 'react-native-keyboard-avoiding-scroll-view';
import { connect } from 'react-redux';
import Utils from '../../../utils';
import { CrispLoader } from '../../Widgets';
import CommonStyles from '../Styles';
import Form from './Form';

class CategoryQuestionScreen extends React.Component {

  componentDidMount() {
    const { category } = this.props.route.params;
    let headerTitle = category.title;
    if (category.form_id === 'moral_wellbeing') {
      headerTitle = 'Moral & bien-être';
    } else if (category.form_id === 'memory_concentration') {
      headerTitle = 'Mémoire';
    }

    Utils.logEvent({
      screenName: 'Question',
      className: 'CategoryQuestionScreen',
    });

    this.props.navigation.setOptions({
      headerTitle,
    });
  }

  goNext = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  render() {
    const { navigation, route } = this.props;
    const { category, type, readOnly } = route.params;
    console.log(category.form_id, category.survey_id, type);
    return (
      <SafeAreaView style={[CommonStyles.container]}>
        <KeyboardAvoidingScrollView
          contentContainerStyle={CommonStyles.wrapper}>
          <Form
            id={category.form_id}
            surveyId={category.survey_id}
            type={type}
            navigation={navigation}
            readOnly={readOnly}
          />
        </KeyboardAvoidingScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CategoryQuestionScreen);
