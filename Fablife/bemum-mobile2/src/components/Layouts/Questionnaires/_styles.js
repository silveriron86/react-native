import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  description: {
    color: 'black',
  },
  validateBtn: {
    marginTop: 20,
  },
  questionRow: {
    borderBottomWidth: 1,
    borderColor: '#ccc',
  },
});
