import AsyncStorage from '@react-native-community/async-storage';
import { CommonActions } from '@react-navigation/core';
import moment from 'moment';
import React from 'react';
import { ActivityIndicator, SafeAreaView, Text, View } from 'react-native';
import { KeyboardAvoidingScrollView } from 'react-native-keyboard-avoiding-scroll-view';
import { connect } from 'react-redux';
import { SurveyActions, AccountActions } from '../../../actions';
import { Colors, SurveyConstants } from '../../../constants';
import Utils from '../../../utils';
import {
  Autocomplete, Check, Date, Input, MyAlert, PrimaryButton, Radio, Switch, Wheel
} from '../../Widgets';
import commonStyles from '../Styles';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.alert = React.createRef();
    this.state = {
      init: true,
      loading: false,
      showErrorMsg: false,
    };
  }

  close = () => {
    this.props.navigation.goBack();
  };

  changeFormData = (question_id, value) => {
    const { type, id } = this.props;
    const form_id = id;

    this.props.updateFormData({
      data: {
        type,
        form_id: form_id,
        question_id: question_id,
        value: value,
      },
      cb: () => {
        this.forceUpdate();
      },
    });
  };

  onPressNext = () => {
    const {
      navigation,
      id,
      surveyId,
      InputtingSurveyIndex,
      InputtingFollowupSurveyIndex,
      type,
      readOnly,
    } = this.props;
    const form_id = id;
    console.log(readOnly);

    if (readOnly === true) {
      if (form_id === 'oxydative_stress') {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: 'FoodPreferences' }],
          }),
        );
      } else {
        if (form_id === 'preference_food') {
          AsyncStorage.setItem('completedFoodPreferences', 'true');
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: 'TabsStack' }],
            }),
          );
        } else {
          const index = this._getFormIndex(form_id);
          navigation.goBack();
          navigation.navigate('CategoryQuestion', {
            type: 'signup',
            category: SurveyConstants.CATEGORIES[index + 1],
            readOnly: false,
          });
        }
      }
      return;
    }

    let requiredFields = 0;
    let filledFields = 0;
    let requiredField = '';
    let questions = this.props[`${type}_${form_id}FormData`].questions;
    let data = this.props[`${type}_${form_id}FormData`].data;
    let form = {};
    let invalid = false;
    let error_q_label = '';
    let isValidInputs = false;
    let conditionErrorMsg = '';
    console.log(form_id, `${type}_${form_id}FormData`, data);

    questions.forEach((question, qIndex) => {
      if (
        invalid === false &&
        Utils.checkVisibleQuestion(
          data,
          questions,
          question,
          this.props,
          type,
          form_id,
        ) === true
      ) {
        form[question.question_id] = question.value;
        if (question.ui !== 'checkbox' && question.required === true) {
          requiredFields++;
          if (question.value !== null) {
            filledFields++;
          } else {
            if (requiredField === '') {
              requiredField = question.label_question;
            }
          }
        }

        if (question.ui === 'stepper') {
          if (
            question.required === true ||
            (question.value !== null && question.value !== '')
          ) {
            if (
              question.ui_config.min > question.value ||
              question.value > question.ui_config.max
            ) {
              invalid = true;
              conditionErrorMsg = Utils.checkValueRange(
                question.ui_config.min,
                question.ui_config.max,
                question.value,
              );
              if (!error_q_label) {
                error_q_label = question.label_question;
              }
            }
          } else {
            if (
              typeof question.conditions !== 'undefined' &&
              question.conditions.length > 0
            ) {
              if (question.value === '' || question.value === null) {
                delete form[question.question_id];
              } else {
                isValidInputs = true;
              }

              if (
                (qIndex < questions.length - 1 &&
                  questions[qIndex + 1].ui !== 'stepper') ||
                qIndex === question.length - 1
              ) {
                invalid = !isValidInputs;
                isValidInputs = false;
                if (invalid === true) {
                  this.alert.current.show(
                    'Erreur',
                    'Le formulaire contient des réponses invalides. Veuillez vérifier votre saisie.',
                  );
                  return;
                }
              }
            } else {
              if (question.value === '') {
                delete form[question.question_id];
              }
            }
          }
        }
      }
    });

    if (invalid) {
      this.setState(
        {
          showErrorMsg: true,
        },
        () => {
          this.alert.current.show(
            'Erreur',
            conditionErrorMsg
              ? `${error_q_label} : ${conditionErrorMsg}`
              : `${error_q_label} : Le formulaire contient des réponses invalides. Veuillez vérifier votre saisie.`,
          );
        },
      );
      return;
    }

    if (filledFields !== requiredFields) {
      this.alert.current.show(
        'Erreur',
        // `${requiredField} renseignez le champs`,
        `Valeur manquante pour ${requiredField}`,
      );
      return;
    }

    this.setState(
      {
        loading: true,
      },
      () => {
        const index = this._getFormIndex(form_id);
        const method = index < InputtingSurveyIndex ? 'PUT' : 'POST';
        this.props.postForm({
          data: {
            type,
            form: form,
            id: form_id,
            surveyId,
            method,
          },
          cb: res => {
            console.log(res);
            this.setState(
              {
                loading: false,
              },
              () => {
                if (!res.success) {
                  let msg = '';
                  if (typeof res.error.body === 'string') {
                    msg = res.error.body
                    const cross_violation_suffix = '_CROSS_CHECK violation';
                    if (msg.indexOf(cross_violation_suffix) > 0) {
                      const property = msg.replace(cross_violation_suffix, '');
                      msg = `${property} should be checked`;
                      msg = this.replaceQuestionLabel(msg, property);
                    }
                  } else if (Array.isArray(res.error.body)) {
                    if (
                      typeof res.error.body[0].constraints !== 'undefined'
                    ) {
                      const values = Object.values(
                        res.error.body[0].constraints,
                      );
                      msg = values[0];
                    }
                    if (res.error.body[0].property) {
                      const property = res.error.body[0].property;
                      msg = this.replaceQuestionLabel(msg, property);
                    } else {
                      const property = res.error.body[0].split(' ')[0];
                      msg = this.replaceQuestionLabel(
                        res.error.body[0],
                        property,
                      );
                      console.log('message = ', msg);
                    }
                  } else {
                    msg = res.error.body.message;
                  }
                  setTimeout(() => {
                    this.alert.current.show(
                      'Erreur',
                      msg,
                    );
                  }, 100);
                  return;
                }



                if (type === 'signup') {
                  const intIndex = parseInt(InputtingSurveyIndex, 10);
                  if (
                    intIndex === parseInt(this._getFormIndex(form_id), 10)
                  ) {
                    let completedSignupSurveys = intIndex + 1;
                    this.props.completedSurvey(completedSignupSurveys);
                    AsyncStorage.setItem(
                      'completedSignupSurveys',
                      completedSignupSurveys.toString(),
                    );
                  }

                  if (form_id === 'oxydative_stress') {
                    // navigation.navigate('FoodPreferences');
                    navigation.dispatch(
                      CommonActions.reset({
                        index: 0,
                        routes: [{ name: 'FoodPreferences' }],
                      }),
                    );
                  } else {
                    if (form_id === 'preference_food') {
                      AsyncStorage.setItem(
                        'completedFoodPreferences',
                        'true',
                      );
                      // navigation.navigate('TabsStack');
                      navigation.dispatch(
                        CommonActions.reset({
                          index: 0,
                          routes: [{ name: 'TabsStack' }],
                        }),
                      );
                    } else {
                      navigation.goBack();
                      navigation.navigate('CategoryQuestion', {
                        type: 'signup',
                        category:
                          type === 'signup'
                            ? SurveyConstants.CATEGORIES[index + 1]
                            : SurveyConstants.FOLLOWUP_CATEGORIES[index + 1],
                        readOnly: false,
                      });
                    }
                  }
                } else {
                  // follow up
                  const intIndex = parseInt(InputtingFollowupSurveyIndex, 10);
                  AsyncStorage.setItem(
                    'FOLLOWUP_UPDATED_DATE',
                    moment()
                      .unix()
                      .toString(),
                  );
                  if (intIndex === index) {
                    let completedFollowupSurveys = intIndex + 1;
                    this.props.completedFollowupSurvey(
                      completedFollowupSurveys,
                    );
                    AsyncStorage.setItem(
                      'completedFollowupSurveys',
                      completedFollowupSurveys.toString(),
                    );
                  }

                  if (index === 7) {
                    this.props.getPatient({
                      cb: () => {}
                    });
                  }

                  navigation.goBack();
                  if (form_id !== 'memory_concentration') {
                    navigation.navigate('CategoryQuestion', {
                      type,
                      category: SurveyConstants.CATEGORIES[index + 1],
                      readOnly: false,
                    });
                  }
                }

              },
            );
          },
        });
      },
    );
  };

  getQuestionById(question_id) {
    const { id, type } = this.props;
    const form_id = id;
    let questions = this.props[`${type}_${form_id}FormData`].questions;
    console.log(questions, question_id);
    const found = questions.find(question => {
      return question.question_id === question_id;
    });
    return found;
  }

  _getFormIndex = form_id => {
    return SurveyConstants.CATEGORIES.findIndex(
      category => category.form_id === form_id,
    );
  };

  replaceQuestionLabel(message, property) {
    const found = this.getQuestionById(property);
    console.log('found= ', found);
    let ret = message;
    if (found) {
      if (message.indexOf('must be a') > 0) {
        ret = `Valeur manquante pour ${found.label_question}`;
      } else if (message.indexOf('must not be greater than') > 0) {
        ret = message.replace(property, '');
        let value = ret.replace('must not be greater than', '').trim();
        ret = `${found.label_question} doit être supérieure à ${value}`;
      } else if (message.indexOf('must not be less than') > 0) {
        ret = message.replace(property, '');
        let value = ret.replace('must not be less than', '').trim();
        ret = `${found.label_question} doit être inférieure à ${value}`;
      } else {
        ret = message.replace(property, found.label_question);
      }
    }
    return ret;
  }

  render() {
    const { showErrorMsg, loading } = this.state;
    const { id, readOnly, type, ingredients } = this.props;
    const form_id = id;
    const itemColor = Colors.theme.DARK;

    let rows = [];
    if (this.props[`${type}_${form_id}FormData`].questions) {
      let questions = this.props[`${type}_${form_id}FormData`].questions;
      let data = this.props[`${type}_${form_id}FormData`].data;
      let isCheckBox = false;
      // console.log(questions);
      questions.forEach((question, idx) => {
        // console.log(question);
        if (
          Utils.checkVisibleQuestion(
            data,
            questions,
            question,
            this.props,
            type,
            form_id,
          ) === true
        ) {
          if (isCheckBox && question.ui !== 'checkbox') {
            isCheckBox = false;
            rows.push(
              <View style={commonStyles.bottomBorder} key={`border-${idx}`} />,
            );
          }
          if (!question.ui) {
            // console.log(question.label_question);
            rows.push(
              <View
                style={commonStyles.textRow}
                key={`question_${question.question_id}`}>
                <Text style={commonStyles.label}>
                  {question.label_question}
                </Text>
                {question.tooltip && (
                  <Text style={commonStyles.tooltipText}>
                    {question.tooltip}
                  </Text>
                )}
              </View>,
            );
          } else if (question.ui === 'date') {
            rows.push(
              <View
                style={[commonStyles.questionRow, commonStyles.pb0]}
                key={`question_${question.question_id}`}>
                <Date
                  label={question.label_question}
                  tooltip={question.tooltip}
                  // max={question.ui_config.max}
                  value={question.value}
                  required={question.required}
                  readOnly={readOnly}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              </View>,
            );
          } else if (question.ui === 'stepper') {
            rows.push(
              <View
                style={[commonStyles.questionRow, commonStyles.pb0]}
                key={`question_${question.question_id}`}>
                <Input
                  type="number"
                  label={question.label_question}
                  placeholder={`0 ${question.unit ? question.unit.short_name : ''
                    }`}
                  tooltip={question.tooltip}
                  isShowableError={showErrorMsg}
                  min={question.ui_config.min}
                  max={question.ui_config.max}
                  precision={question.ui_config.precision}
                  required={question.required}
                  value={question.value}
                  readOnly={readOnly}
                  suffix={question.unit ? ` ${question.unit.short_name}` : ''}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              </View>,
            );
          } else if (question.ui === 'text') {
            rows.push(
              <View
                style={[commonStyles.questionRow, commonStyles.pb0]}
                key={`question_${question.question_id}`}>
                <Input
                  type="text"
                  label={question.label_question}
                  placeholder=""
                  tooltip={question.tooltip}
                  isShowableError={showErrorMsg}
                  max={question.ui_config.max}
                  required={question.required}
                  value={question.value}
                  readOnly={readOnly}
                  suffix={''}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              </View>,
            );
          } else if (question.ui === 'radio') {
            rows.push(
              <View
                style={commonStyles.questionRow}
                key={`question_${question.question_id}`}>
                <Radio
                  itemColor={itemColor}
                  label={question.label_question}
                  tooltip={question.tooltip}
                  data={question.ui_config.choices}
                  required={question.required}
                  value={question.value}
                  readOnly={readOnly}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              </View>,
            );
          } else if (question.ui === 'switch') {
            rows.push(
              <View
                style={commonStyles.questionRow}
                key={`question_${question.question_id}`}>
                <Switch
                  itemColor={itemColor}
                  label={question.label_question}
                  data={question.ui_config.choices}
                  tooltip={question.tooltip}
                  required={question.required}
                  value={question.value}
                  readOnly={readOnly}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              </View>,
            );
          } else if (question.ui === 'checkbox') {
            isCheckBox = true;
            rows.push(
              <Check
                key={`question_${question.question_id}`}
                id={question.question_id}
                label={question.label_question}
                value={question.value}
                itemColor={itemColor}
                readOnly={readOnly}
                onChange={value =>
                  this.changeFormData(question.question_id, value)
                }
              />,
            );
          } else if (question.ui === 'wheel') {
            console.log('value = ', question.value);
            rows.push(
              <View
                style={[commonStyles.questionRow, commonStyles.pb0]}
                key={`question_${question.question_id}`}>
                <Wheel
                  label={question.label_question}
                  tooltip={question.tooltip}
                  placeholder={`0 ${question.unit ? question.unit.short_name : ''
                    }`}
                  config={question.ui_config}
                  unit={question.unit ?? ''}
                  itemColor={itemColor}
                  required={question.required}
                  value={question.value}
                  readOnly={readOnly}
                  onChange={value =>
                    this.changeFormData(question.question_id, value)
                  }
                />
              </View>,
            );
          } else if (question.ui === 'autocomplete') {
            rows.push(
              <View
                style={[commonStyles.questionRow, commonStyles.pb0]}
                key={`question_${question.question_id}`}>
                <Autocomplete
                  ingredients={ingredients}
                  itemColor={itemColor}
                  url={question.ui_config.path}
                  value={question.value ? question.value : []}
                  label={question.label_question}
                  navigation={this.props.navigation}
                  onChange={value => {
                    console.log(value);
                    this.changeFormData(question.question_id, value);
                  }}
                />
              </View>,
            );
          } else {
            // console.log('unkonw', question);
          }
        }
      });
    }

    return (
      <>
        <SafeAreaView style={commonStyles.container}>
          <KeyboardAvoidingScrollView style={{paddingBottom: 65}}>
            {rows}
            {rows.length > 0 && readOnly === false && loading === false && (
              <PrimaryButton onPress={this.onPressNext} title="Valider" />
            )}
            {loading === true && <ActivityIndicator size={'large'} />}
          </KeyboardAvoidingScrollView>
          <MyAlert ref={this.alert} />
        </SafeAreaView>
        {/* {loading === true && <LoadingOverlay loading={true} />} */}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    ingredients: state.IngredientReducer.ingredients,
    InputtingSurveyIndex: state.SurveyReducer.InputtingSurveyIndex,
    signup_profileFormData: state.SurveyReducer.signup_profileFormData,
    signup_reproductive_healthFormData:
      state.SurveyReducer.signup_reproductive_healthFormData,
    signup_healthFormData: state.SurveyReducer.signup_healthFormData,
    signup_physical_activityFormData:
      state.SurveyReducer.signup_physical_activityFormData,
    signup_sleep_fatigueFormData:
      state.SurveyReducer.signup_sleep_fatigueFormData,
    signup_stressFormData: state.SurveyReducer.signup_stressFormData,
    signup_moral_wellbeingFormData:
      state.SurveyReducer.signup_moral_wellbeingFormData,
    signup_memory_concentrationFormData:
      state.SurveyReducer.signup_memory_concentrationFormData,
    signup_oxydative_stressFormData:
      state.SurveyReducer.signup_oxydative_stressFormData,

    InputtingFollowupSurveyIndex:
      state.SurveyReducer.InputtingFollowupSurveyIndex,
    followup_profileFormData: state.SurveyReducer.followup_profileFormData,
    followup_reproductive_healthFormData:
      state.SurveyReducer.followup_reproductive_healthFormData,
    followup_healthFormData: state.SurveyReducer.followup_healthFormData,
    followup_physical_activityFormData:
      state.SurveyReducer.followup_physical_activityFormData,
    followup_sleep_fatigueFormData:
      state.SurveyReducer.followup_sleep_fatigueFormData,
    followup_stressFormData: state.SurveyReducer.followup_stressFormData,
    followup_moral_wellbeingFormData:
      state.SurveyReducer.followup_moral_wellbeingFormData,
    followup_memory_concentrationFormData:
      state.SurveyReducer.followup_memory_concentrationFormData,

    signup_preference_foodFormData: state.SurveyReducer.preference_foodFormData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    completedSurvey: (index) => dispatch(SurveyActions.completedSurvey(index)),
    completedFollowupSurvey: (index) =>
      dispatch(SurveyActions.completedFollowupSurvey(index)),
    updateFormData: (request) =>
      dispatch(SurveyActions.updateFormData(request.data, request.cb)),
    getForm: (request) =>
      dispatch(
        SurveyActions.getForm(
          request.type,
          request.survey_id,
          request.form_id,
          request.cb
        )
      ),
    postForm: (request) =>
      dispatch(SurveyActions.postForm(request.data, request.cb)),
    getPatient: (req) => dispatch(AccountActions.get(req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form);
