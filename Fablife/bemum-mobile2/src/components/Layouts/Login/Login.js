import React from "react";
import { connect } from "react-redux";
import {
  View,
  TouchableWithoutFeedback,
  Keyboard,
  TextInput,
  TouchableOpacity,
  Text,
  KeyboardAvoidingView,
  Platform,
  ImageBackground,
} from "react-native";
import "text-encoding";
import "web-streams-polyfill";
// import {fetch} from 'react-native-fetch-api';
import axios from "axios";
import { AccountActions, SurveyActions } from "../../../actions";
import styles from "./_styles";
import { Logo, LoadingOverlay } from "../../Widgets";
import AsyncStorage from "@react-native-community/async-storage";
import { CommonActions } from "@react-navigation/native";
import NetInfo from "@react-native-community/netinfo";
import commonStyles from "../Styles";
import { SafeAreaView } from "react-native-safe-area-context";
import Utils from "../../../utils";
import { SurveyConstants } from "../../../constants";

const loginBgImg = require("../../../../assets/images/login_bg.png");

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      hasError: false,
      message: "",
      loading: false,
      network: true,

      loaded: 0,
      completed: 0,
    };

    this.passwdInputRef = React.createRef();
  }

  componentDidMount() {    
    Utils.logEvent({
      screenName: "Login",
      className: "LoginScreen",
    });

    AsyncStorage.getItem("completedSignupSurveys", (err, v) => {
      if (!err && v) {
        this.props.completedSurvey(v);
      }
    });

    // @see https://github.com/facebook/react-native/issues/30123#issuecomment-711076098
    this.passwdInputRef.current.setNativeProps({
      style: { fontFamily: "Poppins-Regular" }
    })
  }

  onLostKey = () => {
    const { navigation } = this.props;
    navigation.navigate("Forgot");
  };

  onPressLogin = async () => {
    const networkState = await NetInfo.fetch();

    console.log(networkState);

    if (networkState.isConnected === false) {
      return this.setState({
        hasError: true,
        message:
          "Connexion impossible. Veuillez vérifier que vous disposez d’une connexion Internet suffisante.",
      });
    }

    const { password } = this.state;
    const email = this.state.email.trim();

    if (!email) {
      return this.setState({
        hasError: true,
        message: "Veuillez renseigner votre adresse email",
      });
    }

    if (!password) {
      return this.setState({
        hasError: true,
        message: "Veuillez renseigner votre mot de passe",
      });
    }

    Keyboard.dismiss();

    this.setState(
      {
        loading: true,
        message: "",
      },
      () => {
        this.props.login({
          data: {
            email,
            password,
          },
          cb: (res) => {
            console.log(res);

            if (!res.success) {
              return this.setState({
                message:
                  res._error.status === 423
                    ? "Trop de tentatives de connexion, votre compte est bloqué. Veuillez suivre la procédure de mot de passe oublié."
                    : "Adresse email ou mot de passe incorrect",
                hasError: true,
                loading: false,
              });
            }

            this.setState(
              {
                message: "",
                hasError: false,
              },
              () => {
                const uuid = JSON.parse(
                  Utils.atob(res.data.accessToken.split(".")[1])
                ).sub;
                AsyncStorage.setItem(
                  "ACCESS_TOKEN",
                  res.data.accessToken
                );
                AsyncStorage.setItem("USER_ID", uuid);
                global.accessToken = res.data.accessToken;
                global.userID = uuid;

                this.props.getPatient({
                  cb: (patient) => {
                    console.log("* patient: ", patient);
                    AsyncStorage.setItem(
                      "CRISP_URL",
                      patient.crispConversation
                    );
                    global.eventEmitter.emit("CRISP_URL", {
                      crispUrl: patient.crispConversation,
                    });
                    AsyncStorage.getItem("fcmToken", (_err, fcmToken) => {
                      if (fcmToken) {
                        // Each time a patient logs in the mobile app, generate a new firebase token and push it to the list of the tokens stored for this user
                        console.log("*** fcmToken", fcmToken);
                        if (
                          patient.firebaseTokens === null ||
                          patient.firebaseTokens.indexOf(fcmToken) < 0
                        ) {
                          let firebaseTokens =
                            patient.firebaseTokens === null
                              ? []
                              : [...patient.firebaseTokens];
                          firebaseTokens.push(fcmToken);
                          this.props.updatePatient({
                            id: uuid,
                            data: {
                              firebaseTokens,
                            },
                            cb: (result) => {
                              console.log("* updated: ", result);
                            },
                          });
                        }
                      }

                      if (
                        patient.healthSurveyFilled === true &&
                        patient.memorySurveyFilled === true &&
                        patient.moodSurveyFilled === true &&
                        patient.oxidativeSurveyFilled === true &&
                        patient.physicalActivitySurveyFilled === true &&
                        patient.profileSurveyFilled === true &&
                        patient.reproductiveSurveyFilled === true &&
                        patient.sleepSurveyFilled === true &&
                        patient.stressSurveyFilled
                      ) {
                        console.log("*** all filled");

                        // To get weight/waist/hips, need to call get profile at least
                        this.props.getForm({
                          type: "signup",
                          survey_id: "profile",
                          form_id: "profile",
                          cb: () => { },
                        });

                        this.props.getForm({
                          type: "followup",
                          survey_id: "profile",
                          form_id: "profile",
                          cb: () => { },
                        });

                        // completed sign up
                        this.props.completedSurvey(9);
                        AsyncStorage.setItem(
                          "completedSignupSurveys",
                          "9"
                        );
                        if (
                          patient.foodPreferencesSurveyFilled === true
                        ) {
                          AsyncStorage.setItem(
                            "completedFoodPreferences",
                            "true"
                          );
                          this.goNext();
                        } else {
                          this.loadFoodPreference();
                        }
                      } else {
                        this.loadSurveys();
                      }
                    });
                  },
                });
              }
            );

          },
        });
      }
    );
  };

  loadSurveys = () => {
    SurveyConstants.CATEGORIES.forEach((category) => {
      // console.log(index);
      this.props.getForm({
        type: "signup",
        survey_id: category.survey_id,
        form_id: category.form_id,
        cb: (form) => {
          this.setState(
            {
              loaded: this.state.loaded + 1,
            },
            () => {
              console.log("status = ", form.status);
              if (typeof form.status === "undefined") {
                const completed = this.state.completed + 1;
                this.setState(
                  {
                    completed,
                  },
                  () => {
                    console.log("*** ", completed);
                    this.props.completedSurvey(completed);
                    AsyncStorage.setItem(
                      "completedSignupSurveys",
                      completed.toString()
                    );

                    if (
                      this.state.loaded === SurveyConstants.CATEGORIES.length
                    ) {
                      AsyncStorage.setItem("LOGGED-IN", "true");
                      this.loadFoodPreference();
                    }
                  }
                );
              } else {
                if (this.state.loaded === SurveyConstants.CATEGORIES.length) {
                  AsyncStorage.setItem("LOGGED-IN", "true");
                  this.goNext();
                }
              }
            }
          );
        },
      });
    });
  };

  loadFoodPreference = () => {
    console.log("*** loadFoodPreference");
    this.props.getForm({
      type: "signup",
      survey_id: "food-preference",
      form_id: "preference_food",
      cb: (foodForm) => {
        console.log(foodForm);
        if (typeof foodForm.status === "undefined") {
          AsyncStorage.setItem("completedFoodPreferences", "true");
        }
        this.goNext();
      },
    });
  };

  goNext = () => {
    this.setState(
      {
        loading: false,
      },
      () => {
        const { InputtingSurveyIndex } = this.props;
        AsyncStorage.getItem(
          "completedFoodPreferences",
          (__err, completedFood) => {
            console.log("completedFood=", completedFood);
            const defaultRoute =
              parseInt(InputtingSurveyIndex, 10) < 9
                ? "FirstQuestionnaires"
                : completedFood
                  ? "TabsStack"
                  : "FoodPreferences";

            this.props.navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{ name: defaultRoute }],
              })
            );
          }
        );
      }
    );
  };

  render() {
    const { loading, hasError, message } = this.state;
    return (
      <ImageBackground source={loginBgImg} style={styles.screenBackground}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <SafeAreaView style={commonStyles.flex1}>
            <KeyboardAvoidingView
              style={styles.container}
              behavior={Platform.OS === "ios" ? "padding" : "height"}
            >
              <LoadingOverlay loading={loading} />
              <Logo />
              <View style={styles.inputWrapper}>
                <TextInput
                  keyboardType="email-address"
                  style={[styles.input, commonStyles.TextSecondary]}
                  placeholder="Adresse email"
                  textContentType="username"
                  placeholderTextColor="#5C5C5C"
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  autoCorrect={false}
                  onChangeText={(email) => this.setState({ email })}
                />
              </View>
              <View style={styles.inputWrapper}>
                <TextInput
                  ref={this.passwdInputRef}
                  style={[styles.input, commonStyles.TextSecondary]}
                  placeholder="Mot de passe"
                  textContentType="password"
                  placeholderTextColor="#5C5C5C"
                  secureTextEntry={true}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  onChangeText={(password) => this.setState({ password })}
                />
              </View>
              <View style={styles.buttonView}>
                <TouchableOpacity
                  transparent
                  style={styles.btnLostkey}
                  onPress={this.onLostKey}
                >
                  <Text style={commonStyles.Legend}>Mot de passe oublié ?</Text>
                </TouchableOpacity>
              </View>
              {hasError && (
                <View style={styles.msgWrapper}>
                  <Text style={styles.msg}>{message}</Text>
                </View>
              )}
              <View style={styles.btnWrapper}>
                <TouchableOpacity
                  style={styles.button}
                  underlayColor="#fff"
                  onPress={() => this.onPressLogin()}
                >
                  <Text style={styles.buttonText}>Connexion</Text>
                </TouchableOpacity>
              </View>

              {/* <TouchableOpacity
                style={{position: 'absolute', bottom: 40}}
                onPress={() => {
                  Linking.openURL(
                    'https://www.bemum.co/programme/programme-essentiel',
                  );
                }}>
                <Text style={[commonStyles.H4, {color: Colors.theme.DARK}]}>
                  Créer un compte
                </Text>
              </TouchableOpacity> */}
            </KeyboardAvoidingView>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    InputtingSurveyIndex: state.SurveyReducer.InputtingSurveyIndex,
    userData: state.AccountReducer.userData,
    patient: state.AccountReducer.patient,
    error: state.AccountReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (req) => dispatch(AccountActions.login(req.data, req.cb)),
    getPatient: (req) => dispatch(AccountActions.get(req.cb)),
    updatePatient: (req) =>
      dispatch(AccountActions.update(req.id, req.data, req.cb)),
    completedSurvey: (index) => dispatch(SurveyActions.completedSurvey(index)),
    getForm: (request) =>
      dispatch(
        SurveyActions.getForm(
          request.type,
          request.survey_id,
          request.form_id,
          request.cb
        )
      ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
