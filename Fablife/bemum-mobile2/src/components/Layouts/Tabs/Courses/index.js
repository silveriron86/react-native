/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {ScrollView, View, Text} from 'react-native';
import {connect} from 'react-redux';
import moment from 'moment';
import 'moment/locale/fr';
import commonStyles from '../../Styles';
import styles from './_styles';
import {CrispLoader, LoadingOverlay} from '../../../Widgets';
import {DatesSelector, ActionButtons} from './_widgets';
import {ShoppingActions} from '../../../../actions';
import _ from 'lodash';

class CoursesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      dates: [],
    };
    // moment.locale('fr');
  }

  componentDidMount() {
    this.onSelectDate(moment());

    global.eventEmitter.addListener('UPDATE_MEALPLANNING', data => {
      if (
        this.state.dates.indexOf(data.currentDate) >= 0 ||
        data.currentDate === ''
      ) {
        this.loadData();
      }
    });
  }

  loadData = () => {
    const {dates} = this.state;
    this.setState(
      {
        loading: true,
      },
      () => {
        let datesStr = dates.length > 0 ? dates.join(',') : '';
        this.props.getShoppingList({
          dates: datesStr,
          cb: res => {
            this.setState({
              loading: false,
            });
          },
        });
      },
    );
  };

  onSelectDate = dt => {
    let dates = this.state.dates;
    let selectedDate = dt.format('YYYY-MM-DD');
    let pos = dates.indexOf(selectedDate);
    if (pos < 0) {
      dates.push(selectedDate);
    } else {
      dates.splice(pos, 1);
    }

    this.setState({dates}, () => {
      this.loadData();
    });
  };

  onAddProduct = () => {
    const {navigation} = this.props;
    navigation.navigate('AddProduct');
  };

  render() {
    const {dates, loading} = this.state;
    const {shoppings} = this.props;
    let rows = [];
    if (shoppings && shoppings.length > 0) {
      const groups = _.groupBy(shoppings, 'supermarket_aisle_name');
      // eslint-disable-next-line no-unused-vars
      for (const [key, value] of Object.entries(groups)) {
        rows.push({
          key,
          cols: value,
        });
      }
    }
    console.log('course rows = ', rows);
    return (
      <>
        <ScrollView style={{backgroundColor: 'white'}}>
          <DatesSelector dates={dates} onSelect={this.onSelectDate} />
          <View style={[commonStyles.wrapper]}>
            {rows.length > 0 ? (
              <>
                {rows.map((item, index) => {
                  return (
                    <View key={`item-${index}`}>
                      <Text style={[commonStyles.H3, styles.sectionTitle]}>
                        {item.key.toUpperCase()}
                      </Text>
                      {item.cols.map((row, ii) => (
                        <ActionButtons
                          key={`action-row-${index}-${ii}`}
                          defaultValue={row.quantity}
                          name={row.ingredient_name}
                          unit={row.unit_name}
                          readOnly={true}
                        />
                      ))}
                      <View style={{height: 50}} />
                    </View>
                  );
                })}
              </>
            ) : (
              <View style={[commonStyles.wrapper, {paddingTop: 50}]}>
                <Text
                  style={[
                    commonStyles.H2,
                    styles.alimentTitle,
                    {textAlign: 'center', color: '#999'},
                  ]}>
                  Pas de données
                </Text>
              </View>
            )}
            {/* <Text style={[commonStyles.H3, styles.sectionTitle]}>
              FECLUENTS
            </Text>
            {rows1.map((row, index) => (
              <ActionButtons
                key={`action-row1-${index}`}
                defaultValue={row.value}
                name={row.name}
                unit={row.unit}
                readOnly={true}
              />
            ))}

            <Text
              style={[commonStyles.H3, styles.sectionTitle, {marginTop: 50}]}>
              {'Fruits et légumes'.toUpperCase()}
            </Text>
            {rows2.map((row, index) => (
              <ActionButtons
                key={`action-row2-${index}`}
                defaultValue={1}
                name={row.name}
                unit={row.unit}
                readOnly={true}
              />
            ))} */}

            {/* <OutlineButton
              onPress={this.onAddProduct}
              title="Ajouter un produit supplémentaire"
              style={{marginTop: 40}}
            /> */}
          </View>
        </ScrollView>
        <LoadingOverlay loading={loading} />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    shoppings: state.ShoppingReducer.shoppings,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getShoppingList: req =>
      dispatch(ShoppingActions.getList(req.dates, req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CoursesScreen);
