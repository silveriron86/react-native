/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableOpacity, View, Text, Alert} from 'react-native';
import styles from '../_styles';
import alimenStyles from '../../Alimentation/_styles';
import commonStyles from '../../../Styles';
import {Units} from '../../../../../constants';

export default class ActionButtons extends React.Component {
  constructor(props) {
    super(props);
    const {defaultValue} = props;
    this.state = {
      value: defaultValue ?? 0,
    };
  }

  onPlus = () => {
    this.setState(
      {
        value: this.state.value + 1,
      },
      () => {
        const {onChange} = this.props;
        if (onChange) {
          onChange(this.state.value);
        }
      },
    );
  };

  onMinus = () => {
    Alert.alert(
      'Supprimer ce produit ?',
      'Vous pourrez toujours le rajouter plus tard.',
      [
        {
          text: 'Non',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Oui',
          onPress: () => {
            const {value} = this.state;
            let newValue = value - 1;
            this.setState(
              {
                value: newValue < 0 ? 0 : newValue,
              },
              () => {
                const {onChange} = this.props;
                if (onChange) {
                  onChange(this.state.value);
                }
              },
            );
          },
        },
      ],
      {cancelable: false},
    );
  };

  getShortName = unitName => {
    const found = Units.find(unit => {
      return unit.long === unitName;
    });
    return found ? (found.short === 'none' ? '' : found.short) : '';
  };

  render() {
    const {value} = this.state;
    const {name, unit, disabled, readOnly, defaultValue, noAction} = this.props;

    if (noAction) {
      return (
        <TouchableOpacity
          style={[alimenStyles.instRow, disabled && {opacity: 0.3}]}
          onPress={() => this.props.onChoose()}>
          <Text
            style={[
              commonStyles.flex1,
              commonStyles.TextSecondary,
              {marginTop: 5},
            ]}>
            {name}
          </Text>
        </TouchableOpacity>
      );
    }

    return (
      <View style={[alimenStyles.instRow, disabled && {opacity: 0.3}]}>
        {/* <View style={alimenStyles.ingreRect} /> */}
        <Text
          style={[
            commonStyles.flex1,
            commonStyles.TextSecondary,
            {marginTop: 5},
          ]}>
          {name}
        </Text>
        <View style={styles.actionBtns}>
          {readOnly !== true && value > 0 && (
            <TouchableOpacity
              style={styles.minusPlusBtn}
              onPress={this.onMinus}>
              <Text style={styles.minusPlusText}>-</Text>
            </TouchableOpacity>
          )}
          {(value > 0 || (readOnly === true && defaultValue > 0)) && (
            <Text
              style={[
                styles.valueText,
                {textAlign: readOnly === true ? 'right' : 'center'},
              ]}>
              {readOnly === true ? defaultValue : value}
              {typeof unit !== 'undefined' && unit !== 'unit' && (
                <Text style={{fontSize: 12}}> {this.getShortName(unit)}</Text>
              )}
            </Text>
          )}
          {readOnly !== true && (
            <TouchableOpacity
              disabled={disabled}
              style={styles.minusPlusBtn}
              onPress={this.onPlus}>
              <Text style={styles.minusPlusText}>+</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}
