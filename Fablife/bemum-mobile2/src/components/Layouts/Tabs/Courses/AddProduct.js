/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Image,
  View,
  TextInput,
  Text,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
} from 'react-native';
import commonStyles from '../../Styles';
import styles from './_styles';
import {OutlineButton, LoadingOverlay} from '../../../Widgets';
import {ActionButtons} from './_widgets';
import {IngredientActions} from '../../../../actions';
import {connect} from 'react-redux';

const searchIcon = require('../../../../../assets/icons/search.png');

class AddProductModal extends React.Component {
  constructor(props) {
    super(props);
    const {ingredients, route} = props;
    const {selectedValues} = route.params;
    this.state = {
      loading: ingredients.length > 0 ? false : true,
      researchText: '',
      data: [...ingredients],
      selectedValues: [...selectedValues],
    };
  }

  componentDidMount() {
    const {ingredients, route} = this.props;
    const {selectedValues} = route.params;
    console.log('selected values = ', selectedValues);

    if (ingredients.length === 0) {
      setTimeout(() => {
        this.props.getIngredients({
          cb: res => {
            this.setState({
              loading: false,
              selectedValues,
              data: [...res],
            });
          },
        });
      }, 100);
    }
  }

  handleAddProduct = () => {
    const {selectedValues} = this.state;
    const {navigation} = this.props;
    // let added = [];
    // data.forEach((item, index) => {
    //   if (item.value > 0) {
    //     added.push(item);
    //   }
    // });
    // console.log(added);
    global.eventEmitter.emit('ADDED_PRODUCTS', {products: selectedValues});
    navigation.goBack();
  };

  onChangeValues = (v, index) => {
    let data = [...this.state.data];
    data[index].value = v;
    this.setState({data});
  };

  onChoose = id => {
    let selectedValues = [...this.state.selectedValues];
    selectedValues.push({id});
    this.setState({
      selectedValues,
      researchText: '',
    });
  };

  onRemoveItem = index => {
    let selectedValues = [...this.state.selectedValues];
    selectedValues.splice(index, 1);
    this.setState({
      selectedValues,
    });
  };

  onCancelChoose = () => {
    this.setState(
      {
        researchText: '',
      },
      () => {
        Keyboard.dismiss();
      },
    );
  };

  _getName = id => {
    const {ingredients} = this.props;
    if (!ingredients) {
      return '';
    }

    const found = ingredients.find(ing => {
      return ing.id === id;
    });
    return found ? found.name : 'Unknown';
  };

  render() {
    const {researchText, loading, data, selectedValues} = this.state;
    let filteredItems = [];
    if (researchText) {
      filteredItems = data.filter(row => {
        const found = selectedValues.find(
          s =>
            this._getName(s.id)
              .toLowerCase()
              .indexOf(row.name.toLowerCase()) >= 0,
        );
        if (found) {
          return false;
        }
        return row.name.toLowerCase().indexOf(researchText.toLowerCase()) >= 0;
      });
    }
    console.log('*** filteredItems = ', filteredItems);

    const addedIds = [];
    return (
      <SafeAreaView style={commonStyles.container}>
        <LoadingOverlay loading={loading} />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{flex: 1}}>
          <View style={styles.dateSelContainer}>
            <View style={styles.searchWrapper}>
              <Image source={searchIcon} style={styles.searchIcon} />
              <View style={commonStyles.flex1}>
                <TextInput
                  style={[styles.input, styles.searchInput]}
                  placeholder="Rechercher un produit"
                  placeholderTextColor="#9B9B9B"
                  underlineColorAndroid="#BFBFBF"
                  autoCapitalize="none"
                  value={researchText}
                  onChangeText={v => this.setState({researchText: v})}
                />
              </View>
            </View>
          </View>
          <ScrollView
            keyboardShouldPersistTaps={true}
            style={[commonStyles.wrapper, {paddingTop: 5, flex: 1}]}>
            {researchText !== '' ? (
              <>
                {filteredItems.map((row, index) => {
                  const selected = addedIds.indexOf(index) >= 0;
                  return row.name
                    .toLowerCase()
                    .indexOf(researchText.toLowerCase()) >= 0 ? (
                    <ActionButtons
                      noAction={true}
                      defaultValue={row.value}
                      key={`act-row-${index}`}
                      disabled={selected}
                      name={row.name}
                      onChoose={() => this.onChoose(row.id)}
                      onChange={v => this.onChangeValues(v, index)}
                    />
                  ) : null;
                })}
              </>
            ) : (
              <>
                {selectedValues.map((item, i) => (
                  <View style={styles.withColsRow} key={`ac_${i}`}>
                    <Text style={commonStyles.TextSecondary}>
                      {item.name ? item.name : this._getName(item.id)}
                    </Text>
                    <TouchableOpacity
                      style={styles.closeBtn}
                      underlayColor="#fff"
                      onPress={() => this.onRemoveItem(i)}>
                      <Image
                        resizeMode="stretch"
                        source={require('../../../../../assets/icons/close.png')}
                        style={styles.imgClose}
                      />
                    </TouchableOpacity>
                  </View>
                ))}
              </>
            )}
          </ScrollView>
          {researchText !== '' ? (
            <View
              style={{
                padding: 20,
                marginBottom: Platform.OS === 'ios' ? 0 : 10,
              }}>
              <OutlineButton onPress={this.onCancelChoose} title="Annuler" />
            </View>
          ) : (
            <View
              style={{
                padding: 20,
                marginBottom: Platform.OS === 'ios' ? 0 : 10,
              }}>
              <OutlineButton onPress={this.handleAddProduct} title="Terminé" />
            </View>
          )}
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    ingredients: state.IngredientReducer.ingredients,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getIngredients: req => dispatch(IngredientActions.getIngredients(req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddProductModal);
