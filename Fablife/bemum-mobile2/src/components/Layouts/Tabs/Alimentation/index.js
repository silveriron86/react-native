/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  ScrollView,
  FlatList,
  Image,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';

import ImageProgress from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import moment from 'moment';
import commonStyles from '../../Styles';
import {WeekDaySelector} from './_widgets';
import styles from './_styles';
import progressionStyles from '../Progression/_styles';

import choixIcon1 from '../../../../../assets/icons/choix1.png';
import choixIcon2 from '../../../../../assets/icons/choix2.png';
import choixIcon3 from '../../../../../assets/icons/choix3.png';
import {CrispLoader, LoadingOverlay} from '../../../Widgets';
import MealPlanningActions from '../../../../actions/MealPlanningActions';
import AsyncStorage from '@react-native-community/async-storage';

class AlimentationScreen extends React.Component {
  constructor(props) {
    super(props);
    this.scrollRef = React.createRef();
    this.state = {
      loading: false,
      currentDate: moment(),
    };
  }

  componentDidMount() {
    this.onSelectDate(null);

    // global.eventEmitter.addListener('UPDATE_MEALPLANNING', data => {
    //   if (data.currentDate === this.state.currentDate.format('YYYY-MM-DD')) {
    //     this.loadData();
    //   }
    // });
  }

  loadData = () => {
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.getMealPlanning({
          date: this.state.currentDate.format('YYYY-MM-DD'),
          cb: res => {
            this.setState({
              loading: false,
            });
          },
        });
      },
    );
  };

  onSelectDate = newDate => {
    if (newDate === null) {
      newDate = moment();
    }
    console.log('*** onSelectDate = ', newDate.format('DD/MM/YYYY'));
    this.setState({currentDate: newDate}, () => {
      this.loadData();
    });
  };

  goDetail = (item, data) => {
    // console.log('*** Go detail : ', item, data);
    if (item === '') {
      return;
    }
    const {navigation} = this.props;
    AsyncStorage.setItem('SELECTED_RECIPE', item ? JSON.stringify(item) : '');
    navigation.navigate('AlimentationDetails', {data});
  };

  goOutside = data => {
    const {navigation} = this.props;
    navigation.navigate('AlimentationOutside', {data});
  };

  goMenu = type => {
    const {navigation, mealPlanning} = this.props;
    navigation.navigate('AlimentationMenu', {
      mealPlanning,
      type,
    });
  };

  _onPress = () => {};

  render() {
    const {currentDate, loading} = this.state;
    const {mealPlanning} = this.props;
    const listData = [];

    console.log('*** mealPlanning = ', mealPlanning);
    if (mealPlanning) {
      if (mealPlanning.breakfastOption1) {
        listData.push(mealPlanning.breakfastOption1);
      }
      if (mealPlanning.breakfastOption2) {
        listData.push(mealPlanning.breakfastOption2);
      }
    }

    return (
      <>
        <View
          style={{
            position: 'absolute',
            top: 0,
            width: '100%',
            zIndex: 2,
          }}>
          <WeekDaySelector
            currentDate={currentDate}
            onSelect={this.onSelectDate}
          />
        </View>
        <ScrollView
          ref={this.scrollRef}
          style={{backgroundColor: 'white', zIndex: 1}}>
          {/* scrollEnabled={false}> */}
          {mealPlanning ? (
            <View
              style={[
                commonStyles.wrapper,
                {
                  paddingTop: 0,
                  marginTop: 130,
                },
              ]}>
              {listData.length > 0 && (
                <>
                  <Text style={[commonStyles.H2, styles.alimentTitle]}>
                    Choix du petit-déjeuner
                  </Text>
                  <FlatList
                    style={{marginBottom: 20}}
                    horizontal={true}
                    data={listData}
                    renderItem={({item, index, separators}) => (
                      <TouchableOpacity
                        key={item.key}
                        onPress={() =>
                          this.goDetail(item, {
                            selectedBreakfast:
                              index < 2 ? `OPTION_${index + 1}` : 'OTHER',
                          })
                        }
                        onShowUnderlay={separators.highlight}
                        onHideUnderlay={separators.unhighlight}>
                        <View
                          style={[
                            styles.choixCol,
                            {
                              marginRight: 20,
                              width: 300,
                              marginBottom: 0,
                              // marginTop: 60,
                            },
                          ]}>
                          <ImageProgress
                            source={{
                              uri:
                                item !== '' && item !== null
                                  ? item.imageUrl
                                  : '',
                            }}
                            indicator={ProgressBar}
                            style={styles.choixColImg}
                          />
                          <Text style={styles.choixColTitle}>{item.name}</Text>
                          {/* <Text style={styles.choixColValue}>
                          {item.calories} Calories
                        </Text> */}
                        </View>
                      </TouchableOpacity>
                    )}
                  />
                </>
              )}

              {mealPlanning.morningSnack !== '' &&
                mealPlanning.morningSnack !== null && (
                  <>
                    <Text
                      style={[
                        commonStyles.H2,
                        styles.alimentTitle,
                        {marginTop: 20},
                      ]}>
                      Collation
                    </Text>
                    <View style={{marginBottom: 20}}>
                      <TouchableOpacity
                        onPress={() =>
                          this.goDetail(mealPlanning.morningSnack, {
                            tookMorningSnack: true,
                          })
                        }>
                        <ImageProgress
                          source={{
                            uri: mealPlanning.morningSnack.imageUrl,
                          }}
                          indicator={ProgressBar}
                          style={styles.choixColImg}
                        />
                      </TouchableOpacity>
                      <Text style={styles.choixColTitle}>
                        {mealPlanning.morningSnack.name}
                      </Text>
                      {/* <Text style={styles.choixColValue}>57 Calories</Text> */}
                    </View>
                  </>
                )}

              {mealPlanning.lunchMinute && (
                <>
                  <Text
                    style={[
                      commonStyles.H2,
                      styles.alimentTitle,
                      {marginTop: 20},
                    ]}>
                    Choix du déjeuner
                  </Text>
                  <View style={{flexDirection: 'row', marginBottom: 20}}>
                    <TouchableOpacity
                      onPress={() => this.goMenu('lunch')}
                      style={[progressionStyles.mealFillBtn, {flex: 1}]}>
                      <Image source={choixIcon1} style={styles.choixIcon} />
                      <Text
                        style={[
                          styles.choixColTitle,
                          commonStyles.textCenter,
                          {marginTop: 16},
                        ]}>
                        Je cuisine un menu
                      </Text>
                      <Text
                        style={[
                          styles.choixColValue,
                          commonStyles.textCenter,
                          {marginTop: 16},
                        ]}>
                        Entrée, plat, dessert
                      </Text>
                    </TouchableOpacity>
                    <View style={styles.w20} />
                    <TouchableOpacity
                      onPress={() =>
                        this.goDetail(mealPlanning.lunchMinute, {
                          selectedLunch: 'MINUTE',
                        })
                      }
                      style={[progressionStyles.mealFillBtn, {flex: 1}]}>
                      <Image source={choixIcon2} style={styles.choixIcon} />
                      <Text
                        style={[
                          styles.choixColTitle,
                          commonStyles.textCenter,
                          {marginTop: 16},
                        ]}>
                        Je cuisine en 5 minutes
                      </Text>
                      <Text
                        style={[
                          styles.choixColValue,
                          commonStyles.textCenter,
                          {marginTop: 16},
                        ]}>
                        Plat seul
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.goOutside({
                        selectedLunch: 'OUTSIDE',
                      })
                    }
                    style={[progressionStyles.mealFillBtn, styles.flexMealBtn]}>
                    <View style={styles.w20} />
                    <Image source={choixIcon3} style={styles.choixIcon} />
                    <View style={styles.w20} />
                    <View
                      style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={[styles.choixColTitle, commonStyles.textLeft]}>
                        Je mange à l'extérieur
                      </Text>
                      <Text
                        style={[
                          styles.choixColValue,
                          commonStyles.textLeft,
                          {marginTop: 0},
                        ]}>
                        Voir les recommandations
                      </Text>
                    </View>
                  </TouchableOpacity>
                </>
              )}

              {mealPlanning.afternoonSnack !== '' &&
                mealPlanning.afternoonSnack !== null && (
                  <>
                    <Text
                      style={[
                        commonStyles.H2,
                        styles.alimentTitle,
                        {marginTop: 40},
                      ]}>
                      Collation
                    </Text>
                    <View style={{marginBottom: 20}}>
                      <TouchableOpacity
                        onPress={() =>
                          this.goDetail(mealPlanning.afternoonSnack, {
                            tookAfternoonSnack: true,
                          })
                        }>
                        <ImageProgress
                          source={{
                            uri: mealPlanning.afternoonSnack.imageUrl,
                          }}
                          indicator={ProgressBar}
                          style={styles.choixColImg}
                        />
                      </TouchableOpacity>
                      <Text style={styles.choixColTitle}>
                        {mealPlanning.afternoonSnack.name}
                      </Text>
                      {/* <Text style={styles.choixColValue}>57 Calories</Text> */}
                    </View>
                  </>
                )}

              {mealPlanning.dinerMinute && (
                <>
                  <Text
                    style={[
                      commonStyles.H2,
                      styles.alimentTitle,
                      {marginTop: 40},
                    ]}>
                    Choix du dîner
                  </Text>
                  <View style={{flexDirection: 'row', marginBottom: 20}}>
                    <TouchableOpacity
                      onPress={() => this.goMenu('diner')}
                      style={[progressionStyles.mealFillBtn, {flex: 1}]}>
                      <Image source={choixIcon1} style={styles.choixIcon} />
                      <Text
                        style={[
                          styles.choixColTitle,
                          commonStyles.textCenter,
                          {marginTop: 16},
                        ]}>
                        Je cuisine un menu
                      </Text>
                      <Text
                        style={[styles.choixColValue, commonStyles.textCenter]}>
                        Entrée, plat, dessert
                      </Text>
                    </TouchableOpacity>
                    <View style={styles.w20} />
                    <TouchableOpacity
                      onPress={() =>
                        this.goDetail(mealPlanning.dinerMinute, {
                          selectedDiner: 'MINUTE',
                        })
                      }
                      style={[progressionStyles.mealFillBtn, {flex: 1}]}>
                      <Image source={choixIcon2} style={styles.choixIcon} />
                      <Text
                        style={[
                          styles.choixColTitle,
                          commonStyles.textCenter,
                          {marginTop: 16},
                        ]}>
                        Je cuisine en 5 minutes
                      </Text>
                      <Text
                        style={[styles.choixColValue, commonStyles.textCenter]}>
                        Plat seul
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.goOutside({
                        selectedDiner: 'OUTSIDE',
                      })
                    }
                    style={[progressionStyles.mealFillBtn, styles.flexMealBtn]}>
                    <View style={styles.w20} />
                    <Image source={choixIcon3} style={styles.choixIcon} />
                    <View style={styles.w20} />
                    <View
                      style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={[styles.choixColTitle, commonStyles.textLeft]}>
                        Je mange à l'extérieur
                      </Text>
                      <Text
                        style={[
                          styles.choixColValue,
                          commonStyles.textLeft,
                          {marginTop: 0},
                        ]}>
                        Voir les recommandations
                      </Text>
                    </View>
                  </TouchableOpacity>
                </>
              )}
            </View>
          ) : (
            <View
              style={[
                commonStyles.wrapper,
                {
                  paddingTop: 180,
                  height: Dimensions.get('window').height - 180,
                },
              ]}>
              <Text
                style={[
                  commonStyles.H2,
                  styles.alimentTitle,
                  {textAlign: 'center', color: '#999'},
                ]}>
                Pas de données
              </Text>
            </View>
          )}
        </ScrollView>
        <LoadingOverlay loading={loading} />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    mealPlanning: state.MealPlanningReducer.mealPlanning,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMealPlanning: req => dispatch(MealPlanningActions.get(req.date, req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AlimentationScreen);
