/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {ScrollView, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import CommonStyles from '../../Styles';
import {QuestionnairesList, LoadingOverlay} from '../../../Widgets';
import {SurveyConstants} from '../../../../constants';
import {SurveyActions} from '../../../../actions';
import Utils from '../../../../utils';

class SignupQuestsViewScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: 0,
      completed: 0,
    };
  }

  goDetail = category => {
    const {navigation} = this.props;
    navigation.navigate('ProfileCategoryQuestion', {
      type: 'signup',
      category,
      readOnly: true,
    });
  };

  componentDidMount() {
    Utils.logEvent({
      screenName: 'Consulter',
      className: 'SignupQuestsViewScreen',
    });

    setTimeout(() => {
      SurveyConstants.CATEGORIES.forEach((category, index) => {
        // console.log(index);
        this.props.getForm({
          type: 'signup',
          survey_id: category.survey_id,
          form_id: category.form_id,
          cb: res => {
            this.setState(
              {
                loading: this.state.loading + 1,
              },
              () => {
                if (typeof res.status === 'undefined') {
                  const completed = this.state.completed + 1;
                  this.setState(
                    {
                      completed,
                    },
                    () => {
                      this.props.completedSurvey(completed);
                      AsyncStorage.setItem(
                        'completedSignupSurveys',
                        completed.toString(),
                      );
                    },
                  );
                }
              },
            );
          },
        });
      });
    }, 1000);
  }

  render() {
    const {loading} = this.state;
    return (
      <SafeAreaView style={CommonStyles.container}>
        <ScrollView contentContainerStyle={CommonStyles.wrapper}>
          <QuestionnairesList
            InputtingSurveyIndex={9}
            categories={SurveyConstants.CATEGORIES}
            onPress={this.goDetail}
            containerStyle={{paddingTop: 0}}
          />
        </ScrollView>
        <LoadingOverlay loading={loading < SurveyConstants.CATEGORIES.length} />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    loadedSurveys: state.SurveyReducer.loadedSurveys,
    InputtingSurveyIndex: state.SurveyReducer.InputtingSurveyIndex,
    profileFormData: state.SurveyReducer.signup_profileFormData,
    healthFormData: state.SurveyReducer.signup_healthFormData,
    reproductive_healthFormData:
      state.SurveyReducer.signup_reproductive_healthFormData,
    physical_activityFormData:
      state.SurveyReducer.signup_physical_activityFormData,
    sleep_fatigueFormData: state.SurveyReducer.signup_sleep_fatigueFormData,
    stressFormData: state.SurveyReducer.signup_stressFormData,
    moral_wellbeingFormData: state.SurveyReducer.signup_moral_wellbeingFormData,
    memory_concentrationFormData:
      state.SurveyReducer.signup_memory_concentrationFormData,
    oxydative_stressFormData:
      state.SurveyReducer.signup_oxydative_stressFormData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    completedSurvey: index => dispatch(SurveyActions.completedSurvey(index)),
    getForm: request =>
      dispatch(
        SurveyActions.getForm(
          request.type,
          request.survey_id,
          request.form_id,
          request.cb,
        ),
      ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignupQuestsViewScreen);
