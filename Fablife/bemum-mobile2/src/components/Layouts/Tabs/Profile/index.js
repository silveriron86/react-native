import { ENVIRONMENT } from '@env';
import AsyncStorage from '@react-native-community/async-storage';
import { CommonActions } from '@react-navigation/native';
import * as Sentry from "@sentry/react-native";
import React, { useEffect, useState } from 'react';
import { Linking, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { getBuildNumber, getVersion } from 'react-native-device-info';
import { connect } from 'react-redux';
import { AccountActions, SurveyActions } from '../../../../actions';
import * as rootNavigation from '../../../../navigation/rootNavigation';
import { CrispLoader, LoadingOverlay, RedButton } from '../../../Widgets';
import commonStyles from '../../Styles';
import styles from './_styles';

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }
  componentDidMount() {
    this.loadFoodPreference();
    global.eventEmitter.addListener('VISITED_PROFILE', () => {
      this.loadFoodPreference();
    });
  }

  loadFoodPreference = () => {
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.getForm({
          type: 'signup',
          survey_id: 'food-preference',
          form_id: 'preference_food',
          cb: foodForm => {
            console.log('*** foodForm = ', foodForm);
            this.setState({
              loading: false,
            });
          },
        });
      },
    );
  };

  onLogOut = async () => {
    // state that we should keep
    const fcmToken = await AsyncStorage.getItem('fcmToken');
    const visited1 = await AsyncStorage.getItem('VISITED_WELCOME1');
    const visited2 = await AsyncStorage.getItem('VISITED_WELCOME2');
    
    // wipe global data
    await AsyncStorage.clear();
    global.accessToken = '';

    // restore kept state
    if (visited1) {
      AsyncStorage.setItem('VISITED_WELCOME1', 'true');
    }
    if (visited2) {
      AsyncStorage.setItem('VISITED_WELCOME2', 'true');
    }
    if (fcmToken) {
      AsyncStorage.setItem('fcmToken', fcmToken);
    }

    // go back to login
    global.eventEmitter.emit("CRISP_URL", {
      crispUrl: "",
    });
    setTimeout(() => {
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: "Login" }],
        })
      );
    }, 100);
  };

  goPage = page => {
    if (page === 'FoodPreferences') {
      rootNavigation.navigate(page, {
        from: 'Profile',
      });
    } else {
      const { navigation } = this.props;
      navigation.navigate(page);
    }
  };

  render() {
    const { loading } = this.state;
    const { patient } = this.props;
    const disabled = patient === null || patient.updateProfile !== true;
    
    return (
      <>
        <ScrollView contentContainerStyle={{ flexGrow: 1 }} style={[commonStyles.container]}>
          <View style={[styles.container]}>
            <View style={styles.links}>

              <InfoTile patient={patient} />
              
              <TouchableOpacity
                style={[styles.profileLink]}
                onPress={() => this.goPage('SignupQuestsView')}>
                <Text style={commonStyles.Text}>
                  Consulter mes données initiales
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.profileLink}
                disabled={disabled}
                onPress={() => this.goPage('InformationSheet')}>
                <Text style={[commonStyles.Text, disabled && { color: '#888' }]}>
                  Mettre à jour mes données de profil
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.profileLink}
                onPress={() => this.goPage('FoodPreferencesProfile')}>
                <Text style={commonStyles.Text}>
                  Modifier mes préférences alimentaires
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.profileLink}
                onPress={() => this.goPage('NotificationSettings')}>
                <Text style={commonStyles.Text}>Gérer mes notifications</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.profileLink}
                onPress={() => {
                  Linking.openURL(
                    'https://www.bemum.co/legal/protection-des-donnees',
                  );
                }}>
                <Text style={commonStyles.Text}>
                  Consulter notre politique de confidentialité
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.logoutButton}>
              <RedButton title="Déconnexion" onPress={this.onLogOut} />
            </View>

            <LoadingOverlay loading={loading} />
          </View>
        </ScrollView>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    patient: state.AccountReducer.patient,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPatient: req => dispatch(AccountActions.get(req.id, req.cb)),
    getForm: request =>
      dispatch(
        SurveyActions.getForm(
          request.type,
          request.survey_id,
          request.form_id,
          request.cb,
        ),
      ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileScreen);


/**
 * Presentation component to show user email, app version & environment
 * @param {Object} props React props
 * @param {Object} props.patient A BeMum patient
 */
function InfoTile({ patient }) {
  if (!patient) {
    return null;
  }

  // Retrieve app version and build number
  const [appVersion, setAppVersion] = useState(null);
  const [appBuildNumber, setAppBuildNumber] = useState(null);
  useEffect(() => {
    async function getAppInfo() {
      try {
        const version = await getVersion();
        const buildNumber = await getBuildNumber();
        setAppVersion(version);
        setAppBuildNumber(buildNumber);
      } catch (e) {
        Sentry.captureException(e)
      }
    }
    getAppInfo()
  })

  const styles = StyleSheet.create({
    outerWrapper: {
      padding: 15,
      width: '100%',
    },
    innerWrapper: {
      backgroundColor: '#E6E0FF',
      borderRadius: 10,
      padding: 10,
    },
    title: {
      textTransform: 'uppercase',
      fontWeight: '600'
    },
    userName: {
      marginTop: 5,
      fontWeight: '400',
    },
    small: {
      fontSize: 12,
      lineHeight: 13,
    },
    appVersion: {
      marginTop: 10
    }
  })

  return <View style={styles.outerWrapper}>
    <View style={styles.innerWrapper}>
      <Text style={[commonStyles.Text, styles.title]}>MON APPLICATION</Text>
      <Text style={[commonStyles.Text, styles.userName]}>{patient.firstname} {patient.lastname}</Text>
      <Text style={[commonStyles.Text, styles.small]}>{patient.email}</Text>
      <Text style={[commonStyles.Text, styles.appVersion]}>
        <Text style={[commonStyles.Text, styles.userName]}>Version :</Text>
        &nbsp;{appVersion} ({appBuildNumber}) {ENVIRONMENT !== 'PRODUCTION' ? ENVIRONMENT : ''}
        </Text>
      <Text></Text>
    </View>
  </View>
}