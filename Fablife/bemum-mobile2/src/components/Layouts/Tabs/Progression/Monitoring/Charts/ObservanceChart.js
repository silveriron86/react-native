/* eslint-disable react-native/no-inline-styles */
import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { Grid, LineChart, XAxis } from "react-native-svg-charts";
import styles from "../../_styles";
import commonStyles from "../../../../Styles";
import { Colors } from "../../../../../../constants";
import moment from "moment";

const obIcon1 = require("../../../../../../../assets/icons/observations/observation1.png");
const obIcon2 = require("../../../../../../../assets/icons/observations/observation2.png");
const obIcon3 = require("../../../../../../../assets/icons/observations/observation3.png");
const obIcon4 = require("../../../../../../../assets/icons/observations/observation4.png");

export default class ObservanceChart extends React.Component {
  state = {
    collapsed: false,
  };

  toggleExpand = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    const { observances } = this.props;
    if (observances === null) {
      return null;
    }

    const { foodSupplements, mealPlanning } = observances;
    const { collapsed } = this.state;
    let lineData1 = [];
    let lineData2 = [];
    let dates = [];
    if (foodSupplements) {
      foodSupplements.forEach((w) => {
        let value = w.value === null ? 0 : w.value === "no" ? 1 : 3;
        lineData1.push(value);
        const label = moment(w.date).format("DD/MM");
        dates.push(w.date.split("-")[2] === "01" ? label : "");
      });
    }

    if (mealPlanning) {
      mealPlanning.forEach((w) => {
        let value =
          w.value === null
            ? 0
            : w.value === "no"
            ? 1
            : w.value === "partially"
            ? 2
            : 3;
        lineData2.push(parseFloat(value));
        const label = moment(w.date).format("DD/MM");
        dates.push(w.date.split("-")[2] === "01" ? label : "");
      });
    }

    const lineData = [
      { data: lineData2, svg: { stroke: Colors.theme.DARK, strokeWidth: 2 } },
      { data: lineData1, svg: { stroke: Colors.SKY, strokeWidth: 2 } },
    ];
    const labels = [
      { color: Colors.SKY, label: "Compléments alimentaires" },
      { color: Colors.theme.DARK, label: "Programme alimentaire" },
    ];

    const XaxesSvg = { fontSize: 12, fill: "grey" };
    const verticalContentInset = { top: 40, bottom: 10, left: 2, right: 2 };
    const xAxisHeight = 30;

    const TITLE = (
      <View style={styles.chartLabelRow}>
        <View style={styles.ChartLabels}>
          <Text style={commonStyles.H4}>OBSERVANCE</Text>
        </View>
      </View>
    );

    return (
      <View style={{ marginBottom: 5 }}>
        {collapsed && (
          <TouchableOpacity
            style={[styles.chatArea, styles.marginRow]}
            onPress={this.toggleExpand}
          >
            {TITLE}
          </TouchableOpacity>
        )}

        {!collapsed && (
          <View
            style={[styles.chatArea, styles.marginRow, { paddingBottom: 18 }]}
          >
            <TouchableOpacity
              style={[
                styles.withBorderBottom,
                { paddingBottom: 0, marginBottom: 0 },
              ]}
              onPress={this.toggleExpand}
            >
              {TITLE}
            </TouchableOpacity>

            <View style={styles.imcChartWrapper}>
              <View style={{ flex: 1, marginLeft: 30, position: "relative" }}>
                <LineChart
                  style={{ flex: 1 }}
                  data={lineData}
                  contentInset={verticalContentInset}
                  numberOfTicks={4}
                  yMin={0}
                  yMax={3}
                >
                  <Grid />
                </LineChart>
                <XAxis
                  style={{
                    marginHorizontal: -20,
                    height: xAxisHeight,
                  }}
                  data={lineData1}
                  formatLabel={(value, index) => {
                    return dates[index];
                  }}
                  contentInset={{ left: 40, right: 40 }}
                  svg={XaxesSvg}
                />
                <View style={styles.obChartIcons}>
                  <View style={styles.obChartIconWrapper}>
                    <Image source={obIcon1} style={styles.obChartIcon} />
                  </View>
                  <View style={styles.obChartIconWrapper}>
                    <Image source={obIcon2} style={styles.obChartIcon} />
                  </View>
                  <View style={styles.obChartIconWrapper}>
                    <Image source={obIcon3} style={styles.obChartIcon} />
                  </View>
                  <View style={styles.obChartIconWrapper}>
                    <Image source={obIcon4} style={styles.obChartIcon} />
                  </View>
                </View>
              </View>
            </View>
            <View
              style={[
                styles.imcChartInfos,
                { flexDirection: "column", marginBottom: 0 },
              ]}
            >
              {labels.map((l, idx) => {
                return (
                  <View
                    style={[styles.imcChartInfoCol, { width: "100%" }]}
                    key={`lbl-${idx}`}
                  >
                    <View
                      key={`clr-${idx}`}
                      style={[
                        styles.colorDot,
                        {
                          marginRight: 10,
                          backgroundColor: l.color,
                        },
                      ]}
                    />
                    <Text style={commonStyles.labelSubTitle}>{l.label}</Text>
                  </View>
                );
              })}
            </View>
          </View>
        )}
      </View>
    );
  }
}
