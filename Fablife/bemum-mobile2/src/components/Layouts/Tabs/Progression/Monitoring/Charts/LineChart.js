/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {LineChart, XAxis, YAxis} from 'react-native-svg-charts';
import {Circle} from 'react-native-svg';
import styles from '../../_styles';
import commonStyles from '../../../../Styles';
import moment from 'moment';
import Utils from '../../../../../../utils';

export default class SuiviLineChart extends React.Component {
  state = {
    collapsed: true,
  };

  toggleExpand = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    const {metrics} = this.props;
    if (metrics === null) {
      return null;
    }

    const {collapsed} = this.state;
    let lineData = [];
    let dates = [];
    if (typeof metrics.WEIGHT !== 'undefined') {
      metrics.WEIGHT.forEach(w => {
        lineData.push(parseFloat(w.value));
        const label = moment(w.date).format('MM/YY');
        dates.push(dates.indexOf(label) < 0 ? label : '');
      });
    }

    const XaxesSvg = {fontSize: 12, fill: 'grey'};
    const YaxesSvg = {fontSize: 14, fill: 'grey'};
    const verticalContentInset = {top: 10, bottom: 10, left: 20, right: 20};
    const xAxisHeight = 30;
    const Decorator = ({x, y}) => {
      return lineData.map((value, index) => (
        <Circle
          key={index}
          cx={x(index)}
          cy={y(value)}
          r={4}
          stroke={'black'}
          fill={'black'}
        />
      ));
    };

    // const diff =
    //   signupProfile && followupProfile && followupProfile.data[1].value !== null
    //     ? followupProfile.data[1].value - signupProfile.data[20].value
    //     : 0;
    const weight = lineData;
    const n = weight.length - 1;
    const diff = n < 1 ? 0 : weight[n] - weight[0];

    const currentValue = n < 0 ? 0 : Utils.formatNumber(weight[n]);
    return (
      <>
        <Text style={[commonStyles.H4, styles.chartTitle]}>Mensurations</Text>
        {collapsed && (
          <TouchableOpacity
            style={[styles.chatArea, styles.marginRow]}
            onPress={this.toggleExpand}>
            <View style={styles.chartLabelRow}>
              <View style={styles.ChartLabels}>
                <Text style={commonStyles.H4}>POIDS</Text>
                {diff !== 0 && (
                  <Text style={styles.lienChartValue}>
                    {'   '}
                    {Utils.formatNumber(diff)}kg *
                  </Text>
                )}
              </View>
              <Text
                style={[styles.ChartLabelValue, {fontFamily: 'AmericanaStd'}]}>
                {currentValue} <Text style={styles.lienChartUnit}>kg</Text>
              </Text>
            </View>
          </TouchableOpacity>
        )}
        {!collapsed && (
          <View
            style={[styles.chatArea, styles.marginRow, {paddingBottom: 10}]}>
            <TouchableOpacity
              style={[styles.chartLabelRow, styles.withBorderBottom]}
              onPress={this.toggleExpand}>
              <View style={styles.ChartLabels}>
                <Text style={commonStyles.H4}>POIDS</Text>
                {diff !== 0 && (
                  <Text style={styles.lienChartValue}>
                    {'   '}
                    {Utils.formatNumber(diff)}kg *
                  </Text>
                )}
              </View>
              <Text
                style={[styles.ChartLabelValue, {fontFamily: 'AmericanaStd'}]}>
                {currentValue} <Text style={styles.lienChartUnit}>kg</Text>
              </Text>
            </TouchableOpacity>

            <Text style={[styles.lineChartInfo, styles.mb20]}>
              *Depuis le début du programme
            </Text>
            <View
              style={{
                height: 250,
                paddingHorizontal: 10,
                flexDirection: 'row',
              }}>
              <YAxis
                data={lineData}
                style={{marginBottom: xAxisHeight}}
                contentInset={verticalContentInset}
                svg={YaxesSvg}
                numberOfTicks={5}
                min={40}
                max={140}
              />
              <View style={{flex: 1, marginLeft: 10, position: 'relative'}}>
                <View
                  style={{
                    position: 'absolute',
                    width: '100%',
                    height: 205,
                    marginTop: 6,
                    display: 'flex',
                    flexDirection: 'column',
                    borderTopWidth: 1,
                    borderTopColor: '#ccc',
                  }}>
                  {new Array(10).fill(0).map((opt, ii) => {
                    return (
                      <View
                        style={{
                          flex: 1,
                          borderBottomWidth: 1,
                          borderColor: '#ccc',
                        }}
                        key={`line-${ii}`}
                      />
                    );
                  })}
                </View>
                <LineChart
                  style={{flex: 1}}
                  data={lineData}
                  yMin={40}
                  yMax={140}
                  contentInset={verticalContentInset}
                  svg={{stroke: 'black'}}>
                  {/* <Grid /> */}
                  <Decorator />
                </LineChart>
                <XAxis
                  style={{
                    marginHorizontal: -20,
                    height: xAxisHeight,
                  }}
                  data={lineData}
                  formatLabel={(value, index) => {
                    return dates[index];
                  }}
                  contentInset={{left: 40, right: 40}}
                  svg={XaxesSvg}
                />
              </View>
            </View>
            {/* <PrimaryButton
              title="Mettre à jour"
              style={styles.mettreBtn}
              onPress={this.props.handleSelectPoid}
            /> */}
          </View>
        )}
      </>
    );
  }
}
