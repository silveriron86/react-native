/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Grid, LineChart, YAxis} from 'react-native-svg-charts';
import {Circle} from 'react-native-svg';
import styles from '../../_styles';
import {PrimaryButton} from '../../../../../Widgets';
import commonStyles from '../../../../Styles';

export default class SuiviPoidsChart extends React.Component {
  state = {
    collapsed: true,
  };

  toggleExpand = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    const {collapsed} = this.state;
    const lineData = [30, 10, 40, 35, 5];

    const axesSvg = {fontSize: 14, fill: 'grey'};
    const verticalContentInset = {top: 10, bottom: 10, left: 20, right: 20};
    const xAxisHeight = 30;
    const Decorator = ({x, y}) => {
      return lineData.map((value, index) => (
        <Circle
          key={index}
          cx={x(index)}
          cy={y(value)}
          r={4}
          stroke={'black'}
          fill={'black'}
        />
      ));
    };

    return (
      <>
        {collapsed && (
          <TouchableOpacity
            style={[styles.chatArea, styles.marginRow]}
            onPress={this.toggleExpand}>
            <View style={styles.chartLabelRow}>
              <View style={styles.ChartLabels}>
                <Text style={commonStyles.H4}>POIDS</Text>
                <Text style={styles.lienChartValue}>{'   '}-1.5kg *</Text>
              </View>
              <Text style={styles.ChartLabelValue}>
                50,5 <Text style={styles.lienChartUnit}>kg</Text>
              </Text>
            </View>
          </TouchableOpacity>
        )}

        {!collapsed && (
          <View style={[styles.chatArea, styles.marginRow]}>
            <TouchableOpacity
              style={[styles.chartLabelRow, styles.withBorderBottom]}
              onPress={this.toggleExpand}>
              <View style={styles.ChartLabels}>
                <Text style={commonStyles.H4}>POIDS</Text>
                <Text style={styles.lienChartValue}>{'   '}-1.5kg *</Text>
              </View>
              <Text style={styles.ChartLabelValue}>
                50,5 <Text style={styles.lienChartUnit}>kg</Text>
              </Text>
            </TouchableOpacity>
            <Text style={[styles.lineChartInfo, styles.mb20]}>
              *Depuis le début du programme
            </Text>
            <View
              style={{
                height: 250,
                paddingHorizontal: 10,
                flexDirection: 'row',
              }}>
              <YAxis
                data={lineData}
                style={{marginBottom: xAxisHeight}}
                contentInset={verticalContentInset}
                svg={axesSvg}
              />
              <View style={{flex: 1, marginLeft: 10}}>
                <LineChart
                  style={{flex: 1}}
                  data={lineData}
                  contentInset={verticalContentInset}
                  svg={{stroke: 'black'}}>
                  <Grid ticks={[10, 30, 50, 70, 90, 110, 130]} />
                  <Decorator />
                </LineChart>
                {/* <XAxis
                  style={{
                    marginHorizontal: -20,
                    height: xAxisHeight,
                  }}
                  data={lineData}
                  formatLabel={(value, index) => {
                    const values = [
                      '12/07',
                      '14/07',
                      '18/07',
                      '20/07',
                      '22/07',
                    ];
                    return values[index];
                  }}
                  contentInset={{left: 40, right: 40}}
                  svg={axesSvg}
                /> */}
              </View>
            </View>
            <PrimaryButton
              title="Mettre à jour"
              style={styles.mettreBtn}
              onPress={this.props.handleSelectPoid}
            />
          </View>
        )}
      </>
    );
  }
}
