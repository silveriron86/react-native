import {StyleSheet, Dimensions} from 'react-native';
import {Colors} from '../../../../constants';

export default StyleSheet.create({
  mb20: {
    marginBottom: 20,
  },
  qFollowUp: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    padding: 15,
  },
  qFollowUpText: {
    fontSize: 18,
    textAlign: 'center',
  },
  stepRow: {
    marginVertical: 20,
  },
  stepText: {
    fontSize: 18,
    marginBottom: 5,
  },
  mealFills: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    paddingHorizontal: 10,
    // borderWidth: 1,
  },
  mealFillBtn: {
    backgroundColor: Colors.theme.LIGHT,
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#999',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.48,
    shadowRadius: 4,
    elevation: 1,
  },
  selectedMealFill: {
    backgroundColor: Colors.theme.MAIN,
  },
  mealFillBtnText: {
    fontFamily: 'Poppins-Medium',
    color: 'black',
    fontSize: 21,
  },
  plusView: {
    width: 22,
    height: 22,
    marginBottom: 5,
  },
  plusText: {
    fontSize: 24,
    lineHeight: 24,
    textAlign: 'center',
  },
  selectedMeal: {
    marginTop: 2,
  },
  selectedMealText: {
    fontSize: 15,
    marginTop: 3,
    color: 'red',
  },
  chartTitle: {
    marginTop: 30,
    marginBottom: 15,
    textAlign: 'left',
    fontFamily: 'AmericanaStd',
  },
  marginRow: {
    marginTop: 20,
    paddingBottom: 0,
  },
  chatArea: {
    padding: 24,
    backgroundColor: Colors.CHART,
    borderRadius: 15,
  },
  chartSubTitle: {
    marginTop: 10,
    marginBottom: 20,
    fontFamily: 'Poppins-Medium',
  },
  chartLabelRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15,
  },
  chartColorCircle: {
    width: 16,
    height: 16,
    borderRadius: 8,
    marginTop: 3,
    marginLeft: 7,
    marginRight: 12,
  },
  ChartLabels: {
    flexDirection: 'row',
  },
  ChartLabelTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: 'black',
  },
  ChartLabelValue: {
    fontFamily: 'AmericanaStd',
    fontSize: 22,
  },
  guageInfo: {
    position: 'absolute',
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
    left: Dimensions.get('window').width / 2 - 75,
    marginTop: 190,
  },
  guageValue: {
    fontSize: 34,
    marginBottom: 5,
    fontFamily: 'AmericanaStd',
  },
  mark: {
    backgroundColor: Colors.theme.DARK,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    height: 30,
    marginLeft: 10,
    marginTop: -2,
  },
  yellowBg: {
    backgroundColor: Colors.YELLOW,
  },
  lineChartInfo: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: Colors.GREY,
  },
  withBorderBottom: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    paddingBottom: 15,
    marginBottom: 20,
  },
  lienChartValue: {
    color: Colors.LIGHT_GREEN,
    fontFamily: 'Poppins-SemiBold',
    fontSize: 12,
    marginTop: 3,
  },
  lienChartUnit: {
    fontSize: 13,
    fontWeight: 'normal',
  },
  colorsHozBar: {
    marginTop: 20,
    marginBottom: 15,
    flexDirection: 'row',
    paddingHorizontal: '2%',
  },
  colorHozCol: {
    height: 5,
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  colorsBar: {
    position: 'absolute',
    width: 30,
    marginTop: -18,
    height: '100%',
    paddingTop: 20,
    paddingBottom: 3,
  },
  colorLabelBar: {
    marginBottom: 30,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  colorLabel: {
    marginHorizontal: 5,
    fontSize: 12,
    textAlign: 'center',
  },
  centerColorCol: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderWidth: 3,
    borderColor: Colors.CHART,
    // position: 'absolute',
    // right: '25%',
  },
  colorDot: {
    width: 15,
    height: 15,
    borderRadius: 7.5,
    marginTop: 2,
  },
  imcChartWrapper: {
    height: 250,
    paddingLeft: 10,
    paddingRight: 5,
    flexDirection: 'row',
  },
  imcChartInfos: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    marginTop: 15,
    marginBottom: 25,
  },
  imcChartInfoCol: {
    flexDirection: 'row',
    width: 160,
    height: 30,
  },
  mettreBtn: {
    width: 130,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  obBlocksRow: {
    flexDirection: 'row',
    paddingHorizontal: 18,
    marginTop: 10,
  },
  obBlock: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 8,
    paddingVertical: 15,
  },
  obButtons: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  obIcon: {
    width: 35,
    height: 35,
  },
  obButton: {
    marginHorizontal: 8,
  },
  obChartIcons: {
    position: 'absolute',
    left: -40,
    top: 10,
    height: 228,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  obChartIconWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  obChartIcon: {
    width: 28,
    height: 28,
  },
});
