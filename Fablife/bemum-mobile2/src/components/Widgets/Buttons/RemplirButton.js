/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableOpacity, Text, View, Image} from 'react-native';
import commonStyles from '../../Layouts/Styles';
import styles from '../_styles';

const rightArrow = require('../../../../assets/icons/right_arrow.png');

export default class RemplirButton extends React.Component {
  render() {
    const {onPress, disabled} = this.props;
    return (
      <TouchableOpacity
        testID="button"
        disabled={disabled}
        onPress={onPress}
        style={[
          styles.button,
          {height: 70, marginTop: 20, borderRadius: 10},
          disabled === true && {backgroundColor: '#cdcdcd'},
        ]}>
        <View style={[commonStyles.flexCenter, {height: 70}]}>
          <Text style={[commonStyles.H4, {color: 'white', marginTop: 2}]}>
            Mettre à jour mes données de profil
          </Text>
          <Image
            source={rightArrow}
            style={[commonStyles.rightArrowIcon, {tintColor: 'white'}]}
          />
        </View>
      </TouchableOpacity>
    );
  }
}
