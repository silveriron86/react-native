import React from 'react';
import {TouchableOpacity, Image} from 'react-native';
import styles from '../../Layouts/Tabs/Alimentation/_styles';

const backIcon = require('../../../../assets/icons/back_icon.png');
export default class BackButton extends React.Component {
  goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    const {style} = this.props;
    return (
      <TouchableOpacity
        style={[styles.headerBackButton, style]}
        onPress={this.goBack}>
        <Image source={backIcon} style={styles.headerBackIcon} />
      </TouchableOpacity>
    );
  }
}
