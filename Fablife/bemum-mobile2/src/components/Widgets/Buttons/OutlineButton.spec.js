import React from 'react';
import OutlineButton from './OutlineButton';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<OutlineButton />', () => {
  it('should fire onPress events', () => {
    const onPress = jest.fn();
    const rendered = render(<OutlineButton onPress={onPress} />);
    const buttonComponent = rendered.getByTestId('button');

    fireEvent(buttonComponent, 'press');

    expect(onPress).toHaveBeenCalled();
  });
});
