import React from 'react';
import RemplirButton from './RemplirButton';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<RemplirButton />', () => {
  it('should fire onPress events', () => {
    const onPress = jest.fn();
    const rendered = render(<RemplirButton onPress={onPress} />);
    const buttonComponent = rendered.getByTestId('button');

    fireEvent(buttonComponent, 'press');

    expect(onPress).toHaveBeenCalled();
  });
});
