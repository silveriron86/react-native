import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import commonStyles from '../../Layouts/Styles';
import styles from '../_styles';
import {Colors} from '../../../constants';

export default class RedButton extends React.Component {
  render() {
    const {onPress, title, style} = this.props;
    return (
      <TouchableOpacity
        testID="button"
        onPress={onPress}
        style={[styles.redOutlineButton, style]}>
        <Text style={[commonStyles.Text, {color: Colors.LIGHT_RED}]}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  }
}
