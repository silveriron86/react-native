import React from 'react';
import RedButton from './RedButton';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<RedButton />', () => {
  it('should fire onPress events', () => {
    const onPress = jest.fn();
    const rendered = render(<RedButton onPress={onPress} />);
    const buttonComponent = rendered.getByTestId('button');

    fireEvent(buttonComponent, 'press');

    expect(onPress).toHaveBeenCalled();
  });
});
