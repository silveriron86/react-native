import React from 'react';
import {ScrollView, TouchableOpacity, Text, Image} from 'react-native';
import commonStyles from '../../Layouts/Styles';

const checkIcon = require('../../../../assets/icons/check.png');
const roundIcon = require('../../../../assets/icons/round.png');

export default class QuestionnairesList extends React.Component {
  render() {
    const {
      categories,
      InputtingSurveyIndex,
      onPress,
      containerStyle,
    } = this.props;

    return (
      <ScrollView contentContainerStyle={[containerStyle]}>
        {categories.map((category, index) => {
          const disabled = index > InputtingSurveyIndex;
          return (
            <TouchableOpacity
              testID={`category-${category.survey_id}`}
              disabled={disabled}
              style={[
                commonStyles.profilingCatRow,
                index < InputtingSurveyIndex && commonStyles.selectedCatRow,
                disabled && commonStyles.disabledRow,
              ]}
              key={`profiling-cat-${index}`}
              onPress={() =>
                onPress(category, /*index < InputtingSurveyIndex)*/ false)
              }>
              <Image
                source={index < InputtingSurveyIndex ? checkIcon : roundIcon}
                style={commonStyles.checkIcon}
              />
              <Text
                style={[
                  commonStyles.profilingCatText,
                  disabled && commonStyles.disabledText,
                ]}>
                {category.title}
              </Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    );
  }
}
