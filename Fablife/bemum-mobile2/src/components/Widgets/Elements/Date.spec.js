import React from 'react';
import moment from 'moment';
import Date from './Date';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<Date />', () => {
  it('should fire onChange events', () => {
    const onChange = jest.fn();
    const rendered = render(<Date value={true} onChange={onChange} />);
    const dateComponent = rendered.getByTestId('date');
    const now = moment('2021-03-11');
    fireEvent(dateComponent, 'onDateChange', now);
    expect(onChange).toHaveBeenCalledWith('2021-03-11T00:00:00.000');
  });
});
