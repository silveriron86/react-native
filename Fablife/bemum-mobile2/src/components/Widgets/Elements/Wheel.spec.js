import React from 'react';
import Wheel from './Wheel';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<Wheel />', () => {
  const config = {
    min: 0,
    max: 18,
    precision: 0.25,
  };
  const unit = {
    short_name: 'h',
  };

  it('should fire onSubmit events', () => {
    const onChange = jest.fn();
    const rendered = render(
      <Wheel value={3} config={config} unit={unit} onChange={onChange} />,
    );
    const wrapperComponent = rendered.getByTestId('wheel-wrapper');
    fireEvent(wrapperComponent.children[1], 'onSubmit', 4);
    expect(onChange).toHaveBeenCalledWith(4);
  });
});
