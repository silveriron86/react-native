/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Text,
  View,
  TextInput,
  Platform,
  TouchableWithoutFeedback,
} from 'react-native';
import styles from '../_styles';
import Utils from '../../../utils';
import commonStyles from '../../Layouts/Styles';

export default class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMsg: '',
      val: props.value !== null ? props.value.toString() : '',
    };
  }

  componentDidMount() {
    this.mount = true;
  }

  componentWillUnmount() {
    this.mount = false;
  }

  onChangeText = val => {
    if (!this.mount) {
      return;
    }
    const {type} = this.props;
    this.setState({val});
    this.props.onChange(
      type === 'number' && val !== '' ? parseFloat(val) : val,
    );
  };

  onBlur = () => {
    const {type} = this.props;
    if (type !== 'text') {
      let value = this.state.val;
      let errorMsg = '';
      if (value) {
        const min = this.props.min;
        const max = this.props.max;
        errorMsg = Utils.checkValueRange(min, max, value);
      }
      this.setState({errorMsg});
    }
  };

  onPress = () => {
    this.inputRef.focus();
  };

  render() {
    const inputAccessoryViewID = 'uniqueID';
    const {val, errorMsg} = this.state;
    const {
      required,
      isVisibleError,
      tooltip,
      readOnly,
      placeholder,
      label,
      suffix,
      type,
      max,
    } = this.props;

    return (
      <View style={styles.row}>
        <Text style={commonStyles.H4}>
          {label}
          {required && '*'}
        </Text>
        {tooltip && <Text style={styles.tooltipText}>{tooltip}</Text>}
        <TouchableWithoutFeedback
          onPress={this.onPress}
          style={[
            styles.inputWrapper,
            {
              borderWidth: 1,
              heigh: 45,
              minHeight: 45,
              borderRadius: 25,
              borderColor: '#515151',
              paddingHorizontal: 10,
              marginTop: 5,
              marginBottom: 25,
            },
          ]}>
          {/* <TextInput
            value={`${val}${suffix}`}
            // value={val}
            keyboardType="numeric"
            autoCorrect={false}
            style={styles.input}
            placeholder={placeholder}
            placeholderTextColor="#9B9B9B"
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            returnKeyType="done"
            inputAccessoryViewID={inputAccessoryViewID}
            onChangeText={this.onChangeText}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            editable={readOnly === true ? false : true}
          /> */}
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TextInput
              testID="input"
              ref={input => {
                this.inputRef = input;
              }}
              value={val}
              keyboardType={type === 'text' ? 'default' : 'numeric'}
              autoCorrect={false}
              style={[
                styles.input,
                {minWidth: 28, height: 45},
                {width: placeholder === '' ? '100%' : 'auto'},
              ]}
              allowFontScaling={false}
              placeholder={placeholder ? placeholder.replace(suffix, '') : ''}
              placeholderTextColor="#9B9B9B"
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              returnKeyType="done"
              inputAccessoryViewID={inputAccessoryViewID}
              onChangeText={this.onChangeText}
              maxLength={type === 'text' ? max : 100}
              onBlur={this.onBlur}
              editable={readOnly === true ? false : true}
            />
            <Text
              style={[
                commonStyles.TextSecondary,
                {
                  marginLeft: Platform.OS === 'ios' ? 5 : -4,
                  marginTop: Platform.OS === 'ios' ? 2 : 5,
                  color: '#BDBDBD',
                },
              ]}>
              {suffix}
            </Text>
          </View>
        </TouchableWithoutFeedback>
        {(errorMsg !== '' || isVisibleError === true) && (
          <Text style={styles.inputError}>{errorMsg}</Text>
        )}
      </View>
    );
  }
}
