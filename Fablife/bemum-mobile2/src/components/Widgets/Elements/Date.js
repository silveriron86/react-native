import React from 'react';
import {Text, View} from 'react-native';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import styles from '../_styles';
import commonStyles from '../../Layouts/Styles';

export default class Date extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: props.value
        ? moment(props.value).format('DD/MM/YYYY')
        : props.value,
    };
  }

  render() {
    const {
      type,
      label,
      required,
      // max,
      tooltip,
      readOnly,
      onChange,
    } = this.props;
    return (
      <View style={styles.row}>
        <Text style={commonStyles.H4}>
          {label}
          {required && '*'}
        </Text>
        {tooltip && <Text style={styles.tooltipText}>{tooltip}</Text>}
        <DatePicker
          testID="date"
          style={styles.date}
          date={this.state.date}
          mode="date"
          maxDate={moment()
            .subtract(18, 'years')
            .format('DD/MM/YYYY')}
          placeholder="00/00/0000"
          format="DD/MM/YYYY"
          confirmBtnText="Valider"
          cancelBtnText=""
          showIcon={false}
          customStyles={{
            dateInput: {
              borderWidth: 0,
              alignItems: 'flex-start',
            },
            dateText: {
              textAlign: 'left',
              color: type === 'followup' ? '#292A3E' : 'black',
              fontSize: 15,
            },
          }}
          onDateChange={date => {
            this.setState({date: date});
            onChange(
              `${moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD')}T00:00:00.000`,
            );
          }}
          disabled={readOnly === true ? true : false}
        />
      </View>
    );
  }
}
