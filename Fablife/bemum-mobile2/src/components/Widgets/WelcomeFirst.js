/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import AsyncStorage from '@react-native-community/async-storage';
const closeIcon = require('../../../assets/icons/close_x.png');
const welcomeIcon = require('../../../assets/icons/welcome1.png');
const isSmall =
  Dimensions.get('window').height < 670 || Platform.OS === 'android';

export default class WelcomeFirst extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('VISITED_WELCOME1', (_err, visited) => {
      if (visited !== 'true') {
        this.setState({
          isOpen: true,
        });
      }
    });
  }

  onCloseModal = () => {
    AsyncStorage.setItem('VISITED_WELCOME1', 'true');
    this.setState({
      isOpen: false,
    });
  };

  render() {
    const {isOpen} = this.state;
    return (
      <ModalWrapper
        style={{
          width: '90%',
          height: isSmall ? '90%' : '80%',
          borderRadius: 10,
        }}
        visible={isOpen}>
        <SafeAreaView
          style={{
            flex: 1,
            margin: 25,
            position: 'relative',
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={welcomeIcon}
              style={{
                height: 80,
                resizeMode: 'contain',
              }}
            />
            <Text
              style={{
                fontSize: isSmall ? 21 : 23,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 30,
                marginBottom: 20,
              }}>
              Bienvenue dans votre application BeMum
            </Text>
            <View style={{alignItems: 'flex-start'}}>
              <View style={{marginTop: 15}}>
                <Text style={{fontSize: isSmall ? 18 : 21}}>
                  Commençons par nos questions de personnalisation de votre
                  programme : elles nous permettront de déterminer votre besoin
                  nutritionnel précis, et de connaître vos habitudes et
                  préférences alimentaires.
                </Text>
              </View>
              <View style={{marginTop: 15}}>
                <Text style={{fontSize: isSmall ? 18 : 21}}>
                  Nous pourrons ainsi personnaliser au mieux votre
                  accompagnement.
                </Text>
              </View>
            </View>
          </View>

          <TouchableOpacity
            style={{
              position: 'absolute',
              right: -5,
              top: -5,
            }}
            onPress={this.onCloseModal}>
            <Image
              source={closeIcon}
              style={{
                width: 20,
                height: 20,
              }}
            />
          </TouchableOpacity>
        </SafeAreaView>
      </ModalWrapper>
    );
  }
}
