/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import ModalWrapper from 'react-native-modal-wrapper';
import AppIntroSlider from 'react-native-app-intro-slider';
import AsyncStorage from '@react-native-community/async-storage';
const closeIcon = require('../../../assets/icons/close_x.png');
const isSmall =
  Dimensions.get('window').height < 670 || Platform.OS === 'android';

const slides = [
  {
    key: 1,
    title: 'Découvrez votre programme alimentaire',
    texts: [
      'Grâce à vos réponses, votre premier programme alimentaire sera généré dans quelques instants.',
      'Vous pouvez commencer à réaliser vos recettes selon vos disponibilités.',
      'Votre nutritionniste vous donnera des conseils pratiques pour le mettre en place.',
    ],
    image: require('../../../assets/icons/welcome2.png'),
  },
  {
    key: 2,
    title: 'Mettez régulièrement à jour vos données',
    texts: [
      'Les variations de poids et d’activité physique impactent le besoin nutritionnel.',
      'Mettez à jour vos données afin que nous puissions vous proposer les recettes les plus adaptées à votre besoin.',
      'Vous pouvez les mettre à jour depuis les sections Progression et Profil.',
    ],
    image: require('../../../assets/icons/welcome3.png'),
  },
  {
    key: 3,
    title: 'Validez vos recettes pour générer votre liste de courses',
    texts: [
      'Pour chaque repas, indiquez la recette que vous souhaitez cuisiner pour ajouter ses ingrédients à votre liste de courses.',
      'Vous pouvez à tout moment choisir une autre option, votre liste se met à jour.',
      'Si vous avez programmé des collations, leurs ingrédients sont ajoutés automatiquement.',
    ],
    image: require('../../../assets/icons/welcome4.png'),
  },
];

export default class WelcomeIntro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      index: 0,
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('VISITED_WELCOME2', (_err, visited) => {
      if (visited !== 'true') {
        this.setState({
          isOpen: true,
        });
      }
    });
  }

  onCloseModal = () => {
    AsyncStorage.setItem('VISITED_WELCOME2', 'true');
    this.setState({
      isOpen: false,
    });
  };

  onSlideChange = index => {
    this.setState({
      index,
    });
  };

  _renderItem = ({item}) => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Image
          source={item.image}
          style={{
            height: 80,
            resizeMode: 'contain',
          }}
        />
        <Text
          style={{
            fontSize: isSmall ? 21 : 23,
            fontWeight: 'bold',
            textAlign: 'center',
            marginTop: 30,
            marginBottom: 20,
          }}>
          {item.title}
        </Text>
        <View style={{alignItems: 'flex-start'}}>
          {item.texts.map((text, i) => (
            <View key={`text-${item.key}-${i}`} style={{marginTop: 15}}>
              <Text
                style={{
                  fontSize: isSmall ? 18 : 21,
                }}>
                {text}
              </Text>
            </View>
          ))}
        </View>
      </View>
    );
  };

  render() {
    const {isOpen, index} = this.state;
    return (
      <ModalWrapper
        style={{
          width: '90%',
          height: isSmall ? '95%' : '80%',
          borderRadius: 10,
        }}
        onRequestClose={this.onCloseModal}
        shouldCloseOnOverlayPress={index === 2}
        visible={isOpen}>
        <SafeAreaView
          style={{
            flex: 1,
            margin: 25,
            // marginTop: 0,
            position: 'relative',
          }}>
          <AppIntroSlider
            data={slides}
            renderItem={this._renderItem}
            onSlideChange={this.onSlideChange}
            renderDoneButton={() => {
              return null;
            }}
            renderNextButton={() => {
              return null;
            }}
            dotStyle={{
              backgroundColor: '#e4dffe',
              marginTop: 40,
            }}
            activeDotStyle={{
              backgroundColor: '#a090f4',
              marginTop: 40,
            }}
          />

          {index === 2 && (
            <TouchableOpacity
              style={{
                position: 'absolute',
                right: -5,
                top: -5,
              }}
              onPress={this.onCloseModal}>
              <Image
                source={closeIcon}
                style={{
                  width: 20,
                  height: 20,
                }}
              />
            </TouchableOpacity>
          )}
        </SafeAreaView>
      </ModalWrapper>
    );
  }
}
