import React from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import styles from './_styles';
const closeIcon = require('../../../assets/icons/close.png');

export default class Tags extends React.Component {
  render() {
    const {data, onClose, style} = this.props;

    return (
      <View style={[styles.tagsWrapper, style]}>
        {data.length > 0 &&
          data.map((tag, index) => {
            return (
              <View style={styles.tagView}>
                <TouchableOpacity
                  style={styles.tagCloseBtn}
                  onPress={() => onClose(index)}>
                  <Image source={closeIcon} style={styles.tabCloseIcon} />
                </TouchableOpacity>
                <Text style={styles.tagText}>{tag}</Text>
              </View>
            );
          })}
      </View>
    );
  }
}
