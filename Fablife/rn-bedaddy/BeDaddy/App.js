import React, { Component } from 'react';
import SplashScreen from 'react-native-splash-screen'
import {
  AsyncStorage,
  Alert,
  Linking,
  Platform
} from 'react-native';

import {
  Provider
} from 'react-redux';

import AppNavigator from './src/routes';
import store from './src/store';
import Utils from './src/utils';
import firebase from 'react-native-firebase';
import EventEmitter from 'EventEmitter';
import {
  createAppContainer
} from 'react-navigation';

const AppContainer = createAppContainer(AppNavigator);
export default class App extends Component {
  async componentDidMount(){
    this.checkPermission();
    this.createNotificationListeners();

    Utils.trackEvent('Action', 'Open_App')
    setTimeout(function() {
      SplashScreen.hide();
    }, 500);
  }
  
  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
  }
  
  //3
  async getToken() {
    // console.log('---getToken');
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    // console.log(fcmToken);
    if (!fcmToken) {
      // console.log('get firebase token');
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // console.log(fcmToken);
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }else {
      // console.log('XXXXXXXX');
    }
  }
  
  //2
  async requestPermission() {
    // console.log('requestPermission');
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      // console.log('permission rejected');
      this.requestPermission();
    }
  }  

  componentWillMount() {
    this.eventEmitter = new EventEmitter();
  }

  //Remove listeners allocated in createNotificationListeners()
  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      console.log('-- on notification');
      console.log(notification);
      const { title, body, data } = notification;
      this.showAlert(title, body, data);
    });
  
    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      console.log('-- on onNotificationOpened');
      console.log(notificationOpen.notification);

      const { title, body, data } = notificationOpen.notification;
      this.procNotification(title, data);
    });

    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
      firebase.notifications().getBadge()
        .then( count => {
          count++
          firebase.notifications().setBadge(count)
       })
       .then(() => {
         console.log('Doing great')
       })
       .catch( error => {
         console.log('fail to count')
       })
    });    
  
    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const { title, body, data } = notificationOpen.notification;
        this.showAlert(title, body, data);
    }

    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log('onMessage');
      console.log(JSON.stringify(message));
      this.showAlert(message.data.title, message.data.body, message.data);
    });
  }
  
  showAlert(title, body, data) {   
    if(title) {
      Alert.alert(
        title, body,
        [
          { text: 'OK', onPress: () => {
              this.procNotification(title, data);
            }
          },
        ],
        { cancelable: false },
      );
    }else {
      this.procNotification(title, data);
    }
  }

  procNotification (title, data) {
    firebase.notifications().getBadge()
      .then( count => {
        if(count > 0)
          count--;
        firebase.notifications().setBadge(count);
    });

    console.log('procNotification');
    console.log(data.click_action) 
    if(data.click_action) {
      if(data.click_action == "REPORT") {
        //Http link : https://moncompte.bedaddy.fr (http://dev.moncompte.bedaddy.fr) )
        Linking.openURL('http://dev.moncompte.bedaddy.fr');
      }else {
        if(Platform.OS === 'android' && !title) {
          setTimeout(() => {
            console.log('emit');
            this.eventEmitter.emit('NOTIFICATION-OPENED', data);
          }, 2000);
        }else {
          this.eventEmitter.emit('NOTIFICATION-OPENED', data);
        }
      }
    }  
  }
  
  render() {
    return (
      <Provider store = {store}>
        <AppContainer screenProps={this.eventEmitter}/>
      </Provider>
    );
  }
}