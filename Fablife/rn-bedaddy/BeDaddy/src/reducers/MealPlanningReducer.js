import MealPlanningConstants from '../constants/MealPlanningConstants';
// import Immutable from 'immutable';

const initialState = {
  mealPlanningData: {},
  breakfastData: {},
  lunchData: {},
  dinnerData: {},
  lunchMenuData: {},
  dinnerMenuData: {},
  lunchGuidelinesData: {},
  dinnerGuidelinesData: {},
  shoppingListData: {},
  baseGroceriesData: {},
  mealPlanningComplianceData: null,
  foodSupplementsComplianceData: null,
  error: null,
  dataError: null
};

function MealPlanningReducer(state = initialState, action) {
  switch (action.type) {
    case MealPlanningConstants.GET_MEALPLANNING_DATA_SUCCESS:
      let ret = {
        dataError: null
      }
      if(action.response.type) {
        ret[`${action.response.type}Data`] =  action.response.data
      }else {
        ret.mealPlanningData = action.response.data
      }
      return Object.assign({}, state, ret); 

    case MealPlanningConstants.UPDATE_MEALPLANNING_DATA_SUCCESS:
      return Object.assign({}, state, {
        error: null,
        mealPlanningData: action.response
      });

    case MealPlanningConstants.GET_MEALPLANNING_DATA_ERROR:
      return Object.assign({}, state, {
        mealPlanningData: {},
        dataError: action.error
      }); 

    case MealPlanningConstants.GET_RECIPE_SUCCESS:
			return Object.assign({}, state, {
				recipeData: action.response,
				error: null
			}); 

		case MealPlanningConstants.GET_RECIPE_ERROR:
			return Object.assign({}, state, {
				recipeData: {},
				error: action.error
      }); 
      
    case MealPlanningConstants.GET_MENU_SUCCESS:
      let menuRet = {
        error: null
      }
      menuRet[`${action.response.type}MenuData`] =  action.response.data
      return Object.assign({}, state, menuRet); 

    case MealPlanningConstants.GET_MENU_ERROR:
      let errRet = {
        error: action.error
      }
      errRet[`${action.response.type}MenuData`] =  {}
      return Object.assign({}, state, errRet); 

    case MealPlanningConstants.GET_GUIDELINES_SUCCESS:
      let guidelinesRet = {
        error: null
      }
      guidelinesRet[`${action.response.type}GuidelinesData`] =  action.response.data
      return Object.assign({}, state, guidelinesRet); 

    case MealPlanningConstants.GET_GUIDELINES_ERROR:
      let errorRet = {
        error: action.error
      }
      errorRet[`${action.type}GuidelinesData`] =  {}
      return Object.assign({}, state, errorRet); 
      
    case MealPlanningConstants.GET_SHOPPING_LIST_SUCCESS:
			return Object.assign({}, state, {
				shoppingListData: action.response,
				error: null
			}); 

		case MealPlanningConstants.GET_SHOPPING_LIST_ERROR:
			return Object.assign({}, state, {
				shoppingListData: {},
				error: action.error
      }); 

    case MealPlanningConstants.GET_BASE_GROCERIES_SUCCESS:
			return Object.assign({}, state, {
				baseGroceriesData: action.response,
				error: null
			}); 

		case MealPlanningConstants.GET_BASE_GROCERIES_ERROR:
			return Object.assign({}, state, {
				baseGroceriesData: {},
				error: action.error
      });       

    case MealPlanningConstants.GET_WEEK_COMPLIANCE_SUCCESS:
      let wcsErrRet = {
        error: null
      }

      wcsErrRet[`${action.response.type}ComplianceData`] =  action.response.data
      return Object.assign({}, state, wcsErrRet);       

		case MealPlanningConstants.GET_WEEK_COMPLIANCE_ERROR:
			return Object.assign({}, state, {
        mealPlanningComplianceData: null,
        foodSupplementsComplianceData: null,
				error: action.error
      });   

    default:
      return state;
  }
}

export default MealPlanningReducer;
