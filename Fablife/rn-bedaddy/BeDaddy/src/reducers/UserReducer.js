import UserConstants from '../constants/UserConstants';
// import Immutable from 'immutable';

const initialState = {
	dashboardData: null,
	userDetailsData: null,
	error: null
};

function UserReducer(state = initialState, action) {
	switch (action.type) {
		case UserConstants.GET_DASHBOARD_SUCCESS:
			return Object.assign({}, state, {
				dashboardData: action.response,
				error: null
			}); 

		case UserConstants.GET_DASHBOARD_ERROR:
			return Object.assign({}, state, {
				dashboardData: {},
				error: action.error
			}); 

		case UserConstants.GET_USER_DETAILS_SUCCESS:
			return Object.assign({}, state, {
				userDetailsData: action.response,
				error: null
			}); 

		case UserConstants.GET_USER_DETAILS_ERROR:
			return Object.assign({}, state, {
				userDetailsData: {},
				error: action.error
			}); 

		default:
			return state;
	}
}

export default UserReducer;
