import AccountConstants from '../constants/AccountConstants';
// import Immutable from 'immutable';

const initialState = {
	userData: {},
	error: null
};

function AccountReducer(state = initialState, action) {
	switch (action.type) {
		case AccountConstants.LOGIN_SUCCESS:
			return Object.assign({}, state, {
				userData: action.response,
				error: null
			});
			
		case AccountConstants.LOGIN_ERROR:
			return Object.assign({}, state, {
				userData: {},
				error: action.error
			}); 

		default:
			return state;
	}
}

export default AccountReducer;
