import SupportMessagesConstants from '../constants/SupportMessagesConstants';
// import Immutable from 'immutable';

const initialState = {
	supportMessages: [],
	error: null
};

function SupportMessagesReducer(state = initialState, action) {
	switch (action.type) {
		case SupportMessagesConstants.GET_SUPPORT_MESSAGES_ERROR:
			return Object.assign({}, state, {
				supportMessages: {},
				error: action.error
			}); 

		case SupportMessagesConstants.GET_SUPPORT_MESSAGES_SUCCESS:
			return Object.assign({}, state, {
				supportMessages: action.response,
				error: null
			}); 		

		default:
			return state;
	}
}

export default SupportMessagesReducer;
