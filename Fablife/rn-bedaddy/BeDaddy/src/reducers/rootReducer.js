import {
    combineReducers
} from "redux";
import AccountReducer from "./AccountReducer";
import SurveyReducer from "./SurveyReducer";
import MealPlanningReducer from "./MealPlanningReducer";
import SupportMessagesReducer from "./SupportMessagesReducer";
import UserReducer from "./UserReducer"

export default combineReducers({
    AccountReducer,
    SurveyReducer,
    MealPlanningReducer,
    SupportMessagesReducer,
    UserReducer
})