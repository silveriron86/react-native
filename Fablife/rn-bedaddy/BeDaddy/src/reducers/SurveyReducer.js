import SurveyConstants from '../constants/SurveyConstants';
// import Immutable from 'immutable';

const initialState = {
	loadedSurveys: 0,
	InputingSurveyIndex: 0,

	signup_profileFormData: {},
	signup_healthFormData: {},
	signup_physical_activityFormData: {},
	signup_sleep_fatigueFormData: {},
	signup_stressFormData: {},
	signup_moral_wellbeingFormData: {},
	signup_memory_concentrationFormData: {},
	signup_oxydative_stressFormData: {},

	feedback_profileFormData: {},
	feedback_healthFormData: {},
	feedback_physical_activityFormData: {},
	feedback_sleep_fatigueFormData: {},
	feedback_stressFormData: {},
	feedback_moral_wellbeingFormData: {},
	feedback_memory_concentrationFormData: {},
	feedback_oxydative_stressFormData: {},

	preference_food_habitsFormData: {},
	'genetic-user_genetic-userFormData': {},
	postResult: {},
	signupSurveyData: {},
	preferenceSurveyData: {},
	feedbackSurveyData: {},
	noticeSurveyData: {},
	geneticSurveyData: {},
	searchData: {},
	error: null
};

function SurveyReducer(state = initialState, action) {
	switch (action.type) {
		case SurveyConstants.COMPLETED_SURVEY:
			return Object.assign({}, state, {
				InputingSurveyIndex: action.response,
				error: null
			})		

		case SurveyConstants.LOADED_SURVEY:
			return Object.assign({}, state, {
				loadedSurveys: action.response,
				error: null
			})

		case SurveyConstants.UPDATE_FORM_DATA:
			let res = {
				error: null
			}
			let formData = state[`${action.response.type}_${action.response.form_id}FormData`]			
			formData.questions.forEach((question, idx) => {
				if(question.question_id === action.response.question_id) {
					question.value = action.response.value
				}
			})
			res[`${action.response.type}_${action.response.form_id}FormData`] =  formData
			return Object.assign({}, state, res);

		case SurveyConstants.GET_FORM_SUCCESS:
			let ret = {
				error: null
			}

			// sets default value
			action.response.data.questions.forEach((q) => {
				if(q.value === null || action.response.type === 'feedback') {
					if(q.ui === "stepper") {
						q.value = '';
					}else if(q.ui === "yesno" || q.ui === "checkbox" || q.ui === "radio") {
						q.value = false
					}else {
						q.value = null;
					}
				}			
			})
			ret[`${action.response.type}_${action.response.id}FormData`] =  action.response.data
			let r = Object.assign({}, state, ret);
			return r;
			
		case SurveyConstants.GET_FORM_ERROR:
			return Object.assign({}, state, {
				formData: {},
				error: action.error
			}); 

		case SurveyConstants.POST_FORM_SUCCESS:
			return Object.assign({}, state, {
				postResult: action.response,
				error: null
			}); 

		case SurveyConstants.POST_FORM_ERROR:
			return Object.assign({}, state, {
				postResult: {},
				error: action.error
			}); 

		case SurveyConstants.GET_SEARCH_DATA_SUCCESS:
			return Object.assign({}, state, {
				searchData: action.response,
				error: null
			}); 

		case SurveyConstants.GET_SEARCH_DATA_ERROR:
			return Object.assign({}, state, {
				searchData: {},
				error: action.error
			}); 

		case SurveyConstants.GET_SURVEY_SUCCESS:
			let surveyRet = {
				error: null
			}		
			surveyRet[`${action.response.type}SurveyData`] =  action.response.data
			return Object.assign({}, state, surveyRet);

		case SurveyConstants.GET_SURVEY_ERROR:
			return Object.assign({}, state, {
				surveyResult: {},
				error: action.error
			}); 

		default:
			return state;
	}
}

export default SurveyReducer;
