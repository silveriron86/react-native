import React from 'react';
import {connect} from 'react-redux';
import { ActivityIndicator, NetInfo, AsyncStorage, Platform, StyleSheet, Text, View, Image, TouchableOpacity, TouchableWithoutFeedback, Keyboard, TextInput } from 'react-native';
import {AccountActions, SurveyActions} from '../../../actions';
import commonStyles from '../Categories/_styles';
import Utils from '../../../utils';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      email: '',
      password: '',
      hasError: false,
      message: '',
      loading: false,
      network: true
    }
  }

  handleConnectivityChange = (connection) => {
    this.setState({
      network: (connection.type === "none" || connection.type === "unknown") ? false : true
    })
  }

  componentWillMount = () => {
    NetInfo.addEventListener(
      'connectionChange',
      this.handleConnectivityChange
    )
  }

  onLostKey(){
    const {navigate} = this.props.navigation;
    navigate("Forgot");
  }

  _goPage = (page) => {
    const {navigate} = this.props.navigation;
    this.setState({
      loading: false
    },()=> {
      navigate(page)            
    })
  }  

  onPressLogin(){
    if(this.state.network === false) {
      this.setState({
        hasError: true,
        message: "Connexion impossible. Veuillez vérifier que vous disposez d’une connexion Internet suffisante."
      })
      return
    }

    if(!this.state.email) {
      this.setState({
        hasError: true,
        message: "Veuillez renseigner votre adresse email"
      })
      return;
    }
    if(!this.state.password) {
      this.setState({
        hasError: true,
        message: 'Veuillez renseigner votre mot de passe'
      })      
      return;
    }

    this.setState({
      loading: true,
      message: ''
    })
    this.props.login({
      data: {
        username: encodeURIComponent(this.state.email),
        password: this.state.password
      },    
      cb: (res) => {
        if(res.message) {
          this.setState({
            message: "Adresse email ou mot de passe incorrect",
            hasError: true,
            loading: false
          })
        } else {
          this.setState({
            message: '',
            hasError: false,
            loading: false
          })

          AsyncStorage.multiSet([
            ['accessToken', res.access_token.accessToken], 
            ['refreshToken', res.access_token.refreshToken],
            ['userId', res.access_token.userId],
            ['expired_at', res.access_token.expired_at]
          ], ()=> {
            // const {navigate} = this.props.navigation;
            // navigate("PrivacyPolicy");

            Utils.trackEvent('Action', 'Login_First')
            Utils.checkRoute(this)
          })
        }       
      }
    })
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.container}>
          {
            this.state.loading &&
            <View style={commonStyles.loading}>
              <ActivityIndicator size={'large'}/>
            </View>
          }

          <View style={styles.viewTop}>
            <Image resizeMode='stretch' source={require('../../../../assets/img/logo_white.png')} style={styles.imgLogo} />
          </View>
          <View style={styles.viewBottom}>
            <View style={styles.inputWrapper}>
              <TextInput
                style={styles.input}
                placeholder="Adresse email"
                textContentType="username"
                placeholderTextColor="#9B9B9B"
                underlineColorAndroid="#BFBFBF"
                autoCapitalize='none'
                onChangeText={(email) => this.setState({email})}
              />
            </View>
            <View style={styles.inputWrapper}>
              <TextInput
                style={styles.input}
                placeholder="Mot de passe"
                textContentType="password"
                placeholderTextColor="#9B9B9B"
                secureTextEntry={true}
                underlineColorAndroid="#BFBFBF"
                autoCapitalize='none'
                onChangeText={(password) => this.setState({password})}
              />
            </View>
            <View style={styles.buttonView}>
              <TouchableOpacity transparent  style={styles.btnLostkey} onPress={()=>this.onLostKey()}>
                <Text style={styles.titleLost}>Mot de passe oublié ?</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.msgWrapper}>
              {
                (this.state.hasError) ? 
                <Text style={styles.msg}>{this.state.message}</Text> 
                : null
              }
            </View>
            <View style={styles.btnWrapper}>
              <TouchableOpacity
                style={styles.button}
                underlayColor='#fff' 
                onPress={()=>this.onPressLogin()}>
                <Text style={styles.loginText}>Se connecter</Text>
              </TouchableOpacity>
            </View>                        
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.AccountReducer.userData,
    error: state.AccountReducer.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: (req) => dispatch(AccountActions.login(req.data, req.cb)),
    completedSurvey: (index) => dispatch(SurveyActions.completedSurvey(index)),
    getSurvey: (request) => dispatch(SurveyActions.getSurvey(request.type, request.cb))    
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292A3E',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewTop: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 105,
    marginBottom: 41
  },
  viewBottom: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    paddingHorizontal: 30,
  },
  imgLogo: {
    width: 91,
    height: 65
  },
  logoTitle: {
    fontFamily: 'ITCAvantGardePro-Bold',
    marginTop: 20,
    textAlign: 'center',
    fontSize: 20,
    color: "white",
    letterSpacing:0.0,
  },
  borderView: {
    flexDirection :'row',
    backgroundColor: '#1e1c2a',
    height: 1,
    width: '100%',
    marginTop: 10,
  },
  inputWrapper: {
    width: '100%'
  },
  input: {
    fontFamily: 'ITCAvantGardePro-Bold',
    color: 'white',
    fontSize: 15,
    paddingTop: 32,
    paddingLeft: 3,
    paddingBottom: 10,
    marginBottom: 10,
    borderBottomWidth: (Platform.OS === 'ios') ? 1 : 0,
    borderBottomColor: 'white'  
  },
  buttonView: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  btnLostkey: {
    flex: 0.6,
    justifyContent: 'center'
  },
  titleLost: {
    fontFamily: 'ITCAvantGardePro-Bold',
    color: '#DEFB47',
    fontSize: 13,
    alignSelf: 'flex-end',
    marginTop: 12
  },
  btnWrapper: {
    width: '100%',
    alignItems: 'center',
    marginTop: 10
  },
  button: {
    backgroundColor: "#DEFB47",
    width: 160,
    height: 47,
    borderColor: "transparent",
    borderRadius: 2,
    borderWidth: 0,
    alignItems: 'center',
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 4}
  },
  loginText: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: "#292A3E",
    fontSize: 16,
    marginTop: (Platform.OS === 'ios') ? 18 : 13
  },
  msgWrapper: {
    marginTop: 28,
    height: 39
  },
  msg: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: "#FC706F",
    fontSize: 13,
    lineHeight: 13,
    letterSpacing: 0.2
  }
});