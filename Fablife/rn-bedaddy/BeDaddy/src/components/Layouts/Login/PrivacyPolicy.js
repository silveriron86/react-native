import React from 'react';
import { AsyncStorage, StyleSheet, Text, View, TouchableOpacity, Dimensions, Image, Platform } from 'react-native';
import Pdf from 'react-native-pdf';
import commonStyles from '../Categories/_styles'

export default class PrivacyPolicy extends React.Component {
  onPressAccept = () => {
    const {navigate} = this.props.navigation;
    AsyncStorage.setItem('agreedPrivacy', 'true')
    navigate("CategoryList");
  }

  doClose = () => {
    this.props.navigation.goBack()
  }

  render() {
    let hasBack = (this.props.navigation.getParam('from') === 'Settings')

    return (
      <View style={styles.container}>
        <Text style={styles.title}>Conditions Générales d'utilisation</Text>
        {
          (hasBack === true) &&        
          <TouchableOpacity
            style={[commonStyles.closeButton, {marginTop: -2}]}
            underlayColor='#fff' 
            onPress={()=>this.doClose()}>
            <Image resizeMode='stretch' source={require('../../../../assets/img/ic/ArrowLeft.png')}/>
          </TouchableOpacity>
        }        
        {
          (Platform.OS === "ios") ?
          <Pdf source={require('../../../../assets/pdf/CGU.pdf')} style={styles.pdf}/>
          :
          <Pdf source={{uri:"bundle-assets://pdf/CGU.pdf"}} style={styles.pdf}/>
        }
        {
          (hasBack != true) &&
          <View style={styles.btnWrapper}>
            <TouchableOpacity
              style={styles.button}
              underlayColor='#fff' 
              onPress={()=>this.onPressAccept()}>
              <Text style={styles.loginText}>J'accepte</Text>
            </TouchableOpacity>
          </View>  
        }
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292A3E',
    alignItems: 'baseline',
    paddingLeft: 25,
    paddingRight: 25
  },
  title: {
    fontFamily: 'ITCAvantGardePro-Bold',
    color: '#DEFB47',
    fontSize: 22,
    width: '100%',
    marginTop: (Platform.OS === 'ios') ? 51 : 43,
    marginBottom: 20
  },  
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width - 50,
    marginBottom: 25
  },
  btnWrapper: {
    alignSelf: 'center',
    position: "absolute",
    bottom: 40
  },
  button: {
    backgroundColor: "#DEFB47",
    width: 160,
    height: 47,
    borderColor: "transparent",
    borderRadius: 2,
    borderWidth: 0,
    alignItems: 'center',
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 4}
  },
  loginText: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: "#292A3E",
    fontSize: 16,
    marginTop: (Platform.OS === 'ios') ? 18 : 13
  }  
});