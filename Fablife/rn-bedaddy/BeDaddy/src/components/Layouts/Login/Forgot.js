import React from 'react';
import {connect} from 'react-redux';
import { ActivityIndicator, Platform, StyleSheet, Image, Text, View, TouchableOpacity, TouchableWithoutFeedback, Keyboard, TextInput } from 'react-native';
import commonStyles from '../Categories/_styles';
import {AccountActions} from '../../../actions';

class Forgot extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      email: '',
      hasError: false,
      loading: false,
      message: ''
    }
  }

  goBack = () => {
    this.props.navigation.goBack()
  }

  onPressForgot = () => {
    if(this.state.email) {
      this.setState({
        loading: true,
        hasError: false,
        message: ''
      }, ()=> {
        this.props.forgot({
          email: this.state.email,
          cb: (res) => {
            console.log(res);
            this.setState({
              loading: false
            }, () => {
              if(res.success === true) {
                this.props.navigation.goBack();
              }else {
                this.setState({
                  hasError: true,
                  message: "Aucun compte n'est associé à cette adresse"
                })
              }
            })
          }
        })
      });
    }else {
      this.setState({
        hasError: true,
        message: 'Veuillez entrer une adresse email valide'
      })
    }
  }

  render() {
    let subTitle = "Réinitialiser votre mot de passe"
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.container}>
          {
            this.state.loading &&
            <View style={commonStyles.loading}>
              <ActivityIndicator size={'large'}/>
            </View>
          } 

          <Text style={styles.title}>Mot de passe oublié ?</Text>
          <TouchableOpacity
            style={styles.backButton}
            onPress={this.goBack}
          >
            <Image resizeMode='stretch' source={require('../../../../assets/img/back_button_icon.png')} style={styles.imgBack} />
          </TouchableOpacity>
          <View style={{flex: 1, width: '100%'}}>
            <View style={{flex: 1, width: '100%'}}>
              <Text style={styles.subTitle}>{subTitle.toUpperCase()}</Text>              
            </View>
            <View style={{flex: 1, width: '100%'}}>
              <Text style={styles.text}>Nous vous enverrons un email avec un lien pour réinitialiser votre mot de passe.</Text>
              <View style={styles.inputWrapper}>
                <TextInput
                  keyboardType='email-address'
                  style={styles.input}
                  placeholder="Adresse e-mail"
                  textContentType="emailAddress"
                  placeholderTextColor="#9B9B9B"
                  underlineColorAndroid="#BFBFBF"
                  autoCapitalize='none'
                  onChangeText={(email) => this.setState({email})}
                />
              </View>
              <View style={styles.msgWrapper}>
                {
                  (this.state.hasError) ? 
                  <Text style={styles.msg}>{this.state.message}</Text> 
                  : null
                }
              </View>
            </View>
          </View>
          <View style={{flex: 1, width: '100%', justifyContent: 'center'}}>
            <View style={styles.btnWrapper}>
              <TouchableOpacity
                style={styles.button}
                underlayColor='#fff' 
                onPress={()=>this.onPressForgot()}>
                <Text style={styles.loginText}>Envoyer</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    
  };
};

const mapDispatchToProps = dispatch => {
  return {
    forgot: (req) => dispatch(AccountActions.forgot(req.email, req.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Forgot);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292A3E',
    alignItems: 'baseline',
    paddingLeft: 30,
    paddingRight: 30
  },
  backButton: {
    position: "absolute",
    top: 51,
    right: 30
  },
  imgBack: {
    width: 12,
    height: 18
  },
  title: {
    fontFamily: 'ITCAvantGardePro-Bold',
    color: '#DEFB47',
    fontSize: 28,
    width: '100%',
    marginTop: (Platform.OS === 'ios') ? 51 : 43
  },
  subTitle: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: 'white',
    fontSize: 17,
    width: '100%'
  },
  text: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: 'white',
    fontSize: 15,
    width: '100%'
  },
  inputWrapper: {
    width: '100%'
  },
  input: {
    fontFamily: 'ITCAvantGardePro-Bold',
    color: 'white',
    fontSize: 15,
    paddingTop: 32,
    paddingLeft: 3,
    paddingBottom: 10,
    marginBottom: 10,
    borderBottomWidth: (Platform.OS === 'ios') ? 1 : 0,
    borderBottomColor: 'white'
  },
  msgWrapper: {
    marginTop: 18,
    height: 39
  },
  msg: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: "#FC706F",
    fontSize: 13,
    lineHeight: 13,
    letterSpacing: 0.2
  },
  btnWrapper: {
    width: '100%',
    alignItems: 'center'
  },
  button: {
    backgroundColor: "#DEFB47",
    width: 160,
    height: 47,
    borderColor: "transparent",
    borderRadius: 2,
    borderWidth: 0,
    alignItems: 'center',
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 4}
  },
  loginText: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: "#292A3E",
    fontSize: 16,
    marginTop: (Platform.OS === 'ios') ? 18 : 13
  }
});