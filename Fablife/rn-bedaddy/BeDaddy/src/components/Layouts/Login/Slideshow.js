import React, { Component } from 'react';
import {
  Button,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Platform
} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const slides = [
  {
    key: 'somethun',
    title: 'Bonjour',
    text: 'BeDaddy est votre programme de nutrition personnalisée qui agit à la fois sur votre alimentation et votre hygiène de vie avec un seul objectif: booster votre fertilité.',
    image: require('../../../../assets/img/slide1.png')
  },
  {
    key: 'somethun-dos',
    title: 'Un programme alimentaire hebdomadaire',
    text: 'Chaque semaine, vous aurez accès à des suggestions de repas personnalisées en fonction de vos préférences et vos besoins.',
    image: require('../../../../assets/img/slide2.png')
  },
  {
    key: 'somethun1',
    title: 'Un diététicien pour vous accompagner et avancer dans le programme',
    text: "Un coach fertilité vous accompagne durant tout le programme dans la mise en place d'une meilleure hygiène de vie et une alimentation en adéquation avec vos besoins.",
    image: require('../../../../assets/img/slide3.png')
  }
];

export default class Slideshow extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeSlide: 0
    }
  }

  onPressLogin = () => {
    const {navigate} = this.props.navigation;
    navigate("Login");
  }

  _renderItem ({item, index}) {
    return (
      <View style={styles.slide}>
        <View style={styles.imgWrapper}>
          <Image resizeMode='stretch' source={item.image} style={styles.img} />
        </View>
        <View style={styles.info}>
          <Text style={styles.title}>{item.title.toUpperCase()}</Text>
          <Text style={styles.description}>{item.text}</Text>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <Carousel
          ref={(c) => { this._carousel = c; }}
          data={slides}
          renderItem={this._renderItem}
          sliderWidth={viewportWidth}
          itemWidth={viewportWidth}
          onSnapToItem={(index) => this.setState({ activeSlide: index }) }
        />
        <Pagination
          dotsLength={slides.length}
          activeDotIndex={this.state.activeSlide}
          containerStyle={[styles.pagination, (Dimensions.get('window').width < 370) && {paddingVertical: 20}]}
          dotStyle={styles.dotStyle}
          inactiveDotStyle={styles.inactiveDotStyle}
          inactiveDotScale={1}
        />

        <View style={styles.btnWrapper}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.onPressLogin}
            underlayColor='#fff'>
            <Text style={styles.loginText}>Se connecter</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292A3E',
    alignItems: 'baseline'
  },
  slide: {
    flex: 1,
    paddingHorizontal: Dimensions.get('window').width >= 370 ? 30 : 15,
    paddingTop: 85
  },
  imgWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },
  info: {
    marginTop: 45,
    // flex: 1
  },
  img: {
    width: 180,
    height: 180
  },
  title: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: '#fff',
    fontSize: 18,
    lineHeight: 22,
    textAlign: 'center'
  },
  description: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: '#fff',
    fontSize: 15,
    letterSpacing: 0.2,
    marginTop: 13,
    textAlign: 'center'
  },
  btnWrapper: {
    height: Dimensions.get('window').width >= 370 ? 110 : 90,
    width: '100%',
    alignItems: 'center'
  },
  button: {
    backgroundColor: "#DEFB47",
    width: 160,
    height: 47,
    borderColor: "transparent",
    borderRadius: 2,
    borderWidth: 0,
    alignItems: 'center',
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 4}
  },
  loginText: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: "#292A3E",
    fontSize: 16,
    marginTop: (Platform.OS === 'ios') ? 18 : 13
  },
  dotStyle: {
    backgroundColor:'#DEFB47', 
    width: 8, 
    height: 8,
    borderRadius: 4, 
    marginHorizontal: 0
  },
  inactiveDotStyle: {
    backgroundColor: '#9B9B9B'
  },
  pagination: {
    width: '100%',
    alignItems: 'center'
  }
})
