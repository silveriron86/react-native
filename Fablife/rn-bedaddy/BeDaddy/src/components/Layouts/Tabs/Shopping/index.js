import React from 'react';
import { connect } from 'react-redux';
import { Share, ActivityIndicator, Text, View, TouchableOpacity, Image, ScrollView, Dimensions, Modal, Platform, AsyncStorage } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import moment from 'moment'
import { MealPlanningActions } from '../../../../actions';
import commonStyles from '../../Categories/_styles';
import styles from './_styles'
import Utils from '../../../../utils'
import DialogInput from '../../../Widgets/DialogInput'
import CircleImage from '../../../Widgets/Tabs/CircleImage';

class CheckRow extends React.Component { 
  render() {
    let data = this.props.data
    let shopped = data.shopped 

    let key = (data.qty) ? Object.keys(data.qty)[0] : ''
    let value = (data.qty) ? data.qty[key] : ''
    return (
      <TouchableOpacity onPress={()=>this.props.onCheck(data.id, !shopped)} disabled={(!data.img)}>
        <View style={styles.rowWrapper}>
          {
            (shopped === true) ?
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ON.png')} style={styles.checkImg}/> :
            <Image resizeMode='stretch' source={require('../../../../../assets/img/OFF.png')} style={styles.checkImg}/>
          }
          {data.img && <CircleImage uri={data.img} style={shopped ? {opacity: 0.39}: null}/>}
          <View style={[styles.rowTextWrapper, (!data.img) && {paddingTop: 15, marginLeft: 0}]}>
            <Text style={[styles.text, shopped ? {color: '#939395'}: null]}>{data.name} {value} {key}</Text>
          </View>
        </View>      
      </TouchableOpacity>
    )
  }
}

class Shopping extends React.Component {
  constructor(props) {
    super(props);

    moment.locale('fr')
    this.state = {
      loading: false, 
      dialogVisible: false,
      modalVisible: false,
      articles: [],
      dates: [moment().format('DD-MM-YYYY')]
    }
  }

  onGroceryShare = () => {
    let msg = 'Ma liste de courses BeDaddy:';
    let baseGroceries = this.props.baseGroceriesData;
    
    if(baseGroceries.length > 0) {
      baseGroceries.forEach((block, i) => {
        let sub_msg = '';
        block.ingredients.forEach((row, ii) => {
          sub_msg += '\n - ' + row.name;
        });

        msg += '\n' + block.category.toUpperCase() + sub_msg;
      })
    }    

    console.log(msg);
    Share.share({
      title: 'Nouveau message',
      message: msg
    });
  }

  onShare = () => {
    let msg = 'Ma liste de courses BeDaddy:';
    let shoppingList = this.props.shoppingListData;
    console.log(shoppingList);
    if(shoppingList.length > 0) {
      shoppingList.forEach((block, i) => {
        msg += '\n' + block.category.toUpperCase();
        block.ingredients.forEach((row, ii) => {
          msg += '\n - ' + row.name;
          let key = (row.qty) ? Object.keys(row.qty)[0] : '';
          let value = (row.qty) ? row.qty[key] : '';
          msg += ' ' + value + key;
        });
      });
    }

    console.log(msg);
    Share.share({
      title: 'Nouveau message',
      message: msg
    });
  };  

  openModal = () => {
    Utils.trackScreenView( 'Grocery_List')
    this.setState({
      loading: true
    },() => {
      this.props.getBaseGroceries({
        cb: (res)=> {
          this.setState({
            loading: false,
            modalVisible: true
          })
        }
      })
    })
  }

  closeModal = () => {
    this.setState({
      modalVisible: false
    })    
  }

  onAdd = () => {
    Utils.trackEvent('Action', 'Shopping_List_add')
    this.setState({
      dialogVisible: true
    })
  }

  onCheck = (id, selected) => {
    this.setState({
      loading: true
    },() => {
      this.props.postShoppingList({
        dates: this.state.dates,
        id: id,
        selected: selected,
        cb: (res)=> {
          // this.setState({
          //   loading: false
          // })
          this.loadData()
        }
      })
    })
  }

  emptyArticles = () => {
    AsyncStorage.removeItem('articles')
    this.setState({
      articles: []
    })
  }

  addArticle = (article) => {
    if(!article) {
      return
    }

    let articles = this.state.articles 
    articles.push(article)
    AsyncStorage.setItem('articles', JSON.stringify(articles))
    this.setState({
      articles: articles,
      dialogVisible: false
    })
  }

  componentWillMount = () => {
    AsyncStorage.getItem('articles', (err, articles) => {
      if(articles) {
        this.setState({
          articles: JSON.parse(articles)
        })
      }
    })    
  }

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillUnmount() {
    this.props.onRef(null)
  }  

  loadData = () => {
    this.setState({
      loading: true
    },() => {
      this.props.getShoppingList({
        dates: this.state.dates,
        cb: (res)=> {
          this.setState({
            loading: false
          })
        }
      })
    })
  }

  onSelectDate = (dt) => {
    let dates = this.state.dates
    let selectedDate = dt.format('DD-MM-YYYY')
    let pos = dates.indexOf(selectedDate)
    if(pos < 0) {
      dates.push(selectedDate)
    }else {
      dates.splice(pos, 1)
    }

    this.setState({
      dates: dates
    },() => {
      this.loadData()
    })
  }

  render() {
    let btnCols = []
    let shoppingList = this.props.shoppingListData
    let baseGroceries = this.props.baseGroceriesData
    let articles = this.state.articles

    for(let i=0; i< 9; i++) {
      let dt = moment().add(i, 'day')
      let dateText = `${dt.format('D')} ${Utils.capitalize(dt.format('MMMM'))}`
      let weekDayText = dt.format('dddd')
      let selected = (this.state.dates.indexOf(dt.format('DD-MM-YYYY')) >= 0)
      weekDayText = Utils.capitalize(weekDayText)

      if(i === 0) 
        weekDayText = 'Aujourd’hui'
      else if(i === 1)
        weekDayText = 'Demain'

      btnCols.push(
        <TouchableOpacity 
          key={`date-${i}`} 
          style={[styles.dateSelBtn, selected ? {backgroundColor: '#34E8A5'} : null, (i==8) && {marginRight: 45}]} 
          onPress={()=>this.onSelectDate(dt)}>
          <Text style={[styles.dateText, selected ? {color: '#292A3E'} : null, {fontFamily: 'ITCAvantGardePro-Md'}]}>{weekDayText}</Text>
          <Text style={[styles.dateText, selected ? {color: '#292A3E'} : null]}>{dateText}</Text>
        </TouchableOpacity>
      )
    }

    let allRows = []
    let groceryRows = []
      
    if(shoppingList.length > 0) {
      shoppingList.forEach((block, i) => {
        let checkRows = []
        block.ingredients.forEach((row, ii) => {
          checkRows.push(
            <CheckRow key={`check--${i}-${ii}`} data={row} onCheck={this.onCheck}/>
          )
        })

        allRows.push(
          <View key={`block-${i}`} style={styles.block}>
            <View style={styles.subTitle}>
              <Text style={styles.subTitleText}>{block.category.toUpperCase()}</Text>
            </View>
            {checkRows}
          </View>
        )
      });
    }

    if(articles.length > 0) {
      let checkRows = []
      articles.forEach((article, ii) => {
        checkRows.push(
          <CheckRow key={`article-${ii}`} data={{shopped: false, name: article}} onCheck={null}/>
        )
      })

      allRows.push(
        <View key={`article-block`} style={styles.block}>
          <View style={styles.subTitle}>
            <Text style={styles.subTitleText}>{`AUTRES`}</Text>
            <TouchableOpacity 
              style={{position: 'absolute', right: 0, marginTop: 3}}
              onPress={this.emptyArticles}>
              <Text style={[styles.text, {color: '#DEFB47'}]}>Vider</Text>
            </TouchableOpacity>
          </View>
          {checkRows}
        </View>
      )
    }

    if(baseGroceries.length > 0) {
      baseGroceries.forEach((block, i) => {
        let subRows = []
        block.ingredients.forEach((row, ii) => {
          subRows.push(
            <View key={`subrow-${i}-${ii}`} style={styles.rowWrapper}>
              <CircleImage uri={row.img}/>
              <View style={styles.rowTextWrapper}>
                <Text style={styles.text}>{row.name}</Text>
              </View>
            </View>  
          )
        })

        groceryRows.push(
          <View key={`grocery-row-${i}`} style={[styles.block, (i===baseGroceries.length-1) && {paddingBottom: 45}]}>
            <View style={styles.subTitle}>
              <Text style={styles.subTitleText}>{block.category.toUpperCase()}</Text>
            </View>
            {subRows}
          </View>
        )
      })
    }

    return (
      <View style={commonStyles.container}>              
        {
          this.state.loading &&
          <View style={commonStyles.loading}>
            <ActivityIndicator size={'large'}/>
          </View>
        }         

        <DialogInput 
          isDialogVisible={this.state.dialogVisible}
          title={"Ajounter un article"}
          hintInput ={"Nouvel article"}
          submitInput={ (inputText) => {this.addArticle(inputText)} }
          closeDialog={ ()=>{this.setState({dialogVisible:false})} }
          cancelText={"Annuler"}
          submitText={"Ajouter"}
          modalStyle={{backgroundColor: '#707070'}}
          dialogStyle={{backgroundColor: '#DFDFDF'}}>
        </DialogInput>      
        
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {}}>
          <View style={[styles.modalWrapper, {paddingBottom: 0}]}>
            <View style={{textDirction: 'row'}}>
              <Text style={styles.headerTitle}>Mon épicerie de base</Text>
              <TouchableOpacity
                style={styles.shareBtn}
                underlayColor='#fff' 
                onPress={()=>this.onGroceryShare()}>
                <Image style={styles.shareImg} resizeMode='cover' source={require('../../../../../assets/img/ic/Share.png')}/>
              </TouchableOpacity>               
              <TouchableOpacity
                style={styles.addBtn}
                underlayColor='#fff' 
                onPress={this.closeModal}
              >
                <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Close.png')}/>
              </TouchableOpacity>
            </View>
            <ScrollView>
              <Text style={[styles.text, {lineHeight: 19, marginTop: 5}]}>Voici une liste d’aliments de base, personnalisée en fonction de vos préférences alimentaires, que nous vous recommandons de toujours avoir dans votre cuisine.</Text>
              <View style={{marginTop: 28}}>
                {(groceryRows.length > 0) && groceryRows}    
              </View>
            </ScrollView>
          </View>
        </Modal>
        
        <View style={styles.containerWrapper}>
          <TouchableOpacity
            style={styles.shapeImg}
            underlayColor='#fff' 
            onPress={()=>this.openModal()}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/Shape.png')}/>
          </TouchableOpacity>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.headerTitle}>Panier de courses</Text>   
            <TouchableOpacity
              style={styles.shareBtn}
              underlayColor='#fff' 
              onPress={()=>this.onShare()}>
              <Image style={styles.shareImg} resizeMode='cover' source={require('../../../../../assets/img/ic/Share.png')}/>
            </TouchableOpacity> 
            <TouchableOpacity
              style={styles.addBtn}
              underlayColor='#fff' 
              onPress={()=>this.onAdd()}>
              <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Add.png')}/>
            </TouchableOpacity>
          </View>
          <ScrollView horizontal={true} style={styles.dateSelWrapper}>
            <View style={{flexDirection: 'row'}}>
              {btnCols}
            </View>
          </ScrollView>
        </View>
        {
            (this.props.error != null || (shoppingList.length  === 0 && articles.length === 0)) ? 
              <View style={styles.emptyView}>
                <Image resizeMode='stretch' source={require('../../../../../assets/img/PANIER.png')} style={styles.panierImg}/>
                <Text style={styles.emptyText}>{`Votre panier de courses est vide`.toUpperCase()}</Text>
                <Text style={styles.emptyDescription}>Choisissez vos repas afin de voir la liste de courses associée.{"\n\n"}Vous pouvez également ajouter vos propres courses en cliquant sur  +  en haut à droite.</Text>
              </View>
            :
            <ScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={[commonStyles.container, {paddingLeft: 24, paddingRight: 24}/*, (Platform.OS === "android") && {minHeight: Dimensions.get('window').height - 50}*/]}>
              <View style={{marginTop: 10}}>
                {allRows}
              </View>
            </ScrollView>
        }        
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    shoppingListData: state.MealPlanningReducer.shoppingListData,
    baseGroceriesData: state.MealPlanningReducer.baseGroceriesData,
    error: state.MealPlanningReducer.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postShoppingList: (request) => dispatch(MealPlanningActions.postShoppingList(request.dates, request.id, request.selected, request.cb)),
    getShoppingList: (request) => dispatch(MealPlanningActions.getShoppingList(request.dates, request.cb)),
    getBaseGroceries: (request) => dispatch(MealPlanningActions.getBaseGroceries(request.cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Shopping);