import { StyleSheet, Platform, Dimensions } from 'react-native';

let windowWidth = Dimensions.get('window').width
export default StyleSheet.create({
  containerWrapper: {
    padding: 24
  },
  shapeImg: {
    marginTop: 16,
    marginBottom: 5
  },
  headerTitle: {
    marginTop: (Platform.OS === 'ios') ? 8 : 0,
    fontFamily: 'ITCAvantGardePro-Bold',
    fontSize: (windowWidth <= 320) ? 24 : 28,
    color: '#DEFB47',
    width: windowWidth - 80
  },
  shareBtn: {
    position: 'absolute',
    right: (windowWidth > 320) ? 32 : 0,
    marginTop: (windowWidth > 320) ? 3 : -28,
    width: 28,
    height: 28,
    justifyContent: 'center',
    alignItems: 'center'
  },
  shareImg: {
    width: 21.4,
    height: 28    
  },
  addBtn: {
    position: 'absolute',
    right: 0,
    marginTop: (windowWidth > 320) ? 6 : 3
  },
  panierImg: {
    // marginTop: (windowWidth <= 320) ? 25: 70
  },  
  dateSelWrapper: {
    height: 40,
    width: windowWidth,
    marginLeft: -24,
    marginTop: 13,
    paddingLeft: 24
  },
  dateSelBtn: {
    backgroundColor: '#FFFFFF19',
    height: 40,
    paddingLeft: 11,
    paddingRight: 11,
    borderRadius: 4,
    paddingTop: (Platform.OS === 'ios') ? 9 : 6,
    marginRight: 8
  },
  dateText: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 11,
    color: '#B0B0B0',
    textAlign: 'center',
  },
  emptyView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  emptyText: {
    fontFamily: 'ITCAvantGardePro-Md',
    marginTop: (windowWidth <= 320) ? 25 : 50,
    fontSize: 19, 
    color: '#FC706F', 
    width: 248,
    textAlign: 'center'
  },
  emptyDescription: {
    fontFamily: 'ITCAvantGardePro-Bk',
    marginTop: 22,
    color: 'white',
    width: 295,
    fontSize: 15
  },
  rowTextWrapper: {
    justifyContent: 'center', 
    marginLeft: 15,
    flex: 1,
    paddingTop: (Platform.OS === 'ios') ? 6 : 1
  },
  text: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 15,
    // lineHeight: 19,
    letterSpacing: 0.2,
    color: 'white'
  },     
  block: {
    marginBottom: 15
  },
  rowWrapper: {
    flexDirection: 'row', 
    marginBottom: 16
  }, 
  subTitle: {
    borderBottomWidth: 2, 
    borderBottomColor: '#FC706F', 
    marginBottom: 16, 
    paddingBottom: (Platform.OS === 'ios') ? 1 : 4
  },
  subTitleText: {
    fontFamily: 'ITCAvantGardePro-Md',
    fontSize: 18,
    letterSpacing: 0,
    color: 'white'    
  },
  checkImg: {
    marginTop: 10,
    width: 20,
    height: 20,
    marginRight: 16
  },
  modalWrapper: {
    flex: 1,
    backgroundColor: '#292A3E',
    padding: 25,
    paddingTop: 48
  },
  dialogInputWrapper: {
    backgroundColor: 'white',
    borderColor: '#8E8E93',
    borderWidth: 0.5,
    height: 25,
    margin: 16,
    justifyContent: 'center',
    marginTop: 0
  },
  dialogInput: {
    lineHeight: 18,
    paddingLeft: 3,
    paddingRight: 3
  }
})