import React from 'react';
import { ActivityIndicator, Platform, Image, Text, View, TouchableOpacity, ScrollView, Alert, StyleSheet, Dimensions } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {connect} from 'react-redux';
import {SurveyActions} from '../../../../actions';
import Switch from '../../../Widgets/Switch';
import Radio from '../../../Widgets/Radio';
import Date from '../../../Widgets/Date';
import Input from '../../../Widgets/Input';
import Check from '../../../Widgets/Check';
import Wheel from '../../../Widgets/Wheel';
import Utils from '../../../../utils';
import commonStyles from '../../Categories/_styles';

console.disableYellowBox = true;
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      date: '',
      val: '',
      loading: false
    }
  }

  componentDidMount() {
    console.log('--- Suivi form');
    const {readOnly, type, id} = this.props;
    console.log(readOnly, type, id);
    if(readOnly !== true) {
      this.setState({
        loading: true
      },() => {
        this.props.getForm({
          type: type,
          id: id,
          cb: (form) => {
            this.setState({
              loading: false
            })
          }
        })
      })  
    }
  }

  close = () => {
    this.props.navigation.goBack()
  }

  changeFormData = (question_id, value) => {
    const { navigation, type } = this.props;
    const form_id = this.props.id;

    this.props.updateFormData({
      data: {
        type: type,
        form_id: form_id, 
        question_id: question_id, 
        value: value
      },
      cb: (res) => {
        this.forceUpdate()
      }
    })
  }

  onPressNext = () => {
    const { navigation, type } = this.props;
    const form_id = this.props.id;
    let requiredFields = 0
    let filledFields = 0
    let requiredField = ''
    let questions = this.props[`${type}_${form_id}FormData`].questions
    let data = this.props[`${type}_${form_id}FormData`].data
    let form = {}
    let notVaild = false

    questions.forEach((question) => {
      if(Utils.checkVisibleQuestion(data, questions, question) === true) {  
        form[question.question_id] = question.value
        if(question.ui !== "checkbox" && question.required === true) {
          requiredFields++
          if(question.value !== null) {
            filledFields++            
          }else {
            if(requiredField === '') {
              requiredField = question.label
            }
          }
        }
        
        if(question.ui === 'stepper') {
          if(question.ui_config.min > question.value || question.value > question.ui_config.max) {
            notVaild = true
          }
        }
      }
    })

    if(notVaild) {
      Alert.alert(
        'Erreur', 'Le formulaire contient des réponses invalides. Veuillez vérifier votre saisie.',
        [{text: 'OK'}],
        { cancelable: false }
      )      
      return;
    }

    if(filledFields === requiredFields) {
      this.setState({
        loading: true
      })
      this.props.postForm({
        data: {
          type: 'feedback',
          form: form,
          id: form_id
        },
        cb: (res)=> {
          this.setState({
            loading: false
          },()=> {
            if(res.status && res.status === 400) {
              setTimeout(() => {
                Alert.alert(
                  'Erreur', 'Le formulaire contient des réponses invalides. Veuillez vérifier votre saisie.',
                  [{text: 'OK'}],
                  { cancelable: false }
                )
              }, 100)
            }else {
              this.props.onClose(false)
            }
          })
        }
      })
    }else {
      Alert.alert(
        'Erreur', `${requiredField} renseignez le champs`,
        [{text: 'OK'}],
        { cancelable: false }
      )
    }
  }

  _renderButtonView = (itemColor) => {
    return (
      <View style={[commonStyles.content, (Platform.OS === 'android') && {paddingBottom: 25}]}>
        <View style={commonStyles.btnWrapper}>
          <TouchableOpacity
            disabled={false}
            style={[commonStyles.btn, {backgroundColor: '#292A3E'}]}
            underlayColor='#fff' 
            onPress={()=>this.onPressNext()}>
            <Text style={[commonStyles.btnText, {color: 'white'}]}>Enregistrer</Text>
          </TouchableOpacity>
        </View>          
      </View>         
    )
  }  

  render() {
    const { navigation, type } = this.props;
    const form_id = this.props.id;
    const itemColor = this.props.color
    const itemTitle = this.props.title;
    const readOnly = this.props.readOnly

    let rows = [];
    
    if(this.props[`${type}_${form_id}FormData`].questions) {     
      let questions = this.props[`${type}_${form_id}FormData`].questions
      let data = this.props[`${type}_${form_id}FormData`].data 
      questions.forEach((question, idx) => {
        if(Utils.checkVisibleQuestion(data, questions, question) === true) {
          if(!question.ui) {
            rows.push( 
              <View style={commonStyles.row} key={`question_${idx}`}>
                <Text style={[commonStyles.label, {color: '#292A3E'}]}>{question.label_question}</Text>
                {
                  question.tooltip &&
                  <Text style={commonStyles.tooltipText}>{question.tooltip}</Text>
                }                  
              </View>
            )
          }else if (question.ui === "datepicker") {
            rows.push( 
              <Date 
                key={`question_${idx}`}
                label={question.label_question} 
                max={question.ui_config.max} 
                value={question.value} 
                tooltip={question.tooltip}
                required={question.required}
                type={'followup'}
                readOnly={readOnly}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              /> 
            )
          }else if(question.ui === "stepper") {
            if((type === "signup" && question.value !== "") || (type === "feedback")) {
              rows.push( 
                <Input 
                  key={`question_${idx}`}
                  label={question.label_question} 
                  placeholder={`0 ${(question.unit) ? question.unit.short_name : ''}`} 
                  tooltip={question.tooltip}
                  min={question.ui_config.min}
                  max={question.ui_config.max}
                  precision={question.ui_config.precision}
                  required={question.required}
                  value={question.value}
                  suffix={(question.unit) ? ` ${question.unit.short_name}` : ''}
                  type={'followup'}
                  readOnly={readOnly}
                  onChange={(value)=>this.changeFormData(question.question_id, value)}
                /> 
              )
            }
          }else if(question.ui === "radio") {
            rows.push( 
              <Radio 
                key={`question_${idx}`}
                itemColor={itemColor} 
                label={question.label_question} 
                data={question.ui_config.choices}
                tooltip={question.tooltip}
                required={question.required}
                value={question.value}
                type={'followup'}
                readOnly={readOnly}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "yesno") {
            rows.push( 
              <Switch 
                key={`question_${idx}`}
                itemColor={itemColor} 
                label={question.label_question} 
                data={question.ui_config.choices}
                tooltip={question.tooltip}
                required={question.required}
                value={question.value}
                type={'followup'}
                readOnly={readOnly}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "checkbox") {
            rows.push( 
              <Check
                key={`question_${idx}`}
                id={question.question_id}
                label={question.label_question}
                value={question.value}
                itemColor={itemColor}
                type={'followup'}
                readOnly={readOnly}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "wheel") {
            rows.push(
              <Wheel 
                key={`question_${idx}`}
                label={question.label_question}
                placeholder={`0 ${(question.unit) ? question.unit.short_name : ''}`} 
                tooltip={question.tooltip}
                config={question.ui_config}
                unit={question.unit}
                itemColor={itemColor}
                required={question.required}
                value={question.value}
                type={'followup'}
                readOnly={readOnly}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else {
            console.log('unkonw', question)
          }
        }
      })
    }

    const inputAccessoryViewID = "uniqueID";
    return (
      <View style={[styles.container]}>
        {
          (rows.length === 0 || this.state.loading) &&
          <View style={commonStyles.loading}>
            <ActivityIndicator size={'large'} color="#000000"/>
          </View>
        } 

        <View style={[styles.header, {backgroundColor: itemColor}]}>
          <View style={{justifyContent: 'center', flex: 1}}>
            <Text style={styles.headerTitle}>{itemTitle}</Text>
          </View>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={()=>this.props.onClose(true)}
          >
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Close.png')} style={commonStyles.imgClose} />
          </TouchableOpacity>
        </View>
        <KeyboardAwareScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={{paddingTop: 10}} enableResetScrollToCoords={false}>
          {rows}
          {
            (rows.length > 0 && this.state.loading === false) && (readOnly === false) &&
            <View>
              { this._renderButtonView(itemColor) }
            </View>        
          }        

          {
            (readOnly === true) &&
            <View style={{height: (Platform.OS === 'ios') ? 45 : 70, flex: 1}}></View>
          }
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    InputingSurveyIndex: state.SurveyReducer.InputingSurveyIndex,
    feedback_profileFormData: state.SurveyReducer.feedback_profileFormData,
    feedback_healthFormData: state.SurveyReducer.feedback_healthFormData,
    feedback_physical_activityFormData: state.SurveyReducer.feedback_physical_activityFormData,
    feedback_sleep_fatigueFormData: state.SurveyReducer.feedback_sleep_fatigueFormData,
    feedback_stressFormData: state.SurveyReducer.feedback_stressFormData,
    feedback_moral_wellbeingFormData: state.SurveyReducer.feedback_moral_wellbeingFormData,
    feedback_memory_concentrationFormData: state.SurveyReducer.feedback_memory_concentrationFormData,
    feedback_oxydative_stressFormData: state.SurveyReducer.feedback_oxydative_stressFormData,

    signup_profileFormData: state.SurveyReducer.signup_profileFormData,
    signup_healthFormData: state.SurveyReducer.signup_healthFormData,
    signup_physical_activityFormData: state.SurveyReducer.signup_physical_activityFormData,
    signup_sleep_fatigueFormData: state.SurveyReducer.signup_sleep_fatigueFormData,
    signup_stressFormData: state.SurveyReducer.signup_stressFormData,
    signup_moral_wellbeingFormData: state.SurveyReducer.signup_moral_wellbeingFormData,
    signup_memory_concentrationFormData: state.SurveyReducer.signup_memory_concentrationFormData,
    signup_oxydative_stressFormData: state.SurveyReducer.signup_oxydative_stressFormData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    completedSurvey: (index) => dispatch(SurveyActions.completedSurvey(index)),
    updateFormData: (request) => dispatch(SurveyActions.updateFormData(request.data, request.cb)),
    getForm: (request) => dispatch(SurveyActions.getForm(request.type, request.id, request.cb)),
    postForm: (request) => dispatch(SurveyActions.postForm(request.data, request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },  
  header: {
    flexDirection: 'row',
    minHeight: 48,
    justifyContent: 'space-between',
    paddingLeft: 24,
    paddingRight: 20
  },
  headerTitle: {
    fontFamily: 'ITCAvantGardePro-Bold',
    fontSize: 17,
    paddingTop: (Platform.OS === 'ios') ? 19 : 11,
    paddingBottom: 10,
    color: '#292A3E'
  },
  closeButton: {
    justifyContent: 'center'
  }
})