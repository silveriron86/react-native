import React from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator, Image, Text, View, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import commonStyles from '../../Categories/_styles';
import { AnimatedGaugeProgress, GaugeProgress } from 'react-native-simple-gauge';
import SurveyConstants from '../../../../constants/SurveyConstants';
import { SurveyActions } from '../../../../actions';
import Utils from '../../../../utils'
import styles from './_styles'
import Form from './Form'

class Followup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      modalVisible: false,
      form: 0,
      color: '',
      title: ''
    }
  }

  _hasData = () => {
    return (this.props.feedbackSurveyData && Object.keys(this.props.feedbackSurveyData).length > 0)
  }

  onPressLink = (category, i) => {
    this.scroll.scrollTo({x: 0, y: 0, animated: true});

    // let data = this.props[`${category.id}FormData`]  
    // if(data.questions) {
      this.setState({
        modalVisible: true,
        form: category.id,
        color: SurveyConstants.CATEGORIES[i].color,
        title: category.label
      })
    // }
  }

  closeModal = (onlyClose) => {
    if(onlyClose === true) {
      this.setState({
        modalVisible: false
      })
    }else {
      this.props.getSurvey({
        type: 'feedback',
        cb: (res) => {
          this.setState({
            modalVisible: false
          })
        }
      })  
    }
  }

  render() {
    const feedbackSurveyData = this.props.feedbackSurveyData
    let oneCompletedPercent = 100 / 8
    let totalCompletion = 0
    let completed = 0
    let angle = 0

    let rows = []

    if(this._hasData()) {
      completion = feedbackSurveyData.completion
      totalCompletion = completion
      angle = 360 / feedbackSurveyData.forms.length
      oneCompletedPercent = 100 / feedbackSurveyData.forms.length

      completed = parseInt(completion / oneCompletedPercent, 10)
      if(completed > 0) {
        completion -= completed * oneCompletedPercent
        completion = completion / oneCompletedPercent * 100
      }

      feedbackSurveyData.forms.forEach((category, i) => { 
        console.log(category, i);
        let filledPercent = 0

        if(category.completion === 100) {
          filledPercent = 1
        }else {
          let data = this.props[`${category.form_id}FormData`]
  
          if(data && data.questions) {
            let requiredFields = 0
            let filledFields = 0
  
            data.questions.forEach((question) => {
              if(Utils.checkVisibleQuestion(data.data, data.questions, question) === true) {  
                if(question.ui !== "checkbox" && question.required === true) {
                  requiredFields++
                  if(question.value !== null) {
                    filledFields++
                  }
                }
              }
            })
            filledPercent = filledFields / requiredFields
          }
        }

        rows.push( 
          <TouchableOpacity
            key={`category-${i}`}
            style={[styles.button, styles.btnGray]}
            underlayColor='#fff' 
            onPress={()=>this.onPressLink(category, i)}
            disabled={(category.completion === 100)}
          >
            <View style={styles.btnWrapper}>
              <View style={styles.leftWrapper}>
                <View style={[styles.active, {backgroundColor: SurveyConstants.CATEGORIES[i].color, width: (Dimensions.get('window').width - 48) * filledPercent}]}></View>
                <View style={styles.iconWrapper}>
                  <Image resizeMode='stretch' source={SurveyConstants.CATEGORIES[i].icon} style={styles.imgIcon} />
                </View>                  
                <Text style={styles.btnText}>{category.label}</Text>
              </View>            
              <View style={styles.arrowWrapper}>
                <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowDown.png')} style={styles.imgArrow} />
              </View>
            </View>
          </TouchableOpacity>
        )
      })
    }
    
    return (
      <View style={commonStyles.container}>
        <View style={[commonStyles.header, {backgroundColor: 'transparent'}]}>
          <Text style={[commonStyles.headerTitle, {color: '#DEFB47'}]}>Suivi</Text>
          {
            (this._hasData() === false) && 
            <View style={{height: 200, justifyContent: 'center'}}>
              <ActivityIndicator size={'large'}/>
            </View>            
          }
          {
            this._hasData() && (totalCompletion < 100) &&
            <View>
              <View style={{margin: 24, marginTop: 10, position: 'relative'}}>
                {
                  feedbackSurveyData.forms.map((category, idx) =>
                    <AnimatedGaugeProgress
                      key={`piece-${idx}`}
                      size={94}
                      width={8}
                      fill={(idx > completed) ? 0 : (idx < completed) ? 100 : completion}
                      rotation={113 + idx * angle}
                      cropDegree={315}
                      tintColor={SurveyConstants.CATEGORIES[idx].color}
                      delay={0}
                      backgroundColor="#939395"
                      style={{position: 'absolute', top: 0}}>                  
                    </AnimatedGaugeProgress>
                  )
                }        
              </View>
              {
                Array.apply(null, {length: feedbackSurveyData.forms.length}).map((e, i) =>
                  <View key={`bar-${i}`} style={[styles.dividBar,{ transform: [{rotate: `${i * angle}deg`}]}]}>
                    <View style={{height: 67.5, backgroundColor: '#292A3E'}}></View>
                    <View style={{height: 67.5, backgroundColor: 'transparent'}}></View>
                  </View>
                )
              }
              <View style={styles.percentText}>
                <Text style={commonStyles.h2}>{totalCompletion}%</Text>
              </View>
              <View style={{marginLeft: 132, marginTop: -26, Width: Dimensions.get('window').width - 156, minHeight: 100, justifyContent: 'center'}}>
                <Text style={commonStyles.text}>{feedbackSurveyData.description}</Text>
              </View>
            </View>
          }
        </View>

        {
          this._hasData() && (totalCompletion < 100) &&
          <ScrollView 
            ref={(c) => {this.scroll = c}}
            keyboardDismissMode="on-drag" 
            keyboardShouldPersistTaps='always' 
            style={commonStyles.container} 
            scrollEnabled={!this.state.modalVisible}>
            <View style={{flex: 1, marginTop: 25, marginLeft: 24, marginRight: 24, marginBottom: 14}}>
              {rows}
            </View>
          </ScrollView>
        }

        {
          this._hasData() && (totalCompletion === 100) &&
          <View style={styles.final}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/Merci.png')}/>
            <View style={{marginTop: 27, width: '100%'}}>
              <Text style={[commonStyles.headline, {textAlign: 'center'}]}>Merci!</Text>
              <Text style={[commonStyles.text, {textAlign: 'center'}]}>Toutes vos données sont à jour.</Text>
            </View>
          </View>
        }
        
        { 
          this.state.modalVisible &&
          <View style={styles.modalWrapper}>
            <Form id={this.state.form} type={'feedback'} color={this.state.color} title={this.state.title} onClose={this.closeModal} readOnly={false}></Form>
          </View>
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profileFormData: state.SurveyReducer.feedback_profileFormData,
    healthFormData: state.SurveyReducer.feedback_healthFormData,
    physical_activityFormData: state.SurveyReducer.feedback_physical_activityFormData,
    sleep_fatigueFormData: state.SurveyReducer.feedback_sleep_fatigueFormData,
    stressFormData: state.SurveyReducer.feedback_stressFormData,
    moral_wellbeingFormData: state.SurveyReducer.feedback_moral_wellbeingFormData,
    memory_concentrationFormData: state.SurveyReducer.feedback_memory_concentrationFormData,
    oxydative_stressFormData: state.SurveyReducer.feedback_oxydative_stressFormData,      
    feedbackSurveyData: state.SurveyReducer.feedbackSurveyData,
    error: state.SurveyReducer.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getForm: (request) => dispatch(SurveyActions.getForm(request.type, request.id, request.cb)),
    getSurvey: (request) => dispatch(SurveyActions.getSurvey(request.type, request.cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Followup);