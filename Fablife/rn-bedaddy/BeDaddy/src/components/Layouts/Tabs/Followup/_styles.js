import { StyleSheet, Dimensions, Platform } from 'react-native';

let windowWidth = Dimensions.get('window').width
export default StyleSheet.create({
	dividBar: {
		position: 'absolute', 
		top: 0, 
		left: 67, 
		width: 9, 
		height: 115,
		flex: 1,
		flexDirection: 'column'
	},
	percentText: {
		position: 'absolute',
		top: 10, 
		left: 24,
		width: 94,
		height: 94,
		paddingTop: (Platform.OS === 'ios') ? 12 : 4,
		justifyContent: 'center'
	},
	final: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		// marginTop: 105
	},
	button: {
		width: "100%",
		minHeight: 48,
		borderColor: "transparent",
		borderRadius: 2,
		borderWidth: 0,
		shadowColor: 'black',
		shadowOpacity: 0.2,
		shadowOffset: {width: 2, height: 4},
		marginBottom: 8
	},  
	btnGray: {
		backgroundColor: "#515262"
	},
	btnWrapper: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	btnText: {
		fontFamily: 'ITCAvantGardePro-Bold',
		color: '#292A3E',
		fontSize: (windowWidth <= 320) ? 16 : 17,
		lineHeight: 15,
		paddingTop: 19,
		paddingBottom: 10,
		width: Dimensions.get('window').width - 117 - 36 - 8
	},
	leftWrapper: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-start'
	},
	iconWrapper: {
		height: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		width: 67
	},
	imgIcon: {
		width: 35,
		height: 35
	},
	arrowWrapper: {
		height: '100%',
		alignItems: 'flex-end',
		justifyContent: 'flex-end'    
	},
	imgArrow: {
		width: 28,
		height: 28,
		marginRight: 8,
		marginBottom: 9
	},
	active: {
		height: '100%',
		position: 'absolute'
	},
	modalWrapper: {
		backgroundColor: 'white',
		position: 'absolute',
		left: 24,
		top: 86,
		width: Dimensions.get('window').width - 48,
		height: Dimensions.get('window').height - 130,
		zIndex: 9999
	}
})