import React from 'react';
import { StyleSheet, Platform, Text, View, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import commonStyles from '../../Categories/_styles';

export default class Cook extends React.Component {
  close = () => {
    this.props.navigation.goBack()
  }  

  render() {
    return (
      <KeyboardAwareScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={commonStyles.container}>              
        <View style={[commonStyles.header, {backgroundColor: 'transparent'}]}>
          <Text style={[commonStyles.headerTitle, {color: '#DEFB47'}]}>Cook</Text>

          <TouchableOpacity
            underlayColor='#fff' 
            onPress={()=>this.close()}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Close.png')}/>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    )
  }
}

const styles = StyleSheet.create({
 
});