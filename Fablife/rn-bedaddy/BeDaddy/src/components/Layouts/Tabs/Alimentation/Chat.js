import React from 'react';
import { connect } from 'react-redux';
import { Dimensions, Text, View, TouchableOpacity, Image, ScrollView, Modal, TextInput, Alert, Keyboard, KeyboardAvoidingView, Platform } from 'react-native';
import modalStyles from '../Shopping/_styles';
import styles from './_styles';
import { SupportMessagesActions } from '../../../../actions';

const CHAT_TIMER_INTERVAL = 10000
let chatTimer = null
class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      input: ''
    }
  }

  onChangeText = (input) => {
    this.setState({ input })
  }

  onViewLoaded = () => {
    this.props.getSupportMessages({
      cb: (supportMessages) => {
        if(supportMessages.length > 0) {
          this.scrollToEnd()
          
          supportMessages.forEach((msg, idx) => {
            if(msg.read_at === null) {
              this.props.markSupportMessage({
                id: msg.id,
                type: 'read',
                cb: () => {}
              })
            }
          })
        }
      }
    })
    
    chatTimer = setInterval(() => {
      this.reloadList()
    }, CHAT_TIMER_INTERVAL)
  }

  reloadList = () => {
    let currentMsgCnt = this.props.supportMessages.length
    this.props.getSupportMessages({
      cb: (supportMessages) => {
        if(supportMessages.length > currentMsgCnt) {
          // It means that server replied
          this.scrollToEnd()
        }
      }
    })
  }

  onSend = () => {
    if(!this.state.input) {
      Alert.alert(
        'Le texte ne doit pas être vide.', '',
        [{text: 'OK'}],
        { cancelable: false }
      )
      return
    }

    Keyboard.dismiss()
    this.props.postSupportMessage({
      text: this.state.input,
      cb: (res) => {
        this.setState({
          input: ''
        })

        if(!this.props.error) {
          this.props.getSupportMessages({
            cb: () => {
              this.scrollToEnd()
            }
          })
        }
      }
    })
  }

  scrollToEnd = () => {
    setTimeout(()=> {
      if(this.refs.scrollview) {
        this.refs.scrollview.scrollToEnd({animated: true})
      }
    }, 100)    
  }

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillUnmount() {
    this.props.onRef(null)
  }

  onClose = () => {
    clearInterval(chatTimer)
    this.props.onClose()
  }

  onFocus = () => {
    console.log('onFocus')
    this.refs.scrollview.scrollToEnd({animated: true})
  }

  onModalClose = () => {
    
  }

  render() {
    const inputAccessoryViewID = "CHAT_INPUT"
    let rows = []
    let supportMessages = [{
      from: 'support',
      text: 'Besoin d’aide?\nPosez-nous vos questions.'
    }]
    
    supportMessages = this.props.supportMessages.concat(supportMessages)

    for(i=supportMessages.length-1; i>=0; i--) {
      let msg = supportMessages[i]
      rows.push(
        <View key={`msg-${i}`} style={styles.chatRow}>
          <View style={(msg.from === 'support') ? styles.chatFromWrap : styles.chatToWrap}>
            <Text style={[styles.chatText, (msg.from === 'support') && styles.chartSupportText]}>{msg.text}</Text>
          </View>
          {
            (msg.from === 'support') ? 
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/fromTail.png')} style={styles.tailImg}/>
            : <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/toTail.png')} style={styles.toTailImg}/>
          }
        </View>
      )
    }

    return (                
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.visible}
        onRequestClose={this.onModalClose}>
        <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={(Platform.OS === 'ios') ? 0 : 25} enabled style={[modalStyles.modalWrapper, {paddingBottom: 0}]}>
          <View style={{flexDirection: 'row'}}>
            <Text style={modalStyles.headerTitle}>Assistance</Text>
            <TouchableOpacity
              style={modalStyles.addBtn}
              underlayColor='#fff' 
              onPress={this.onClose}
            >
              <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Close.png')}/>
            </TouchableOpacity>
          </View>

          <View style={{flex: 1, marginTop: 30}}>
            <View style={{height: Dimensions.get('window').height - ((Platform.OS === 'ios') ? 164 : 185)}}>
              <ScrollView ref="scrollview" keyboardDismissMode="on-drag" style={{flex: 1}}>
                {rows}
              </ScrollView>
            </View>
            <View style={styles.chatInputBoxContainer}>
              <View style={styles.chatInputBoxWrap}>
                <TextInput 
                  placeholder={'Message'}
                  placeholderTextColor={'#9B9B9B'}
                  style={styles.chatInput}
                  value={this.state.input}
                  autoCapitalize='none'
                  inputAccessoryViewID={inputAccessoryViewID}
                  onSubmitEditing={Keyboard.dismiss}
                  onChangeText={this.onChangeText}
                  onFocus={this.onFocus}
                />
              </View>
              <TouchableOpacity
                  style={styles.chatSendBtn}
                  underlayColor='#fff' 
                  onPress={this.onSend}
                >
                <Text style={styles.chatBtnTxt}>Envoyer</Text>
              </TouchableOpacity>            
            </View>   
          </View>       
        </KeyboardAvoidingView>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.SupportMessagesReducer.error,
    supportMessages: state.SupportMessagesReducer.supportMessages,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getSupportMessages: (request) => dispatch(SupportMessagesActions.getSupportMessages(request.cb)),
    postSupportMessage: (request) => dispatch(SupportMessagesActions.postSupportMessage(request.text, request.cb)),
    markSupportMessage: (request) => dispatch(SupportMessagesActions.markSupportMessage(request.id, request.type, request.cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);