import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ActivityIndicator, Text, View, TouchableOpacity, Image as Img, Dimensions, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import moment from 'moment';
import HTML from 'react-native-render-html';
import commonStyles from '../../Categories/_styles';
import mpStyles from './_styles';
import Info from '../../../Widgets/Tabs/Info'
import Nutrients from '../../../Widgets/Tabs/Nutrients'
import CircleImage from '../../../Widgets/Tabs/CircleImage'
import { MealPlanningActions } from '../../../../actions';

class Eatingoutside extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      guidelinesData: null,
      selected: false,
      scrolling: false
    }
  }

  componentWillMount = () => {
    const { navigation } = this.props; 
    const type = navigation.getParam('type');
    const date = navigation.getParam('date');
    this.setState({
      loading: true
    })
    this.props.getGuidelines({
      date: moment(date).format('DD-MM-YYYY'),
      type: type,
      cb: (res) => {
        console.log(res)
        this.setState({
          loading: false,
          guidelinesData: res
        })
      }
    })
  }

  doClose = () => {
    this.props.navigation.goBack()
  }  

  doPUT = () => {
    const { navigation } = this.props;    
    const type = navigation.getParam('type');
    const date = navigation.getParam('date');

    this.setState({
      loading: true
    })
    this.props.putData({
      date: moment(date).format('DD-MM-YYYY'),
      type: type,
      recipe_id: "guidelines",
      cb: (res) => {
        console.log(res)
        this.setState({
          loading: false,
          selected: true
        },() => {
          navigation.goBack()
          navigation.state.params.onGoBack()
        })        
      }
    })
  }

  handeScroll = (e) => {
    if(e.nativeEvent.contentOffset.y > 0) {
      this.setState({
        scrolling: true
      });
    }else {
      this.setState({
        scrolling: false
      });
    }
  }  

  render() {
    const { navigation } = this.props;    
    const recipe_name = navigation.getParam('recipe_name');
    const recipe_img_url = navigation.getParam('recipe_img_url');
    const selected_date = navigation.getParam('date');
    const type = navigation.getParam('type');
    const data = this.state.guidelinesData

    return (
    <View style={commonStyles.container}>
      <View style={[commonStyles.header, styles.header, this.state.scrolling && {backgroundColor: '#292A3E'}]}>
        <Text style={[commonStyles.headerTitle, styles.headerTitle]}>{recipe_name}</Text>
        {data && <Text style={[commonStyles.text, styles.subTitle]}>{`${moment(selected_date).format('D MMMM YYYY')} - ${(type === 'lunch') ? 'Déjeuner' : 'Dîner'}`.toUpperCase()}</Text>}
        <TouchableOpacity
          style={[commonStyles.closeButton, {marginTop: -2}]}
          underlayColor='#fff' 
          onPress={()=>this.doClose()}>
          <Img resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowLeft.png')}/>
        </TouchableOpacity>
      </View>      
        <KeyboardAwareScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={commonStyles.container} onScroll={this.handeScroll}>  
          {
            this.state.loading &&
            <View style={commonStyles.loading}>
              <ActivityIndicator size={'large'}/>
            </View>
          }   
          <Image
            source={{uri: recipe_img_url}}
            indicator={Progress.Bar}
            indicatorProps={{
              size: 45,
              borderWidth: 0,
              color: 'rgba(150, 150, 150, 1)',
              unfilledColor: 'rgba(200, 200, 200, 0.2)'
            }}
            style={styles.backgroundImg}/>
          
          <Img resizeMode='stretch' source={require('../../../../../assets/img/Gradient.png')} style={styles.gradient}/>
          <View style={(Platform.OS === "android") && {minHeight: Dimensions.get('window').height - 50}}>
          { data && 
            <View>
              <View style={[styles.contentWrapper, {marginBottom: 30}]}>
                <Text style={styles.intro}>{data.intro.toUpperCase()}</Text>
                {
                  data['advice-diet'] && 
                  <View>
                    <View style={styles.row}>
                      <View style={styles.imgWrap}>
                        <Img resizeMode='stretch' source={require('../../../../../assets/img/Meal/Protein.png')} style={{width: 48, height: 48}}/>
                      </View>
                      <View style={styles.htmlWrap}>
                        <HTML baseFontStyle={styles.intro} html={data['advice-diet'].state}></HTML>
                      </View>
                    </View>
                    <View style={styles.row}>
                      <View style={styles.imgWrap}>
                        <Img resizeMode='stretch' source={require('../../../../../assets/img/Meal/Solution.png')} style={{width: 48, height: 48}}/>
                      </View>
                      <View style={styles.htmlWrap}>
                        <HTML baseFontStyle={styles.intro} html={data['advice-diet'].solution}></HTML>
                      </View>
                    </View>
                  </View>
                }
                <View style={styles.redHR}></View>
                {
                  data['advice-menu'] && 
                  <View>
                    <View style={styles.row}>
                      <HTML baseFontStyle={styles.intro} html={data['advice-menu'].intro}></HTML>
                    </View> 
                    {
                      (data['advice-menu'].appetizer) && 
                      <View style={styles.row}>
                        <View style={styles.imgWrap}>
                          <Img resizeMode='stretch' source={require('../../../../../assets/img/Meal/Starter1.png')} style={{width: 48, height: 48}}/>
                        </View>
                        <View style={styles.htmlWrap}>
                          <HTML baseFontStyle={styles.intro} html={data['advice-menu'].appetizer}></HTML>
                        </View>
                      </View>
                    }
                    {
                      (data['advice-menu'].dish) && 
                      <View style={styles.row}>
                        <View style={styles.imgWrap}>
                          <Img resizeMode='stretch' source={require('../../../../../assets/img/Meal/Starter2.png')} style={{width: 48, height: 48}}/>
                        </View>
                        <View style={styles.htmlWrap}>
                          <HTML baseFontStyle={styles.intro} html={data['advice-menu'].dish}></HTML>
                        </View>
                      </View>
                    }
                    <View style={[styles.row, {justifyContent: 'center', marginTop: 30, marginBottom: 0}]}>
                      <Img resizeMode='stretch' source={require('../../../../../assets/img/Meal/Proportions.png')} style={styles.proportionsImg}/>
                    </View>
                    {
                      (data['advice-menu'].dessert) && 
                      <View style={styles.row}>
                        <View style={styles.imgWrap}>
                          <Img resizeMode='stretch' source={require('../../../../../assets/img/Meal/Starter.png')} style={{width: 48, height: 48}}/>
                        </View>
                        <View style={styles.htmlWrap}>
                          <HTML baseFontStyle={styles.intro} html={data['advice-menu'].dessert}></HTML>
                        </View>
                      </View>
                    }
                  </View>
                }              
              </View>
              
              { 
                (this.state.selected === false) &&
                <TouchableOpacity
                  style={[mpStyles.doButton, {marginTop: 0}]}
                  underlayColor='#fff' 
                  onPress={()=>this.doPUT()}>
                  <Text style={mpStyles.doBtnText}>Suivre ces conseils</Text>
                </TouchableOpacity>   
              }
            </View>
          }
          </View>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    dinnerGuidelinesData: state.MealPlanningReducer.dinnerGuidelinesData,
    lunchGuidelinesData: state.MealPlanningReducer.lunchGuidelinesData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getGuidelines: (request) => dispatch(MealPlanningActions.getGuidelines(request.date, request.type, request.cb)),
    putData: (request) => dispatch(MealPlanningActions.putData(request.date, request.type, request.recipe_id, request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Eatingoutside);

const styles = StyleSheet.create({
  backgroundImg: {
    marginTop: 0, 
    width: '100%', 
    height: 367
  },  
  gradient: {
    position: 'absolute', 
    width: '100%',
    top: 0
  },
  header: {
    backgroundColor: 'transparent', 
    position: 'absolute', 
    width: '100%',
    zIndex: 9999
  },
  headerTitle: {
    color: '#DEFB47', 
    width: Dimensions.get('window').width - 70
  },
  subTitle: {
    marginLeft: 25,
    marginTop: -2
  },
  contentWrapper: {
    marginTop: 12,
    marginLeft: 21,
    marginRight: 21
  },
  intro: {
    fontFamily: 'ITC Avant Garde Gothic Pro',
    color: 'white',
    fontSize: 15,
    letterSpacing: 0.5,
    lineHeight: 20,
    paddingTop: 8
  },
  row: {
    flexDirection: 'row', 
    marginTop: 30,
    justifyContent: 'center'
  },
  imgWrap: {
    width: 48,
    height: 48,
    borderRadius: 24,
    overflow: 'hidden'
  },  
  htmlWrap: {
    flexWrap: 'wrap', 
    width: Dimensions.get('window').width - 112, 
    marginLeft: 16
  },
  proportionsImg: {
    width: 296,
    height: 172
  },
  redHR: {
    width: '100%', 
    height: 2, 
    backgroundColor: '#FC706F', 
    marginTop: 30
  }
});