import React from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator, ScrollView, AsyncStorage, StyleSheet, Platform, Text, View, TouchableOpacity, Image, Dimensions } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import _ from 'lodash'
import moment from 'moment'
import 'moment/locale/fr'
import commonStyles from '../../Categories/_styles';
import { MealPlanningActions } from '../../../../actions';
import Utils from '../../../../utils'
import CardRow from '../../../Widgets/Tabs/CardRow'
import Dots from '../../../Widgets/Tabs/Dots'
import Chat from './Chat'
import DlgBox from './DlgBox'

const DINNER_COLOR = '#7558FF'
const BREAKFAST_COLOR = '#43CDEE'
const COLLATION_COLOR = '#FC706F'
const LUNCH_COLOR = '#34E8A5'
const WEEK = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']

class Alimentation extends React.Component {
  // willFocus = this.props.navigation.addListener(
  //   'willFocus',
  //   payload => {
  //     this.selectDate(this.state.currentDate)
  //   }
  // )

  constructor(props) {
    super(props);

    moment.locale('fr')
    this.state = {
      loading: false, 
      currentDate: moment(),
      dots: 0,
      morningSnackPos: 0,
      lunchPos: 0,
      afternoonSnackPos: 0,
      dinnerPos: 0,
      breakfast: null,
      lunch: null,
      dinner: null,
      breakfastData: null,
      lunchData: null,
      dinnerData: null,
      chatVisible: false,
      fsDlgBoxVisible: false,
      mpDlgBoxVisible: false,
      fsWeekBoxHidden: true,
      mpWeekBoxHidden: true
    }
  }

  openAssitance = () => {
    this.setState({
      chatVisible: true
    },() => {
      Utils.trackEvent('Action', 'Chat_Open')
      this.chatview.onViewLoaded()
    })
  }

  closeAssitance = () => {
    this.setState({
      chatVisible: false
    })
  }  

  openPreference = () => {
    Utils.trackEvent('Action', 'Food_habit_Open')
    const {navigate} = this.props.navigation;
    navigate("Cook", {
      from: 'alimentation'
    });
  }

  goRecipe = (data, selected) => {
    const {navigate} = this.props.navigation;
    data['date'] = moment(this.state.currentDate).format('YYYY-MM-DD')
    if(selected === false) {
      data['onGoBack'] = ()=> {
        this.selectDate(this.state.currentDate)
      }

      if(data.id) {
        //menu, minute_meal, guidelines, 
        if(data.id === "menu") {
          if(data.recipe_id != null) {
            data.selected = true
          }
          Utils.trackScreenView('Mealplanning_View_Menu')
          navigate("Menu", data)
        }else if(data.id === "guidelines") {
          Utils.trackScreenView('Mealplanning_View_Eatout')
          navigate("Eatingoutside", data)
        }else {
          Utils.trackScreenView('Mealplanning_View_Recipe')
          navigate("Recipe", data)
        }
      }else {
        Utils.trackScreenView('Mealplanning_View_Recipe')
        navigate("Recipe", data)
      }
    }else {
      Utils.trackEvent('Action', 'Mealplanning_View_Switch')

      let state = this.state
      this.state[data.type]['selected'] = null
      this.setState(state) 

      this.props.mealPlanningData[data.type]['selected'] = null
      this.props.updateData({
        data: this.props.mealPlanningData,
        cb: ()=> {}
      })      
    }
  }  

  selectDate = (newDate) => {
    if(newDate === null) {
      newDate = moment()
    }

    this.setState({    
      currentDate: newDate
    },() => {
      this.loadData()      
    })    
    
    AsyncStorage.getItem(`fsWeekBoxHidden${newDate.format('DDMMYYYY')}`, (err, value) => {
      this.setState({
        fsWeekBoxHidden: (value === 'true')
      })
    })
    AsyncStorage.getItem(`mpWeekBoxHidden${newDate.format('DDMMYYYY')}`, (err, value) => {
      this.setState({
        mpWeekBoxHidden: (value === 'true')
      })
    })
    AsyncStorage.getItem(`fsDlgBoxVisible${newDate.format('DDMMYYYY')}`, (err, value) => {
      this.setState({
        fsDlgBoxVisible: (value === 'true')
      })
    })
    AsyncStorage.getItem(`mpDlgBoxVisible${newDate.format('DDMMYYYY')}`, (err, value) => {
      this.setState({
        mpDlgBoxVisible: (value === 'true')
      })
    })
  }

  loadData = () => {
    let curDate = this.state.currentDate.format('DD-MM-YYYY')
    this.setState({
      loading: true,
    },() => {
      this.props.getData({
        date: curDate,
        type: null,
        cb: (res) => {
          this.setState({
            loading: false
          })          
          
          if(this.props.dataError == null) {
            if(res.compliance.mealPlanning != null) {
              this.props.getWeekCompliance({
                type: 'meal-planning',
                cb: ()=>{}
              })

            }
            if(res.compliance.foodSupplements != null) {
              this.props.getWeekCompliance({
                type: 'food-supplements',
                cb: ()=>{}
              })
            }
            let dots = 0
            if(res.breakfast) {
              dots++
              let breakfast = res.breakfast
              this.setState({breakfast})
            }
            if(res.morning_snack) {
              this.setState({
                morningSnackPos: dots
              })
              dots++           
            }
            if(res.lunch) {
              this.setState({
                lunch: res.lunch,
                lunchPos: dots
              })
              dots++
              let lunch = res.lunch       
            } 
            if(res.afternoon_snack) {
              this.setState({
                afternoonSnackPos: dots
              })
              dots++
            }
            if(res.dinner) {
              this.setState({
                dinner: res.dinner,
                dinnerPos: dots
              })
              dots++
              let dinner = res.dinner
            }

            this.setState({
              dots: dots
            })
          }
        }
      })   
    })    
  }

  componentDidMount() {
    Utils.trackScreenView('Mealplanning_View_Time')
  }

  componentWillMount = () => {
    this.selectDate(this.state.currentDate)
  }

  selectCompliance = (type, value) => {
    let curDate = this.state.currentDate.format('DD-MM-YYYY')
    this.setState({
      loading: true
    })

    Utils.trackEvent('Action', 'Survey_Finish')

    this.props.postCompliance({
      date: curDate, 
      type: type, 
      value: value, 
      cb: (res) => {
        let indexLabel = (type === 'food-supplements') ? 'foodSupplements' : 'mealPlanning'
        let weekText = WEEK[this.state.currentDate.format('e')]
        
        if(this.props[`${indexLabel}ComplianceData`] != null) {
          this.props[`${indexLabel}ComplianceData`][weekText] = value
          // this.props.updateWeekComplianceData({
          //   type: type,
          //   data: this.props[`${indexLabel}ComplianceData`]
          // })
        }else {
          // if compliance data is null, it should get the data from API
          if(indexLabel === 'mealPlanning') {
            this.props.getWeekCompliance({
              type: 'meal-planning',
              cb: ()=>{}
            })
          }else {
            this.props.getWeekCompliance({
              type: 'food-supplements',
              cb: (res)=>{}
            })
          }
        }
        
        this.props.mealPlanningData.compliance[indexLabel] = true
        this.props.updateData({
          data: this.props.mealPlanningData,
          cb: ()=> {
            this.setDlgBoxVisible((type === 'food-supplements') ? 'fsDlgBoxVisible' : 'mpDlgBoxVisible', false)
            this.setState({
              loading: false
            })
          }
        })
      }
    })
  }

  closeSummary = (type) => {
    // type: true => food-supplements, false => meal-planning
    let label = (type === true) ? 'fsWeekBoxHidden' : 'mpWeekBoxHidden'
    this.setDlgBoxVisible(label, true)
  }

  resetSummary = (type) => {    
    let label = (type === true) ? 'fsDlgBoxVisible' : 'mpDlgBoxVisible'
    this.setDlgBoxVisible(label, true)
  }

  setDlgBoxVisible(label, value) {
    let state = {}
    state[label] = value
    this.setState(state)    
    AsyncStorage.setItem(`${label}${this.state.currentDate.format('DDMMYYYY')}`, value.toString())
  }

  render() {
    const WEEKS = ['D', 'L', 'M', 'M', 'J', 'V', 'S']
    let currentWDay = this.state.currentDate.format('D') 
    let currentWeekText = WEEK[this.state.currentDate.format('e')]
    let mealPlanningData = this.props.mealPlanningData
    let compliance = mealPlanningData.compliance

    let foodSupplementsCompliance = this.props.foodSupplementsComplianceData //compliance.foodSupplements
    let mealPlanningCompliance = this.props.mealPlanningComplianceData //compliance.mealPlanning
    
    // breakfast
    let breakfast = this.state.breakfast    
    let morning_snack = mealPlanningData.morning_snack
    let lunch = this.state.lunch
    let afternoon_snack = mealPlanningData.afternoon_snack
    let dinner = this.state.dinner
    let dotsLength = this.state.dots
    let cols = []

    for(let i=0; i< 9; i++) {
      let dt = moment().add(i, 'day')
      let weekDay = dt.format('d')
      let selcted = (currentWDay === dt.format('D'))
      
      cols.push(
        <View key={`date_${i}`} style={styles.dateCol}>
          <Text style={[styles.dateText, selcted && {color: 'white'}, (i==8 && Dimensions.get('window').width <= 350) && {marginRight: 25}]}>{WEEKS[weekDay]}</Text>
          <TouchableOpacity
            style={[styles.dateBtn, selcted && styles.selectedDateBtn, (i==8 && Dimensions.get('window').width <= 350) && {marginRight: 25}]}
            underlayColor='#fff' 
            onPress={()=>this.selectDate(dt)}>
            <Text style={[styles.dateBtnText, selcted && styles.selectedDateBtnText]}>{dt.format('D')}</Text>
          </TouchableOpacity>
        </View>
      )
    }

    let selectedBreakfast = null
    if(breakfast && breakfast.selected) {
      selectedBreakfast = breakfast.choices.find(x => x.recipe_id.toString() === breakfast.selected)
    }

    let selectedLunch = null
    if(lunch && lunch.selected) {
      selectedLunch = lunch.choices.find(x => x.id === lunch.selected)
    }

    let selectedDinner = null
    if(dinner && dinner.selected) {
      selectedDinner = dinner.choices.find(x => x.id === dinner.selected)
    }

    return (
      <View style={commonStyles.container}> 
        {
          this.state.loading &&
          <View style={commonStyles.loading}>
            <ActivityIndicator size={'large'}/>
          </View>
        }                   
        <Chat onRef={ref => (this.chatview = ref)} visible={this.state.chatVisible} onClose={this.closeAssitance}/>
        <View style={[commonStyles.header, {backgroundColor: 'transparent'}]}>
          <Text style={[commonStyles.headerTitle, {color: '#DEFB47'}, {marginLeft: (Dimensions.get('window').width <= 320) ? 16: 25}]}>Alimentation</Text>

          <TouchableOpacity
            style={styles.assitanceBtn}
            underlayColor='#fff' 
            onPress={()=>this.openAssitance()}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Chat.png')}/>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.preferenceBtn}
            underlayColor='#fff' 
            onPress={()=>this.openPreference()}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Preferences.png')}/>
          </TouchableOpacity>
        </View>        

        <View style={[styles.contentWrapper, {paddingBottom: 0}]}>
          <Text style={styles.currentYM}>{Utils.capitalize(moment().format('MMMM YYYY'))}</Text>
          <ScrollView horizontal={true} style={styles.dateSelWrapper}>
            <View style={[styles.dateSelector, (Dimensions.get('window').width > 374) && {width: Dimensions.get('window').width - 50}]}>
              {cols}
            </View>
          </ScrollView>
        </View>
        <KeyboardAwareScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={[commonStyles.container, styles.contentWrapper]}> 
          {
            (this.props.dataError && this.props.dataError.status === 404) ?
              <View style={styles.foodImg}>
                <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Food.png')}/>
                <Text style={[commonStyles.text, styles.availableText]}>Votre programme alimentaire sera bientôt disponible.</Text>
                {
                  (Platform.OS === "android") &&
                  <View style={{height: Dimensions.get('window').height - 300}}></View>
                }
              </View>              
            :
            <View style={(Platform.OS === "android") && {minHeight: Dimensions.get('window').height - 50}}>
              {
                (!_.isEmpty(mealPlanningData)) &&
                <View>
                  {
                    <View style={styles.dlgContainer}>
                      {
                        (compliance.foodSupplements === null || this.state.fsDlgBoxVisible === true) &&
                        <DlgBox compliance={'food-supplements'} type={'day'} onSelect={(v)=>this.selectCompliance('food-supplements', v)}/>
                      }
                      {    
                        (compliance.foodSupplements != null) && (this.state.fsDlgBoxVisible === false) && (this.state.fsWeekBoxHidden === false) &&
                        <DlgBox compliance={'food-supplements'} today={currentWeekText} type={'week'} data={foodSupplementsCompliance} onClose={()=>this.closeSummary(true)} onReset={()=>this.resetSummary(true)}/>
                      }
                      {
                        (compliance.mealPlanning === null || this.state.mpDlgBoxVisible === true) &&
                        <DlgBox compliance={'meal-planning'} type={'day'} onSelect={(v)=>this.selectCompliance('meal-planning', v)}/>
                      }
                      {
                        (compliance.mealPlanning != null) && (this.state.mpDlgBoxVisible === false) && (this.state.mpWeekBoxHidden === false) &&
                        <DlgBox compliance={'meal-planning'} today={currentWeekText} type={'week'} data={mealPlanningCompliance} onClose={()=>this.closeSummary(false)} onReset={()=>this.resetSummary(false)}/>
                      }
                    </View>
                  }
                </View>
              }
              
            {
              (breakfast) &&
              <View style={styles.cardWrapper}>
                <Text style={styles.cardTitle}>{'Petit déjeuner'.toUpperCase()}</Text>
                <Dots color={BREAKFAST_COLOR} selected={0} total={dotsLength}/>
                <View style={{flex: 1}}>
                {
                  (breakfast.selected === null) &&
                    /* 2 options to choose for breakfast */
                    breakfast.choices.map((receipe, i) =>
                      <CardRow 
                        key={`breakfast-${i}`} 
                        index={i} 
                        size={2}
                        color={BREAKFAST_COLOR} 
                        data={{
                          recipe_id: receipe.recipe_id,
                          recipe_name: receipe.recipe_name, 
                          recipe_img_url: receipe.recipe_img_url,
                          selected_id: breakfast.selected,
                          type: 'breakfast'
                        }} 
                        selected={false} 
                        onPress={this.goRecipe}
                        />
                    )
                }
                {
                  (breakfast.selected !== null) &&
                    /* one breakfast */
                    <CardRow 
                      index={0}
                      color={BREAKFAST_COLOR} 
                      selected={true}
                      data={{
                        recipe_id: selectedBreakfast.recipe_id,
                        recipe_name: selectedBreakfast.recipe_name, 
                        recipe_img_url: selectedBreakfast.recipe_img_url, 
                        selected_id: breakfast.selected,
                        type: 'breakfast'
                      }}
                      onPress={this.goRecipe}/>
                }
                </View>
              </View>
            }

            {
              (morning_snack) &&
              <View style={styles.cardWrapper}>
                <Text style={styles.cardTitle}>COLLATION</Text>
                <Dots color={COLLATION_COLOR} selected={this.state.morningSnackPos} total={dotsLength}/>
                <CardRow 
                  index={0} 
                  size={1}
                  color={COLLATION_COLOR} 
                  data={{
                    recipe_id: morning_snack.recipe_id,
                    recipe_name: morning_snack.recipe_name, 
                    recipe_img_url: morning_snack.recipe_img_url, 
                    type: 'morning_snack'
                  }} 
                  selected={false}
                  onPress={this.goRecipe}/>
              </View>              
            }       

            {
              (lunch) &&
              <View style={styles.cardWrapper}>
                <Text style={styles.cardTitle}>{'Déjeuner'.toUpperCase()}</Text>
                <Dots color={LUNCH_COLOR} selected={this.state.lunchPos} total={dotsLength}/>
                <View style={{flex: 1}}>
                {
                  (lunch.selected === null) &&
                    /* 3 options to choose for lunch */
                    lunch.choices.map((recipe, i) =>
                      <CardRow 
                        key={`lunch-${i}`} 
                        index={i} 
                        size={3}                        
                        color={LUNCH_COLOR} 
                        data={{
                          id: recipe.id,
                          recipe_id: (recipe.recipe_id) ? recipe.recipe_id : null,
                          recipe_name: recipe.choice_name, 
                          recipe_img_url: recipe.recipes_img_url ? recipe.recipes_img_url[0] : recipe.recipe_img_url ? recipe.recipe_img_url : recipe.choice_img_url,
                          selected: recipe.selected,
                          selected_id: lunch.selected,
                          type: 'lunch'
                        }} 
                        selected={false}
                        onPress={this.goRecipe}/>
                      )
                }
                {
                  (lunch.selected !== null) &&
                    /* one lunch */
                    <CardRow 
                      index={0}
                      color={LUNCH_COLOR} 
                      selected={true}
                      data={{
                        recipe_id: selectedLunch.recipe_id,
                        recipe_name: selectedLunch.choice_name, 
                        recipe_img_url: selectedLunch.recipes_img_url ? selectedLunch.recipes_img_url[0] : selectedLunch.recipe_img_url ? selectedLunch.recipe_img_url : selectedLunch.choice_img_url,
                        selected_id: selectedLunch.recipe_id,
                        type: 'lunch',
                        id: selectedLunch.id
                      }}
                      onPress={this.goRecipe}/>
                }
                </View>
              </View>
            }            

            {
              (afternoon_snack) &&
              <View style={styles.cardWrapper}>
                <Text style={styles.cardTitle}>COLLATION</Text>
                <Dots color={COLLATION_COLOR} selected={this.state.afternoonSnackPos} total={dotsLength}/>
                <CardRow 
                  index={0} 
                  size={1}
                  color={COLLATION_COLOR} 
                  data={{
                    recipe_id: afternoon_snack.recipe_id,
                    recipe_name: afternoon_snack.recipe_name, 
                    recipe_img_url: afternoon_snack.recipe_img_url, 
                    selected_id: afternoon_snack.selected_id,
                    type: 'afternoon_snack'
                  }} 
                  selected={false}
                  onPress={this.goRecipe}/>
              </View>              
            } 

            {
              (dinner) &&
              <View style={styles.cardWrapper}>
                <Text style={styles.cardTitle}>{'Dîner'.toUpperCase()}</Text>
                <Dots color={DINNER_COLOR} selected={this.state.dinnerPos} total={dotsLength}/>
                <View style={{flex: 1}}>
                {
                  (dinner.selected === null) &&
                    /* 3 options to choose for dinner */
                    dinner.choices.map((recipe, i) =>
                      <CardRow 
                        key={`dinner-${i}`} 
                        index={i} 
                        size={3}                        
                        color={DINNER_COLOR} 
                        data={{
                          id: recipe.id,
                          recipe_id: (recipe.recipe_id) ? recipe.recipe_id : null,
                          recipe_name: recipe.choice_name, 
                          recipe_img_url: recipe.recipes_img_url ? recipe.recipes_img_url[0] : recipe.recipe_img_url ? recipe.recipe_img_url : recipe.choice_img_url,
                          selected: recipe.selected,
                          type: 'dinner',
                        }} selected={false}
                        onPress={this.goRecipe}/>
                    )
                }
                {
                  (dinner.selected !== null) &&
                    /* one dinner */
                    <CardRow 
                      index={0}
                      color={DINNER_COLOR} 
                      selected={true}
                      data={{
                        recipe_id: selectedDinner.recipe_id,
                        recipe_name: selectedDinner.choice_name, 
                        recipe_img_url: selectedDinner.recipes_img_url ? selectedDinner.recipes_img_url[0] : selectedDinner.recipe_img_url ? selectedDinner.recipe_img_url : selectedDinner.choice_img_url,
                        selected_id: selectedDinner.recipe_id,
                        type: 'dinner',
                        id: selectedDinner.id
                      }}
                      onPress={this.goRecipe}/>
                }
                </View>
              </View>
            }               
            </View>
          }
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    mealPlanningData: state.MealPlanningReducer.mealPlanningData,
    breakfastData: state.MealPlanningReducer.breakfastData,
    lunchData: state.MealPlanningReducer.lunchData,
    dinnerData: state.MealPlanningReducer.dinnerData,
    mealPlanningComplianceData: state.MealPlanningReducer.mealPlanningComplianceData,
    foodSupplementsComplianceData: state.MealPlanningReducer.foodSupplementsComplianceData,
    dataError: state.MealPlanningReducer.dataError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateData: (request) => dispatch(MealPlanningActions.updateData(request.data, request.cb)),
    updateWeekComplianceData: (request) => dispatch(MealPlanningActions.updateWeekComplianceData(request.type, request.data)),
    getData: (request) => dispatch(MealPlanningActions.getData(request.date, request.type, request.cb)),
    postCompliance: (request) => dispatch(MealPlanningActions.postCompliance(request.date, request.type, request.value, request.cb)),
    getWeekCompliance: (request) => dispatch(MealPlanningActions.getWeekCompliance(request.type, request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Alimentation);

const styles = StyleSheet.create({
  assitanceBtn: {
    position: 'absolute', 
    top: 50, 
    right: 63
  },
  preferenceBtn: {
    position: 'absolute', 
    top: 48, 
    right: 20
  },
  contentWrapper: {
    paddingLeft: (Dimensions.get('window').width <= 320) ? 16 : 25,
    paddingRight: (Dimensions.get('window').width <= 320) ? 16 : 25,
    paddingBottom: 15
  },
  currentYM: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: 'white',
    fontSize: 24
  },
  dateSelWrapper: {
    width: Dimensions.get('window').width,
    marginLeft: (Dimensions.get('window').width <= 320) ? -15 : -24,
    paddingLeft: (Dimensions.get('window').width <= 320) ? 15 : 24
  },
  dateSelector: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
    marginBottom: 8,
  },
  dateCol: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  dateText: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: '#616170',
    fontSize: 12
  },
  dateBtn: {
    width: 36,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5
  },
  selectedDateBtn: {
    backgroundColor: '#34E8A5',
    borderRadius: 18
  },
  dateBtnText: {
    fontFamily: 'ITCAvantGardePro-Demi',
    color: '#4A4B5C',
    fontSize: 23,
    width: '100%',
    textAlign: 'center',
    paddingTop: (Platform.OS === 'ios') ? 9 : 0
  },
  selectedDateBtnText: {
    color: '#292A3E'
  },
  foodImg: {
    marginTop: 90,
    alignItems: 'center',
    justifyContent: 'center'
  },
  availableText: {
    marginTop: 25,
    textAlign: 'center',
    width: 235
  },
  cardWrapper: {
    marginTop: 22,
    marginBottom: 11,
    flex: 1
  },  
  cardTitle: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: 'white',
    fontSize: 18,
    marginBottom: 4
  },
  dlgContainer: {
    marginTop: 15
  }
})