import React from 'react'
import { ActivityIndicator, StyleSheet, Text, View, TouchableOpacity, Image, Platform } from 'react-native'
import WeekSummary from '../../../Widgets/Tabs/WeekSummary'
import commonStyles from '../../Categories/_styles'

export default class DlgBox extends React.Component {
  render() {
    let complianceType = this.props.compliance
    let data = this.props.data
    let today = this.props.today

    const fsTexts = [
      'Continuez comme ça, les compléments alimentaires sont votre meilleur allié pour y arriver !',
      'Les compléments alimentaires sont d’une grande aide, essayez de les prendre régulièrement.'
    ]
    const mpTexts = [
      'Bravo ! Nous essayons de toujours vous proposer des repas variés et à votre goût pour vous mener au succès.',
      'Ce n’est pas grave ! Il n’est pas toujours facile de suivre le programme à la lettre, n’abandonnez pas !',
      'Ce n’est pas grave ! Il n’est pas toujours facile de suivre le programme à la lettre, n’abandonnez pas !'
    ]

    let weekSummaryText = ''
    if(data && today) {
      idx = data[today]
      if(complianceType === 'food-supplements') {
        idx = (idx === true) ? 0 : 1
        weekSummaryText = fsTexts[idx]
      }else {
        weekSummaryText = mpTexts[idx]
      }     
    }

    return (
      <View>
        {
          (this.props.type === 'day') &&
          <View> 
            {
              (complianceType === 'food-supplements') ?
              <View style={styles.complianceWrap}>
                <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/FoodSupplement.png')}/>
                <View style={{paddingLeft: 15, flex: 1}}>
                  <View style={{marginTop: 5}}>
                    <Text style={commonStyles.text}>Avez-vous pris vos compléments alimentaires aujourd’hui?</Text>
                  </View>
                  <View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
                    <TouchableOpacity
                      style={styles.complianceBtn}
                      underlayColor='#fff' 
                      onPress={()=>this.props.onSelect(true)}>
                      <Text style={[commonStyles.text, styles.complianceBtnText]}>Oui</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.complianceBtn}
                      underlayColor='#fff' 
                      onPress={()=>this.props.onSelect(false)}>
                      <Text style={[commonStyles.text, styles.complianceBtnText]}>Non</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              :
              <View style={[styles.complianceWrap, {flexDirection: 'column'}]}>
                <View style={{flexDirection: 'row'}}>
                  <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/MealPlanning.png')}/>                
                  <View style={{flex: 1, marginLeft: 15, marginTop: 5, justifyContent: 'center'}}>
                    <Text style={commonStyles.text}>Avez-vous suivi tous les repas proposés aujourd'hui?</Text>
                  </View>
                </View>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginTop: 20}}>
                  <TouchableOpacity
                    style={styles.complianceBtn}
                    underlayColor='#fff' 
                    onPress={()=>this.props.onSelect(0)}>
                    <Text style={[commonStyles.text, styles.complianceBtnText]}>Tous</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.complianceBtn}
                    underlayColor='#fff' 
                    onPress={()=>this.props.onSelect(1)}>
                    <Text style={[commonStyles.text, styles.complianceBtnText]}>Pas Tous</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[styles.complianceBtn, {marginRight: 0}]}
                    underlayColor='#fff' 
                    onPress={()=>this.props.onSelect(2)}>
                    <Text style={[commonStyles.text, styles.complianceBtnText]}>Aucun</Text>
                  </TouchableOpacity>
                </View>
              </View>
            }
          </View>
        }

        {
          (this.props.type === 'week') &&
          <View>
          {
            (weekSummaryText === '') ?
            <View style={styles.summaryWrapper}>
              <View style={{height: 30, justifyContent: 'center'}}>
                <ActivityIndicator size={'large'}/>
              </View>   
            </View>
            :
            <View style={styles.summaryWrapper}>
              <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flex: 1}}>
                  <Text style={commonStyles.text}>{weekSummaryText}</Text>
                </View>
                <View style={{width: 35, marginTop: -5, alignItems: 'flex-end'}}>
                  <TouchableOpacity
                    underlayColor='#fff' 
                    onPress={this.props.onClose}
                  >
                    <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Close.png')} />
                  </TouchableOpacity>     
                </View>               
              </View>

              <WeekSummary data={data}/>

              <View style={styles.modifyBtnWrap}>
                <TouchableOpacity
                  underlayColor='#fff' 
                  onPress={this.props.onReset}
                  >
                    <Text style={styles.btnTxt}>Modifier ma réponse</Text>
                </TouchableOpacity>                         
              </View>
            </View>
          }
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  complianceWrap: {
    backgroundColor: '#515262',
    padding: 13,
    borderRadius: 3,
    flex: 1,
    flexDirection: 'row',
    marginBottom: 11,
  },
  complianceBtn: {
    backgroundColor: '#DEFB47',
    borderRadius: 3,
    height: 30,
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'center',
    marginRight: 16
  },
  complianceBtnText: {
    color: '#292A3E',
    fontFamily: 'ITCAvantGardePro-Md',
    textAlign: 'center',
    marginTop: (Platform.OS === 'ios') ? 8 : 0
  },
  summaryWrapper: {
    backgroundColor: '#515262',
    padding: 15,
    borderRadius: 3,
    marginBottom: 11
  },
  modifyBtnWrap: {
    marginTop: 19,
    flex: 1, 
    alignItems: 'center'
  },
  btnTxt: {
    fontFamily: 'ITCAvantGardePro-Bold',
    fontSize: 11,
    color: '#B0B0B0',
    textAlign: 'center'
  }
})