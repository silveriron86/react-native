import { StyleSheet, Dimensions, Platform } from 'react-native';

export default StyleSheet.create({
  bodyTitle: {
    fontFamily: 'ITCAvantGardePro-Md',
    fontSize: 23,
    lineHeight: 29,
    color: 'white',
    width: Dimensions.get('window').width - 70,
  },
  likeBtn: {
    position: "absolute",
    right: 0,
    marginTop: 2
  },  
  reportBtn: {
    position: "absolute",
    right: 0,
    marginTop: 30,
    width: 36,
    height: 36,
    marginRight: -8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  reportImg: {
    width: 24,
    height: 24
  },
  sharetBtn: {
    marginTop: 72
  },
  shareImg: {
    width: 24,
    height: 31.44
  },  
  rowFlexWrapper: {
    flex: 1,
    flexDirection: 'row'
  },
  block: {
    paddingBottom: 45
  },
  blockTitleWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: '#FC706F',
    paddingBottom: 8,
    marginBottom: 15
  },
  blockTitle: {
    fontFamily: 'ITCAvantGardePro-Demi',
    color: 'white',
    fontSize: 15,
    letterSpacing: 0.5,
    lineHeight: 20,
  },
  infoTextWrapper: {
    marginLeft: 8, 
    justifyContent: 'center',
    paddingRight: 17,
    paddingTop: 6
  },
  smallText: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 13,
    color: 'white',
    letterSpacing: 0.17,
    lineHeight: 19
  },
  tagsWrapper: {
    flex: 1,
    flexDirection: 'row',    
    marginTop: 0,
    marginBottom: 15,
    flexWrap: 'wrap'
  },
  tag: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#DEFB47',
    height: 24,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 12,
    marginRight: 8,
    marginBottom: 8
  },
  tagText: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: '#DEFB47',
    textAlign: 'center',
    fontSize: 13,
    marginTop: (Platform.OS === 'ios') ? 7 : 0
  },
  nut: {
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 36,
  },  
  nutValue: {
    fontFamily: 'ITCAvantGardePro-Md',
    fontSize: 13,
    color: 'white'
  },
  nutName: {
    fontFamily: 'ITCAvantGardePro-Md',
    fontSize: 12,
    color: '#9B9B9B'
  },
  ingredientRowsWrapper: {
    justifyContent: 'space-between', 
    flexDirection: 'column', 
    paddingTop: 5
  },
  circleImgWrapper: {
    width: 40,
    height: 40,
    borderRadius: 20,
    overflow: 'hidden'
  },
  stepText: {
    fontFamily: 'ITCAvantGardePro-demi',
    fontSize: 15,
    color: 'white',
    letterSpacing: 0.5,
    lineHeight: 20
  },
  bottomTextWrapper: {
    flexDirection: 'row', 
    flexWrap: 'wrap', 
    paddingBottom: 20,
    marginTop: -5
  },
  bottomText: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 12,
    color: 'white',
    letterSpacing: 0.16,
    lineHeight: 19
  },
  doButton: {
    backgroundColor: '#DEFB47',
    height: 43,
    width: '100%',
    justifyContent: 'center'
  },
  doBtnText: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: '#292A3E',
    fontSize: 15,
    letterSpacing: 0.2,
    lineHeight: 19,
    textAlign: 'center',
    paddingTop: (Platform.OS === 'ios') ? 5 : 1
  },
  chatInputBoxContainer: {
    backgroundColor: '#3E3F51',
    height: 44,
    flexDirection: 'row', 
    width: Dimensions.get('window').width,
    marginLeft: -24,
    padding: 4
  },
  chatInputBoxWrap: {
    height: 36,
    width: Dimensions.get('window').width - 94,
    marginLeft: 12,
    justifyContent: 'center',
    borderRadius: 18,
    backgroundColor: '#292A3E'
  },
  chatInput: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: 'white',
    fontSize: 15,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: (Platform.OS === 'ios') ? 4 : 0,
    paddingBottom: 0
  },
  chatSendBtn: {
    marginLeft: 10,
    height: 36,
    paddingTop: (Platform.OS === 'ios') ? 4 : 2,
    justifyContent: 'center'
  },
  chatBtnTxt: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: '#DEFB47',
    fontSize: 15,
    letterSpacing: -0.36
  },
  chatText: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 15,
    color: '#DEFB47'
  },
  chartSupportText: {
    color : 'white'
  },
  chatFromWrap: {
    backgroundColor: '#48495A', 
    borderColor: '#979797', 
    borderRadius: 19, 
    padding: 13,
    paddingBottom: (Platform.OS === 'ios') ? 8 : 13,
    alignSelf: 'flex-start',
    minWidth: 60
  },
  chatToWrap: {
    backgroundColor: '#48495A', 
    borderColor: '#979797', 
    borderRadius: 19, 
    paddingTop: 7,
    paddingBottom: (Platform.OS === 'ios') ? 3 : 7,
    paddingLeft: 18,
    paddingRight: 18,
    alignSelf: 'flex-end',
    minWidth: 60
  },
  chatRow: {
    marginBottom: 20
  },
  tailImg: {
    marginLeft: 20, 
    marginTop: -1
  },
  toTailImg: {
    alignSelf: 'flex-end',
    marginRight: 20,
    marginTop: -1
  }
});