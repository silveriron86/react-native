import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ActivityIndicator, Text, View, TouchableOpacity, Image as Img, Dimensions, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import moment from 'moment';
import commonStyles from '../../Categories/_styles';
import mpStyles from './_styles';
import Info from '../../../Widgets/Tabs/Info'
import Nutrients from '../../../Widgets/Tabs/Nutrients'
import CircleImage from '../../../Widgets/Tabs/CircleImage'
import { MealPlanningActions } from '../../../../actions';
import Utils from '../../../../utils';

const DESSERT_COLOR = '#7558FF'
const APPETIZER_COLOR = '#43CDEE'
const DISH_COLOR = '#FC706F'

class CardRow extends React.Component {
  render() {
    const color = this.props.color
    const data = this.props.data 
    
    return (
      <TouchableOpacity onPress={()=>this.props.onPress(data)}>
        <View style={styles.cardArrowWrapper}>
          <Img resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowRight.png')} style={{tintColor: color}}/>
        </View>      
        <View style={[styles.cardRow, {backgroundColor: `${color}20`, borderColor: color}]}>
          <View style={styles.cardImgWrapper}>
            <View style={styles.cardImg}>
              <Image
                source={{uri: data.recipe_img_url}}
                indicator={Progress.Circle}
                indicatorProps={{
                  size: 45,
                  borderWidth: 0,
                  color: 'rgba(150, 150, 150, 1)',
                  unfilledColor: 'rgba(200, 200, 200, 0.2)'
                }}
                style={{width: 48, height: 48}}/>
            </View>
          </View>
          <View style={styles.cardRightWrapper}>
            <View style={{flexDirection: 'row'}}>
              <View style={styles.cardRowText}>
                <Text style={commonStyles.text}>{data.recipe_name}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              { 
                data.recipe_preparation_duration &&
                <View style={styles.iconBlock}>
                  <View style={{justifyContent: 'center'}}>
                    <Img resizeMode='contain' source={require('../../../../../assets/img/ic/Preparation.png')} style={{height: 15, width: 18}}/>
                  </View>
                  <View style={styles.infoTextWrapper}><Text style={[mpStyles.smallText, {marginTop: 4}]}>{data.recipe_preparation_duration}</Text></View>
                </View>
              }

              { 
                data.recipe_cooking_duration &&
                <View style={styles.iconBlock}>
                  <View style={{justifyContent: 'center'}}>
                    <Img resizeMode='contain' source={require('../../../../../assets/img/ic/Cooking.png')} style={{width: 16, height: 20}}/>
                  </View>
                  <View style={styles.infoTextWrapper}><Text style={[mpStyles.smallText, {marginTop: 4}]}>{data.recipe_cooking_duration}</Text></View>
                </View>
              }

              { 
                data.servings &&
                <View style={styles.iconBlock}>
                  <View style={{justifyContent: 'center'}}>
                    <Img resizeMode='contain' source={require('../../../../../assets/img/ic/Serving.png')} style={{height: 20, width: 16}}/>
                  </View>
                  <View style={styles.infoTextWrapper}><Text style={[mpStyles.smallText, {marginTop: 4}]}>{data.servings} pers</Text></View>
                </View>
              }  
            </View>
          </View>
        </View>   
      </TouchableOpacity>    
    ) 
  }
}

class Menu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      menuData: null,
      selected: false
    }
  }

  componentWillMount = () => {
    const { navigation } = this.props;    
    const date = navigation.getParam('date');
    const type = navigation.getParam('type');
    
    this.setState({
      loading: true
    })
    this.props.getMenu({
      date: moment(date).format('DD-MM-YYYY'),
      type: type,
      cb: (res) => {
        this.setState({
          loading: false,
          menuData: res
        })
      }
    })
  }

  doClose = () => {
    this.props.navigation.goBack()
  }  

  doPUT = () => {
    const { navigation } = this.props;    
    const type = navigation.getParam('type');
    const date = navigation.getParam('date');
    const selected = navigation.getParam('selected') // #1471

    if(selected === true) {
      Utils.trackEvent('Action', 'Mealplanning_View_Cooking')
      // #1471: if the user already selected, when click yellow button, it will redirect to first available recipe.
      const data = this.state.menuData
      if(data.appetizer) {
        this.onPress(data.appetizer)
      }else if(data.dish) {
        this.onPress(data.dish)
      }else if(data.dessert) {
        this.onPress(data.dessert)
      }    
      return  
    } 

    // if user didn't choose this option yet
    this.setState({
      loading: true
    })
    this.props.putData({
      date: moment(date).format('DD-MM-YYYY'),
      type: type,
      recipe_id: "menu",
      cb: (res) => {
        this.setState({
          loading: false,
          selected: true
        },() => {
          navigation.goBack()
          navigation.state.params.onGoBack()
        })        
      }
    })
    
  }

  onPress = (data) => {
    const { navigation } = this.props;    
    const selected_date = navigation.getParam('date');
    
    navigation.navigate("Recipe", {
      recipe_id: data.recipe_id,
      recipe_name: data.recipe_name, 
      recipe_img_url: data.recipe_img_url,
      date: moment(selected_date).format('YYYY-MM-DD'),
      selected: navigation.getParam('selected'),
      type: 'UNKNOWN',
      from: 'menu',
      option: data.title,
      menuType: navigation.getParam('type'), // lunch or dinner
      appetizer: this.state.menuData.appetizer,
      dish: this.state.menuData.dish,
      dessert: this.state.menuData.dessert
    })
  }

  render() {
    const { navigation } = this.props
    const selected_date = navigation.getParam('date')
    const type = navigation.getParam('type')
    const selected = navigation.getParam('selected') // #1471
    const data = this.state.menuData

    return (
      <View style={commonStyles.container}>  
        {
          this.state.loading &&
          <View style={commonStyles.loading}>
            <ActivityIndicator size={'large'}/>
          </View>
        } 

        <View style={[commonStyles.header, styles.header]}>
          <Text style={[commonStyles.headerTitle, styles.headerTitle]}>{data ? data.title : ''}</Text>
          {data && <Text style={[commonStyles.text, styles.subTitle]}>{`${moment(selected_date).format('D MMMM YYYY')} - ${(type === 'lunch') ? 'Déjeuner' : 'Dîner'}`.toUpperCase()}</Text>}
          <TouchableOpacity
            style={[commonStyles.closeButton, {marginTop: -2}]}
            underlayColor='#fff' 
            onPress={()=>this.doClose()}>
            <Img resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowLeft.png')}/>
          </TouchableOpacity>
        </View>
        
        <KeyboardAwareScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={commonStyles.container}>
          <View style={(Platform.OS === "android") && {minHeight: Dimensions.get('window').height - 50}}>
          {
            data &&
            <View style={styles.contentWrapper}>
              <Info data={data} user_like={null} fromMenu={true}></Info>
              <Nutrients data={data}></Nutrients>

              <View style={{marginTop: -20}}>
                {
                  data.appetizer &&
                  <View style={styles.cardWrapper}>
                    <Text style={styles.cardTitle}>{'Entrée'.toUpperCase()}</Text>
                    <View style={{flex: 1}}>              
                      <CardRow data={data.appetizer} color={APPETIZER_COLOR} onPress={this.onPress}/>
                    </View>
                  </View>
                }

                {
                  data.dish &&
                  <View style={styles.cardWrapper}>
                    <Text style={styles.cardTitle}>{'PLAT'.toUpperCase()}</Text>
                    <View style={{flex: 1}}>              
                      <CardRow data={data.dish} color={DISH_COLOR} onPress={this.onPress}/>
                    </View>
                  </View>
                }

                {
                  data.dessert &&
                  <View style={styles.cardWrapper}>
                    <Text style={styles.cardTitle}>{'Déssert'.toUpperCase()}</Text>
                    <View style={{flex: 1}}>              
                      <CardRow data={data.dessert} color={DESSERT_COLOR} onPress={this.onPress}/>
                    </View>
                  </View>
                }
              </View>
              
              {
                (this.state.selected === true) &&
                <View style={mpStyles.bottomTextWrapper}>
                  <Text style={mpStyles.bottomText}>Powered by </Text><Text style={[mpStyles.bottomText, {color: '#FC706F'}]}>Youmiam</Text>
                </View>
              }
            </View>
          }

          {
            /* will be shown only if the user should select a recipe(meaning if the user has not already selected) */
            data && (data.type !== 'Collation') && (this.state.selected === false) &&
            <TouchableOpacity
              style={[mpStyles.doButton, {marginTop: 20}]}
              underlayColor='#fff' 
              onPress={()=>this.doPUT()}>
              <Text style={mpStyles.doBtnText}>{(selected === true) ? 'En cuisine !' : 'Choisir ce menu'}</Text>
            </TouchableOpacity>   
          }
          </View>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    menuData: state.MealPlanningReducer.menuData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMenu: (request) => dispatch(MealPlanningActions.getMenu(request.date, request.type, request.cb)),
    putData: (request) => dispatch(MealPlanningActions.putData(request.date, request.type, request.recipe_id, request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'transparent', 
    width: '100%'
  },
  headerTitle: {
    color: '#DEFB47', 
    width: Dimensions.get('window').width - 70
  },
  subTitle: {
    marginLeft: 25,
    marginTop: -2
  },
  contentWrapper: {
    marginTop: 12,
    marginLeft: 21,
    marginRight: 21
  },
  cardWrapper: {
    marginTop: 5,
    marginBottom: 11,
    flex: 1
  },    
  cardTitle: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: 'white',
    fontSize: 18,
    marginBottom: 4
  },
  cardRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 104,
    borderLeftWidth: 3,
    borderRadius: 3
  },
  cardImgWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 72
  },
  cardImg: {
    width: 48,
    height: 48,
    borderRadius: 24,
    overflow: 'hidden'
  },
  cardRightWrapper: {
    flexDirection: 'column', 
    justifyContent: 'center', 
    width: Dimensions.get('window').width - 60
  },
  cardRowText: {
    borderColor: 'white', 
    width: Dimensions.get('window').width - 60 - 124, 
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingTop: 8
  },
  infoTextWrapper: {
    marginLeft: (Dimensions.get('window').width <= 320) ? 2 : 4, 
    justifyContent: 'center',
    paddingRight: (Dimensions.get('window').width <= 320) ? 7 : 14,
  },  
  cardArrowWrapper: {
    position: 'absolute',
    right: 11,
    width: 28,
    height: 28,
    top: 52,
    marginTop: -16
  },
  iconBlock: {
    flexDirection: 'row', 
    justifyContent: 'center', 
    height: 24
  }  
});