import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ActivityIndicator, Text, View, TouchableOpacity, Image as Img, Dimensions } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import moment from 'moment';
import commonStyles from '../../Categories/_styles';
import mpStyles from './_styles';
import Info from '../../../Widgets/Tabs/Info'
import Nutrients from '../../../Widgets/Tabs/Nutrients'
import CircleImage from '../../../Widgets/Tabs/CircleImage'
import { MealPlanningActions } from '../../../../actions';

class Recipe extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      receipeData: null,
      user_like: false,
      selected: false,
      option: '',
      recipeImg: '',
      scrolling: false
    }
  }

  componentWillMount = () => {
    const recipe_id = this.props.navigation.getParam('recipe_id');    
    this.loadData(recipe_id)
  }

  loadData = (recipe_id) => {
    const selected = this.props.navigation.getParam('selected');
    this.setState({
      loading: true,
      selected: (selected) ? selected : false // this will be used from menu cook view
    })
    this.props.getRecipe({
      recipe_id: recipe_id,
      cb: (res) => {
        this.setState({
          loading: false,
          receipeData: res,
          user_like: res.user_like
        })
      }
    })    
  }

  componentDidMount() {
    this.setState({
      option: this.props.navigation.getParam('option'),
      recipeImg: this.props.navigation.getParam('recipe_img_url')
    })
  }

  selectOption = (opt) => {
    const { navigation } = this.props;   
    let data = null
    if(opt === 'Entrée') {
      data = navigation.getParam('appetizer')
    }else if(opt === 'Plat') {
      data = navigation.getParam('dish')
    }else {
      data = navigation.getParam('dessert')
    }

    this.setState({
      option: opt,
      recipeImg: data.recipe_img_url
    },() => {
      this.loadData(data.recipe_id)
    })
  }

  doClose = () => {
    this.props.navigation.goBack()
  }  

  doLike = () => {
    this.setState({
      loading: true
    })

    const recipe_id = this.props.navigation.getParam('recipe_id');
    if(this.state.user_like === true) {
      this.props.delLikes({
        recipe_id: recipe_id,
        cb: (res) => {
          this.setState({
            loading: false,
            user_like: false
          })
        }
      })
    }else {
      this.props.postLikes({
        recipe_id: recipe_id,
        cb: (res) => {
          this.setState({
            loading: false,
            user_like: true
          })
        }
      })
    }
  }

  doPUT = () => {
    const { navigation } = this.props;    
    const recipe_id = navigation.getParam('recipe_id');
    const id = navigation.getParam('id');
    const type = navigation.getParam('type');
    const date = navigation.getParam('date');

    this.setState({
      loading: true
    })
    this.props.putData({
      date: moment(date).format('DD-MM-YYYY'),
      type: type,
      recipe_id: id ? id : recipe_id,
      cb: (res) => {
        this.setState({
          loading: false,
          selected: true
        },() => {
          navigation.goBack()          
          navigation.state.params.onGoBack()
        })        
      }
    })
  }

  handeScroll = (e) => {
    if(e.nativeEvent.contentOffset.y > 0) {
      this.setState({
        scrolling: true
      });
    }else {
      this.setState({
        scrolling: false
      });
    }
  }

  render() {
    const { navigation } = this.props
    const recipe_name = navigation.getParam('recipe_name')
    const recipe_img_url = this.state.recipeImg
    const selected_date = navigation.getParam('date')
    const type = navigation.getParam('type')
    const from = navigation.getParam('from')
    const option = this.state.option
    const menuType = navigation.getParam('menuType')

    const appetizer = navigation.getParam('appetizer')
    const dish = navigation.getParam('dish')
    const dessert = navigation.getParam('dessert')
    const data = this.state.receipeData

    recipe_id = this.props.navigation.getParam('recipe_id')
    selected_id = this.props.navigation.getParam('selected_id')
    
    let ingredientRows = []
    let cols = []
    if(data && data.ingredients && data.ingredients.length > 0) {
      data.ingredients.forEach((ingredient, i) => {
        cols.push(
          <View key={`ingredient-${i}`} style={[mpStyles.rowFlexWrapper, (i%2 === 0) ? {marginRight: 4} : {marginLeft: 4}]}>
            <CircleImage uri={ingredient.img_url}/>
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between', marginLeft: 8}}>
              <Text style={[mpStyles.smallText, {lineHeight: 15}]}>{ingredient.name}</Text>
              <Text style={[mpStyles.smallText, {lineHeight: 15}]}>{ingredient.qty_value} {ingredient.qty_unit}</Text>
            </View>
          </View>
        )
        if(i % 2 === 1 || i === data.ingredients.length-1) {
          ingredientRows.push(
            <View 
              key={`ingrdient-col-${i}`} 
              style={[mpStyles.rowFlexWrapper, (i < data.ingredients.length-1 ) ? {marginBottom: 16} : null]}>
              {cols}
            </View>
          )
          cols = []
        }
      })
    }

    return (
      <View style={commonStyles.container}>
        <View style={[commonStyles.header, styles.header, this.state.scrolling && {backgroundColor: '#292A3E'}]}>
          <Text style={[commonStyles.headerTitle, styles.headerTitle]}>{from === 'menu' ? 'Menu à cuisiner' : recipe_name}</Text>
          {data && <Text style={[commonStyles.text, styles.subTitle]}>{`${moment(selected_date).format('D MMMM YYYY')} - ${(from === 'menu') ? ((menuType === 'lunch') ? 'Déjeuner' : 'Dîner') : data.type}`.toUpperCase()}</Text>}
          <TouchableOpacity
            style={[commonStyles.closeButton, {marginTop: -2}]}
            underlayColor='#fff' 
            onPress={()=>this.doClose()}>
            <Img resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowLeft.png')}/>
          </TouchableOpacity>
          {
            (from === 'menu') && 
            <View style={styles.menuOptions}>
              {
                appetizer &&
                <TouchableOpacity
                  underlayColor='#fff' 
                  onPress={()=>this.selectOption('Entrée')}
                  disabled={(option === 'Entrée')}>
                  <Text style={[styles.optionText, (option === 'Entrée') && {color: 'white'}]}>Entrée</Text>
                </TouchableOpacity>
              }
              {
                appetizer && dish &&
                <View style={styles.yellowDivider}></View>
              }
              {
                dish &&              
                <TouchableOpacity
                  underlayColor='#fff' 
                  onPress={()=>this.selectOption('Plat')}
                  disabled={(option === 'Plat')}>
                  <Text style={[styles.optionText, (option === 'Plat') && {color: 'white'}]}>Plat</Text>
                </TouchableOpacity>
              }
              {
                dish && dessert &&
                <View style={styles.yellowDivider}></View>
              }
              {
                dessert &&       
                <TouchableOpacity
                  underlayColor='#fff' 
                  onPress={()=>this.selectOption('Dessert')}
                  disabled={(option === 'Dessert')}>               
                  <Text style={[styles.optionText, (option === 'Dessert') && {color: 'white'}]}>Dessert</Text>
                </TouchableOpacity>
              }
            </View>
          }

        </View>      
        <KeyboardAwareScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={commonStyles.container} onScroll={this.handeScroll}>  
          {
            this.state.loading &&
            <View style={commonStyles.loading}>
              <ActivityIndicator size={'large'}/>
            </View>
          } 

          <Image
            source={{uri: recipe_img_url}}
            indicator={Progress.Bar}
            indicatorProps={{
              size: 45,
              borderWidth: 0,
              color: 'rgba(150, 150, 150, 1)',
              unfilledColor: 'rgba(200, 200, 200, 0.2)'
            }}
            style={styles.backgroundImg}/>
          
          <Img resizeMode='stretch' source={require('../../../../../assets/img/Gradient.png')} style={styles.gradient}/>
          {
            data &&
            <View style={styles.contentWrapper}>
              <Info data={data} user_like={this.state.user_like} doLike={this.doLike}></Info>
              <Nutrients data={data}></Nutrients>

              {
                (ingredientRows.length > 0) && 
                <View style={mpStyles.block}>
                  <View style={mpStyles.blockTitleWrapper}>
                    <Text style={mpStyles.blockTitle}>{`Ingrédients`.toUpperCase()}</Text>
                  </View>
                  <View style={mpStyles.ingrdientRowsWrapper}>
                    {ingredientRows}
                  </View>        
                </View>
              }

              {
                (data.steps && data.steps.length > 0) && 
                <View style={[mpStyles.block, {paddingBottom: 23}]}>
                  <View style={mpStyles.blockTitleWrapper}>
                    <Text style={mpStyles.blockTitle}>{`Instructions`.toUpperCase()}</Text>
                  </View>

                  {data.steps.map((step, i) => 
                    <View key={`step-${i}`} style={{marginBottom: 22}}>
                      <Text style={mpStyles.stepText}>{`étape`.toUpperCase()} {i+1}</Text>
                      <View style={{flexDirection: 'row', flexWrap: 'wrap', marginTop: 8}}>
                        {
                          (step.ingredients.length > 0) &&
                          step.ingredients.map((ing, ii) =>
                            <CircleImage key={`step-ingredient-${i}-${ii}`} uri={ing.img_url} style={{marginRight: 10}}/>
                          )
                        }
                      </View>
                      <Text style={[commonStyles.text, {marginTop: 8}]}>{step.instructions}</Text>
                    </View>
                  )}
                </View>
              }
              
              {
                (this.state.selected === true) &&
                <View style={mpStyles.bottomTextWrapper}>
                  <Text style={mpStyles.bottomText}>Powered by </Text><Text style={[mpStyles.bottomText, {color: '#FC706F'}]}>Youmiam</Text>
                </View>
              }
            </View>
          }

          {
            /* will be shown only if the user should select a recipe(meaning if the user has not already selected) */
            data && (type === 'breakfast' || type === 'lunch' || type === 'dinner') && (this.state.selected === false) && (selected_id != recipe_id) &&
            <TouchableOpacity
              style={mpStyles.doButton}
              underlayColor='#fff' 
              onPress={()=>this.doPUT()}>
                <Text style={mpStyles.doBtnText}>Choisir cette recette</Text>
            </TouchableOpacity>   
          }
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    recipeData: state.MealPlanningReducer.recipeData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRecipe: (request) => dispatch(MealPlanningActions.getRecipe(request.recipe_id, request.cb)),
    postLikes: (request) => dispatch(MealPlanningActions.postLikes(request.recipe_id, request.cb)),
    delLikes: (request) => dispatch(MealPlanningActions.delLikes(request.recipe_id, request.cb)),
    putData: (request) => dispatch(MealPlanningActions.putData(request.date, request.type, request.recipe_id, request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Recipe);

const styles = StyleSheet.create({
  backgroundImg: {
    marginTop: 0, 
    width: '100%', 
    height: 367
  },  
  gradient: {
    position: 'absolute', 
    width: '100%',
    top: 0
  },
  header: {
    backgroundColor: 'transparent', 
    position: 'absolute', 
    width: '100%',
    zIndex: 9999
  },
  headerTitle: {
    color: '#DEFB47', 
    width: Dimensions.get('window').width - 70
  },
  subTitle: {
    marginLeft: 25,
    marginTop: -2
  },
  contentWrapper: {
    marginTop: 12,
    marginLeft: 21,
    marginRight: 21
  },
  optionText: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: '#9D9D9D',
    fontSize: 21,
    marginTop: 7
  },
  yellowDivider: {
    backgroundColor: '#DEFB47', 
    width: 2, 
    height: 30
  },
  menuOptions: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center',
    marginTop: 15, 
    paddingLeft: 34,
    paddingRight: 34,
    height: 30
  }
});