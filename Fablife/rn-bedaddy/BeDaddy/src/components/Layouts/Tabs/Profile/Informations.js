import React from 'react';
import { ActivityIndicator, Dimensions, StyleSheet, Image, Text, View, TouchableOpacity } from 'react-native';
import moment from 'moment'
import commonStyles from '../../Categories/_styles';
import Utils from '../../../../utils'

export default class Informations extends React.Component {
  goSurvey = () => {
    Utils.trackEvent('Action', 'Profil_Informations_Questions');
    this.props.navigation.navigate("Survey");
  }

  goGenericData = () => {
    Utils.trackEvent('Action', 'My_Genetic_Data');
    this.props.navigation.navigate("GenticData");
  }

  render() {
    let data = this.props.data
    if(!data) {
      return (
        <View style={{height: 200, justifyContent: 'center'}}>
          <ActivityIndicator size={'large'}/>
        </View>
      )
    }
    
    console.log(data)
    return (
      <View>
        <View style={styles.mt20}>
          <Text style={commonStyles.h1}>{`Mon compte`.toUpperCase()}</Text>
          <View style={styles.grayBlock}>
            <Text style={commonStyles.text}>{Utils.capitalize(data.firstname)} {Utils.capitalize(data.lastname)}</Text>
            <View style={styles.hr}></View>
            
            <Text style={styles.labelText}>Adresse Email</Text>
            <Text style={commonStyles.text}>{data.email}</Text>
            <View style={styles.hr}></View>

            <Text style={styles.labelText}>Date d’inscription</Text>
            <Text style={commonStyles.text}>{moment(data.created_at).format('DD MMMM YYYY')}</Text>
          </View>
        </View>

        <View style={styles.mt20}>
          <Text style={commonStyles.h1}>{`Ma santé`.toUpperCase()}</Text>
          <TouchableOpacity
            style={styles.btnRow}
            onPress={this.goSurvey}
            underlayColor='#fff'>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Profile/Nutrinome.png')} style={styles.btnIcon}/>
            <View style={styles.btnTxtWrapper}>
              <Text style={styles.btnText}>Questionnaire de profil</Text>
            </View>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowRight.png')} style={{tintColor: '#292A3E'}}/>
          </TouchableOpacity>

          {/* <TouchableOpacity
            style={[styles.btnRow, {backgroundColor: '#B04DFF'}]}
            onPress={this.goSurvey}
            underlayColor='#fff'>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Profile/MonBilan.png')} style={styles.btnIcon}/>
            <View style={styles.btnTxtWrapper}>
              <Text style={styles.btnText}>Mon Bilan BeDaddy</Text>
            </View>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowRight.png')} style={{tintColor: '#292A3E'}}/>
          </TouchableOpacity> */}

          {
            (data.group !== "NO_GENETIC") &&
              <TouchableOpacity
                style={[styles.btnRow, {backgroundColor: '#FC706F'}]}
                onPress={this.goGenericData}
                underlayColor='#FFF'>
                <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Profile/genetic_data.png')} style={styles.btnIcon}/>
                <View style={styles.btnTxtWrapper}>
                  <Text style={styles.btnText}>Mes données génétiques</Text>
                </View>
                <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowRight.png')} style={{tintColor: '#292A3E'}}/>
              </TouchableOpacity>
          }
        </View>        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  grayBlock: {
    backgroundColor: '#515262',
    borderRadius: 3,
    paddingLeft: 19,
    paddingRight: 19,
    paddingTop: 15,
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10
  },
  hr: {
    height: 1,
    backgroundColor: '#292A3E',
    flex: 1,
    marginTop: 10,
    marginBottom: 14
  },
  labelText: {
    fontFamily: 'ITCAvantGardePro-Bold',
    fontSize: 11,
    color: '#292A3E',
    paddingBottom: 2
  },
  mt20: {
    marginTop: 20
  },
  btnRow: {
    backgroundColor: '#DEFB47',
    borderRadius: 3,
    height: 81,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 11,
    paddingRight: 16,
    marginBottom: 11
  },
  btnIcon: {
    width: 47,
    height: 47
  },
  btnText: {
    fontFamily: 'ITCAvantGardePro-Bold',
    fontSize: 17,
    color: '#292A3E',
    marginTop: 4,
  },
  btnTxtWrapper: {
    width: Dimensions.get('window').width-170
  }
})