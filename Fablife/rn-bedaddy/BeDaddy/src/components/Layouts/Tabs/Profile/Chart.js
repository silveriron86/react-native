import React from 'react'
import { Text, Platform } from 'react-native';
import ChartView from 'react-native-highcharts'
import moment from 'moment'
import _ from 'lodash'
import commonStyles from '../../Categories/_styles'

export default class Chart extends React.Component {
  render() {
    const options = {}
    let chart = this.props.data
    let labels = []
    let series = []
    let data = []
    let colors = []

    console.log(chart);
    if(chart === null || chart.series === null) {
      return <Text style={commonStyles.text}>Vous ne disposez pas encore d’assez de données pour suivre vos progressions.</Text>
    }

    let chartSeries = JSON.parse(JSON.stringify(chart.series))

    if(!_.every(chart.series, _.isObject)) {
      chartSeries = [chart.series]
    }

    chartSeries.forEach(line => {
      data = []
      labels = []

      // line.data.sort((a, b) => {
      //   return moment(a.date).isAfter(b.date)
      // });
      line.data = _.sortBy(line.data, 'date')

      line.data.forEach((row) => {
        data.push(row.value)
        labels.push(moment(row.date).format('DD/MM'))
      })
  
      series.push({
        name: line.name,
        data: data
      })

      colors.push(chart.lineColor ? chart.lineColor : line.lineColor)
    })

    let Highcharts = 'Highcharts';
    let conf = {
      chart: {
        backgroundColor: this.props.backgroundColor,
        type: 'line',
        animation: Highcharts.svg,
        spacingLeft: 0
      },
      colors: colors,
      title: {
        text: ''
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      exporting: {
        enabled: false
      },
      plotOptions: {
        series: {
          marker: {
            fillColor: '#FFFFFF'
          }
        }
      },
      xAxis: {
        categories: labels,
        labels: {
          style: {
            color: '#292A3E'
          }
        },        
        tickWidth: 0,
        lineWidth: 1,
        lineColor: '#FFFFFF'
      }, 
      yAxis: {
        title: {
          text: ''
        },
        labels: {
          style: {
            color: '#292A3E'
          }
        },
        lineWidth: 1,
        lineColor: '#FFFFFF',
        gridLineDashStyle: 'dash',
        // max: 100,
        // tickInterval: 15
      },
      series: series
    } 

    return (
      <ChartView style={{height: 140, marginBottom: 3}} config={conf} options={options} originWhitelist={['']}/>
    );
  }
}