import React from 'react';
import { Dimensions, ActivityIndicator, AsyncStorage, StyleSheet, Image, Text, View, TouchableOpacity, ScrollView, Platform } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import {connect} from 'react-redux';
import _ from 'lodash'
import commonStyles from '../../Categories/_styles';
import Switch from '../../../Widgets/Switch';
import { SurveyActions } from '../../../../actions';
import Utils from '../../../../utils';

class Settings extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      loading: false
    }
  }

  componentDidMount = () => {
    this.props.getNoticeSurvey({
      cb: (res)=> {}
    })
  }

  onToggle = (question_id, value) => {
    let data = {}
    data[question_id] = value

    this.setState({
      loading: true
    }, ()=> {
      Utils.trackEvent('Action', (value === true) ? 'Notification_Yes' : 'Notification_No')
      this.props.postNoticeSurvey({
        data: data,
        cb: ()=> {
          this.props.getNoticeSurvey({
            cb: ()=> {
              this.setState({
                loading: false
              })
            }
          })
        }
      })
    })
  }

  doClose = () => {
    this.props.navigation.goBack()
  }
  
  goPrivacyPolicy = () => {
    // const rootNav = this.props.screenProps.rootNavigation
    // rootNav.navigate('PrivacyPolicy')

    this.props.navigation.navigate('PrivacyPolicy', {
      from: 'Settings'
    })
  }

  signOut = () => {
    AsyncStorage.clear(()=> {
      Utils.trackEvent('Action', 'App_log_out')

      const rootNav = this.props.screenProps.rootNavigation
      rootNav.dispatch(StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Login' })],
      }))
    })
  }

  render() {
    return (
      <View style={commonStyles.container}>              
        {
          this.state.loading &&
          <View style={commonStyles.loading}>
            <ActivityIndicator size={'large'}/>
          </View>
        } 

        <View style={[commonStyles.header, {backgroundColor: 'transparent'}]}>
          <Text style={[commonStyles.headerTitle, {color: '#DEFB47'}]}>Paramètres</Text>

          <TouchableOpacity
            style={[commonStyles.closeButton, {marginTop: -2}]}
            underlayColor='#fff' 
            onPress={()=>this.doClose()}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowLeft.png')}/>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          {/* <Text style={styles.title}>{`Notifications`.toUpperCase()}</Text> */}
          <ScrollView>
            {/* {
              (_.isEmpty(this.props.noticeSurveyData)) ? 
              <View style={{height: 200, justifyContent: 'center'}}>
                <ActivityIndicator size={'large'}/>
              </View>
              :            
              <View>
                {
                  this.props.noticeSurveyData.questions.map((row, i) => 
                    <View style={styles.switchRow} key={`notice-question-${i}`}>
                      <Switch 
                        itemColor={'#42ECA5'} 
                        label={row.label_question} 
                        data={[{"label": "Oui", "value": true}, {"label": "Non", "value": false}]}
                        from={'settings'}
                        value={row.value}
                        onChange={(value)=>this.onToggle(row.question_id, value)}/>    
                    </View>
                  )
                }
              </View>
            } */}

            <TouchableOpacity
              style={styles.termsRow}
              onPress={this.goPrivacyPolicy}
              underlayColor='#fff'>
              <View style={{justifyContent: 'center', width: Dimensions.get('window').width - 80}}>
                <Text style={styles.termsText}>Conditions générales d’utilisation</Text>
              </View>
              <View style={{justifyContent: 'center'}}>
                <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowRight.png')} style={{tintColor: '#292A3E'}}/>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.signoutRow}
              onPress={this.signOut}
              underlayColor='#fff'>
              <Text style={[commonStyles.text, styles.signoutText]}>Déconnexion</Text>
            </TouchableOpacity>          
          </ScrollView>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    noticeSurveyData: state.SurveyReducer.noticeSurveyData,
    error: state.SurveyReducer.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postNoticeSurvey: (request) => dispatch(SurveyActions.postNoticeSurvey(request.data, request.cb)),
    getNoticeSurvey: (request) => dispatch(SurveyActions.getSurvey('notice', request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);

const styles = StyleSheet.create({
  title: {
    fontFamily: 'ITCAvantGardePro-Md',
    fontSize: 18,
    color: 'white',
    marginLeft: 25,
    paddingTop: 5,
    paddingBottom: 6
  },
  switchRow: {
    backgroundColor: '#515262',
    marginTop: 1,
    paddingBottom: 8
  },
  termsRow: {
    backgroundColor: '#34E8A5',
    marginTop: 13,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 21,
    paddingRight: 16
  },
  termsText: {
    fontFamily: 'ITCAvantGardePro-Bold',
    fontSize: 16,
    color: '#292A3E',
    paddingTop: (Platform.OS === 'ios') ? 5 : 0
  },
  signoutRow: {
    backgroundColor: 'black',
    height: 56,
    marginTop: 13,
    justifyContent: 'center'
  },
  signoutText: {
    textAlign: 'center',
    paddingTop: (Platform.OS === 'ios') ? 4 : 0
  }
})