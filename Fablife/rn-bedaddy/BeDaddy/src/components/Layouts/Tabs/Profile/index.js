import React from 'react';
import { Dimensions, StyleSheet, Image, Text, View, TouchableOpacity, Platform } from 'react-native';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import commonStyles from '../../Categories/_styles';
import { UserActions } from '../../../../actions';
import Progression from './Progression'
import Informations from './Informations'
import Utils from '../../../../utils';

class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tab: 1
    }
  }

  componentWillMount() {
    this.props.screenProps.eventEmitter.addListener('NOTIFICATION-OPENED', (data)=>{
      if(data.click_action == "GENETIC"){
        this.setState({
          tab: 2
        },() => {
          const {navigate} = this.props.navigation; 
          navigate("GenticData") 
        })
      }
    });    
  }

  componentDidMount() {
    this.props.screenProps.onRef(this)
  }

  componentWillUnmount() {
    this.props.screenProps.onRef(null)
  }  

  loadData = () => {
    this.props.getDashboard({
      cb: (res) => {
        console.log('--- get dashboard')
        console.log(res);
      }
    })

    this.props.getUserDetails({
      cb: (res)=> {
        console.log('--- user details');
        console.log(res.group);
      }
    })
  }

  onSelectTab = (tabIndex) => {
    Utils.trackScreenView((tabIndex === 1) ? 'Dashboard_Open' : 'Profil_Informations')
    this.setState({
      tab: tabIndex
    })
  }

  onOpenSettings = () => {
    const {navigate} = this.props.navigation;
    Utils.trackEvent('Action', 'Parameter_Open')
    navigate("Settings")
  }

  render() {
    let selectedTab = this.state.tab

    return (
      <View style={commonStyles.container}>
        <View style={[commonStyles.header, {backgroundColor: 'transparent'}]}>
          <Text style={[commonStyles.headerTitle, {color: '#DEFB47'}]}>Profil</Text>

          <TouchableOpacity
            style={styles.settingsBtn}
            underlayColor='#fff' 
            onPress={()=>this.onOpenSettings()}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Settings.png')}/>
          </TouchableOpacity>          
        </View>  
        
        <View style={{paddingTop: 0, height: 32}}>
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              style={styles.tabBtn}
              underlayColor='#fff' 
              onPress={()=>this.onSelectTab(1)}>
              <Text style={[styles.tabText, (selectedTab == 1) && styles.selectedTab]}>Progression</Text>
            </TouchableOpacity>   
            <View style={{height: 30, width: 2, backgroundColor: '#DEFB47'}}></View>
            <TouchableOpacity
            style={styles.tabBtn}
              underlayColor='#fff' 
              onPress={()=>this.onSelectTab(2)}>
              <Text style={[styles.tabText, (selectedTab == 2) && styles.selectedTab]}>Informations</Text>
            </TouchableOpacity>    
          </View>
        </View>
        <KeyboardAwareScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={{paddingLeft: 16, paddingRight: 16}}>  
          { (selectedTab === 1) && <Progression data={this.props.dashboardData}/> }
          { (selectedTab === 2) && <Informations data={this.props.userDetailsData} navigation={this.props.navigation}/> }
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    dashboardData: state.UserReducer.dashboardData,
    userDetailsData: state.UserReducer.userDetailsData,
    error: state.UserReducer.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getDashboard: (request) => dispatch(UserActions.getDashboard(request.cb)),
    getUserDetails: (request) => dispatch(UserActions.getUserDetails(request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

const styles = StyleSheet.create({
  settingsBtn: {
    position: 'absolute', 
    top: 50, 
    right: 22
  },
  tabBtn: {
    alignItems: 'center',
    paddingTop: (Platform.OS === 'ios') ? 4 : 0,
    flex: 1
  },
  selectedTab: {
    color: 'white'
  },
  tabText: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: (Dimensions.get('window').width <= 320) ? 22: 24,
    color: '#7E7F8B',
    textAlign: 'center'
  }
})