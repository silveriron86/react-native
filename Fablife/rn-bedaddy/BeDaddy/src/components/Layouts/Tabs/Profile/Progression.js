import React from 'react'
import { ActivityIndicator, Image, Text, View, TouchableOpacity, ScrollView, Platform, Dimensions } from 'react-native'
import HTML from 'react-native-render-html';
import commonStyles from '../../Categories/_styles'
import styles from './_styles'
import WeekSummary from '../../../Widgets/Tabs/WeekSummary'
import Chart from './Chart'
import Utils from '../../../../utils'

const bmiColors = ['#FFB7FF', '#FFA6A6', '#FFC9A6', '#FFFFA6', '#A6FFA6', '#A6FFFF', '#84BFFF']
class ArrowValue extends React.Component {
  render() {
    let tintColor = (this.props.color) ? this.props.color : 'white'
    let value = this.props.value
    let style = this.props.style
    let unit = (this.props.unit) ? this.props.unit : 'kg'
    if(value === null) {
      return null
    }

    return (
      <View style={[{flexDirection: 'row'}, style]}>
        {
          (value > 0) ?
          <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/ic_arrow_upward_24px.png')} style={{tintColor: tintColor}}/> :
          <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/ic_arrow_downward_24px.png')} style={{tintColor: tintColor}}/>        
        }
        <Text style={[styles.rowTopRightText, {color: tintColor}]}>{(value === 0) ? '' : (value > 0) ? '+' : '-'} {Math.abs(value)} {unit}*</Text>
      </View>
    )
  }
}
export default class Progression extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      weightCollapsed: false,
      bmiCollapsed: false,
      waistAndHipsCollapsed: false,
      sedentarityCollapsed: false
    }
  }  

  doCollapse = (type) => {
    let state = this.state
    state[`${type}Collapsed`] = !state[`${type}Collapsed`]
    this.setState(state)
    Utils.trackEvent('Action', 'Dashboard_Card_Open')
  }

  render() {
    let data = this.props.data
    console.log(data);
    if(!data) {
      return (
        <View style={{height: 200, justifyContent: 'center'}}>
          <ActivityIndicator size={'large'}/>
        </View>
      )
    }

    let fsComplianceData = data.compliance.foodSupplements
    let mpcomplianceData = data.compliance.mealPlanning
    let complianceTitle = data.compliance.sectionTitle

    let weightCollapsed =  this.state.weightCollapsed
    let bmiCollapsed =  this.state.bmiCollapsed
    let waistAndHipsCollapsed =  this.state.waistAndHipsCollapsed
    let sedentarityCollapsed =  this.state.sedentarityCollapsed

    return (
      <View style={styles.container}>
        <ScrollView style={{paddingTop: 6}}>
          <View style={styles.blockHeader}>
            <Text style={[commonStyles.h1, {marginLeft: 7}]}>{complianceTitle}</Text>
            <TouchableOpacity
              style={styles.addBtn}
              underlayColor='#fff'>
              <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/Add.png')}/>
            </TouchableOpacity>
          </View>

          <View style={styles.grayBlock}>
            <Text style={styles.BlockTxt}>Compléments alimentaires</Text>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/FoodSupplement.png')} style={styles.gbIcon}/>
              <View style={styles.wsWrapper}>
                <WeekSummary data={fsComplianceData}/>
              </View>
            </View>
          </View>

          <View style={styles.grayBlock}>
            <Text style={styles.BlockTxt}>Repas</Text>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/MealPlanning.png')} style={styles.gbIcon}/>
              <View style={styles.wsWrapper}>
                <WeekSummary data={mpcomplianceData}/>
              </View>
            </View>
          </View>

          <View style={[styles.blockHeader]}>
            <Text style={[commonStyles.h1, {marginLeft: 7}]}>{data.measurements.sectionTitle}</Text>
            <TouchableOpacity
              style={styles.addBtn}
              underlayColor='#fff'>
              <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/Add.png')}/>
            </TouchableOpacity>
          </View> 

          <View style={[styles.commonRow, weightCollapsed && styles.ChartRow, weightCollapsed && {backgroundColor: '#34E8A5'}, (!weightCollapsed) && styles.greenRow]}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={[styles.BlockTxt, {color: weightCollapsed ? '#292A3E' : '#34E8A5'}]}>{data.measurements.weight.cardTitle}</Text>
              <ArrowValue value={data.measurements.weight.variationSinceBeginning} color={weightCollapsed ? '#292A3E' : 'white'}/>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 14}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.rowValue}>{data.measurements.weight.currentValue}</Text>
                <Text style={[styles.BlockTxt, {color: 'white', marginTop: (Platform.OS === 'ios') ? 21 : 17, marginLeft: 3}]}>kg</Text>
              </View>
              {
                (!weightCollapsed) &&                
                <TouchableOpacity
                  style={styles.arrowDownIcon}
                  underlayColor='#fff'
                  onPress={()=>this.doCollapse('weight')}>
                  <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowDown.png')} style={{tintColor: '#34E8A5'}}/>
                </TouchableOpacity>                
              }
            </View>    

            {
              weightCollapsed &&
              <View style={{paddingBottom: 20}}>
                <Chart backgroundColor={'#34E8A5'} data={data.measurements.weight.chart}/>
                {
                  (data.measurements.weight.variationSinceBeginning !== null) &&
                  <Text style={commonStyles.text}>* {data.since_beginning_legend}</Text>
                }
                
                <TouchableOpacity
                  style={styles.arrowUpIcon}
                  underlayColor='#fff'
                  onPress={()=>this.doCollapse('weight')}>
                  <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowUp.png')} />
                </TouchableOpacity>
              </View>
            }
          </View>

          <View style={[styles.commonRow, bmiCollapsed && styles.ChartRow, bmiCollapsed && {backgroundColor: '#FC706F'}, (!bmiCollapsed) && styles.redRow]}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={[styles.BlockTxt, {color: bmiCollapsed ? '#292A3E' : '#FC706F'}]}>{data.measurements.bmi.cardTitle}</Text>
              <ArrowValue value={data.measurements.bmi.variationSinceBeginning} color={bmiCollapsed ? '#292A3E' : 'white'}/>              
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 14}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.rowValue}>{data.measurements.bmi.currentValue}</Text>
                {
                  data.measurements.bmi.displayUnit &&
                  <Text style={[styles.BlockTxt, {color: 'white', marginTop: 21}]}>{data.measurements.bmi.displayUnit}</Text>
                }
                {
                  data.measurements.bmi.tagValue && 
                  <View style={styles.surpoidsMark}>
                    <Text style={styles.markText}>{data.measurements.bmi.tagValue}</Text>
                  </View>
                }
              </View>
              {
                (!bmiCollapsed) &&                
                <TouchableOpacity
                  style={styles.arrowDownIcon}
                  underlayColor='#fff'
                  onPress={()=>this.doCollapse('bmi')}>
                  <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowDown.png')} style={{tintColor: '#FC706F'}}/>
                </TouchableOpacity>                
              }              
            </View>    

            {
              bmiCollapsed &&
              <View style={{paddingBottom: 20}}>
                <Chart backgroundColor={'#FC706F'} data={data.measurements.bmi.chart}/>

                { 
                  data.measurements.bmi.chart && data.measurements.bmi.chart.series && 
                  <View>
                    {
                      data.measurements.bmi.chart.yAxis.map((y, i) => 
                        <View key={`bmi-row-${i}`} style={{flexDirection: 'row'}}>
                          <View style={[styles.bmiRect, {backgroundColor: bmiColors[i]}]}></View>
                          <Text style={commonStyles.text}>{y.name}</Text>
                        </View>
                      )                  
                    }
                    {
                      (data.measurements.bmi.variationSinceBeginning !== null) &&
                      <Text style={[commonStyles.text, {marginTop: 10}]}>* {data.since_beginning_legend}</Text>
                    }
                  </View>
                }
                
                <TouchableOpacity
                  style={styles.arrowUpIcon}
                  underlayColor='#fff'
                  onPress={()=>this.doCollapse('bmi')}>
                  <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowUp.png')} />
                </TouchableOpacity>
              </View>
            }
          </View>  

          <View style={[styles.commonRow, waistAndHipsCollapsed && styles.ChartRow, waistAndHipsCollapsed && {backgroundColor: '#7558FF'}, (!waistAndHipsCollapsed) && styles.purpleRow]}>
            <Text style={[styles.BlockTxt, {color: waistAndHipsCollapsed ? '#292A3E' : '#7558FF'}]}>{data.measurements.waistAndHips.cardTitle}</Text>
            <View style={{flexDirection: 'row', marginTop: 13}}>
              <View style={{flex: 1}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <Text style={[styles.BlockTxt, {fontSize: 13, fontFamily: 'ITCAvantGardePro-Md', color: '#FC706F'}]}>Taille</Text>
                  <ArrowValue style={{paddingRight: 8}} value={data.measurements.waistAndHips.waist.variationSinceBeginning} unit={data.measurements.waistAndHips.waist.displayUnit} color={waistAndHipsCollapsed ? '#292A3E' : 'white'}/>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Text style={[styles.rowValue, {fontSize: 40}]}>{data.measurements.waistAndHips.waist.currentValue}</Text>
                  <Text style={[styles.BlockTxt, {color: 'white', marginTop: (Platform.OS === 'ios') ? 17 : 12, marginLeft: 3}]}>{data.measurements.waistAndHips.waist.displayUnit}</Text>
                </View>
              </View>
              <View style={{width: 1, height: 48, backgroundColor: '#B0B0B0', marginLeft: 8, marginRight: 8}}></View>
              <View style={{flex: 1}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <Text style={[styles.BlockTxt, {fontSize: 13, fontFamily: 'ITCAvantGardePro-Md', color: '#34E8A5'}]}>Hanches</Text>
                  <ArrowValue style={{paddingRight: 8}} value={data.measurements.waistAndHips.hips.variationSinceBeginning} unit={data.measurements.waistAndHips.hips.displayUnit} color={waistAndHipsCollapsed ? '#292A3E' : 'white'}/>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{flexDirection: 'row', marginTop: 10}}>
                    <Text style={[styles.rowValue, {fontSize: 40}]}>{data.measurements.waistAndHips.hips.currentValue}</Text>
                    <Text style={[styles.BlockTxt, {color: 'white', marginTop: (Platform.OS === 'ios') ? 17 : 12, marginLeft: 3}]}>{data.measurements.waistAndHips.hips.displayUnit}</Text>
                  </View>
                  {
                    (!waistAndHipsCollapsed) &&                
                    <TouchableOpacity
                      style={[styles.arrowDownIcon, {marginTop: 5}]}
                      underlayColor='#fff'
                      onPress={()=>this.doCollapse('waistAndHips')}>
                      <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowDown.png')} style={{tintColor: '#7558FF'}}/>
                    </TouchableOpacity>                
                  }                     
                </View>
              </View>              
            </View>
            
            {
              waistAndHipsCollapsed &&
              <View style={{paddingBottom: 20}}>
                <Chart backgroundColor={'#7558FF'} data={data.measurements.waistAndHips.chart}/>
                {
                  (data.measurements.waistAndHips.waist.variationSinceBeginning !== null || data.measurements.waistAndHips.hips.variationSinceBeginning !== null) &&
                  <Text style={[commonStyles.text, {marginTop: 10}]}>* {data.since_beginning_legend}</Text>
                }
                <TouchableOpacity
                    style={styles.arrowUpIcon}
                    underlayColor='#fff'
                    onPress={()=>this.doCollapse('waistAndHips')}>
                    <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowUp.png')} />
                  </TouchableOpacity>
              </View>            
            }
          </View>          

          <View style={[styles.blockHeader]}>
            <Text style={[commonStyles.h1, {marginLeft: 7}]}>{data.metabolism.sectionTitle}</Text>
            <TouchableOpacity
              style={styles.addBtn}
              underlayColor='#fff'>
              <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/Add.png')}/>
            </TouchableOpacity>
          </View>

          <View style={[styles.commonRow, styles.yellowRow]}>
            <Text style={[styles.BlockTxt, {color: '#DEFB47'}]}>{data.metabolism.dailyNeedCalories.title}</Text>
            <View style={{flexDirection: 'row', marginTop: (Platform.OS === 'ios') ? 12 : 14}}>
              <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/Burn.png')}/>
              <Text style={[styles.rowValue, {fontSize: 40, marginLeft: 13, marginTop: (Platform.OS === 'ios') ? 10 : 0}]}>{data.metabolism.dailyNeedCalories.value}</Text>
              <Text style={[styles.BlockTxt, {color: 'white', marginLeft: 8, marginTop: (Platform.OS === 'ios') ? 28 : 24}]}>{data.metabolism.dailyNeedCalories.displayUnit}</Text>
            </View>
          </View> 

          <View style={{alignItems: 'center'}}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/Link.png')}/>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.yelloSubwRow}>
              <Text style={[styles.BlockTxt, {color: '#DEFB47', textAlign: 'center'}]}>{data.metabolism.basalMetabolicRate.title}</Text>
              <View style={{height: 106, justifyContent: 'center'}}>
                <View style={{alignItems: 'center'}}>
                  <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/Metabolism.png')}/>
                </View>
                {
                  (data.metabolism.basalMetabolicRate.value != null) && 
                  <Text style={[styles.BlockTxt, styles.smallTxt, {fontSize: 32, paddingTop: (Platform.OS === 'ios') ? 16 : 11}]}>{data.metabolism.basalMetabolicRate.value}</Text>
                }
              </View>
              <View style={{height: 28}}>
                <Text style={[styles.BlockTxt, styles.smallTxt]}>{data.metabolism.basalMetabolicRate.displayUnit}</Text>
                <Text style={[styles.BlockTxt, styles.smallTxt]}>utilisées</Text>
              </View>
            </View> 
            
            {/* <View style={{width: 17}}></View> */}
            <View style={styles.yelloSubwRow}>
              <Text style={[styles.BlockTxt, {color: '#DEFB47', textAlign: 'center'}]}>{data.metabolism.physicalActivity.title}</Text>
              <View style={{height: 106, justifyContent: 'center'}}>
                <View style={{alignItems: 'center'}}>
                  <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/Sport.png')}/>
                </View>
                {
                  (data.metabolism.physicalActivity.value != null) &&
                  <Text style={[styles.BlockTxt, styles.smallTxt, {fontSize: 32, paddingTop: (Platform.OS === 'ios') ? 16 : 11}]}>{data.metabolism.physicalActivity.value}</Text>
                }
              </View>
              <View style={{height: 28}}>
                <Text style={[styles.BlockTxt, styles.smallTxt]}>{data.metabolism.physicalActivity.displayUnit}</Text>
                <Text style={[styles.BlockTxt, styles.smallTxt, {marginTop: -2}]}>brûlées</Text>
              </View>
            </View>
          </View>

          <View style={[styles.commonRow, sedentarityCollapsed ? styles.chairRow : styles.blueRow]}>
            <View>
              <Text style={[styles.BlockTxt, {color: sedentarityCollapsed ? '#292A3E' : '#43CDEE'}]}>{data.sedentarity.sectionTitle}</Text>
              <View style={{flexDirection: 'row', marginTop: -2, marginLeft: 6}}>
                <View style={{width: 71, height: 71, justifyContent: 'center', alignItems: 'center'}}>
                  <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Tab/Chair.png')}/>
                </View>
                <Text style={[styles.rowValue, {fontSize: 40, marginLeft: (Dimensions.get('window').width <= 320) ? 0 : 16, marginTop: (Platform.OS === 'ios') ? 19 : 14}]}>{data.sedentarity.value}</Text>
                <View style={{flex: 1, marginLeft: 8}}>
                  <Text style={[styles.BlockTxt, {color: 'white', marginTop: 19}]}>{data.sedentarity.displayUnit} {data.sedentarity.extraUnit}</Text>
                </View>
                {
                  (!sedentarityCollapsed) &&                
                  <TouchableOpacity
                    style={[styles.arrowDownIcon, {marginTop: 33}]}
                    underlayColor='#fff'
                    onPress={()=>this.doCollapse('sedentarity')}>
                    <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowDown.png')} style={{tintColor: '#43CDEE'}}/>
                  </TouchableOpacity>                
                }                
              </View>   
            </View>
            {
              sedentarityCollapsed &&
              <View>
                <View style={{paddingLeft: 7, paddingRight: 7, marginTop: 5, paddingBottom: 20}}>
                  <HTML baseFontStyle={commonStyles.text} html={data.sedentarity.mainText.p1}></HTML>
                  <Text></Text>
                  <HTML baseFontStyle={commonStyles.text} html={data.sedentarity.mainText.p2}></HTML>
                  <Text></Text>
                  <HTML baseFontStyle={commonStyles.text} html={data.sedentarity.mainText.p3}></HTML>
                </View>
                  
                <TouchableOpacity
                  style={styles.arrowUpIcon}
                  underlayColor='#fff'
                  onPress={()=>this.doCollapse('sedentarity')}>
                  <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowUp.png')} />
                </TouchableOpacity>
              </View>
            }
          </View>          
        </ScrollView>
      </View>
    )
  }
}