import React from 'react';
import { connect } from 'react-redux';
import { Dimensions, Modal, Image, StyleSheet, Platform, Text, View, ActivityIndicator, TouchableOpacity, ScrollView } from 'react-native';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import commonStyles from '../../Categories/_styles';
import { SurveyActions } from '../../../../actions';
// import LinearGradient from 'react-native-linear-gradient';

const leftPad = Dimensions.get('window').width / 2 - 32 - 110
class GenticData extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      loading: false,
      opened: false,
      selected: null,
      selectedIndex: 0,
      body: {},
      success: false,
      submitted: false,
      error: ''
    };
  }

  goBack = () => {
    this.props.navigation.goBack()
  }
  
  componentDidMount() {
    this.setState({
      loading: true,
      body: {},
      error: ''
    }, ()=> {
      this.props.getForm({
        type: 'genetic-user',
        id: 'genetic-user',
        cb: (res) => {
          console.log('--- get genetic user');
          console.log(res);
          if(res.status === 402) {
            this.setState({
              loading: false,
              error: "L'abonnement Bedaddy n'inclus pas l'analyse du génome."
            })            
          }else {
            if(res.questions) {
              let body = this.state.body;
              res.questions.forEach(r=> {
                body[r.question_id] = r.value;
              });

              this.setState({
                loading: false,
                body: body
              });            
            }else {
              this.setState({
                loading: false
              });            
            }
          }
        }
      });
    });     
  }

  cancelAdd = () => {
    this.setState({
      opened: false
    });
  }

  okAdd = () => {
    let body = this.state.body;
    body[this.state.selected.question_id] = this.state.selected.ui_config.choices[this.state.selectedIndex].value;
    this.setState({
      body: body,
      opened: false
    });
  }

  onAdd = (question) => {
    console.log(question);
    selectedIndex = 0;
    if(question.value) {
      question.ui_config.choices.forEach((r, i) => {
        if(r.value == question.value) {
          selectedIndex = i;
        }
      });
    }
    console.log(selectedIndex);
    this.setState({
      opened: true,
      selected: question,
      selectedIndex: selectedIndex
    });
  }

  checkDisabled = () => {
    let body = this.state.body;
    let ret = false;
    Object.keys(body).forEach(i => {
      if(!body[i]) {
        ret = true;
      }
    });

    return ret;
  }

  onValidate = () => {
    if(this.refs.scrollview) {
      this.refs.scrollview.scrollTo(0);
    }    

    this.setState({
      loading: true
    }, () => {
      this.props.postForm({
        data: {
          form: this.state.body,
          type: 'genetic-user',
          id: 'genetic-user',
        },
        cb: (res)=> {
          console.log(res);
          this.setState({
            loading: false,
            success: true,
            submitted: true
          });
        }
      });
    });
  }

  close = () => {
    this.setState({
      success: false
    })
  }

  render() {
    let data = this.props.geneticFormData;
    let canSubmit = false;
    let choices = [];
    let disabled = this.checkDisabled();
    let body = this.state.body;
    if(data) {
      canSubmit = (data.completion < 100);
    }

    if(this.state.selected) {
      this.state.selected.ui_config.choices.forEach(r => {
        choices.push(r.value == "NULL" ? r.label : r.value)
      });
    }
    return (
      <View
        style={commonStyles.container}>
        {
          this.state.loading &&
            <View style={commonStyles.loading}>
              <ActivityIndicator size={'large'}/>
            </View>
        }
        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.success}>
          <View style={{justifyContent: 'center', alignItems: 'center', marginTop: Dimensions.get('window').height/2 - 92}}>
            <View style={styles.msgWrapper}>
              <TouchableOpacity
                style={[commonStyles.closeButton, styles.closeBtn]}
                onPress={this.close}
              >
                <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Close.png')} style={commonStyles.imgClose} />
              </TouchableOpacity>            
              <View style={styles.msg}>
                <Text style={[commonStyles.text, styles.msgText]}>
                  Bravo !
                </Text>
                <Text style={[commonStyles.text, styles.msgText]}>
                  Vous avez saisi vos résultats génétiques. 
                </Text>
                <Text style={[commonStyles.text, styles.msgText]}>                
                  Nous allons pouvoir calculer votre Nutrinome®.                
                </Text>
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.opened}>
          <View style={{justifyContent: 'center', alignItems: 'center', marginTop: Dimensions.get('window').height/2 - 102}}>
            <View style={styles.selWrapper}>
              <ScrollPicker
                dataSource={choices}
                selectedIndex={this.state.selectedIndex}
                renderItem={(data, index, isSelected) => {
                  console.log(data, index, isSelected);
                }}
                onValueChange={(data, selectedIndex) => {
                  this.setState({
                    selectedIndex: selectedIndex
                  })
                }}
                wrapperHeight={165}
                wrapperWidth={281}
                wrapperBackground={'transparent'}
                itemHeight={48}
                highlightColor={'#ffffff'}
                highlightBorderWidth={1}
                activeItemColor={'#ffffff'}
                itemColor={'#ffffff45'}
              >   
              {/* <LinearGradient
                colors={['transparent', '#ffffff30']}
                style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  bottom: 0,
                  height: 80,
                  zIndex: 1,
                }}
              />               */}
              
              </ScrollPicker>
              <View style={styles.selFooter}>
                <TouchableOpacity
                  style={styles.footerBtn}
                  underlayColor='#fff' 
                  onPress={()=>this.cancelAdd()}>
                  <Text style={styles.cancelText}>Annuler</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.footerBtn}
                  underlayColor='#fff' 
                  onPress={()=>this.okAdd()}>
                  <Text style={styles.okText}>OK</Text>
                </TouchableOpacity>
              </View>     
            </View>    
          </View>
        </Modal>

        <View style={[commonStyles.header, {backgroundColor: 'transparent'}]}>
          <Text style={[commonStyles.headerTitle, {color: '#DEFB47'}]}>Mes données génétiques</Text>

          <TouchableOpacity
            style={[commonStyles.closeButton, {marginTop: -2}]}
            underlayColor='#fff' 
            onPress={()=>this.goBack()}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowLeft.png')}/>
          </TouchableOpacity>
        </View>
        
        {
          (this.state.error !== '') 
          ?
          <View style={styles.contentWrapper}>
            <Text style={styles.errMsg}>{this.state.error}</Text>
          </View>
          :
          <ScrollView 
          ref = "scrollview"
          keyboardDismissMode="on-drag" 
          keyboardShouldPersistTaps='always' 
          style={commonStyles.container} 
          scrollEnabled={!this.state.loading}>
            <View style={styles.contentWrapper}>
              <Text style={commonStyles.text}>Merci de renseigner vos résultats génétiques ci-desous : </Text>
              <View style={styles.titleBar}>
                <Text style={[commonStyles.text, styles.titleText]}>NOM DU GÈNE</Text>
                <View style={{flex: 1}}>
                  <Text style={[commonStyles.text, styles.titleText, {textAlign: 'center'}]}>VOTRE GÉNOTYPE</Text>
                </View>
              </View>
              {
                (Platform.OS === "android") && this.state.loading && (!data || !data.questions || data.questions.length == 0) && 
                <View style={{height: Dimensions.get('window').height - 250}}></View>
              }          
              {
                (data.questions && data.questions.length > 0) &&
                  data.questions.map((question, i) =>
                    <View key={`q_${i}`} style={styles.qRow}>
                      <Text style={[commonStyles.text, styles.qText]}>{question.label}</Text>
                        {/* <View style={styles.addBtn}>
                          <View style={styles.qValue}>
                            <Text style={styles.qValueText}>{question.value}</Text>
                          </View>
                        </View> */}                  
                      {
                        (question.value) ?
                        <TouchableOpacity
                          style={styles.addBtn}
                          underlayColor='#fff' 
                          disabled={!canSubmit}
                          onPress={()=>this.onAdd(question)}>
                          <View style={styles.qValue}>
                            <Text style={styles.qValueText}>{body[question.question_id]}</Text>
                          </View>
                        </TouchableOpacity>                       
                        :
                        <TouchableOpacity
                          style={styles.addBtn}
                          underlayColor='#fff' 
                          onPress={()=>this.onAdd(question)}>
                          <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/Button/Add.png')}/>
                        </TouchableOpacity>      
                      }
                    </View>
                  )
              }
            </View>

            {
              (data.questions && (data.questions.length > 0) && canSubmit && (this.state.submitted === false)) ?
                <View style={[commonStyles.btnWrapper, {marginTop: 15}]}>
                  <TouchableOpacity
                    disabled={disabled}
                    style={[commonStyles.btn, {backgroundColor: disabled ? "#FFFFFF35" : "#DEFB47"}]}
                    underlayColor='#fff' 
                    onPress={()=>this.onValidate()}>
                    <Text style={commonStyles.btnText}>Valider</Text>
                  </TouchableOpacity>
                </View>     
                :
                <View style={{height: 10}}></View>
            }
          </ScrollView>
        }
      </View>
    )
  }
};

const mapStateToProps = (state) => {
  return {  
    geneticSurveyData: state.SurveyReducer.geneticSurveyData,
    geneticFormData: state.SurveyReducer['genetic-user_genetic-userFormData']
  };
};
  
const mapDispatchToProps = dispatch => {
  return {
    getForm: (request) => dispatch(SurveyActions.getForm(request.type, request.id, request.cb)),
    getSurvey: (request) => dispatch(SurveyActions.getSurvey(request.type, request.cb)),
    postForm: (request) => dispatch(SurveyActions.postForm(request.data, request.cb))
  };
};

const styles = StyleSheet.create({
  contentWrapper: {
    paddingLeft: (Dimensions.get('window').width <= 320) ? 16 : 25,
    paddingRight: (Dimensions.get('window').width <= 320) ? 16 : 25,
    paddingBottom: 15
  },
  titleBar: {
    flexDirection: 'row', 
    paddingLeft: leftPad, 
    marginTop: 25, 
    marginBottom: 5
  },
  qRow: {
    backgroundColor: "#FFFFFF10",
    paddingLeft: leftPad,
    marginBottom: 2,
    height: 39,
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  qText: {
    marginTop: (Platform.OS === 'ios') ? 13 : 11,
    flex: 1,
    fontSize: (Dimensions.get('window').width <= 320) ? 14 : 15,
  },
  titleText: {
    color: '#DEFB47',
    fontSize: (Dimensions.get('window').width <= 320) ? 14 : 15
  },  
  addBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'    
  },
  selWrapper: {
    width: 281,
    height: 204,
    backgroundColor: '#292A3E',
    borderRadius: 5
  },
  selFooter: {
    flexDirection: 'row',
    height: 39,
    backgroundColor: '#FFFFFF17',
    borderTopWidth: 1, 
    borderBottomWidth: 1,
    borderColor: '#979797',
  },
  footerBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelText: {
    fontFamily: 'ITCAvantGardePro-Demi',
    color: 'white',
    fontSize: 15,
    marginTop: (Platform.OS === 'ios') ? 8 : 0
  },
  okText: {
    fontFamily: 'ITCAvantGardePro-Demi',
    color: '#DEFB47',
    fontSize: 15,
    marginTop: (Platform.OS === 'ios') ? 8 : 0
  },
  qValue: {
    width: 29,
    height: 29,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 3,
    justifyContent: 'center',
  },
  qValueText: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
    marginTop: (Platform.OS === 'ios') ? 8 : 0
  },
  msgWrapper: {
    width: 281,
    height: 183,    
    backgroundColor: '#48485A', 
    borderRadius: 2,
    paddingTop: 33
  },
  msg: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  msgText: {
    width: 227,
    textAlign: 'center'
  },
  closeBtn: {
    top: 0, 
    right: 0, 
    width: 60, 
    height: 60, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  errMsg: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: "#FC706F",
    marginTop: 50,
    fontSize: 16,
    lineHeight: 20,
    textAlign: 'center'
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(GenticData);