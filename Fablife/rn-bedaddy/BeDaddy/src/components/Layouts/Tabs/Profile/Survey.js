import React from 'react';
import { connect } from 'react-redux';
import { Image, StyleSheet, Platform, Text, View, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import commonStyles from '../../Categories/_styles';
import SurveyConstants from '../../../../constants/SurveyConstants';
import { SurveyActions } from '../../../../actions';
import styles from '../Followup/_styles'
import Form from '../Followup/Form'

class Survey extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      modalVisible: false,
      form: 0,
      color: '',
      title: ''
    }
  }

  _hasData = () => {
    return (this.props.signupSurveyData && Object.keys(this.props.signupSurveyData).length > 0)
  }

  onPressLink = (category, i) => {
    this.scroll.scrollTo({x: 0, y: 0, animated: true});
    
    let data = this.props[`${category.id}FormData`]  
    console.log(this.props);
    if(data.questions) {
      this.setState({
        modalVisible: true,
        form: category.id,
        color: SurveyConstants.CATEGORIES[i].color,
        title: category.label
      })
    }
  }

  goBack = () => {
    this.props.navigation.goBack()
  }  

  closeModal = (onlyClose) => {
    if(onlyClose === true) {
      this.setState({
        modalVisible: false
      })
    }else {
      this.props.getSurvey({
        type: 'signup',
        cb: () => {
          this.setState({
            modalVisible: false
          })
        }
      })  
    }
  }

  render() {
    const signupSurveyData = this.props.signupSurveyData
    let rows = []

    if(this._hasData()) {

      signupSurveyData.forms.forEach((category, i) => { 
        rows.push( 
          <TouchableOpacity
            key={`category-${i}`}
            style={[styles.button, styles.btnGray]}
            underlayColor='#fff' 
            onPress={()=>this.onPressLink(category, i)}
          >
            <View style={styles.btnWrapper}>
              <View style={styles.leftWrapper}>
                <View style={[styles.active, {backgroundColor: SurveyConstants.CATEGORIES[i].color, width: (Dimensions.get('window').width - 48)}]}></View>
                <View style={styles.iconWrapper}>
                  <Image resizeMode='stretch' source={SurveyConstants.CATEGORIES[i].icon} style={styles.imgIcon} />
                </View>                  
                <Text style={styles.btnText}>{category.label}</Text>
              </View>            
              <View style={styles.arrowWrapper}>
                <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowDown.png')} style={styles.imgArrow} />
              </View>
            </View>
          </TouchableOpacity>
        )
      })
    }
    
    return (
      <View style={commonStyles.container}>

        <View style={[commonStyles.header, {backgroundColor: 'transparent'}]}>
          <Text style={[commonStyles.headerTitle, {color: '#DEFB47'}]}>Questionnaire de profil</Text>

          <TouchableOpacity
            style={[commonStyles.closeButton, {marginTop: -2}]}
            underlayColor='#fff' 
            onPress={()=>this.goBack()}>
            <Image resizeMode='stretch' source={require('../../../../../assets/img/ic/ArrowLeft.png')}/>
          </TouchableOpacity>
        </View>
        { 
          this.state.modalVisible &&
          <View style={styles.modalWrapper}>
            <Form id={this.state.form} type={'signup'} color={this.state.color} title={this.state.title} onClose={this.closeModal} readOnly={true}></Form>
          </View>
        }
        <ScrollView 
          ref={(c) => {this.scroll = c}}
          keyboardDismissMode="on-drag" 
          keyboardShouldPersistTaps='always' 
          style={commonStyles.container} 
          scrollEnabled={!this.state.modalVisible}>
          {
            this._hasData() &&
            <View style={{flex: 1, marginTop: 25, marginLeft: 24, marginRight: 24, marginBottom: 14}}>
              {rows}
            </View>
          }
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profileFormData: state.SurveyReducer.signup_profileFormData,
    healthFormData: state.SurveyReducer.signup_healthFormData,
    physical_activityFormData: state.SurveyReducer.signup_physical_activityFormData,
    sleep_fatigueFormData: state.SurveyReducer.signup_sleep_fatigueFormData,
    stressFormData: state.SurveyReducer.signup_stressFormData,
    moral_wellbeingFormData: state.SurveyReducer.signup_moral_wellbeingFormData,
    memory_concentrationFormData: state.SurveyReducer.signup_memory_concentrationFormData,
    oxydative_stressFormData: state.SurveyReducer.signup_oxydative_stressFormData,      
    signupSurveyData: state.SurveyReducer.signupSurveyData,
    error: state.SurveyReducer.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getForm: (request) => dispatch(SurveyActions.getForm(request.type, request.id, request.cb)),
    getSurvey: (request) => dispatch(SurveyActions.getSurvey(request.type, request.cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Survey);