import { Dimensions, StyleSheet, Platform } from 'react-native';

let windowWidth = Dimensions.get('window').width
export default StyleSheet.create({
	container: {
		marginTop: 5
	},
	addBtn: {
		justifyContent: 'center',
		opacity: 0,
		width: 0,
		height: 0,
		// width: 24,
		// height: 24,
		paddingLeft: 5.5,
		borderRadius: 3,
		borderWidth: 1,
		borderColor: 'white',
		marginTop: (Platform.OS === 'ios') ? -6 : 0
	},
	blockHeader: {
		flexDirection: 'row', 
		justifyContent: 'space-between', 
		paddingBottom: 10,
		marginTop: 16
	},
	grayBlock: {
		backgroundColor: '#515262',
		borderRadius: 3,
		padding: 16,
		marginBottom: 8    
	},
	BlockTxt: {
		fontFamily: 'ITCAvantGardePro-Bold',
		fontSize: (windowWidth <= 320) ? 15 : 17,
		color: '#292A3E',
		marginTop: (Platform.OS === 'ios') ? 0 : -4
	},
	gbIcon: {
		marginTop: 12
	},
	wsWrapper: {
		paddingLeft: 10,
		flex: 1
	},
	commonRow: {
		borderRadius: 3,
		borderLeftWidth: 6,
		height: 107,
		marginBottom: 8,
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 18
	},
	greenRow: {
		backgroundColor: '#2A4F52',
		borderColor: '#34E8A5'
	},
	redRow: {
		backgroundColor: '#523747',
		borderColor: '#FC706F'
	},
	purpleRow: {
		backgroundColor: '#373364',
		height: 126,
		borderColor: '#7558FF'
	},
	yellowRow: {
		backgroundColor: '#4C533F',
		height: 117,
		borderColor: '#DEFB47',
		marginBottom: 0
	},
	yelloSubwRow: {
		flex: 1,
		borderRadius: 3,
		borderTopWidth: 6,
		height: 213,
		marginBottom: 12,
		backgroundColor: '#4C533F',
		borderColor: '#DEFB47',
		padding: 12,
		justifyContent: 'space-between'
	},
	blueRow: {
		backgroundColor: '#2D4A61',
		height: 122,
		borderColor: '#43CDEE'
	},
	rowTopRightText: {
		fontFamily: 'ITCAvantGardePro-Md',
		fontSize: 13,
		color: 'white',
		marginLeft: 3,
		marginTop: (Platform.OS === 'ios') ? 0 : -4
	},
	rowValue: {
		fontFamily: 'ITCAvantGardePro-Bold',
		fontSize: 48,
		color: 'white',
		marginTop: (Platform.OS === 'ios') ? 0 : -12
	},
	surpoidsMark: {
		marginTop: 9,
		backgroundColor: '#FFE200',
		borderRadius: 12,
		height: 24,
		justifyContent: 'center',
		marginLeft: 14
	},
	markText: {
		fontFamily: 'ITCAvantGardePro-Demi',
		fontSize: (windowWidth <= 320) ? 10 : 14,   
		color: '#292A3E',
		textAlign: 'center',
		paddingLeft: (windowWidth <= 320) ? 10 : 15,
		paddingRight: (windowWidth <= 320) ? 10 : 15,
		paddingTop: 7
	},
	smallTxt: {
		color: 'white', 
		fontSize: 14, 
		textAlign: 'center'    
	},
	chairRow: {
		backgroundColor: '#43CDEE',
		borderLeftWidth: 0,
		height: 'auto'
	},
	arrowDownIcon: {
		marginTop: 0,
		padding: 16,
		right: -16,
		position: 'absolute'
	},
	arrowUpIcon: {
		position: 'absolute', 
		bottom: 0, 
		right: -10, 
		// tintColor: 'white'    
		padding: 15
	},
	ChartRow: {
		borderLeftWidth: 0,
		height: 'auto',
		paddingLeft: 16
	},
	bmiRect: {
		width: 8, 
		height: 8, 
		marginTop: 5,
		marginRight: 10
	}
})