import React from 'react';
import { connect } from 'react-redux';
import { AsyncStorage, StyleSheet, Platform, Text, View, TouchableOpacity } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import BottomNavigation from '../../Widgets/react-native-bottom-navigation';
import Followup from './Followup/index';
import Alimentation from './Alimentation/index';
// import Cook from './Alimentation/Cook';
import Preference from '../Categories/Preference'
import Recipe from './Alimentation/Recipe';
import Menu from './Alimentation/Menu';
import Eatingoutside from './Alimentation/Eatingoutside';
import Shopping from './Shopping/index';
import Profile from './Profile/index';
import Settings from './Profile/Settings';
import Survey from './Profile/Survey';
import GenticData from './Profile/GenticData';
import PrivacyPolicy from '../Login/PrivacyPolicy';
import commonStyles from '../Categories/_styles';
import { SurveyActions } from '../../../actions';
import Utils from '../../../utils'

const FOLLOWUP_TIMER_INTERVAL = 60000
let followupAvailTimer = null

const AlimentationNavigator = createStackNavigator({
  Home: {
    screen: Alimentation,
    navigationOptions: { header : null }
  },
  Cook: {
    screen: Preference,
    navigationOptions: { header : null }
  },
  Recipe: {
    screen: Recipe,
    navigationOptions: { header : null }
  },
  Menu: {
    screen: Menu,
    navigationOptions: { header : null }
  },
  Eatingoutside: {
    screen: Eatingoutside,
    navigationOptions: { header : null }
  }
},
{
  headerMode: 'screen',
  initialRouteName: 'Home'
})

const AlimentationContainer = createAppContainer(AlimentationNavigator)

const FollowupNavigator = createStackNavigator({
  Home: {
    screen: Followup,
    navigationOptions: { header : null }
  }
},
{
  headerMode: 'screen',
  initialRouteName: 'Home'
})

const FollowupContainer = createAppContainer(FollowupNavigator)

const ProfileNavigator = createStackNavigator({
  Home: {
    screen: Profile,
    navigationOptions: { header : null }
  },
  Settings: {
    screen: Settings,
    navigationOptions: { header : null }
  },
  Survey: {
    screen: Survey,
    navigationOptions: { header : null }
  },  
  PrivacyPolicy: {
    screen: PrivacyPolicy,
    navigationOptions: { header : null }
  },
  GenticData: {
    screen: GenticData,
    navigationOptions: { header : null }
  }
},
{
  headerMode: 'screen',
  initialRouteName: 'Home'
})

const ProfileContainer = createAppContainer(ProfileNavigator)

class Main extends React.Component {
  constructor() {
    super()
    this.state = {
      initialTab: 1,
      currentTab: 1
    }
  }

  componentWillMount = () => {
    this.props.getSurvey({
      type: 'feedback',
      cb: (res) => {
        // if(res.forms) {
        //   res.forms.forEach((category, idx) => {
        //     this.props.getForm({
        //       type: 'feedback',
        //       id: category.id,
        //       cb: (form) => {}
        //     })  
        //   })
        // }
      }
    });

    this.props.getSurvey({
      type: 'signup',
      cb: (res) => {
        if(res.forms) {
          res.forms.forEach((category, idx) => {
            this.props.getForm({
              type: 'signup',
              id: category.id,
              cb: (form) => {}
            })  
          })
        }
      }
    });    

    this.props.screenProps.addListener('NOTIFICATION-OPENED', (data)=>{
      console.log('*** notification opened');
      console.log(data);
      //FOLLOWUP, FOODSUPPLEMENT, REPORT, GENETIC
      if(data.click_action == "MEALPLANNING" || data.click_action == "FOODSUPPLEMENT") {
        this.setState({
          currentTab: 1
        });
      }else if(data.click_action == "FOLLOWUP"){
        this.setState({
          currentTab: 0
        });
      }else if(data.click_action == "SHOPPING"){
        this.setState({
          currentTab: 2
        });
      }else if(data.click_action == "GENETIC"){
        this.setState({
          currentTab: 3
        });
      }
    });    
  }

  componentDidMount() {
    followupAvailTimer = setInterval(() => {
      this.props.getSurvey({
        type: 'feedback',
        cb: (res) => {
          console.log('feedback survey');
          console.log(res);
        }
      })
    }, FOLLOWUP_TIMER_INTERVAL)
  }

  componentWillUnmount() {
    clearInterval(followupAvailTimer)
  }

  isAvailable = () => {
    let data = this.props.feedbackSurveyData
    return (Object.keys(data).length > 0 && data.completion < 100)
  }

  onChangeTab = (arg) => {
    this.setState({
      currentTab: arg.i
    }, () => {
      if(arg.i === 0) {
        Utils.trackScreenView( 'Update_Open')
      }else if(arg.i === 1) {
        Utils.trackScreenView( 'Mealplanning_View_Time')
      }else if(arg.i === 2) {
        // Shopping Tab
        Utils.trackScreenView( 'Shopping_List')
        this.shoppingTab.loadData()
      }else {
        AsyncStorage.getItem('visitedProfile', (err, visited) => {
          if(visited != 'true') {
            Utils.trackScreenView('Dashboard_Open')
            AsyncStorage.setItem('visitedProfile', 'true')
          }
        })
        this.profileTab.loadData()
      }  
    });
  }

  onRefProfile = (ref) => {
    this.profileTab = ref
  }

  render() {
    return (
      <View style={commonStyles.container}>
        <BottomNavigation
          style={{height: 100}}
          initialPage={this.state.initialTab}
          page={this.state.currentTab}
          activeColor="white"
          inactiveColor="#9B9B9B"
          tabBarColor="#394648"
          borderWidth={0}
          rippleColor="transparent"
          maskColor="transparent"
          inactiveFontSize={10}
          activeFontSize={10}
          displayLabels={1}
          animated={false}
          animatedTabSwitch={false}
          onChangeTab={this.onChangeTab}
          >
          <FollowupContainer
            tabRippleColor="transparent"
            tabMaskColor="transparent"
            tabLabel="Suivi"
            tabIcon={require('../../../../assets/img/ic/Tab/Suivi.png')}
            badgeValue={this.isAvailable() ? 1 : 0}
            badgeStyle={{backgroundColor: '#FC706F'}}
            />
        
          <AlimentationContainer
            tabRippleColor="transparent"
            tabMaskColor="transparent"
            tabLabel="Alimentation"
            tabIcon={require('../../../../assets/img/ic/Tab/Alimentation.png')}
            />
        
          <Shopping
            onRef={ref => (this.shoppingTab = ref)}
            tabRippleColor="transparent"
            tabMaskColor="transparent"
            tabLabel="Courses"
            tabIcon={require('../../../../assets/img/ic/Tab/Courses.png')}
            />
        
          <ProfileContainer
            tabRippleColor="transparent"
            tabMaskColor="transparent"
            tabLabel="Profil"
            screenProps={{ rootNavigation: this.props.navigation, eventEmitter: this.props.screenProps, onRef: this.onRefProfile }}
            tabIcon={require('../../../../assets/img/ic/Tab/Profil.png')}
            />
        </BottomNavigation>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {  
    feedbackSurveyData: state.SurveyReducer.feedbackSurveyData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getForm: (request) => dispatch(SurveyActions.getForm(request.type, request.id, request.cb)),
    getSurvey: (request) => dispatch(SurveyActions.getSurvey(request.type, request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderTopWidth: 0
  }
});
