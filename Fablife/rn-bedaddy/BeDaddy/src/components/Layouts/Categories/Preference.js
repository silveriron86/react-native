import React from 'react';
import { AsyncStorage, ActivityIndicator, Platform, Text, View, TouchableOpacity, Dimensions, Alert, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {connect} from 'react-redux';
import _ from 'lodash';
import {SurveyActions} from '../../../actions';
import Switch from '../../Widgets/Switch';
import Radio from '../../Widgets/Radio';
import Date from '../../Widgets/Date';
import Input from '../../Widgets/Input';
import Check from '../../Widgets/Check';
import Wheel from '../../Widgets/Wheel';
import Autocomplete from '../../Widgets/Autocomplete';
import Utils from '../../../utils';
import commonStyles from './_styles';

class Preference extends React.Component {
  constructor(props) {
    console.log('constructor');
    super(props);
    this.form_id = 'food_habits'
    this.state = { 
      date: '',
      val: '',
      loading: false,
      posting: false,
      isShowAdd: false
    }
  }

  checkAutoComplete = () => {
    this.props.food_habitsFormData.questions.forEach((question, idx) => {
      if(question.ui === "autocomplete" && question.value && question.value.length > 0) {
        this.setState({
          isShowAdd: true
        })
      }
    });    
  }

  componentWillMount = () => {
    // console.log('componentWillMount', _.has(this.props.food_habitsFormData, 'questions'));
    // if(_.has(this.props.food_habitsFormData, 'questions')) {
    //   this.checkAutoComplete();
    // }else {
      this.setState({
        loading: true
      }, ()=> {
        this.props.getForm({
          type: 'preference',
          id: 'food_habits',
          cb: (res) => {
            console.log(res);
            this.checkAutoComplete();
            this.setState({
              loading: false
            })
          }
        })  
      }) 
    // }
  }

  doClose = () => {
    this.props.navigation.goBack()
  }    

  changeFormData = (question_id, value) => {
    const { navigation } = this.props;

    this.props.updateFormData({
      data: {
        type: 'preference',
        form_id: this.form_id, 
        question_id: question_id, 
        value: value
      },
      cb: (res) => {
        this.forceUpdate()
      }
    })
  }

  onPressNext = () => {
    const { navigation } = this.props;
    let requiredFields = 0
    let filledFields = 0
    let requiredField = ''
    let questions = this.props[`${this.form_id}FormData`].questions
    let data = this.props[`${this.form_id}FormData`].data
    let form = {}
    let notVaild = false

    questions.forEach((question) => {
      if(Utils.checkVisibleQuestion(data, questions, question) === true) {  
        form[question.question_id] = question.value
        if(question.ui !== "checkbox" && question.required === true) {
          requiredFields++
          if(question.value !== null) {
            filledFields++            
          }else {
            if(requiredField === '') {
              requiredField = question.label
            }
          }
        }

        if(question.ui === 'stepper') {
          if(question.ui_config.min > question.value || question.value > question.ui_config.max) {
            notVaild = true
          }
        }        
      }
    })

    if(notVaild) {
      return;
    }

    if(filledFields === requiredFields) {
      this.setState({
        posting: true
      });

      if(form['FOOD_EXCLUSION'] === null || form['FOOD_EXCLUSION'].length === 0) {
        form['FOOD_EXCLUSION'] = "[]";
      }
      console.log(form);
      this.props.postForm({
        data: {
          type: 'preference',
          form: form,
          id: this.form_id
        },
        cb: (res)=> {
          console.log('posted preference');
          console.log(res);
          this.setState({
            posting: false
          },()=> {
            if(res.status && res.status === 400) {
              setTimeout(() => {
                Alert.alert(
                  'Le formulaire contient des réponses invalides. Veuillez vérifier votre saisie.', '',
                  [{text: 'OK'}],
                  { cancelable: false }
                )
              }, 100)
            }else {
              // Completed
              AsyncStorage.setItem('completedPreference', '1')

              const nav = this.props.navigation;
              if(nav.getParam('from') === 'alimentation') {
                this.doClose()
              }else {
                Utils.trackEvent('Action', 'Onbaording_Finish')
                nav.navigate("TabMain")              
              }
            }
          })
        }
      })
    }else {
      Alert.alert(
        `${requiredField} renseignez le champs`, "",
        [{text: 'OK'}],
        { cancelable: false }
      )
    }
  }

  _renderButtonView = (itemColor) => {
    return (
      <View style={commonStyles.content}>
        <View style={commonStyles.btnWrapper}>
          <TouchableOpacity
            disabled={false}
            style={[commonStyles.btn, {backgroundColor: itemColor}]}
            underlayColor='#fff' 
            onPress={()=>this.onPressNext()}>
            <Text style={commonStyles.btnText}>Valider</Text>
          </TouchableOpacity>
        </View>          
      </View>         
    )
  }  

  toggleVisibleAdd = (value, question_id) => {
    this.setState({
      isShowAdd: value
    },() => {
      if(value === false) {
        this.changeFormData(question_id, []);
      }
    })
  }

  render() {
    const { navigation } = this.props;
    const itemColor = '#DEFB47';
    let rows = [];
    if(this.props[`${this.form_id}FormData`].questions) {     
      let questions = this.props[`${this.form_id}FormData`].questions
      let data = this.props[`${this.form_id}FormData`].data
      questions.forEach((question, idx) => {
        if(Utils.checkVisibleQuestion(data, questions, question) === true) {
          let row = null
          let extraStyle = null

          if(!question.ui) {
            row = ( 
              <View style={commonStyles.row}>
                <Text style={commonStyles.label}>{question.label_question}</Text>
                {
                  question.tooltip &&
                  <Text style={commonStyles.tooltipText}>{question.tooltip}</Text>
                }                  
              </View>
            )
          }else if (question.ui === "datepicker") {
            row = ( 
              <Date 
                label={question.label_question} 
                max={question.ui_config.max} 
                value={question.value} 
                tooltip={question.tooltip}
                required={question.required}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              /> 
            )
          }else if(question.ui === "stepper") {
            row = ( 
              <Input 
                label={question.label_question} 
                placeholder={`0 ${(question.unit) ? question.unit.short_name : ''}`} 
                tooltip={question.tooltip}
                min={question.ui_config.min}
                max={question.ui_config.max}
                precision={question.ui_config.precision}
                required={question.required}
                value={question.value}
                suffix={(question.unit) ? ` ${question.unit.short_name}` : ''}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              /> 
            )
          }else if(question.ui === "radio") {
            row = ( 
              <Radio 
                itemColor={itemColor} 
                label={question.label_question} 
                tooltip={question.tooltip}
                data={question.ui_config.choices}
                required={question.required}
                value={question.value}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "yesno") {
            extraStyle = {
              paddingBottom: 15
            }            
            row = ( 
              <Switch 
                itemColor={itemColor} 
                label={question.label_question} 
                tooltip={question.tooltip}
                data={question.ui_config.choices}
                required={question.required}
                value={question.value}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "checkbox") {
            row = ( 
              <Check
                id={question.question_id}
                label={question.label_question}
                tooltip={question.tooltip}
                value={question.value}
                itemColor={itemColor}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "wheel") {
            row = (
              <Wheel 
                label={question.label_question}
                placeholder={`0 ${(question.unit) ? question.unit.short_name : ''}`} 
                tooltip={question.tooltip}
                config={question.ui_config}
                unit={question.unit}
                itemColor={itemColor}
                required={question.required}
                value={question.value}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "autocomplete") {
            if(this.state.loading === false) {
              extraStyle = commonStyles.acRowWrap
              row = (
                <View>
                  <Switch 
                    itemColor={itemColor} 
                    label={question.label_question} 
                    tooltip={question.tooltip}
                    data={[{label: 'Oui', value: true}, {label: 'Non', value: false}]}
                    value={this.state.isShowAdd}
                    onChange={(value)=>this.toggleVisibleAdd(value, question.question_id)}
                  />
                  {
                    this.state.isShowAdd && 
                    <Autocomplete 
                      itemColor={itemColor}
                      url={question.ui_config.path}
                      value={question.value ? question.value : []}
                      onChange={(value)=>this.changeFormData(question.question_id, value)}
                    />
                  }
                </View>
              )
            }
          }else {
            console.log('unknown', question)
          }

          rows.push(
            <View key={`question_${idx}`} style={[commonStyles.preferRowWrap, extraStyle]}>
              {row}
            </View>
          )
        }
      })
    }

    const inputAccessoryViewID = "uniqueID";
    return (
      <View style={commonStyles.container}>   
        {
          (this.state.loading || this.state.posting) &&
          <View style={commonStyles.loading}>
            <ActivityIndicator size={'large'}/>
          </View>
        }   
        <View style={[commonStyles.header, {backgroundColor: 'transparent'}]}>
          <Text style={[commonStyles.headerTitle, {color: '#DEFB47'}]}>Préférences alimentaires</Text>
          {
            (this.props.navigation.getParam('from') === 'alimentation') &&
            <TouchableOpacity
              style={[commonStyles.closeButton, {marginTop: -2}]}
              underlayColor='#fff' 
              onPress={()=>this.doClose()}>
              <Image resizeMode='stretch' source={require('../../../../assets/img/ic/ArrowLeft.png')}/>
            </TouchableOpacity>
          }
        </View>
        <KeyboardAwareScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={commonStyles.container} enableResetScrollToCoords={false}>
          <View style={(Platform.OS === "android") && {minHeight: Dimensions.get('window').height - 50}}>
            {rows}
            {
              (rows.length > 0 && this.state.loading === false) &&
              <View>
                {/* {
                  (Platform.OS === 'ios') && 
                  <InputAccessoryView nativeID={inputAccessoryViewID}>
                    <View style={{width: '100%', height: 15}}></View>
                    { this._renderButtonView(itemColor) }
                  </InputAccessoryView>                 
                } */}
                { this._renderButtonView(itemColor) }
              </View>        
            }        
          </View>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    food_habitsFormData: state.SurveyReducer.preference_food_habitsFormData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateFormData: (request) => dispatch(SurveyActions.updateFormData(request.data, request.cb)),
    getForm: (request) => dispatch(SurveyActions.getForm(request.type, request.id, request.cb)),
    postForm: (request) => dispatch(SurveyActions.postForm(request.data, request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Preference);