import React from 'react';
import { ActivityIndicator, ScrollView, StyleSheet, Image, Text, View, TouchableOpacity, Dimensions } from 'react-native';
import {connect} from 'react-redux';
import {SurveyActions} from '../../../actions';
import SurveyConstants from '../../../constants/SurveyConstants';
import commonStyles from './_styles';

let windowWidth = Dimensions.get('window').width
class CategoryList extends React.Component {
  willFocus = this.props.navigation.addListener(
    'willFocus',
    payload => {
      this.forceUpdate();
    }
  )

  onPressLink = (category, index) => {
    const {navigate} = this.props.navigation;
    navigate("Form", {      
      page_no: `0${index+1}`,
      form_id: category.form_id,
      title: category.title,
      color: category.color
    });    
  }

  componentDidMount = () => {
    // if(this.props.loadedSurveys < 8) {
    //   let cnt = 0
    //   this.setState({
    //     loading: true
    //   })
    //   SurveyConstants.CATEGORIES.forEach((category, idx) => {
    //     this.props.getForm({
    //       type: 'signup',
    //       id: category.form_id,
    //       cb: (res) => {
    //         if(this.props.error !== null) {
    //           cnt++
    //           this.props.setLoadedSurveys(cnt)
    //         }
    //       }
    //     })   
    //   })
    // }
  }

  render() {
    let rows = []
    let InputingSurveyIndex = parseInt(this.props.InputingSurveyIndex, 10)
    SurveyConstants.CATEGORIES.forEach((category, i) => {
      // console.log(category);
      let filledPercent = 0
      if(i === InputingSurveyIndex) {
        let data = this.props[`${category.form_id}FormData`]
        if(data.questions) {
          filledPercent = 1
        }
      }

      if(i >= InputingSurveyIndex) {
        rows.push( 
          <TouchableOpacity
            key={`category-${i}`}
            style={[styles.button, styles.btnGray]}
            underlayColor='#fff' 
            onPress={()=>this.onPressLink(category, i)}
            disabled={(i>InputingSurveyIndex)}
          >
            <View style={styles.btnWrapper}>
              <View style={styles.leftWrapper}>
                <View style={[styles.active, {backgroundColor: category.color, width: (Dimensions.get('window').width - 50) * filledPercent}]}></View>
                <View style={styles.iconWrapper}>
                  <Image resizeMode='stretch' source={category.icon} style={styles.imgIcon} />
                </View>                  
                <Text style={styles.btnText}>{category.title}</Text>
              </View>            
              <View style={styles.arrowWrapper}>
                <Image resizeMode='stretch' source={require('../../../../assets/img/ic/ArrowDown.png')} style={styles.imgArrow} />
              </View>
            </View>
          </TouchableOpacity>
        )
      }
    })

    return (
      <View style={styles.container}>
        {/* {
          (this.props.loadedSurveys == 0) &&
          <View style={commonStyles.loading}>
            <ActivityIndicator size={'large'}/>
          </View>
        } */}
        <Text style={styles.title}>Bienvenue</Text>
        <Text style={styles.text}>En répondant de manière honnête et précise à quelques questions, nous pourrons calculer vos besoins nutritionnels précis et définir votre programme alimentaire pour booster votre fertilité.</Text>
        <ScrollView style={[styles.container, {paddingLeft: 0, paddingRight: 0}]}>
          {rows}    
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    loadedSurveys: state.SurveyReducer.loadedSurveys,
    InputingSurveyIndex: state.SurveyReducer.InputingSurveyIndex,
    profileFormData: state.SurveyReducer.signup_profileFormData,
    healthFormData: state.SurveyReducer.signup_healthFormData,
    physical_activityFormData: state.SurveyReducer.signup_physical_activityFormData,
    sleep_fatigueFormData: state.SurveyReducer.signup_sleep_fatigueFormData,
    stressFormData: state.SurveyReducer.signup_stressFormData,
    moral_wellbeingFormData: state.SurveyReducer.signup_moral_wellbeingFormData,
    memory_concentrationFormData: state.SurveyReducer.signup_memory_concentrationFormData,
    oxydative_stressFormData: state.SurveyReducer.signup_oxydative_stressFormData,    
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getForm: (request) => dispatch(SurveyActions.getForm(request.type, request.id, request.cb)),
    setLoadedSurveys: (val) => dispatch(SurveyActions.setLoadedSurveys(val))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryList);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292A3E',
    paddingLeft: 25,
    paddingRight: 25
  },
  title: {
    fontFamily: 'ITCAvantGardePro-Bold',
    color: '#DEFB47',
    fontSize: 28,
    width: '100%',
    marginTop: 51
  },
  text: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: 'white',
    fontSize: 15,
    marginTop: 5,
    marginBottom: 26,
    width: '100%',
    lineHeight: 19,
    letterSpacing: 0.2
  },
  button: {
    width: "100%",
    minHeight: 48,
    borderColor: "transparent",
    borderRadius: 2,
    borderWidth: 0,
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 4},
    marginBottom: 8
  },
  btn1: {
    backgroundColor: "#43CDEE"
  },
  btn2: {
    backgroundColor: "#34E8A5"
  },
  btn3: {
    backgroundColor: "#FC706F"
  },  
  btn4: {
    height: 64
  },
  btnGray: {
    backgroundColor: "#515262"
  },
  btnWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnText: {
    fontFamily: 'ITCAvantGardePro-Bold',
    color: '#292A3E',
    fontSize: (windowWidth <= 320) ? 16 : 17,
    lineHeight: 15,
    paddingTop: 19,
    paddingBottom: 10,
    width: Dimensions.get('window').width - 117 - 36 - 8
  },
  leftWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  iconWrapper: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    width: 67
  },
  imgIcon: {
    width: 35,
    height: 35
  },
  arrowWrapper: {
    height: '100%',
    alignItems: 'flex-end',
    justifyContent: 'flex-end'    
  },
  imgArrow: {
    width: 28,
    height: 28,
    marginRight: 8,
    marginBottom: 9
  },
  active: {
    height: '100%',
    position: 'absolute'
  }
});