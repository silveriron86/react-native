import React from 'react';
import { AsyncStorage, ActivityIndicator, Image, Text, View, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {connect} from 'react-redux';
import {SurveyActions} from '../../../actions';
import Switch from '../../Widgets/Switch';
import Radio from '../../Widgets/Radio';
import Date from '../../Widgets/Date';
import Input from '../../Widgets/Input';
import Check from '../../Widgets/Check';
import Wheel from '../../Widgets/Wheel';
import Utils from '../../../utils';
import commonStyles from './_styles';

console.disableYellowBox = true;
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      date: '',
      val: '',
      loading: false,
      showErrorMsg: false
    }
  }

  componentDidMount() {
    this.setState({
      loading: true,
      showErrorMsg: false
    })

    const { navigation } = this.props;
    const form_id = navigation.getParam('form_id');
    this.props.getForm({
      type: 'signup',
      id: form_id,
      cb: (res) => {
        this.setState({
          loading: false
        })
      }
    })  
  }

  close = () => {
    this.props.navigation.goBack()
  }

  changeFormData = (question_id, value) => {
    const { navigation } = this.props;
    const form_id = navigation.getParam('form_id');

    this.props.updateFormData({
      data: {
        type: 'signup',
        form_id: form_id, 
        question_id: question_id, 
        value: value
      },
      cb: (res) => {
        this.forceUpdate()
      }
    })
  }

  onPressNext = () => {
    const { navigation } = this.props;
    const form_id = navigation.getParam('form_id');
    let requiredFields = 0
    let filledFields = 0
    let requiredField = ''
    let questions = this.props[`${form_id}FormData`].questions
    let data = this.props[`${form_id}FormData`].data
    let form = {}
    let notVaild = false
    let isValidInputs = false;

    questions.forEach((question, qIndex) => {
      if(Utils.checkVisibleQuestion(data, questions, question) === true) {  
        form[question.question_id] = question.value
        if(question.ui !== "checkbox" && question.required === true) {
          requiredFields++
          if(question.value !== null) {
            filledFields++            
          }else {
            if(requiredField === '') {
              requiredField = question.label
            }
          }
        }
        
        if(question.ui === 'stepper') {
          if(question.required === true) {
            if(question.ui_config.min > question.value || question.value > question.ui_config.max) {
              notVaild = true
            }
          }else {
            if(question.conditions.length > 0) {
              if(question.value === "") {
                delete form[question.question_id];
              }else {
                isValidInputs = true;
              }
              
              if((qIndex < questions.length && questions[qIndex+1].ui !== 'stepper') || qIndex === question.length-1) {
                notVaild = !isValidInputs;
                isValidInputs = false;
                if(notVaild === true) {
                  Alert.alert(
                    'Erreur', 'Le formulaire contient des réponses invalides. Veuillez vérifier votre saisie.',
                    [{text: 'OK'}],
                    { cancelable: false }
                  )
                  return;
                }
              }
            }else {
              if(question.value === "") {
                delete form[question.question_id];
              }
            }
          }
        }
      }
    })

    if(notVaild) {
      this.setState({
        showErrorMsg: true
      },()=> {
        Alert.alert(
          'Erreur', 'Le formulaire contient des réponses invalides. Veuillez vérifier votre saisie.',
          [{text: 'OK'}],
          { cancelable: false }
        ) 
      })     
      return;
    }  

    if(filledFields === requiredFields) {
      this.setState({
        loading: true
      })
      
      this.props.postForm({
        data: {
          type: 'signup',
          form: form,
          id: form_id
        },
        cb: (res)=> {
          this.setState({
            loading: false
          },()=> {
            console.log(res);
            if(res.status && res.status === 400) {
              setTimeout(() => {
                Alert.alert(
                  'Erreur', 'Le formulaire contient des réponses invalides. Veuillez vérifier votre saisie.',
                  [{text: 'OK'}],
                  { cancelable: false }
                )
              }, 100)
            }else {
              let completedSignupSurveys = this.props.InputingSurveyIndex + 1
              this.props.completedSurvey(completedSignupSurveys)
              AsyncStorage.setItem('completedSignupSurveys', completedSignupSurveys.toString())

              if(form_id === 'oxydative_stress') {
                navigation.navigate("Preference")
              }else {

                if(form_id === 'profile') {
                  Utils.trackEvent('Action', 'Onboarding_Start')
                }
                navigation.goBack()
              }  
            }
          })
        }
      })
    }else {
      Alert.alert(
        'Erreur', `${requiredField} renseignez le champs`,
        [{text: 'OK'}],
        { cancelable: false }
      )
    }
  }

  _renderButtonView = (itemColor) => {
    return (
      <View style={commonStyles.content}>
        <View style={commonStyles.btnWrapper}>
          <TouchableOpacity
            disabled={false}
            style={[commonStyles.btn, {backgroundColor: itemColor}]}
            underlayColor='#fff' 
            onPress={()=>this.onPressNext()}>
            <Text style={commonStyles.btnText}>Suivant</Text>
          </TouchableOpacity>
        </View>          
      </View>         
    )
  }  

  render() {
    const { navigation } = this.props;
    const { showErrorMsg } = this.state;
    const page_no = navigation.getParam('page_no');
    const form_id = navigation.getParam('form_id');
    const itemColor = navigation.getParam('color');
    const itemTitle = navigation.getParam('title');

    let rows = [];
    if(this.props[`${form_id}FormData`].questions) {   
      let questions = this.props[`${form_id}FormData`].questions 
      let data = this.props[`${form_id}FormData`].data;
      
      questions.forEach((question, idx) => {
        if(Utils.checkVisibleQuestion(data, questions, question) === true) {          
          if(!question.ui) {
            rows.push( 
              <View style={commonStyles.row} key={`question_${idx}`}>
                <Text style={commonStyles.label}>{question.label_question}</Text>
                {
                  question.tooltip &&
                  <Text style={commonStyles.tooltipText}>{question.tooltip}</Text>
                }  
              </View>
            )
          }else if (question.ui === "datepicker") {
            rows.push( 
              <Date 
                key={`question_${idx}`}
                label={question.label_question} 
                tooltip={question.tooltip}          
                max={question.ui_config.max} 
                value={question.value} 
                required={question.required}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              /> 
            )
          }else if(question.ui === "stepper") {            
            rows.push( 
              <Input 
                key={`question_${idx}`}
                label={question.label_question} 
                placeholder={`0 ${(question.unit) ? question.unit.short_name : ''}`} 
                tooltip={question.tooltip}
                isShowableError={showErrorMsg}
                min={question.ui_config.min}
                max={question.ui_config.max}
                precision={question.ui_config.precision}
                required={question.required}
                value={question.value}
                suffix={(question.unit) ? ` ${question.unit.short_name}` : ''}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              /> 
            )
          }else if(question.ui === "radio") {
            rows.push( 
              <Radio 
                key={`question_${idx}`}
                itemColor={itemColor} 
                label={question.label_question} 
                tooltip={question.tooltip}           
                data={question.ui_config.choices}
                required={question.required}
                value={question.value}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "yesno") {
            rows.push( 
              <Switch 
                key={`question_${idx}`}
                itemColor={itemColor} 
                label={question.label_question}       
                data={question.ui_config.choices}
                tooltip={question.tooltip}
                required={question.required}
                value={question.value}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "checkbox") {
            rows.push( 
              <Check
                key={`question_${idx}`}
                id={question.question_id}
                label={question.label_question}       
                value={question.value}
                itemColor={itemColor}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else if(question.ui === "wheel") {
            rows.push(
              <Wheel 
                key={`question_${idx}`}
                label={question.label_question}
                tooltip={question.tooltip}          
                placeholder={`0 ${(question.unit) ? question.unit.short_name : ''}`} 
                config={question.ui_config}
                unit={question.unit}
                itemColor={itemColor}
                required={question.required}
                value={question.value}
                onChange={(value)=>this.changeFormData(question.question_id, value)}
              />
            )
          }else {
            console.log('unkonw', question)
          }
        }
      })
    }

    const inputAccessoryViewID = "uniqueID";
    return (
      <View style={commonStyles.container}>              
        {
          (rows.length === 0 || this.state.loading) &&
          <View style={commonStyles.loading}>
            <ActivityIndicator size={'large'}/>
          </View>
        }

        <View style={[commonStyles.header, {backgroundColor: itemColor}]}>
          <Text style={commonStyles.headerTitle}>{itemTitle}</Text>
          <TouchableOpacity
            style={commonStyles.closeButton}
            onPress={this.close}
          >
            <Image resizeMode='stretch' source={require('../../../../assets/img/ic/Close.png')} style={commonStyles.imgClose} />
          </TouchableOpacity>
        </View>
        <KeyboardAwareScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always' style={commonStyles.container} enableResetScrollToCoords={false}>
          <Text style={commonStyles.PageNo}>{page_no} / 08</Text>          
          {rows}
          {
            (rows.length > 0 && this.state.loading === false) &&
            <View>
              {
                // (Platform.OS === 'ios') && 
                // <InputAccessoryView nativeID={inputAccessoryViewID}>
                //   <View style={{width: '100%', height: 15}}></View>
                //   { this._renderButtonView(itemColor) }
                // </InputAccessoryView>                 
              }
              { this._renderButtonView(itemColor) }
            </View>        
          }        
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    InputingSurveyIndex: state.SurveyReducer.InputingSurveyIndex,
    profileFormData: state.SurveyReducer.signup_profileFormData,
    healthFormData: state.SurveyReducer.signup_healthFormData,
    physical_activityFormData: state.SurveyReducer.signup_physical_activityFormData,
    sleep_fatigueFormData: state.SurveyReducer.signup_sleep_fatigueFormData,
    stressFormData: state.SurveyReducer.signup_stressFormData,
    moral_wellbeingFormData: state.SurveyReducer.signup_moral_wellbeingFormData,
    memory_concentrationFormData: state.SurveyReducer.signup_memory_concentrationFormData,
    oxydative_stressFormData: state.SurveyReducer.signup_oxydative_stressFormData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    completedSurvey: (index) => dispatch(SurveyActions.completedSurvey(index)),
    updateFormData: (request) => dispatch(SurveyActions.updateFormData(request.data, request.cb)),
    getForm: (request) => dispatch(SurveyActions.getForm(request.type, request.id, request.cb)),
    postForm: (request) => dispatch(SurveyActions.postForm(request.data, request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);