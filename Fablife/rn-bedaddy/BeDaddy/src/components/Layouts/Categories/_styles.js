import { StyleSheet, Platform, Dimensions } from 'react-native';

let windowWidth = Dimensions.get('window').width
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292A3E'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "rgba(0, 0, 0, 0.4)",
    height: Dimensions.get('window').height,
    zIndex: 99999
  },
  header: {
    backgroundColor: "#43CDEE",
    minHeight: 88
  },
  headerTitle: {
    fontFamily: 'ITCAvantGardePro-Bold',
    marginLeft: 25,
    marginTop: 52,
    marginBottom: 5,
    fontSize: (windowWidth <= 320) ? 25 : 28,
    lineHeight: 28,
    color: '#292A3E',
    width: Dimensions.get('window').width - 90,
  },
  headline: {
    fontFamily: 'ITCAvantGardePro-Bold',
    fontSize: 28,
    color: '#DEFB47',
  },
  closeButton: {
    position: "absolute",
    top: 51,
    right: 20
  },
  imgClose: {
    width: 28,
    height: 28
  },
  content: {
    paddingLeft: 25,
    paddingRight: 25    
  },
  btnWrapper: {
    width: '100%',
    alignItems: 'center',
    marginTop: 45,
    marginBottom: 35
  },
  btn: {
    backgroundColor: "#43CDEE",
    width: 160,
    height: 47,
    borderColor: "transparent",
    borderRadius: 2,
    borderWidth: 0,
    alignItems: 'center',
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 4}
  },
  btnText: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: "#292A3E",
    fontSize: 16,
    marginTop: (Platform.OS === 'ios') ? 18 : 13
  },
  PageNo: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: '#B0B0B0',
    fontSize: 15,
    width: '100%',
    lineHeight: 48,
    height: 48,
    paddingTop: 3,
    justifyContent: 'center',
    alignItems: 'center', 
    textAlign: 'center'     
  },
  row: {
    paddingLeft: 23,
    paddingRight: 23,
    marginTop: 16
  },
  label: {
    fontFamily: 'ITCAvantGardePro-Bold',
    fontSize: 15,
    color: 'white'
  },
  inputWrapper: {
    width: '100%',
    minHeight: 52
  },
  input: {
    fontFamily: 'ITCAvantGardePro-Bold',
    color: 'white',
    fontSize: 15,
    paddingTop: 12,
    paddingLeft: 3,
    paddingBottom: 10,
    borderBottomWidth: (Platform.OS === 'ios') ? 1 : 0,
    borderBottomColor: 'white'  
  },  
  inputError: {
    color: 'red',
    marginTop: -8
  },
  date: {
    width: '100%'
  },
  switchRowWrap: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 5,
    paddingBottom: 5
  },
  switchLabel: {
    paddingRight: 5,
    marginTop: (Platform.OS === 'ios') ? 0 : -3
  },
  switchWrap: {
    borderRadius: 32,
    paddingLeft: 4,
    paddingRight: 4,
    height: 32
  },
  radioGroupWrap: {
    marginTop: 10,
    marginBottom: 10
  },
  radioBtn: {
    marginTop: 10,
  },
  radioLabel: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 15, 
    color: 'white',
    marginLeft: 8,
    marginTop: 2
  },
  checkBox: {
    alignSelf: 'flex-start'
  },
  checkBoxLabel: {
    fontFamily: 'ITCAvantGardePro-Bk',
    color: 'white',
    fontSize: 15,
    marginLeft: 10,
    marginTop: (Platform.OS === 'ios') ? 4 : 1
  },
  checkWrapper: {
    flexDirection: 'row',
    alignSelf: 'flex-start'
  },
  selectInput: {
    flexDirection: 'row',
    height: 38,
    backgroundColor: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: 'white'      
  },  
  wheelPlaceholder: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 15,
    color: '#9B9B9B',
    position: "absolute",
    marginTop: 12,
    marginLeft: 3,
    borderColor: 'red'
  },
  acRowWrap: {
    marginTop: 11,
    marginBottom: 11,
    paddingBottom: 5
  },
  text: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 15,
    lineHeight: 19,
    letterSpacing: 0.2,
    color: 'white'
  },
  searchModalWrap: {
    paddingTop: 32, 
    paddingBottom: 0,
    flex: 1
  },
  searchWrap: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'space-between',
    paddingLeft: 16,
    paddingRight: 16
  },
  searchTexthWrap: {
    alignItems: 'flex-end', 
    justifyContent: 'center', 
    height: 50
  },
  searchIcon: {
    position: "absolute",
    width: 14,
    height: 14,
    marginLeft: 10,
    marginTop: 11
  },
  searchText: {
    fontFamily: 'ITCAvantGardePro-Bk',
    borderColor: '#656565', 
    borderWidth: 1, 
    borderRadius: 18, 
    color: 'white', 
    fontSize: 15, 
    letterSpacing: 0.2, 
    lineHeight: 19,
    width: Dimensions.get('window').width - 32 - 62,
    height: 36,
    paddingLeft: 30,
    paddingRight: 5
  },
  canelBtnWrap: {
    alignItems: 'flex-end', 
    justifyContent: 'center', 
    height: 40
  },
  preferRowWrap: {
    backgroundColor: '#3E3F51', 
    marginBottom: 1
  },
  withColsRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  acRowCol: {
    height: 32, 
    justifyContent: 'center'
  },
  h1: {
    fontFamily: 'ITCAvantGardePro-Md',
    color: 'white',
    fontSize: 18,
    letterSpacing: 0
  },
  h2: {
    fontFamily: 'ITCAvantGardePro-Bold',
    color: 'white',
    fontSize: 25,
    textAlign: 'center',
    letterSpacing: -0.87
  },
  tooltipText: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontStyle: 'italic',
    color: 'gray',
    fontSize: 14
  }
});