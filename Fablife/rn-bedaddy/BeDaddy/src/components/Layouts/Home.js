import React from 'react';
import { ActivityIndicator, AsyncStorage, View } from 'react-native';
import {connect} from 'react-redux';
import commonStyles from './Categories/_styles';
import {SurveyActions} from '../../actions';
import Utils from '../../utils';

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true
    }
  }

  _goPage = (page) => {
    const {navigate} = this.props.navigation;
    this.setState({
      loading: false
    },()=> {
      navigate(page)            
    })
  }

  componentWillMount = () => {
    Utils.getLocalStorage('accessToken').then((token)=> {
      if(!token) {
        this._goPage("Slideshow")
        return
      }
      
      Utils.checkRoute(this)
    })
  }

  render() {
    return (
        <View style={commonStyles.container}>
          <View style={commonStyles.loading}>
            <ActivityIndicator size={'large'}/>
          </View>
        </View>        
    )
  }
}

const mapStateToProps = (state) => {
  return {
    signupSurveyData: state.SurveyReducer.signupSurveyData,
    preferenceSurveyData: state.SurveyReducer.preferenceSurveyData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    completedSurvey: (index) => dispatch(SurveyActions.completedSurvey(index)),
    getSurvey: (request) => dispatch(SurveyActions.getSurvey(request.type, request.cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);