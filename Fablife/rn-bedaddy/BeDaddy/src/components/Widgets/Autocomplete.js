import React from 'react';
import { Image, Modal, StyleSheet, Text, TextInput, ScrollView, View, Keyboard, TouchableOpacity } from 'react-native';
import {connect} from 'react-redux';
import commonStyles from '../Layouts/Categories/_styles'
import Check from './Check';
import {SurveyActions} from '../../actions';

class Autocomplete extends React.Component { 
  constructor(props) {
    super(props)

    let values = this.props.value;
    if(values && typeof values === 'string') {
      values = [];
    }
    this.state = {
      modalVisible: false,
      searchData: [],
      values: values.slice(0),
      selectedValues: values.slice(0)
    }
    this.updateValue(values);
  }

  componentWillMount = () => {
    // this._getSearchData('')
  }

  _getSearchData = (search) => {
    this.props.getSearchData({
      url: this.props.url,
      search: search,
      cb: (res)=> {
        this.setState({
          searchData: Array.isArray(res) ? res : []
        })
      }
    })    
  }

  onChangeText = (search) => {
    this.setState({ search })
    this._getSearchData(search)
  }  

  openModal = () => {
    this.setState({
      values: this.state.selectedValues.slice(0)
    })
    this.setModalVisible(true);
  }

  setModalVisible = (visible) => {
    this.setState({modalVisible: visible});
  }

  selectSearchRow = (row, value) => {
    Keyboard.dismiss()
    
    let values = this.state.values 
    let addable = true
    
    let pos = this._getCheckedPos(row)
    if(pos > -1) {
      values.splice(pos, 1)
    }else {
      values.push(row)
    }
    this.setState({values})
  }

  onValidate = () => {
    let values = this.state.values.slice(0)

    this.setState({
      selectedValues: values
    })
    this.updateValue(values)
    this.setModalVisible(false)
  }

  updateValue = (values) => {
    let ids = []
    if(values.length > 0) {
      values.forEach((v) => {
        ids.push(v._id)
      })
    }
    this.props.onChange(`[${ids.toString()}]`)
  }

  removeItem = (idx) => {
    this.state.selectedValues.splice(idx, 1)
    this.state.values.splice(idx, 1)
    this.setState({
      values: this.state.values,
      selectedValues: this.state.selectedValues
    },()=> {
      this.updateValue(this.state.values)
    })
  }

  _getCheckedPos = (row) => {
    let pos = -1
    let values = this.state.values
    for(let i=0; i<values.length; i++) {
      if(values[i].name === row.name) {
        pos = i
        break
      }
    }
    // let pos = this.state.values.indexOf( row )
    return pos
  }

  render() {
    let rows = []
    let searchData = this.state.searchData
    let itemColor = this.props.itemColor
    if(searchData && searchData.length) {
      searchData.forEach((row, idx)=> {
        let key = `search_row_${idx}`
        rows.push(
          <View key={key} style={[commonStyles.preferRowWrap, {paddingBottom: 12, paddingTop: 0}]}>
            <Check
              id={key}
              label={row.name}
              value={this._getCheckedPos(row) > -1 ? true : false}
              itemColor={itemColor}
              onChange={(value)=>this.selectSearchRow(row, value)}
            />
          </View>
        )
      })
    }
    return (
      <View style={commonStyles.row}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {}}>
          <View style={commonStyles.container}>
            <View style={commonStyles.searchModalWrap}>
              <View style={{height: 50}}>
                <View style={commonStyles.searchWrap}>
                  <View style={commonStyles.searcTexthWrap}>
                  <Image source={require('../../../assets/img/ic/Search.png')} style={commonStyles.searchIcon} />
                    <TextInput
                      placeholder={'Search'}
                      placeholderTextColor={'#656565'}
                      style={commonStyles.searchText}
                      value={this.state.search}
                      autoCapitalize='none'
                      onChangeText={this.onChangeText}
                    />
                  </View>
                  <View style={commonStyles.canelBtnWrap}>
                    <TouchableOpacity                  
                      onPress={() => {
                        this.setModalVisible(false);
                      }}>
                      <Text style={{color: itemColor}}>Annuler</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              <ScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='always'>
                {rows}  
                {
                  (rows.length > 0) &&
                  <View style={[commonStyles.btnWrapper, {marginTop: 15}]}>
                    <TouchableOpacity
                      disabled={false}
                      style={[commonStyles.btn, {backgroundColor: itemColor}]}
                      underlayColor='#fff' 
                      onPress={()=>this.onValidate()}>
                      <Text style={commonStyles.btnText}>Valider</Text>
                    </TouchableOpacity>
                  </View>          
                }                   
              </ScrollView>
            </View>
          </View>
        </Modal>

        <TouchableOpacity
          style={{marginBottom: 10}}
          underlayColor='#fff' 
          onPress={()=>this.openModal()}>
            <Text style={[commonStyles.text, {color: this.props.itemColor, fontFamily: 'ITCAvantGardePro-Demi'}]}>+ Ajouter un aliment</Text>
        </TouchableOpacity>    

        {
          (this.state.selectedValues.length > 0) &&
          <View style={{paddingTop: -5, paddingBottom: 5}}>
            {
              this.state.selectedValues.map((item, i) =>
                <View style={commonStyles.withColsRow} key={`ac_${i}`}>
                  <View style={commonStyles.acRowCol}>
                    <Text style={commonStyles.text}>{item.name}</Text>
                  </View>
                  <TouchableOpacity
                    style={commonStyles.acRowCol}
                    underlayColor='#fff' 
                    onPress={()=>this.removeItem(i)}
                  >
                    <Image resizeMode='stretch' source={require('../../../assets/img/ic/Close.png')} style={commonStyles.imgClose} />
                  </TouchableOpacity>
                </View>  
              )       
            }
          </View>     
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.SurveyReducer.error,
    searchData: state.SurveyReducer.searchData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getSearchData: (req) => dispatch(SurveyActions.getSearchData(req.url, req.search, req.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Autocomplete);