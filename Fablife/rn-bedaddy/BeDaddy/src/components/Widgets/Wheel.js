import React from 'react';
import { StyleSheet, Text, View, Platform } from 'react-native';
// import SelectInput from 'react-native-select-input-ios';
import SelectInput from '@tele2/react-native-select-input';
import commonStyles from '../Layouts/Categories/_styles'
import SimplePicker from 'react-native-simple-picker';
import Picker from 'react-native-simple-modal-picker'

export default class Wheel extends React.Component { 
  constructor(props) {
    super(props)

    let items = []
    let config = props.config
    let unit = props.unit
    let options = [];
    let labels = [];
    for(let i = config.min; i <= config.max; i+= config.precision) {
      let label = `${i.toString()}${(unit.short_name) ? ' ' + unit.short_name : ''}`;
      items.push({
        value: i,
        label: label
      })
      options.push(i);
      labels.push(label);
    }    
    this.state = {
      val: this.props.value,
      items: items,
      labels: labels,
      options: options
    }   
  }

  onSelect = (option) => {
    this.setState({
      val: option,
    },()=> {
      this.props.onChange(option)    
    });
  }

  render() {
    let initialOptionIndex = 0;
    if(this.state.val) {
      this.state.options.forEach((opt, index) => {
        if(opt === this.state.val) {
          initialOptionIndex = index
        }
      })      
    }
    console.log(initialOptionIndex);

    return (
      <View style={commonStyles.row}>
        <Text style={[commonStyles.text, (this.props.type === 'followup') && {color: '#292A3E'}]}>{this.props.label}{(this.props.required) && '*'}</Text>
        {
          this.props.tooltip &&
          <Text style={commonStyles.tooltipText}>{this.props.tooltip}</Text>
        }        
        <View style={commonStyles.inputWrapper}> 
          {/* {
            (this.state.val === '') &&
            <Text style={[commonStyles.wheelPlaceholder, (this.props.type === 'followup') && {color: '#292A3E'}]}>{this.props.placeholder}</Text>
          } */}
          
          {/* <SelectInput
            value={this.state.val}
            options={this.state.items}
            onSubmitEditing={(val) => {
                this.setState({val})
                this.props.onChange(val)
              }
            }
            submitKeyText='Valider'
            cancelKeyText=''
            buttonsTextColor={this.props.itemColor}
            style={commonStyles.selectInput}
            labelStyle={commonStyles.input}
          />           */}

          {/* <SelectInput
            options={this.state.items}
            placeholder={this.props.placeholder}
            value={this.state.val}
            labelStyle={styles.differentSelectLabel}
            valueStyle={[styles.differentSelectValue, (this.props.type === 'followup') && {color: '#292A3E'}]}
            onChange={(val) => {
              console.log(val)
              this.setState({val})
              this.props.onChange(val)
            }}
            renderArrowIcon={()=> {
              return null
            }}
            disabled={(this.props.readOnly === true) ? true : false}
          /> */}
          <View style={styles.selector}>
            <Text
              style={styles.differentSelectValue}
              onPress={() => {
                if(Platform.OS === 'ios') {
                  this.refs.picker.show();
                }else {
                  this.simplePicker.setModalVisible(true)
                }
              }}
            >
                {(this.state.val) ? `${this.state.val.toString()}${(this.props.unit.short_name) ? ' ' + this.props.unit.short_name : ''}` : this.props.placeholder}
            </Text>          
          </View>
          
          {
            (Platform.OS === 'ios') ?
            <SimplePicker
              ref={'picker'}
              options={this.state.options}
              labels={this.state.labels}
              initialOptionIndex={initialOptionIndex}
              confirmText='Confirmer'
              cancelText='Annuler'
              onSubmit={(option) => {
                this.onSelect(option)
              }}
            />
            :
            <Picker 
              ref={instance => this.simplePicker = instance} 
              data={this.state.items} 
              label={'label'} 
              value={'value'}
              onValueChange={(value) => this.onSelect(value)} />
          }
        </View>     
      </View>   
    )
  }
}

const styles = StyleSheet.create({
  selector: {
    width: '100%',
    paddingVertical: 10,
    marginTop: 8,
    marginBottom: 18,
    borderBottomWidth: 1,
    borderBottomColor: 'white'
  },
  differentSelectLabel: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 15,
    color: '#9B9B9B',
  },
  differentSelectValue: {
    fontFamily: 'ITCAvantGardePro-Bk',
    fontSize: 15,
    color: 'white'
  },
});