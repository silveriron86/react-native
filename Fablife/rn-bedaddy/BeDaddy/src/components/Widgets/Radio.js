import React from 'react';
import { Text, View } from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from './RadioButton/SimpleRadioButton';
import commonStyles from '../Layouts/Categories/_styles'

export default class Radio extends React.Component { 
  constructor(props) {
    super(props)

    let index = ''
    this.props.data.forEach((row, idx) => {
      if(row.value === this.props.value) {
        index = idx
      }
    })
    this.state = {
      value: this.props.value,
      index: index
    }
  }

  onPress = (value, index) => {
    this.setState({
      value: value,
      index: index
    })
    
    this.props.onChange(value)
  }

  render() {
    return (
      <View style={commonStyles.row}>
        <Text style={[commonStyles.label, (this.props.type === 'followup') && {color: '#292A3E'}]}>{this.props.label}{(this.props.required) && '*'}</Text>
        {
          this.props.tooltip &&
          <Text style={commonStyles.tooltipText}>{this.props.tooltip}</Text>
        }        
        <View style={commonStyles.radioGroupWrap}>
          <RadioForm
            ref="radioForm"
            radio_props={this.props.data}
            initial={this.state.index}
            formHorizontal={false}
            labelHorizontal={true}
            buttonColor={(this.props.type === 'followup') ? '#292A3E' : 'white'}
            buttonSize={20}
            buttonOuterSize={20}
            selectedButtonColor={this.props.itemColor}
            labelColor={(this.props.type === 'followup') ? '#292A3E' : 'white'}
            selectedLabelColor={'white'}
            animation={true}
            radioStyle={commonStyles.radioBtn}
            labelStyle={[commonStyles.radioLabel, (this.props.type === 'followup') && {color: '#292A3E'}]}
            onPress={this.onPress}
            disabled={(this.props.readOnly === true) ? true : false}
          />
        </View>
      </View>
    )
  }
}