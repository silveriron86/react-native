import React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import FlipToggle from 'react-native-flip-toggle-button';
import commonStyles from '../Layouts/Categories/_styles'

export default class Switch extends React.Component { 
  constructor(props) {
    super(props)
    this.state = {
      toggled: (this.props.value) ? true : false
    }
  }

  render() {
    return (
      <View style={[commonStyles.row, (this.props.from === 'settings') && {paddingLeft: 21, paddingRight: 16}]}>
        <View style={[commonStyles.switchRowWrap]}>
          <View style={{flex: 1}}>
            <Text style={[commonStyles.label, commonStyles.switchLabel, (this.props.type === 'followup') && {color: '#292A3E'}, (this.props.from === 'settings') && {fontFamily: 'ITCAvantGardePro-Bk'}]}>{this.props.label}{(this.props.required) && '*'}</Text>
            {
              this.props.tooltip &&
              <Text style={commonStyles.tooltipText}>{this.props.tooltip}</Text>
            }                
          </View>
          <View style={[commonStyles.switchWrap, {backgroundColor: (this.state.toggled) ? this.props.itemColor : '#939395'}]}>
            <FlipToggle
              value={this.state.toggled}
              buttonWidth={69}
              buttonHeight={32}
              buttonRadius={25}
              sliderHeight={25}
              sliderWidth={25}
              sliderRadius={69}
              onLabel={this.props.data[0].label}
              offLabel={this.props.data[1].label}
              buttonOnColor={this.props.itemColor}
              buttonOffColor='#939395'
              sliderOnColor='white'
              sliderOffColor='white'
              labelStyle={{ 
                color: (this.state.toggled) ? '#292A3E' : 'white', 
                fontFamily: 'ITCAvantGardePro-Md',
                marginTop: (Platform.OS === 'ios') ? 8 : 0,
                fontSize: 15, 
                paddingLeft: 10, 
                paddingRight: 10, 
                // marginTop: (this.state.toggled) ? -3 : 5, 
                textAlign: (this.state.toggled) ? 'left':'right', width: '100%' 
              }}
              onToggle={(value) => {
                if(this.props.readOnly !== true) {
                  this.setState({ toggled: value })
                  this.props.onChange(value)
                }
              }}
            />
          </View>
        </View>
      </View>
    )
  }
}