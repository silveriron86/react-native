import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import commonStyles from '../Layouts/Categories/_styles'
import Utils from '../../utils'

export default class Input extends React.Component { 
  constructor(props) {
    super(props)
    this.state = {
      errorMsg: '',
      val: this.props.value,
      selection: {
        start: 0,
        end: 0
      }
    }
  }

  componentDidMount() {
    this.mount = true
  }

  componentWillUnmount() {
    this.mount = false
  }

  onChangeText = (val) => {
    if(!this.mount) return
    if(this.props.suffix) {
      val = val.replace(this.props.suffix, '')
    }
    // if(val !== '' && val.substring(val.length-1) !== '.' && val.substring(val.length-1) !== ',') {
    //   val = parseFloat(val).toString()
    // }

    this.setState({ val })
    this.props.onChange(val)
  }

  onSelectionChange = ({ nativeEvent: { selection } }) => {
    if(!this.mount) return
    let pos = this.state.val.toString().length
    if(selection.start > pos) {
      this._setEndPos()
    }else {
      this.setState({ selection })
    }
  }

  _setEndPos = () => {
    let pos = this.state.val.toString().length
    this.setState({
      selection: {
        start: pos,
        end: pos
      }
    })    
  }

  onFocus = () => {
    if(!this.mount) return
    this._setEndPos()
  }

  onBlur = () => {
    let value = this.state.val
    errorMsg = '';
    if(value) {
      const min = this.props.min
      const max = this.props.max
      errorMsg = Utils.checkValueRange(min, max, value)
    }
    this.setState({errorMsg});
  }

  render() {
    const inputAccessoryViewID = "uniqueID"
    const { val, errorMsg } = this.state
    const { 
      required, 
      isShowableError,
      tooltip,
      type,
      readOnly,
      placeholder,
      label,
      suffix
    } = this.props

    return (      
      <View style={commonStyles.row}>
        <Text style={[commonStyles.text, (type === 'followup') && {color: '#292A3E'}]}>{label}{(required) && '*'}</Text>
        {
          tooltip &&
          <Text style={commonStyles.tooltipText}>{tooltip}</Text>
        }
        <View style={commonStyles.inputWrapper}>
          <TextInput
            ref={"inputBox"}
            value={`${val}${suffix}`}
            keyboardType='numeric'
            autoCorrect={false}
            style={[commonStyles.input, (type === 'followup') && {color: '#292A3E', borderBottomColor: '#292A3E'}]}
            placeholder={placeholder}
            placeholderTextColor="#9B9B9B"
            underlineColorAndroid={(type === 'followup') ? '#292A3E' : '#BFBFBF'}
            autoCapitalize='none'
            returnKeyType='done'
            inputAccessoryViewID={inputAccessoryViewID}
            onChangeText={this.onChangeText}
            selection={this.state.selection} 
            onSelectionChange={this.onSelectionChange}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            editable={(readOnly === true) ? false : true}
          />          
        </View>        
        {
          ((errorMsg != '') || (isShowableError === true)) && 
          <Text style={commonStyles.inputError}>{errorMsg}</Text>
        }
      </View> 
    )
  }
}