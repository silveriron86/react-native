import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import moment from 'moment';
import DatePicker from 'react-native-datepicker-latest';
import commonStyles from '../Layouts/Categories/_styles'

export default class Date extends React.Component { 
  constructor(props) {
    super(props)
    this.state = {
      date: this.props.value
    }
  }

  render() {
    return (
      <View style={commonStyles.row}>
        <Text style={[commonStyles.text, (this.props.type === 'followup') && {color: '#292A3E'}]}>{this.props.label}{(this.props.required) && '*'}</Text>
        {
          this.props.tooltip &&
          <Text style={commonStyles.tooltipText}>{this.props.tooltip}</Text>
        }        
        <DatePicker
          style={commonStyles.date}
          date={this.state.date}
          mode="date"
          maxDate={this.props.max.substring(0, 10)}
          placeholder="0000-00-00"
          format="YYYY-MM-DD"
          confirmBtnText="Valider"
          cancelBtnText=""
          showIcon={false}
          customStyles={{              
            dateInput: {
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderTopWidth: 0,
              alignItems: 'flex-start'
            },
            dateText: {
              fontFamily: 'ITCAvantGardePro-Bk',
              textAlign: 'left',
              color: (this.props.type === 'followup') ? '#292A3E' : 'white',
              fontSize: 15
            }
          }}
          onDateChange={(date) => {
              this.setState({date: date})
              this.props.onChange(date)
            }
          }
          disabled={(this.props.readOnly === true) ? true : false}
        />
      </View>
    )
  }
}