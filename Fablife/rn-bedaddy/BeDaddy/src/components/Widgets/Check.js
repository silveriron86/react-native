import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, TouchableOpacity } from 'react-native';
import commonStyles from '../Layouts/Categories/_styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import _ from 'underscore';

var BACKGROUND_COLOR, BORDER_RADIUS, BORDER_WIDTH, COLOR, MARGIN, SIZE, BORDER_COLOR;

class Checkbox extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        backgroundColor: 'transparent',
        borderRadius: 2,
        borderWidth: 1,
        checked: false,
        color: '#000',
        margin: 2,
        name: '',
        onChange: null,
        size: 16,
        borderColor: '#000'
      };
    }

    componentDidMount() {
      this.setState(_.extend({}, this.props.style, _.omit(this.props, 'style')))
    }

    componentWillReceiveProps(nextProps) {
      this.props = nextProps;
      this.setState({ checked: nextProps.checked});
    }
    
    render() {
      BACKGROUND_COLOR = this.state.backgroundColor;
      BORDER_RADIUS = this.state.borderRadius;
      BORDER_WIDTH = this.state.borderWidth;
      COLOR = this.state.color;
      MARGIN = this.state.margin;
      SIZE = this.state.size;
      BORDER_COLOR = this.state.borderColor;
      return (
        <TouchableHighlight underlayColor='transparent' onPress={() => { this._toggleCheck() }} style={commonStyles.checkBox}>
          <View style={[commonStyles.checkWrapper]}>
            <View style={{backgroundColor: BACKGROUND_COLOR, borderColor: BORDER_COLOR,
                      borderRadius: BORDER_RADIUS, borderWidth: BORDER_WIDTH,
                      height: SIZE, margin: MARGIN, width: SIZE }}>
              <View style={{flex: 1, alignItems: 'center'}}>
                { this.state.checked &&
                <Icon name='check' size={SIZE - 4 } color={COLOR} style={{marginLeft: 1, marginTop: 1}}/> }
              </View>            
            </View>
            <Text style={[commonStyles.checkBoxLabel, (this.props.type === 'followup') && {color: '#292A3E'}]}>{this.props.label}</Text>
          </View>
        </TouchableHighlight>
      );
    }

    _toggleCheck() {
      if(this.props.disabled) {
        return
      }
      var checked = !this.state.checked;
      this.setState({ checked: checked });
      this.props.onChange && this.props.onChange(this.props.name, checked);
    }
}

export default class Check extends React.Component { 
  constructor(props) {
    super(props)
    this.state = {
      val: this.props.value
    }
  }

  onChange = (name, val) => {
    this.setState({
      val: val
    })
    this.props.onChange(val)
  }

  componentWillReceiveProps = (nextProps) => {
    // This will be used for only when search from autocomplete widget
    this.setState({
      val: nextProps.value
    })
  }

  render() {
    return (
      <View style={[commonStyles.row, {marginTop: 12}]}>        
        <Checkbox
          label={this.props.label}
          checked={this.state.val}
          style={{
            color: this.props.itemColor, 
            borderColor: this.props.itemColor, 
            marginTop: 5
          }}
          type={this.props.type}
          disabled={(this.props.readOnly === true) ? true : false}
          onChange={this.onChange}
        />
      </View>   
    )
  }
}