import React from 'react';
import { StyleSheet, Text, View, Modal, TextInput, TouchableOpacity, Platform } from 'react-native';

class DialogInput extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      inputModal: props.value || ''
    }
  }
  render(){
    let title = this.props.title || '';
    let hintInput = this.props.hintInput || '';

    let textProps = this.props.textInputProps || null;
    let modalStyleProps = this.props.modalStyle || {};
    let dialogStyleProps = this.props.dialogStyle || {};
    var cancelText = this.props.cancelText || 'Cancel';
    var submitText = this.props.submitText || 'Submit';

    return(
      <Modal
        animationType="fade"
        transparent={false}
        visible={this.props.isDialogVisible}
      	onRequestClose={() => {
      	  this.props.closeDialog();
      	}}>
        <View style={[styles.container, {...modalStyleProps}]}>
          <View style={[styles.modal_container, {...dialogStyleProps}]}>
            <View style={styles.modal_body}>
              <Text style={styles.title_modal}>{title}</Text>
              <Text style={[this.props.message ? styles.message_modal : {height:0} ]}>{this.props.message}</Text>
              <TextInput style={styles.input_container}
                autoCorrect={(textProps && textProps.autoCorrect==false)?false:true}
                autoCapitalize={(textProps && textProps.autoCapitalize)?textProps.autoCapitalize:'none'}
                clearButtonMode={(textProps && textProps.clearButtonMode)?textProps.clearButtonMode:'never'}
                clearTextOnFocus={(textProps && textProps.clearTextOnFocus==true)?textProps.clearTextOnFocus:false}
                keyboardType={(textProps && textProps.keyboardType)?textProps.keyboardType:'default'}
                underlineColorAndroid='transparent'
                placeholder={hintInput}
                onChangeText={(inputModal) => this.setState({inputModal})}
		            value={this.state.inputModal}
                />
            </View>
            <View style={styles.btn_container}>
              <TouchableOpacity style={styles.touch_modal}
                onPress={() => {
                  this.props.closeDialog();
                }}>
                <Text style={[styles.btn_modal_left, {color: '#B0B0B0'}]}>{cancelText}</Text>
              </TouchableOpacity>
	            <View style={styles.divider_btn}></View>
              <TouchableOpacity  style={styles.touch_modal}
                onPress={() => {
                  this.props.submitInput(this.state.inputModal);
                  this.setState({
                    inputModal: ''
                  })
                }}>
                <Text style={styles.btn_modal_right}>{submitText}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    flex:1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    ...Platform.select({
      android:{
        backgroundColor: 'rgba(0,0,0,0.62)'
      }
    }),
  },
  modal_container:{
    marginLeft: 30,
    marginRight: 30,
    ...Platform.select({
      ios: {
        backgroundColor:'rgba(220,220,220, 0.6)',
        borderRadius: 10,
        minWidth: 300,
      },
      android: {
        backgroundColor:'#fff',
        elevation: 24,
        minWidth: 280,
        borderRadius: 5,
      },
    }),
  },
  modal_body:{
    ...Platform.select({
      ios: {
        padding: 10,
        paddingLeft: 16,
        paddingRight: 16
      },
      android: {
        padding: 24,
      },
    }),
  },
  title_modal:{
    // fontWeight: 'bold',
    fontSize: 17,
    ...Platform.select({
      ios: {
        marginTop: 10,
        textAlign:'center',
        marginBottom: 5,
      },
      android: {
        textAlign:'left',
      },
    }),
  },
  message_modal:{
    fontSize: 16,
    ...Platform.select({
      ios: {
        textAlign:'center',
        marginBottom: 10,
      },
      android: {
        textAlign:'left',
        marginTop: 20
      },
    }),
  },
  input_container:{
    textAlign:'left',
    fontSize: 13,
    height: 25,
    paddingLeft: 6,
    paddingRight: 6,
    justifyContent: 'center',
    color: 'rgba(0,0,0,0.54)',
    ...Platform.select({
      ios: {
        backgroundColor: 'white',
 	      borderWidth: 0.5,
        borderColor: '#8E8E93',
        paddingLeft: 3,
        marginBottom: 15,
        marginTop: 10,
      },
      android: {
        marginTop: 8,
        borderBottomWidth: 2,
        borderColor: '#009688',
        height: 40
      },
    }),
  },
  btn_container:{
    flex: 1,
    flexDirection: 'row',
    ...Platform.select({
      ios: {
        justifyContent: 'center',
        borderTopWidth: 0.5,
        borderColor: '#3F3F3F',
        maxHeight: 40,
      },
      android:{
        alignSelf: 'flex-end',
        maxHeight: 40,
        paddingTop: 8,
        paddingBottom: 8,
      }
    }),
  },
  divider_btn:{
    ...Platform.select({
      ios:{
      	width: 1,
        backgroundColor: '#3F3F3F',
      },
      android:{
	      width: 0
      },
    }),
  },
  touch_modal:{
    borderWidth: 0,
    ...Platform.select({
      ios: {
        flex: 1,
      },
      android:{
        paddingRight: 8,
        minWidth: 64,
        height: 36,
      }
    }),
  },
  btn_modal_left:{
    // fontWeight: 'bold',
    ...Platform.select({
      ios: {
        fontSize:17,
        color:'#B0B0B0',
        textAlign:'center',
        // borderRightWidth: 5,
        // borderColor: '#3F3F3F',
        padding: 10,
	      height: 40,
	      maxHeight: 40,
      },
      android: {
        textAlign:'right',
        color:'#B0B0B0',
        padding: 8
      },
    }),
  },
  btn_modal_right:{
    // fontWeight: 'bold',
    ...Platform.select({
      ios: {
        fontSize:17,
        color:'#2699FF',
        textAlign:'center',
        padding: 10,
      },
      android: {
        textAlign:'right',
        color:'#009688',
        padding: 8
      },
    }),
  },
});
export default DialogInput;
