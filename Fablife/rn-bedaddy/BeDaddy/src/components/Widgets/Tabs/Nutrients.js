import React from 'react';
import { View, Image, Text } from 'react-native';
import mpStyles from '../../Layouts/Tabs/Alimentation/_styles';

export default class Nutrients extends React.Component { 
  render() {
    let data = this.props.data
    return (
      <View style={mpStyles.block}>
        <View style={mpStyles.blockTitleWrapper}>
          <Text style={mpStyles.blockTitle}>{`Valeurs nutritionnelles`.toUpperCase()}</Text>
        </View>
        {
          (data.tags_nutrients && data.tags_nutrients.length > 0) &&
          <View style={mpStyles.tagsWrapper}>
            {
              data.tags_nutrients.map((tag, i) => 
                <View key={`tag-${i}`} style={mpStyles.tag}>
                  <Text style={mpStyles.tagText}>{(typeof(tag) === 'string') ? tag.toUpperCase() : tag.name.toUpperCase()}</Text>
                </View>
              )
            }          
          </View>
        }
        
        {
          (data.nutrients && data.nutrients.length > 0) && 
          <View style={[mpStyles.rowFlexWrapper, {justifyContent: 'space-between'}]}>
            {
              data.nutrients.map((nut, i) => 
                <View key={`nut-${i}`} style={mpStyles.nut}>
                  <Text style={mpStyles.nutValue}>{nut.value} {nut.value_unit}</Text>
                  <Text style={mpStyles.nutName}>{nut.name.toUpperCase()}</Text>
                </View>
              )
            }          
          </View>        
        }
      </View>
    )
  }
}