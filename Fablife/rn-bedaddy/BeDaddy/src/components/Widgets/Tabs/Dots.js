import React from 'react';
import { StyleSheet, View } from 'react-native';

export default class Dots extends React.Component { 
  render() {
    const selectedIndex = this.props.selected
    const color = this.props.color
    const total = this.props.total
    
    return (
      <View style={styles.container}>
        {
          Array.apply(null, {length: total}).map((e, i) =>
            <View key={`dot-${i}`} style={[styles.dot, (i === selectedIndex) ? {backgroundColor: color} : null]}>
            </View>
          )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: 8,
    position: 'absolute',
    right: 0,
    marginTop: 7
  },
  dot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 8,
    backgroundColor: 'rgba(255, 255, 255, 0.3)'
  }
})