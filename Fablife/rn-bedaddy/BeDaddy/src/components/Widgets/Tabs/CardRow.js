import React from 'react';
import { StyleSheet, Text, View, Platform, Dimensions, TouchableOpacity, Image as Img } from 'react-native';
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import commonStyles from '../../Layouts/Categories/_styles';

export default class CardRow extends React.Component {
  render() {
    const color = this.props.color
    const i = this.props.index
    const data = this.props.data
    const selected = this.props.selected
    const size = this.props.size

    return (
      <TouchableOpacity onPress={()=>this.props.onPress(this.props.data, false)}>
        <View style={[
          styles.cardRow, 
          (selected === true || size === 1) ? null : (i === 0) ? styles.odd : (i > 0 && i < size-1) ? styles.mid : styles.even, 
          {backgroundColor: `${color}20`, borderColor: color}]}
        >
          <View style={styles.cardImgWrapper}>
            <View style={styles.cardImg}>
              <Image
                source={{uri: data.recipe_img_url}}
                indicator={Progress.Circle}
                indicatorProps={{
                  size: 45,
                  borderWidth: 0,
                  color: 'rgba(150, 150, 150, 1)',
                  unfilledColor: 'rgba(200, 200, 200, 0.2)'
                }}
                style={{width: 48, height: 48}}/>
            </View>
          </View>
          <View style={styles.cardRowText}>
            <Text style={commonStyles.text}>{data.recipe_name}</Text>
          </View>
          <View style={styles.cardArrowWrapper}>
            { selected ? 
                <TouchableOpacity onPress={()=>this.props.onPress(this.props.data, true)} style={{padding: 10}}>
                  <Img resizeMode='stretch' source={require('../../../../assets/img/ic/Change.png')} style={{tintColor: color}}/>
                </TouchableOpacity>
              : <Img resizeMode='stretch' source={require('../../../../assets/img/ic/ArrowRight.png')} style={{tintColor: color}}/>
            }
          </View>
        </View>   
      </TouchableOpacity>  
    )       
  }
}

const styles = StyleSheet.create({
  cardRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 80,
    borderLeftWidth: 3,
    borderRadius: 3
  },
  odd: {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  },
  even: {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    marginTop: 1
  },
  mid: {
    borderRadius: 0,
    marginTop: 1
  },
  cardImgWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 72
  },
  cardImg: {
    width: 48,
    height: 48,
    borderRadius: 24,
    overflow: 'hidden'
  },
  cardRowText: {
    borderColor: 'white', 
    width: Dimensions.get('window').width - 60 - 124, 
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingTop: (Platform.OS === 'ios') ? 7 : 0
  },
  cardArrowWrapper: {
    width: 56,
    justifyContent: 'center',
    alignItems: 'center',    
  }
});