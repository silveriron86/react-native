import React from 'react';
import { connect } from 'react-redux';
import { Share, View, Image, Text, TouchableOpacity, Alert } from 'react-native';
import { ActionSheetCustom as ActionSheet } from '../../Widgets/ActionSheet';
import mpStyles from '../../Layouts/Tabs/Alimentation/_styles';
import { MealPlanningActions } from '../../../actions';
import moment from 'moment';

const ACTIONSHEET_TEXT = {
  // color: 'rgb(52, 120, 246)',
  color: '#007AFF',
  fontFamily: 'ITCAvantGardePro-Bk',
  paddingTop: 5
};
const options = [
  'Cancel', 
  <Text style={ACTIONSHEET_TEXT}>Ne prend pas en compte mes préférences alimentaires</Text>, 
  <Text style={ACTIONSHEET_TEXT}>Est proposée trop souvent !</Text>, 
  <Text style={ACTIONSHEET_TEXT}>Je n’aime pas un ingrédient !</Text>, 
  <Text style={ACTIONSHEET_TEXT}>La recette est mal rédigée</Text>
]

class Info extends React.Component {
  handleReport = () => {
    this.ActionSheet.show()
  }

  onActionSheetItem = (index) => {
    if(index > 0) {
      setTimeout(()=> {
        let data = this.props.data;
        console.log(data);
        let recipe_id = data.id;
        this.props.reportRecipe({
          data: {
            date: moment().format('DD-MM-YYYY'),
            recipe_id: recipe_id,
            problem: options[index]
          },
          cb: (res) => {
            console.log(res);
            Alert.alert(
              'MERCI POUR VOTRE RETOUR',
              'Nous allons prendre en compte votre retour pour votre prochain programme alimentaire',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              {cancelable: false},
            );          
          }
        })
      }, 100);
    }
  }

  onShare = () => {
    let msg = 'Ma recette BeDaddy:';// + '\n' + this.props.data.name;
    let data = this.props.data;
    console.log(msg, data);
    msg += '\n' + data.name;
    if(data.ingredients && data.ingredients.length > 0) {
      msg += '\n' + `Ingrédients`.toUpperCase();
      data.ingredients.forEach((ingredient, index) => {
        msg += '\n - ' + ingredient.name + ' ' + ingredient.qty_value + ingredient.qty_unit;
      });
    }

    if(data.steps && data.steps.length > 0) {
      msg += '\n' + `Instructions`.toUpperCase();
      data.steps.forEach((step, index) => {
        msg += '\n - ' + 'étape'.toUpperCase() + ' ' + (index + 1);
        msg += '\n   ' + step.instructions;
      });
    }    
    console.log(msg);
    Share.share({
      title: 'Nouveau message',
      message: msg
    });
  };    

  render() {
    let data = this.props.data;
    let user_like = this.props.user_like

    let preparation = (data.menu_preparation_duration) ? data.menu_preparation_duration : null
    let cooking = (data.menu_preparation_cooking) ? data.menu_preparation_cooking : null

    if(data.timings && data.timings.length > 0) {
      preparation = data.timings[0].duration_source
      cooking = data.timings[1].duration_source
    }

    return (
      <View>
        {
          (user_like === null) ? <Text style={[mpStyles.bodyTitle, {width: '100%', paddingTop: 15}]}>{data.title.toUpperCase()}</Text>
          :
          <View>
            {
              data.name && 
              <Text style={mpStyles.bodyTitle}>{data.name.toUpperCase()}</Text>
            }
            <TouchableOpacity
              style={mpStyles.likeBtn}
              onPress={()=>this.props.doLike()}>
              {
                user_like === true ? 
                <Image
                  resizeMode='stretch' 
                  source={require('../../../../assets/img/ic/Like_ON.png')} />
                :
                <Image
                  resizeMode='stretch' 
                  source={require('../../../../assets/img/ic/Like.png')} />
              }
            </TouchableOpacity> 
          </View>
        }

        <View style={[mpStyles.block, {marginTop: 10, paddingBottom: 35}]}>
          <View style={mpStyles.rowFlexWrapper}>
            { 
              preparation &&
              <View style={{flexDirection: 'row'}}>
                <View style={{justifyContent: 'center'}}>
                  <Image source={require('../../../../assets/img/ic/Preparation.png')}/>
                </View>
                <View style={mpStyles.infoTextWrapper}><Text style={mpStyles.smallText}>{preparation}</Text></View>
              </View>
            }

            { 
              cooking &&
              <View style={{flexDirection: 'row'}}>
                <View style={{justifyContent: 'center'}}>
                  <Image source={require('../../../../assets/img/ic/Cooking.png')}/>
                </View>
                <View style={mpStyles.infoTextWrapper}><Text style={mpStyles.smallText}>{cooking}</Text></View>
              </View>
            }

            { 
              data.menu_calories_per_serving &&
              <View style={{flexDirection: 'row'}}>
                <View style={{justifyContent: 'center'}}>
                  <Image source={require('../../../../assets/img/ic/Calories.png')}/>
                </View>
                <View style={mpStyles.infoTextWrapper}><Text style={mpStyles.smallText}>{data.menu_calories_per_serving}</Text></View>
              </View>
            }              

            { 
              data.servings &&
              <View style={{flexDirection: 'row'}}>
                <View style={{justifyContent: 'center'}}>
                  <Image source={require('../../../../assets/img/ic/Serving.png')}/>
                </View>
                <View style={mpStyles.infoTextWrapper}><Text style={mpStyles.smallText}>{data.servings} pers</Text></View>
              </View>
            }       
          </View>
        </View>

        
        {
          (this.props.fromMenu != true) &&
          <TouchableOpacity style={mpStyles.reportBtn} onPress={this.handleReport}>
            <Image
              style={mpStyles.reportImg}
              resizeMode='cover' 
              source={require('../../../../assets/img/ic/Report.png')} />
          </TouchableOpacity>
        }
        {
          (this.props.fromMenu != true) &&
          <TouchableOpacity style={[mpStyles.reportBtn, mpStyles.sharetBtn]} onPress={this.onShare}>
            <Image
              style={mpStyles.shareImg}
              resizeMode='cover' 
              source={require('../../../../assets/img/ic/Share.png')} />
          </TouchableOpacity>           
        }

        <ActionSheet
          ref={o => this.ActionSheet = o}
          title={<Text>Quel est le problème avec cette recette ?</Text>}
          options={options}
          cancelButtonIndex={0}
          onPress={(index) => this.onActionSheetItem(index)}
        />        
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    reportRecipe: (request) => dispatch(MealPlanningActions.reportRecipe(request.data, request.cb))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Info);