import React from 'react';
import { View } from 'react-native';
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import mpStyles from '../../Layouts/Tabs/Alimentation/_styles';

export default class CircleImage extends React.Component { 
  render() {
    let uri = this.props.uri
    let customStyle = (this.props.style) ? this.props.style : null
    return (
      <View style={[mpStyles.circleImgWrapper, customStyle]}>
        <Image
          source={{uri: uri}}
          indicator={Progress.Circle}
          indicatorProps={{
            size: 40,
            borderWidth: 0,
            color: 'rgba(150, 150, 150, 1)',
            unfilledColor: 'rgba(200, 200, 200, 0.2)'
          }}
          style={{width: 40, height: 40}}/>
      </View>
    )
  }
}