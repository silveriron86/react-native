import React from 'react';
import { StyleSheet, View, Text, Image, ActivityIndicator } from 'react-native';

export default class WeekSummary extends React.Component { 
  render() {
    const WEEK = ['L', 'M', 'M', 'J', 'V', 'S', 'D']
    let cols = []
    let data = this.props.data
    if(data === null) {
      return null
    }

    var res = Object.keys(data).map(function(k) {
      return data[k]
    })
    if(res.length > 7) {
      res.splice(0, 1)
    }

    res.forEach((w, i) => {
      cols.push(
        <View key={`w-${i}`} style={styles.weekCol}>
          <Text style={[styles.weekTxt, (w === null) && {color: '#E3E3E3'}]}>{WEEK[i]}</Text>
          {
            (w === false || w === 2) ?
            <Image resizeMode='stretch' source={require('../../../../assets/img/ic/FollowUp/No.png')} style={styles.wsIcon}/>
            : (w === 1) ?
            <Image resizeMode='stretch' source={require('../../../../assets/img/ic/FollowUp/NotAll.png')} style={styles.wsIcon}/>
            : (w === true || w === 0) ?
            <Image resizeMode='stretch' source={require('../../../../assets/img/ic/FollowUp/Yes.png')} style={styles.wsIcon}/>
            : (w === null) &&
            <Image resizeMode='stretch' source={require('../../../../assets/img/ic/FollowUp/Empty.png')} style={styles.wsIcon}/>
          }
        </View>
      )
    })
        
    return (
      <View style={styles.container}>
        {
          (this.props.data) ? 
          <View style={styles.colsWrapper}>
            {cols}
          </View> 
          :
          <View style={{marginTop: 15, height: 30, justifyContent: 'center'}}>
            <ActivityIndicator size={'large'}/>
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'center',
  },
  colsWrapper: {
    justifyContent: 'space-between', 
    flexDirection: 'row',
    maxWidth: 222,
  },
  weekCol: {
    marginTop: 15,
    width: '14.2857%',
    // paddingLeft: 8,
    // paddingRight: 8
  },
  weekTxt: {
    fontFamily: 'ITCAvantGardePro-Md',
    fontSize: 12,
    color: 'black',
    textAlign: 'center',
    marginBottom: 6
  },
  wsIcon: {
    alignSelf: 'center'
  }
})