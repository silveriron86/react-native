import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import rootReducer from './reducers/rootReducer';
import thunk from 'redux-thunk';

const createStoreWithMW = applyMiddleware(logger, thunk)(createStore)
const store = createStoreWithMW(rootReducer)
 
export default store;