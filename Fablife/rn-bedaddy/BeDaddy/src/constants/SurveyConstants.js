module.exports = {
	CATEGORIES: [
		{
			title: 'Profil',
			form_id: 'profile',
			icon: require('../../assets/img/ic/SALUS/Profile.png'),
			color: '#43CDEE'
		},
		{
			title: 'Santé',
			form_id: 'health',
			icon: require('../../assets/img/ic/SALUS/Health.png'),
			color: '#34E8A5'
		},
		{
			title: 'Activité physique',
			form_id: 'physical_activity',
			icon: require('../../assets/img/ic/SALUS/Exercise.png'),
			color: '#FC706F'
		},
		{
			title: 'Sommeil et fatigue',
			form_id: 'sleep_fatigue',
			icon: require('../../assets/img/ic/SALUS/Sleep.png'),
			color: '#7558FF'
		},
		{
			title: 'Stress',
			form_id: 'stress',
			icon: require('../../assets/img/ic/SALUS/Stress.png'),
			color: '#FFE200'
		},
		{
			title: 'Moral & Bien-être Psychologique',
			form_id: 'moral_wellbeing',
			icon: require('../../assets/img/ic/SALUS/WellBeing.png'),
			color: '#85E834'
		},
		{
			title: 'Mémoire & Concentration',
			form_id: 'memory_concentration',
			icon: require('../../assets/img/ic/SALUS/Memory.png'),
			color: '#B04DFF'
		},
		{
			title: 'Stress Oxydant',
			form_id: 'oxydative_stress',
			icon: require('../../assets/img/ic/SALUS/OxydantStress.png'),
			color: '#FF9E1F'
		}
	],
	COMPLETED_SURVEY: 'COMPLETED_SURVEY',
	LOADED_SURVEY: 'LOADED_SURVEY',
	GET_FORM_SUCCESS: 'SURVEY_GET_FORM_SUCCESS',
	GET_FORM_ERROR: 'SURVEY_GET_FORM_ERROR',
	POST_FORM_SUCCESS: 'SURVEY_POST_FORM_SUCCESS',
	POST_FORM_ERROR: 'SURVEY_POST_FORM_ERROR',	
	UPDATE_FORM_DATA: 'UPDATE_FORM_DATA',
	GET_SEARCH_DATA_SUCCESS: 'GET_SEARCH_DATA_SUCCESS',
	GET_SEARCH_DATA_ERROR: 'GET_SEARCH_DATA_ERROR',
	GET_SURVEY_SUCCESS: 'GET_SURVEY_SUCCESS',
	GET_SURVEY_ERROR: 'GET_SURVEY_ERROR'
};