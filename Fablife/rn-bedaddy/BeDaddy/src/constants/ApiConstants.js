import { Platform } from 'react-native';

const CLIENT_IDS = {
    ios: 'i_cid_9dd308a8e567d7fdb56b3a35b8838c71',
    android: 'a_cid_dd20251b76366b53b66dc664d6879335'
}

const CLIENT_SECRETS = {
    ios: '11fe2f9748c3b67cbac5385115c67bad',
    android: '9bb281c50e9c25e01717d6d118c8e1db'
}

let _inv = {
    DEV: {
        url: 'https://dev.apis.bedaddy.fr/',
        client_id: 'wordpress_cid',
        client_secret: 'secret_wordpress_secret'
    },
    PRODUCTION: {
        url: 'https://apis.bedaddy.fr/',
        client_id: CLIENT_IDS[Platform.OS],
        client_secret: CLIENT_SECRETS[Platform.OS]
    },
    ENVIRONMENT: 'PRODUCTION',
};


module.exports = {
    getApiPath: () => {
        return _inv[_inv.ENVIRONMENT].url;        
    },

    getApiClient: () => {
        let data = _inv[_inv.ENVIRONMENT];
        return {
            client_id: data.client_id,
            client_secret: data.client_secret
        }
    }
};