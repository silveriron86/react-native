import { AsyncStorage } from 'react-native';
import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import DeviceInfo from 'react-native-device-info';
import ApiConstants from '../constants/ApiConstants';

const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
let tracker = new GoogleAnalyticsTracker("UA-129680512-1")
    
let Utils = {
   btoa: (input) => {
		let str = input;
		let output = '';
	
		for (let block = 0, charCode, i = 0, map = chars;
		str.charAt(i | 0) || (map = '=', i % 1);
		output += map.charAt(63 & block >> 8 - i % 1 * 8)) {
	
			charCode = str.charCodeAt(i += 3/4);
		
			if (charCode > 0xFF) {
				throw new Error("'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.");
			}
			
			block = block << 8 | charCode;
		}
		
		return output;
  },

  checkRoute(_this) {
    _this.props.getSurvey({
      type: 'signup',
      cb: (res) => {
        if(res.status === 401) {
          //Missing authorization header (accessToken)
          _this._goPage("Login")
          return
        }

        AsyncStorage.getItem('fcmToken', (err, token) => {
          console.log(' utils token');
          AsyncStorage.getItem('savedFirebaseToken', (err, saved) => {
            if(!saved) {
              Utils.saveFirebaseToken(token);
            }
          });
        });

        if(res.completion === 100) {
          _this.props.completedSurvey(8)
          AsyncStorage.setItem('completedSignupSurveys', '8')

          _this.props.getSurvey({
            type: 'preference',
            cb: (res) => {   
              if(res.completion === 100) {
                AsyncStorage.setItem('completedPreference', '1')
                _this._goPage("TabMain")
              }else {
                _this._goPage("Preference")
              }
            }
          })         
        }else {
          let completedSignup = 0
          for(let i=0; i<res.forms.length; i++) {
            if(res.forms[i].completed === true) {
              completedSignup++;
            }else {
              break;
            }
          }
          _this.props.completedSurvey(completedSignup)
          // Utils.getLocalStorage('agreedPrivacy').then((agreed)=> {
          //   if(agreed === null && res.completion === 0) {
          //     _this._goPage("PrivacyPolicy")
          //   }else {
              AsyncStorage.setItem('agreedPrivacy', 'true')
              _this._goPage("CategoryList")
            // }
          // })            
        }
      }
    })
  },

  capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
  },
  
  checkValueRange(min, max, value) {
    let errorMsg = ''

    if((value < min && value > max) || value === '') {
      errorMsg = `La valeur doit être comprise entre ${min} et ${max}`
    }else if(value < min) {
      errorMsg = `La valeur doit être supérieure à ${min}`
    }else if(value > max) {
      errorMsg = `La valeur doit être inférieure à ${max}`
    }

    return errorMsg
  },

	_getQuestionValue: (questions, questionId) => {
    for(let i=0; i<questions.length; i++) {
      let item = questions[i]
      if(item.question_id === questionId) {
        return item.value
      }
    }
    return null;
  },

  checkVisibleQuestion: (data, questions, question) => {    
    if(question.conditions.length === 0) {
      return true
    }

    let showable = true
    for(let i=0; i<question.conditions.length; i++) {
      let condition = question.conditions[i]      
      let value = Utils._getQuestionValue(questions, condition.parentQuestionId)
      if(value === null) {
        console.log(condition.parentQuestionId, data[condition.parentQuestionId]);
        if(data && typeof data[condition.parentQuestionId] !== "undefined") {
          value = data[condition.parentQuestionId];
        }
      }

      if(condition.operator ===	"$eq") {
        showable = (value === condition.value)
      }else if(condition.operator ===	"$neq" || condition.operator ===	"$ne") {
        showable = (value !== condition.value)
      }else if(condition.operator ===	"$gt") {
        showable = (value > condition.value)
      }else if(condition.operator ===	"$gte") {
        showable = (value >= condition.value)
      }else if(condition.operator ===	"$lt") {
        showable = (value < condition.value)
      }else if(condition.operator ===	"$lte") {
        showable = (value <= condition.value)
      }

      if(question.conditions_assoc === "OR") {
        if(showable === true) {
          return true
        }
      }else {
        if(showable === false) {
          return false
        }
      }
    }

    return showable
  },

  trackScreenView: (screen) => {
    tracker.trackScreenView(screen)
  },

  trackEvent: (action, name) => {
    tracker.trackEvent(action, name);
  },

  async getLocalStorage(key) {
    const v = await AsyncStorage.getItem(key);
    return v
  },
  saveFirebaseToken(fcmToken) {
    AsyncStorage.getItem('accessToken', (err, token) => {
      AsyncStorage.getItem('userId', (err, user_id) => {
        fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/firebase`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
          body: JSON.stringify({
            token: fcmToken
          })
        })
        .then(res => {
          console.log('Saved fcm token successfully');
          console.log(fcmToken);
          console.log(res);
          if(res.ok === true) {
            AsyncStorage.setItem('savedFirebaseToken', 'SAVED');
          }
        })
        .catch(error => {
          console.log('error')          
        })
      })
    });
  },
  nativeBaseDoesHeaderCorrectlyAlready() {
    // if (!DeviceInfo.hasNotch()) {
    //   return true
    // }
    const model = DeviceInfo.getModel()

    switch (model) {
      case "iPhone X":
      case "iPhone XS":
      case "iPhone XS Max":
        return true
    }
    return false
  }
};

export default Utils;
