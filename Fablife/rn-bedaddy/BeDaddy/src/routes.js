import { createStackNavigator } from 'react-navigation';
import Home from './components/Layouts/Home';
import Slideshow from './components/Layouts/Login/Slideshow';
import Login from './components/Layouts/Login/Login';
import Forgot from './components/Layouts/Login/Forgot';
import PrivacyPolicy from './components/Layouts/Login/PrivacyPolicy';
import CategoryList from './components/Layouts/Categories/CategoryList';
import Form from './components/Layouts/Categories/Form';
import Preference from './components/Layouts/Categories/Preference';
import Main from './components/Layouts/Tabs/Main';
  
export default AppNavigator = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions :{ header : null }
  },  
  Slideshow: {
    screen: Slideshow,
    navigationOptions :{ header : null }
  },
  Login: {
    screen: Login,
    navigationOptions :{ header : null }
  },
  Forgot: {
    screen: Forgot,
    navigationOptions :{ header : null }
  },
  PrivacyPolicy: {
    screen: PrivacyPolicy,
    navigationOptions :{ header : null }
  },
  CategoryList: {
    screen: CategoryList,
    navigationOptions :{ header : null }
  },
  Form: {
    screen: Form,
    navigationOptions :{ header : null }
  },
  Preference: {
    screen: Preference,
    navigationOptions :{ header : null } 
  },
  TabMain: {
    screen: Main,
    navigationOptions :{ header : null } 
  }
},
{
  headerMode: 'screen',
  initialRouteName: 'Home'
});

