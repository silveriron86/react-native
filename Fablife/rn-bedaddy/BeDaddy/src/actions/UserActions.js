import { AsyncStorage } from 'react-native';
import UserConstants from '../constants/UserConstants';
import ApiConstants from '../constants/ApiConstants';

let UserActions = {
	getDashboardError: function(error) {
		return {
			error,
			type: UserConstants.GET_DASHBOARD_ERROR
		};
	},

	getDashboardSuccess: function(response) {
		return {
			response,
			type: UserConstants.GET_DASHBOARD_SUCCESS
		};
	},

	getDashboard: function(cb){
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/dashboard`, {
						method: 'GET',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`,
							'Accept-Language': 'fr-FR'
						}
					})
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.getDashboardSuccess(response));
								cb(response);	
							})	
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getDashboardError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.getDashboardError(error));
						cb(error);
					})
				})
			})
		};		
	},

	getUserDetailsError: function(error) {
		return {
			error,
			type: UserConstants.GET_USER_DETAILS_ERROR
		};
	},

	getUserDetailsSuccess: function(response) {
		return {
			response,
			type: UserConstants.GET_USER_DETAILS_SUCCESS
		};
	},

	getUserDetails: function(cb){
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}`, {
						method: 'GET',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`,
							'Accept-Language': 'fr-FR'
						}
					})
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.getUserDetailsSuccess(response));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getUserDetailsError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.getUserDetailsError(error));
						cb(error);
					})
				})
			})
		};		
	}	
};

export default UserActions;