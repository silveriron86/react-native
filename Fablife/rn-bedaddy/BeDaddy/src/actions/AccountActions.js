import AccountConstants from '../constants/AccountConstants';
import ApiConstants from '../constants/ApiConstants';
import Utils from '../utils';

let AccountActions = {
	loginError: function(error) {
		return {
			error,
			type: AccountConstants.LOGIN_ERROR
		};
	},

	loginSuccess: function(response) {
		return {
			response,
			type: AccountConstants.LOGIN_SUCCESS
		};
	},

	login: function(data, cb){
        let _apiBody="grant_type=password&username="+data.username+"&password="+data.password;
        return dispatch => {
            fetch(ApiConstants.getApiPath() + 'oauth2/token', {
				method: 'POST',
				headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
					'Authorization': 'Basic ' + Utils.btoa(ApiConstants.getApiClient().client_id + ':' + ApiConstants.getApiClient().client_secret)
				},
				body: _apiBody
			})
			.then(res => {
				if(res.ok) {
					res.json().then(response => {
						dispatch(this.loginSuccess(response));
						cb(response);	
					})
				}else {
					let _error={
						status: res.status,
						error: res.statusText
					};
					dispatch(this.loginError(_error));
					cb(_error);
				}
			})
			.catch(error => {
				console.log('error')
				dispatch(this.loginError(error));
				cb(error);
			})
		};
	},

	forgotError: function(error) {
		return {
			error,
			type: AccountConstants.FORGOT_ERROR
		};
	},

	forgotSuccess: function(response) {
		return {
			response,
			type: AccountConstants.FORGOT_SUCCESS
		};
	},

	forgot: function(email, cb){
        return dispatch => {
            fetch(ApiConstants.getApiPath() + 'api/v1/users/password/forgot', {
				method: 'POST',
				headers: {
                   	'Content-Type': 'application/json',	
					'Authorization': 'Basic ' + Utils.btoa(ApiConstants.getApiClient().client_id + ':' + ApiConstants.getApiClient().client_secret)
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => {
				if(res.ok) {
					res.json().then(response => {
						dispatch(this.forgotSuccess(response));
						cb(response);	
					})
				}else {
					let _error={
						status: res.status,
						error: res.statusText
					};
					dispatch(this.forgotError(_error));
					cb(_error);
				}
			})
			.catch(error => {
				console.log('error')
				dispatch(this.forgotError(error));
				cb(error);
			})
		};
	},
};

export default AccountActions;
