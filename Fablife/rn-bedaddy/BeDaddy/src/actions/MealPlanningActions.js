import { AsyncStorage } from 'react-native';
import MealPlanningConstants from '../constants/MealPlanningConstants';
import ApiConstants from '../constants/ApiConstants';
import Utils from '../utils';

let MealPlanningActions = {
  updateData: function(data, cb) {
		return dispatch => {
			dispatch({
				type: MealPlanningConstants.UPDATE_MEALPLANNING_DATA_SUCCESS,
				response: data
      });	
      cb()
		};	
  },

  updateWeekComplianceData: function(type, data) {
		return dispatch => {
      dispatch(this.getWeekComplianceSuccess({
        type: (data.type == 'meal-planning') ? 'mealPlanning' : 'foodSupplements',
        data: data
      }));  
    };	  
  },  
  
  getDataError: function(error) {
    return {
      error,
      type: MealPlanningConstants.GET_MEALPLANNING_DATA_ERROR
    };
  },

  getDataSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.GET_MEALPLANNING_DATA_SUCCESS
    };
  },

  getData: function(date, type, cb){
    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          let detail = type ? `/${type}` : '/summary'
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/meal-planning/${date}${detail}`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`,
              'Accept-Language': 'fr-FR'
            }
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.getDataSuccess({
                  type: type,
                  data: response
                }));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getDataError(_error));
							cb(_error);
						}
					})
					.catch(error => {
            console.log('error')
            console.log(error.response)
						dispatch(this.getDataError(error));
						cb(error);
          })
        })
      })
    };    
  },

  getRecipeError: function(error) {
    return {
      error,
      type: MealPlanningConstants.GET_RECIPE_ERROR
    };
  },

  getRecipeSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.GET_RECIPE_SUCCESS
    };
  },

  getRecipe: function(recipe_id, cb){
    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/recipes/${recipe_id}`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`,
              'Accept-Language': 'fr-FR'
            }
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.getRecipeSuccess(response));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getRecipeError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.getRecipeError(error));
						cb(error);
          })
        })
      })
    };    
  },
  
  postLikesError: function(error) {
    return {
      error,
      type: MealPlanningConstants.POST_LIKES_ERROR
    };
  },

  postLikesSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.POST_LIKES_SUCCESS
    };
  },

  postLikes: function(recipe_id, cb){
    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/likes`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
              resource_type: 'recipe',
              resource_id: recipe_id
            })
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.postLikesSuccess(response));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.postLikesError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.postLikesError(error));
						cb(error);
          })
        })
      })
    };    
  },

  delLikesError: function(error) {
    return {
      error,
      type: MealPlanningConstants.DEL_LIKES_ERROR
    };
  },

  delLikesSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.DEL_LIKES_SUCCESS
    };
  },

  delLikes: function(recipe_id, cb){
    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          let resource_type = 'recipe'
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/likes/${resource_type}/${recipe_id}`, {
            method: 'DELETE',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            }
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.delLikesSuccess(response));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.delLikesError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.delLikesError(error));
						cb(error);
          })
        })
      })
    };    
  },

  putDataError: function(error) {
    return {
      error,
      type: MealPlanningConstants.PUT_DATA_ERROR
    };
  },

  putDataSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.PUT_DATA_SUCCESS
    };
  },

  putData: function(date, type, recipe_id, cb){
    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/meal-planning/${date}/${type}`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
              choice: recipe_id
            })
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.putDataSuccess(response));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.putDataError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.putDataError(error));
						cb(error);
          })
        })
      })
    };    
  },  

  getMenuError: function(error) {
    return {
      error,
      type: MealPlanningConstants.GET_MENU_ERROR
    };
  },

  getMenuSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.GET_MENU_SUCCESS
    };
  },

  getMenu: function(date, type, cb){
    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/meal-planning/${date}/${type}/menu2`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`,
              'Accept-Language': 'fr-FR'
            }
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.getMenuSuccess({
                  type: type,
                  data: response
                }));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getMenuError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.getMenuError(error));
						cb(error);
          })
        })
      })
    };    
  },
  
  getGuidelinesError: function(error) {
    return {
      error,
      type: MealPlanningConstants.GET_GUIDELINES_ERROR
    };
  },

  getGuidelinesSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.GET_GUIDELINES_SUCCESS
    };
  },

  getGuidelines: function(date, type, cb){
    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/meal-planning/${date}/${type}/guidelines`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`,
              'Accept-Language': 'fr-FR'
            }
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.getGuidelinesSuccess({
                  type: type,
                  data: response
                }));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getGuidelinesError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.getGuidelinesError(error));
						cb(error);
          })
        })
      })
    };    
  },  

  getShoppingListError: function(error) {
    return {
      error,
      type: MealPlanningConstants.GET_SHOPPING_LIST_ERROR
    };
  },

  getShoppingListSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.GET_SHOPPING_LIST_SUCCESS
    };
  },

  getShoppingList: function(dates, cb){
    let params = dates.map(val => `dates[]=${encodeURIComponent(val)}`).join('&')

    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/meal-planning/shopping-list?${params}`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`,
              'Accept-Language': 'fr-FR'
            }          
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.getShoppingListSuccess(response));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getShoppingListError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.getShoppingListError(error));
						cb(error);
          })          
        })
      })
    };    
  },  

  postShoppingListError: function(error) {
    return {
      error,
      type: MealPlanningConstants.POST_SHOPPING_LIST_ERROR
    };
  },

  postShoppingListSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.POST_SHOPPING_LIST_SUCCESS
    };
  },

  postShoppingList: function(dates, id, selected, cb){
    let params = dates.map(val => `dates[]=${encodeURIComponent(val)}`).join('&')

    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/meal-planning/shopping-list?${params}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`,
              'Accept-Language': 'fr-FR'
            },
            body: JSON.stringify({
              id_ingredient: id,
              shopped: selected
            })            
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.postShoppingListSuccess(response));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.postShoppingListError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.postShoppingListError(error));
						cb(error);
          })
        })
      })
    };    
  },    

  getBaseGroceriesError: function(error) {
    return {
      error,
      type: MealPlanningConstants.GET_BASE_GROCERIES_ERROR
    };
  },

  getBaseGroceriesSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.GET_BASE_GROCERIES_SUCCESS
    };
  },

  getBaseGroceries: function(cb){
    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        fetch(`${ApiConstants.getApiPath()}api/v1/base-groceries`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'Accept-Language': 'fr-FR'
          }          
        })
        .then(res => {
          if(res.ok) {
            res.json().then(response => {
              dispatch(this.getBaseGroceriesSuccess(response));
              cb(response);	
            })							
          }else {
            let _error={
              status: res.status,
              error: res.statusText
            };
            dispatch(this.getBaseGroceriesError(_error));
            cb(_error);
          }
        })
        .catch(error => {
          console.log('error')
          dispatch(this.getBaseGroceriesError(error));
          cb(error);
        })
      })
    };    
  },    

	postComplianceError: function(error) {
		return {
			error,
			type: MealPlanningConstants.POST_COMPLIANCE_ERROR
		};
	},

	postComplianceSuccess: function(response) {
		return {
			response,
			type: MealPlanningConstants.POST_COMPLIANCE_SUCCESS
		};
	},

	postCompliance: function(date, type, value, cb){
    // type: 'meal-planning' or 'food-supplements'
    let data = (type === 'food-supplements') ? {foodSupplementsComplianceValue: value} : {mealPlanningComplianceValue: value}
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/meal-planning/${date}/compliance/${type}`, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`,
							'Accept-Language': 'fr-FR'
            },
            body: JSON.stringify(data)
          })
          .then(res => {
            if(res.ok) {
              res.json().then(response => {
                dispatch(this.postComplianceSuccess(response));
                cb(response);	
              })							
            }else {
              let _error={
                status: res.status,
                error: res.statusText
              };
              dispatch(this.postComplianceError(_error));
              cb(_error);
            }
          })
          .catch(error => {
            console.log('error')
            dispatch(this.postComplianceError(error));
            cb(error);
          })
				})
			})
		};		
	},

	getWeekComplianceError: function(error) {
		return {
			error,
			type: MealPlanningConstants.GET_WEEK_COMPLIANCE_ERROR
		};
	},

	getWeekComplianceSuccess: function(response) {
		return {
			response,
			type: MealPlanningConstants.GET_WEEK_COMPLIANCE_SUCCESS
		};
	},

	getWeekCompliance: function(type, cb){
		// type: 'meal-planning' or 'food-supplements'
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/meal-planning/weekly-compliance/${type}`, {
						method: 'GET',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`,
							'Accept-Language': 'fr-FR'
						}					
          })
          .then(res => {
            if(res.ok) {
              res.json().then(response => {
                dispatch(this.getWeekComplianceSuccess({
                  type: (type == 'meal-planning') ? 'mealPlanning' : 'foodSupplements',
                  data: response
                }));
                cb(response);	
              })							
            }else {
              let _error={
                status: res.status,
                error: res.statusText
              };
              dispatch(this.getWeekComplianceError(_error));
              cb(_error);
            }
          })
          .catch(error => {
            console.log('error')
            dispatch(this.getWeekComplianceError(error));
            cb(error);
          })
				})
			})
		};		
  },
  
  reportRecipeError: function(error) {
    return {
      error,
      type: MealPlanningConstants.REPORT_RECIPE_ERROR
    };
  },

  reportRecipeSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.REPORT_RECIPE_SUCCESS
    };
  },

  reportRecipe: function(data, cb){
    return dispatch => {
      AsyncStorage.getItem('accessToken', (err, token) => {
        AsyncStorage.getItem('userId', (err, user_id) => {
          fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/recipe-reporting`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(data)
          })
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.reportRecipeSuccess(response));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.reportRecipeError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.reportRecipeError(error));
						cb(error);
          })
        })
      })
    };    
  },
};

export default MealPlanningActions;