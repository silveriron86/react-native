import { AsyncStorage } from 'react-native';
import SurveyConstants from '../constants/SurveyConstants';
import ApiConstants from '../constants/ApiConstants';
import Utils from '../utils';

let SurveyActions = {
	completedSurvey: function(index){
        return {
			type: SurveyConstants.COMPLETED_SURVEY,
			response: index
		}
	},

	setLoadedSurveys: function(val){
        return {
			type: SurveyConstants.LOADED_SURVEY,
			response: val
		}
	},

	updateFormData: function(data, cb) {
		return dispatch => {
			dispatch({
				type: SurveyConstants.UPDATE_FORM_DATA,
				response: data
			});
			cb(data);		
		};		
	},

	getFormError: function(error) {
		return {
			error,
			type: SurveyConstants.GET_FORM_ERROR
		};
	},

	getFormSuccess: function(response) {
		return {
			response,
			type: SurveyConstants.GET_FORM_SUCCESS
		};
	},

	getForm: function(type, form_id, cb){
		//type: 'signup' or 'preference', 'genetic-user'
		console.log('--- getForm', type, form_id);
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				if(err) {
					// token is expired
				}
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/survey/${type}/${form_id}`, {
						method: 'GET',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`,
							'Accept-Language': 'fr-FR'
						}
					})
					.then(res => {
						console.log(res)
						if(res.ok) {
							res.json().then(response => {
								console.log(response);
								dispatch(this.getFormSuccess({
									type: type,
									id: form_id,
									data: response
								}));
								cb(response);	
							})	
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getFormError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.getFormError(error));
						cb(error);
					})
				})
			})
		};		
	},

	postFormError: function(error) {
		return {
			error,
			type: SurveyConstants.POST_FORM_ERROR
		};
	},

	postFormSuccess: function(response) {
		return {
			response,
			type: SurveyConstants.POST_FORM_SUCCESS
		};
	},

	postForm: function(data, cb){
		console.log('--- postForm', data.type, data.id);
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/survey/${data.type}/${data.id}`, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify(data.form)
					})
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.postFormSuccess(response));
								cb(response);	
							})
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.postFormError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.postFormError(error));
						cb(error);
					})
				})
			})
		};	
	},

	getSearchDataError: function(error) {
		return {
			error,
			type: SurveyConstants.GET_SEARCH_DATA_ERROR
		};
	},

	getSearchDataSuccess: function(response) {
		return {
			response,
			type: SurveyConstants.GET_SEARCH_DATA_SUCCESS
		};
	},

	getSearchData: function(url, search, cb){
		url = url.substring(1).replace(':search', search)
		return dispatch => {
			fetch(`${ApiConstants.getApiPath()}${url}`, {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Basic ' + Utils.btoa(ApiConstants.getApiClient().client_id + ':' + ApiConstants.getApiClient().client_secret),
					'Accept-Language': 'fr-FR'
				}
			})
			.then(res => {
				if(res.ok) {
					res.json().then(response => {
						dispatch(this.getSearchDataSuccess(response));
						cb(response);	
					})	
				}else {
					let _error={
						status: res.status,
						error: res.statusText
					};
					dispatch(this.getSearchDataError(_error));
					cb(_error);
				}
			})
			.catch(error => {
				console.log('error')
				dispatch(this.getSearchDataError(error));
				cb(error);
			})
		}	
	},	


	getSurveyError: function(error) {
		return {
			error,
			type: SurveyConstants.GET_SURVEY_ERROR
		};
	},

	getSurveySuccess: function(response) {
		return {
			response,
			type: SurveyConstants.GET_SURVEY_SUCCESS
		};
	},

	getSurvey: function(type, cb){
		//type: 'signup' or 'preference', 'notice', 'genetic-user'
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/survey/${type}`, {
						method: 'GET',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`,
							'Accept-Language': 'fr-FR'
						}
					})
					.then(res => {
						// if(res.status === 401) {
						// 	dispatch(this.getSurveyError(res));
						// }						
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.getSurveySuccess({
									type: type,
									data: response
								}));
								cb(response);	
							})	
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getSurveyError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.getSurveyError(error));
						cb(error);
					})
				})
			})
		};		
	},	

	postNoticeSurvey: function(data, cb){
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/survey/notice`, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify(data)
					})
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						cb(error);
					})
				})
			})
		};	
	}
};

export default SurveyActions;