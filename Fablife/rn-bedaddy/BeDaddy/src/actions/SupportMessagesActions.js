import { AsyncStorage } from 'react-native';
import SupportMessagesConstants from '../constants/SupportMessagesConstants';
import ApiConstants from '../constants/ApiConstants';
import Utils from '../utils';

let SupportMessagesActions = {
	getSupportMessagesError: function(error) {
		return {
			error,
			type: SupportMessagesConstants.GET_SUPPORT_MESSAGES_ERROR
		};
	},

	getSupportMessagesSuccess: function(response) {
		return {
			response,
			type: SupportMessagesConstants.GET_SUPPORT_MESSAGES_SUCCESS
		};
	},

	getSupportMessages: function(cb){
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/support-messages`, {
						method: 'GET',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`,
							'Accept-Language': 'fr-FR'
						}
					})
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.getSupportMessagesSuccess(response));
								cb(response);	
							})							
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.getSupportMessagesError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.getSupportMessagesError(error));
						cb(error);
					})
				})
			})
		};		
	},

	postSupportMessageError: function(error) {
		return {
			error,
			type: SupportMessagesConstants.POST_SUPPORT_MESSAGE_ERROR
		};
	},

	postSupportMessageSuccess: function(response) {
		return {
			response,
			type: SupportMessagesConstants.POST_SUPPORT_MESSAGE_SUCCESS
		};
	},

	postSupportMessage: function(text, cb){
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/support-messages`, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`,
							'Accept-Language': 'fr-FR'
						},
						body: JSON.stringify({
							text: text
						})						
					})
					.then(res => {
						if(res.ok) {	
							res.json().then(response => {
								dispatch(this.postSupportMessageSuccess(response));
								cb(response);	
							})	
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.postSupportMessageError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.postSupportMessageError(error));
						cb(error);
					})
				})
			})
		};		
	},	

	markSupportMessageError: function(error) {
		return {
			error,
			type: SupportMessagesConstants.MARK_SUPPORT_MESSAGE_ERROR
		};
	},

	markSupportMessageSuccess: function(response) {
		return {
			response,
			type: SupportMessagesConstants.MARK_SUPPORT_MESSAGE_SUCCESS
		};
	},

	markSupportMessage: function(support_message_id, type, cb){
		// type: read or processed
		return dispatch => {
			AsyncStorage.getItem('accessToken', (err, token) => {
				AsyncStorage.getItem('userId', (err, user_id) => {
					fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/support-messages/${support_message_id}/${type}`, {
						method: 'PUT',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`
						}
					})
					.then(res => {
						if(res.ok) {
							res.json().then(response => {
								dispatch(this.markSupportMessageSuccess(response));
								cb(response);	
							})	
						}else {
							let _error={
								status: res.status,
								error: res.statusText
							};
							dispatch(this.markSupportMessageError(_error));
							cb(_error);
						}
					})
					.catch(error => {
						console.log('error')
						dispatch(this.markSupportMessageError(error));
						cb(error);
					})
				})
			})
		};	
	}
};

export default SupportMessagesActions;