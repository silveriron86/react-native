module.exports = {
  preset: 'react-native',
  verbose: true,
  collectCoverage: true,
  moduleDirectories: ['node_modules', 'src'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
  },
  moduleNameMapper: {
    '@react-native-firebase/analytics':
      '<rootDir>/__mocks__/firebase/analytics.js',
    '^.+.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
  },
  setupFiles: [
    '<rootDir>/jest.setup.js',
    './node_modules/react-native-gesture-handler/jestSetup.js',
  ],
  coveragePathIgnorePatterns: ['/node_modules/', '/jest'],
  transformIgnorePatterns: [
    'node_modules/(?!(react-native|@react-native|@sentry/react-native|react-native-image-progress|react-native-progress|react-native-iphone-x-helper|react-native-static-safe-area-insets|react-native-keyboard-avoiding-scroll-view|react-native-modal-wrapper|react-native-keyboard-spacer|react-native-webview|react-native-app-intro-slider|react-native-linear-gradient|react-native-flip-toggle-button|react-native-datepicker|react-native-simple-picker|react-native-device-info|@react-navigation/stack|react-native-swipe-gestures|react-native-calendars|react-native-actionsheet|react-native-unordered-list|react-native-reanimated|react-native-svg-charts|@react-native-firebase/messaging|@react-native-firebase/app|react-native-gesture-handler|react-native-fetch-api)/)',
  ],
};
