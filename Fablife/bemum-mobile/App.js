import React, {Component} from 'react';
import {Alert, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import AppNavigator from './src/navigation/routes';
import store from './src/store';
import AsyncStorage from '@react-native-community/async-storage';
import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import messaging from '@react-native-firebase/messaging';

export default class App extends Component {
  async componentDidMount() {
    this.requestPermission();

    // When the application is running, but in the background
    messaging().onNotificationOpenedApp(remoteMessage => {
      this.showAlert(remoteMessage);
    });

    // When the application is opened from a quit state
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          this.showAlert(remoteMessage);
        }
      });

    messaging().onMessage(async remoteMessage => {
      this.showAlert(remoteMessage);
    });
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      this.showAlert(remoteMessage);
    });

    // this._setCrisp();
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log('*** Saved fcmToken = ', fcmToken);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      console.log('*** New fcmToken = ', fcmToken);
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }

    messaging().onTokenRefresh(token => {
      if (token) {
        AsyncStorage.setItem('fcmToken', token);
      }
    });
  }

  async requestPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
      this.getToken();
    }
  }

  onMessageReceived = message => {
    console.log('Received: ', message);
  };

  UNSAFE_componentWillMount() {
    this.eventEmitter = new EventEmitter();
    global.eventEmitter = this.eventEmitter;
  }

  showAlert(remoteMessage) {
    const {title, body} = remoteMessage.notification;
    Alert.alert(
      title,
      body,
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  }

  render() {
    return (
      <Provider store={store}>
        <StatusBar barStyle={'dark-content'} />
        <AppNavigator screenProps={this.eventEmitter} />
      </Provider>
    );
  }
}
