/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, Platform, TouchableWithoutFeedback} from 'react-native';
import styles from '../_styles';
import CheckBox from '@react-native-community/checkbox';
import {Colors} from '../../../constants';

export default class Check extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      val: props.value,
    };
  }

  onToggle = () => {
    this.setState({
      val: !this.state.val,
    });
  };

  onChange = val => {
    this.setState({val});
    this.props.onChange(val);
  };

  render() {
    const {readOnly, label, style} = this.props;
    const disabled = readOnly === true ? true : false;
    return (
      <View style={[styles.row, styles.checkRow, style]}>
        <CheckBox
          testID={'check'}
          style={styles.checkBox}
          boxType="square"
          onCheckColor="white"
          tintColor={Colors.theme.DARK}
          onFillColor={Colors.theme.DARK}
          onTintColor={Colors.theme.DARK}
          value={this.state.val}
          disabled={disabled}
          onValueChange={this.onChange}
        />
        {disabled && (
          <View
            style={{
              position: 'absolute',
              width: '100%',
              height: 30,
              backgroundColor: 'transparent',
            }}
          />
        )}
        <View style={{flex: 1}}>
          <TouchableWithoutFeedback disabled={disabled} onPress={this.onToggle}>
            <Text
              style={{
                marginTop: Platform.OS === 'ios' ? 2 : 0,
                marginLeft: 1,
              }}>
              {label}
            </Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}
