/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View} from 'react-native';
import FlipToggle from './FilpToggle';
import styles from '../_styles';
import {Colors} from '../../../constants';
import commonStyles from '../../Layouts/Styles';

export default class Switch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggled: props.value ? true : false,
    };
  }

  render() {
    const {
      from,
      label,
      required,
      tooltip,
      // itemColor,
      data,
      readOnly,
      onChange,
      size,
    } = this.props;
    const {toggled} = this.state;

    const color = Colors.theme.MAIN;

    return (
      <View
        style={[
          styles.row,
          from === 'settings' && {
            paddingLeft: 21,
            paddingRight: 16,
          },
        ]}>
        <View style={[styles.switchRowWrap]}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={[commonStyles.H4, styles.switchLabel]}>
              {label}
              {required && '*'}
            </Text>
          </View>
          <View
            testID="switch-wrapper"
            style={[
              styles.switchWrap,
              {
                backgroundColor: toggled ? color : Colors.LIGHT_GREY,
              },
            ]}>
            <FlipToggle
              value={toggled}
              buttonWidth={size === 'small' ? 55 : 68}
              buttonHeight={32}
              buttonRadius={25}
              sliderHeight={25}
              sliderWidth={25}
              sliderRadius={69}
              onLabel={data[0].label}
              offLabel={data[1].label}
              buttonOnColor={color}
              buttonOffColor={Colors.LIGHT_GREY}
              sliderOnColor={Colors.theme.DARK}
              sliderOffColor="white"
              labelStyle={{
                color: Colors.DARK_GREY,
                fontSize: 15,
                paddingLeft: 10,
                paddingRight: 10,
                textAlign: toggled ? 'left' : 'right',
                width: '100%',
              }}
              onToggle={value => {
                if (readOnly !== true) {
                  this.setState({toggled: value});
                  onChange(value);
                }
              }}
            />
          </View>
        </View>
        {typeof tooltip !== 'undefined' && (
          <Text style={styles.tooltipText}>{tooltip}</Text>
        )}
      </View>
    );
  }
}
