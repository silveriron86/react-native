/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View} from 'react-native';
import RadioForm from './RadioButton/SimpleRadioButton';
import styles from '../../_styles';
import commonStyles from '../../../Layouts/Styles';

export default class Radio extends React.Component {
  constructor(props) {
    super(props);
    let index = '';
    props.data.forEach((row, idx) => {
      if (row.value === props.value) {
        index = idx;
      }
    });

    this.state = {
      value: props.value,
      index,
    };
  }

  onPress = (value, index) => {
    this.setState({
      value: value,
      index: index,
    });

    this.props.onChange(value);
  };

  render() {
    const {
      type,
      label,
      required,
      tooltip,
      data,
      // itemColor,
      readOnly,
    } = this.props;
    return (
      <View style={styles.row}>
        <Text style={commonStyles.H4}>
          {label}
          {required && '*'}
        </Text>
        {tooltip && <Text style={styles.tooltipText}>{tooltip}</Text>}
        <View style={styles.radioGroupWrap}>
          <RadioForm
            radio_props={data}
            initial={this.state.index}
            formHorizontal={false}
            labelHorizontal={true}
            buttonColor={'black'}
            buttonSize={12}
            buttonOuterSize={20}
            selectedButtonColor={'#262626'}
            labelColor={type === 'followup' ? '#292A3E' : 'black'}
            selectedLabelColor={'black'}
            animation={true}
            radioStyle={styles.radioBtn}
            labelStyle={[
              styles.radioLabel,
              type === 'followup' && {color: '#292A3E'},
            ]}
            onPress={this.onPress}
            disabled={readOnly === true ? true : false}
          />
        </View>
      </View>
    );
  }
}
