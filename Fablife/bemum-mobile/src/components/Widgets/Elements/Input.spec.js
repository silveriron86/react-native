import React from 'react';
import Input from './Input';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<Input />', () => {
  it('should fire onChange events', () => {
    const onChange = jest.fn();
    const rendered = render(<Input value="" onChange={onChange} />);
    const inputComponent = rendered.getByTestId('input');
    fireEvent(inputComponent, 'changeText', 'test');

    expect(onChange).toHaveBeenCalledWith('test');
  });
});
