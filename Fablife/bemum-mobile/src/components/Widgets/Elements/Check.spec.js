import React from 'react';
import Check from './Check';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<Check />', () => {
  it('should fire onChange events', () => {
    const onChange = jest.fn();
    const rendered = render(<Check value={true} onChange={onChange} />);
    const checkComponent = rendered.getByTestId('check');
    fireEvent(checkComponent, 'onValueChange', {nativeEvent: {}});
    expect(onChange).toHaveBeenCalled();
  });
});
