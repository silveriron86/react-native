import React from 'react';
import Switch from './Switch';
import {render, cleanup, fireEvent} from 'react-native-testing-library';

afterEach(cleanup);

describe('<Switch />', () => {
  const choices = [
    {
      label: 'Oui',
      value: true,
    },
    {
      label: 'Non',
      value: false,
    },
  ];

  it('should fire onChange events', () => {
    const onChange = jest.fn();
    const rendered = render(
      <Switch
        value={false}
        label="test"
        required={false}
        data={choices}
        readOnly={false}
        onChange={onChange}
      />,
    );
    const switchComponent = rendered.getByTestId('switch-wrapper');
    fireEvent(switchComponent.children[0], 'onToggle');

    expect(onChange).toHaveBeenCalled();
  });
});
