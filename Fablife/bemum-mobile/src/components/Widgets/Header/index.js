import React from 'react';
import {StyleSheet, Platform, ImageBackground} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Colors} from '../../../constants';
import Utils from '../../../utils';
import commonStyles from '../../Layouts/Styles';

const headerBG = require('../../../../assets/images/header_bg.png');

export default class Header extends React.Component {
  render() {
    if (this.props.isMenuTab === true) {
      return (
        <LinearGradient
          useAngle={true}
          angle={45}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={[Colors.theme.DARK, '#F9CFCA', 'white']}
          style={[
            styles.linearGradient,
            commonStyles.headerStyle,
            this.props.style,
          ]}>
          {this.props.children}
        </LinearGradient>
      );
    }

    return (
      <ImageBackground
        source={headerBG}
        style={[
          styles.linearGradient,
          commonStyles.headerStyle,
          this.props.style,
        ]}
        imageStyle={[styles.imageStyle]}>
        {this.props.children}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  linearGradient: {
    width: '100%',
    height: Platform.OS === 'android' ? 60 : Utils.getInsetTop() > 20 ? 90 : 65,
    justifyContent: 'center',
  },
  imageStyle: {
    resizeMode: 'cover',
  },
});
