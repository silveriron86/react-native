import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import styles from '../_styles';

export default class PrimaryButton extends React.Component {
  render() {
    const {onPress, title, style} = this.props;
    return (
      <TouchableOpacity
        testID="button"
        onPress={onPress}
        style={[styles.button, style]}>
        <Text style={styles.buttonText}>{title}</Text>
      </TouchableOpacity>
    );
  }
}
