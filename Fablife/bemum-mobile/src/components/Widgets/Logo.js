import React from 'react';
import {View, Image} from 'react-native';
import styles from '../Layouts/Login/_styles';

const logoIcon = require('../../../assets/images/logo.png');

export default class Logo extends React.Component {
  render() {
    const {style} = this.props;
    return (
      <View style={[styles.logoWrapper, style]}>
        <Image source={logoIcon} style={styles.imgLogo} />
      </View>
    );
  }
}
