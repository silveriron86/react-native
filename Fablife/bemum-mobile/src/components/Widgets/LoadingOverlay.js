import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import styles from './_styles';

export default class LoadingOverlay extends React.Component {
  render() {
    const {loading, containerStyle} = this.props;

    return loading ? (
      <View style={[styles.loading, containerStyle]}>
        <ActivityIndicator size={'large'} />
      </View>
    ) : null;
  }
}
