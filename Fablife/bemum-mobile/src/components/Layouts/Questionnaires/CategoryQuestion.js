import React from 'react';
import {connect} from 'react-redux';
import {SafeAreaView} from 'react-native';
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';
import CommonStyles from '../Styles';
import {CrispLoader} from '../../Widgets';
// import QuestionsListView from './QuestionsListView';
import Form from './Form';
import Utils from '../../../utils';

class CategoryQuestionScreen extends React.Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     tags: ['Test 1', 'Test 2', 'Test 3'],
  //   };
  // }

  componentDidMount() {
    const {category} = this.props.route.params;
    let headerTitle = category.title;
    if (category.form_id === 'moral_wellbeing') {
      headerTitle = 'Moral & bien-être';
    } else if (category.form_id === 'memory_concentration') {
      headerTitle = 'Mémoire';
    }

    Utils.logEvent({
      screenName: 'Question',
      className: 'CategoryQuestionScreen',
    });

    this.props.navigation.setOptions({
      headerTitle,
    });
  }

  goNext = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  // changeFormData = (question_id, value) => {
  //   console.log('* changeFormData', question_id, value);
  // };

  // onCloseTags = index => {
  //   let tags = this.state.tags;
  //   tags.splice(index, 1);
  //   this.setState({tags});
  // };

  render() {
    const {navigation, route} = this.props;
    // const {tags} = this.state;
    const {category, type, readOnly} = route.params;
    console.log(category.form_id, category.survey_id, type);
    // console.log(category);
    return (
      <SafeAreaView style={[CommonStyles.container]}>
        <KeyboardAvoidingScrollView
          contentContainerStyle={CommonStyles.wrapper}>
          {/* <QuestionsListView
            questions={category.questions}
            navigation={navigation}
            changeFormData={this.changeFormData}
          /> */}
          <Form
            id={category.form_id}
            surveyId={category.survey_id}
            type={type}
            navigation={navigation}
            readOnly={readOnly}
          />
          {/* <Tags
            data={tags}
            onClose={this.onCloseTags}
            style={{marginVertical: 5}}
          /> */}
          {/* <View style={styles.validateBtn}>
            <PrimaryButton onPress={this.goNext} title="Valider" />
          </View> */}
        </KeyboardAvoidingScrollView>
        <CrispLoader />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CategoryQuestionScreen);
