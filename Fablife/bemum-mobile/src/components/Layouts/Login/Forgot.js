/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  TouchableWithoutFeedback,
  Keyboard,
  TextInput,
  TouchableOpacity,
  Text,
  KeyboardAvoidingView,
  Platform,
  ImageBackground,
  SafeAreaView,
} from 'react-native';
import {AccountActions} from '../../../actions';
import styles from './_styles';
import {Logo, LoadingOverlay} from '../../Widgets';
import commonStyles from '../Styles';
import Utils from '../../../utils';

const loginBgImg = require('../../../../assets/images/login_bg.png');

class ForgotScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      hasError: false,
      loading: false,
      message: '',
      sentStatus: 0,
    };
  }

  componentDidMount() {
    Utils.logEvent({
      screenName: 'Forgot',
      className: 'ForgotScreen',
    });
  }

  onPressForgot = () => {
    const {email} = this.state;
    if (!email) {
      this.setState({
        hasError: true,
        message: 'Veuillez entrer une adresse email valide',
      });
      return;
    }

    this.setState(
      {
        loading: true,
        hasError: false,
        message: '',
      },
      () => {
        this.props.forgot({
          email: this.state.email,
          cb: res => {
            console.log(res);
            this.setState({
              loading: false,
              sentStatus: res.success
                ? res.response.status
                : res.error.response.status,
            });
          },
        });
      },
    );
  };

  render() {
    const {loading, hasError, message, sentStatus} = this.state;
    let errorText = '';
    let statusText =
      'Vous recevrez dans quelques instants à cette adresse un email pour réinitialiser votre mot de passe.';
    if (hasError) {
      errorText = message;
    } else {
      switch (sentStatus) {
        case 202:
          statusText =
            'Un email contenant les instructions de réinitialisation vous a été envoyé.';
          break;
        case 400:
          errorText = 'Veuillez entrer une adresse e-mail valide';
          statusText =
            'Vous recevrez dans quelques instants à cette adresse un email pour réinitialiser votre mot de passe.';
          break;
        case 404:
          errorText = "Aucun compte n'est associé à cette adresse";
          statusText = '';
          break;
        case 429:
          statusText =
            'Une demande vient d’être réalisée pour ce compte, veuillez patienter quelques instants avant de réessayer.';
          break;
        case 500:
          errorText =
            'Une erreur technique est survenue, veuillez réessayez dans quelques instants.';
          statusText = '';
          break;
      }
    }

    return (
      <ImageBackground source={loginBgImg} style={styles.screenBackground}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <SafeAreaView style={commonStyles.flex1}>
            <KeyboardAvoidingView
              style={styles.container}
              behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
              <LoadingOverlay loading={loading} />
              <Logo style={{marginTop: -80}} />
              {/* <Text style={[commonStyles.Legend, {marginBottom: 10}]}>
                Nous vous enverrons un email avec un lien pour réinitialiser
                votre mot de passe.
              </Text> */}
              <View style={styles.inputWrapper}>
                <TextInput
                  keyboardType="email-address"
                  style={[styles.input, commonStyles.TextSecondary]}
                  placeholder="Adresse e-mail"
                  textContentType="emailAddress"
                  placeholderTextColor="#5C5C5C"
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  onChangeText={email => this.setState({email})}
                />
                {errorText !== '' && (
                  <View style={{marginHorizontal: 10, marginTop: 10}}>
                    <Text style={styles.msg}>{errorText}</Text>
                  </View>
                )}

                <View
                  style={{
                    marginHorizontal: 10,
                    marginTop: 8,
                    marginBottom: 20,
                  }}>
                  <Text style={commonStyles.Legend}>{statusText}</Text>
                </View>
              </View>
              <View style={[styles.btnWrapper, {marginTop: 1}]}>
                <TouchableOpacity
                  style={styles.button}
                  underlayColor="#fff"
                  onPress={() => this.onPressForgot()}>
                  <Text style={styles.buttonText}>Envoyer</Text>
                </TouchableOpacity>
              </View>
            </KeyboardAvoidingView>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </ImageBackground>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    forgot: req => dispatch(AccountActions.forgot(req.email, req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotScreen);
