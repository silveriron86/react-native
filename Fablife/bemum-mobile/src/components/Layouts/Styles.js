import {StyleSheet, Platform} from 'react-native';
import {Colors} from '../../constants';
import Utils from '../../utils';
// import Colors from '../../constants/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
  },
  flex1: {
    flex: 1,
  },
  wrapper: {
    padding: 24,
  },
  topBorder0: {
    borderTopWidth: 0,
  },
  qCategoryBtn: {
    borderWidth: 1,
    borderColor: 'black',
    padding: 8,
    marginVertical: 3,
  },
  qCategoryBtnText: {
    color: 'black',
    textAlign: 'center',
  },
  btnText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: Colors.theme.BLACK,
  },
  smallText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 10,
    color: Colors.theme.BLACK,
  },
  labelSubTitle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: Colors.DARK_GREY,
  },
  H1: {
    // fontFamily: 'Americans-Regular',
    fontFamily: 'AmericanaStd',
    fontSize: 26,
    color: Colors.theme.BLACK,
    // width: Dimensions.get('window').width - 130,
    // width: '100%',
    textAlign: 'left',
  },
  H2: {
    // fontFamily: 'Americans-Bold',
    fontFamily: 'AmericanaStd',
    fontSize: 25,
    color: Colors.theme.BLACK,
  },
  H3: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: Colors.theme.DARK_GREY,
    letterSpacing: 0.57,
  },
  Legend: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: Colors.DARK_GREY,
  },
  TextSecondary: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: Colors.DARK_GREY,
  },
  Text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    lineHeight: 23,
    color: 'black',
  },
  H4: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: Colors.theme.BLACK,
  },
  H5: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: Colors.theme.BLACK,
  },
  textLeft: {
    textAlign: 'left',
  },
  textCenter: {
    textAlign: 'center',
  },
  mt0: {
    marginTop: 0,
  },
  rightArrowIcon: {
    width: 10,
    height: 20,
    tintColor: Colors.theme.BLACK,
    marginLeft: 11,
  },
  flexCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  headerStyle: {
    height: Platform.OS === 'ios' ? Utils.getInsetTop() + 85 - 20 : 85,
  },
  checkIcon: {
    width: 22,
    height: 22,
    marginRight: 16,
  },
  profilingCatRow: {
    borderRadius: 10,
    backgroundColor: Colors.theme.LIGHT,
    height: 70,
    paddingHorizontal: 20,
    marginVertical: 6,
    tintColor: 'white',
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: 5},
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectedCatRow: {
    backgroundColor: Colors.theme.MAIN,
  },
  disabledRow: {
    backgroundColor: '#F4F4F4',
  },
  disabledText: {
    color: '#636872',
  },
  profilingCatText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 17,
  },
  questionRow: {
    paddingBottom: 25,
    marginBottom: 25,
    borderBottomWidth: 1,
    borderBottomColor: '#EAEAEA',
  },
  textRow: {
    // marginTop: 25,
    paddingBottom: 15,
  },
  bottomBorder: {
    paddingTop: 25,
    marginBottom: 25,
    borderBottomWidth: 1,
    borderBottomColor: '#EAEAEA',
  },
  pb0: {
    paddingBottom: 0,
  },
});
