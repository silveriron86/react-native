import React from 'react';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {View} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import styles from './Login/_styles';

class InitialScreen extends React.Component {
  UNSAFE_componentWillMount() {
    AsyncStorage.getItem('ACCESS_TOKEN', (__err, accessToken) => {
      AsyncStorage.getItem('USER_ID', (_err, userID) => {
        global.accessToken = accessToken;
        global.userID = userID;
        AsyncStorage.getItem(
          'completedSignupSurveys',
          (___err, InputtingSurveyIndex) => {
            AsyncStorage.getItem(
              'completedFoodPreferences',
              (____err, completedFood) => {
                const defaultRoute =
                  InputtingSurveyIndex === null ||
                  parseInt(InputtingSurveyIndex, 10) < 9
                    ? 'FirstQuestionnaires'
                    : completedFood
                    ? 'TabsStack'
                    : 'FoodPreferences';

                // navigation.dispatch(
                //   StackActions.replace(accessToken ? defaultRoute : 'Login'),
                // );
                this.props.navigation.dispatch(
                  CommonActions.reset({
                    index: 0,
                    routes: [{name: accessToken ? defaultRoute : 'Login'}],
                  }),
                );
              },
            );
          },
        );
      });
    });
  }

  render() {
    return <View style={styles.container} />;
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InitialScreen);
