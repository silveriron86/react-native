import {StyleSheet} from 'react-native';
import {Colors} from '../../../../constants';

export default StyleSheet.create({
  dateSelContainer: {
    shadowColor: '#999',
    shadowOffset: {
      width: 0,
      height: 9,
    },
    shadowOpacity: 0.48,
    shadowRadius: 10,
    elevation: 1,
    backgroundColor: 'white',
    marginBottom: 15,
    paddingHorizontal: 20,
    paddingTop: 15,
    paddingBottom: 0,
  },
  dateSelWrapper: {
    width: '100%',
    marginBottom: 15,
  },
  dateSelBtn: {
    backgroundColor: '#FFFFFF19',
    paddingHorizontal: 11,
    paddingVertical: 6,
    marginRight: 8,
    borderWidth: 1,
    borderColor: Colors.theme.MAIN,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedDateSelBtn: {
    backgroundColor: Colors.theme.MAIN,
  },
  dateText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: Colors.theme.DARK,
    textAlign: 'center',
  },
  input: {
    borderWidth: 1,
    height: 38,
    marginTop: 2,
    marginBottom: 15,
  },
  sectionTitle: {
    marginVertical: 10,
  },
  actionBtns: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  minusPlusBtn: {
    backgroundColor: Colors.theme.LIGHT,
    width: 35,
    height: 35,
    borderRadius: 17.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  minusPlusText: {
    fontSize: 25,
    lineHeight: 25,
    color: Colors.theme.DARK,
  },
  valueText: {
    fontSize: 19,
    textAlign: 'center',
    // width: 70,
  },
  searchIcon: {
    width: 25,
    height: 25,
    marginTop: -15,
    marginRight: 20,
  },
  searchInput: {
    fontSize: 16,
    borderWidth: 0,
  },
  searchWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  withColsRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 5,
  },
  closeBtn: {
    backgroundColor: Colors.theme.MAIN,
    width: 30,
    height: 30,
    marginTop: 8,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgClose: {
    width: 18,
    height: 18,
    tintColor: 'black',
  },
});
