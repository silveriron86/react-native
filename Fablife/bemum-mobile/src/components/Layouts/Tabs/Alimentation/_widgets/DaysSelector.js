/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import moment from 'moment';
import 'moment/locale/fr';
import commonStyles from '../../../Styles.js';
import styles from '../_styles';
import Colors from '../../../../../constants/Colors';

export default class DaysSelector extends React.Component {
  constructor(props) {
    super(props);
    moment.locale('fr');
  }

  toggleExpand = () => {
    this.setState({
      isCalendarOpened: !this.state.isCalendarOpened,
    });
  };

  render() {
    const WEEKS = ['DIM', 'LUN', 'MAR', 'MER', 'JEU', 'VEN', 'SAM'];
    const startDate = moment().subtract(6, 'day');
    const {currentDate, onSelect} = this.props;
    const currentWDay = currentDate.format('D');

    let cols = [];

    for (let i = 0; i < 7; i++) {
      const dt = moment(startDate).add(i, 'day');
      const weekDay = dt.format('d');
      const selected = currentWDay === dt.format('D');

      cols.push(
        <TouchableOpacity
          underlayColor="#fff"
          onPress={() => onSelect(dt)}
          key={`date_${i}`}
          style={[
            styles.dateCol,
            selected && styles.selectedDateBtn,
            {zIndex: 1},
          ]}>
          <Text style={commonStyles.smallText}>{WEEKS[weekDay]}</Text>
          <View style={styles.dateBtn}>
            <Text
              style={[
                styles.dateBtnText,
                selected && styles.selectedDateBtnText,
              ]}>
              {dt.format('D')}
            </Text>
          </View>
        </TouchableOpacity>,
      );
    }

    const selectedDot = {
      key: 'selected',
      color: 'red',
      selectedDotColor: 'blue',
    };
    const markedDates = {};
    markedDates[currentDate.format('YYYY-MM-DD')] = {
      dots: [selectedDot],
      selected: true,
      selectedColor: Colors.DARK_LINEN,
    };

    return (
      <View
        style={[
          styles.wdContainer,
          {
            position: 'relative',
            borderBottomWidth: 1,
            borderBottomColor: Colors.LINEN,
            height: 80,
          },
        ]}>
        <View
          style={{
            paddingHorizontal: 20,
          }}>
          <View style={[styles.dateSelector]}>{cols}</View>
        </View>
      </View>
    );
  }
}
