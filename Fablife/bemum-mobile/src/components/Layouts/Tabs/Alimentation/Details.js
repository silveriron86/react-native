/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Platform,
  Text,
  Image,
  View,
} from 'react-native';
import ImageProgress from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import commonStyles from '../../Styles';
import {
  Header,
  BackButton,
  PrimaryButton,
  LoadingOverlay,
} from '../../../Widgets';
import {Calories} from './_widgets';
import styles from './_styles';
import Utils from '../../../../utils';
import LikesButtons from '../../../Widgets/Buttons/LikesButtons';
import {
  RecipeActions,
  MealPlanningActions,
  AccountActions,
} from '../../../../actions';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import ActionButtons from '../Courses/_widgets/ActionButtons';
import {MyAlert} from '../../../Widgets';

const circleIcon = require('../../../../../assets/icons/choix2.png');
const userIcon = require('../../../../../assets/icons/tab4.png');

class AlimentationDetailsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.alert = React.createRef();
    this.state = {
      loading: true,
      recipe: null,
      selected: null,
    };
  }

  componentDidMount() {
    Utils.logEvent({
      screenName: 'AlimentationDetails',
      className: 'AlimentationDetailsScreen',
    });

    AsyncStorage.getItem('SELECTED_RECIPE', (_err, got) => {
      if (got) {
        const selected = JSON.parse(got);
        this.setState(
          {
            selected,
          },
          () => {
            // this.props.navigation.setOptions({
            //   headerTitle: selected ? selected.name : '',
            // });
          },
        );

        this.props.getRecipe({
          recipeId: selected.id,
          cb: res => {
            console.log('*** recipe= ', res);
            this.setState({
              loading: false,
              recipe: res,
            });
          },
        });
      }
    });
  }

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  onChoose = () => {
    const {mealPlanning, route} = this.props;
    const {data} = route.params;
    console.log('*** mealPlanning = ', mealPlanning, data);
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.updateMealPlanning({
          mealPlanningId: mealPlanning.id,
          data,
          cb: res => {
            console.log(res);
            this.setState(
              {
                loading: false,
              },
              () => {
                if (res.success) {
                  global.eventEmitter.emit('UPDATE_MEALPLANNING', {
                    currentDate: '',
                  });
                  this.goBack();
                } else {
                  this.alert.current.show('Erreur', 'Une erreur est survenue');
                }
              },
            );
          },
        });
      },
    );
  };

  onDislike = index => {
    const {recipe} = this.state;
    if (index === 2) {
      this.props.navigation.navigate('FoodExclusion');
    } else {
      let data = null;
      if (index === 3) {
        data = {
          reason: 'too-frequent',
        };
      } else if (index === 4) {
        data = {
          reason: 'badly-written',
        };
      }
      this.setState(
        {
          loading: true,
        },
        () => {
          this.props.dislike({
            recipeId: recipe.id,
            data,
            cb: res => {
              console.log(res);
              this.setState(
                {
                  loading: false,
                },
                () => {
                  const alreadyDisliked =
                    typeof res.data !== 'undefined' &&
                    res.data.statusCode === 409;
                  const success = res.success;

                  if (alreadyDisliked) {
                    this.alert.current.show(
                      'Déjà enregistré',
                      'Les changements seront visibles sur vos prochains menus.',
                    );
                  } else {
                    this.alert.current.show(
                      success ? 'Compris !' : 'Une erreur est survenue',
                      success
                        ? 'Cette recette ne vous sera plus proposée.'
                        : "Nous n'avons pas pu prendre en compte votre demande.",
                    );
                  }
                },
              );
            },
          });
        },
      );
    }
  };

  render() {
    const {navigation} = this.props;
    const insetTop = Platform.OS === 'android' ? 0 : Utils.getInsetTop();
    const {loading, recipe, selected} = this.state;

    // const {selected} = this.props.route.params;
    // const {data} = this.props.route.params;
    let ingredients = [];
    let steps = [];
    if (recipe) {
      recipe.ingredients.forEach(ing => {
        ingredients.push(ing);
      });
      if (recipe.steps) {
        steps = JSON.parse(recipe.steps);
      }
    }

    return (
      <>
        <ScrollView style={commonStyles.flex1}>
          <Header
            style={{
              height: Platform.OS === 'android' ? 120 : 148,
              padding: 20,
            }}>
            <View
              style={{
                paddingTop: Platform.OS === 'android' ? 10 : insetTop,
                flexDirection: 'row',
                alignItems: 'flex-start',
              }}>
              <BackButton
                style={[
                  // {top: insetTop > 20 ? insetTop : insetTop + 10},
                  {
                    position: 'relative',
                    // backgroundColor: 'white',
                    marginLeft: 0,
                    marginTop: Platform.OS === 'android' ? -25 : -15,
                  },
                ]}
                navigation={navigation}
              />
              <View style={{flex: 1, paddingLeft: 15}}>
                <Text numberOfLines={1} style={commonStyles.H1}>
                  {selected ? selected.name : ''}
                </Text>
                {recipe !== null && (
                  <View
                    style={[
                      styles.headerInfos,
                      {justifyContent: 'center', marginLeft: -50},
                    ]}>
                    <Image source={circleIcon} style={styles.infoIcons} />
                    <Text style={styles.infoText}>
                      {recipe.timings.preparation / 60 / 1000} min
                    </Text>
                    <View style={styles.w20} />
                    <Image source={userIcon} style={styles.infoIcons} />
                    <Text style={styles.infoText}>{recipe.servings} pers</Text>
                  </View>
                )}
              </View>
            </View>
          </Header>
          <SafeAreaView style={commonStyles.container}>
            <ImageProgress
              source={{
                uri: recipe !== '' && recipe !== null ? recipe.imageUrl : '',
              }}
              indicator={ProgressBar}
              style={[
                styles.headerImage,
                insetTop > 20 && {height: 250 + insetTop},
              ]}>
              <LikesButtons isDetail={true} onDislike={this.onDislike} />
            </ImageProgress>
            {recipe !== null && (
              <View style={{padding: 24, paddingTop: 0}}>
                <Calories title="VALEURS NUTRITIONNELLES" recipe={recipe} />
                <View style={styles.section}>
                  <Text style={commonStyles.H3}>
                    {'Ingrédients'.toUpperCase()}
                  </Text>
                  {ingredients.map((ingre, index) => (
                    <ActionButtons
                      key={`ingre-${index}`}
                      defaultValue={ingre.qty}
                      name={ingre.ingredient.name}
                      unit={ingre.unit ? ingre.unit.name : ''}
                      readOnly={true}
                    />
                  ))}
                </View>
                <View style={styles.section}>
                  <Text style={commonStyles.H3}>INSTRUCTIONS</Text>
                  {steps.map((inst, index) => (
                    <View style={styles.instRow} key={`inst-${index}`}>
                      <Text style={commonStyles.H1}>0{index + 1}</Text>
                      <View style={styles.w20} />
                      <View style={{flex: 1}}>
                        <Text style={[commonStyles.TextSecondary]}>
                          {inst.instructions}
                        </Text>
                      </View>
                    </View>
                  ))}
                </View>
                <View>
                  <PrimaryButton
                    onPress={this.onChoose}
                    title="Choisir cette recette"
                  />
                </View>
              </View>
            )}
          </SafeAreaView>
        </ScrollView>
        <LoadingOverlay loading={loading} />
        <MyAlert ref={this.alert} />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    mealPlanning: state.MealPlanningReducer.mealPlanning,
    recipe: state.RecipeReducer.recipe,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRecipe: req => dispatch(RecipeActions.get(req.recipeId, req.cb)),
    dislike: req =>
      dispatch(RecipeActions.dislike(req.recipeId, req.data, req.cb)),
    updateMealPlanning: req =>
      dispatch(
        MealPlanningActions.update(req.mealPlanningId, req.data, req.cb),
      ),
    updateIngredientsToAvoid: req =>
      dispatch(AccountActions.updateIngredientsToAvoid(req.data, req.cb)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AlimentationDetailsScreen);
