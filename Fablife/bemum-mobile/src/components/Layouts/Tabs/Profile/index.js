/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {View, Text, TouchableOpacity, Linking} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import commonStyles from '../../Styles';
import styles from './_styles';
import {RedButton, CrispLoader, LoadingOverlay} from '../../../Widgets';
import * as rootNavigation from '../../../../navigation/rootNavigation';
import {AccountActions, SurveyActions} from '../../../../actions';

// const rightArrow = require('../../../../../assets/icons/right_arrow.png');

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }
  componentDidMount() {
    this.loadFoodPreference();
    global.eventEmitter.addListener('VISITED_PROFILE', () => {
      this.loadFoodPreference();
    });
  }

  loadFoodPreference = () => {
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.getForm({
          type: 'signup',
          survey_id: 'food-preference',
          form_id: 'preference_food',
          cb: foodForm => {
            console.log('*** foodForm = ', foodForm);
            this.setState({
              loading: false,
            });
          },
        });
      },
    );
  };

  onLogOut = () => {
    AsyncStorage.getItem('fcmToken', (_err, fcmToken) => {
      // AsyncStorage.removeItem('ACCESS_TOKEN');
      AsyncStorage.getItem('VISITED_WELCOME1', (__err, visited1) => {
        AsyncStorage.getItem('VISITED_WELCOME2', (___err, visited2) => {
          AsyncStorage.clear();
          global.accessToken = '';
          if (visited1) {
            AsyncStorage.setItem('VISITED_WELCOME1', 'true');
          }
          if (visited2) {
            AsyncStorage.setItem('VISITED_WELCOME2', 'true');
          }
          if (fcmToken) {
            AsyncStorage.setItem('fcmToken', fcmToken);
          }
          this.props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{name: 'Login'}],
            }),
          );
        });
      });
    });
  };

  goPage = page => {
    if (page === 'FoodPreferences') {
      rootNavigation.navigate(page, {
        from: 'Profile',
      });
    } else {
      const {navigation} = this.props;
      navigation.navigate(page);
    }
  };

  render() {
    const {loading} = this.state;
    const {patient} = this.props;
    const disabled = patient === null || patient.updateProfile !== true;
    return (
      <View style={[commonStyles.container, styles.container]}>
        <View style={styles.links}>
          <TouchableOpacity
            style={styles.profileLink}
            onPress={() => this.goPage('SignupQuestsView')}>
            <Text style={commonStyles.Text}>
              Consulter mes données initiales
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.profileLink}
            disabled={disabled}
            onPress={() => this.goPage('InformationSheet')}>
            <Text style={[commonStyles.Text, disabled && {color: '#888'}]}>
              Mettre à jour mes données de profil
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.profileLink}
            onPress={() => this.goPage('FoodPreferencesProfile')}>
            <Text style={commonStyles.Text}>
              Modifier mes préférences alimentaires
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.profileLink}
            onPress={() => this.goPage('NotificationSettings')}>
            <Text style={commonStyles.Text}>Gérer mes notifications</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.profileLink}
            onPress={() => {
              Linking.openURL(
                'https://www.bemum.co/legal/protection-des-donnees',
              );
            }}>
            <Text style={commonStyles.Text}>
              Consulter notre politique de confidentialité
            </Text>
          </TouchableOpacity>

          {/* <View style={{padding: 24, width: '100%'}}>
            <OutlineButton style={{height: 70, marginTop: 20}}>
              <View style={commonStyles.flexCenter}>
                <Text style={commonStyles.H4}>
                  Remplir le questionnaire SALUS
                </Text>
                <Image
                  source={rightArrow}
                  style={commonStyles.rightArrowIcon}
                />
              </View>
            </OutlineButton>
          </View> */}
        </View>

        <View style={styles.logoutButton}>
          <RedButton title="Déconnexion" onPress={this.onLogOut} />
        </View>
        <CrispLoader hasTab={true} />
        <LoadingOverlay loading={loading} />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    patient: state.AccountReducer.patient,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPatient: req => dispatch(AccountActions.get(req.id, req.cb)),
    getForm: request =>
      dispatch(
        SurveyActions.getForm(
          request.type,
          request.survey_id,
          request.form_id,
          request.cb,
        ),
      ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileScreen);
