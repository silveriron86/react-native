/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {ScrollView, SafeAreaView} from 'react-native';
import CommonStyles from '../../Styles';
import {QuestionnairesList} from '../../../Widgets';
import {SurveyConstants} from '../../../../constants';
import {SurveyActions} from '../../../../actions';
import Utils from '../../../../utils';

class InformationSheetScreen extends React.Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     loading: 0,
  //     completed: 0,
  //   };
  // }

  goDetail = category => {
    const {navigation} = this.props;
    navigation.navigate('CategoryQuestion', {
      type: 'followup',
      category,
      readOnly: false,
    });
  };

  componentDidMount() {
    Utils.logEvent({
      screenName: 'Mettre',
      className: 'InformationSheetScreen',
    });

    AsyncStorage.getItem('completedFollowupSurveys', (err, v) => {
      console.log('get completedFollowupSurveys = ', err, v);
      if (!err && v) {
        this.props.completedFollowupSurvey(v);
      }
    });
    // AsyncStorage.getItem('completedFollowupSurveys', (_err, v) => {
    //   if (!v) {
    //     setTimeout(() => {
    //       SurveyConstants.FOLLOWUP_CATEGORIES.forEach((category, index) => {
    //         this.props.getForm({
    //           type: 'followup',
    //           survey_id: category.survey_id,
    //           form_id: category.form_id,
    //           cb: res => {
    //             this.setState(
    //               {
    //                 loading: this.state.loading + 1,
    //               },
    //               () => {
    //                 console.log('status = ', res.status);
    //                 if (typeof res.status === 'undefined') {
    //                   const completed = this.state.completed + 1;
    //                   this.setState(
    //                     {
    //                       completed,
    //                     },
    //                     () => {
    //                       console.log('*** ', completed);
    //                       this.props.completedFollowupSurvey(completed);
    //                       AsyncStorage.setItem(
    //                         'completedFollowupSurveys',
    //                         completed.toString(),
    //                       );
    //                     },
    //                   );
    //                 }
    //               },
    //             );
    //           },
    //         });
    //       });
    //     }, 500);
    //   }
    // });
  }

  render() {
    const {InputtingSurveyIndex} = this.props;
    // const {loading} = this.state;

    return (
      <SafeAreaView style={CommonStyles.container}>
        <ScrollView contentContainerStyle={CommonStyles.wrapper}>
          <QuestionnairesList
            InputtingSurveyIndex={InputtingSurveyIndex}
            categories={SurveyConstants.FOLLOWUP_CATEGORIES}
            onPress={this.goDetail}
            containerStyle={{paddingTop: 0}}
          />
        </ScrollView>
        {/* <LoadingOverlay
          loading={loading < SurveyConstants.FOLLOWUP_CATEGORIES.length}
        /> */}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    loadedSurveys: state.SurveyReducer.loadedSurveys,
    InputtingSurveyIndex: state.SurveyReducer.InputtingFollowupSurveyIndex,
    profileFormData: state.SurveyReducer.followup_profileFormData,
    healthFormData: state.SurveyReducer.followup_healthFormData,
    reproductive_healthFormData:
      state.SurveyReducer.followup_reproductive_healthFormData,
    physical_activityFormData:
      state.SurveyReducer.followup_physical_activityFormData,
    sleep_fatigueFormData: state.SurveyReducer.followup_sleep_fatigueFormData,
    stressFormData: state.SurveyReducer.followup_stressFormData,
    moral_wellbeingFormData:
      state.SurveyReducer.followup_moral_wellbeingFormData,
    memory_concentrationFormData:
      state.SurveyReducer.followup_memory_concentrationFormData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    completedFollowupSurvey: index =>
      dispatch(SurveyActions.completedFollowupSurvey(index)),
    getForm: request =>
      dispatch(
        SurveyActions.getForm(
          request.type,
          request.survey_id,
          request.form_id,
          request.cb,
        ),
      ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InformationSheetScreen);
