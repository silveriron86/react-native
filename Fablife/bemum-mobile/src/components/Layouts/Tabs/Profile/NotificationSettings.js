import React from 'react';
import {View} from 'react-native';
import commonStyles from '../../Styles';
import styles from './_styles';
import {Switch} from '../../../Widgets';
import Utils from '../../../../utils';

export default class NotificationSettingsScreen extends React.Component {
  state = {
    aa: true,
    bb: false,
  };

  toggleSwitch = arg => {
    let state = this.state;
    state[arg] = !state[arg];
    this.setState(state);
  };

  componentDidMount() {
    Utils.logEvent({
      screenName: 'NotificationSettings',
      className: 'NotificationSettingsScreen',
    });
  }

  render() {
    const {aa, bb} = this.state;
    return (
      <View style={commonStyles.container}>
        <View style={styles.notificationRow}>
          <Switch
            size={'small'}
            label="Je dois mettre à jour mon profil"
            data={[{label: ''}, {label: ''}]}
            value={aa}
            onChange={value => this.toggleSwitch('aa')}
          />
        </View>
        <View style={styles.notificationRow}>
          <Switch
            size={'small'}
            label="J'ai reçu un nouveau message"
            data={[{label: ''}, {label: ''}]}
            value={bb}
            onChange={value => this.toggleSwitch('bb')}
          />
        </View>
      </View>
    );
  }
}
