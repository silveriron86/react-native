import CoursesScreen from './Courses';
import AddProductModal from './Courses/AddProduct';
import ProgressionScreen from './Progression';
import ProfilingCategoryScreen from './Progression/Monitoring/ProfilingCategory';
import ProfilingDetailScreen from './Progression/Monitoring/ProfilingDetail';

import AlimentationScreen from './Alimentation';
import AlimentationDetailsScreen from './Alimentation/Details';
import AlimentationMenuScreen from './Alimentation/Menu';
import AlimentationMenuDetailsScreen from './Alimentation/MenuDetails';
import AlimentationOutsideScreen from './Alimentation/Outside';

import ProfileScreen from './Profile';
import SignupQuestsViewScreen from './Profile/SignupQuestsView';
import InformationSheetScreen from './Profile/InformationSheet';
import GeneralUseConditionScreen from './Profile/GeneralUseCondition';
import NotificationSettingsScreen from './Profile/NotificationSettings';
import TermsScreen from './Profile/Terms';

export {
  CoursesScreen,
  AddProductModal,
  ProgressionScreen,
  AlimentationScreen,
  AlimentationDetailsScreen,
  AlimentationMenuScreen,
  AlimentationMenuDetailsScreen,
  AlimentationOutsideScreen,
  ProfileScreen,
  SignupQuestsViewScreen,
  InformationSheetScreen,
  NotificationSettingsScreen,
  TermsScreen,
  GeneralUseConditionScreen,
  ProfilingCategoryScreen,
  ProfilingDetailScreen,
};
