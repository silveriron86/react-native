/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {LineChart, XAxis} from 'react-native-svg-charts';
import {Circle} from 'react-native-svg';
import styles from '../../_styles';
import commonStyles from '../../../../Styles';
import moment from 'moment';
import Utils from '../../../../../../utils';

export default class SuiviImcChart extends React.Component {
  state = {
    collapsed: true,
  };

  toggleExpand = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    const {metrics} = this.props;
    if (metrics === null) {
      return null;
    }

    const {collapsed} = this.state;
    let lineData = [];
    let dates = [];
    if (typeof metrics.BMI !== 'undefined') {
      metrics.BMI.forEach(w => {
        if (w.value !== null && w.value > 0) {
          lineData.push(parseFloat(w.value));
          const label = moment(w.date).format('MM/YY');
          dates.push(dates.indexOf(label) < 0 ? label : '');
        }
      });
    }

    const labels = [
      {color: '#B3A3FF', label: 'Sous-poids'},
      {color: '#0CE6A3', label: 'Normal'},
      {color: '#5DDEFC', label: 'Surpoids'},
      {color: '#B3DFFB', label: 'Obésité'},
    ];

    const axesSvg = {fontSize: 12, fill: 'grey'};
    const verticalContentInset = {top: 10, bottom: 10, left: 20, right: 20};
    const xAxisHeight = 30;
    const Decorator = ({x, y}) => {
      return lineData.map((value, index) => (
        <Circle
          key={`decorator-${index}`}
          cx={x(index)}
          cy={y(value)}
          r={4}
          stroke={'black'}
          fill={'black'}
        />
      ));
    };

    const WEIGHT =
      typeof metrics.WEIGHT !== 'undefined' && metrics.WEIGHT.length > 0
        ? parseFloat(metrics.WEIGHT[metrics.WEIGHT.length - 1].value)
        : 0;
    const BMI = lineData.length < 1 ? 1 : lineData[lineData.length - 1];

    const weight_to_BMI_18_5_v = (18.5 * WEIGHT) / BMI;
    const weight_to_BMI_18_5 = Utils.formatNumber(weight_to_BMI_18_5_v);
    const weight_to_BMI_25_v = (25 * WEIGHT) / BMI;
    const weight_to_BMI_25 = Utils.formatNumber(weight_to_BMI_25_v);
    const weight_to_BMI_30_v = (30 * WEIGHT) / BMI;
    const weight_to_BMI_30 = Utils.formatNumber(weight_to_BMI_30_v);

    const currentBMI =
      lineData.length < 1
        ? 0
        : Utils.formatNumber(lineData[lineData.length - 1]);

    let clrIndex = -1;
    let sameIndex = -1;

    if (WEIGHT < weight_to_BMI_18_5_v) {
      clrIndex = 0;
    } else if (WEIGHT > weight_to_BMI_18_5_v && WEIGHT < weight_to_BMI_25_v) {
      clrIndex = 1;
    } else if (WEIGHT > weight_to_BMI_25_v && WEIGHT < weight_to_BMI_30_v) {
      clrIndex = 2;
    } else if (WEIGHT > weight_to_BMI_30_v) {
      clrIndex = 3;
    } else {
      if (WEIGHT === weight_to_BMI_18_5_v) {
        sameIndex = 0;
      } else if (WEIGHT === weight_to_BMI_25_v) {
        sameIndex = 1;
      } else if (WEIGHT === weight_to_BMI_30_v) {
        sameIndex = 2;
      }
    }

    return (
      <>
        {collapsed && (
          <TouchableOpacity
            style={[styles.chatArea, styles.marginRow]}
            onPress={this.toggleExpand}>
            <View style={styles.chartLabelRow}>
              <View style={styles.ChartLabels}>
                <Text style={commonStyles.H4}>IMC</Text>
                {/* <View style={[styles.mark, styles.yellowBg]}>
                  <Text style={[commonStyles.labelSubTitle, {color: 'white'}]}>
                    surpoids
                  </Text>
                </View> */}
              </View>
              <Text
                style={[styles.ChartLabelValue, {fontFamily: 'AmericanaStd'}]}>
                {currentBMI}
              </Text>
            </View>
          </TouchableOpacity>
        )}

        {!collapsed && (
          <View style={[styles.chatArea, styles.marginRow]}>
            <TouchableOpacity
              style={[styles.chartLabelRow, styles.withBorderBottom]}
              onPress={this.toggleExpand}>
              <View style={styles.ChartLabels}>
                <Text style={commonStyles.H4}>IMC</Text>
                {/* <View style={[styles.mark, styles.yellowBg]}>
                  <Text style={[commonStyles.labelSubTitle, {color: 'white'}]}>
                    surpoids
                  </Text>
                </View> */}
              </View>
              <Text
                style={[styles.ChartLabelValue, {fontFamily: 'AmericanaStd'}]}>
                {currentBMI}
              </Text>
            </TouchableOpacity>

            <View style={[styles.mb20, {flexDirection: 'row'}]}>
              <Text style={styles.lineChartInfo}>
                IMC : poids (kg) / taille
              </Text>
              <Text style={[styles.lineChartInfo, {fontSize: 9}]}>2</Text>
              <Text style={styles.lineChartInfo}> (cm)</Text>
            </View>

            <View style={[styles.colorsHozBar]}>
              {labels.map((l, idx) => {
                return (
                  <View
                    key={`clr-${idx}`}
                    style={[
                      styles.colorHozCol,
                      {
                        width: '25%',
                        backgroundColor: l.color,
                      },
                    ]}>
                    {idx === clrIndex && (
                      <View
                        style={[
                          styles.centerColorCol,
                          // {backgroundColor: l.color},
                          {backgroundColor: 'black'},
                        ]}
                      />
                    )}
                  </View>
                );
              })}
            </View>
            {clrIndex === -1 && (
              <View
                style={{
                  paddingHorizontal: '2%',
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: -28,
                  marginBottom: 8,
                }}>
                {[0, 1, 2, 3].map((v, i) => {
                  if (i === sameIndex) {
                    return (
                      <View
                        key={`color-col-${clrIndex}-${i}`}
                        style={{
                          width: '25%',
                          alignItems: 'flex-end',
                        }}>
                        <View
                          style={[
                            styles.centerColorCol,
                            {backgroundColor: 'black'},
                            {marginRight: -11},
                          ]}
                        />
                      </View>
                    );
                  } else {
                    return null;
                  }
                })}
              </View>
            )}
            <View style={styles.colorLabelBar}>
              {[
                0,
                weight_to_BMI_18_5,
                weight_to_BMI_25,
                weight_to_BMI_30,
                0,
              ].map((v, i) => {
                if (i === 0 || i === 4) {
                  return <View key={`v-${i}`} style={{width: '12.5%'}} />;
                }
                return (
                  <View style={{width: '25%'}} key={`v-${i}`}>
                    <Text
                      style={[commonStyles.labelSubTitle, styles.colorLabel]}>
                      {v}kg
                    </Text>
                  </View>
                );
              })}
            </View>
            <View style={styles.imcChartWrapper}>
              {/* <YAxis
              data={lineData}
              style={{marginBottom: xAxisHeight}}
              contentInset={verticalContentInset}
              svg={axesSvg}
            /> */}
              <View style={[styles.colorsBar, {height: 234}]}>
                {labels.map((l, idx) => {
                  return (
                    <View
                      key={`clr-${idx}`}
                      style={{
                        // flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 6,
                        height: `${([7.5, 10, 6.5, 3.5][idx] / 27.5) * 100}%`,
                        backgroundColor: labels[labels.length - idx - 1].color,
                      }}
                    />
                  );
                })}
              </View>

              <View style={{flex: 1, marginLeft: 20, position: 'relative'}}>
                <View
                  style={{
                    position: 'absolute',
                    width: '100%',
                    height: 205,
                    marginTop: 6,
                    display: 'flex',
                    flexDirection: 'column',
                    borderTopWidth: 1,
                    borderTopColor: '#ccc',
                  }}>
                  {new Array(10).fill(0).map((opt, ii) => {
                    return (
                      <View
                        style={{
                          flex: 1,
                          borderBottomWidth: 1,
                          borderColor: '#ccc',
                        }}
                        key={`line-${ii}`}
                      />
                    );
                  })}
                </View>
                <LineChart
                  style={{flex: 1}}
                  data={lineData}
                  contentInset={verticalContentInset}
                  svg={{stroke: 'black'}}
                  yMin={15}
                  yMax={42.5}>
                  {/* <Grid /> */}
                  <Decorator />
                </LineChart>
                <XAxis
                  style={{
                    marginHorizontal: -20,
                    height: xAxisHeight,
                  }}
                  data={lineData}
                  formatLabel={(value, index) => {
                    return dates[index];
                  }}
                  contentInset={{left: 40, right: 40}}
                  svg={axesSvg}
                />
              </View>
            </View>
            <View style={styles.imcChartInfos}>
              {labels.map((l, idx) => {
                return (
                  <View style={styles.imcChartInfoCol} key={`lbl-${idx}`}>
                    <View
                      key={`clr-${idx}`}
                      style={[
                        styles.colorDot,
                        {marginRight: 10, backgroundColor: l.color},
                      ]}
                    />
                    <Text style={commonStyles.labelSubTitle}>{l.label}</Text>
                  </View>
                );
              })}
            </View>
          </View>
        )}
      </>
    );
  }
}
