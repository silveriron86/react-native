import React from 'react';
import {ScrollView} from 'react-native';
import commonStyles from '../../../Styles';

// Mon questionnaire de suivi
export default class FollowUpQuestionnaireModal extends React.Component {
  goDetail = category => {
    const {navigation} = this.props;
    navigation.navigate('CategoryQuestion', {type: 'signup', category});
  };

  render() {
    return (
      <ScrollView contentContainerStyle={commonStyles.wrapper}>
        {/* <QuestionnairesList categories={categories} onPress={this.goDetail} /> */}
      </ScrollView>
    );
  }
}
