/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text} from 'react-native';
import {ProgressCircle} from 'react-native-svg-charts';
import styles from '../../_styles';
import {Colors} from '../../../../../../constants';
import commonStyles from '../../../../Styles';
// import Utils from '../../../../../../utils';

export default class SuiviGaugeChart extends React.Component {
  render() {
    const {nutrinomes} = this.props;
    let BMR = 0;
    let daily_need_calories = 0;
    if (nutrinomes && nutrinomes.length > 0) {
      const firstOne = nutrinomes[nutrinomes.length - 1];
      daily_need_calories = firstOne.DAILY_NEED_PERSO_CALORIES;
      BMR = firstOne.BMR;
    }
    const PAL_calories = daily_need_calories - BMR;
    return (
      <>
        <Text style={[commonStyles.H4, styles.chartTitle]}>
          Métabolisme énergétique
        </Text>
        <View style={styles.chatArea}>
          <Text style={[commonStyles.btnText, styles.chartSubTitle]}>
            {'Besoin ÉNERGÉTIQUE journalier'.toUpperCase()}
          </Text>
          <ProgressCircle
            style={{height: 300}}
            progress={0.7}
            progressColor={Colors.theme.MAIN}
            backgroundColor={Colors.theme.DARK}
            strokeWidth={40}
            startAngle={-Math.PI * 0.8}
            endAngle={Math.PI * 0.8}
          />

          <View style={styles.guageInfo}>
            <Text style={styles.guageValue}>
              {parseInt(daily_need_calories, 10)}
            </Text>
            <Text style={commonStyles.labelSubTitle}>kcal</Text>
          </View>

          <View style={styles.chartLabelRow}>
            <View style={styles.ChartLabels}>
              <View
                style={[
                  styles.chartColorCircle,
                  {backgroundColor: Colors.theme.MAIN},
                ]}
              />
              <View>
                <Text style={styles.ChartLabelTitle}>Calories utilisées</Text>
                <Text style={commonStyles.labelSubTitle}>
                  Métabolisme de base
                </Text>
              </View>
            </View>
            <Text style={styles.ChartLabelValue}>{parseInt(BMR, 10)}</Text>
          </View>

          <View style={styles.chartLabelRow}>
            <View style={styles.ChartLabels}>
              <View
                style={[
                  styles.chartColorCircle,
                  {backgroundColor: Colors.theme.DARK},
                ]}
              />
              <View>
                <Text style={styles.ChartLabelTitle}>Calories brûlées</Text>
                <Text style={commonStyles.labelSubTitle}>
                  Activité physique
                </Text>
              </View>
            </View>
            <Text style={styles.ChartLabelValue}>
              {parseInt(PAL_calories, 10)}
            </Text>
          </View>

          {/* <View style={styles.chartLabelRow}>
            <View style={styles.chartColorCircle} />
            <View style={styles.ChartLabels}>
              <Text style={styles.ChartLabelTitle}></Text>
              <Text style={commonStyles.labelSubTitle}></Text>
            </View>
            <Text style={styles.ChartLabelValue}>828</Text>
          </View> */}
        </View>
      </>
    );
  }
}
