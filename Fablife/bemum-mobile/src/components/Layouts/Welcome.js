/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './Login/_styles';
import {Logo} from '../Widgets';
import {Colors} from '../../constants';

export default class WelcomeScreen extends React.Component {
  onDone = () => {
    AsyncStorage.setItem('WELCOME_DONE', 'YES');
    this.props.navigation.navigate('Login');
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Logo />
        <Text
          style={{color: Colors.theme.DARK, fontSize: 30, marginBottom: 30}}>
          Slideshow
        </Text>
        <TouchableOpacity
          style={styles.button}
          underlayColor="#fff"
          onPress={() => this.onDone()}>
          <Text style={styles.buttonText}>Se connecter</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
