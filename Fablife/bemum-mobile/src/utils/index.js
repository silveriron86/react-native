/* eslint-disable no-bitwise */
import {Platform} from 'react-native';
import analytics from '@react-native-firebase/analytics';
import AsyncStorage from '@react-native-community/async-storage';
import ApiConstants from '../constants/ApiConstants';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import * as Sentry from '@sentry/react-native';
import {createIntl, createIntlCache} from 'react-intl';
import Intl from 'intl';
import 'intl/locale-data/jsonp/fr';
import {SurveyConstants} from '../constants';

if (Platform.OS === 'android') {
  // See https://github.com/expo/expo/issues/6536 for this issue.
  if (typeof Intl.__disableRegExpRestore === 'function') {
    Intl.__disableRegExpRestore();
  }
}

const cache = createIntlCache();
const intl = createIntl(
  {
    // Locale of the application
    locale: 'fr',
    // Locale of the fallback defaultMessage
    defaultLocale: 'en',
  },
  cache,
);

const chars =
  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
// console.log(ApiConstants.getSentryDNS());
Sentry.init({
  environment: 'production',
  dsn: ApiConstants.getSentryDNS(),
  enableNative: process.env.JEST ? false : true,
});

let Utils = {
  btoa: (input = '') => {
    let str = input;
    let output = '';

    for (
      let block = 0, charCode, i = 0, map = chars;
      str.charAt(i | 0) || ((map = '='), i % 1);
      output += map.charAt(63 & (block >> (8 - (i % 1) * 8)))
    ) {
      charCode = str.charCodeAt((i += 3 / 4));

      if (charCode > 0xff) {
        throw new Error(
          "'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.",
        );
      }

      block = (block << 8) | charCode;
    }

    return output;
  },

  atob: (input = '') => {
    // eslint-disable-next-line no-div-regex
    let str = input.replace(/=+$/, '');
    let output = '';

    if (str.length % 4 === 1) {
      throw new Error(
        "'atob' failed: The string to be decoded is not correctly encoded.",
      );
    }
    for (
      let bc = 0, bs = 0, buffer, i = 0;
      (buffer = str.charAt(i++));
      ~buffer && ((bs = bc % 4 ? bs * 64 + buffer : buffer), bc++ % 4)
        ? (output += String.fromCharCode(255 & (bs >> ((-2 * bc) & 6))))
        : 0
    ) {
      buffer = chars.indexOf(buffer);
    }

    return output;
  },

  capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },

  getHeader(body) {
    return {
      'Content-Type': 'application/json',
      'Content-Length': body ? JSON.stringify(body).length : 0,
      Host: ApiConstants.getApiHost(),
      'X-Bemum-Client': 'customer-mobile-client',
    };
  },

  saveFirebaseToken(fcmToken) {
    AsyncStorage.getItem('ACCESS_TOKEN', (_err, token) => {
      AsyncStorage.getItem('USER_ID', (__err, user_id) => {
        fetch(`${ApiConstants.getApiPath()}api/v1/users/${user_id}/firebase`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            token: fcmToken,
          }),
        })
          .then(res => {
            console.log('Saved fcm token successfully');
            console.log(fcmToken);
            console.log(res);
            if (res.ok === true) {
              AsyncStorage.setItem('savedFirebaseToken', 'SAVED');
            }
          })
          .catch(error => {
            console.log('error');
          });
      });
    });
  },
  checkValueRange(min, max, value) {
    let errorMsg = '';
    if (value === '') {
      errorMsg = `La valeur doit être comprise entre ${min} et ${max}`;
    } else if (value < min) {
      errorMsg = `La valeur doit être supérieure à ${min}`;
    } else if (value > max) {
      errorMsg = `La valeur doit être inférieure à ${max}`;
    }

    return errorMsg;
  },
  getInsetTop: () => {
    if (typeof StaticSafeAreaInsets !== 'undefined') {
      //Jest TypeError: Cannot read property 'safeAreaInsetsTop' of undefined
      return Platform.OS === 'ios' ? StaticSafeAreaInsets.safeAreaInsetsTop : 0;
    }
    return 0;
  },
  getInsetBottom: () => {
    if (typeof StaticSafeAreaInsets !== 'undefined') {
      return Platform.OS === 'ios'
        ? StaticSafeAreaInsets.safeAreaInsetsBottom
        : 0;
    }
    return 0;
  },
  padNumber: (num, size) => {
    var s = num + '';
    while (s.length < size) {
      s = '0' + s;
    }
    return s;
  },
  _getQuestionValue: (questions, questionId) => {
    for (let i = 0; i < questions.length; i++) {
      let item = questions[i];
      if (item.question_id === questionId) {
        return item.value;
      }
    }
    return null;
  },

  findParentValue: (
    categories,
    type,
    form_id,
    props,
    condition,
    is_followup,
  ) => {
    let found = null;
    let value = null;
    categories.forEach((category, index) => {
      if (!found) {
        if ((!is_followup && category.form_id !== form_id) || is_followup) {
          const form = props[`${type}_${category.form_id}FormData`];
          if (typeof form !== 'undefined') {
            const formData = form.data;
            if (formData) {
              found = Object.entries(formData).find(
                ([k, v]) =>
                  formData[k].question_id === condition.parentQuestionId,
              );
              if (found) {
                value = found[1].value;
              }
            }
          }
        }
      }
    });
    return value;
  },

  checkVisibleQuestion: (data, questions, question, props, type, form_id) => {
    if (
      typeof question.conditions === 'undefined' ||
      question.conditions.length === 0
    ) {
      return true;
    }

    let visible = true;
    for (let i = 0; i < question.conditions.length; i++) {
      let condition = question.conditions[i];
      let value = Utils._getQuestionValue(
        questions,
        condition.parentQuestionId,
      );
      if (value === null) {
        if (data) {
          if (typeof data[condition.parentQuestionId] !== 'undefined') {
            value = data[condition.parentQuestionId];
          } else {
            // when the question is in other forms, for example, ALCOHOL_RELAXED
            value = Utils.findParentValue(
              type === 'signup'
                ? SurveyConstants.CATEGORIES
                : SurveyConstants.FOLLOWUP_CATEGORIES,
              type,
              form_id,
              props,
              condition,
              false,
            );
            if (type === 'followup' && value === null) {
              // if parent question is not found for followup, it will use signup questions
              value = Utils.findParentValue(
                SurveyConstants.CATEGORIES,
                'signup',
                form_id,
                props,
                condition,
                true,
              );
            }
            // let found = null;
            // categories.forEach((category, index) => {
            //   if (!found) {
            //     if (category.form_id !== form_id) {
            //       const formData =
            //         props[`${type}_${category.form_id}FormData`].data;
            //       if (formData) {
            //         found = Object.entries(formData).find(
            //           ([k, v]) =>
            //             formData[k].question_id === condition.parentQuestionId,
            //         );
            //         if (found) {
            //           value = found[1].value;
            //         }
            //       }
            //     }
            //   }
            // });
            console.log('found = ', value);
          }
        }
      }

      if (condition.operator === '$eq') {
        visible = value === condition.value;
      } else if (
        condition.operator === '$neq' ||
        condition.operator === '$ne'
      ) {
        visible = value !== condition.value;
      } else if (condition.operator === '$gt') {
        visible = value > condition.value;
      } else if (condition.operator === '$gte') {
        visible = value >= condition.value;
      } else if (condition.operator === '$lt') {
        visible = value < condition.value;
      } else if (condition.operator === '$lte') {
        visible = value <= condition.value;
      }

      if (question.conditions_assoc === 'OR') {
        if (visible === true) {
          return true;
        }
      } else {
        if (visible === false) {
          return false;
        }
      }
    }

    return visible;
  },

  formatNumber: value => {
    return intl.formatNumber(value, {
      minimumFractionDigits: 1,
      maximumFractionDigits: 1,
    });
  },

  logEvent: data => {
    AsyncStorage.getItem('USER_ID', (_err, patientId) => {
      Utils._logEvent(patientId, data);
    });
  },

  _logEvent: async (patientId, data) => {
    const {screenName, className} = data;
    // await analytics().setCurrentScreen(screenName);
    await analytics()
      .logEvent(`screen_${screenName}`, {
        id: patientId,
        name: screenName,
        class: className,
      })
      .then(res => {
        // console.log('res = ', res);
      });
  },
};

export default Utils;
