module.exports = {
  MAIN_BACKGROUND: '#292A3E', // main background
  PRIMARY: '#5B45F3', // primary button
  BLUE: '#357AD3',
  SKY: '#65D8FB',
  GREEN: '#33A594', // green button
  LIGHT_GREEN: '#31E8A4',
  RED: '#C53C53', // red button
  LIGHT_RED: '#FF6C64',
  LINEN: '#FCEBE8',
  DARK_LINEN: '#FECECD',
  CHART: '#FDF6F5',
  LIGHT_GREY: '#E7E7E7', // selected color
  LIGHT_YELLOW: '#FFFEF5',
  YELLOW: '#FFD730',
  GREY: '#878890',
  DARK_GREY: '#515151',
  theme: {
    DARK: '#B3A3FF',
    MAIN: '#E2DAFF',
    LIGHT: '#F6F4FF',
    BLACK: '#262626',
  },
};
