module.exports = [
  {
    question_id: 'SPECIFIC_DIET',
    required: true,
    label_question: 'Suivez-vous un régime alimentaire spécifique ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Non',
          value: 'false',
        },
        {
          label: 'Végétarien',
          value: 'vegetarian',
        },
        {
          label: 'Pesco-végétarien',
          value: 'pesco-vegetarian',
        },
        {
          label: 'Sans porc',
          value: 'without-pork',
        },
      ],
    },
  },
  {
    question_id: 'FOOD_EXCLUSION',
    required: false,
    label_question:
      'Au-delà de tout régime alimentaire spécifique, y a-t-il des aliments auxquels vous êtes allergique ou que vous ne consommez pas ?',
    ui: 'autocomplete',
    ui_config: {
      path: '',
    },
    value: [],
  },
  {
    question_id: 'BREAKFAST',
    required: true,
    label_question: 'Avez-vous l’habitude de prendre un petit-déjeuner ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Uniquement en semaine',
          value: 'week',
        },
        {
          label: 'Uniquement le week-end',
          value: 'weekend',
        },
        {
          label: 'En semaine et le week-end',
          value: 'always',
        },
        {
          label: 'Jamais',
          value: 'no',
        },
      ],
    },
  },
  {
    question_id: 'BREAKFAST_WEEK_CHANGE',
    ui: 'switch',
    label_question: 'Seriez-vous prête à changer cette habitude ?',
    ui_config: {
      choices: [
        {
          label: 'Oui',
          value: true,
        },
        {
          label: 'Non',
          value: false,
        },
      ],
    },
    conditions: [
      {
        parentQuestionId: 'BREAKFAST',
        operator: '$eq',
        value: 'no',
      },
      {
        parentQuestionId: 'BREAKFAST',
        operator: '$eq',
        value: 'weekend',
      },
    ],
    conditions_assoc: 'OR',
  },
  {
    question_id: 'MORNING_SNACK',
    required: true,
    label_question: 'Prenez-vous une collation le matin ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Uniquement en semaine',
          value: 'week',
        },
        {
          label: 'Uniquement le week-end',
          value: 'weekend',
        },
        {
          label: 'En semaine et le week-end',
          value: 'always',
        },
        {
          label: 'Jamais',
          value: 'no',
        },
      ],
    },
  },
  {
    question_id: 'LUNCH_WEEK_COMPOSITION',
    required: true,
    label_question:
      'Quelle est habituellement la composition de votre déjeuner en semaine ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Plat uniquement',
          value: 'main-course',
        },
        {
          label: 'Entrée Plat',
          value: 'starter',
        },
        {
          label: 'Plat Dessert',
          value: 'dessert',
        },
        {
          label: 'Entrée Plat Dessert',
          value: 'all',
        },
      ],
    },
  },
  {
    question_id: 'LUNCH_WEEKEND_COMPOSITION',
    required: true,
    label_question:
      'Quelle est habituellement la composition de votre déjeuner le week-end ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Plat uniquement',
          value: 'main-course',
        },
        {
          label: 'Entrée Plat',
          value: 'starter',
        },
        {
          label: 'Plat Dessert',
          value: 'dessert',
        },
        {
          label: 'Entrée Plat Dessert',
          value: 'all',
        },
      ],
    },
  },
  {
    question_id: 'AFTERNOON_SNACK',
    required: true,
    label_question: 'Prenez-vous une collation l’après-midi ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Uniquement en semaine',
          value: 'week',
        },
        {
          label: 'Uniquement le week-end',
          value: 'weekend',
        },
        {
          label: 'En semaine et le week-end',
          value: 'always',
        },
        {
          label: 'Jamais',
          value: 'no',
        },
      ],
    },
  },
  {
    question_id: 'DINER_WEEK_COMPOSITION',
    required: true,
    label_question:
      'Quelle est habituellement la composition de votre dîner en semaine ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Plat uniquement',
          value: 'main-course',
        },
        {
          label: 'Entrée Plat',
          value: 'starter',
        },
        {
          label: 'Plat Dessert',
          value: 'dessert',
        },
        {
          label: 'Entrée Plat Dessert',
          value: 'all',
        },
      ],
    },
  },
  {
    question_id: 'DINER_WEEKEND_COMPOSITION',
    required: true,
    label_question:
      'Quelle est habituellement la composition de votre dîner le week-end ?',
    ui: 'radio',
    ui_config: {
      choices: [
        {
          label: 'Plat uniquement',
          value: 'main-course',
        },
        {
          label: 'Entrée Plat',
          value: 'starter',
        },
        {
          label: 'Plat Dessert',
          value: 'dessert',
        },
        {
          label: 'Entrée Plat Dessert',
          value: 'all',
        },
      ],
    },
  },
  {
    question_id: 'MEAL_PREPARATION_TIME_WEEK',
    ui: 'wheel',
    label_question:
      'Combien de temps par jour vous ou votre conjoint·e êtes-vous prêt·e·s à consacrer à la préparation de vos repas la semaine ?',
    ui_config: {
      min: 15,
      max: 240,
      precision: 5,
    },
    unit: {
      short_name: 'min',
    },
  },
  {
    question_id: 'MEAL_PREPARATION_TIME_WEEKEND',
    ui: 'wheel',
    label_question:
      'Combien de temps par jour vous ou votre conjoint·e êtes-vous prêt·e·s à consacrer à la préparation de vos repas le week-end ?',
    ui_config: {
      min: 15,
      max: 240,
      precision: 5,
    },
    unit: {
      short_name: 'min',
    },
  },
];
