// import axios from 'axios';
import 'text-encoding';
import 'web-streams-polyfill';
import {fetch} from 'react-native-fetch-api';
import AccountConstants from '../constants/AccountConstants';
import ApiConstants from '../constants/ApiConstants';
import Utils from '../utils';

let AccountActions = {
  loginError: function(error) {
    return {
      error,
      type: AccountConstants.LOGIN_ERROR,
    };
  },

  loginSuccess: function(response) {
    return {
      response,
      type: AccountConstants.LOGIN_SUCCESS,
    };
  },

  login: function(args, cb) {
    const {email, password} = args;
    const data = JSON.stringify({
      email,
      password,
    });
    return dispatch => {
      return fetch(`${ApiConstants.getApiPath()}authentication/patient/jwt`, {
        method: 'POST',
        headers: Utils.getHeader(data),
        body: data,
      })
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb({
                  success: true,
                  data: response,
                });
              }
              dispatch(this.loginSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb({success: false, _error});
            }
            dispatch(this.loginError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb({success: false, error});
          }
          dispatch(this.loginError(error));
        });
    };
  },

  forgotError: function(error) {
    return {
      error,
      type: AccountConstants.FORGOT_ERROR,
    };
  },

  forgotSuccess: function(response) {
    return {
      response,
      type: AccountConstants.FORGOT_SUCCESS,
    };
  },

  forgot: function(email, cb) {
    let data = JSON.stringify({
      email,
    });
    return dispatch => {
      return fetch(
        `${ApiConstants.getApiPath()}authentication/patient/forgot-password`,
        {
          method: 'POST',
          headers: Utils.getHeader(data),
          body: data,
        },
      )
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb({success: true, response});
              }
              dispatch(this.forgotSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb({
                success: false,
                _error,
              });
            }
            dispatch(this.forgotError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb({success: false, error});
          }
          dispatch(this.forgotError(error));
        });
    };
  },

  updateError: function(error) {
    return {
      error,
      type: AccountConstants.UPDATE_ERROR,
    };
  },

  updateSuccess: function(response) {
    return {
      response,
      type: AccountConstants.UPDATE_SUCCESS,
    };
  },

  update: function(id, data, cb) {
    return dispatch => {
      return fetch(`${ApiConstants.getApiPath()}patients/${id}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
        body: JSON.stringify(data),
      })
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb(response);
              }
              dispatch(this.updateSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb(_error);
            }
            dispatch(this.updateError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb(error);
          }
          dispatch(this.updateError(error));
        });
    };
  },

  getError: function(error) {
    return {
      error,
      type: AccountConstants.GET_ERROR,
    };
  },
  getSuccess: function(response) {
    return {
      response,
      type: AccountConstants.GET_SUCCESS,
    };
  },
  get: function(cb) {
    return dispatch => {
      return fetch(`${ApiConstants.getApiPath()}patients/${global.userID}`, {
        method: 'GET',
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb(response);
              }
              dispatch(this.getSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb(_error);
            }
            dispatch(this.getError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb(error);
          }
          dispatch(this.getError(error));
        });
    };
  },

  getMetricsError: function(error) {
    return {
      error,
      type: AccountConstants.GET_METRICS_ERROR,
    };
  },

  getMetricsSuccess: function(response) {
    return {
      response,
      type: AccountConstants.GET_METRICS_SUCCESS,
    };
  },

  getMetrics: function(cb) {
    return dispatch => {
      return fetch(
        `${ApiConstants.getApiPath()}patients/${
          global.userID
        }/metrics?limit=10&order_by_date=ASC&include=WEIGHT,BMI,PAL,HIPS,WAIST`,
        {
          method: 'GET',
          headers: {
            accept: 'application/json',
            Authorization: `Bearer ${global.accessToken}`,
          },
        },
      )
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb(response);
              }
              dispatch(this.getMetricsSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb(_error);
            }
            dispatch(this.getMetricsError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb(error);
          }
          dispatch(this.getMetricsError(error));
        });
    };
  },

  getNutrinomesError: function(error) {
    return {
      error,
      type: AccountConstants.GET_NUTRINOMES_ERROR,
    };
  },

  getNutrinomesSuccess: function(response) {
    return {
      response,
      type: AccountConstants.GET_NUTRINOMES_SUCCESS,
    };
  },

  getNutrinomes: function(cb) {
    return dispatch => {
      return fetch(
        `${ApiConstants.getApiPath()}patients/${global.userID}/nutrinomes`,
        {
          method: 'GET',
          headers: {
            accept: 'application/json',
            Authorization: `Bearer ${global.accessToken}`,
          },
        },
      )
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb(response);
              }
              dispatch(this.getNutrinomesSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb(_error);
            }
            dispatch(this.getNutrinomesError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb(error);
          }
          dispatch(this.getNutrinomesError(error));
        });
    };
  },

  updateIngredientsToAvoidError: function(error) {
    return {
      error,
      type: AccountConstants.UPDATE_INGREDIENTS_TO_AVOID_ERROR,
    };
  },

  updateIngredientsToAvoidSuccess: function(response) {
    return {
      response,
      type: AccountConstants.UPDATE_INGREDIENTS_TO_AVOID_SUCCESS,
    };
  },

  updateIngredientsToAvoid: function(data, cb) {
    return dispatch => {
      return fetch(
        `${ApiConstants.getApiPath()}patients/${
          global.userID
        }/ingredients-to-avoid`,
        {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            accept: 'application/json',
            Authorization: `Bearer ${global.accessToken}`,
          },
          body: JSON.stringify(data),
        },
      )
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb(response);
              }
              dispatch(this.updateIngredientsToAvoidSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb(_error);
            }
            dispatch(this.updateIngredientsToAvoidError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb(error);
          }
          dispatch(this.updateIngredientsToAvoidError(error));
        });
    };
  },
};

export default AccountActions;
