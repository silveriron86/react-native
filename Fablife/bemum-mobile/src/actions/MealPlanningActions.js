// import axios from 'axios';
import 'text-encoding';
import 'web-streams-polyfill';
import {fetch} from 'react-native-fetch-api';
import ApiConstants from '../constants/ApiConstants';
import {MealPlanningConstants} from '../constants';

const MealPlanningActions = {
  getError: function(error) {
    return {
      error,
      type: MealPlanningConstants.GET_MEAL_PLANNING_ERROR,
    };
  },

  getSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.GET_MEAL_PLANNING_SUCCESS,
    };
  },

  get: function(date, cb) {
    return dispatch => {
      return fetch(
        `${ApiConstants.getApiPath()}meal-planner?date=${date}&patientId=${
          global.userID
        }`,
        {
          method: 'GET',
          headers: {
            accept: 'application/json',
            Authorization: `Bearer ${global.accessToken}`,
          },
        },
      )
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb({success: true, data: response});
              }
              dispatch(this.getSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb({success: false, _error});
            }
            dispatch(this.getError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb({success: false, error});
          }
          dispatch(this.getError(error));
        });
    };
  },

  updateError: function(error) {
    return {
      error,
      type: MealPlanningConstants.UPDATE_MEAL_PLANNING_ERROR,
    };
  },

  updateSuccess: function(response) {
    return {
      response,
      type: MealPlanningConstants.UPDATE_MEAL_PLANNING_SUCCESS,
    };
  },

  update: function(mealPlanningId, data, cb) {
    return dispatch => {
      return fetch(
        `${ApiConstants.getApiPath()}meal-planner/${mealPlanningId}`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
            accept: 'application/json',
            Authorization: `Bearer ${global.accessToken}`,
          },
          body: JSON.stringify(data),
        },
      )
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb({success: true, data: response});
              }
              dispatch(this.updateSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb({success: false, _error});
            }
            dispatch(this.updateError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb({success: false, error});
          }
          dispatch(this.updateError(error));
        });
    };
  },
};

export default MealPlanningActions;
