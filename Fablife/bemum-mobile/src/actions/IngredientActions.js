// import axios from 'axios';
import 'text-encoding';
import 'web-streams-polyfill';
import {fetch} from 'react-native-fetch-api';
import ApiConstants from '../constants/ApiConstants';
import {IngredientConstants} from '../constants';

const IngredientActions = {
  getIngredientsError: function(error) {
    return {
      error,
      type: IngredientConstants.GET_INGREDIENTS_ERROR,
    };
  },

  getIngredientsSuccess: function(response) {
    return {
      response,
      type: IngredientConstants.GET_INGREDIENTS_SUCCESS,
    };
  },

  getIngredients: function(cb) {
    return dispatch => {
      return fetch(`${ApiConstants.getApiPath()}ingredients`, {
        method: 'GET',
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${global.accessToken}`,
        },
      })
        .then(res => {
          if (res.ok) {
            res.json().then(response => {
              if (cb) {
                cb(response);
              }
              dispatch(this.getIngredientsSuccess(response));
            });
          } else {
            let _error = {
              status: res.status,
              error: res.statusText,
            };
            if (cb) {
              cb(_error);
            }
            dispatch(this.getIngredientsError(_error));
          }
        })
        .catch(error => {
          if (cb) {
            cb(error);
          }
          dispatch(this.getIngredientsError(error));
        });
    };
  },
};

export default IngredientActions;
