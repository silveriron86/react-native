import createMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import IngredientActions from './IngredientActions';
import {IngredientConstants} from '../constants';

const mws = [thunk];
const mockStore = createMockStore(mws);
const store = mockStore({});
const mock = new MockAdapter(axios);

describe('GET Ingredients Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const url = 'getIngredients';
  const response = {response: {response: {item: 'item1'}}};
  const expectedSuccessActions = {
    type: IngredientConstants.GET_INGREDIENTS_SUCCESS,
    response,
  };
  const expectedErrorActions = {
    type: IngredientConstants.GET_INGREDIENTS_ERROR,
    error: {},
  };

  it('Get Ingredients Error', () => {
    expect(IngredientActions.getIngredientsError({})).toEqual(
      expectedErrorActions,
    );
  });

  it('Get Ingredients Success', () => {
    expect(IngredientActions.getIngredientsSuccess(response)).toEqual(
      expectedSuccessActions,
    );
  });

  it('Dispatches GET_INGREDIENTS_SUCCESS after a successful API requests', () => {
    mock.onGet(url).reply(201, {response: {item: 'item1'}});
    store.dispatch(IngredientActions.getIngredients()).then(() => {
      expect(store.getActions()).toContainEqual(expectedSuccessActions);
    });
  });

  it('dispatches GET_INGREDIENTS_ERROR after a FAILED API requests', () => {
    mock.onGet(url).reply(400, {error: {message: 'error message'}});

    store.dispatch(IngredientActions.getIngredients()).then(() => {
      expect(store.getActions()).toContainEqual(expectedErrorActions);
    });
  });
});
