import {ShoppingConstants} from '../constants';

const initialState = {
  shoppings: [],
  error: null,
};

function ShoppingReducer(state = initialState, action) {
  switch (action.type) {
    case ShoppingConstants.GET_SHOPPING_LIST_SUCCESS:
      return Object.assign({}, state, {
        shoppings: action.response,
        error: null,
      });

    case ShoppingConstants.GET_SHOPPING_LIST_ERROR:
      return Object.assign({}, state, {
        shoppings: [],
        error: action.error,
      });

    default:
      return state;
  }
}

export default ShoppingReducer;
