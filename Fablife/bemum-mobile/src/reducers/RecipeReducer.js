import {RecipeConstants} from '../constants';

const initialState = {
  recipe: null,
  error: null,
};

function RecipeReducer(state = initialState, action) {
  switch (action.type) {
    case RecipeConstants.GET_RECIPE_SUCCESS:
      return Object.assign({}, state, {
        recipe: action.response,
        error: null,
      });

    case RecipeConstants.GET_RECIPE_ERROR:
      return Object.assign({}, state, {
        recipe: null,
        error: action.error,
      });

    default:
      return state;
  }
}

export default RecipeReducer;
