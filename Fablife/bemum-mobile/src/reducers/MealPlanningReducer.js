import {MealPlanningConstants} from '../constants';

const initialState = {
  mealPlanning: null,
  error: null,
};

function MealPlanningReducer(state = initialState, action) {
  switch (action.type) {
    case MealPlanningConstants.GET_MEAL_PLANNING_SUCCESS:
      return Object.assign({}, state, {
        mealPlanning: action.response,
        error: null,
      });

    case MealPlanningConstants.GET_MEAL_PLANNING_ERROR:
      return Object.assign({}, state, {
        mealPlanning: null,
        error: action.error,
      });

    default:
      return state;
  }
}

export default MealPlanningReducer;
