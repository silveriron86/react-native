module.exports = {
  dependencies: {
    'react-native-vector-icons': {
      platforms: {
        android: null,
        ios: null,
      },
    },
    'react-native-linear-gradient': {
      platforms: {
        android: null,
        ios: null,
      },
    },
    'react-native-push-notification': {
      platforms: {
        android: null,
      },
    },
    'react-native-splash-screen': {
      platforms: {
        android: null,
        ios: null,
      },
    },
    'react-native-date-picker': {
      platforms: {
        ios: null,
      },
    },
  },
  assets: ['./assets/fonts/'],
};
