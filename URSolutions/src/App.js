// import React from 'react';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
// import {createStackNavigator} from 'react-navigation-stack';
import Routes from './navigation/Routes';
const AppNavigator = createSwitchNavigator(Routes, {
  initialRouteName: 'InitialScreen',
  // initialRouteName: 'MainScreen',
});
export default createAppContainer(AppNavigator);
