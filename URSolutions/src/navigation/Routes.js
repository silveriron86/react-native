/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import InitialScreen from '../screens/InitialScreen';
import SlidesScreen from '../screens/SlidesScreen';
import IWillScreen from '../screens/MainScreen/IWillScreen';
import VideoScreen from '../screens/MainScreen/VideoScreen';
import ChallengeScreen from '../screens/MainScreen/ChallengeScreen';

const MainScreen = createBottomTabNavigator(
  {
    Iwill: {
      screen: IWillScreen,
      navigationOptions: {
        tabBarLabel: 'I Wills',
        tabBarIcon: ({focused}) =>
          focused ? (
            <Text style={[styles.tabIcon, styles.selected]}>&#xf3f0;</Text>
          ) : (
            <Text style={styles.tabIcon}>&#xf3ef;</Text>
          ),
      },
    },
    Videos: {
      screen: VideoScreen,
      navigationOptions: {
        tabBarLabel: 'Videos',
        tabBarIcon: ({focused}) =>
          focused ? (
            <Text style={[styles.tabIcon, styles.selected]}>&#xf24d;</Text>
          ) : (
            <Text style={styles.tabIcon}>&#xf24c;</Text>
          ),
      },
    },
    Challenge: {
      screen: ChallengeScreen,
      navigationOptions: {
        tabBarLabel: 'Challenge',
        tabBarIcon: ({focused}) =>
          focused ? (
            <Text style={[styles.tabIcon, styles.selected]}>&#xf4cd;</Text>
          ) : (
            <Text style={styles.tabIcon}>&#xf4cc;</Text>
          ),
      },
    },
  },
  {
    lazy: false,
    initialRouteName: 'Iwill',
    // tabBarComponent: TabBar,
    tabBarOptions: {
      inactiveTintColor: '#FFFFFF',
      activeTintColor: '#356f24',
      style: {
        backgroundColor: '#6ed961',
        // height: 48,
        // borderTopWidth: 0.001,
        // borderTopColor: '#CACACA',
      },
      tabStyle: {
        // borderRightWidth: 0,
        // borderRightColor: '#BDBDBD',
      },
    },
  },
);

const Routes = {
  InitialScreen,
  SlidesScreen,
  MainScreen,
};

export default Routes;

const styles = StyleSheet.create({
  tabIcon: {
    fontFamily: 'Ionicons',
    color: 'white',
    fontSize: 26,
  },
  selected: {
    color: '#356f24',
  },
});
