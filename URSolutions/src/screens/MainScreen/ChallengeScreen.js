/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
// import VideoRecorder from 'react-native-beautiful-video-recorder';
import VideoRecorder from './VideoRecorder';
import Share from 'react-native-share';

class ChallengeScreen extends React.Component {
  start = () => {
    // 600 seconds : 10 mins
    this.videoRecorder.open({maxLength: 600}, data => {
      console.log('captured data', data);
      setTimeout(() => {
        Share.open({
          title: 'record.mov',
          type: 'video/quicktime',
          url: data.uri,
        });
      }, 10);
    });
  };

  render() {
    return (
      <LinearGradient colors={['#6FDB62', '#55a74a']} style={{flex: 1}}>
        <SafeAreaView style={styles.container}>
          <View style={{padding: 10, flex: 1}}>
            <Text style={styles.topline}>Record a video</Text>
            <Text style={[styles.topline, {fontWeight: 'bold'}]}>
              to challenge your friends
            </Text>
            <TouchableOpacity onPress={this.start} style={styles.challengeBtn}>
              <Text style={styles.challengeText}>
                Tap here to record your challenge!
              </Text>
            </TouchableOpacity>
          </View>
          <VideoRecorder
            ref={ref => {
              this.videoRecorder = ref;
            }}
          />
        </SafeAreaView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topline: {
    fontSize: 16,
    textTransform: 'uppercase',
    textAlign: 'center',
    color: 'white',
  },
  challengeBtn: {
    padding: 60,
    backgroundColor: '#3f842a',
    height: '50%',
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  challengeText: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
  },
});

export default ChallengeScreen;
