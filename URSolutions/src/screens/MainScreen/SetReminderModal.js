/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Alert,
  Platform,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Switch} from 'react-native-switch';
import DateTimePicker from '@react-native-community/datetimepicker';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

class SetReminderModal extends React.Component {
  state = {
    isReminderSet: false,
    reminderTime: '',
    date: new Date(new Date().getTime() + 60 * 1000),
  };

  componentDidMount() {
    AsyncStorage.getItem('ALLOW_NOTIFICATION', (_err, allowed) => {
      if (allowed) {
        this.setState({
          isReminderSet: true,
        });
      }
    });
    AsyncStorage.getItem('REMINDER_TIME', (_err, time) => {
      if (time) {
        let dt = moment(`${moment().format('YYYY-MM-DD')}T${time}`);
        this.setState({
          reminderTime: dt.format('h:mm A'),
          date: dt.toDate(),
        });
      }
    });
  }

  checkedChange = v => {
    if (v === false) {
      AsyncStorage.removeItem('ALLOW_NOTIFICATION');
      AsyncStorage.removeItem('REMINDER_TIME');
      this.props.deleteNotificationScheduler();
    } else {
      AsyncStorage.setItem('ALLOW_NOTIFICATION', 'true');
    }
    this.setState({
      isReminderSet: v,
    });
  };

  setReminder = v => {
    AsyncStorage.setItem(
      'REMINDER_TIME',
      moment(this.state.date).format('HH:mm:00.sssZ'),
    );
    const time = moment(this.state.date).format('h:mm A');
    this.props.createNotificationScheduler();
    this.setState(
      {
        reminderTime: time,
      },
      () => {
        Alert.alert(
          'Alert',
          `Reminder set for ${time}!`,
          [{text: 'OK', onPress: () => {}}],
          {cancelable: true},
        );
      },
    );
  };

  setDate = (event, date) => {
    this.changeTime(date);
  };

  changeTime = date => {
    date = date || this.state.date;
    this.setState({date});
  };

  render() {
    const {isReminderSet, reminderTime, date} = this.state;
    return (
      <LinearGradient colors={['#6FDB62', '#55a74a']} style={{flex: 1}}>
        <SafeAreaView style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.headerText}>I WILL REMINDERS</Text>
            <TouchableOpacity
              style={styles.closeModalButton}
              onPress={this.props.onClose}>
              <Text style={styles.closeModalText}>&#xf2d7;</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.notificationSettings}>
            <Text style={{color: 'white'}}>Allow Notifications</Text>
            <View
              style={{
                borderWidth: 1,
                borderColor: 'white',
                borderRadius: 15,
                width: 62,
              }}>
              <Switch
                value={isReminderSet}
                backgroundActive="#55a74a"
                backgroundInactive="transparent"
                onValueChange={this.checkedChange}
                outerCircleStyle={{borderColor: 'red'}}
                innerCircleStyle={{borderColor: 'transparent'}}
              />
            </View>
          </View>
          {isReminderSet && (
            <View style={styles.timePicker}>
              {reminderTime !== '' && (
                <Text style={{color: 'white'}}>
                  Current reminder set for : {reminderTime}
                </Text>
              )}
              {Platform.OS === 'ios' ? (
                <DateTimePicker
                  value={date}
                  mode="time"
                  is24Hour={true}
                  display="default"
                  onChange={this.setDate}
                />
              ) : (
                <View
                  style={{
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 15,
                  }}>
                  <DatePicker
                    // fadeToColor="none"
                    date={date}
                    mode="time"
                    onDateChange={this.changeTime}
                  />
                </View>
              )}
              <TouchableOpacity style={styles.btn} onPress={this.setReminder}>
                <Text style={styles.btnText}>Set Reminder</Text>
              </TouchableOpacity>
            </View>
          )}
        </SafeAreaView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#6FDB62',
    paddingVertical: 10,
    borderBottomColor: '#55a74a',
    borderBottomWidth: 1,
  },
  headerText: {
    textAlign: 'center',
    width: '90%',
    fontSize: 20,
    color: 'white',
  },
  btn: {
    backgroundColor: '#3c8b32',
    borderRadius: 8,
    padding: 8,
    marginTop: 20,
  },
  btnText: {
    textTransform: 'capitalize',
    fontFamily: 'Lato',
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
  },
  timePicker: {
    margin: 20,
  },
  notificationSettings: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop: 20,
  },
  closeModalButton: {
    position: 'absolute',
    right: 5,
    top: 10,
    width: 30,
    height: 30,
  },
  closeModalText: {
    fontFamily: 'Ionicons',
    textAlign: 'center',
    fontSize: 26,
    color: 'white',
  },
});

export default SetReminderModal;
