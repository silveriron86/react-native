/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  ScrollView,
  Dimensions,
  // Alert,
} from 'react-native';
import Share from 'react-native-share';
import LinearGradient from 'react-native-linear-gradient';
import ModalWrapper from 'react-native-modal-wrapper';
import moment from 'moment';
import SetReminderModal from './SetReminderModal';
import {openDatabase} from 'react-native-sqlite-storage';
import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import AsyncStorage from '@react-native-community/async-storage';

const {height} = Dimensions.get('window');
const isSmall = height < 600;
const isTiny = height < 530;

class IWillScreen extends React.Component {
  constructor(props) {
    super(props);
    this.db = openDatabase({name: 'iwillDB', createFromLocation: 1}, () => {
      this.db.transaction(tx => {
        this.updateTodayList(tx);
      });
    });
    this.todayDate = new Date();
    this.dateFormat = 'MM/DD/YYYY';
    this.datesEntered = [];
    this.state = {
      selectedDate: new Date(),
      selectedWills: [],
      editState: false,
      addState: false,
      willModel: '',
      datePosition: 0,
      isModalVisible: false,
    };
  }

  componentDidMount() {
    PushNotification.configure({
      onRegister: function(token) {
        // console.log("TOKEN:", token);
      },
      // (required) Called when a remote or local notification is opened or received
      onNotification: notification => {
        this._onLocalNotification(notification);
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      permissions: {
        alert: true,
        // badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
    this.createNotificationScheduler();
  }

  createNotificationScheduler = () => {
    AsyncStorage.getItem('REMINDER_TIME', (_err, time) => {
      if (time) {
        const fireDate = `${moment().format('YYYY-MM-DD')}T${time}`;
        if (moment().unix() < moment(fireDate).unix()) {
          const notification = {
            fireDate: fireDate,
            alertTitle: 'Your IWills',
            alertBody: 'Be sure to check your I Wills!',
            repeatInterval: 'day', // day
          };
          if (Platform.OS === 'ios') {
            PushNotificationIOS.cancelAllLocalNotifications();
            PushNotificationIOS.scheduleLocalNotification(notification);
          } else {
            PushNotification.cancelAllLocalNotifications();
            PushNotification.localNotificationSchedule({
              title: 'Your IWills',
              message: 'Be sure to check your I Wills!',
              date: moment(fireDate).toDate(),
              repeatType: 'day', // day
            });
          }
        }
      }
    });
  };

  _getWillsText = selectedWills => {
    let iWills = '';
    selectedWills.forEach((will, index) => {
      if (index > 0) {
        iWills += ', ';
      }
      iWills += will.text;
    });
    return iWills;
  };

  _onLocalNotification(notification) {
    console.log(notification);
    // Alert.alert('Alert', 'I will: ' + notification.message, [
    //   {
    //     text: 'Dismiss',
    //     onPress: null,
    //   },
    // ]);
  }

  toggleAddEntry = () => {
    this.setState({
      willModel: '',
      addState: !this.state.addState,
    });
  };

  updateTodayList = tx => {
    tx.executeSql('SELECT * FROM iwills', [], (_tx, res) => {
      const rows = res.rows;
      let iWills = [];
      for (let i = 0; i < rows.length; i++) {
        iWills.push({
          ...rows.item(i),
        });
      }

      iWills.sort((a, b) => {
        moment(a.date, 'MM/DD/YYYY').format('x') -
          moment(b.date, 'MM/DD/YYYY').format('x');
      });

      // this.setState({selectedWills: iWills});
      let dateSet = new Set();
      iWills.forEach(w => dateSet.add(w.date));
      dateSet.add(moment().format(this.dateFormat));
      this.datesEntered = Array.from(dateSet);
      // console.log(this.datesEntered);
      this.setState({
        datePosition: this.datesEntered.length - 1,
      });
      this.getWillsByDate();
    });
  };

  changeDate = dir => {
    const {datePosition} = this.state;
    let dateIndex = datePosition + dir;
    dateIndex = dateIndex >= 0 ? dateIndex : 0;
    this.setState({
      editState: false,
      addState: false,
      datePosition: dateIndex,
    });
    this.setState(
      {
        selectedDate: this.datesEntered[dateIndex],
      },
      () => {
        this.getWillsByDate();
      },
    );
  };

  getWillsByDate = () => {
    const {selectedDate} = this.state;
    this.db = openDatabase({name: 'iwillDB', createFromLocation: 1}, () => {
      this.db.transaction(tx => {
        tx.executeSql(
          'SELECT * FROM iwills WHERE date = ?',
          [moment(selectedDate).format(this.dateFormat)],
          (_tx, res) => {
            const rows = res.rows;
            let iWills = [];
            for (let i = 0; i < rows.length; i++) {
              iWills.push({
                ...rows.item(i),
              });
            }
            this.setState({
              selectedWills: iWills,
            });
            if (iWills.length === 0 && this.state.editState === true) {
              this.setState({
                editState: false,
              });
            }
          },
        );
      });
    });
  };

  saveEntry = () => {
    this.db = openDatabase({name: 'iwillDB', createFromLocation: 1}, () => {
      this.db.transaction(tx => {
        tx.executeSql(
          'INSERT INTO iwills (date, text, completed) VALUES (?,?,?)',
          [moment().format(this.dateFormat), this.state.willModel, 0],
          (_tx, res) => {
            this.setState({
              willModel: '',
              addState: false,
            });
            this.getWillsByDate();
            this.createNotificationScheduler();
          },
          error => {
            console.log(error);
          },
        );
      });
    });
  };

  shareEntries = () => {
    const iWills = this._getWillsText(this.state.selectedWills);
    console.log('I will: ' + iWills);
    Share.open({
      title: 'URSolutions',
      message: 'I will: ' + iWills,
    });
  };

  toggleRemindersModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    });
  };

  isToday = () => {
    const {selectedDate} = this.state;
    return (
      moment(selectedDate, this.dateFormat).format(this.dateFormat) ===
      moment(this.todayDate, this.dateFormat).format(this.dateFormat)
    );
  };

  handleChange = text => {
    this.setState({
      willModel: text,
    });
  };

  handleChangeText = (index, v) => {
    let selectedWills = [...this.state.selectedWills];
    selectedWills[index].text = v;
    this.setState({
      selectedWills,
    });
  };

  deleteEntry = will => {
    this.db = openDatabase({name: 'iwillDB', createFromLocation: 1}, () => {
      this.db.transaction(tx => {
        tx.executeSql(
          'DELETE FROM iwills WHERE id = ?',
          [will.id],
          (_tx, res) => {
            this.getWillsByDate();
            this.createNotificationScheduler();
          },
          error => {
            console.log(error);
          },
        );
      });
    });
  };

  updateEntries = () => {
    this.setState({
      editState: !this.state.editState,
    });

    const {selectedWills} = this.state;
    this.db = openDatabase({name: 'iwillDB', createFromLocation: 1}, () => {
      this.db.transaction(tx => {
        selectedWills.forEach((will, index) => {
          tx.executeSql(
            'UPDATE iwills set text=? where id=?',
            [will.text, will.id],
            (_tx, res) => {
              console.log(res);
              if (index === selectedWills.length - 1) {
                this.createNotificationScheduler();
              }
            },
            error => {
              console.log(error);
            },
          );
        });
      });
    });
  };

  willItemTap = will => {
    this.db = openDatabase({name: 'iwillDB', createFromLocation: 1}, () => {
      this.db.transaction(tx => {
        tx.executeSql(
          'UPDATE iwills set completed=? where id=?',
          [will.completed ? 0 : 1, will.id],
          (_tx, res) => {
            this.getWillsByDate();
          },
        );
      });
    });
  };

  editEntries = () => {
    this.setState({
      editState: !this.state.editState,
    });
  };

  render() {
    const {
      selectedWills,
      editState,
      addState,
      willModel,
      datePosition,
      selectedDate,
      isModalVisible,
    } = this.state;
    return (
      <LinearGradient colors={['#6FDB62', '#55a74a']} style={{flex: 1}}>
        <SafeAreaView style={styles.container}>
          <ScrollView style={{flex: 1}}>
            <Text style={styles.topline}>Make a change: 1 Step at a time</Text>
            <Text style={[styles.topline, {marginTop: 0, fontSize: 40}]}>
              I Will:
            </Text>
            {addState ? (
              <LinearGradient
                colors={['#fafafa', '#d9d9d9']}
                style={styles.entryPanel}>
                <TextInput
                  multiline={true}
                  style={styles.willEntry}
                  onChangeText={this.handleChange}
                />
                <View style={styles.entryActions}>
                  {willModel !== '' ? (
                    <TouchableOpacity
                      onPress={this.saveEntry}
                      style={[styles.addBtn, {width: '49%'}]}>
                      <Text style={styles.addBtnText}>Save</Text>
                    </TouchableOpacity>
                  ) : (
                    <View style={{width: '49%'}} />
                  )}
                  <TouchableOpacity
                    onPress={this.toggleAddEntry}
                    style={[styles.addBtn, {width: '49%'}]}>
                    <Text style={styles.addBtnText}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </LinearGradient>
            ) : (
              <LinearGradient
                colors={['#fafafa', '#d9d9d9']}
                style={styles.entryPanel}>
                {!selectedWills.length && (
                  <Text style={styles.noText}>
                    No I Wills have been entered for today!
                  </Text>
                )}
                {!editState && (
                  <>
                    {selectedWills.map((will, index) => {
                      return (
                        <TouchableOpacity
                          disabled={!this.isToday()}
                          onPress={() => this.willItemTap(will)}
                          style={styles.willListItem}
                          key={`will-edit-item-${index}`}>
                          <View>
                            <View style={{position: 'absolute'}}>
                              {will.completed ? (
                                <Text
                                  style={[
                                    styles.iconCheck,
                                    styles.completed,
                                    !this.isToday() && styles.disabledItem,
                                  ]}>
                                  &#xf3ff;
                                </Text>
                              ) : (
                                <Text
                                  style={[
                                    styles.iconCheck,
                                    !this.isToday() && styles.disabledItem,
                                  ]}>
                                  &#xf21b;
                                </Text>
                              )}
                            </View>
                            <View style={{position: 'absolute'}}>
                              <Text style={[styles.iconCheck, styles.outline]}>
                                &#xf401;
                              </Text>
                            </View>
                          </View>
                          <Text style={styles.willListItemText}>
                            {will.text}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </>
                )}
                {editState && (
                  <>
                    {selectedWills.map((will, index) => {
                      return (
                        <View
                          style={[
                            styles.willListItem,
                            styles.edit,
                            {flexDirection: 'row', marginVertical: 10},
                          ]}
                          key={`will-edit-item-${index}`}>
                          <TouchableOpacity
                            onPress={() => this.deleteEntry(will)}>
                            <Text style={[styles.iconCheck, styles.iconDelete]}>
                              &#xf128;
                            </Text>
                          </TouchableOpacity>
                          <View style={{flex: 1, height: 30}}>
                            <TextInput
                              value={will.text}
                              onChangeText={v =>
                                this.handleChangeText(index, v)
                              }
                              style={styles.editItemTextInput}
                            />
                          </View>
                        </View>
                      );
                    })}
                    <TouchableOpacity
                      onPress={this.updateEntries}
                      style={styles.addBtn}>
                      <Text style={styles.addBtnText}>Save Edits</Text>
                    </TouchableOpacity>
                  </>
                )}
                {selectedWills.length < 3 && this.isToday() && !editState && (
                  <TouchableOpacity
                    onPress={this.toggleAddEntry}
                    style={styles.addBtn}>
                    <Text style={styles.addBtnText}>Add I Will</Text>
                  </TouchableOpacity>
                )}

                {!editState && (
                  <View style={styles.iconButtons}>
                    {this.isToday() && selectedWills.length > 0 && (
                      <TouchableOpacity onPress={this.editEntries}>
                        <Text style={styles.iconText}>&#xf417;</Text>
                      </TouchableOpacity>
                    )}
                    {selectedWills.length > 0 && (
                      <TouchableOpacity onPress={this.shareEntries}>
                        <Text style={styles.iconText}>&#xf4ca;</Text>
                      </TouchableOpacity>
                    )}
                    <TouchableOpacity onPress={this.toggleRemindersModal}>
                      <Text style={styles.iconText}>&#xf3c7;</Text>
                    </TouchableOpacity>
                  </View>
                )}
              </LinearGradient>
            )}

            <View style={styles.dateSelector}>
              <TouchableOpacity
                onPress={() => this.changeDate(-1)}
                disabled={addState || datePosition === 0}
                style={{width: 40, alignItems: 'center'}}>
                <Text
                  style={[
                    styles.iconArrow,
                    (addState || datePosition === 0) && styles.disabled,
                  ]}>
                  &#xf3cf;
                </Text>
              </TouchableOpacity>
              <View>
                {this.isToday() && (
                  <Text style={[styles.dateText, styles.todayText]}>Today</Text>
                )}
                <Text style={[styles.dateText, styles.selectedDate]}>
                  {moment(selectedDate).format('MMMM DD, YYYY')}
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => this.changeDate(1)}
                disabled={addState || this.isToday()}
                style={{width: 40, alignItems: 'center'}}>
                <Text
                  style={[
                    styles.iconArrow,
                    (addState || this.isToday()) && styles.disabled,
                  ]}>
                  &#xf3d1;
                </Text>
              </TouchableOpacity>
            </View>
            <ModalWrapper
              containerStyle={{width: '100%', height: '100%'}}
              style={{width: '100%', height: '100%'}}
              onRequestClose={this.toggleRemindersModal}
              shouldAnimateOnRequestClose={true}
              shouldCloseOnOverlayPress={true}
              visible={isModalVisible}
              position={'right'}>
              <SetReminderModal
                onClose={this.toggleRemindersModal}
                deleteNotificationScheduler={() => {
                  if (Platform.OS === 'ios') {
                    PushNotificationIOS.cancelAllLocalNotifications();
                  } else {
                    PushNotification.cancelAllLocalNotifications();
                  }
                }}
                createNotificationScheduler={this.createNotificationScheduler}
              />
            </ModalWrapper>
          </ScrollView>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#014584',
    flex: 1,
  },
  topline: {
    marginTop: 10,
    color: 'white',
    textTransform: 'uppercase',
    textAlign: 'center',
    // fontFamily: 'Lato-Light',
    fontFamily: 'Lato',
  },
  noText: {
    // fontFamily: 'Lato-Light',
    fontFamily: 'Lato',
    color: '#5a5a5a',
    paddingVertical: 10,
    paddingHorizontal: 40,
    lineHeight: 28,
    fontSize: isTiny ? 20 : 22,
    textAlign: 'center',
  },
  addBtn: {
    backgroundColor: '#3c8b32',
    borderRadius: 8,
    padding: 8,
    marginTop: 20,
  },
  addBtnText: {
    textTransform: 'capitalize',
    fontFamily: 'Lato',
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
  },
  iconText: {
    fontFamily: 'Ionicons',
    fontSize: 36,
    color: '#3c8b32',
  },
  disabled: {
    opacity: 0.2,
  },
  iconArrow: {
    fontFamily: 'Ionicons',
    fontSize: 40,
    color: 'white',
    opacity: 0.6,
  },
  iconButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopColor: '#aaa',
    borderTopWidth: 1,
    marginTop: 20,
    marginBottom: 8,
    marginHorizontal: 0,
    paddingHorizontal: 20,
    paddingTop: 20,
    paddingBottom: 0,
  },
  dateSelector: {
    marginHorizontal: 20,
    marginTop: isSmall ? 0 : 20,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dateText: {
    // fontFamily: 'Lato-Light',
    fontFamily: 'Lato',
    fontSize: 15,
    textTransform: 'uppercase',
    color: 'white',
    textAlign: 'center',
  },
  todayText: {
    fontFamily: 'Lato-Regular',
  },
  willEntry: {
    height: 100,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 10,
    marginVertical: 10,
    fontSize: 16,
  },
  entryActions: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: -20,
  },
  entryPanel: {
    margin: 20,
    marginTop: isSmall ? 10 : 20,
    padding: 10,
    borderRadius: 10,
  },
  disabledItem: {
    color: '#aaa',
  },
  willListItem: {
    padding: 10,
    paddingVertical: isSmall ? 5 : 10,
  },
  edit: {
    paddingVertical: 0,
    paddingHorizontal: 10,
  },
  willListItemText: {
    fontFamily: 'Lato-Light',
    fontSize: isSmall ? 19 : 22,
    color: '#5a5a5a',
    marginLeft: 35,
    marginTop: isSmall ? 7 : 3,
  },
  editItemTextInput: {
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    paddingVertical: 0,
    borderRadius: 10,
    height: 33,
  },
  iconCheck: {
    fontFamily: 'Ionicons',
    fontSize: 33,
    marginRight: 10,
    color: 'white',
    opacity: 0.6,
  },
  iconDelete: {
    color: '#5a5a5a',
  },
  completed: {
    color: '#3c8b32',
    opacity: 1,
  },
  outline: {
    color: '#bebebe',
  },
});

export default IWillScreen;
