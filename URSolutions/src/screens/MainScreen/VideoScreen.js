/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {SafeAreaView, StyleSheet, View, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {WebView} from 'react-native-webview';

class VideoScreen extends React.Component {
  state = {};

  render() {
    const uri = 'https://vimeo.com/299535699';
    return (
      <LinearGradient colors={['#6FDB62', '#55a74a']} style={{flex: 1}}>
        <SafeAreaView style={styles.container}>
          <Text style={styles.topline}>Make a change: 1 Step at a time</Text>
          <Text style={[styles.topline, {marginTop: 0, fontSize: 40}]}>
            I Will:
          </Text>
          <View style={styles.section}>
            <View style={styles.video}>
              <WebView source={{uri: uri}} />
            </View>
            <Text style={styles.text}>Urban Risk Solutions I Will</Text>
          </View>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#014584',
    flex: 1,
  },
  section: {
    padding: 10,
  },
  video: {
    width: '100%',
    height: 200,
  },
  text: {
    marginVertical: 10,
    color: '#b7efb7',
    textTransform: 'uppercase',
  },
  topline: {
    marginTop: 10,
    color: 'white',
    textTransform: 'uppercase',
    textAlign: 'center',
    // fontFamily: 'Lato-Light',
    fontFamily: 'Lato',
  },
});

export default VideoScreen;
