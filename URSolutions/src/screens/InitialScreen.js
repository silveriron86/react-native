import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';
import {Platform} from 'react-native';

export default class InitialScreen extends React.Component {
  static navigationOptions = {
    header: false,
  };

  // UNSAFE_componentWillMount() {
  componentDidMount() {
    if (Platform.OS === 'android') {
      SplashScreen.hide();
    }
    AsyncStorage.getItem('VISITED', (_err, done) => {
      this.props.navigation.navigate(done ? 'MainScreen' : 'SlidesScreen');
    });
  }

  render() {
    return null;
  }
}
