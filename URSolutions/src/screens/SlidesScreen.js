/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react-native/no-inline-styles */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import FadeView from 'react-native-fade-view';
import AsyncStorage from '@react-native-community/async-storage';
// import {StackActions, NavigationActions} from 'react-navigation';

const {height} = Dimensions.get('window');
const isSmall = height < 600;
const isTiny = height < 530;

// eslint-disable-next-line no-unused-vars
const logo = require('../../assets/images/new_logo.png');
const slide1 = require('../../assets/images/slide1.png');
const slide2 = require('../../assets/images/slide2.png');
const slide3 = require('../../assets/images/slide3.png');
const slide4 = require('../../assets/images/slide4.png');
const slide5 = require('../../assets/images/slide5.png');

class SlidesScreen extends React.Component {
  state = {
    index: 0,
    active: false,
  };

  goNext = () => {
    if (this.state.index === 4) {
      this.onSkip();
      // const resetAction = StackActions.reset({
      //   index: 0,
      //   actions: [NavigationActions.navigate({routeName: 'MainScreen'})],
      // });
      // this.props.navigation.dispatch(resetAction);
      return;
    }

    this.setState({
      index: this.state.index + 1,
      active: true,
    });
    setTimeout(() => {
      this.setState({
        active: false,
      });
    }, 10);
  };

  goPrev = () => {
    this.setState({
      index: this.state.index - 1,
      active: true,
    });
    setTimeout(() => {
      this.setState({
        active: false,
      });
    }, 10);
  };

  onSkip = () => {
    AsyncStorage.setItem('VISITED', 'true');
    this.props.navigation.navigate('MainScreen');
  };

  render() {
    let {index, active} = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <View style={{flex: 1}}>
          {index === 0 && (
            <FadeView
              style={styles.introItem}
              animationDuration={450}
              active={active}>
              <View style={{flex: 1, justifyContent: 'space-between'}}>
                <View style={{flex: 1}}>
                  <View style={styles.topPad} />
                  <View>
                    <View>
                      <Text style={styles.label}>
                        <Text style={styles.top}>Welcome to{'\n'}</Text>
                        <Text style={styles.main}>I will</Text>
                      </Text>
                    </View>
                    <View style={styles.sub}>
                      <Text style={[styles.label, styles.subText]}>
                        A simple process to create the life you desire
                      </Text>
                    </View>
                    <View style={styles.content}>
                      <Text style={[styles.label, styles.contentText]}>
                        {
                          'Most of us live our lives like ships without rudders, drifting aimlessly wherever the wind blows and usually ending up on a rocky shore rather than a sandy beach.\n'
                        }
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={{flex: 1}}>
                  <Image
                    source={slide1}
                    style={[styles.slideImage, {top: isTiny ? -20 : 0}]}
                  />
                </View>
              </View>
            </FadeView>
          )}
          {index === 1 && (
            <FadeView
              style={styles.introItem}
              animationDuration={450}
              active={active}>
              <View style={{flex: 1, justifyContent: 'space-between'}}>
                <View style={{flex: 1}}>
                  <View style={styles.topPad} />
                  <View>
                    <Text style={styles.label}>
                      <Text style={styles.top}>FIRST STEP:{'\n'}</Text>
                      <Text style={styles.main}>COMMIT</Text>
                    </Text>
                  </View>
                  <View style={styles.sub}>
                    <Text style={[styles.label, styles.subText]}>
                      Write down your &ldquo;One Thing.&rdquo;
                    </Text>
                  </View>
                  <View style={styles.content}>
                    <Text style={[styles.label, styles.contentText]}>
                      {
                        'What is your INTENTION? What is your purpose in life? Your goal? Enter it on this app. You are 25% more likely to do something if you write it down.'
                      }
                    </Text>
                  </View>
                </View>
                <Image
                  source={slide2}
                  style={[
                    styles.slideImage,
                    {
                      width: isSmall ? 90 : 160,
                      height: isSmall ? 90 : 160,
                      alignSelf: 'center',
                      marginTop: 80,
                    },
                  ]}
                />
              </View>
            </FadeView>
          )}
          {index === 2 && (
            <FadeView
              style={styles.introItem}
              animationDuration={450}
              active={active}>
              <View style={{flex: 1, justifyContent: 'space-between'}}>
                <View style={{flex: 1, justifyContent: 'flex-start'}}>
                  <View style={styles.topPad} />
                  <View>
                    <Text style={styles.label}>
                      <Text style={styles.top}>SECOND STEP:{'\n'}</Text>
                      <Text style={styles.main}>SHARE</Text>
                    </Text>
                  </View>
                  <View style={styles.sub}>
                    <Text style={[styles.label, styles.subText]}>
                      Share your intention with someone you care about.
                    </Text>
                  </View>
                  <View style={styles.content}>
                    <Text style={[styles.label, styles.contentText]}>
                      {
                        'Success increases to 76.7% if you share your personal commitment with someone you care about and respect.'
                      }
                    </Text>
                  </View>
                </View>
                <Image
                  source={slide3}
                  style={[
                    styles.slideImage,
                    {
                      width: isSmall ? 90 : 160,
                      height: isSmall ? 90 : 160,
                      alignSelf: 'center',
                      marginTop: 80,
                    },
                  ]}
                />
              </View>
            </FadeView>
          )}
          {index === 3 && (
            <FadeView
              style={styles.introItem}
              animationDuration={450}
              active={active}>
              <View style={{flex: 1, justifyContent: 'space-between'}}>
                <View style={{flex: 1, justifyContent: 'flex-start'}}>
                  <View style={styles.topPad} />
                  <View>
                    <Text style={styles.label}>
                      <Text style={styles.top}>
                        Third Step (Your New Habit):{'\n'}
                      </Text>
                      <Text style={styles.main}>CHALLENGE</Text>
                    </Text>
                  </View>
                  <View style={styles.sub}>
                    <Text style={[styles.label, styles.subText]}>
                      Challenge a friend.
                    </Text>
                  </View>
                  <View style={styles.content}>
                    <Text style={[styles.label, styles.contentText]}>
                      {
                        'What three things SHOULD you be doing today to reach your intention? Use the URSolutions app to hold yourself and your friend accountable, DAILY.'
                      }
                    </Text>
                  </View>
                </View>
                <Image
                  source={slide4}
                  style={[
                    styles.slideImage,
                    {
                      width: isSmall ? 90 : 160,
                      height: isSmall ? 90 : 160,
                      alignSelf: 'center',
                      marginTop: 80,
                    },
                  ]}
                />
              </View>
            </FadeView>
          )}
          {index === 4 && (
            <FadeView
              style={styles.introItem}
              animationDuration={450}
              active={active}>
              <View style={{flex: 1, justifyContent: 'space-between'}}>
                <View style={{flex: 1, justifyContent: 'flex-start'}}>
                  <View style={styles.topPad} />
                  <View style={styles.sub}>
                    <Text
                      style={[styles.label, styles.subText, {fontSize: 26}]}>
                      Try this for a month, and see if your life gets better.
                    </Text>
                  </View>
                  <View style={styles.content}>
                    <Text
                      style={[
                        styles.label,
                        styles.contentText,
                        styles.contentFinal,
                      ]}>
                      What's your One Thing?
                    </Text>
                  </View>
                </View>
                <Image
                  source={slide5}
                  style={[
                    styles.slideImage,
                    {
                      width: isSmall ? 90 : 160,
                      height: isSmall ? 90 : 160,
                      alignSelf: 'center',
                    },
                  ]}
                />
                <TouchableOpacity onPress={this.goNext}>
                  <Text style={[styles.label, styles.getStarted]}>
                    get started >
                  </Text>
                </TouchableOpacity>
              </View>
            </FadeView>
          )}
        </View>
        <View style={styles.footer}>
          {index !== 0 ? (
            <TouchableOpacity style={{flex: 1}} onPress={this.goPrev}>
              <Text style={styles.buttonText}>&#xf124;</Text>
            </TouchableOpacity>
          ) : (
            <View style={{flex: 1}} />
          )}
          {index < 4 && (
            <View style={{flex: 1, alignItems: 'center'}}>
              <TouchableOpacity onPress={this.onSkip}>
                <Text style={[styles.buttonText, styles.skipText]}>SKIP</Text>
              </TouchableOpacity>
            </View>
          )}
          {index < 4 && (
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <TouchableOpacity onPress={this.goNext}>
                <Text style={styles.buttonText}>&#xf125;</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3c7b29',
    flex: 1,
  },
  introItem: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  label: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'Lato',
    // fontFamily: 'Lato-Light',
  },
  top: {
    textTransform: 'uppercase',
    padding: 0,
    fontSize: 20,
  },
  sub: {
    marginTop: 10,
    marginRight: 40,
    marginBottom: 30,
    marginLeft: 40,
  },
  subText: {
    fontSize: 18,
  },
  main: {
    fontSize: 40,
  },
  content: {
    borderTopWidth: 1,
    borderTopColor: 'white',
    paddingTop: 20,
    marginHorizontal: isTiny ? 20 : 40,
  },
  contentText: {
    fontSize: 18,
  },
  slideImage: {
    resizeMode: 'contain',
    width: '100%',
    tintColor: '#bcf1bc',
  },
  topPad: {
    height: isSmall ? 30 : 60,
  },
  contentFinal: {
    fontSize: 26,
    fontWeight: 'bold',
    paddingTop: 20,
  },
  footer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 20,
    width: '100%',
    marginBottom: isTiny ? 0 : 20,
  },
  buttonText: {
    fontFamily: 'Ionicons',
    fontSize: 40,
    color: '#7df07d',
    opacity: 0.4,
  },
  skipText: {
    fontSize: 26,
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  getStarted: {
    fontSize: 30,
    textTransform: 'uppercase',
    marginTop: isSmall ? 20 : 40,
    color: '#7df07d',
  },
});

export default SlidesScreen;
